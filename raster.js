
var Module;
if (typeof Module === 'undefined') Module = eval('(function() { try { return Module || {} } catch(e) { return {} } })()');
if (!Module.expectedDataFileDownloads) {
  Module.expectedDataFileDownloads = 0;
  Module.finishedDataFileDownloads = 0;
}
Module.expectedDataFileDownloads++;
(function() {
    function fetchRemotePackage(packageName, callback, errback) {
      var xhr = new XMLHttpRequest();
      xhr.open('GET', packageName, true);
      xhr.responseType = 'arraybuffer';
      xhr.onprogress = function(event) {
        var url = packageName;
        if (event.loaded && event.total) {
          if (!xhr.addedTotal) {
            xhr.addedTotal = true;
            if (!Module.dataFileDownloads) Module.dataFileDownloads = {};
            Module.dataFileDownloads[url] = {
              loaded: event.loaded,
              total: event.total
            };
          } else {
            Module.dataFileDownloads[url].loaded = event.loaded;
          }
          var total = 0;
          var loaded = 0;
          var num = 0;
          for (var download in Module.dataFileDownloads) {
          var data = Module.dataFileDownloads[download];
            total += data.total;
            loaded += data.loaded;
            num++;
          }
          total = Math.ceil(total * Module.expectedDataFileDownloads/num);
          if (Module['setStatus']) Module['setStatus']('Downloading data... (' + loaded + '/' + total + ')');
        } else if (!Module.dataFileDownloads) {
          if (Module['setStatus']) Module['setStatus']('Downloading data...');
        }
      };
      xhr.onload = function(event) {
        var packageData = xhr.response;
        callback(packageData);
      };
      xhr.send(null);
    };
    function handleError(error) {
      console.error('package error:', error);
    };
      var fetched = null, fetchedCallback = null;
      fetchRemotePackage('raster.data', function(data) {
        if (fetchedCallback) {
          fetchedCallback(data);
          fetchedCallback = null;
        } else {
          fetched = data;
        }
      }, handleError);
  function runWithFS() {
function assert(check, msg) {
  if (!check) throw msg + new Error().stack;
}
    function DataRequest(start, end, crunched, audio) {
      this.start = start;
      this.end = end;
      this.crunched = crunched;
      this.audio = audio;
    }
    DataRequest.prototype = {
      requests: {},
      open: function(mode, name) {
        this.name = name;
        this.requests[name] = this;
        Module['addRunDependency']('fp ' + this.name);
      },
      send: function() {},
      onload: function() {
        var byteArray = this.byteArray.subarray(this.start, this.end);
          this.finish(byteArray);
      },
      finish: function(byteArray) {
        var that = this;
        Module['FS_createPreloadedFile'](this.name, null, byteArray, true, true, function() {
          Module['removeRunDependency']('fp ' + that.name);
        }, function() {
          if (that.audio) {
            Module['removeRunDependency']('fp ' + that.name); // workaround for chromium bug 124926 (still no audio with this, but at least we don't hang)
          } else {
            Module.printErr('Preloading file ' + that.name + ' failed');
          }
        }, false, true); // canOwn this data in the filesystem, it is a slide into the heap that will never change
        this.requests[this.name] = null;
      },
    };
      new DataRequest(0, 20480512, 0, 0).open('GET', '/caf_small.tmp');
    var PACKAGE_PATH;
    if (typeof window === 'object') {
      PACKAGE_PATH = window['encodeURIComponent'](window.location.pathname.toString().substring(0, window.location.pathname.toString().lastIndexOf('/')) + '/');
    } else {
      // worker
      PACKAGE_PATH = encodeURIComponent(location.pathname.toString().substring(0, location.pathname.toString().lastIndexOf('/')) + '/');
    }
    var PACKAGE_NAME = 'raster.data';
    var REMOTE_PACKAGE_NAME = 'raster.data';
    var PACKAGE_UUID = 'fecbd357-4a05-4670-ad75-1146623646a3';
    function processPackageData(arrayBuffer) {
      Module.finishedDataFileDownloads++;
      assert(arrayBuffer, 'Loading data file failed.');
      var byteArray = new Uint8Array(arrayBuffer);
      var curr;
      // copy the entire loaded file into a spot in the heap. Files will refer to slices in that. They cannot be freed though.
      var ptr = Module['_malloc'](byteArray.length);
      Module['HEAPU8'].set(byteArray, ptr);
      DataRequest.prototype.byteArray = Module['HEAPU8'].subarray(ptr, ptr+byteArray.length);
          DataRequest.prototype.requests["/caf_small.tmp"].onload();
          Module['removeRunDependency']('datafile_raster.data');
    };
    Module['addRunDependency']('datafile_raster.data');
    if (!Module.preloadResults) Module.preloadResults = {};
      Module.preloadResults[PACKAGE_NAME] = {fromCache: false};
      if (fetched) {
        processPackageData(fetched);
        fetched = null;
      } else {
        fetchedCallback = processPackageData;
      }
  }
  if (Module['calledRun']) {
    runWithFS();
  } else {
    if (!Module['preRun']) Module['preRun'] = [];
    Module["preRun"].push(runWithFS); // FS is not initialized yet, wait for it
  }
})();
// Note: For maximum-speed code, see "Optimizing Code" on the Emscripten wiki, https://github.com/kripken/emscripten/wiki/Optimizing-Code
// Note: Some Emscripten settings may limit the speed of the generated code.
// The Module object: Our interface to the outside world. We import
// and export values on it, and do the work to get that through
// closure compiler if necessary. There are various ways Module can be used:
// 1. Not defined. We create it here
// 2. A function parameter, function(Module) { ..generated code.. }
// 3. pre-run appended it, var Module = {}; ..generated code..
// 4. External script tag defines var Module.
// We need to do an eval in order to handle the closure compiler
// case, where this code here is minified but Module was defined
// elsewhere (e.g. case 4 above). We also need to check if Module
// already exists (e.g. case 3 above).
// Note that if you want to run closure, and also to use Module
// after the generated code, you will need to define   var Module = {};
// before the code. Then that object will be used in the code, and you
// can continue to use Module afterwards as well.
var Module;
if (!Module) Module = eval('(function() { try { return Module || {} } catch(e) { return {} } })()');
// Sometimes an existing Module object exists with properties
// meant to overwrite the default module functionality. Here
// we collect those properties and reapply _after_ we configure
// the current environment's defaults to avoid having to be so
// defensive during initialization.
var moduleOverrides = {};
for (var key in Module) {
  if (Module.hasOwnProperty(key)) {
    moduleOverrides[key] = Module[key];
  }
}
// The environment setup code below is customized to use Module.
// *** Environment setup code ***
var ENVIRONMENT_IS_NODE = typeof process === 'object' && typeof require === 'function';
var ENVIRONMENT_IS_WEB = typeof window === 'object';
var ENVIRONMENT_IS_WORKER = typeof importScripts === 'function';
var ENVIRONMENT_IS_SHELL = !ENVIRONMENT_IS_WEB && !ENVIRONMENT_IS_NODE && !ENVIRONMENT_IS_WORKER;
if (ENVIRONMENT_IS_NODE) {
  // Expose functionality in the same simple way that the shells work
  // Note that we pollute the global namespace here, otherwise we break in node
  Module['print'] = function print(x) {
    process['stdout'].write(x + '\n');
  };
  Module['printErr'] = function printErr(x) {
    process['stderr'].write(x + '\n');
  };
  var nodeFS = require('fs');
  var nodePath = require('path');
  Module['read'] = function read(filename, binary) {
    filename = nodePath['normalize'](filename);
    var ret = nodeFS['readFileSync'](filename);
    // The path is absolute if the normalized version is the same as the resolved.
    if (!ret && filename != nodePath['resolve'](filename)) {
      filename = path.join(__dirname, '..', 'src', filename);
      ret = nodeFS['readFileSync'](filename);
    }
    if (ret && !binary) ret = ret.toString();
    return ret;
  };
  Module['readBinary'] = function readBinary(filename) { return Module['read'](filename, true) };
  Module['load'] = function load(f) {
    globalEval(read(f));
  };
  Module['arguments'] = process['argv'].slice(2);
  module['exports'] = Module;
}
else if (ENVIRONMENT_IS_SHELL) {
  Module['print'] = print;
  if (typeof printErr != 'undefined') Module['printErr'] = printErr; // not present in v8 or older sm
  if (typeof read != 'undefined') {
    Module['read'] = read;
  } else {
    Module['read'] = function read() { throw 'no read() available (jsc?)' };
  }
  Module['readBinary'] = function readBinary(f) {
    return read(f, 'binary');
  };
  if (typeof scriptArgs != 'undefined') {
    Module['arguments'] = scriptArgs;
  } else if (typeof arguments != 'undefined') {
    Module['arguments'] = arguments;
  }
  this['Module'] = Module;
  eval("if (typeof gc === 'function' && gc.toString().indexOf('[native code]') > 0) var gc = undefined"); // wipe out the SpiderMonkey shell 'gc' function, which can confuse closure (uses it as a minified name, and it is then initted to a non-falsey value unexpectedly)
}
else if (ENVIRONMENT_IS_WEB || ENVIRONMENT_IS_WORKER) {
  Module['read'] = function read(url) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, false);
    xhr.send(null);
    return xhr.responseText;
  };
  if (typeof arguments != 'undefined') {
    Module['arguments'] = arguments;
  }
  if (typeof console !== 'undefined') {
    Module['print'] = function print(x) {
      console.log(x);
    };
    Module['printErr'] = function printErr(x) {
      console.log(x);
    };
  } else {
    // Probably a worker, and without console.log. We can do very little here...
    var TRY_USE_DUMP = false;
    Module['print'] = (TRY_USE_DUMP && (typeof(dump) !== "undefined") ? (function(x) {
      dump(x);
    }) : (function(x) {
      // self.postMessage(x); // enable this if you want stdout to be sent as messages
    }));
  }
  if (ENVIRONMENT_IS_WEB) {
    this['Module'] = Module;
  } else {
    Module['load'] = importScripts;
  }
}
else {
  // Unreachable because SHELL is dependant on the others
  throw 'Unknown runtime environment. Where are we?';
}
function globalEval(x) {
  eval.call(null, x);
}
if (!Module['load'] == 'undefined' && Module['read']) {
  Module['load'] = function load(f) {
    globalEval(Module['read'](f));
  };
}
if (!Module['print']) {
  Module['print'] = function(){};
}
if (!Module['printErr']) {
  Module['printErr'] = Module['print'];
}
if (!Module['arguments']) {
  Module['arguments'] = [];
}
// *** Environment setup code ***
// Closure helpers
Module.print = Module['print'];
Module.printErr = Module['printErr'];
// Callbacks
Module['preRun'] = [];
Module['postRun'] = [];
// Merge back in the overrides
for (var key in moduleOverrides) {
  if (moduleOverrides.hasOwnProperty(key)) {
    Module[key] = moduleOverrides[key];
  }
}
// === Auto-generated preamble library stuff ===
//========================================
// Runtime code shared with compiler
//========================================
var Runtime = {
  stackSave: function () {
    return STACKTOP;
  },
  stackRestore: function (stackTop) {
    STACKTOP = stackTop;
  },
  forceAlign: function (target, quantum) {
    quantum = quantum || 4;
    if (quantum == 1) return target;
    if (isNumber(target) && isNumber(quantum)) {
      return Math.ceil(target/quantum)*quantum;
    } else if (isNumber(quantum) && isPowerOfTwo(quantum)) {
      return '(((' +target + ')+' + (quantum-1) + ')&' + -quantum + ')';
    }
    return 'Math.ceil((' + target + ')/' + quantum + ')*' + quantum;
  },
  isNumberType: function (type) {
    return type in Runtime.INT_TYPES || type in Runtime.FLOAT_TYPES;
  },
  isPointerType: function isPointerType(type) {
  return type[type.length-1] == '*';
},
  isStructType: function isStructType(type) {
  if (isPointerType(type)) return false;
  if (isArrayType(type)) return true;
  if (/<?{ ?[^}]* ?}>?/.test(type)) return true; // { i32, i8 } etc. - anonymous struct types
  // See comment in isStructPointerType()
  return type[0] == '%';
},
  INT_TYPES: {"i1":0,"i8":0,"i16":0,"i32":0,"i64":0},
  FLOAT_TYPES: {"float":0,"double":0},
  or64: function (x, y) {
    var l = (x | 0) | (y | 0);
    var h = (Math.round(x / 4294967296) | Math.round(y / 4294967296)) * 4294967296;
    return l + h;
  },
  and64: function (x, y) {
    var l = (x | 0) & (y | 0);
    var h = (Math.round(x / 4294967296) & Math.round(y / 4294967296)) * 4294967296;
    return l + h;
  },
  xor64: function (x, y) {
    var l = (x | 0) ^ (y | 0);
    var h = (Math.round(x / 4294967296) ^ Math.round(y / 4294967296)) * 4294967296;
    return l + h;
  },
  getNativeTypeSize: function (type) {
    switch (type) {
      case 'i1': case 'i8': return 1;
      case 'i16': return 2;
      case 'i32': return 4;
      case 'i64': return 8;
      case 'float': return 4;
      case 'double': return 8;
      default: {
        if (type[type.length-1] === '*') {
          return Runtime.QUANTUM_SIZE; // A pointer
        } else if (type[0] === 'i') {
          var bits = parseInt(type.substr(1));
          assert(bits % 8 === 0);
          return bits/8;
        } else {
          return 0;
        }
      }
    }
  },
  getNativeFieldSize: function (type) {
    return Math.max(Runtime.getNativeTypeSize(type), Runtime.QUANTUM_SIZE);
  },
  dedup: function dedup(items, ident) {
  var seen = {};
  if (ident) {
    return items.filter(function(item) {
      if (seen[item[ident]]) return false;
      seen[item[ident]] = true;
      return true;
    });
  } else {
    return items.filter(function(item) {
      if (seen[item]) return false;
      seen[item] = true;
      return true;
    });
  }
},
  set: function set() {
  var args = typeof arguments[0] === 'object' ? arguments[0] : arguments;
  var ret = {};
  for (var i = 0; i < args.length; i++) {
    ret[args[i]] = 0;
  }
  return ret;
},
  STACK_ALIGN: 8,
  getAlignSize: function (type, size, vararg) {
    // we align i64s and doubles on 64-bit boundaries, unlike x86
    if (vararg) return 8;
    if (!vararg && (type == 'i64' || type == 'double')) return 8;
    if (!type) return Math.min(size, 8); // align structures internally to 64 bits
    return Math.min(size || (type ? Runtime.getNativeFieldSize(type) : 0), Runtime.QUANTUM_SIZE);
  },
  calculateStructAlignment: function calculateStructAlignment(type) {
    type.flatSize = 0;
    type.alignSize = 0;
    var diffs = [];
    var prev = -1;
    var index = 0;
    type.flatIndexes = type.fields.map(function(field) {
      index++;
      var size, alignSize;
      if (Runtime.isNumberType(field) || Runtime.isPointerType(field)) {
        size = Runtime.getNativeTypeSize(field); // pack char; char; in structs, also char[X]s.
        alignSize = Runtime.getAlignSize(field, size);
      } else if (Runtime.isStructType(field)) {
        if (field[1] === '0') {
          // this is [0 x something]. When inside another structure like here, it must be at the end,
          // and it adds no size
          // XXX this happens in java-nbody for example... assert(index === type.fields.length, 'zero-length in the middle!');
          size = 0;
          if (Types.types[field]) {
            alignSize = Runtime.getAlignSize(null, Types.types[field].alignSize);
          } else {
            alignSize = type.alignSize || QUANTUM_SIZE;
          }
        } else {
          size = Types.types[field].flatSize;
          alignSize = Runtime.getAlignSize(null, Types.types[field].alignSize);
        }
      } else if (field[0] == 'b') {
        // bN, large number field, like a [N x i8]
        size = field.substr(1)|0;
        alignSize = 1;
      } else if (field[0] === '<') {
        // vector type
        size = alignSize = Types.types[field].flatSize; // fully aligned
      } else if (field[0] === 'i') {
        // illegal integer field, that could not be legalized because it is an internal structure field
        // it is ok to have such fields, if we just use them as markers of field size and nothing more complex
        size = alignSize = parseInt(field.substr(1))/8;
        assert(size % 1 === 0, 'cannot handle non-byte-size field ' + field);
      } else {
        assert(false, 'invalid type for calculateStructAlignment');
      }
      if (type.packed) alignSize = 1;
      type.alignSize = Math.max(type.alignSize, alignSize);
      var curr = Runtime.alignMemory(type.flatSize, alignSize); // if necessary, place this on aligned memory
      type.flatSize = curr + size;
      if (prev >= 0) {
        diffs.push(curr-prev);
      }
      prev = curr;
      return curr;
    });
    if (type.name_ && type.name_[0] === '[') {
      // arrays have 2 elements, so we get the proper difference. then we scale here. that way we avoid
      // allocating a potentially huge array for [999999 x i8] etc.
      type.flatSize = parseInt(type.name_.substr(1))*type.flatSize/2;
    }
    type.flatSize = Runtime.alignMemory(type.flatSize, type.alignSize);
    if (diffs.length == 0) {
      type.flatFactor = type.flatSize;
    } else if (Runtime.dedup(diffs).length == 1) {
      type.flatFactor = diffs[0];
    }
    type.needsFlattening = (type.flatFactor != 1);
    return type.flatIndexes;
  },
  generateStructInfo: function (struct, typeName, offset) {
    var type, alignment;
    if (typeName) {
      offset = offset || 0;
      type = (typeof Types === 'undefined' ? Runtime.typeInfo : Types.types)[typeName];
      if (!type) return null;
      if (type.fields.length != struct.length) {
        printErr('Number of named fields must match the type for ' + typeName + ': possibly duplicate struct names. Cannot return structInfo');
        return null;
      }
      alignment = type.flatIndexes;
    } else {
      var type = { fields: struct.map(function(item) { return item[0] }) };
      alignment = Runtime.calculateStructAlignment(type);
    }
    var ret = {
      __size__: type.flatSize
    };
    if (typeName) {
      struct.forEach(function(item, i) {
        if (typeof item === 'string') {
          ret[item] = alignment[i] + offset;
        } else {
          // embedded struct
          var key;
          for (var k in item) key = k;
          ret[key] = Runtime.generateStructInfo(item[key], type.fields[i], alignment[i]);
        }
      });
    } else {
      struct.forEach(function(item, i) {
        ret[item[1]] = alignment[i];
      });
    }
    return ret;
  },
  dynCall: function (sig, ptr, args) {
    if (args && args.length) {
      if (!args.splice) args = Array.prototype.slice.call(args);
      args.splice(0, 0, ptr);
      return Module['dynCall_' + sig].apply(null, args);
    } else {
      return Module['dynCall_' + sig].call(null, ptr);
    }
  },
  functionPointers: [],
  addFunction: function (func) {
    for (var i = 0; i < Runtime.functionPointers.length; i++) {
      if (!Runtime.functionPointers[i]) {
        Runtime.functionPointers[i] = func;
        return 2*(1 + i);
      }
    }
    throw 'Finished up all reserved function pointers. Use a higher value for RESERVED_FUNCTION_POINTERS.';
  },
  removeFunction: function (index) {
    Runtime.functionPointers[(index-2)/2] = null;
  },
  getAsmConst: function (code, numArgs) {
    // code is a constant string on the heap, so we can cache these
    if (!Runtime.asmConstCache) Runtime.asmConstCache = {};
    var func = Runtime.asmConstCache[code];
    if (func) return func;
    var args = [];
    for (var i = 0; i < numArgs; i++) {
      args.push(String.fromCharCode(36) + i); // $0, $1 etc
    }
    return Runtime.asmConstCache[code] = eval('(function(' + args.join(',') + '){ ' + Pointer_stringify(code) + ' })'); // new Function does not allow upvars in node
  },
  warnOnce: function (text) {
    if (!Runtime.warnOnce.shown) Runtime.warnOnce.shown = {};
    if (!Runtime.warnOnce.shown[text]) {
      Runtime.warnOnce.shown[text] = 1;
      Module.printErr(text);
    }
  },
  funcWrappers: {},
  getFuncWrapper: function (func, sig) {
    assert(sig);
    if (!Runtime.funcWrappers[func]) {
      Runtime.funcWrappers[func] = function dynCall_wrapper() {
        return Runtime.dynCall(sig, func, arguments);
      };
    }
    return Runtime.funcWrappers[func];
  },
  UTF8Processor: function () {
    var buffer = [];
    var needed = 0;
    this.processCChar = function (code) {
      code = code & 0xFF;
      if (buffer.length == 0) {
        if ((code & 0x80) == 0x00) {        // 0xxxxxxx
          return String.fromCharCode(code);
        }
        buffer.push(code);
        if ((code & 0xE0) == 0xC0) {        // 110xxxxx
          needed = 1;
        } else if ((code & 0xF0) == 0xE0) { // 1110xxxx
          needed = 2;
        } else {                            // 11110xxx
          needed = 3;
        }
        return '';
      }
      if (needed) {
        buffer.push(code);
        needed--;
        if (needed > 0) return '';
      }
      var c1 = buffer[0];
      var c2 = buffer[1];
      var c3 = buffer[2];
      var c4 = buffer[3];
      var ret;
      if (buffer.length == 2) {
        ret = String.fromCharCode(((c1 & 0x1F) << 6)  | (c2 & 0x3F));
      } else if (buffer.length == 3) {
        ret = String.fromCharCode(((c1 & 0x0F) << 12) | ((c2 & 0x3F) << 6)  | (c3 & 0x3F));
      } else {
        // http://mathiasbynens.be/notes/javascript-encoding#surrogate-formulae
        var codePoint = ((c1 & 0x07) << 18) | ((c2 & 0x3F) << 12) |
                        ((c3 & 0x3F) << 6)  | (c4 & 0x3F);
        ret = String.fromCharCode(
          Math.floor((codePoint - 0x10000) / 0x400) + 0xD800,
          (codePoint - 0x10000) % 0x400 + 0xDC00);
      }
      buffer.length = 0;
      return ret;
    }
    this.processJSString = function processJSString(string) {
      string = unescape(encodeURIComponent(string));
      var ret = [];
      for (var i = 0; i < string.length; i++) {
        ret.push(string.charCodeAt(i));
      }
      return ret;
    }
  },
  stackAlloc: function (size) { var ret = STACKTOP;STACKTOP = (STACKTOP + size)|0;STACKTOP = (((STACKTOP)+7)&-8); return ret; },
  staticAlloc: function (size) { var ret = STATICTOP;STATICTOP = (STATICTOP + size)|0;STATICTOP = (((STATICTOP)+7)&-8); return ret; },
  dynamicAlloc: function (size) { var ret = DYNAMICTOP;DYNAMICTOP = (DYNAMICTOP + size)|0;DYNAMICTOP = (((DYNAMICTOP)+7)&-8); if (DYNAMICTOP >= TOTAL_MEMORY) enlargeMemory();; return ret; },
  alignMemory: function (size,quantum) { var ret = size = Math.ceil((size)/(quantum ? quantum : 8))*(quantum ? quantum : 8); return ret; },
  makeBigInt: function (low,high,unsigned) { var ret = (unsigned ? ((+((low>>>0)))+((+((high>>>0)))*(+4294967296))) : ((+((low>>>0)))+((+((high|0)))*(+4294967296)))); return ret; },
  GLOBAL_BASE: 8,
  QUANTUM_SIZE: 4,
  __dummy__: 0
}
//========================================
// Runtime essentials
//========================================
var __THREW__ = 0; // Used in checking for thrown exceptions.
var ABORT = false; // whether we are quitting the application. no code should run after this. set in exit() and abort()
var EXITSTATUS = 0;
var undef = 0;
// tempInt is used for 32-bit signed values or smaller. tempBigInt is used
// for 32-bit unsigned values or more than 32 bits. TODO: audit all uses of tempInt
var tempValue, tempInt, tempBigInt, tempInt2, tempBigInt2, tempPair, tempBigIntI, tempBigIntR, tempBigIntS, tempBigIntP, tempBigIntD, tempDouble, tempFloat;
var tempI64, tempI64b;
var tempRet0, tempRet1, tempRet2, tempRet3, tempRet4, tempRet5, tempRet6, tempRet7, tempRet8, tempRet9;
function assert(condition, text) {
  if (!condition) {
    abort('Assertion failed: ' + text);
  }
}
var globalScope = this;
// C calling interface. A convenient way to call C functions (in C files, or
// defined with extern "C").
//
// Note: LLVM optimizations can inline and remove functions, after which you will not be
//       able to call them. Closure can also do so. To avoid that, add your function to
//       the exports using something like
//
//         -s EXPORTED_FUNCTIONS='["_main", "_myfunc"]'
//
// @param ident      The name of the C function (note that C++ functions will be name-mangled - use extern "C")
// @param returnType The return type of the function, one of the JS types 'number', 'string' or 'array' (use 'number' for any C pointer, and
//                   'array' for JavaScript arrays and typed arrays; note that arrays are 8-bit).
// @param argTypes   An array of the types of arguments for the function (if there are no arguments, this can be ommitted). Types are as in returnType,
//                   except that 'array' is not possible (there is no way for us to know the length of the array)
// @param args       An array of the arguments to the function, as native JS values (as in returnType)
//                   Note that string arguments will be stored on the stack (the JS string will become a C string on the stack).
// @return           The return value, as a native JS value (as in returnType)
function ccall(ident, returnType, argTypes, args) {
  return ccallFunc(getCFunc(ident), returnType, argTypes, args);
}
Module["ccall"] = ccall;
// Returns the C function with a specified identifier (for C++, you need to do manual name mangling)
function getCFunc(ident) {
  try {
    var func = Module['_' + ident]; // closure exported function
    if (!func) func = eval('_' + ident); // explicit lookup
  } catch(e) {
  }
  assert(func, 'Cannot call unknown function ' + ident + ' (perhaps LLVM optimizations or closure removed it?)');
  return func;
}
// Internal function that does a C call using a function, not an identifier
function ccallFunc(func, returnType, argTypes, args) {
  var stack = 0;
  function toC(value, type) {
    if (type == 'string') {
      if (value === null || value === undefined || value === 0) return 0; // null string
      value = intArrayFromString(value);
      type = 'array';
    }
    if (type == 'array') {
      if (!stack) stack = Runtime.stackSave();
      var ret = Runtime.stackAlloc(value.length);
      writeArrayToMemory(value, ret);
      return ret;
    }
    return value;
  }
  function fromC(value, type) {
    if (type == 'string') {
      return Pointer_stringify(value);
    }
    assert(type != 'array');
    return value;
  }
  var i = 0;
  var cArgs = args ? args.map(function(arg) {
    return toC(arg, argTypes[i++]);
  }) : [];
  var ret = fromC(func.apply(null, cArgs), returnType);
  if (stack) Runtime.stackRestore(stack);
  return ret;
}
// Returns a native JS wrapper for a C function. This is similar to ccall, but
// returns a function you can call repeatedly in a normal way. For example:
//
//   var my_function = cwrap('my_c_function', 'number', ['number', 'number']);
//   alert(my_function(5, 22));
//   alert(my_function(99, 12));
//
function cwrap(ident, returnType, argTypes) {
  var func = getCFunc(ident);
  return function() {
    return ccallFunc(func, returnType, argTypes, Array.prototype.slice.call(arguments));
  }
}
Module["cwrap"] = cwrap;
// Sets a value in memory in a dynamic way at run-time. Uses the
// type data. This is the same as makeSetValue, except that
// makeSetValue is done at compile-time and generates the needed
// code then, whereas this function picks the right code at
// run-time.
// Note that setValue and getValue only do *aligned* writes and reads!
// Note that ccall uses JS types as for defining types, while setValue and
// getValue need LLVM types ('i8', 'i32') - this is a lower-level operation
function setValue(ptr, value, type, noSafe) {
  type = type || 'i8';
  if (type.charAt(type.length-1) === '*') type = 'i32'; // pointers are 32-bit
    switch(type) {
      case 'i1': HEAP8[(ptr)]=value; break;
      case 'i8': HEAP8[(ptr)]=value; break;
      case 'i16': HEAP16[((ptr)>>1)]=value; break;
      case 'i32': HEAP32[((ptr)>>2)]=value; break;
      case 'i64': (tempI64 = [value>>>0,(tempDouble=value,(+(Math_abs(tempDouble))) >= (+1) ? (tempDouble > (+0) ? ((Math_min((+(Math_floor((tempDouble)/(+4294967296)))), (+4294967295)))|0)>>>0 : (~~((+(Math_ceil((tempDouble - +(((~~(tempDouble)))>>>0))/(+4294967296))))))>>>0) : 0)],HEAP32[((ptr)>>2)]=tempI64[0],HEAP32[(((ptr)+(4))>>2)]=tempI64[1]); break;
      case 'float': HEAPF32[((ptr)>>2)]=value; break;
      case 'double': HEAPF64[((ptr)>>3)]=value; break;
      default: abort('invalid type for setValue: ' + type);
    }
}
Module['setValue'] = setValue;
// Parallel to setValue.
function getValue(ptr, type, noSafe) {
  type = type || 'i8';
  if (type.charAt(type.length-1) === '*') type = 'i32'; // pointers are 32-bit
    switch(type) {
      case 'i1': return HEAP8[(ptr)];
      case 'i8': return HEAP8[(ptr)];
      case 'i16': return HEAP16[((ptr)>>1)];
      case 'i32': return HEAP32[((ptr)>>2)];
      case 'i64': return HEAP32[((ptr)>>2)];
      case 'float': return HEAPF32[((ptr)>>2)];
      case 'double': return HEAPF64[((ptr)>>3)];
      default: abort('invalid type for setValue: ' + type);
    }
  return null;
}
Module['getValue'] = getValue;
var ALLOC_NORMAL = 0; // Tries to use _malloc()
var ALLOC_STACK = 1; // Lives for the duration of the current function call
var ALLOC_STATIC = 2; // Cannot be freed
var ALLOC_DYNAMIC = 3; // Cannot be freed except through sbrk
var ALLOC_NONE = 4; // Do not allocate
Module['ALLOC_NORMAL'] = ALLOC_NORMAL;
Module['ALLOC_STACK'] = ALLOC_STACK;
Module['ALLOC_STATIC'] = ALLOC_STATIC;
Module['ALLOC_DYNAMIC'] = ALLOC_DYNAMIC;
Module['ALLOC_NONE'] = ALLOC_NONE;
// allocate(): This is for internal use. You can use it yourself as well, but the interface
//             is a little tricky (see docs right below). The reason is that it is optimized
//             for multiple syntaxes to save space in generated code. So you should
//             normally not use allocate(), and instead allocate memory using _malloc(),
//             initialize it with setValue(), and so forth.
// @slab: An array of data, or a number. If a number, then the size of the block to allocate,
//        in *bytes* (note that this is sometimes confusing: the next parameter does not
//        affect this!)
// @types: Either an array of types, one for each byte (or 0 if no type at that position),
//         or a single type which is used for the entire block. This only matters if there
//         is initial data - if @slab is a number, then this does not matter at all and is
//         ignored.
// @allocator: How to allocate memory, see ALLOC_*
function allocate(slab, types, allocator, ptr) {
  var zeroinit, size;
  if (typeof slab === 'number') {
    zeroinit = true;
    size = slab;
  } else {
    zeroinit = false;
    size = slab.length;
  }
  var singleType = typeof types === 'string' ? types : null;
  var ret;
  if (allocator == ALLOC_NONE) {
    ret = ptr;
  } else {
    ret = [_malloc, Runtime.stackAlloc, Runtime.staticAlloc, Runtime.dynamicAlloc][allocator === undefined ? ALLOC_STATIC : allocator](Math.max(size, singleType ? 1 : types.length));
  }
  if (zeroinit) {
    var ptr = ret, stop;
    assert((ret & 3) == 0);
    stop = ret + (size & ~3);
    for (; ptr < stop; ptr += 4) {
      HEAP32[((ptr)>>2)]=0;
    }
    stop = ret + size;
    while (ptr < stop) {
      HEAP8[((ptr++)|0)]=0;
    }
    return ret;
  }
  if (singleType === 'i8') {
    if (slab.subarray || slab.slice) {
      HEAPU8.set(slab, ret);
    } else {
      HEAPU8.set(new Uint8Array(slab), ret);
    }
    return ret;
  }
  var i = 0, type, typeSize, previousType;
  while (i < size) {
    var curr = slab[i];
    if (typeof curr === 'function') {
      curr = Runtime.getFunctionIndex(curr);
    }
    type = singleType || types[i];
    if (type === 0) {
      i++;
      continue;
    }
    if (type == 'i64') type = 'i32'; // special case: we have one i32 here, and one i32 later
    setValue(ret+i, curr, type);
    // no need to look up size unless type changes, so cache it
    if (previousType !== type) {
      typeSize = Runtime.getNativeTypeSize(type);
      previousType = type;
    }
    i += typeSize;
  }
  return ret;
}
Module['allocate'] = allocate;
function Pointer_stringify(ptr, /* optional */ length) {
  // TODO: use TextDecoder
  // Find the length, and check for UTF while doing so
  var hasUtf = false;
  var t;
  var i = 0;
  while (1) {
    t = HEAPU8[(((ptr)+(i))|0)];
    if (t >= 128) hasUtf = true;
    else if (t == 0 && !length) break;
    i++;
    if (length && i == length) break;
  }
  if (!length) length = i;
  var ret = '';
  if (!hasUtf) {
    var MAX_CHUNK = 1024; // split up into chunks, because .apply on a huge string can overflow the stack
    var curr;
    while (length > 0) {
      curr = String.fromCharCode.apply(String, HEAPU8.subarray(ptr, ptr + Math.min(length, MAX_CHUNK)));
      ret = ret ? ret + curr : curr;
      ptr += MAX_CHUNK;
      length -= MAX_CHUNK;
    }
    return ret;
  }
  var utf8 = new Runtime.UTF8Processor();
  for (i = 0; i < length; i++) {
    t = HEAPU8[(((ptr)+(i))|0)];
    ret += utf8.processCChar(t);
  }
  return ret;
}
Module['Pointer_stringify'] = Pointer_stringify;
// Given a pointer 'ptr' to a null-terminated UTF16LE-encoded string in the emscripten HEAP, returns
// a copy of that string as a Javascript String object.
function UTF16ToString(ptr) {
  var i = 0;
  var str = '';
  while (1) {
    var codeUnit = HEAP16[(((ptr)+(i*2))>>1)];
    if (codeUnit == 0)
      return str;
    ++i;
    // fromCharCode constructs a character from a UTF-16 code unit, so we can pass the UTF16 string right through.
    str += String.fromCharCode(codeUnit);
  }
}
Module['UTF16ToString'] = UTF16ToString;
// Copies the given Javascript String object 'str' to the emscripten HEAP at address 'outPtr',
// null-terminated and encoded in UTF16LE form. The copy will require at most (str.length*2+1)*2 bytes of space in the HEAP.
function stringToUTF16(str, outPtr) {
  for(var i = 0; i < str.length; ++i) {
    // charCodeAt returns a UTF-16 encoded code unit, so it can be directly written to the HEAP.
    var codeUnit = str.charCodeAt(i); // possibly a lead surrogate
    HEAP16[(((outPtr)+(i*2))>>1)]=codeUnit;
  }
  // Null-terminate the pointer to the HEAP.
  HEAP16[(((outPtr)+(str.length*2))>>1)]=0;
}
Module['stringToUTF16'] = stringToUTF16;
// Given a pointer 'ptr' to a null-terminated UTF32LE-encoded string in the emscripten HEAP, returns
// a copy of that string as a Javascript String object.
function UTF32ToString(ptr) {
  var i = 0;
  var str = '';
  while (1) {
    var utf32 = HEAP32[(((ptr)+(i*4))>>2)];
    if (utf32 == 0)
      return str;
    ++i;
    // Gotcha: fromCharCode constructs a character from a UTF-16 encoded code (pair), not from a Unicode code point! So encode the code point to UTF-16 for constructing.
    if (utf32 >= 0x10000) {
      var ch = utf32 - 0x10000;
      str += String.fromCharCode(0xD800 | (ch >> 10), 0xDC00 | (ch & 0x3FF));
    } else {
      str += String.fromCharCode(utf32);
    }
  }
}
Module['UTF32ToString'] = UTF32ToString;
// Copies the given Javascript String object 'str' to the emscripten HEAP at address 'outPtr',
// null-terminated and encoded in UTF32LE form. The copy will require at most (str.length+1)*4 bytes of space in the HEAP,
// but can use less, since str.length does not return the number of characters in the string, but the number of UTF-16 code units in the string.
function stringToUTF32(str, outPtr) {
  var iChar = 0;
  for(var iCodeUnit = 0; iCodeUnit < str.length; ++iCodeUnit) {
    // Gotcha: charCodeAt returns a 16-bit word that is a UTF-16 encoded code unit, not a Unicode code point of the character! We must decode the string to UTF-32 to the heap.
    var codeUnit = str.charCodeAt(iCodeUnit); // possibly a lead surrogate
    if (codeUnit >= 0xD800 && codeUnit <= 0xDFFF) {
      var trailSurrogate = str.charCodeAt(++iCodeUnit);
      codeUnit = 0x10000 + ((codeUnit & 0x3FF) << 10) | (trailSurrogate & 0x3FF);
    }
    HEAP32[(((outPtr)+(iChar*4))>>2)]=codeUnit;
    ++iChar;
  }
  // Null-terminate the pointer to the HEAP.
  HEAP32[(((outPtr)+(iChar*4))>>2)]=0;
}
Module['stringToUTF32'] = stringToUTF32;
function demangle(func) {
  try {
    // Special-case the entry point, since its name differs from other name mangling.
    if (func == 'Object._main' || func == '_main') {
      return 'main()';
    }
    if (typeof func === 'number') func = Pointer_stringify(func);
    if (func[0] !== '_') return func;
    if (func[1] !== '_') return func; // C function
    if (func[2] !== 'Z') return func;
    switch (func[3]) {
      case 'n': return 'operator new()';
      case 'd': return 'operator delete()';
    }
    var i = 3;
    // params, etc.
    var basicTypes = {
      'v': 'void',
      'b': 'bool',
      'c': 'char',
      's': 'short',
      'i': 'int',
      'l': 'long',
      'f': 'float',
      'd': 'double',
      'w': 'wchar_t',
      'a': 'signed char',
      'h': 'unsigned char',
      't': 'unsigned short',
      'j': 'unsigned int',
      'm': 'unsigned long',
      'x': 'long long',
      'y': 'unsigned long long',
      'z': '...'
    };
    function dump(x) {
      //return;
      if (x) Module.print(x);
      Module.print(func);
      var pre = '';
      for (var a = 0; a < i; a++) pre += ' ';
      Module.print (pre + '^');
    }
    var subs = [];
    function parseNested() {
      i++;
      if (func[i] === 'K') i++; // ignore const
      var parts = [];
      while (func[i] !== 'E') {
        if (func[i] === 'S') { // substitution
          i++;
          var next = func.indexOf('_', i);
          var num = func.substring(i, next) || 0;
          parts.push(subs[num] || '?');
          i = next+1;
          continue;
        }
        if (func[i] === 'C') { // constructor
          parts.push(parts[parts.length-1]);
          i += 2;
          continue;
        }
        var size = parseInt(func.substr(i));
        var pre = size.toString().length;
        if (!size || !pre) { i--; break; } // counter i++ below us
        var curr = func.substr(i + pre, size);
        parts.push(curr);
        subs.push(curr);
        i += pre + size;
      }
      i++; // skip E
      return parts;
    }
    var first = true;
    function parse(rawList, limit, allowVoid) { // main parser
      limit = limit || Infinity;
      var ret = '', list = [];
      function flushList() {
        return '(' + list.join(', ') + ')';
      }
      var name;
      if (func[i] === 'N') {
        // namespaced N-E
        name = parseNested().join('::');
        limit--;
        if (limit === 0) return rawList ? [name] : name;
      } else {
        // not namespaced
        if (func[i] === 'K' || (first && func[i] === 'L')) i++; // ignore const and first 'L'
        var size = parseInt(func.substr(i));
        if (size) {
          var pre = size.toString().length;
          name = func.substr(i + pre, size);
          i += pre + size;
        }
      }
      first = false;
      if (func[i] === 'I') {
        i++;
        var iList = parse(true);
        var iRet = parse(true, 1, true);
        ret += iRet[0] + ' ' + name + '<' + iList.join(', ') + '>';
      } else {
        ret = name;
      }
      paramLoop: while (i < func.length && limit-- > 0) {
        //dump('paramLoop');
        var c = func[i++];
        if (c in basicTypes) {
          list.push(basicTypes[c]);
        } else {
          switch (c) {
            case 'P': list.push(parse(true, 1, true)[0] + '*'); break; // pointer
            case 'R': list.push(parse(true, 1, true)[0] + '&'); break; // reference
            case 'L': { // literal
              i++; // skip basic type
              var end = func.indexOf('E', i);
              var size = end - i;
              list.push(func.substr(i, size));
              i += size + 2; // size + 'EE'
              break;
            }
            case 'A': { // array
              var size = parseInt(func.substr(i));
              i += size.toString().length;
              if (func[i] !== '_') throw '?';
              i++; // skip _
              list.push(parse(true, 1, true)[0] + ' [' + size + ']');
              break;
            }
            case 'E': break paramLoop;
            default: ret += '?' + c; break paramLoop;
          }
        }
      }
      if (!allowVoid && list.length === 1 && list[0] === 'void') list = []; // avoid (void)
      return rawList ? list : ret + flushList();
    }
    return parse();
  } catch(e) {
    return func;
  }
}
function demangleAll(text) {
  return text.replace(/__Z[\w\d_]+/g, function(x) { var y = demangle(x); return x === y ? x : (x + ' [' + y + ']') });
}
function stackTrace() {
  var stack = new Error().stack;
  return stack ? demangleAll(stack) : '(no stack trace available)'; // Stack trace is not available at least on IE10 and Safari 6.
}
// Memory management
var PAGE_SIZE = 4096;
function alignMemoryPage(x) {
  return (x+4095)&-4096;
}
var HEAP;
var HEAP8, HEAPU8, HEAP16, HEAPU16, HEAP32, HEAPU32, HEAPF32, HEAPF64;
var STATIC_BASE = 0, STATICTOP = 0, staticSealed = false; // static area
var STACK_BASE = 0, STACKTOP = 0, STACK_MAX = 0; // stack area
var DYNAMIC_BASE = 0, DYNAMICTOP = 0; // dynamic area handled by sbrk
function enlargeMemory() {
  abort('Cannot enlarge memory arrays in asm.js. Either (1) compile with -s TOTAL_MEMORY=X with X higher than the current value ' + TOTAL_MEMORY + ', or (2) set Module.TOTAL_MEMORY before the program runs.');
}
var TOTAL_STACK = Module['TOTAL_STACK'] || 5242880;
var TOTAL_MEMORY = Module['TOTAL_MEMORY'] || 268435456;
var FAST_MEMORY = Module['FAST_MEMORY'] || 2097152;
var totalMemory = 4096;
while (totalMemory < TOTAL_MEMORY || totalMemory < 2*TOTAL_STACK) {
  if (totalMemory < 16*1024*1024) {
    totalMemory *= 2;
  } else {
    totalMemory += 16*1024*1024
  }
}
if (totalMemory !== TOTAL_MEMORY) {
  Module.printErr('increasing TOTAL_MEMORY to ' + totalMemory + ' to be more reasonable');
  TOTAL_MEMORY = totalMemory;
}
// Initialize the runtime's memory
// check for full engine support (use string 'subarray' to avoid closure compiler confusion)
assert(typeof Int32Array !== 'undefined' && typeof Float64Array !== 'undefined' && !!(new Int32Array(1)['subarray']) && !!(new Int32Array(1)['set']),
       'Cannot fallback to non-typed array case: Code is too specialized');
var buffer = new ArrayBuffer(TOTAL_MEMORY);
HEAP8 = new Int8Array(buffer);
HEAP16 = new Int16Array(buffer);
HEAP32 = new Int32Array(buffer);
HEAPU8 = new Uint8Array(buffer);
HEAPU16 = new Uint16Array(buffer);
HEAPU32 = new Uint32Array(buffer);
HEAPF32 = new Float32Array(buffer);
HEAPF64 = new Float64Array(buffer);
// Endianness check (note: assumes compiler arch was little-endian)
HEAP32[0] = 255;
assert(HEAPU8[0] === 255 && HEAPU8[3] === 0, 'Typed arrays 2 must be run on a little-endian system');
Module['HEAP'] = HEAP;
Module['HEAP8'] = HEAP8;
Module['HEAP16'] = HEAP16;
Module['HEAP32'] = HEAP32;
Module['HEAPU8'] = HEAPU8;
Module['HEAPU16'] = HEAPU16;
Module['HEAPU32'] = HEAPU32;
Module['HEAPF32'] = HEAPF32;
Module['HEAPF64'] = HEAPF64;
function callRuntimeCallbacks(callbacks) {
  while(callbacks.length > 0) {
    var callback = callbacks.shift();
    if (typeof callback == 'function') {
      callback();
      continue;
    }
    var func = callback.func;
    if (typeof func === 'number') {
      if (callback.arg === undefined) {
        Runtime.dynCall('v', func);
      } else {
        Runtime.dynCall('vi', func, [callback.arg]);
      }
    } else {
      func(callback.arg === undefined ? null : callback.arg);
    }
  }
}
var __ATPRERUN__  = []; // functions called before the runtime is initialized
var __ATINIT__    = []; // functions called during startup
var __ATMAIN__    = []; // functions called when main() is to be run
var __ATEXIT__    = []; // functions called during shutdown
var __ATPOSTRUN__ = []; // functions called after the runtime has exited
var runtimeInitialized = false;
function preRun() {
  // compatibility - merge in anything from Module['preRun'] at this time
  if (Module['preRun']) {
    if (typeof Module['preRun'] == 'function') Module['preRun'] = [Module['preRun']];
    while (Module['preRun'].length) {
      addOnPreRun(Module['preRun'].shift());
    }
  }
  callRuntimeCallbacks(__ATPRERUN__);
}
function ensureInitRuntime() {
  if (runtimeInitialized) return;
  runtimeInitialized = true;
  callRuntimeCallbacks(__ATINIT__);
}
function preMain() {
  callRuntimeCallbacks(__ATMAIN__);
}
function exitRuntime() {
  callRuntimeCallbacks(__ATEXIT__);
}
function postRun() {
  // compatibility - merge in anything from Module['postRun'] at this time
  if (Module['postRun']) {
    if (typeof Module['postRun'] == 'function') Module['postRun'] = [Module['postRun']];
    while (Module['postRun'].length) {
      addOnPostRun(Module['postRun'].shift());
    }
  }
  callRuntimeCallbacks(__ATPOSTRUN__);
}
function addOnPreRun(cb) {
  __ATPRERUN__.unshift(cb);
}
Module['addOnPreRun'] = Module.addOnPreRun = addOnPreRun;
function addOnInit(cb) {
  __ATINIT__.unshift(cb);
}
Module['addOnInit'] = Module.addOnInit = addOnInit;
function addOnPreMain(cb) {
  __ATMAIN__.unshift(cb);
}
Module['addOnPreMain'] = Module.addOnPreMain = addOnPreMain;
function addOnExit(cb) {
  __ATEXIT__.unshift(cb);
}
Module['addOnExit'] = Module.addOnExit = addOnExit;
function addOnPostRun(cb) {
  __ATPOSTRUN__.unshift(cb);
}
Module['addOnPostRun'] = Module.addOnPostRun = addOnPostRun;
// Tools
// This processes a JS string into a C-line array of numbers, 0-terminated.
// For LLVM-originating strings, see parser.js:parseLLVMString function
function intArrayFromString(stringy, dontAddNull, length /* optional */) {
  var ret = (new Runtime.UTF8Processor()).processJSString(stringy);
  if (length) {
    ret.length = length;
  }
  if (!dontAddNull) {
    ret.push(0);
  }
  return ret;
}
Module['intArrayFromString'] = intArrayFromString;
function intArrayToString(array) {
  var ret = [];
  for (var i = 0; i < array.length; i++) {
    var chr = array[i];
    if (chr > 0xFF) {
      chr &= 0xFF;
    }
    ret.push(String.fromCharCode(chr));
  }
  return ret.join('');
}
Module['intArrayToString'] = intArrayToString;
// Write a Javascript array to somewhere in the heap
function writeStringToMemory(string, buffer, dontAddNull) {
  var array = intArrayFromString(string, dontAddNull);
  var i = 0;
  while (i < array.length) {
    var chr = array[i];
    HEAP8[(((buffer)+(i))|0)]=chr;
    i = i + 1;
  }
}
Module['writeStringToMemory'] = writeStringToMemory;
function writeArrayToMemory(array, buffer) {
  for (var i = 0; i < array.length; i++) {
    HEAP8[(((buffer)+(i))|0)]=array[i];
  }
}
Module['writeArrayToMemory'] = writeArrayToMemory;
function writeAsciiToMemory(str, buffer, dontAddNull) {
  for (var i = 0; i < str.length; i++) {
    HEAP8[(((buffer)+(i))|0)]=str.charCodeAt(i);
  }
  if (!dontAddNull) HEAP8[(((buffer)+(str.length))|0)]=0;
}
Module['writeAsciiToMemory'] = writeAsciiToMemory;
function unSign(value, bits, ignore, sig) {
  if (value >= 0) {
    return value;
  }
  return bits <= 32 ? 2*Math.abs(1 << (bits-1)) + value // Need some trickery, since if bits == 32, we are right at the limit of the bits JS uses in bitshifts
                    : Math.pow(2, bits)         + value;
}
function reSign(value, bits, ignore, sig) {
  if (value <= 0) {
    return value;
  }
  var half = bits <= 32 ? Math.abs(1 << (bits-1)) // abs is needed if bits == 32
                        : Math.pow(2, bits-1);
  if (value >= half && (bits <= 32 || value > half)) { // for huge values, we can hit the precision limit and always get true here. so don't do that
                                                       // but, in general there is no perfect solution here. With 64-bit ints, we get rounding and errors
                                                       // TODO: In i64 mode 1, resign the two parts separately and safely
    value = -2*half + value; // Cannot bitshift half, as it may be at the limit of the bits JS uses in bitshifts
  }
  return value;
}
// check for imul support, and also for correctness ( https://bugs.webkit.org/show_bug.cgi?id=126345 )
if (!Math['imul'] || Math['imul'](0xffffffff, 5) !== -5) Math['imul'] = function imul(a, b) {
  var ah  = a >>> 16;
  var al = a & 0xffff;
  var bh  = b >>> 16;
  var bl = b & 0xffff;
  return (al*bl + ((ah*bl + al*bh) << 16))|0;
};
Math.imul = Math['imul'];
var Math_abs = Math.abs;
var Math_cos = Math.cos;
var Math_sin = Math.sin;
var Math_tan = Math.tan;
var Math_acos = Math.acos;
var Math_asin = Math.asin;
var Math_atan = Math.atan;
var Math_atan2 = Math.atan2;
var Math_exp = Math.exp;
var Math_log = Math.log;
var Math_sqrt = Math.sqrt;
var Math_ceil = Math.ceil;
var Math_floor = Math.floor;
var Math_pow = Math.pow;
var Math_imul = Math.imul;
var Math_fround = Math.fround;
var Math_min = Math.min;
// A counter of dependencies for calling run(). If we need to
// do asynchronous work before running, increment this and
// decrement it. Incrementing must happen in a place like
// PRE_RUN_ADDITIONS (used by emcc to add file preloading).
// Note that you can add dependencies in preRun, even though
// it happens right before run - run will be postponed until
// the dependencies are met.
var runDependencies = 0;
var runDependencyWatcher = null;
var dependenciesFulfilled = null; // overridden to take different actions when all run dependencies are fulfilled
function addRunDependency(id) {
  runDependencies++;
  if (Module['monitorRunDependencies']) {
    Module['monitorRunDependencies'](runDependencies);
  }
}
Module['addRunDependency'] = addRunDependency;
function removeRunDependency(id) {
  runDependencies--;
  if (Module['monitorRunDependencies']) {
    Module['monitorRunDependencies'](runDependencies);
  }
  if (runDependencies == 0) {
    if (runDependencyWatcher !== null) {
      clearInterval(runDependencyWatcher);
      runDependencyWatcher = null;
    }
    if (dependenciesFulfilled) {
      var callback = dependenciesFulfilled;
      dependenciesFulfilled = null;
      callback(); // can add another dependenciesFulfilled
    }
  }
}
Module['removeRunDependency'] = removeRunDependency;
Module["preloadedImages"] = {}; // maps url to image data
Module["preloadedAudios"] = {}; // maps url to audio data
var memoryInitializer = null;
// === Body ===
STATIC_BASE = 8;
STATICTOP = STATIC_BASE + 36624;
var _stdout;
var _stdout=_stdout=allocate([0,0,0,0,0,0,0,0], "i8", ALLOC_STATIC);
var _stdin;
var _stdin=_stdin=allocate([0,0,0,0,0,0,0,0], "i8", ALLOC_STATIC);
var _stderr;
var _stderr=_stderr=allocate([0,0,0,0,0,0,0,0], "i8", ALLOC_STATIC);
/* global initializers */ __ATINIT__.push({ func: function() { runPostSets() } },{ func: function() { __GLOBAL__I_a() } },{ func: function() { __GLOBAL__I_a191() } });
var ___fsmu8;
var ___dso_handle;
var ___dso_handle=___dso_handle=allocate([0,0,0,0,0,0,0,0], "i8", ALLOC_STATIC);
var __ZTVN10__cxxabiv120__si_class_type_infoE;
__ZTVN10__cxxabiv120__si_class_type_infoE=allocate([0,0,0,0,168,45,0,0,244,0,0,0,130,0,0,0,64,0,0,0,152,0,0,0,8,0,0,0,10,0,0,0,2,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0], "i8", ALLOC_STATIC);
var __ZTVN10__cxxabiv117__class_type_infoE;
__ZTVN10__cxxabiv117__class_type_infoE=allocate([0,0,0,0,184,45,0,0,244,0,0,0,238,0,0,0,64,0,0,0,152,0,0,0,8,0,0,0,26,0,0,0,4,0,0,0,6,0,0,0,0,0,0,0,0,0,0,0], "i8", ALLOC_STATIC);
var __ZNSt13runtime_errorC1EPKc;
var __ZNSt13runtime_errorD1Ev;
var __ZNSt12length_errorD1Ev;
var __ZNSt3__16localeC1Ev;
var __ZNSt3__16localeC1ERKS0_;
var __ZNSt3__16localeD1Ev;
var __ZNSt8bad_castC1Ev;
var __ZNSt8bad_castD1Ev;
/* memory initializer */ allocate([95,112,137,0,255,9,47,15,10,0,0,0,100,0,0,0,232,3,0,0,16,39,0,0,160,134,1,0,64,66,15,0,128,150,152,0,0,225,245,5,74,117,108,0,0,0,0,0,110,37,115,0,0,0,0,0,117,110,115,117,112,112,111,114,116,101,100,32,108,111,99,97,108,101,32,102,111,114,32,115,116,97,110,100,97,114,100,32,105,110,112,117,116,0,0,0,74,117,110,0,0,0,0,0,112,37,115,0,0,0,0,0,65,112,114,0,0,0,0,0,102,37,115,0,0,0,0,0,77,97,114,0,0,0,0,0,97,37,115,0,0,0,0,0,70,101,98,0,0,0,0,0,122,37,115,0,0,0,0,0,74,97,110,0,0,0,0,0,121,37,115,0,0,0,0,0,68,101,99,101,109,98,101,114,0,0,0,0,0,0,0,0,37,46,42,76,102,0,0,0,78,111,118,101,109,98,101,114,0,0,0,0,0,0,0,0,79,99,116,111,98,101,114,0,83,101,112,116,101,109,98,101,114,0,0,0,0,0,0,0,61,0,0,0,0,0,0,0,87,0,0,0,0,0,0,0,65,117,103,117,115,116,0,0,37,115,58,37,117,32,45,45,32,99,111,110,100,105,116,105,111,110,32,34,37,115,34,32,102,97,105,108,101,100,58,32,37,115,10,0,0,0,0,0,74,117,108,121,0,0,0,0,65,66,67,32,49,50,51,0,74,117,110,101,0,0,0,0,83,101,116,116,105,110,103,115,0,0,0,0,0,0,0,0,77,97,121,0,0,0,0,0,83,99,97,108,105,110,103,0,65,112,114,105,108,0,0,0,67,88,32,77,111,100,101,0,77,97,114,99,104,0,0,0,67,111,110,116,114,111,108,115,0,0,0,0,0,0,0,0,70,101,98,114,117,97,114,121,0,0,0,0,0,0,0,0,84,104,114,101,101,0,0,0,74,97,110,117,97,114,121,0,84,119,111,0,0,0,0,0,98,97,115,105,99,95,115,116,114,105,110,103,0,0,0,0,79,110,101,0,0,0,0,0,68,0,0,0,101,0,0,0,99,0,0,0,0,0,0,0,83,116,117,102,102,105,110,103,0,0,0,0,0,0,0,0,74,0,0,0,0,0,0,0,78,0,0,0,111,0,0,0,118,0,0,0,0,0,0,0,71,114,101,97,116,0,0,0,79,0,0,0,99,0,0,0,116,0,0,0,0,0,0,0,73,115,0,0,0,0,0,0,83,0,0,0,101,0,0,0,112,0,0,0,0,0,0,0,83,101,97,110,0,0,0,0,65,0,0,0,117,0,0,0,103,0,0,0,0,0,0,0,84,101,115,116,105,110,103,0,74,0,0,0,117,0,0,0,108,0,0,0,0,0,0,0,74,0,0,0,117,0,0,0,110,0,0,0,0,0,0,0,69,69,69,73,0,0,0,0,77,0,0,0,97,0,0,0,121,0,0,0,0,0,0,0,66,76,85,69,0,0,0,0,65,0,0,0,112,0,0,0,114,0,0,0,0,0,0,0,77,0,0,0,97,0,0,0,114,0,0,0,0,0,0,0,98,102,95,109,109,97,112,114,32,45,45,32,109,97,112,32,111,102,32,39,37,115,39,32,102,97,105,108,101,100,46,10,0,0,0,0,0,0,0,0,70,0,0,0,101,0,0,0,98,0,0,0,0,0,0,0,98,102,95,109,109,97,112,114,32,45,45,32,102,105,108,101,32,39,37,115,39,32,105,115,32,116,111,111,32,115,109,97,108,108,10,0,0,0,0,0,72,122,47,115,101,99,0,0,74,0,0,0,97,0,0,0,110,0,0,0,0,0,0,0,98,102,95,109,109,97,112,114,32,45,45,32,101,114,114,111,114,32,111,112,101,110,105,110,103,32,102,105,108,101,32,39,37,115,39,10,0,0,0,0,68,0,0,0,101,0,0,0,99,0,0,0,101,0,0,0,109,0,0,0,98,0,0,0,101,0,0,0,114,0,0,0,0,0,0,0,0,0,0,0,32,84,67,95,80,82,69,67,32,61,32,37,108,102,0,0,78,0,0,0,111,0,0,0,118,0,0,0,101,0,0,0,109,0,0,0,98,0,0,0,101,0,0,0,114,0,0,0,0,0,0,0,0,0,0,0,85,110,107,110,111,119,110,32,102,111,114,109,97,116,32,116,121,112,101,32,100,101,115,105,103,110,97,116,111,114,58,32,37,99,10,0,0,0,0,0,79,0,0,0,99,0,0,0,116,0,0,0,111,0,0,0,98,0,0,0,101,0,0,0,114,0,0,0,0,0,0,0,85,110,107,110,111,119,110,32,102,111,114,109,97,116,32,115,105,122,101,32,100,101,115,105,103,110,97,116,111,114,58,32,37,99,10,0,0,0,0,0,83,0,0,0,101,0,0,0,112,0,0,0,116,0,0,0,101,0,0,0,109,0,0,0,98,0,0,0,101,0,0,0,114,0,0,0,0,0,0,0,69,114,114,111,114,32,45,45,32,111,110,108,121,32,115,117,112,112,111,114,116,32,115,99,97,108,97,114,32,97,110,100,32,99,111,109,112,108,101,120,32,100,97,116,97,10,0,0,65,0,0,0,117,0,0,0,103,0,0,0,117,0,0,0,115,0,0,0,116,0,0,0,0,0,0,0,0,0,0,0,69,114,114,111,114,32,45,45,32,116,114,105,101,100,32,116,111,32,97,100,100,32,115,111,117,114,99,101,32,116,111,32,112,108,111,116,32,119,105,116,104,32,105,110,99,111,109,112,97,116,105,98,108,101,32,117,110,105,116,115,46,10,0,0,74,0,0,0,117,0,0,0,108,0,0,0,121,0,0,0,0,0,0,0,0,0,0,0,105,110,102,105,110,105,116,121,0,0,0,0,0,0,0,0,74,0,0,0,117,0,0,0,110,0,0,0,101,0,0,0,0,0,0,0,0,0,0,0,70,105,108,101,32,116,121,112,101,115,32,111,116,104,101,114,32,116,104,97,110,32,50,48,48,48,32,110,111,116,32,121,101,116,32,115,117,112,112,111,114,116,101,100,10,0,0,0,9,69,114,114,111,114,32,119,97,115,0,0,0,0,0,0,65,0,0,0,112,0,0,0,114,0,0,0,105,0,0,0,108,0,0,0,0,0,0,0,69,114,114,111,114,32,114,101,97,100,105,110,103,32,102,105,108,101,32,39,37,115,39,10,0,0,0,0,0,0,0,0,109,47,115,101,99,94,51,0,77,0,0,0,97,0,0,0,114,0,0,0,99,0,0,0,104,0,0,0,0,0,0,0,99,97,102,95,115,109,97,108,108,46,116,109,112,0,0,0,70,0,0,0,101,0,0,0,98,0,0,0,114,0,0,0,117,0,0,0,97,0,0,0,114,0,0,0,121,0,0,0,0,0,0,0,0,0,0,0,101,114,114,111,114,32,105,110,105,116,105,97,108,105,122,105,110,103,0,0,0,0,0,0,74,0,0,0,97,0,0,0,110,0,0,0,117,0,0,0,97,0,0,0,114,0,0,0,121,0,0,0,0,0,0,0,83,68,76,95,73,110,105,116,40,83,68,76,95,73,78,73,84,95,69,86,69,82,89,84,72,73,78,71,41,32,33,61,32,45,49,0,0,0,0,0,115,114,99,47,114,97,115,116,101,114,46,99,99,0,0,0,80,77,0,0,0,0,0,0,45,104,0,0,0,0,0,0,65,77,0,0,0,0,0,0,45,45,104,101,108,112,0,0,45,45,109,97,103,112,104,97,115,101,0,0,0,0,0,0,80,0,0,0,77,0,0,0,0,0,0,0,0,0,0,0,45,45,108,111,103,49,48,0,65,0,0,0,77,0,0,0,0,0,0,0,0,0,0,0,45,45,110,111,115,99,97,108,101,0,0,0,0,0,0,0,45,45,115,116,97,116,117,115,0,0,0,0,0,0,0,0,109,47,115,101,99,94,50,0,45,45,104,101,105,103,104,116,0,0,0,0,0,0,0,0,45,45,119,105,100,116,104,0,45,45,108,101,102,116,0,0,45,45,116,111,112,0,0,0,45,45,112,97,114,101,110,116,0,0,0,0,0,0,0,0,45,45,103,114,97,121,0,0,10,79,112,116,105,111,110,115,58,10,32,32,45,45,104,101,108,112,47,45,104,32,32,112,114,105,110,116,32,116,104,105,115,32,109,101,115,115,97,103,101,32,97,110,100,32,113,117,105,116,10,32,32,45,45,103,114,97,121,32,32,32,32,32,117,115,101,32,103,114,97,121,115,99,97,108,101,32,99,111,108,111,114,109,97,112,32,98,121,32,100,101,102,97,117,108,116,10,32,32,45,45,112,97,114,101,110,116,32,32,32,88,49,49,32,73,68,32,111,102,32,119,105,110,100,111,119,32,116,111,32,112,97,114,101,110,116,32,116,111,32,32,32,32,32,32,32,32,32,32,91,100,101,102,97,117,108,116,58,32,48,93,10,32,32,45,45,116,111,112,32,32,32,32,32,32,115,101,116,32,116,104,101,32,105,110,105,116,105,97,108,32,121,32,112,111,115,105,116,105,111,110,32,111,102,32,116,104,101,32,112,108,111,116,32,91,100,101,102,97,117,108,116,58,32,50,48,48,93,10,32,32,45,45,108,101,102,116,32,32,32,32,32,115,101,116,32,116,104,101,32,105,110,105,116,105,97,108,32,120,32,112,111,115,105,116,105,111,110,32,111,102,32,116,104,101,32,112,108,111,116,32,91,100,101,102,97,117,108,116,58,32,50,48,48,93,10,32,32,45,45,119,105,100,116,104,32,32,32,32,115,101,116,32,116,104,101,32,105,110,105,116,105,97,108,32,119,105,100,116,104,32,111,102,32,116,104,101,32,112,108,111,116,32,32,32,32,32,32,91,100,101,102,97,117,108,116,58,32,56,48,48,93,10,32,32,45,45,104,101,105,103,104,116,32,32,32,115,101,116,32,116,104,101,32,105,110,105,116,105,97,108,32,104,101,105,103,104,116,32,111,102,32,116,104,101,32,112,108,111,116,32,32,32,32,32,91,100,101,102,97,117,108,116,58,32,56,48,48,93,10,32,32,45,45,115,116,97,116,117,115,32,32,32,115,101,116,32,116,104,101,32,104,101,105,103,104,116,32,111,102,32,116,104,101,32,115,116,97,116,117,115,32,100,105,115,112,108,97,121,32,32,32,91,100,101,102,97,117,108,116,58,32,50,48,112,120,93,10,32,32,45,45,110,111,115,99,97,108,101,32,32,100,105,115,97,98,108,101,32,116,104,101,32,115,99,97,108,101,32,119,105,100,103,101,116,10,32,32,45,45,108,111,103,49,48,32,32,32,32,101,110,97,98,108,101,32,100,105,115,112,108,97,121,32,111,102,32,108,111,103,49,48,32,111,102,32,100,97,116,97,10,32,32,45,45,109,97,103,112,104,97,115,101,32,101,110,97,98,108,101,32,109,97,112,112,105,110,103,32,111,102,32,99,111,108,111,114,32,98,121,32,109,97,103,110,105,116,117,100,101,32,97,110,100,32,112,104,97,115,101,10,10,0,0,0,0,0,85,115,97,103,101,58,32,37,115,32,91,111,112,116,105,111,110,115,93,32,91,60,105,110,112,117,116,32,102,105,108,101,115,62,93,10,37,115,0,0,100,101,103,47,115,101,99,94,50,0,0,0,0,0,0,0,109,47,115,101,99,0,0,0,108,111,99,97,108,101,32,110,111,116,32,115,117,112,112,111,114,116,101,100,0,0,0,0,114,97,100,47,115,101,99,94,50,0,0,0,0,0,0,0,100,101,103,47,115,101,99,0,37,0,0,0,73,0,0,0,58,0,0,0,37,0,0,0,77,0,0,0,58,0,0,0,37,0,0,0,83,0,0,0,32,0,0,0,37,0,0,0,112,0,0,0,0,0,0,0,114,97,100,47,115,101,99,0,37,73,58,37,77,58,37,83,32,37,112,0,0,0,0,0,114,112,109,0,0,0,0,0,37,0,0,0,97,0,0,0,32,0,0,0,37,0,0,0,98,0,0,0,32,0,0,0,37,0,0,0,100,0,0,0,32,0,0,0,37,0,0,0,72,0,0,0,58,0,0,0,37,0,0,0,77,0,0,0,58,0,0,0,37,0,0,0,83,0,0,0,32,0,0,0,37,0,0,0,89,0,0,0,0,0,0,0,0,0,0,0,114,112,115,0,0,0,0,0,37,97,32,37,98,32,37,100,32,37,72,58,37,77,58,37,83,32,37,89,0,0,0,0,71,47,115,101,99,0,0,0,115,116,100,58,58,98,97,100,95,97,108,108,111,99,0,0,37,0,0,0,72,0,0,0,58,0,0,0,37,0,0,0,77,0,0,0,58,0,0,0,37,0,0,0,83,0,0,0,0,0,0,0,0,0,0,0,71,0,0,0,0,0,0,0,37,72,58,37,77,58,37,83,0,0,0,0,0,0,0,0,107,110,111,116,115,47,115,101,99,0,0,0,0,0,0,0,37,0,0,0,109,0,0,0,47,0,0,0,37,0,0,0,100,0,0,0,47,0,0,0,37,0,0,0,121,0,0,0,0,0,0,0,0,0,0,0,110,109,105,47,115,101,99,94,50,0,0,0,0,0,0,0,115,116,100,58,58,98,97,100,95,99,97,115,116,0,0,0,37,109,47,37,100,47,37,121,0,0,0,0,0,0,0,0,102,116,47,115,101,99,94,50,0,0,0,0,0,0,0,0,109,0,0,0,0,0,0,0,107,110,111,116,115,61,110,109,105,47,104,114,0,0,0,0,102,0,0,0,97,0,0,0,108,0,0,0,115,0,0,0,101,0,0,0,0,0,0,0,110,109,105,47,115,101,99,0,37,112,0,0,0,0,0,0,102,97,108,115,101,0,0,0,102,116,47,115,101,99,0,0,116,0,0,0,114,0,0,0,117,0,0,0,101,0,0,0,0,0,0,0,0,0,0,0,110,109,105,0,0,0,0,0,116,114,117,101,0,0,0,0,102,116,0,0,0,0,0,0,58,32,0,0,0,0,0,0,115,116,101,114,0,0,0,0,100,66,87,0,0,0,0,0,100,66,109,0,0,0,0,0,105,111,115,95,98,97,115,101,58,58,99,108,101,97,114,0,100,66,0,0,0,0,0,0,99,110,116,0,0,0,0,0,72,122,0,0,0,0,0,0,99,121,99,108,101,115,0,0,100,101,103,0,0,0,0,0,110,97,110,0,0,0,0,0,67,0,0,0,0,0,0,0,114,97,100,0,0,0,0,0,118,101,99,116,111,114,0,0,87,47,77,72,122,0,0,0,37,46,48,76,102,0,0,0,87,47,109,0,0,0,0,0,109,111,110,101,121,95,103,101,116,32,101,114,114,111,114,0,87,47,109,94,50,0,0,0,83,97,116,0,0,0,0,0,70,114,105,0,0,0,0,0,105,111,115,116,114,101,97,109,0,0,0,0,0,0,0,0,37,76,102,0,0,0,0,0,84,104,117,0,0,0,0,0,87,101,100,0,0,0,0,0,84,117,101,0,0,0,0,0,87,47,114,97,100,0,0,0,77,111,110,0,0,0,0,0,83,117,110,0,0,0,0,0,83,97,116,117,114,100,97,121,0,0,0,0,0,0,0,0,70,114,105,100,97,121,0,0,84,104,117,114,115,100,97,121,0,0,0,0,0,0,0,0,87,101,100,110,101,115,100,97,121,0,0,0,0,0,0,0,84,117,101,115,100,97,121,0,77,111,110,100,97,121,0,0,83,117,110,100,97,121,0,0,87,47,115,116,101,114,0,0,83,0,0,0,97,0,0,0,116,0,0,0,0,0,0,0,70,0,0,0,114,0,0,0,105,0,0,0,0,0,0,0,84,0,0,0,104,0,0,0,117,0,0,0,0,0,0,0,32,32,32,100,120,58,32,37,46,57,102,32,37,115,32,32,100,121,58,32,37,46,51,102,32,37,115,0,0,0,0,0,87,0,0,0,101,0,0,0,100,0,0,0,0,0,0,0,120,58,32,37,46,57,102,32,37,115,32,32,121,58,32,37,46,54,102,32,37,115,0,0,84,0,0,0,117,0,0,0,101,0,0,0,0,0,0,0,37,46,54,102,0,0,0,0,77,0,0,0,111,0,0,0,110,0,0,0,0,0,0,0,32,32,121,58,32,37,89,58,37,109,58,37,100,58,58,37,72,58,37,77,58,37,83,0,117,110,115,112,101,99,105,102,105,101,100,32,105,111,115,116,114,101,97,109,95,99,97,116,101,103,111,114,121,32,101,114,114,111,114,0,0,0,0,0,83,0,0,0,117,0,0,0,110,0,0,0,0,0,0,0,120,58,32,37,46,57,102,32,37,115,0,0,0,0,0,0,83,0,0,0,97,0,0,0,116,0,0,0,117,0,0,0,114,0,0,0,100,0,0,0,97,0,0,0,121,0,0,0,0,0,0,0,0,0,0,0,70,0,0,0,114,0,0,0,105,0,0,0,100,0,0,0,97,0,0,0,121,0,0,0,0,0,0,0,0,0,0,0,49,101,37,43,48,51,100,32,37,115,0,0,0,0,0,0,84,0,0,0,104,0,0,0,117,0,0,0,114,0,0,0,115,0,0,0,100,0,0,0,97,0,0,0,121,0,0,0,0,0,0,0,0,0,0,0,89,37,115,0,0,0,0,0,108,0,0,0,0,0,0,0,87,0,0,0,101,0,0,0,100,0,0,0,110,0,0,0,101,0,0,0,115,0,0,0,100,0,0,0,97,0,0,0,121,0,0,0,0,0,0,0,90,37,115,0,0,0,0,0,84,0,0,0,117,0,0,0,101,0,0,0,115,0,0,0,100,0,0,0,97,0,0,0,121,0,0,0,0,0,0,0,69,37,115,0,0,0,0,0,77,0,0,0,111,0,0,0,110,0,0,0,100,0,0,0,97,0,0,0,121,0,0,0,0,0,0,0,0,0,0,0,80,37,115,0,0,0,0,0,83,0,0,0,117,0,0,0,110,0,0,0,100,0,0,0,97,0,0,0,121,0,0,0,0,0,0,0,0,0,0,0,84,37,115,0,0,0,0,0,71,37,115,0,0,0,0,0,68,101,99,0,0,0,0,0,77,37,115,0,0,0,0,0,78,111,118,0,0,0,0,0,107,37,115,0,0,0,0,0,79,99,116,0,0,0,0,0,83,101,112,0,0,0,0,0,109,37,115,0,0,0,0,0,65,117,103,0,0,0,0,0,117,37,115,0,0,0,0,0,103,0,0,0,0,0,0,0,115,101,99,0,0,0,0,0,85,0,0,0,0,0,0,0,48,49,50,51,52,53,54,55,56,57,0,0,0,0,0,0,48,49,50,51,52,53,54,55,56,57,0,0,0,0,0,0,37,0,0,0,89,0,0,0,45,0,0,0,37,0,0,0,109,0,0,0,45,0,0,0,37,0,0,0,100,0,0,0,37,0,0,0,72,0,0,0,58,0,0,0,37,0,0,0,77,0,0,0,58,0,0,0,37,0,0,0,83,0,0,0,37,0,0,0,72,0,0,0,58,0,0,0,37,0,0,0,77,0,0,0,0,0,0,0,37,0,0,0,73,0,0,0,58,0,0,0,37,0,0,0,77,0,0,0,58,0,0,0,37,0,0,0,83,0,0,0,32,0,0,0,37,0,0,0,112,0,0,0,0,0,0,0,37,0,0,0,109,0,0,0,47,0,0,0,37,0,0,0,100,0,0,0,47,0,0,0,37,0,0,0,121,0,0,0,37,0,0,0,72,0,0,0,58,0,0,0,37,0,0,0,77,0,0,0,58,0,0,0,37,0,0,0,83,0,0,0,37,72,58,37,77,58,37,83,37,72,58,37,77,0,0,0,37,73,58,37,77,58,37,83,32,37,112,0,0,0,0,0,37,89,45,37,109,45,37,100,37,109,47,37,100,47,37,121,37,72,58,37,77,58,37,83,37,0,0,0,0,0,0,0,37,112,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,192,39,0,0,34,0,0,0,116,0,0,0,68,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,208,39,0,0,194,0,0,0,162,0,0,0,34,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,224,39,0,0,70,0,0,0,0,1,0,0,36,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,240,39,0,0,94,0,0,0,8,0,0,0,104,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,40,0,0,94,0,0,0,22,0,0,0,104,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,24,40,0,0,166,0,0,0,82,0,0,0,52,0,0,0,2,0,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,56,40,0,0,248,0,0,0,186,0,0,0,52,0,0,0,4,0,0,0,14,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,88,40,0,0,160,0,0,0,188,0,0,0,52,0,0,0,8,0,0,0,12,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,120,40,0,0,250,0,0,0,140,0,0,0,52,0,0,0,6,0,0,0,10,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,24,41,0,0,246,0,0,0,92,0,0,0,52,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,56,41,0,0,158,0,0,0,108,0,0,0,52,0,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,88,41,0,0,42,0,0,0,110,0,0,0,52,0,0,0,118,0,0,0,4,0,0,0,32,0,0,0,6,0,0,0,20,0,0,0,56,0,0,0,2,0,0,0,248,255,255,255,88,41,0,0,20,0,0,0,10,0,0,0,32,0,0,0,14,0,0,0,2,0,0,0,30,0,0,0,122,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,128,41,0,0,236,0,0,0,220,0,0,0,52,0,0,0,18,0,0,0,16,0,0,0,60,0,0,0,26,0,0,0,18,0,0,0,2,0,0,0,4,0,0,0,248,255,255,255,128,41,0,0,62,0,0,0,100,0,0,0,112,0,0,0,120,0,0,0,88,0,0,0,42,0,0,0,52,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,168,41,0,0,76,0,0,0,190,0,0,0,52,0,0,0,44,0,0,0,38,0,0,0,8,0,0,0,42,0,0,0,48,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,184,41,0,0,62,0,0,0,66,0,0,0,52,0,0,0,40,0,0,0,76,0,0,0,12,0,0,0,54,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,200,41,0,0,240,0,0,0,2,0,0,0,52,0,0,0,24,0,0,0,30,0,0,0,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,232,41,0,0,50,0,0,0,206,0,0,0,52,0,0,0,38,0,0,0,14,0,0,0,18,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8,42,0,0,208,0,0,0,112,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,16,42,0,0,32,0,0,0,138,0,0,0,36,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,32,42,0,0,6,0,0,0,172,0,0,0,52,0,0,0,8,0,0,0,6,0,0,0,12,0,0,0,4,0,0,0,10,0,0,0,4,0,0,0,2,0,0,0,12,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,64,42,0,0,98,0,0,0,20,0,0,0,52,0,0,0,20,0,0,0,24,0,0,0,34,0,0,0,22,0,0,0,22,0,0,0,8,0,0,0,6,0,0,0,18,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,96,42,0,0,44,0,0,0,28,0,0,0,52,0,0,0,48,0,0,0,46,0,0,0,38,0,0,0,40,0,0,0,30,0,0,0,44,0,0,0,36,0,0,0,54,0,0,0,52,0,0,0,50,0,0,0,24,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,128,42,0,0,56,0,0,0,4,0,0,0,52,0,0,0,76,0,0,0,70,0,0,0,64,0,0,0,66,0,0,0,58,0,0,0,68,0,0,0,62,0,0,0,28,0,0,0,74,0,0,0,72,0,0,0,42,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,160,42,0,0,72,0,0,0,90,0,0,0,52,0,0,0,6,0,0,0,10,0,0,0,12,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,176,42,0,0,30,0,0,0,174,0,0,0,52,0,0,0,16,0,0,0,14,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,192,42,0,0,12,0,0,0,184,0,0,0,52,0,0,0,2,0,0,0,10,0,0,0,14,0,0,0,116,0,0,0,94,0,0,0,24,0,0,0,108,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,224,42,0,0,178,0,0,0,132,0,0,0,52,0,0,0,14,0,0,0,16,0,0,0,18,0,0,0,48,0,0,0,8,0,0,0,20,0,0,0,84,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,43,0,0,178,0,0,0,24,0,0,0,52,0,0,0,6,0,0,0,4,0,0,0,4,0,0,0,92,0,0,0,58,0,0,0,10,0,0,0,124,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,32,43,0,0,178,0,0,0,100,0,0,0,52,0,0,0,12,0,0,0,8,0,0,0,22,0,0,0,28,0,0,0,66,0,0,0,8,0,0,0,126,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,64,43,0,0,178,0,0,0,38,0,0,0,52,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,80,43,0,0,60,0,0,0,156,0,0,0,52,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,96,43,0,0,178,0,0,0,78,0,0,0,52,0,0,0,20,0,0,0,2,0,0,0,4,0,0,0,10,0,0,0,16,0,0,0,28,0,0,0,24,0,0,0,6,0,0,0,4,0,0,0,8,0,0,0,10,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,128,43,0,0,254,0,0,0,40,0,0,0,52,0,0,0,10,0,0,0,4,0,0,0,18,0,0,0,36,0,0,0,8,0,0,0,6,0,0,0,26,0,0,0,12,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,176,43,0,0,68,0,0,0,216,0,0,0,70,0,0,0,2,0,0,0,14,0,0,0,32,0,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,192,43,0,0,178,0,0,0,84,0,0,0,52,0,0,0,12,0,0,0,8,0,0,0,22,0,0,0,28,0,0,0,66,0,0,0,8,0,0,0,126,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,208,43,0,0,178,0,0,0,232,0,0,0,52,0,0,0,12,0,0,0,8,0,0,0,22,0,0,0,28,0,0,0,66,0,0,0,8,0,0,0,126,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,224,43,0,0,128,0,0,0,228,0,0,0,20,0,0,0,22,0,0,0,16,0,0,0,12,0,0,0,80,0,0,0,96,0,0,0,34,0,0,0,26,0,0,0,24,0,0,0,6,0,0,0,44,0,0,0,22,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,232,43,0,0,10,0,0,0,118,0,0,0,60,0,0,0,40,0,0,0,28,0,0,0,8,0,0,0,46,0,0,0,78,0,0,0,18,0,0,0,6,0,0,0,12,0,0,0,26,0,0,0,16,0,0,0,20,0,0,0,0,0,0,0,0,0,0,0,4,0,0,0,0,0,0,0,24,44,0,0,48,0,0,0,204,0,0,0,252,255,255,255,252,255,255,255,24,44,0,0,146,0,0,0,126,0,0,0,0,0,0,0,0,0,0,0,4,0,0,0,0,0,0,0,48,44,0,0,210,0,0,0,230,0,0,0,252,255,255,255,252,255,255,255,48,44,0,0,106,0,0,0,198,0,0,0,0,0,0,0,0,0,0,0,8,0,0,0,0,0,0,0,72,44,0,0,86,0,0,0,2,1,0,0,248,255,255,255,248,255,255,255,72,44,0,0,180,0,0,0,226,0,0,0,0,0,0,0,0,0,0,0,8,0,0,0,0,0,0,0,96,44,0,0,104,0,0,0,202,0,0,0,248,255,255,255,248,255,255,255,96,44,0,0,136,0,0,0,54,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,120,44,0,0,200,0,0,0,182,0,0,0,36,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,160,44,0,0,242,0,0,0,224,0,0,0,16,0,0,0,22,0,0,0,16,0,0,0,12,0,0,0,54,0,0,0,96,0,0,0,34,0,0,0,26,0,0,0,24,0,0,0,6,0,0,0,30,0,0,0,28,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,176,44,0,0,154,0,0,0,176,0,0,0,38,0,0,0,40,0,0,0,28,0,0,0,8,0,0,0,82,0,0,0,78,0,0,0,18,0,0,0,6,0,0,0,12,0,0,0,26,0,0,0,42,0,0,0,14,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,224,44,0,0,218,0,0,0,144,0,0,0,52,0,0,0,60,0,0,0,114,0,0,0,30,0,0,0,78,0,0,0,4,0,0,0,34,0,0,0,50,0,0,0,24,0,0,0,40,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,45,0,0,102,0,0,0,58,0,0,0,52,0,0,0,106,0,0,0,4,0,0,0,66,0,0,0,74,0,0,0,76,0,0,0,26,0,0,0,110,0,0,0,50,0,0,0,10,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,32,45,0,0,222,0,0,0,114,0,0,0,52,0,0,0,16,0,0,0,56,0,0,0,6,0,0,0,44,0,0,0,80,0,0,0,52,0,0,0,86,0,0,0,56,0,0,0,14,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,64,45,0,0,74,0,0,0,170,0,0,0,52,0,0,0,98,0,0,0,102,0,0,0,32,0,0,0,72,0,0,0,28,0,0,0,22,0,0,0,72,0,0,0,70,0,0,0,68,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,120,45,0,0,88,0,0,0,18,0,0,0,58,0,0,0,22,0,0,0,16,0,0,0,12,0,0,0,80,0,0,0,96,0,0,0,34,0,0,0,64,0,0,0,74,0,0,0,12,0,0,0,44,0,0,0,22,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,136,45,0,0,16,0,0,0,212,0,0,0,62,0,0,0,40,0,0,0,28,0,0,0,8,0,0,0,46,0,0,0,78,0,0,0,18,0,0,0,90,0,0,0,22,0,0,0,2,0,0,0,16,0,0,0,20,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,152,45,0,0,244,0,0,0,196,0,0,0,64,0,0,0,152,0,0,0,8,0,0,0,2,0,0,0,6,0,0,0,4,0,0,0,0,0,0,0,0,0,0,0,83,116,57,116,121,112,101,95,105,110,102,111,0,0,0,0,83,116,57,101,120,99,101,112,116,105,111,110,0,0,0,0,83,116,57,98,97,100,95,97,108,108,111,99,0,0,0,0,83,116,56,98,97,100,95,99,97,115,116,0,0,0,0,0,83,116,49,51,114,117,110,116,105,109,101,95,101,114,114,111,114,0,0,0,0,0,0,0,83,116,49,50,108,101,110,103,116,104,95,101,114,114,111,114,0,0,0,0,0,0,0,0,83,116,49,49,108,111,103,105,99,95,101,114,114,111,114,0,78,83,116,51,95,95,49,57,116,105,109,101,95,98,97,115,101,69,0,0,0,0,0,0,78,83,116,51,95,95,49,57,109,111,110,101,121,95,112,117,116,73,119,78,83,95,49,57,111,115,116,114,101,97,109,98,117,102,95,105,116,101,114,97,116,111,114,73,119,78,83,95,49,49,99,104,97,114,95,116,114,97,105,116,115,73,119,69,69,69,69,69,69,0,0,0,78,83,116,51,95,95,49,57,109,111,110,101,121,95,112,117,116,73,99,78,83,95,49,57,111,115,116,114,101,97,109,98,117,102,95,105,116,101,114,97,116,111,114,73,99,78,83,95,49,49,99,104,97,114,95,116,114,97,105,116,115,73,99,69,69,69,69,69,69,0,0,0,78,83,116,51,95,95,49,57,109,111,110,101,121,95,103,101,116,73,119,78,83,95,49,57,105,115,116,114,101,97,109,98,117,102,95,105,116,101,114,97,116,111,114,73,119,78,83,95,49,49,99,104,97,114,95,116,114,97,105,116,115,73,119,69,69,69,69,69,69,0,0,0,78,83,116,51,95,95,49,57,109,111,110,101,121,95,103,101,116,73,99,78,83,95,49,57,105,115,116,114,101,97,109,98,117,102,95,105,116,101,114,97,116,111,114,73,99,78,83,95,49,49,99,104,97,114,95,116,114,97,105,116,115,73,99,69,69,69,69,69,69,0,0,0,78,83,116,51,95,95,49,57,98,97,115,105,99,95,105,111,115,73,119,78,83,95,49,49,99,104,97,114,95,116,114,97,105,116,115,73,119,69,69,69,69,0,0,0,0,0,0,0,78,83,116,51,95,95,49,57,98,97,115,105,99,95,105,111,115,73,99,78,83,95,49,49,99,104,97,114,95,116,114,97,105,116,115,73,99,69,69,69,69,0,0,0,0,0,0,0,78,83,116,51,95,95,49,57,95,95,110,117,109,95,112,117,116,73,119,69,69,0,0,0,78,83,116,51,95,95,49,57,95,95,110,117,109,95,112,117,116,73,99,69,69,0,0,0,78,83,116,51,95,95,49,57,95,95,110,117,109,95,103,101,116,73,119,69,69,0,0,0,78,83,116,51,95,95,49,57,95,95,110,117,109,95,103,101,116,73,99,69,69,0,0,0,78,83,116,51,95,95,49,56,116,105,109,101,95,112,117,116,73,119,78,83,95,49,57,111,115,116,114,101,97,109,98,117,102,95,105,116,101,114,97,116,111,114,73,119,78,83,95,49,49,99,104,97,114,95,116,114,97,105,116,115,73,119,69,69,69,69,69,69,0,0,0,0,78,83,116,51,95,95,49,56,116,105,109,101,95,112,117,116,73,99,78,83,95,49,57,111,115,116,114,101,97,109,98,117,102,95,105,116,101,114,97,116,111,114,73,99,78,83,95,49,49,99,104,97,114,95,116,114,97,105,116,115,73,99,69,69,69,69,69,69,0,0,0,0,78,83,116,51,95,95,49,56,116,105,109,101,95,103,101,116,73,119,78,83,95,49,57,105,115,116,114,101,97,109,98,117,102,95,105,116,101,114,97,116,111,114,73,119,78,83,95,49,49,99,104,97,114,95,116,114,97,105,116,115,73,119,69,69,69,69,69,69,0,0,0,0,78,83,116,51,95,95,49,56,116,105,109,101,95,103,101,116,73,99,78,83,95,49,57,105,115,116,114,101,97,109,98,117,102,95,105,116,101,114,97,116,111,114,73,99,78,83,95,49,49,99,104,97,114,95,116,114,97,105,116,115,73,99,69,69,69,69,69,69,0,0,0,0,78,83,116,51,95,95,49,56,110,117,109,112,117,110,99,116,73,119,69,69,0,0,0,0,78,83,116,51,95,95,49,56,110,117,109,112,117,110,99,116,73,99,69,69,0,0,0,0,78,83,116,51,95,95,49,56,109,101,115,115,97,103,101,115,73,119,69,69,0,0,0,0,78,83,116,51,95,95,49,56,109,101,115,115,97,103,101,115,73,99,69,69,0,0,0,0,78,83,116,51,95,95,49,56,105,111,115,95,98,97,115,101,69,0,0,0,0,0,0,0,78,83,116,51,95,95,49,56,105,111,115,95,98,97,115,101,55,102,97,105,108,117,114,101,69,0,0,0,0,0,0,0,78,83,116,51,95,95,49,55,110,117,109,95,112,117,116,73,119,78,83,95,49,57,111,115,116,114,101,97,109,98,117,102,95,105,116,101,114,97,116,111,114,73,119,78,83,95,49,49,99,104,97,114,95,116,114,97,105,116,115,73,119,69,69,69,69,69,69,0,0,0,0,0,78,83,116,51,95,95,49,55,110,117,109,95,112,117,116,73,99,78,83,95,49,57,111,115,116,114,101,97,109,98,117,102,95,105,116,101,114,97,116,111,114,73,99,78,83,95,49,49,99,104,97,114,95,116,114,97,105,116,115,73,99,69,69,69,69,69,69,0,0,0,0,0,78,83,116,51,95,95,49,55,110,117,109,95,103,101,116,73,119,78,83,95,49,57,105,115,116,114,101,97,109,98,117,102,95,105,116,101,114,97,116,111,114,73,119,78,83,95,49,49,99,104,97,114,95,116,114,97,105,116,115,73,119,69,69,69,69,69,69,0,0,0,0,0,78,83,116,51,95,95,49,55,110,117,109,95,103,101,116,73,99,78,83,95,49,57,105,115,116,114,101,97,109,98,117,102,95,105,116,101,114,97,116,111,114,73,99,78,83,95,49,49,99,104,97,114,95,116,114,97,105,116,115,73,99,69,69,69,69,69,69,0,0,0,0,0,78,83,116,51,95,95,49,55,99,111,108,108,97,116,101,73,119,69,69,0,0,0,0,0,78,83,116,51,95,95,49,55,99,111,108,108,97,116,101,73,99,69,69,0,0,0,0,0,78,83,116,51,95,95,49,55,99,111,100,101,99,118,116,73,119,99,49,49,95,95,109,98,115,116,97,116,101,95,116,69,69,0,0,0,0,0,0,0,78,83,116,51,95,95,49,55,99,111,100,101,99,118,116,73,99,99,49,49,95,95,109,98,115,116,97,116,101,95,116,69,69,0,0,0,0,0,0,0,78,83,116,51,95,95,49,55,99,111,100,101,99,118,116,73,68,115,99,49,49,95,95,109,98,115,116,97,116,101,95,116,69,69,0,0,0,0,0,0,78,83,116,51,95,95,49,55,99,111,100,101,99,118,116,73,68,105,99,49,49,95,95,109,98,115,116,97,116,101,95,116,69,69,0,0,0,0,0,0,78,83,116,51,95,95,49,54,108,111,99,97,108,101,53,102,97,99,101,116,69,0,0,0,78,83,116,51,95,95,49,54,108,111,99,97,108,101,53,95,95,105,109,112,69,0,0,0,78,83,116,51,95,95,49,53,99,116,121,112,101,73,119,69,69,0,0,0,0,0,0,0,78,83,116,51,95,95,49,53,99,116,121,112,101,73,99,69,69,0,0,0,0,0,0,0,78,83,116,51,95,95,49,50,48,95,95,116,105,109,101,95,103,101,116,95,99,95,115,116,111,114,97,103,101,73,119,69,69,0,0,0,0,0,0,0,78,83,116,51,95,95,49,50,48,95,95,116,105,109,101,95,103,101,116,95,99,95,115,116,111,114,97,103,101,73,99,69,69,0,0,0,0,0,0,0,78,83,116,51,95,95,49,49,57,95,95,105,111,115,116,114,101,97,109,95,99,97,116,101,103,111,114,121,69,0,0,0,78,83,116,51,95,95,49,49,55,95,95,119,105,100,101,110,95,102,114,111,109,95,117,116,102,56,73,76,106,51,50,69,69,69,0,0,0,0,0,0,78,83,116,51,95,95,49,49,54,95,95,110,97,114,114,111,119,95,116,111,95,117,116,102,56,73,76,106,51,50,69,69,69,0,0,0,0,0,0,0,78,83,116,51,95,95,49,49,53,98,97,115,105,99,95,115,116,114,101,97,109,98,117,102,73,119,78,83,95,49,49,99,104,97,114,95,116,114,97,105,116,115,73,119,69,69,69,69,0,0,0,0,0,0,0,0,78,83,116,51,95,95,49,49,53,98,97,115,105,99,95,115,116,114,101,97,109,98,117,102,73,99,78,83,95,49,49,99,104,97,114,95,116,114,97,105,116,115,73,99,69,69,69,69,0,0,0,0,0,0,0,0,78,83,116,51,95,95,49,49,52,101,114,114,111,114,95,99,97,116,101,103,111,114,121,69,0,0,0,0,0,0,0,0,78,83,116,51,95,95,49,49,52,95,95,115,104,97,114,101,100,95,99,111,117,110,116,69,0,0,0,0,0,0,0,0,78,83,116,51,95,95,49,49,52,95,95,110,117,109,95,112,117,116,95,98,97,115,101,69,0,0,0,0,0,0,0,0,78,83,116,51,95,95,49,49,52,95,95,110,117,109,95,103,101,116,95,98,97,115,101,69,0,0,0,0,0,0,0,0,78,83,116,51,95,95,49,49,51,109,101,115,115,97,103,101,115,95,98,97,115,101,69,0,78,83,116,51,95,95,49,49,51,98,97,115,105,99,95,111,115,116,114,101,97,109,73,119,78,83,95,49,49,99,104,97,114,95,116,114,97,105,116,115,73,119,69,69,69,69,0,0,78,83,116,51,95,95,49,49,51,98,97,115,105,99,95,111,115,116,114,101,97,109,73,99,78,83,95,49,49,99,104,97,114,95,116,114,97,105,116,115,73,99,69,69,69,69,0,0,78,83,116,51,95,95,49,49,51,98,97,115,105,99,95,105,115,116,114,101,97,109,73,119,78,83,95,49,49,99,104,97,114,95,116,114,97,105,116,115,73,119,69,69,69,69,0,0,78,83,116,51,95,95,49,49,51,98,97,115,105,99,95,105,115,116,114,101,97,109,73,99,78,83,95,49,49,99,104,97,114,95,116,114,97,105,116,115,73,99,69,69,69,69,0,0,78,83,116,51,95,95,49,49,50,115,121,115,116,101,109,95,101,114,114,111,114,69,0,0,78,83,116,51,95,95,49,49,50,99,111,100,101,99,118,116,95,98,97,115,101,69,0,0,78,83,116,51,95,95,49,49,50,95,95,100,111,95,109,101,115,115,97,103,101,69,0,0,78,83,116,51,95,95,49,49,49,95,95,115,116,100,111,117,116,98,117,102,73,119,69,69,0,0,0,0,0,0,0,0,78,83,116,51,95,95,49,49,49,95,95,115,116,100,111,117,116,98,117,102,73,99,69,69,0,0,0,0,0,0,0,0,78,83,116,51,95,95,49,49,49,95,95,109,111,110,101,121,95,112,117,116,73,119,69,69,0,0,0,0,0,0,0,0,78,83,116,51,95,95,49,49,49,95,95,109,111,110,101,121,95,112,117,116,73,99,69,69,0,0,0,0,0,0,0,0,78,83,116,51,95,95,49,49,49,95,95,109,111,110,101,121,95,103,101,116,73,119,69,69,0,0,0,0,0,0,0,0,78,83,116,51,95,95,49,49,49,95,95,109,111,110,101,121,95,103,101,116,73,99,69,69,0,0,0,0,0,0,0,0,78,83,116,51,95,95,49,49,48,109,111,110,101,121,112,117,110,99,116,73,119,76,98,49,69,69,69,0,0,0,0,0,78,83,116,51,95,95,49,49,48,109,111,110,101,121,112,117,110,99,116,73,119,76,98,48,69,69,69,0,0,0,0,0,78,83,116,51,95,95,49,49,48,109,111,110,101,121,112,117,110,99,116,73,99,76,98,49,69,69,69,0,0,0,0,0,78,83,116,51,95,95,49,49,48,109,111,110,101,121,112,117,110,99,116,73,99,76,98,48,69,69,69,0,0,0,0,0,78,83,116,51,95,95,49,49,48,109,111,110,101,121,95,98,97,115,101,69,0,0,0,0,78,83,116,51,95,95,49,49,48,99,116,121,112,101,95,98,97,115,101,69,0,0,0,0,78,83,116,51,95,95,49,49,48,95,95,116,105,109,101,95,112,117,116,69,0,0,0,0,78,83,116,51,95,95,49,49,48,95,95,115,116,100,105,110,98,117,102,73,119,69,69,0,78,83,116,51,95,95,49,49,48,95,95,115,116,100,105,110,98,117,102,73,99,69,69,0,78,49,48,95,95,99,120,120,97,98,105,118,49,50,49,95,95,118,109,105,95,99,108,97,115,115,95,116,121,112,101,95,105,110,102,111,69,0,0,0,78,49,48,95,95,99,120,120,97,98,105,118,49,50,48,95,95,115,105,95,99,108,97,115,115,95,116,121,112,101,95,105,110,102,111,69,0,0,0,0,78,49,48,95,95,99,120,120,97,98,105,118,49,49,55,95,95,99,108,97,115,115,95,116,121,112,101,95,105,110,102,111,69,0,0,0,0,0,0,0,78,49,48,95,95,99,120,120,97,98,105,118,49,49,54,95,95,115,104,105,109,95,116,121,112,101,95,105,110,102,111,69,0,0,0,0,0,0,0,0,0,0,0,0,8,28,0,0,0,0,0,0,24,28,0,0,0,0,0,0,40,28,0,0,184,39,0,0,0,0,0,0,0,0,0,0,56,28,0,0,184,39,0,0,0,0,0,0,0,0,0,0,72,28,0,0,184,39,0,0,0,0,0,0,0,0,0,0,96,28,0,0,0,40,0,0,0,0,0,0,0,0,0,0,120,28,0,0].concat([184,39,0,0,0,0,0,0,0,0,0,0,136,28,0,0,224,27,0,0,160,28,0,0,0,0,0,0,2,0,0,0,64,43,0,0,2,0,0,0,192,44,0,0,0,0,0,0,224,27,0,0,232,28,0,0,0,0,0,0,2,0,0,0,64,43,0,0,2,0,0,0,200,44,0,0,0,0,0,0,224,27,0,0,48,29,0,0,0,0,0,0,2,0,0,0,64,43,0,0,2,0,0,0,208,44,0,0,0,0,0,0,224,27,0,0,120,29,0,0,0,0,0,0,2,0,0,0,64,43,0,0,2,0,0,0,216,44,0,0,0,0,0,0,0,0,0,0,192,29,0,0,8,42,0,0,0,0,0,0,0,0,0,0,240,29,0,0,8,42,0,0,0,0,0,0,224,27,0,0,32,30,0,0,0,0,0,0,1,0,0,0,0,44,0,0,0,0,0,0,224,27,0,0,56,30,0,0,0,0,0,0,1,0,0,0,0,44,0,0,0,0,0,0,224,27,0,0,80,30,0,0,0,0,0,0,1,0,0,0,8,44,0,0,0,0,0,0,224,27,0,0,104,30,0,0,0,0,0,0,1,0,0,0,8,44,0,0,0,0,0,0,224,27,0,0,128,30,0,0,0,0,0,0,2,0,0,0,64,43,0,0,2,0,0,0,112,45,0,0,0,8,0,0,224,27,0,0,200,30,0,0,0,0,0,0,2,0,0,0,64,43,0,0,2,0,0,0,112,45,0,0,0,8,0,0,224,27,0,0,16,31,0,0,0,0,0,0,3,0,0,0,64,43,0,0,2,0,0,0,16,40,0,0,2,0,0,0,160,43,0,0,0,8,0,0,224,27,0,0,88,31,0,0,0,0,0,0,3,0,0,0,64,43,0,0,2,0,0,0,16,40,0,0,2,0,0,0,168,43,0,0,0,8,0,0,0,0,0,0,160,31,0,0,64,43,0,0,0,0,0,0,0,0,0,0,184,31,0,0,64,43,0,0,0,0,0,0,224,27,0,0,208,31,0,0,0,0,0,0,2,0,0,0,64,43,0,0,2,0,0,0,16,44,0,0,2,0,0,0,224,27,0,0,232,31,0,0,0,0,0,0,2,0,0,0,64,43,0,0,2,0,0,0,16,44,0,0,2,0,0,0,0,0,0,0,0,32,0,0,0,0,0,0,24,32,0,0,120,44,0,0,0,0,0,0,224,27,0,0,56,32,0,0,0,0,0,0,2,0,0,0,64,43,0,0,2,0,0,0,184,40,0,0,0,0,0,0,224,27,0,0,128,32,0,0,0,0,0,0,2,0,0,0,64,43,0,0,2,0,0,0,208,40,0,0,0,0,0,0,224,27,0,0,200,32,0,0,0,0,0,0,2,0,0,0,64,43,0,0,2,0,0,0,232,40,0,0,0,0,0,0,224,27,0,0,16,33,0,0,0,0,0,0,2,0,0,0,64,43,0,0,2,0,0,0,0,41,0,0,0,0,0,0,0,0,0,0,88,33,0,0,64,43,0,0,0,0,0,0,0,0,0,0,112,33,0,0,64,43,0,0,0,0,0,0,224,27,0,0,136,33,0,0,0,0,0,0,2,0,0,0,64,43,0,0,2,0,0,0,136,44,0,0,2,0,0,0,224,27,0,0,176,33,0,0,0,0,0,0,2,0,0,0,64,43,0,0,2,0,0,0,136,44,0,0,2,0,0,0,224,27,0,0,216,33,0,0,0,0,0,0,2,0,0,0,64,43,0,0,2,0,0,0,136,44,0,0,2,0,0,0,224,27,0,0,0,34,0,0,0,0,0,0,2,0,0,0,64,43,0,0,2,0,0,0,136,44,0,0,2,0,0,0,0,0,0,0,40,34,0,0,248,43,0,0,0,0,0,0,0,0,0,0,64,34,0,0,64,43,0,0,0,0,0,0,224,27,0,0,88,34,0,0,0,0,0,0,2,0,0,0,64,43,0,0,2,0,0,0,104,45,0,0,2,0,0,0,224,27,0,0,112,34,0,0,0,0,0,0,2,0,0,0,64,43,0,0,2,0,0,0,104,45,0,0,2,0,0,0,0,0,0,0,136,34,0,0,0,0,0,0,176,34,0,0,0,0,0,0,216,34,0,0,144,44,0,0,0,0,0,0,0,0,0,0,248,34,0,0,32,43,0,0,0,0,0,0,0,0,0,0,32,35,0,0,32,43,0,0,0,0,0,0,0,0,0,0,72,35,0,0,0,0,0,0,128,35,0,0,0,0,0,0,184,35,0,0,0,0,0,0,216,35,0,0,0,0,0,0,248,35,0,0,0,0,0,0,24,36,0,0,0,0,0,0,56,36,0,0,224,27,0,0,80,36,0,0,0,0,0,0,1,0,0,0,152,40,0,0,3,244,255,255,224,27,0,0,128,36,0,0,0,0,0,0,1,0,0,0,168,40,0,0,3,244,255,255,224,27,0,0,176,36,0,0,0,0,0,0,1,0,0,0,152,40,0,0,3,244,255,255,224,27,0,0,224,36,0,0,0,0,0,0,1,0,0,0,168,40,0,0,3,244,255,255,0,0,0,0,16,37,0,0,224,39,0,0,0,0,0,0,0,0,0,0,40,37,0,0,0,0,0,0,64,37,0,0,240,43,0,0,0,0,0,0,0,0,0,0,88,37,0,0,224,43,0,0,0,0,0,0,0,0,0,0,120,37,0,0,232,43,0,0,0,0,0,0,0,0,0,0,152,37,0,0,0,0,0,0,184,37,0,0,0,0,0,0,216,37,0,0,0,0,0,0,248,37,0,0,224,27,0,0,24,38,0,0,0,0,0,0,2,0,0,0,64,43,0,0,2,0,0,0,96,45,0,0,2,0,0,0,224,27,0,0,56,38,0,0,0,0,0,0,2,0,0,0,64,43,0,0,2,0,0,0,96,45,0,0,2,0,0,0,224,27,0,0,88,38,0,0,0,0,0,0,2,0,0,0,64,43,0,0,2,0,0,0,96,45,0,0,2,0,0,0,224,27,0,0,120,38,0,0,0,0,0,0,2,0,0,0,64,43,0,0,2,0,0,0,96,45,0,0,2,0,0,0,0,0,0,0,152,38,0,0,0,0,0,0,176,38,0,0,0,0,0,0,200,38,0,0,0,0,0,0,224,38,0,0,224,43,0,0,0,0,0,0,0,0,0,0,248,38,0,0,232,43,0,0,0,0,0,0,0,0,0,0,16,39,0,0,184,45,0,0,0,0,0,0,0,0,0,0,56,39,0,0,184,45,0,0,0,0,0,0,0,0,0,0,96,39,0,0,200,45,0,0,0,0,0,0,0,0,0,0,136,39,0,0,176,39,0,0,0,0,0,0,48,49,50,51,52,53,54,55,56,57,97,98,99,100,101,102,65,66,67,68,69,70,120,88,43,45,112,80,105,73,110,78,0,0,0,0,0,0,0,0,0,33,181,246,210,94,14,134,180,31,34,233,255,237,255,255,255,255,220,22,16,136,118,1,50,166,210,153,23,0,0,0,68,255,248,216,122,7,0,0,55,204,211,255,255,131,0,0,0,0,0,93,255,221,0,0,0,0,0,41,255,254,0,0,0,0,0,31,255,255,4,0,0,0,0,1,227,255,85,1,0,0,0,0,63,247,255,232,0,0,0,0,99,255,246,200,0,0,0,5,238,255,44,0,0,0,0,34,255,255,2,0,0,0,0,40,255,255,0,0,0,0,0,48,255,247,0,0,0,0,1,130,255,208,0,0,55,220,236,255,255,116,0,0,64,255,254,224,135,3,0,0,2,12,12,40,255,255,40,255,255,40,255,255,40,255,255,40,255,255,40,255,255,40,255,255,40,255,255,40,255,255,40,255,255,40,255,255,40,255,255,40,255,255,40,255,255,40,255,255,40,255,255,1,4,4,0,0,0,0,0,17,143,221,251,255,28,0,0,0,172,255,252,207,204,23,0,0,10,254,255,57,0,0,0,0,0,39,255,255,1,0,0,0,0,0,45,255,248,0,0,0,0,0,4,124,255,192,0,0,0,0,16,255,255,238,44,0,0,0,0,14,222,251,250,68,0,0,0,0,0,0,84,255,202,0,0,0,0,0,0,43,255,250,0,0,0,0,0,0,40,255,255,0,0,0,0,0,0,31,255,255,7,0,0,0,0,0,3,247,255,89,0,0,0,0,0,0,155,255,255,230,220,21,0,0,0,13,156,232,255,255,24,0,0,200,255,255,255,255,255,255,136,0,172,220,220,220,237,255,255,96,0,0,0,0,19,221,255,157,1,0,0,0,5,191,255,198,7,0,0,0,1,151,255,228,25,0,0,0,0,107,255,247,52,0,0,0,0,67,252,255,91,0,0,0,16,20,238,255,253,224,224,224,224,237,40,255,255,255,255,255,255,255,244,0,0,0,0,0,0,0,53,255,255,57,0,0,1,235,255,39,1,213,255,117,0,0,40,255,222,1,0,118,255,207,0,0,114,255,142,0,0,26,253,255,41,0,198,255,50,0,0,0,183,255,132,33,255,214,1,0,0,0,87,255,222,126,255,117,0,0,0,0,9,240,255,241,253,25,0,0,0,0,0,152,255,255,181,0,0,0,0,0,0,57,255,255,85,0,0,0,0,6,0,29,255,238,8,0,0,0,27,180,15,167,255,142,0,0,0,0,148,255,255,255,233,24,0,0,0,0,21,151,207,176,43,0,0,0,0,0,0,0,0,0,0,0,41,247,255,82,0,0,171,255,175,1,0,108,255,230,16,72,255,237,24,0,0,1,184,255,160,220,255,89,0,0,0,0,25,236,255,255,174,1,0,0,0,0,0,138,255,255,71,0,0,0,0,0,36,245,255,255,217,11,0,0,0,4,199,255,140,196,255,152,0,0,0,123,255,226,12,39,248,255,79,0,49,251,255,79,0,0,117,255,237,27,0,0,0,0,0,0,32,255,255,18,0,0,0,40,255,240,1,239,255,33,26,244,116,40,255,207,0,189,255,72,81,255,182,40,255,172,0,140,255,110,136,255,241,43,255,130,0,91,255,148,191,195,255,93,255,89,0,41,255,190,243,86,247,180,255,46,0,3,245,251,255,22,192,251,252,7,0,0,199,255,219,0,132,255,211,0,0,0,149,255,161,0,71,255,161,0,0,0,0,0,0,0,87,255,255,14,0,0,19,255,248,11,8,238,255,78,0,0,83,255,184,0,0,146,255,170,0,0,161,255,99,0,0,48,255,248,15,6,240,246,14,0,0,0,205,255,99,76,255,160,0,0,0,0,106,255,191,164,255,53,0,0,0,0,17,248,255,246,203,0,0,0,0,0,0,165,255,255,96,0,0,0,0,0,0,66,255,237,9,0,0,0,0,0,0,0,0,0,255,255,40,0,0,255,255,40,255,255,40,0,0,255,255,40,255,255,40,0,0,255,255,40,255,255,40,0,0,255,255,40,255,255,40,0,0,255,255,40,251,255,55,0,19,255,255,40,222,255,120,0,132,255,255,40,147,255,253,232,249,255,255,41,19,171,240,225,81,242,255,61,0,8,74,115,50,0,0,0,0,40,255,255,42,0,0,0,0,40,255,255,13,0,0,0,244,255,255,255,255,255,196,0,199,216,255,255,208,208,160,0,0,40,255,255,0,0,0,0,0,40,255,255,0,0,0,0,0,40,255,255,0,0,0,0,0,37,255,255,2,0,0,0,0,16,255,255,46,46,176,20,0,1,207,255,255,255,255,102,0,0,50,208,244,202,99,3,0,10,123,213,244,205,136,14,0,176,255,226,194,239,255,192,23,255,255,28,0,8,149,84,5,227,255,232,150,72,6,0,0,36,177,253,255,255,222,55,0,41,0,20,88,201,255,219,45,246,110,9,0,71,255,228,108,254,255,247,229,255,255,119,0,60,179,223,245,207,98,1,40,255,255,73,209,247,214,118,6,40,255,255,254,255,248,255,229,7,40,255,255,213,40,6,121,116,0,40,255,255,60,0,0,0,3,0,40,255,255,6,0,0,0,0,0,40,255,255,0,0,0,0,0,0,40,255,255,0,0,0,0,0,0,40,255,255,0,0,0,0,0,0,40,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,55,192,242,218,125,255,255,0,56,251,255,234,235,255,255,255,0,196,255,162,3,4,173,255,255,7,252,255,37,0,0,70,255,255,30,255,255,10,0,0,48,255,255,4,249,255,54,0,0,77,255,255,0,183,255,197,18,13,193,255,255,0,46,247,255,252,251,255,255,255,0,0,47,188,242,220,123,255,255,0,0,0,0,0,0,40,255,255,0,0,0,0,0,0,40,255,255,0,0,0,0,0,0,40,255,255,0,0,0,0,255,255,113,218,242,191,56,0,0,255,255,255,239,245,255,252,56,0,255,255,161,2,6,171,255,192,0,255,255,57,0,0,41,255,248,3,255,255,47,0,0,10,255,255,29,255,255,76,0,0,43,255,253,8,255,255,183,5,9,176,255,208,0,255,255,255,240,243,255,255,68,0,255,255,119,209,242,195,65,0,0,255,255,40,0,0,0,0,0,0,255,255,40,0,0,0,0,0,0,255,255,40,0,0,0,0,0,0,0,0,0,0,0,0,24,163,227,233,174,38,0,0,0,29,234,255,233,235,255,247,43,0,0,176,255,164,4,5,168,255,188,0,4,248,255,36,0,0,40,255,249,4,30,255,255,8,0,0,9,255,255,29,5,250,255,42,0,0,35,255,248,4,0,185,255,173,5,2,156,255,187,0,0,39,243,255,237,234,255,248,46,0,0,0,32,169,230,233,178,41,0,0,0,0,0,0,0,0,255,255,73,172,236,229,144,9,255,255,252,252,230,255,255,131,255,255,195,22,0,134,255,217,255,255,64,0,0,50,255,249,255,255,40,0,0,40,255,255,255,255,40,0,0,40,255,255,255,255,40,0,0,40,255,255,255,255,40,0,0,40,255,255,255,255,40,0,0,40,255,255,0,0,0,113,201,62,11,191,90,0,40,255,255,255,255,236,232,255,241,7,40,255,255,68,255,255,118,255,255,31,40,255,255,5,255,255,50,255,255,40,40,255,255,0,255,255,40,255,255,40,40,255,255,0,255,255,40,255,255,40,40,255,255,0,255,255,40,255,255,40,40,255,255,0,255,255,40,255,255,40,40,255,255,0,255,255,40,255,255,40,40,255,255,0,255,255,40,255,255,40,0,0,0,0,140,255,255,255,255,0,0,0,103,188,199,255,255,0,0,0,0,0,40,255,255,0,0,0,0,0,40,255,255,0,0,0,0,0,40,255,255,0,0,0,0,0,40,255,255,0,0,0,0,0,40,255,255,0,0,0,0,0,40,255,255,0,0,0,0,0,40,255,255,0,0,0,0,0,40,255,255,0,0,0,0,0,40,255,255,0,0,0,0,0,40,255,255,0,0,0,127,188,199,255,255,188,188,97,172,255,255,255,255,255,255,132,255,255,40,0,0,0,0,0,0,255,255,40,0,0,0,0,0,0,255,255,40,0,0,0,0,0,0,255,255,40,0,0,0,0,0,0,255,255,40,0,0,0,0,0,0,255,255,40,0,34,225,255,108,0,255,255,40,37,227,253,97,0,0,255,255,80,230,251,87,0,0,0,255,255,246,255,112,0,0,0,0,255,255,255,255,221,18,0,0,0,255,255,91,226,255,174,1,0,0,255,255,40,59,252,255,103,0,0,255,255,40,0,121,255,248,49,0,255,255,40,0,2,186,255,222,18,0,0,0,0,0,0,1,164,132,0,0,0,0,31,255,245,0,0,0,0,4,199,165,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,16,255,255,255,255,255,0,13,208,208,216,255,255,0,0,0,0,40,255,255,0,0,0,0,40,255,255,0,0,0,0,40,255,255,0,0,0,0,40,255,255,0,0,0,0,40,255,255,0,0,0,0,40,255,255,0,0,0,0,43,255,251,0,94,44,0,63,255,219,13,230,192,24,160,255,170,47,241,255,255,255,250,46,0,40,154,206,178,54,0,0,0,0,0,7,208,178,0,0,0,0,25,255,240,0,0,0,0,0,98,80,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,255,255,255,255,0,0,3,188,199,255,255,0,0,0,0,40,255,255,0,0,0,0,40,255,255,0,0,0,0,40,255,255,0,0,0,0,40,255,255,0,0,0,0,40,255,255,0,0,21,188,199,255,255,188,133,28,255,255,255,255,255,180,0,0,0,0,0,0,255,255,40,0,0,0,0,0,255,255,40,0,0,0,0,0,255,255,40,0,0,0,0,0,255,255,40,0,0,0,0,0,255,255,40,0,0,0,0,0,255,255,74,175,238,228,142,8,255,255,252,250,231,255,255,127,255,255,190,17,1,141,255,214,255,255,62,0,0,52,255,249,255,255,40,0,0,40,255,255,255,255,40,0,0,40,255,255,255,255,40,0,0,40,255,255,255,255,40,0,0,40,255,255,255,255,40,0,0,40,255,255,0,0,71,188,237,237,152,185,245,125,0,101,255,252,213,248,255,255,216,130,0,226,255,80,0,38,255,255,40,0,0,226,255,147,18,67,255,251,9,0,0,85,252,255,255,255,255,143,0,0,0,1,214,255,233,195,111,4,0,0,0,22,255,255,33,0,0,0,0,0,0,15,247,255,255,255,232,183,55,0,0,46,213,254,221,221,244,255,239,9,18,244,255,56,0,0,3,216,255,31,5,228,255,231,179,174,217,255,194,1,0,32,151,219,242,245,211,129,15,0,0,0,0,27,154,204,177,94,0,0,0,27,237,255,255,255,255,133,0,0,160,255,184,19,54,221,120,0,0,218,255,73,0,0,49,14,0,0,252,255,45,0,0,0,0,217,220,255,255,226,220,210,0,0,252,255,255,255,255,255,244,0,0,0,0,255,255,40,0,0,0,0,0,0,255,255,40,0,0,0,0,0,0,255,255,40,0,0,0,0,0,0,255,255,40,0,0,0,0,0,0,255,255,40,0,0,0,0,0,0,255,255,40,0,0,0,0,0,0,255,255,40,0,0,0,0,0,0,0,0,36,173,227,239,189,62,0,0,43,246,255,201,200,255,254,58,0,190,255,102,0,0,106,255,189,5,250,255,224,220,220,227,255,237,30,255,255,255,255,255,255,255,255,3,248,255,40,0,0,0,0,0,0,181,255,182,12,0,18,145,41,0,32,239,255,240,214,252,255,163,0,0,25,159,220,243,196,112,2,0,0,0,0,0,0,0,0,0,0,0,0,0,40,255,255,0,0,0,0,0,0,40,255,255,0,0,0,0,0,0,40,255,255,0,0,0,0,0,0,40,255,255,0,0,0,0,0,0,40,255,255,0,0,47,188,242,216,124,255,255,0,45,247,255,238,233,255,255,255,0,187,255,170,6,1,156,255,255,4,249,255,40,0,0,60,255,255,30,255,255,9,0,0,48,255,255,7,253,255,39,0,0,78,255,255,0,204,255,177,10,7,186,255,255,0,64,255,255,245,243,254,255,255,0,0,64,195,243,216,95,255,255,0,0,0,0,17,149,213,247,219,134,12,0,0,23,227,255,255,235,255,255,198,2,0,167,255,203,28,0,36,215,105,0,3,246,255,55,0,0,0,19,0,0,29,255,255,12,0,0,0,0,0,0,3,246,255,61,0,0,0,0,0,0,0,171,255,213,43,1,41,184,60,0,0,26,231,255,255,245,255,255,182,0,0,0,20,155,218,242,195,119,8,0,0,0,0,0,0,0,255,255,40,0,0,0,0,0,0,255,255,40,0,0,0,0,0,0,255,255,40,0,0,0,0,0,0,255,255,40,0,0,0,0,0,0,255,255,40,0,0,0,0,0,0,255,255,112,213,240,190,59,0,0,255,255,255,228,242,255,254,64,0,255,255,146,0,10,182,255,208,0,255,255,54,0,0,49,255,253,9,255,255,46,0,0,12,255,255,28,255,255,74,0,0,49,255,243,1,255,255,175,3,17,189,255,176,0,255,249,255,239,249,255,242,33,0,255,164,101,221,235,173,32,0,0,0,0,0,0,46,173,224,244,210,100,1,0,45,249,254,219,222,255,255,101,0,0,79,34,0,0,84,255,209,0,0,67,176,221,250,255,255,248,0,105,255,255,246,228,226,255,255,3,241,255,106,6,0,57,255,255,26,255,255,33,0,15,189,255,255,1,213,255,238,192,240,255,255,255,0,38,178,236,234,175,81,255,255,0,0,0,0,0,0,0,0,25,0,0,169,245,15,0,169,255,114,0,69,255,242,46,1,210,255,216,0,52,233,186,97,220,220,220,220,220,220,220,220,52,112,255,255,255,255,255,255,255,255,60,0,0,0,0,0,0,0,160,170,0,0,0,0,0,65,255,255,56,0,0,0,8,217,226,242,196,1,0,0,128,255,94,137,255,81,0,40,249,212,3,25,249,217,5,24,144,74,0,0,127,106,6,252,255,255,255,255,255,40,217,220,220,220,255,255,40,0,0,0,0,255,255,40,0,0,0,0,255,255,40,0,0,0,0,255,255,40,0,0,0,0,255,255,40,0,0,0,0,255,255,40,0,0,0,0,255,255,40,0,0,0,0,255,255,40,0,0,0,0,255,255,40,0,0,0,0,255,255,40,0,0,0,0,255,255,40,0,0,0,0,255,255,40,255,255,255,255,255,255,40,220,220,220,220,220,220,35,0,0,0,0,0,0,0,1,52,0,0,0,0,0,0,183,253,34,0,0,0,0,0,140,255,146,0,0,0,0,0,29,252,245,18,0,0,0,0,0,164,255,121,0,0,0,0,0,48,255,231,7,0,0,0,0,0,187,255,97,0,0,0,0,0,70,255,212,1,0,0,0,0,1,210,255,72,0,0,0,0,0,94,255,188,0,0,0,0,0,6,228,255,48,0,0,0,0,0,117,255,163,0,0,0,0,0,15,242,251,29,0,0,0,0,0,140,255,139,0,0,0,0,0,29,187,62,255,255,255,255,255,255,44,255,255,226,220,220,220,38,255,255,40,0,0,0,0,255,255,40,0,0,0,0,255,255,40,0,0,0,0,255,255,40,0,0,0,0,255,255,40,0,0,0,0,255,255,40,0,0,0,0,255,255,40,0,0,0,0,255,255,40,0,0,0,0,255,255,40,0,0,0,0,255,255,40,0,0,0,0,255,255,40,0,0,0,0,255,255,255,255,255,255,44,220,220,220,220,220,220,38,0,0,0,0,0,0,0,0,252,255,255,255,255,255,255,255,8,0,237,240,240,240,240,252,255,229,4,0,0,0,0,0,25,240,255,82,0,0,0,0,0,0,171,255,179,1,0,0,0,0,0,78,255,245,31,0,0,0,0,0,13,227,255,117,0,0,0,0,0,0,147,255,210,5,0,0,0,0,0,56,254,254,57,0,0,0,0,0,6,211,255,151,0,0,0,0,0,0,123,255,232,16,0,0,0,7,16,28,248,255,249,236,236,236,236,247,84,52,255,255,255,255,255,255,255,255,84,130,255,231,9,0,0,8,234,255,103,16,239,255,111,0,0,100,255,229,7,0,121,255,231,9,2,214,255,113,0,0,12,233,255,110,73,255,236,12,0,0,0,112,255,230,193,255,124,0,0,0,0,8,227,255,255,242,17,0,0,0,0,0,102,255,255,135,0,0,0,0,0,0,5,255,255,45,0,0,0,0,0,0,0,255,255,40,0,0,0,0,0,0,0,255,255,40,0,0,0,0,0,0,0,255,255,40,0,0,0,0,0,0,0,255,255,40,0,0,0,70,255,251,35,0,0,64,255,244,22,0,184,255,166,0,1,198,255,132,0,0,46,254,255,50,77,255,239,16,0,0,0,157,255,187,210,255,122,0,0,0,0,27,246,255,255,234,12,0,0,0,0,0,132,255,255,115,0,0,0,0,0,0,163,255,255,156,0,0,0,0,0,47,254,255,255,254,46,0,0,0,0,182,255,167,174,255,186,1,0,0,64,255,252,37,40,252,255,74,0,1,201,255,151,0,0,153,255,213,4,84,255,247,26,0,0,25,245,255,104,240,255,72,0,3,4,0,9,255,255,24,207,255,96,0,161,255,21,27,255,246,1,173,255,120,0,217,255,76,44,255,214,0,140,255,144,17,255,255,132,61,255,182,0,107,255,169,71,255,251,188,79,255,149,0,73,255,193,127,238,180,242,98,255,116,0,40,255,217,182,179,119,255,157,255,84,0,9,255,242,236,119,59,255,230,255,51,0,0,229,255,255,59,8,249,255,255,18,0,0,196,255,248,7,0,197,255,242,1,0,0,163,255,194,0,0,138,255,209,0,0,0,129,255,134,0,0,78,255,176,0,0,0,0,0,0,162,255,171,0,0,0,0,165,255,154,78,255,242,6,0,0,3,236,255,70,8,242,255,69,0,0,55,255,238,5,0,165,255,146,0,0,128,255,159,0,0,81,255,223,1,0,201,255,75,0,0,9,244,255,44,20,254,241,7,0,0,0,168,255,121,90,255,164,0,0,0,0,84,255,198,163,255,80,0,0,0,0,11,246,254,239,244,9,0,0,0,0,0,171,255,255,169,0,0,0,0,0,0,87,255,255,85,0,0,0,0,0,0,12,247,247,12,0,0,0,40,255,255,0,0,0,0,255,255,40,40,255,255,0,0,0,0,255,255,40,40,255,255,0,0,0,0,255,255,40,40,255,255,0,0,0,0,255,255,40,40,255,255,0,0,0,0,255,255,40,40,255,255,0,0,0,0,255,255,40,40,255,255,0,0,0,0,255,255,40,36,255,255,3,0,0,4,255,255,35,9,255,255,27,0,0,33,255,252,5,0,210,255,138,2,4,157,255,202,0,0,77,255,255,241,246,255,254,59,0,0,0,66,192,239,232,186,53,0,0,120,255,255,255,255,255,255,255,255,116,113,240,240,243,255,255,240,240,240,109,0,0,0,40,255,255,0,0,0,0,0,0,0,40,255,255,0,0,0,0,0,0,0,40,255,255,0,0,0,0,0,0,0,40,255,255,0,0,0,0,0,0,0,40,255,255,0,0,0,0,0,0,0,40,255,255,0,0,0,0,0,0,0,40,255,255,0,0,0,0,0,0,0,40,255,255,0,0,0,0,0,0,0,40,255,255,0,0,0,0,0,0,0,40,255,255,0,0,0,0,0,69,192,238,241,193,87,1,81,255,255,229,252,255,255,155,210,255,106,0,13,160,244,77,243,255,74,0,0,11,40,0,169,255,239,109,13,0,0,0,16,179,255,255,243,144,29,0,0,1,69,182,254,255,239,60,0,0,0,0,35,200,255,208,60,11,0,0,0,56,255,245,191,195,49,1,8,149,255,203,161,255,255,247,252,255,254,68,2,102,211,248,224,180,52,0,255,255,255,255,232,195,82,1,0,255,255,253,253,255,255,255,98,0,255,255,40,0,15,151,255,213,0,255,255,40,0,0,51,255,246,0,255,255,40,1,21,153,255,203,0,255,255,255,255,255,255,252,70,0,255,255,226,233,255,239,56,0,0,255,255,40,28,249,255,57,0,0,255,255,40,0,150,255,188,0,0,255,255,40,0,31,250,255,63,0,255,255,40,0,0,154,255,195,1,255,255,40,0,0,33,251,255,70,0,0,0,0,0,0,87,209,246,207,131,10,0,0,0,116,255,255,255,255,255,182,1,0,30,252,253,109,17,75,243,255,71,0,140,255,165,0,0,0,131,255,190,0,196,255,81,0,0,0,49,255,249,7,231,255,55,0,0,0,20,255,255,28,248,255,50,0,0,0,7,255,255,32,223,255,74,0,0,0,23,255,255,7,185,255,130,0,0,0,59,255,231,0,95,255,227,19,0,1,166,255,149,0,10,216,255,216,131,177,255,248,40,0,0,40,220,255,255,255,242,77,0,0,0,0,4,151,255,235,29,0,0,0,0,0,0,54,253,255,255,255,117,0,0,0,0,0,71,176,208,220,95,0,0,0,255,255,255,255,240,201,97,2,0,255,255,253,252,255,255,255,125,0,255,255,40,0,14,137,255,245,3,255,255,40,0,0,14,255,255,29,255,255,40,0,1,101,255,251,6,255,255,226,220,238,255,255,140,0,255,255,255,255,245,207,111,4,0,255,255,40,0,0,0,0,0,0,255,255,40,0,0,0,0,0,0,255,255,40,0,0,0,0,0,0,255,255,40,0,0,0,0,0,0,255,255,40,0,0,0,0,0,0,0,0,0,0,0,0,92,189,241,211,139,13,0,0,0,134,255,255,255,255,255,195,2,0,42,255,251,101,15,70,239,255,87,0,154,255,152,0,0,0,119,255,205,0,204,255,76,0,0,0,42,255,254,12,240,255,50,0,0,0,13,255,255,33,239,255,58,0,0,0,11,255,255,28,206,255,91,0,0,0,31,255,255,6,156,255,177,0,0,0,105,255,204,0,43,255,255,127,30,73,235,255,108,0,0,132,255,255,255,255,255,193,4,0,0,0,101,217,249,225,136,9,0,0,40,255,255,207,1,0,0,255,255,40,40,255,255,255,52,0,0,255,255,40,40,255,255,255,153,0,0,255,255,40,40,255,255,198,243,12,0,255,255,40,40,255,255,96,255,99,0,255,255,40,40,255,255,10,241,200,0,255,255,40,40,255,255,0,148,255,45,255,255,40,40,255,255,0,47,255,146,255,255,40,40,255,255,0,0,201,239,255,255,40,40,255,255,0,0,99,255,255,255,40,40,255,255,0,0,12,242,255,255,40,40,255,255,0,0,0,151,255,255,40,40,255,255,115,0,0,115,255,255,40,40,255,255,193,0,0,192,255,255,40,40,255,255,253,19,17,252,255,255,40,40,255,255,238,93,89,236,255,255,40,40,255,255,163,171,165,159,255,255,40,40,255,255,85,243,238,78,255,255,40,40,255,255,14,250,245,9,255,255,40,40,255,255,0,156,147,0,255,255,40,40,255,255,0,0,0,0,255,255,40,40,255,255,0,0,0,0,255,255,40,40,255,255,0,0,0,0,255,255,40,40,255,255,0,0,0,0,255,255,40,40,255,255,16,0,0,0,0,0,40,255,255,0,0,0,0,0,0,40,255,255,0,0,0,0,0,0,40,255,255,0,0,0,0,0,0,40,255,255,0,0,0,0,0,0,40,255,255,0,0,0,0,0,0,40,255,255,0,0,0,0,0,0,40,255,255,0,0,0,0,0,0,40,255,255,0,0,0,0,0,0,40,255,255,0,0,0,0,0,0,40,255,255,224,224,224,224,224,119,40,255,255,255,255,255,255,255,136,0,0,0,0,40,255,255,0,0,0,102,255,231,32,40,255,255,0,0,75,253,243,49,0,40,255,255,0,52,245,251,69,0,0,40,255,255,34,233,255,92,0,0,0,40,255,255,217,255,118,0,0,0,0,40,255,255,255,254,53,0,0,0,0,40,255,255,222,255,215,9,0,0,0,40,255,255,24,236,255,143,0,0,0,40,255,255,0,88,255,255,64,0,0,40,255,255,0,1,175,255,224,14,0,40,255,255,0,0,24,237,255,157,0,40,255,255,0,0,0,90,255,255,77,0,0,72,255,255,255,255,255,255,0,0,62,220,220,255,255,226,220,0,0,0,0,0,255,255,40,0,0,0,0,0,0,255,255,40,0,0,0,0,0,0,255,255,40,0,0,0,0,0,0,255,255,40,0,0,0,0,0,0,255,255,40,0,0,0,0,0,4,255,255,40,0,0,25,3,0,40,255,255,20,0,17,214,111,1,112,255,236,1,0,76,255,255,247,255,255,117,0,0,0,73,182,239,221,119,3,0,0,0,0,0,0,104,255,255,255,255,255,255,96,90,220,226,255,255,220,220,83,0,0,40,255,255,0,0,0,0,0,40,255,255,0,0,0,0,0,40,255,255,0,0,0,0,0,40,255,255,0,0,0,0,0,40,255,255,0,0,0,0,0,40,255,255,0,0,0,0,0,40,255,255,0,0,0,0,0,40,255,255,0,0,0,115,216,223,255,255,216,216,102,136,255,255,255,255,255,255,120,40,255,255,0,0,0,40,255,255,40,255,255,0,0,0,40,255,255,40,255,255,0,0,0,40,255,255,40,255,255,0,0,0,40,255,255,40,255,255,220,220,220,226,255,255,40,255,255,255,255,255,255,255,255,40,255,255,0,0,0,40,255,255,40,255,255,0,0,0,40,255,255,40,255,255,0,0,0,40,255,255,40,255,255,0,0,0,40,255,255,40,255,255,0,0,0,40,255,255,40,255,255,0,0,0,40,255,255,0,0,0,0,0,0,57,177,240,242,193,77,0,0,0,102,254,255,253,248,255,255,114,0,41,251,255,129,10,5,111,255,223,7,149,255,180,0,0,0,0,109,29,0,212,255,89,0,0,0,0,0,0,0,241,255,55,0,0,0,0,0,0,0,241,255,58,0,68,255,255,255,255,40,204,255,95,0,59,220,220,255,255,40,154,255,191,0,0,0,0,255,255,40,36,251,255,143,17,0,36,255,255,40,0,124,255,255,254,241,255,255,231,25,0,1,80,193,243,241,201,129,12,0,255,255,255,255,255,255,255,140,255,255,236,232,232,232,232,127,255,255,40,0,0,0,0,0,255,255,40,0,0,0,0,0,255,255,226,220,220,220,110,0,255,255,255,255,255,255,128,0,255,255,40,0,0,0,0,0,255,255,40,0,0,0,0,0,255,255,40,0,0,0,0,0,255,255,40,0,0,0,0,0,255,255,40,0,0,0,0,0,255,255,40,0,0,0,0,0,255,255,255,255,255,255,255,252,255,255,240,236,236,236,236,233,255,255,40,0,0,0,0,0,255,255,40,0,0,0,0,0,255,255,40,0,0,0,0,0,255,255,255,255,255,255,252,0,255,255,226,220,220,220,217,0,255,255,40,0,0,0,0,0,255,255,40,0,0,0,0,0,255,255,40,0,0,0,0,0,255,255,240,236,236,236,236,222,255,255,255,255,255,255,255,240,40,255,255,255,248,222,135,13,0,0,40,255,255,221,248,255,255,197,4,0,40,255,255,0,5,89,248,255,87,0,40,255,255,0,0,0,133,255,199,0,40,255,255,0,0,0,47,255,245,1,40,255,255,0,0,0,17,255,255,21,40,255,255,0,0,0,12,255,255,26,40,255,255,0,0,0,41,255,248,2,40,255,255,0,0,0,125,255,207,0,40,255,255,0,0,75,242,255,90,0,40,255,255,198,232,255,255,185,3,0,40,255,255,255,241,198,105,5,0,0,0,0,4,116,215,248,223,135,15,0,0,2,177,255,255,227,254,255,208,15,0,96,255,240,53,0,25,221,255,116,0,197,255,121,0,0,0,62,64,1,4,253,255,40,0,0,0,0,0,0,26,255,255,12,0,0,0,0,0,0,25,255,255,17,0,0,0,0,0,0,1,245,255,54,0,0,0,0,0,0,0,207,255,147,0,0,0,22,18,0,0,111,255,252,90,3,34,206,237,65,0,4,188,255,255,243,255,255,215,15,0,0,8,122,198,242,231,149,16,0,40,255,255,255,255,233,199,99,2,0,40,255,255,220,222,244,255,255,134,0,40,255,255,0,0,3,116,255,234,0,40,255,255,0,0,4,117,255,220,0,40,255,255,220,224,245,255,234,60,0,40,255,255,255,255,255,255,200,20,0,40,255,255,0,5,44,189,255,166,0,40,255,255,0,0,0,35,255,253,14,40,255,255,0,0,0,14,255,255,25,40,255,255,0,0,3,119,255,231,1,40,255,255,232,233,247,255,255,90,0,40,255,255,255,252,223,180,61,0,0,0,0,0,26,254,136,0,0,0,0,0,0,0,111,255,228,3,0,0,0,0,0,0,198,255,255,70,0,0,0,0,0,30,255,218,255,166,0,0,0,0,0,116,255,94,217,247,15,0,0,0,0,203,250,16,129,255,100,0,0,0,34,255,182,0,41,255,196,0,0,0,121,255,255,255,255,255,255,36,0,0,208,255,228,220,220,241,255,130,0,39,255,249,13,0,0,94,255,224,2,126,255,182,0,0,0,13,246,255,65,213,255,103,0,0,0,0,166,255,160,0,0,6,126,210,246,216,150,18,0,0,6,200,255,221,156,168,225,226,15,0,124,255,204,33,131,181,218,255,141,0,221,255,119,242,255,255,255,255,206,10,254,255,212,255,201,59,56,255,243,30,255,255,249,255,60,0,67,255,255,4,251,255,254,255,148,21,170,255,255,0,218,255,186,255,255,255,255,255,255,0,161,255,180,97,193,193,105,220,220,0,46,251,255,102,0,0,1,47,1,0,0,106,255,255,205,173,220,255,125,0,0,0,71,177,231,244,219,162,60,0,0,69,166,207,185,77,0,3,156,255,255,255,255,254,71,95,255,244,102,16,139,255,194,2,149,62,0,0,51,255,242,0,0,0,0,0,84,255,238,0,0,0,0,5,210,255,163,0,0,0,0,135,255,234,28,0,0,0,13,248,255,71,0,0,0,0,40,255,255,5,0,0,0,0,40,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,55,168,95,0,0,0,0,0,234,255,224,0,0,0,0,0,119,240,161,0,0,37,61,0,0,0,0,0,0,0,0,64,255,180,46,0,0,0,0,0,0,27,189,255,254,162,32,0,0,0,0,0,0,67,201,255,249,143,21,0,0,0,0,0,1,79,211,255,242,125,11,0,0,0,0,0,2,150,255,255,76,0,0,0,0,33,170,255,253,147,14,0,0,12,133,248,255,196,47,0,0,2,95,230,255,232,93,1,0,0,0,71,255,252,143,15,0,0,0,0,0,72,192,44,0,0,0,0,0,0,0,10,1,0,0,0,0,0,0,0,0,52,255,255,255,255,255,255,255,255,24,45,220,220,220,220,220,220,220,220,21,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,52,255,255,255,255,255,255,255,255,24,45,220,220,220,220,220,220,220,220,21,0,0,0,0,0,0,0,0,0,0,0,1,76,22,0,0,0,0,0,0,58,195,255,36,0,0,0,0,41,175,255,255,174,14,0,0,28,156,253,255,186,53,0,0,16,136,247,255,198,64,0,0,0,0,96,255,255,123,1,0,0,0,0,0,20,159,255,254,154,23,0,0,0,0,0,0,58,207,255,242,117,7,0,0,0,0,0,3,107,239,255,220,80,1,0,0,0,0,0,21,157,255,255,47,0,0,0,0,0,0,0,56,205,48,0,0,0,0,0,0,0,0,3,8,93,203,102,0,224,255,242,0,123,244,137,0,0,0,0,0,0,0,0,0,0,0,0,0,53,139,52,0,218,255,241,10,131,255,255,23,25,255,193,0,147,237,35,0,137,66,0,0,90,203,103,222,255,242,120,244,137,0,0,0,0,0,0,0,0,0,67,173,81,218,255,240,129,246,144,0,0,0,0,0,0,0,47,185,238,213,137,6,0,0,52,249,255,255,255,255,188,2,0,197,255,173,33,81,240,255,80,11,253,255,32,0,0,115,255,182,23,255,255,17,0,0,52,255,220,1,228,255,132,1,16,164,255,248,0,84,255,255,237,253,255,255,226,0,0,69,196,244,216,157,255,191,0,0,0,0,0,0,144,255,134,0,25,181,44,3,91,252,245,22,5,207,255,255,251,255,252,99,0,0,22,157,222,235,180,67,0,0,0,0,0,0,0,1,87,204,245,218,126,8,0,0,98,255,255,253,255,255,162,0,0,215,255,110,3,84,255,254,15,0,237,255,64,0,38,255,250,14,0,129,255,240,110,203,255,114,0,0,4,180,255,255,255,127,0,0,0,84,254,242,171,255,250,74,0,5,231,255,77,0,105,255,228,2,30,255,255,13,0,14,255,255,27,3,244,255,135,17,117,255,243,3,0,119,255,255,255,255,255,113,0,0,1,100,210,245,208,95,1,0,0,0,0,0,240,255,255,255,255,255,255,104,229,244,244,244,247,255,255,66,0,0,0,0,116,255,218,2,0,0,0,1,217,255,116,0,0,0,0,63,255,249,20,0,0,0,0,165,255,165,0,0,0,0,18,248,255,65,0,0,0,0,105,255,230,3,0,0,0,0,196,255,145,0,0,0,0,32,255,255,58,0,0,0,0,121,255,226,2,0,0,0,0,211,255,140,0,0,0,0,0,0,91,213,247,207,132,9,0,122,255,255,249,255,255,169,32,252,249,80,2,69,159,11,141,255,131,0,0,0,0,0,195,255,152,224,242,194,58,0,228,255,255,251,240,255,253,55,248,255,158,12,5,166,255,188,221,255,51,0,0,56,255,239,184,255,111,0,0,70,255,225,85,255,238,73,39,198,255,163,3,193,255,255,255,255,239,29,0,9,152,239,235,174,33,0,0,246,255,255,255,255,255,255,9,255,255,232,232,232,232,232,26,255,246,0,0,0,0,0,45,255,228,0,0,0,0,0,63,255,249,235,243,200,68,0,81,255,255,245,244,255,255,65,39,172,123,5,7,170,255,195,0,0,0,0,0,59,255,238,0,63,1,0,0,67,255,231,152,255,127,19,30,193,255,175,102,255,255,255,255,255,249,42,0,74,192,242,233,179,43,0,0,0,0,0,19,229,255,0,0,0,0,0,1,171,255,255,0,0,0,0,0,96,255,255,255,0,0,0,0,34,243,242,255,255,0,0,0,4,197,253,97,255,255,0,0,0,125,255,129,40,255,255,0,0,54,252,202,5,40,255,255,0,0,198,255,255,255,255,255,255,255,160,183,220,220,220,226,255,255,220,138,0,0,0,0,40,255,255,0,0,0,0,0,0,40,255,255,0,0,0,0,0,0,40,255,255,0,0,0,0,0,0,0,6,122,200,244,212,111,4,0,0,153,255,255,248,255,255,142,0,0,42,196,52,1,90,255,250,7,0,0,0,0,0,17,255,255,24,0,0,1,10,43,167,255,185,1,0,0,36,255,255,255,170,10,0,0,0,31,221,238,255,250,84,0,0,0,0,0,2,108,255,236,4,0,0,22,0,0,14,255,255,29,0,121,207,34,2,123,255,249,4,17,235,255,255,250,255,255,134,0,0,35,163,219,244,208,106,4,0,0,0,0,0,0,8,133,223,245,204,90,1,0,2,187,255,255,244,255,255,109,0,43,248,227,31,1,111,255,239,2,0,52,97,0,0,14,255,255,27,0,0,0,0,0,78,255,240,2,0,0,0,0,31,227,255,117,0,0,0,0,32,223,255,169,3,0,0,0,29,224,255,163,5,0,0,0,12,212,255,159,2,0,0,0,0,154,255,198,4,0,0,6,7,43,254,255,234,220,220,220,233,40,80,255,255,255,255,255,255,255,40,0,0,0,0,0,3,95,225,255,40,64,214,255,255,255,40,83,228,154,255,255,40,2,1,0,255,255,40,0,0,0,255,255,40,0,0,0,255,255,40,0,0,0,255,255,40,0,0,0,255,255,40,0,0,0,255,255,40,0,0,0,255,255,40,0,0,0,255,255,40,0,0,0,255,255,40,0,0,5,137,222,239,158,15,0,0,0,2,177,255,255,255,255,192,2,0,0,79,255,234,41,30,216,255,80,0,0,192,255,111,0,12,209,255,196,0,1,242,255,38,2,176,249,255,241,1,22,255,255,9,135,255,92,255,255,19,24,255,255,93,255,133,10,255,255,26,1,245,255,249,173,2,37,255,248,2,0,198,255,207,11,0,114,255,212,0,0,84,255,228,47,55,238,255,107,0,0,3,184,255,255,255,255,209,7,0,0,0,7,145,224,228,155,13,0,0,0,0,0,0,0,2,35,0,0,0,0,0,0,74,252,122,0,0,0,0,0,190,255,95,0,0,0,0,51,255,230,6,0,0,0,0,166,255,120,0,0,0,0,30,252,244,17,0,0,0,0,141,255,144,0,0,0,0,15,242,253,32,0,0,0,0,117,255,169,0,0,0,0,5,228,255,54,0,0,0,0,92,255,193,0,0,0,0,1,208,255,77,0,0,0,0,68,255,217,2,0,0,0,0,184,255,102,0,0,0,0,0,84,188,9,0,0,0,0,0,67,172,74,219,255,237,132,246,144,0,0,0,0,0,0,0,192,255,255,255,255,255,255,156,165,220,220,220,220,220,220,135,0,95,194,96,0,0,223,255,252,16,0,105,255,255,19,0,42,255,182,0,12,203,233,28,0,9,172,63,0,0,0,0,0,0,0,0,20,20,4,0,0,0,0,0,0,0,255,255,40,0,0,0,0,0,0,0,255,255,40,0,0,0,0,0,0,0,255,255,40,0,0,0,55,220,220,220,255,255,226,220,220,66,64,255,255,255,255,255,255,255,255,76,0,0,0,0,255,255,40,0,0,0,0,0,0,0,255,255,40,0,0,0,0,0,0,0,255,255,40,0,0,0,0,0,0,0,128,128,20,0,0,0,0,0,0,0,0,0,0,14,128,128,20,0,0,0,0,0,0,10,255,255,15,0,0,0,0,129,56,0,242,236,0,57,99,0,32,255,255,188,239,232,194,255,243,14,10,83,163,239,255,255,230,151,71,5,0,0,2,175,255,255,149,0,0,0,0,0,136,255,165,196,255,102,0,0,0,76,255,232,19,40,248,250,42,0,0,6,155,81,0,0,118,118,1,0,0,0,0,0,0,0,0,44,0,0,0,0,0,9,246,170,27,0,0,0,21,206,255,235,39,0,0,0,6,149,255,225,27,0,0,0,2,177,255,176,0,0,0,0,20,248,255,43,0,0,0,0,158,255,149,0,0,0,0,92,255,195,0,0,0,0,59,255,235,0,0,0,0,56,255,237,0,0,0,0,91,255,196,0,0,0,0,150,255,152,0,0,0,14,243,255,50,0,0,0,146,255,184,0,0,0,96,255,237,40,0,16,158,255,245,56,0,0,47,255,206,53,0,0,0,1,98,4,0,0,0,0,0,0,0,0,0,0,6,39,0,0,0,0,63,214,212,2,0,0,83,254,252,143,24,0,63,249,245,62,0,0,2,214,255,102])
.concat([0,0,0,81,255,219,1,0,0,0,172,255,125,0,0,0,0,208,255,77,0,0,0,0,243,255,52,0,0,0,0,235,255,60,0,0,0,0,199,255,96,0,0,0,0,156,255,161,0,0,0,0,55,255,247,18,0,0,0,0,194,255,154,0,0,0,0,45,244,255,96,0,0,0,0,81,255,255,129,3,0,0,0,75,231,239,18,0,0,0,0,19,70,0,0,0,0,146,235,49,0,233,255,127,0,217,255,89,2,235,255,29,51,255,219,0,63,198,133,0,0,0,13,152,234,232,150,13,0,0,0,0,0,160,255,224,220,255,159,0,0,0,0,0,239,255,61,57,255,240,0,0,0,0,0,195,255,105,83,255,207,0,0,0,0,0,56,249,237,231,251,67,0,0,0,0,0,27,208,255,251,63,0,0,0,0,0,38,233,209,216,255,137,0,156,75,0,0,193,255,59,63,255,255,129,250,239,16,21,255,255,7,0,157,255,255,255,92,0,15,255,255,87,0,100,255,255,248,35,0,0,174,255,254,214,254,255,211,255,209,6,0,14,143,223,247,196,69,14,193,82,0,0,0,0,0,36,190,245,206,60,0,1,190,255,110,204,255,177,255,239,7,91,255,207,4,240,255,80,255,255,41,232,255,56,0,160,255,255,255,209,148,255,154,0,0,12,144,208,168,83,254,236,19,0,0,0,0,0,3,202,255,98,0,0,0,0,0,0,105,255,208,147,204,128,4,0,0,23,239,252,211,255,255,255,127,0,0,161,255,152,254,255,103,255,223,0,63,255,230,40,255,255,53,255,243,6,213,255,86,1,212,255,191,255,181,118,255,187,1,0,38,194,244,184,29,0,0,0,8,8,2,0,0,0,0,0,0,255,255,40,0,0,0,0,68,181,255,255,197,91,1,0,91,255,255,255,255,255,255,166,6,221,255,107,255,255,114,255,135,1,240,255,84,255,255,40,63,0,0,159,255,249,255,255,42,0,0,0,14,169,255,255,255,229,110,2,0,0,0,50,255,255,255,255,141,0,0,0,0,255,255,147,255,253,9,44,45,0,255,255,59,255,255,27,181,220,56,255,255,158,255,228,1,122,255,255,255,255,255,251,83,0,0,62,167,255,255,174,54,0,0,0,0,0,220,220,35,0,0,0,0,0,0,1,243,255,20,227,255,33,0,0,0,17,255,244,5,253,254,6,0,0,0,46,255,212,30,255,231,0,0,73,255,255,255,255,255,255,255,255,109,51,148,195,255,205,190,255,215,148,49,0,0,134,255,111,123,255,135,0,0,0,0,165,255,80,156,255,104,0,0,140,255,255,255,255,255,255,255,255,16,133,220,254,255,223,251,255,226,220,3,0,3,252,253,4,233,255,24,0,0,0,27,255,226,9,255,248,2,0,0,0,55,255,195,39,255,218,0,0,0,0,10,35,23,13,45,24,0,0,0,0,0,0,0,0,0,0,62,237,130,6,210,156,0,137,255,226,36,255,246,0,119,255,202,41,255,221,0,138,255,136,62,255,159,0,209,255,60,119,255,90,6,173,213,3,105,222,21,0,0,0,0,0,0,86,110,0,223,251,7,255,255,38,255,255,40,255,255,40,255,255,40,255,255,40,255,255,40,255,255,40,212,212,34,0,0,0,146,210,86,248,255,240,170,234,112,0,0,0,0,0,0,0,0,0,0,0,0,0,0,10,0,0,0,0,0,0,0,3,0,14,0,14,0,3,0,10,0,0,0,192,82,0,0,7,0,6,0,14,0,1,0,10,0,0,0,144,82,0,0,10,0,13,0,12,0,0,0,10,0,0,0,8,82,0,0,9,0,15,0,14,0,1,0,10,0,0,0,128,81,0,0,10,0,12,0,12,0,0,0,10,0,0,0,8,81,0,0,11,0,12,0,12,0,0,0,10,0,0,0,128,80,0,0,4,0,6,0,14,0,3,0,10,0,0,0,104,80,0,0,7,0,18,0,14,0,2,0,10,0,0,0,232,79,0,0,7,0,18,0,14,0,1,0,10,0,0,0,104,79,0,0,10,0,9,0,11,0,0,0,10,0,0,0,8,79,0,0,10,0,10,0,12,0,0,0,10,0,0,0,160,78,0,0,5,0,6,0,3,0,2,0,10,0,0,0,128,78,0,0,8,0,2,0,7,0,1,0,10,0,0,0,112,78,0,0,3,0,3,0,3,0,3,0,10,0,0,0,96,78,0,0,8,0,15,0,14,0,1,0,10,0,0,0,232,77,0,0,10,0,12,0,12,0,0,0,10,0,0,0,112,77,0,0,6,0,12,0,12,0,1,0,10,0,0,0,40,77,0,0,9,0,12,0,12,0,0,0,10,0,0,0,184,76,0,0,9,0,12,0,12,0,0,0,10,0,0,0,72,76,0,0,9,0,12,0,12,0,1,0,10,0,0,0,216,75,0,0,8,0,12,0,12,0,1,0,10,0,0,0,120,75,0,0,8,0,12,0,12,0,1,0,10,0,0,0,24,75,0,0,8,0,12,0,12,0,1,0,10,0,0,0,184,74,0,0,9,0,12,0,12,0,0,0,10,0,0,0,72,74,0,0,9,0,12,0,12,0,0,0,10,0,0,0,216,73,0,0,3,0,9,0,9,0,3,0,10,0,0,0,184,73,0,0,4,0,12,0,9,0,3,0,10,0,0,0,136,73,0,0,10,0,12,0,12,0,0,0,10,0,0,0,16,73,0,0,10,0,6,0,9,0,0,0,10,0,0,0,208,72,0,0,10,0,12,0,12,0,0,0,10,0,0,0,88,72,0,0,8,0,14,0,14,0,1,0,10,0,0,0,232,71,0,0,10,0,12,0,12,0,0,0,10,0,0,0,112,71,0,0,10,0,12,0,12,0,0,0,10,0,0,0,248,70,0,0,10,0,12,0,12,0,0,0,10,0,0,0,128,70,0,0,10,0,12,0,12,0,0,0,10,0,0,0,8,70,0,0,10,0,12,0,12,0,0,0,10,0,0,0,144,69,0,0,8,0,12,0,12,0,1,0,10,0,0,0,48,69,0,0,8,0,12,0,12,0,1,0,10,0,0,0,208,68,0,0,10,0,12,0,12,0,0,0,10,0,0,0,88,68,0,0,9,0,12,0,12,0,0,0,10,0,0,0,232,67,0,0,8,0,12,0,12,0,1,0,10,0,0,0,136,67,0,0,9,0,12,0,12,0,0,0,10,0,0,0,24,67,0,0,10,0,12,0,12,0,0,0,10,0,0,0,160,66,0,0,9,0,12,0,12,0,0,0,10,0,0,0,48,66,0,0,10,0,12,0,12,0,0,0,10,0,0,0,184,65,0,0,10,0,12,0,12,0,0,0,10,0,0,0,64,65,0,0,10,0,12,0,12,0,0,0,10,0,0,0,200,64,0,0,9,0,12,0,12,0,1,0,10,0,0,0,88,64,0,0,10,0,15,0,12,0,0,0,10,0,0,0,192,63,0,0,9,0,12,0,12,0,1,0,10,0,0,0,80,63,0,0,8,0,12,0,12,0,1,0,10,0,0,0,240,62,0,0,10,0,12,0,12,0,0,0,10,0,0,0,120,62,0,0,10,0,12,0,12,0,0,0,10,0,0,0,0,62,0,0,10,0,12,0,12,0,0,0,10,0,0,0,136,61,0,0,11,0,12,0,12,0,0,0,10,0,0,0,0,61,0,0,10,0,12,0,12,0,0,0,10,0,0,0,136,60,0,0,10,0,12,0,12,0,0,0,10,0,0,0,16,60,0,0,10,0,12,0,12,0,0,0,10,0,0,0,152,59,0,0,7,0,15,0,13,0,2,0,10,0,0,0,40,59,0,0,8,0,15,0,14,0,1,0,10,0,0,0,176,58,0,0,7,0,15,0,13,0,1,0,10,0,0,0,64,58,0,0,8,0,6,0,12,0,1,0,10,0,0,0,16,58,0,0,10,0,2,0,0,0,0,0,10,0,0,0,248,57,0,0,4,0,6,0,15,0,2,0,10,0,0,0,224,57,0,0,9,0,9,0,9,0,0,0,10,0,0,0,136,57,0,0,9,0,14,0,14,0,1,0,10,0,0,0,8,57,0,0,10,0,9,0,9,0,0,0,10,0,0,0,168,56,0,0,9,0,14,0,14,0,0,0,10,0,0,0,40,56,0,0,9,0,9,0,9,0,0,0,10,0,0,0,208,55,0,0,9,0,14,0,14,0,1,0,10,0,0,0,80,55,0,0,10,0,12,0,9,0,0,0,10,0,0,0,216,54,0,0,8,0,14,0,14,0,1,0,10,0,0,0,104,54,0,0,7,0,14,0,14,0,1,0,10,0,0,0,0,54,0,0,7,0,18,0,14,0,0,0,10,0,0,0,128,53,0,0,9,0,14,0,14,0,1,0,10,0,0,0,0,53,0,0,8,0,14,0,14,0,1,0,10,0,0,0,144,52,0,0,10,0,10,0,10,0,0,0,10,0,0,0,40,52,0,0,8,0,9,0,9,0,1,0,10,0,0,0,224,51,0,0,10,0,9,0,9,0,0,0,10,0,0,0,128,51,0,0,9,0,12,0,9,0,1,0,10,0,0,0,16,51,0,0,9,0,12,0,9,0,0,0,10,0,0,0,160,50,0,0,9,0,9,0,9,0,1,0,10,0,0,0,72,50,0,0,8,0,9,0,9,0,1,0,10,0,0,0,0,50,0,0,8,0,12,0,12,0,1,0,10,0,0,0,160,49,0,0,8,0,9,0,9,0,1,0,10,0,0,0,88,49,0,0,10,0,9,0,9,0,0,0,10,0,0,0,248,48,0,0,10,0,9,0,9,0,0,0,10,0,0,0,152,48,0,0,10,0,9,0,9,0,0,0,10,0,0,0,56,48,0,0,10,0,13,0,9,0,0,0,10,0,0,0,176,47,0,0,9,0,9,0,9,0,0,0,10,0,0,0,88,47,0,0,9,0,15,0,12,0,0,0,10,0,0,0,208,46,0,0,3,0,18,0,14,0,3,0,10,0,0,0,152,46,0,0,8,0,15,0,12,0,1,0,10,0,0,0,32,46,0,0,10,0,3,0,9,0,0,0,10,0,0,0,0,46,0,0,0,22,82,20,0,12,3,39,211,144,221,114,195,64,6,28,0,39,130,58,0,0,0,0,150,202,141,6,0,0,0,1,154,107,0,0,0,0,89,155,0,0,0,0,83,156,0,0,0,0,30,206,15,0,0,0,0,134,226,29,0,0,39,204,3,0,0,0,83,164,0,0,0,0,80,169,0,0,0,0,115,148,0,0,104,158,227,36,0,0,41,52,9,0,0,0,1,28,8,224,8,224,8,224,8,224,8,224,8,224,8,224,8,224,8,224,8,224,8,224,1,28,0,0,0,0,0,0,0,0,18,158,200,121,0,0,144,116,0,0,0,0,195,52,0,0,0,0,193,46,0,0,0,27,214,6,0,0,58,234,96,0,0,0,0,12,223,10,0,0,0,0,204,42,0,0,0,0,206,40,0,0,0,0,186,74,0,0,0,0,67,224,152,84,0,0,0,16,53,33,5,100,100,100,100,85,0,6,124,124,124,210,166,0,0,0,0,70,212,14,0,0,0,35,223,40,0,0,0,12,209,80,0,0,0,1,171,131,0,0,0,0,70,252,223,220,220,222,38,0,0,0,0,0,0,0,30,83,0,0,0,73,29,16,234,14,0,3,230,18,0,157,104,0,60,178,0,0,53,205,0,149,81,0,0,1,206,54,214,6,0,0,0,102,213,145,0,0,0,0,13,244,49,0,0,2,0,37,202,1,0,0,120,150,219,71,0,0,0,12,72,33,0,0,0,0,0,0,18,89,1,0,17,84,0,1,180,96,0,160,99,0,0,25,222,106,187,1,0,0,0,95,255,36,0,0,0,1,171,215,125,0,0,0,90,188,15,218,55,0,25,222,32,0,60,213,12,0,0,0,0,0,0,0,76,20,0,0,0,34,55,160,67,1,169,0,107,111,109,115,38,255,38,137,70,58,165,99,166,114,167,28,11,211,154,34,187,182,1,0,210,165,0,199,180,0,0,159,124,0,130,158,0,0,0,0,0,0,0,0,40,70,0,0,0,71,19,34,228,4,0,4,217,6,0,188,77,0,64,159,0,0,88,178,0,154,69,0,0,8,233,43,206,2,0,0,0,145,220,109,0,0,0,0,45,240,14,0,0,0,0,0,0,0,0,0,16,86,0,0,4,96,0,40,220,0,0,8,244,0,40,220,0,0,8,244,0,40,220,0,0,8,244,0,37,234,0,0,28,244,0,7,239,37,0,113,244,0,0,69,192,166,110,246,1,0,0,0,0,0,0,0,0,0,15,36,0,0,0,0,0,120,138,0,0,0,5,100,182,171,100,46,0,6,108,196,166,108,49,0,0,0,159,90,0,0,0,0,0,169,77,0,0,0,0,0,178,76,0,0,0,0,0,158,144,3,68,0,0,0,47,217,227,153,4,0,0,0,46,90,55,1,0,0,119,187,101,165,160,0,0,189,110,1,0,27,0,0,47,197,218,129,21,0,0,0,0,37,145,206,0,11,144,19,0,45,229,1,6,115,210,214,201,58,0,0,0,0,0,0,0,0,65,39,31,86,55,0,164,172,184,100,185,27,164,197,6,0,2,0,164,101,0,0,0,0,164,88,0,0,0,0,164,88,0,0,0,0,162,87,0,0,0,0,0,0,0,0,0,0,0,0,55,87,24,88,5,0,151,141,83,182,242,12,50,197,0,0,33,255,12,97,153,0,0,3,254,12,77,182,0,0,13,255,12,14,232,64,4,134,254,12,0,60,211,215,105,236,12,0,0,0,0,0,236,12,0,0,0,0,0,236,12,0,0,0,0,0,78,4,0,0,19,82,19,81,60,1,0,48,233,147,90,186,149,0,48,239,4,0,11,237,35,47,215,0,0,0,192,77,47,233,0,0,0,207,53,46,255,69,0,81,219,7,46,215,141,219,203,48,0,46,212,0,0,0,0,0,45,212,0,0,0,0,0,15,70,0,0,0,0,0,0,0,0,0,39,87,40,0,0,0,120,195,123,209,111,0,41,221,7,0,20,235,20,100,164,0,0,0,191,65,85,186,0,0,0,205,49,16,229,70,0,75,214,5,0,46,193,217,188,38,0,0,0,0,0,0,0,0,7,94,5,63,66,2,16,245,159,115,191,130,16,255,48,0,36,208,16,244,1,0,17,232,16,240,0,0,16,232,16,240,0,0,16,232,16,237,0,0,16,229,0,0,0,0,0,0,50,51,81,29,47,78,2,128,206,96,230,125,189,86,128,121,2,249,10,128,115,128,104,0,236,0,124,116,128,104,0,236,0,124,116,128,104,0,236,0,124,116,126,103,0,233,0,123,115,0,0,0,0,0,0,0,65,76,71,0,0,112,140,236,0,0,0,16,236,0,0,0,16,236,0,0,0,16,236,0,0,0,16,236,0,0,0,16,236,0,0,0,16,236,0,0,0,16,236,0,0,192,207,249,204,160,0,0,0,0,0,0,8,74,1,0,0,0,0,24,229,0,0,0,0,0,24,228,0,0,0,0,0,24,228,0,0,23,94,3,24,228,0,30,208,71,0,24,228,37,211,62,0,0,24,243,220,195,4,0,0,24,243,31,180,142,0,0,24,228,0,17,221,93,0,24,225,0,0,48,238,52,0,0,0,0,0,6,1,0,0,0,0,0,0,0,15,0,0,0,0,13,253,41,0,0,0,0,41,2,0,29,100,100,100,5,0,33,116,116,248,12,0,0,0,0,240,12,0,0,0,0,240,12,0,0,0,0,240,12,0,0,0,0,240,12,0,0,0,0,242,11,0,15,0,21,242,2,33,219,126,198,115,0,0,45,102,54,0,0,0,0,0,0,16,0,0,0,33,255,25,0,0,1,42,1,0,55,100,97,0,0,62,117,248,0,0,0,8,248,0,0,0,8,248,0,0,0,8,248,0,0,0,8,248,0,0,128,206,251,204,90,0,0,0,0,0,0,4,76,3,0,0,0,12,249,0,0,0,0,12,248,0,0,0,0,12,248,2,57,67,2,12,250,153,147,208,129,12,255,56,0,34,211,12,250,0,0,9,240,12,248,0,0,8,240,12,248,0,0,8,240,12,245,0,0,8,237,0,0,0,0,0,23,139,158,65,124,69,2,210,97,74,235,101,21,30,210,0,0,159,76,0,3,221,92,68,220,27,0,0,129,173,164,54,0,0,0,223,75,54,41,3,0,3,177,156,170,187,202,8,94,134,0,0,0,192,47,51,225,149,126,177,169,3,0,13,63,76,40,0,0,0,0,0,0,0,11,60,32,0,0,0,47,212,142,198,96,0,0,173,80,0,5,31,0,0,199,45,0,0,0,32,200,244,210,200,44,0,0,0,200,44,0,0,0,0,0,200,44,0,0,0,0,0,200,44,0,0,0,0,0,200,44,0,0,0,0,0,197,44,0,0,0,0,0,0,0,41,90,46,0,0,0,118,188,107,192,106,0,26,217,5,0,14,225,2,73,234,188,188,188,236,11,61,190,0,0,0,0,0,10,220,67,0,10,65,0,0,45,189,201,196,84,0,0,0,0,0,0,0,0,0,0,0,0,0,69,7,0,0,0,0,0,232,5,0,0,0,0,0,232,4,0,1,58,80,16,232,4,0,148,173,125,182,242,4,40,206,2,0,50,255,4,86,155,0,0,8,255,4,75,174,0,0,15,255,4,20,229,32,0,108,255,4,0,73,204,181,114,241,9,0,0,0,0,22,80,66,10,0,0,75,216,132,164,207,16,7,229,36,0,0,50,1,45,213,0,0,0,0,0,27,237,5,0,0,0,0,1,186,141,5,10,104,3,0,19,161,225,214,120,3,0,0,0,0,0,0,0,15,69,0,0,0,0,0,48,209,0,0,0,0,0,48,208,0,0,0,0,0,48,208,15,80,52,0,0,48,226,167,130,219,121,0,48,245,12,0,33,244,15,48,216,0,0,0,217,54,48,217,0,0,0,219,38,48,252,32,0,61,213,3,48,178,155,183,194,44,0,0,0,0,1,54,88,43,0,0,123,166,116,190,114,0,3,0,0,24,230,0,23,127,168,178,243,16,217,81,32,21,237,67,201,0,0,85,244,5,159,182,160,137,238,0,0,0,0,0,0,2,43,0,0,3,212,14,0,0,125,166,1,0,19,206,25,0,0,0,0,0,0,0,0,100,188,188,188,188,188,74,20,36,36,36,36,36,15,0,0,0,1,123,1,0,0,87,249,73,0,10,209,66,203,2,123,106,0,159,81,16,3,0,15,5,0,0,0,0,0,0,0,66,76,76,76,5,117,136,136,234,16,0,0,0,208,16,0,0,0,208,16,0,0,0,208,16,0,0,0,208,16,0,0,0,208,16,0,0,0,208,16,0,0,0,208,16,0,0,0,208,16,109,124,124,232,16,70,80,80,80,5,0,0,0,0,0,28,0,0,0,0,7,228,22,0,0,0,0,133,128,0,0,0,0,24,228,10,0,0,0,0,156,104,0,0,0,0,41,217,3,0,0,0,0,179,80,0,0,0,0,63,196,0,0,0,0,1,202,56,0,0,0,0,86,172,0,0,0,0,4,100,0,0,0,0,0,0,16,76,76,76,54,52,217,136,136,96,52,172,0,0,0,52,172,0,0,0,52,172,0,0,0,52,172,0,0,0,52,172,0,0,0,52,172,0,0,0,52,172,0,0,0,52,172,0,0,0,52,213,124,124,90,17,80,80,80,58,0,0,0,0,29,180,180,180,180,180,24,7,44,44,44,98,209,4,0,0,0,3,203,61,0,0,0,0,106,163,0,0,0,0,22,225,25,0,0,0,0,160,115,0,0,0,0,60,213,6,0,0,0,4,209,68,0,0,0,0,72,250,220,220,220,221,81,0,79,116,0,0,0,127,56,18,233,26,0,28,224,6,0,132,144,0,138,118,0,0,19,233,37,226,15,0,0,0,132,224,140,0,0,0,0,19,255,35,0,0,0,0,0,252,16,0,0,0,0,0,252,16,0,0,0,0,0,249,16,0,0,0,37,147,1,0,9,160,6,1,194,73,0,116,136,0,0,58,211,18,224,20,0,0,0,177,208,133,0,0,0,0,67,255,34,0,0,0,0,159,228,134,0,0,0,41,226,32,233,27,0,0,173,102,0,123,154,0,51,215,5,0,10,221,39,0,128,30,0,5,0,31,114,148,77,0,149,0,74,128,107,118,8,247,30,108,88,67,160,66,222,98,142,49,26,201,134,94,167,175,11,1,227,176,4,217,181,0,0,201,162,0,176,185,0,0,161,96,0,110,146,0,0,119,29,0,44,106,0,0,98,83,0,0,0,91,81,66,190,0,0,0,197,42,4,230,23,0,23,210,0,0,150,105,0,100,128,0,0,65,190,0,180,44,0,0,3,229,34,207,1,0,0,0,149,186,130,0,0,0,0,64,255,46,0,0,0,0,3,191,1,0,0,0,57,122,0,0,0,124,40,80,160,0,0,0,176,56,80,160,0,0,0,176,56,80,160,0,0,0,176,56,80,160,0,0,0,176,56,80,161,0,0,0,177,56,71,187,0,0,0,205,45,16,230,57,0,69,221,3,0,51,195,215,187,48,0,0,105,180,180,180,180,180,62,26,44,78,217,44,44,16,0,0,40,208,0,0,0,0,0,40,208,0,0,0,0,0,40,208,0,0,0,0,0,40,208,0,0,0,0,0,40,208,0,0,0,0,0,40,208,0,0,0,0,0,40,205,0,0,0,0,0,11,118,168,131,22,0,0,175,137,43,98,171,0,1,245,16,0,0,5,0,0,160,183,33,0,0,0,0,7,112,228,163,27,0,0,0,0,7,111,221,10,0,2,0,0,0,200,52,27,179,28,0,53,227,14,2,112,210,221,197,57,0,0,43,180,180,178,139,15,0,60,202,52,64,153,192,0,60,188,0,0,2,239,19,60,188,0,0,32,238,7,60,236,180,189,231,93,0,60,198,36,124,155,0,0,60,188,0,10,226,31,0,60,188,0,0,115,154,0,60,186,0,0,11,227,33,0,0,5,115,164,102,5,0,0,174,142,64,175,161,0,58,193,0,0,10,224,27,113,118,0,0,0,153,93,146,92,0,0,0,127,115,132,110,0,0,0,139,108,97,157,0,0,0,177,68,15,231,40,0,44,226,7,0,75,228,187,222,59,0,0,0,13,222,20,0,0,0,0,0,123,218,198,9,0,0,0,0,19,36,2,0,0,0,0,37,180,180,178,145,24,0,52,214,52,61,133,214,3,52,202,0,0,0,210,44,52,201,0,0,19,233,17,52,240,180,186,228,104,0,52,212,36,35,12,0,0,52,204,0,0,0,0,0,52,204,0,0,0,0,0,52,201,0,0,0,0,0,0,0,6,117,164,104,6,0,1,180,142,67,176,168,0,64,191,0,0,10,224,33,116,119,0,0,0,156,97,147,96,0,0,0,133,115,124,118,0,0,0,141,105,83,179,0,0,0,196,53,5,215,91,6,108,210,2,0,36,188,233,175,33,0,0,57,148,1,0,0,144,30,80,255,75,0,0,204,36,80,208,213,4,0,204,36,80,156,168,103,0,204,36,80,156,34,226,13,204,36,80,156,0,143,132,204,36,80,156,0,19,230,228,36,80,156,0,0,117,255,36,79,154,0,0,8,223,36,0,82,86,0,0,0,108,54,116,225,6,0,20,244,76,116,239,104,0,137,234,76,116,138,215,28,197,158,76,116,116,137,218,76,156,76,116,116,20,178,1,156,76,116,116,0,0,0,156,76,116,116,0,0,0,156,76,115,115,0,0,0,154,75,0,17,169,1,0,0,0,0,24,228,0,0,0,0,0,24,228,0,0,0,0,0,24,228,0,0,0,0,0,24,228,0,0,0,0,0,24,228,0,0,0,0,0,24,228,0,0,0,0,0,24,228,0,0,0,0,0,24,248,212,212,212,212,4,0,76,112,0,0,37,164,9,108,152,0,21,212,60,0,108,152,10,199,89,0,0,108,155,176,123,0,0,0,108,245,231,80,0,0,0,108,170,47,229,24,0,0,108,152,0,124,183,1,0,108,152,0,5,204,108,0,107,150,0,0,43,240,41,0,0,0,0,0,6,1,0,0,0,0,119,180,180,180,45,0,0,19,28,242,28,7,0,0,0,0,240,0,0,0,0,0,0,240,0,0,0,0,0,0,240,0,0,0,0,0,0,240,0,0,0,0,0,8,234,0,0,14,96,3,88,181,0,0,15,163,220,191,28,0,0,0,144,180,180,180,79,26,81,200,32,14,0,56,192,0,0,0,56,192,0,0,0,56,192,0,0,0,56,192,0,0,0,56,192,0,0,0,56,192,0,0,186,218,240,204,109,0,0,0,48,137,0,0,0,164,12,68,188,0,0,0,232,8,68,188,0,0,0,232,8,68,188,0,0,0,232,8,68,246,216,216,216,253,8,68,188,0,0,0,236,8,68,188,0,0,0,236,8,68,188,0,0,0,236,8,67,186,0,0,0,233,8,0,0,1,89,165,133,36,0,0,135,176,60,93,220,21,31,209,6,0,0,37,1,80,137,0,0,0,0,0,117,118,0,7,52,52,16,99,135,0,20,156,214,76,56,208,1,0,0,148,76,1,191,115,1,6,176,76,0,21,165,217,207,134,14,0,169,180,180,180,178,240,43,36,36,36,240,8,0,0,0,240,39,32,32,8,240,187,184,184,44,240,8,0,0,0,240,8,0,0,0,240,8,0,0,0,237,8,0,0,0,0,0,0,37,180,180,180,180,180,6,52,196,40,40,40,40,2,52,184,0,0,0,0,0,52,184,0,0,0,0,0,52,250,232,232,232,66,0,52,184,0,0,0,0,0,52,184,0,0,0,0,0,52,184,0,0,0,0,0,52,243,220,220,220,220,4,0,48,180,180,163,78,0,0,68,174,38,70,207,104,0,68,160,0,0,32,233,1,68,160,0,0,0,211,31,68,160,0,0,0,180,70,68,160,0,0,0,203,60,68,160,0,0,8,239,14,68,160,0,3,157,148,0,67,228,190,207,143,8,0,0,0,1,84,163,126,36,0,0,125,171,39,79,225,37,24,224,8,0,0,46,10,72,158,0,0,0,0,0,106,132,0,0,0,0,0,84,153,0,0,0,0,0,42,218,3,0,0,0,0,0,175,131,3,15,161,18,0,16,158,219,199,87,0,0,62,180,180,165,117,6,0,88,169,33,52,162,156,0,88,156,0,0,32,217,0,88,167,29,49,158,132,0,88,227,180,202,226,31,0,88,156,0,0,48,220,2,88,156,0,0,0,211,44,88,156,0,0,61,236,7,87,240,220,228,207,53,0,0,0,0,4,94,0,0,0,0,0,70,213,1,0,0,0,0,157,220,52,0,0,0,6,208,85,148,0,0,0,75,141,8,220,7,0,0,162,128,80,196,83,0,8,224,112,112,145,178,0,80,159,0,0,6,230,23,163,76,0,0,0,142,111,0,0,1,94,164,122,19,0,0,147,123,21,69,182,6,46,170,0,0,16,164,64,106,89,25,184,152,188,93,138,62,128,93,0,128,96,112,90,106,172,63,219,96,62,164,3,102,129,78,45,1,180,103,1,2,26,0,0,12,145,202,210,144,0,0,0,0,14,62,23,0,0,114,226,181,239,79,5,139,14,0,90,213,0,0,0,0,75,226,0,0,0,12,204,110,0,0,0,151,126,0,0,0,0,226,9,0,0,0,0,66,0,0,0,0,8,122,21,0,0,0,29,227,58,0,0,0,0,0,88,110,7,0,0,0,0,19,139,213,92,2,0,0,0,0,26,149,203,74,1,0,0,0,0,48,240,88,0,0,4,105,215,100,2,0,67,208,148,17,0,0,101,196,48,0,0,0,0,25,1,0,0,0,0,0,13,32,32,32,32,32,8,72,184,184,184,184,184,46,0,0,0,0,0,0,0,24,60,60,60,60,60,15,63,160,160,160,160,160,40,0,0,0,0,0,0,0,0,0,13,128,60,0,0,6,110,216,120,9,2,93,209,129,14,0,0,123,223,27,0,0,0,0,6,122,211,82,0,0,0,0,0,29,171,190,46,0,0,0,0,0,67,211,63,0,0,0,0,0,5,20,82,194,4,54,139,1,0,0,0,0,0,0,24,68,0,114,255,23,6,228,5,101,101,0,33,1,0,0,0,0,0,0,0,6,0,104,236,4,31,92,0,0,0,0,0,0,0,45,122,1,92,214,4,0,0,0,0,5,118,160,78,0,0,150,141,55,193,87,2,230,4,0,46,188,5,232,1,0,8,222,0,176,118,35,152,249,0,17,146,179,89,235,0,0,0,0,48,197,0,54,9,14,194,91,0,115,221,225,114,1,0,0,0,5,111,168,117,6,0,0,130,158,46,172,132,0,0,181,61,0,71,173,0,0,87,193,34,180,73,0,0,9,197,232,185,6,0,0,181,89,8,142,157,0,22,227,1,0,3,236,2,8,234,59,3,89,208,0,0,61,203,226,189,39,0,0,169,180,180,180,162,57,60,60,143,166,0,0,0,199,65,0,0,46,219,1,0,0,146,119,0,0,4,231,28,0,0,70,196,0,0,0,160,111,0,0,8,236,28,0,0,0,0,0,0,0,58,162,151,42,0,0,60,210,65,76,97,0,0,185,63,0,0,0,0,2,239,56,138,97,2,0,14,247,176,80,170,134,0,11,243,8,0,13,229,0,1,233,12,0,1,233,1,0,147,133,1,83,179,0,0,10,163,219,186,29,0,0,0,104,180,180,180,144,0,0,162,90,40,40,32,0,0,179,47,0,0,0,0,0,197,142,166,119,8,0,0,162,92,35,153,164,0,0,0,0,0,7,243,6,0,0,0,0,1,240,13,5,170,44,0,100,199,0,0,66,197,219,185,34,0,0,0,0,0,6,163,17,0,0,0,0,126,254,24,0,0,0,44,170,229,24,0,0,4,187,27,228,24,0,0,117,103,0,228,24,0,36,217,70,68,236,86,14,51,148,148,148,245,162,31,0,0,0,0,228,24,0,0,0,0,0,225,24,0,0,0,22,136,166,87,0,0,107,90,66,204,82,0,0,0,0,96,143,0,0,3,37,191,63,0,0,109,223,177,6,0,0,0,1,122,130,0,0,0,0,42,193,1,122,24,3,148,141,1,106,216,222,161,13,0,0,8,113,166,108,4,0,161,119,56,176,136,0,15,0,0,24,221,0,0,0,0,52,195,0,0,0,16,201,67,0,0,13,198,98,0,0,4,190,98,0,0,0,114,148,0,0,0,0,241,222,216,216,221,4,0,0,0,54,167,20,125,180,240,28,1,0,220,28,0,0,220,28,0,0,220,28,0,0,220,28,0,0,220,28,0,0,220,28,0,0,217,28,0,0,0,0,0,0,92,161,74,0,0,0,104,168,57,196,72,0,6,214,11,0,121,196,0,43,178,0,58,195,223,4,72,155,23,206,28,198,30,53,181,187,63,0,208,15,16,248,113,0,11,218,0,0,154,133,3,134,125,0,0,11,167,226,152,6,0,0,0,0,0,0,0,27,0,0,0,0,50,204,0,0,0,0,166,92,0,0,0,31,222,5,0,0,0,143,115,0,0,0,16,228,14,0,0,0,119,138,0,0,0,7,224,28,0,0,0,96,161,0,0,0,1,211,46,0,0,0,7,97,0,0,0,0,0,0,0,0,0,0,45,122,1,92,214,4,0,0,11,220,220,220,220,200,1,16,16,16,16,15,0,0,0,0,24,68,0,114,255,23,6,228,5,101,101,0,33,1,0,0,0,0,0,69,0,0,0,0,0,0,220,0,0,0,0,0,0,220,0,0,0,86,220,220,251,220,220,55,0,0,0,220,0,0,0,0,0,0,220,0,0,0,0,0,0,124,0,0,0,0,0,0,0,0,0,0,0,0,5,64,1,0,0,0,0,6,234,0,0,0,53,144,28,190,39,152,19,16,93,172,240,156,80,9,0,2,167,149,130,0,0,0,130,144,1,186,82,0,0,55,11,0,32,30,0,0,0,0,0,0,0,0,0,23,0,0,0,0,192,133,2,0,0,12,181,133,0,0,0,14,221,50,0,0,0,108,162,0,0,0,34,218,0,0,0,6,247,0,0,0,31,218,0,0,0,98,164,0,0,8,213,53,0,4,161,144,0,1,191,154,4,0,0,48,0,0,0,0,0,0,0,0,0,0,0,0,0,2,27,0,0,25,194,117,0,8,200,91,0,0,106,157,0,0,0,209,46,0,0,4,240,1,0,0,20,229,0,0,0,1,244,5,0,0,0,195,71,0,0,0,93,187,1,0,0,3,189,117,0,0,0,22,198,128,0,0,0,5,42,0,0,0,0,0,0,0,0,37,7,0,200,77,0,204,33,10,206,0,5,32,0,0,0,17,141,159,39,0,0,0,153,138,86,208,0,0,0,163,77,15,211,0,0,0,63,207,167,100,0,0,0,52,236,180,0,1,0,23,223,41,201,77,110,76,99,157,0,33,220,214,20,93,208,15,33,209,204,6,5,160,230,194,55,156,60,0,0,0,0,0,1,0,0,0,26,181,166,8,1,150,19,134,86,145,92,80,158,0,130,100,146,92,205,28,0,20,171,153,107,133,0,0,0,0,11,208,14,0,0,0,0,124,107,73,92,2,0,22,204,66,182,153,105,0,147,81,104,112,73,142,34,188,1,23,178,174,41,0,0,0,0,20,3,0,0,0,0,19,217,40,0,0,0,86,207,235,187,164,1,0,218,23,204,4,55,0,0,177,133,210,4,0,0,0,20,159,250,148,26,0,0,0,0,204,103,224,17,0,4,0,204,4,173,63,10,193,61,207,78,223,16,0,56,162,245,164,42,0,0,0,0,134,3,0,0,0,0,0,0,0,121,29,60,89,0,0,0,198,15,113,102,0,41,94,229,100,183,147,50,38,83,219,71,200,92,30,0,31,177,0,204,11,0,49,119,187,90,229,99,26,56,147,158,83,216,67,14,0,120,87,37,174,0,0,0,121,48,53,120,0,0,0,0,44,0,34,10,23,251,6,187,93,24,212,0,184,50,78,140,4,213,3,17,20,3,35,0,0,0,0,0,0,0,0,6,44,0,87,243,0,84,242,0,64,223,0,51,205,0,38,188,0,25,171,0,4,28,0,44,127,1,88,215,6,0,0,0,0,2,4,6,8,11,14,17,21,24,28,32,36,40,45,49,54,58,63,68,73,78,83,88,93,98,104,109,114,120,125,130,136,141,146,151,157,162,167,172,177,182,187,192,197,201,206,210,215,219,223,227,231,234,238,241,244,247,249,251,253,255,255,0,0,0,0,0,0,0,0,7,0,0,0,0,0,0,0,3,0,10,0,10,0,2,0,7,0,0,0,200,109,0,0,5,0,5,0,10,0,1,0,7,0,0,0,168,109,0,0,7,0,9,0,9,0,0,0,7,0,0,0,104,109,0,0,7,0,11,0,10,0,0,0,7,0,0,0,24,109,0,0,7,0,9,0,9,0,0,0,7,0,0,0,216,108,0,0,7,0,10,0,9,0,0,0,7,0,0,0,144,108,0,0,3,0,5,0,10,0,2,0,7,0,0,0,128,108,0,0,5,0,13,0,10,0,1,0,7,0,0,0,56,108,0,0,5,0,13,0,10,0,0,0,7,0,0,0,240,107,0,0,7,0,7,0,8,0,0,0,7,0,0,0,184,107,0,0,7,0,7,0,8,0,0,0,7,0,0,0,128,107,0,0,3,0,5,0,2,0,2,0,7,0,0,0,112,107,0,0,6,0,2,0,5,0,0,0,7,0,0,0,96,107,0,0,3,0,2,0,2,0,2,0,7,0,0,0,88,107,0,0,6,0,11,0,10,0,0,0,7,0,0,0,16,107,0,0,7,0,9,0,9,0,0,0,7,0,0,0,208,106,0,0,4,0,9,0,9,0,1,0,7,0,0,0,168,106,0,0,6,0,9,0,9,0,1,0,7,0,0,0,112,106,0,0,6,0,9,0,9,0,0,0,7,0,0,0,56,106,0,0,7,0,9,0,9,0,0,0,7,0,0,0,248,105,0,0,7,0,9,0,9,0,0,0,7,0,0,0,184,105,0,0,7,0,9,0,9,0,0,0,7,0,0,0,120,105,0,0,5,0,9,0,9,0,1,0,7,0,0,0,72,105,0,0,7,0,9,0,9,0,0,0,7,0,0,0,8,105,0,0,6,0,9,0,9,0,0,0,7,0,0,0,208,104,0,0,3,0,7,0,7,0,2,0,7,0,0,0,184,104,0,0,3,0,9,0,6,0,2,0,7,0,0,0,152,104,0,0,7,0,8,0,8,0,0,0,7,0,0,0,96,104,0,0,7,0,5,0,7,0,0,0,7,0,0,0,56,104,0,0,7,0,8,0,8,0,0,0,7,0,0,0,0,104,0,0,6,0,10,0,10,0,0,0,7,0,0,0,192,103,0,0,7,0,9,0,9,0,0,0,7,0,0,0,128,103,0,0,7,0,9,0,9,0,0,0,7,0,0,0,64,103,0,0,7,0,9,0,9,0,0,0,7,0,0,0,0,103,0,0,7,0,9,0,9,0,0,0,7,0,0,0,192,102,0,0,7,0,9,0,9,0,0,0,7,0,0,0,128,102,0,0,7,0,9,0,9,0,0,0,7,0,0,0,64,102,0,0,5,0,9,0,9,0,1,0,7,0,0,0,16,102,0,0,7,0,9,0,9,0,0,0,7,0,0,0,208,101,0,0,7,0,9,0,9,0,0,0,7,0,0,0,144,101,0,0,5,0,9,0,9,0,1,0,7,0,0,0,96,101,0,0,7,0,9,0,9,0,0,0,7,0,0,0,32,101,0,0,7,0,10,0,9,0,0,0,7,0,0,0,216,100,0,0,7,0,9,0,9,0,0,0,7,0,0,0,152,100,0,0,7,0,9,0,9,0,0,0,7,0,0,0,88,100,0,0,7,0,9,0,9,0,0,0,7,0,0,0,24,100,0,0,7,0,9,0,9,0,0,0,7,0,0,0,216,99,0,0,7,0,9,0,9,0,0,0,7,0,0,0,152,99,0,0,7,0,12,0,9,0,0,0,7,0,0,0,64,99,0,0,7,0,9,0,9,0,0,0,7,0,0,0,0,99,0,0,7,0,9,0,9,0,0,0,7,0,0,0,192,98,0,0,7,0,9,0,9,0,0,0,7,0,0,0,128,98,0,0,7,0,9,0,9,0,0,0,7,0,0,0,64,98,0,0,7,0,9,0,9,0,0,0,7,0,0,0,0,98,0,0,7,0,9,0,9,0,0,0,7,0,0,0,192,97,0,0,7,0,9,0,9,0,0,0,7,0,0,0,128,97,0,0,7,0,9,0,9,0,0,0,7,0,0,0,64,97,0,0,7,0,9,0,9,0,0,0,7,0,0,0,0,97,0,0,5,0,12,0,10,0,1,0,7,0,0,0,192,96,0,0,6,0,11,0,10,0,0,0,7,0,0,0,120,96,0,0,5,0,12,0,10,0,1,0,7,0,0,0,56,96,0,0,5,0,5,0,9,0,1,0,7,0,0,0,24,96,0,0,7,0,2,0,0,0,0,0,7,0,0,0,8,96,0,0,4,0,5,0,10,0,1,0,7,0,0,0,240,95,0,0,6,0,7,0,7,0,0,0,7,0,0,0,192,95,0,0,7,0,10,0,10,0,0,0,7,0,0,0,120,95,0,0,7,0,7,0,7,0,0,0,7,0,0,0,64,95,0,0,7,0,10,0,10,0,0,0,7,0,0,0,248,94,0,0,7,0,7,0,7,0,0,0,7,0,0,0,192,94,0,0,7,0,10,0,10,0,0,0,7,0,0,0,120,94,0,0,7,0,10,0,7,0,0,0,7,0,0,0,48,94,0,0,6,0,10,0,10,0,0,0,7,0,0,0,240,93,0,0,5,0,10,0,10,0,1,0,7,0,0,0,184,93,0,0,6,0,13,0,10,0,0,0,7,0,0,0,104,93,0,0,7,0,11,0,10,0,0,0,7,0,0,0,24,93,0,0,5,0,10,0,10,0,1,0,7,0,0,0,224,92,0,0,7,0,7,0,7,0,0,0,7,0,0,0,168,92,0,0,6,0,7,0,7,0,0,0,7,0,0,0,120,92,0,0,7,0,7,0,7,0,0,0,7,0,0,0,64,92,0,0,7,0,10,0,7,0,0,0,7,0,0,0,248,91,0,0,7,0,10,0,7,0,0,0,7,0,0,0,176,91,0,0,6,0,7,0,7,0,1,0,7,0,0,0,128,91,0,0,7,0,7,0,7,0,0,0,7,0,0,0,72,91,0,0,7,0,9,0,9,0,0,0,7,0,0,0,8,91,0,0,7,0,7,0,7,0,0,0,7,0,0,0,208,90,0,0,7,0,7,0,7,0,0,0,7,0,0,0,152,90,0,0,7,0,7,0,7,0,0,0,7,0,0,0,96,90,0,0,7,0,7,0,7,0,0,0,7,0,0,0,40,90,0,0,7,0,10,0,7,0,0,0,7,0,0,0,224,89,0,0,7,0,7,0,7,0,0,0,7,0,0,0,168,89,0,0,6,0,12,0,9,0,0,0,7,0,0,0,96,89,0,0,2,0,13,0,10,0,2,0,7,0,0,0,64,89,0,0,6,0,12,0,9,0,1,0,7,0,0,0,248,88,0,0,7,0,3,0,7,0,0,0,7,0,0,0,224,88,0,0])
, "i8", ALLOC_NONE, Runtime.GLOBAL_BASE)
var tempDoublePtr = Runtime.alignMemory(allocate(12, "i8", ALLOC_STATIC), 8);
assert(tempDoublePtr % 8 == 0);
function copyTempFloat(ptr) { // functions, because inlining this code increases code size too much
  HEAP8[tempDoublePtr] = HEAP8[ptr];
  HEAP8[tempDoublePtr+1] = HEAP8[ptr+1];
  HEAP8[tempDoublePtr+2] = HEAP8[ptr+2];
  HEAP8[tempDoublePtr+3] = HEAP8[ptr+3];
}
function copyTempDouble(ptr) {
  HEAP8[tempDoublePtr] = HEAP8[ptr];
  HEAP8[tempDoublePtr+1] = HEAP8[ptr+1];
  HEAP8[tempDoublePtr+2] = HEAP8[ptr+2];
  HEAP8[tempDoublePtr+3] = HEAP8[ptr+3];
  HEAP8[tempDoublePtr+4] = HEAP8[ptr+4];
  HEAP8[tempDoublePtr+5] = HEAP8[ptr+5];
  HEAP8[tempDoublePtr+6] = HEAP8[ptr+6];
  HEAP8[tempDoublePtr+7] = HEAP8[ptr+7];
}
  var ERRNO_CODES={EPERM:1,ENOENT:2,ESRCH:3,EINTR:4,EIO:5,ENXIO:6,E2BIG:7,ENOEXEC:8,EBADF:9,ECHILD:10,EAGAIN:11,EWOULDBLOCK:11,ENOMEM:12,EACCES:13,EFAULT:14,ENOTBLK:15,EBUSY:16,EEXIST:17,EXDEV:18,ENODEV:19,ENOTDIR:20,EISDIR:21,EINVAL:22,ENFILE:23,EMFILE:24,ENOTTY:25,ETXTBSY:26,EFBIG:27,ENOSPC:28,ESPIPE:29,EROFS:30,EMLINK:31,EPIPE:32,EDOM:33,ERANGE:34,ENOMSG:42,EIDRM:43,ECHRNG:44,EL2NSYNC:45,EL3HLT:46,EL3RST:47,ELNRNG:48,EUNATCH:49,ENOCSI:50,EL2HLT:51,EDEADLK:35,ENOLCK:37,EBADE:52,EBADR:53,EXFULL:54,ENOANO:55,EBADRQC:56,EBADSLT:57,EDEADLOCK:35,EBFONT:59,ENOSTR:60,ENODATA:61,ETIME:62,ENOSR:63,ENONET:64,ENOPKG:65,EREMOTE:66,ENOLINK:67,EADV:68,ESRMNT:69,ECOMM:70,EPROTO:71,EMULTIHOP:72,EDOTDOT:73,EBADMSG:74,ENOTUNIQ:76,EBADFD:77,EREMCHG:78,ELIBACC:79,ELIBBAD:80,ELIBSCN:81,ELIBMAX:82,ELIBEXEC:83,ENOSYS:38,ENOTEMPTY:39,ENAMETOOLONG:36,ELOOP:40,EOPNOTSUPP:95,EPFNOSUPPORT:96,ECONNRESET:104,ENOBUFS:105,EAFNOSUPPORT:97,EPROTOTYPE:91,ENOTSOCK:88,ENOPROTOOPT:92,ESHUTDOWN:108,ECONNREFUSED:111,EADDRINUSE:98,ECONNABORTED:103,ENETUNREACH:101,ENETDOWN:100,ETIMEDOUT:110,EHOSTDOWN:112,EHOSTUNREACH:113,EINPROGRESS:115,EALREADY:114,EDESTADDRREQ:89,EMSGSIZE:90,EPROTONOSUPPORT:93,ESOCKTNOSUPPORT:94,EADDRNOTAVAIL:99,ENETRESET:102,EISCONN:106,ENOTCONN:107,ETOOMANYREFS:109,EUSERS:87,EDQUOT:122,ESTALE:116,ENOTSUP:95,ENOMEDIUM:123,EILSEQ:84,EOVERFLOW:75,ECANCELED:125,ENOTRECOVERABLE:131,EOWNERDEAD:130,ESTRPIPE:86};
  var ERRNO_MESSAGES={0:"Success",1:"Not super-user",2:"No such file or directory",3:"No such process",4:"Interrupted system call",5:"I/O error",6:"No such device or address",7:"Arg list too long",8:"Exec format error",9:"Bad file number",10:"No children",11:"No more processes",12:"Not enough core",13:"Permission denied",14:"Bad address",15:"Block device required",16:"Mount device busy",17:"File exists",18:"Cross-device link",19:"No such device",20:"Not a directory",21:"Is a directory",22:"Invalid argument",23:"Too many open files in system",24:"Too many open files",25:"Not a typewriter",26:"Text file busy",27:"File too large",28:"No space left on device",29:"Illegal seek",30:"Read only file system",31:"Too many links",32:"Broken pipe",33:"Math arg out of domain of func",34:"Math result not representable",35:"File locking deadlock error",36:"File or path name too long",37:"No record locks available",38:"Function not implemented",39:"Directory not empty",40:"Too many symbolic links",42:"No message of desired type",43:"Identifier removed",44:"Channel number out of range",45:"Level 2 not synchronized",46:"Level 3 halted",47:"Level 3 reset",48:"Link number out of range",49:"Protocol driver not attached",50:"No CSI structure available",51:"Level 2 halted",52:"Invalid exchange",53:"Invalid request descriptor",54:"Exchange full",55:"No anode",56:"Invalid request code",57:"Invalid slot",59:"Bad font file fmt",60:"Device not a stream",61:"No data (for no delay io)",62:"Timer expired",63:"Out of streams resources",64:"Machine is not on the network",65:"Package not installed",66:"The object is remote",67:"The link has been severed",68:"Advertise error",69:"Srmount error",70:"Communication error on send",71:"Protocol error",72:"Multihop attempted",73:"Cross mount point (not really error)",74:"Trying to read unreadable message",75:"Value too large for defined data type",76:"Given log. name not unique",77:"f.d. invalid for this operation",78:"Remote address changed",79:"Can   access a needed shared lib",80:"Accessing a corrupted shared lib",81:".lib section in a.out corrupted",82:"Attempting to link in too many libs",83:"Attempting to exec a shared library",84:"Illegal byte sequence",86:"Streams pipe error",87:"Too many users",88:"Socket operation on non-socket",89:"Destination address required",90:"Message too long",91:"Protocol wrong type for socket",92:"Protocol not available",93:"Unknown protocol",94:"Socket type not supported",95:"Not supported",96:"Protocol family not supported",97:"Address family not supported by protocol family",98:"Address already in use",99:"Address not available",100:"Network interface is not configured",101:"Network is unreachable",102:"Connection reset by network",103:"Connection aborted",104:"Connection reset by peer",105:"No buffer space available",106:"Socket is already connected",107:"Socket is not connected",108:"Can't send after socket shutdown",109:"Too many references",110:"Connection timed out",111:"Connection refused",112:"Host is down",113:"Host is unreachable",114:"Socket already connected",115:"Connection already in progress",116:"Stale file handle",122:"Quota exceeded",123:"No medium (in tape drive)",125:"Operation canceled",130:"Previous owner died",131:"State not recoverable"};
  var ___errno_state=0;function ___setErrNo(value) {
      // For convenient setting and returning of errno.
      HEAP32[((___errno_state)>>2)]=value
      return value;
    }
  var PATH={splitPath:function (filename) {
        var splitPathRe = /^(\/?|)([\s\S]*?)((?:\.{1,2}|[^\/]+?|)(\.[^.\/]*|))(?:[\/]*)$/;
        return splitPathRe.exec(filename).slice(1);
      },normalizeArray:function (parts, allowAboveRoot) {
        // if the path tries to go above the root, `up` ends up > 0
        var up = 0;
        for (var i = parts.length - 1; i >= 0; i--) {
          var last = parts[i];
          if (last === '.') {
            parts.splice(i, 1);
          } else if (last === '..') {
            parts.splice(i, 1);
            up++;
          } else if (up) {
            parts.splice(i, 1);
            up--;
          }
        }
        // if the path is allowed to go above the root, restore leading ..s
        if (allowAboveRoot) {
          for (; up--; up) {
            parts.unshift('..');
          }
        }
        return parts;
      },normalize:function (path) {
        var isAbsolute = path.charAt(0) === '/',
            trailingSlash = path.substr(-1) === '/';
        // Normalize the path
        path = PATH.normalizeArray(path.split('/').filter(function(p) {
          return !!p;
        }), !isAbsolute).join('/');
        if (!path && !isAbsolute) {
          path = '.';
        }
        if (path && trailingSlash) {
          path += '/';
        }
        return (isAbsolute ? '/' : '') + path;
      },dirname:function (path) {
        var result = PATH.splitPath(path),
            root = result[0],
            dir = result[1];
        if (!root && !dir) {
          // No dirname whatsoever
          return '.';
        }
        if (dir) {
          // It has a dirname, strip trailing slash
          dir = dir.substr(0, dir.length - 1);
        }
        return root + dir;
      },basename:function (path) {
        // EMSCRIPTEN return '/'' for '/', not an empty string
        if (path === '/') return '/';
        var lastSlash = path.lastIndexOf('/');
        if (lastSlash === -1) return path;
        return path.substr(lastSlash+1);
      },extname:function (path) {
        return PATH.splitPath(path)[3];
      },join:function () {
        var paths = Array.prototype.slice.call(arguments, 0);
        return PATH.normalize(paths.join('/'));
      },join2:function (l, r) {
        return PATH.normalize(l + '/' + r);
      },resolve:function () {
        var resolvedPath = '',
          resolvedAbsolute = false;
        for (var i = arguments.length - 1; i >= -1 && !resolvedAbsolute; i--) {
          var path = (i >= 0) ? arguments[i] : FS.cwd();
          // Skip empty and invalid entries
          if (typeof path !== 'string') {
            throw new TypeError('Arguments to path.resolve must be strings');
          } else if (!path) {
            continue;
          }
          resolvedPath = path + '/' + resolvedPath;
          resolvedAbsolute = path.charAt(0) === '/';
        }
        // At this point the path should be resolved to a full absolute path, but
        // handle relative paths to be safe (might happen when process.cwd() fails)
        resolvedPath = PATH.normalizeArray(resolvedPath.split('/').filter(function(p) {
          return !!p;
        }), !resolvedAbsolute).join('/');
        return ((resolvedAbsolute ? '/' : '') + resolvedPath) || '.';
      },relative:function (from, to) {
        from = PATH.resolve(from).substr(1);
        to = PATH.resolve(to).substr(1);
        function trim(arr) {
          var start = 0;
          for (; start < arr.length; start++) {
            if (arr[start] !== '') break;
          }
          var end = arr.length - 1;
          for (; end >= 0; end--) {
            if (arr[end] !== '') break;
          }
          if (start > end) return [];
          return arr.slice(start, end - start + 1);
        }
        var fromParts = trim(from.split('/'));
        var toParts = trim(to.split('/'));
        var length = Math.min(fromParts.length, toParts.length);
        var samePartsLength = length;
        for (var i = 0; i < length; i++) {
          if (fromParts[i] !== toParts[i]) {
            samePartsLength = i;
            break;
          }
        }
        var outputParts = [];
        for (var i = samePartsLength; i < fromParts.length; i++) {
          outputParts.push('..');
        }
        outputParts = outputParts.concat(toParts.slice(samePartsLength));
        return outputParts.join('/');
      }};
  var TTY={ttys:[],init:function () {
        // https://github.com/kripken/emscripten/pull/1555
        // if (ENVIRONMENT_IS_NODE) {
        //   // currently, FS.init does not distinguish if process.stdin is a file or TTY
        //   // device, it always assumes it's a TTY device. because of this, we're forcing
        //   // process.stdin to UTF8 encoding to at least make stdin reading compatible
        //   // with text files until FS.init can be refactored.
        //   process['stdin']['setEncoding']('utf8');
        // }
      },shutdown:function () {
        // https://github.com/kripken/emscripten/pull/1555
        // if (ENVIRONMENT_IS_NODE) {
        //   // inolen: any idea as to why node -e 'process.stdin.read()' wouldn't exit immediately (with process.stdin being a tty)?
        //   // isaacs: because now it's reading from the stream, you've expressed interest in it, so that read() kicks off a _read() which creates a ReadReq operation
        //   // inolen: I thought read() in that case was a synchronous operation that just grabbed some amount of buffered data if it exists?
        //   // isaacs: it is. but it also triggers a _read() call, which calls readStart() on the handle
        //   // isaacs: do process.stdin.pause() and i'd think it'd probably close the pending call
        //   process['stdin']['pause']();
        // }
      },register:function (dev, ops) {
        TTY.ttys[dev] = { input: [], output: [], ops: ops };
        FS.registerDevice(dev, TTY.stream_ops);
      },stream_ops:{open:function (stream) {
          var tty = TTY.ttys[stream.node.rdev];
          if (!tty) {
            throw new FS.ErrnoError(ERRNO_CODES.ENODEV);
          }
          stream.tty = tty;
          stream.seekable = false;
        },close:function (stream) {
          // flush any pending line data
          if (stream.tty.output.length) {
            stream.tty.ops.put_char(stream.tty, 10);
          }
        },read:function (stream, buffer, offset, length, pos /* ignored */) {
          if (!stream.tty || !stream.tty.ops.get_char) {
            throw new FS.ErrnoError(ERRNO_CODES.ENXIO);
          }
          var bytesRead = 0;
          for (var i = 0; i < length; i++) {
            var result;
            try {
              result = stream.tty.ops.get_char(stream.tty);
            } catch (e) {
              throw new FS.ErrnoError(ERRNO_CODES.EIO);
            }
            if (result === undefined && bytesRead === 0) {
              throw new FS.ErrnoError(ERRNO_CODES.EAGAIN);
            }
            if (result === null || result === undefined) break;
            bytesRead++;
            buffer[offset+i] = result;
          }
          if (bytesRead) {
            stream.node.timestamp = Date.now();
          }
          return bytesRead;
        },write:function (stream, buffer, offset, length, pos) {
          if (!stream.tty || !stream.tty.ops.put_char) {
            throw new FS.ErrnoError(ERRNO_CODES.ENXIO);
          }
          for (var i = 0; i < length; i++) {
            try {
              stream.tty.ops.put_char(stream.tty, buffer[offset+i]);
            } catch (e) {
              throw new FS.ErrnoError(ERRNO_CODES.EIO);
            }
          }
          if (length) {
            stream.node.timestamp = Date.now();
          }
          return i;
        }},default_tty_ops:{get_char:function (tty) {
          if (!tty.input.length) {
            var result = null;
            if (ENVIRONMENT_IS_NODE) {
              result = process['stdin']['read']();
              if (!result) {
                if (process['stdin']['_readableState'] && process['stdin']['_readableState']['ended']) {
                  return null;  // EOF
                }
                return undefined;  // no data available
              }
            } else if (typeof window != 'undefined' &&
              typeof window.prompt == 'function') {
              // Browser.
              result = window.prompt('Input: ');  // returns null on cancel
              if (result !== null) {
                result += '\n';
              }
            } else if (typeof readline == 'function') {
              // Command line.
              result = readline();
              if (result !== null) {
                result += '\n';
              }
            }
            if (!result) {
              return null;
            }
            tty.input = intArrayFromString(result, true);
          }
          return tty.input.shift();
        },put_char:function (tty, val) {
          if (val === null || val === 10) {
            Module['print'](tty.output.join(''));
            tty.output = [];
          } else {
            tty.output.push(TTY.utf8.processCChar(val));
          }
        }},default_tty1_ops:{put_char:function (tty, val) {
          if (val === null || val === 10) {
            Module['printErr'](tty.output.join(''));
            tty.output = [];
          } else {
            tty.output.push(TTY.utf8.processCChar(val));
          }
        }}};
  var MEMFS={ops_table:null,CONTENT_OWNING:1,CONTENT_FLEXIBLE:2,CONTENT_FIXED:3,mount:function (mount) {
        return MEMFS.createNode(null, '/', 16384 | 0777, 0);
      },createNode:function (parent, name, mode, dev) {
        if (FS.isBlkdev(mode) || FS.isFIFO(mode)) {
          // no supported
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        if (!MEMFS.ops_table) {
          MEMFS.ops_table = {
            dir: {
              node: {
                getattr: MEMFS.node_ops.getattr,
                setattr: MEMFS.node_ops.setattr,
                lookup: MEMFS.node_ops.lookup,
                mknod: MEMFS.node_ops.mknod,
                mknod: MEMFS.node_ops.mknod,
                rename: MEMFS.node_ops.rename,
                unlink: MEMFS.node_ops.unlink,
                rmdir: MEMFS.node_ops.rmdir,
                readdir: MEMFS.node_ops.readdir,
                symlink: MEMFS.node_ops.symlink
              },
              stream: {
                llseek: MEMFS.stream_ops.llseek
              }
            },
            file: {
              node: {
                getattr: MEMFS.node_ops.getattr,
                setattr: MEMFS.node_ops.setattr
              },
              stream: {
                llseek: MEMFS.stream_ops.llseek,
                read: MEMFS.stream_ops.read,
                write: MEMFS.stream_ops.write,
                allocate: MEMFS.stream_ops.allocate,
                mmap: MEMFS.stream_ops.mmap
              }
            },
            link: {
              node: {
                getattr: MEMFS.node_ops.getattr,
                setattr: MEMFS.node_ops.setattr,
                readlink: MEMFS.node_ops.readlink
              },
              stream: {}
            },
            chrdev: {
              node: {
                getattr: MEMFS.node_ops.getattr,
                setattr: MEMFS.node_ops.setattr
              },
              stream: FS.chrdev_stream_ops
            },
          };
        }
        var node = FS.createNode(parent, name, mode, dev);
        if (FS.isDir(node.mode)) {
          node.node_ops = MEMFS.ops_table.dir.node;
          node.stream_ops = MEMFS.ops_table.dir.stream;
          node.contents = {};
        } else if (FS.isFile(node.mode)) {
          node.node_ops = MEMFS.ops_table.file.node;
          node.stream_ops = MEMFS.ops_table.file.stream;
          node.contents = [];
          node.contentMode = MEMFS.CONTENT_FLEXIBLE;
        } else if (FS.isLink(node.mode)) {
          node.node_ops = MEMFS.ops_table.link.node;
          node.stream_ops = MEMFS.ops_table.link.stream;
        } else if (FS.isChrdev(node.mode)) {
          node.node_ops = MEMFS.ops_table.chrdev.node;
          node.stream_ops = MEMFS.ops_table.chrdev.stream;
        }
        node.timestamp = Date.now();
        // add the new node to the parent
        if (parent) {
          parent.contents[name] = node;
        }
        return node;
      },ensureFlexible:function (node) {
        if (node.contentMode !== MEMFS.CONTENT_FLEXIBLE) {
          var contents = node.contents;
          node.contents = Array.prototype.slice.call(contents);
          node.contentMode = MEMFS.CONTENT_FLEXIBLE;
        }
      },node_ops:{getattr:function (node) {
          var attr = {};
          // device numbers reuse inode numbers.
          attr.dev = FS.isChrdev(node.mode) ? node.id : 1;
          attr.ino = node.id;
          attr.mode = node.mode;
          attr.nlink = 1;
          attr.uid = 0;
          attr.gid = 0;
          attr.rdev = node.rdev;
          if (FS.isDir(node.mode)) {
            attr.size = 4096;
          } else if (FS.isFile(node.mode)) {
            attr.size = node.contents.length;
          } else if (FS.isLink(node.mode)) {
            attr.size = node.link.length;
          } else {
            attr.size = 0;
          }
          attr.atime = new Date(node.timestamp);
          attr.mtime = new Date(node.timestamp);
          attr.ctime = new Date(node.timestamp);
          // NOTE: In our implementation, st_blocks = Math.ceil(st_size/st_blksize),
          //       but this is not required by the standard.
          attr.blksize = 4096;
          attr.blocks = Math.ceil(attr.size / attr.blksize);
          return attr;
        },setattr:function (node, attr) {
          if (attr.mode !== undefined) {
            node.mode = attr.mode;
          }
          if (attr.timestamp !== undefined) {
            node.timestamp = attr.timestamp;
          }
          if (attr.size !== undefined) {
            MEMFS.ensureFlexible(node);
            var contents = node.contents;
            if (attr.size < contents.length) contents.length = attr.size;
            else while (attr.size > contents.length) contents.push(0);
          }
        },lookup:function (parent, name) {
          throw FS.genericErrors[ERRNO_CODES.ENOENT];
        },mknod:function (parent, name, mode, dev) {
          return MEMFS.createNode(parent, name, mode, dev);
        },rename:function (old_node, new_dir, new_name) {
          // if we're overwriting a directory at new_name, make sure it's empty.
          if (FS.isDir(old_node.mode)) {
            var new_node;
            try {
              new_node = FS.lookupNode(new_dir, new_name);
            } catch (e) {
            }
            if (new_node) {
              for (var i in new_node.contents) {
                throw new FS.ErrnoError(ERRNO_CODES.ENOTEMPTY);
              }
            }
          }
          // do the internal rewiring
          delete old_node.parent.contents[old_node.name];
          old_node.name = new_name;
          new_dir.contents[new_name] = old_node;
          old_node.parent = new_dir;
        },unlink:function (parent, name) {
          delete parent.contents[name];
        },rmdir:function (parent, name) {
          var node = FS.lookupNode(parent, name);
          for (var i in node.contents) {
            throw new FS.ErrnoError(ERRNO_CODES.ENOTEMPTY);
          }
          delete parent.contents[name];
        },readdir:function (node) {
          var entries = ['.', '..']
          for (var key in node.contents) {
            if (!node.contents.hasOwnProperty(key)) {
              continue;
            }
            entries.push(key);
          }
          return entries;
        },symlink:function (parent, newname, oldpath) {
          var node = MEMFS.createNode(parent, newname, 0777 | 40960, 0);
          node.link = oldpath;
          return node;
        },readlink:function (node) {
          if (!FS.isLink(node.mode)) {
            throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
          }
          return node.link;
        }},stream_ops:{read:function (stream, buffer, offset, length, position) {
          var contents = stream.node.contents;
          if (position >= contents.length)
            return 0;
          var size = Math.min(contents.length - position, length);
          assert(size >= 0);
          if (size > 8 && contents.subarray) { // non-trivial, and typed array
            buffer.set(contents.subarray(position, position + size), offset);
          } else
          {
            for (var i = 0; i < size; i++) {
              buffer[offset + i] = contents[position + i];
            }
          }
          return size;
        },write:function (stream, buffer, offset, length, position, canOwn) {
          var node = stream.node;
          node.timestamp = Date.now();
          var contents = node.contents;
          if (length && contents.length === 0 && position === 0 && buffer.subarray) {
            // just replace it with the new data
            if (canOwn && offset === 0) {
              node.contents = buffer; // this could be a subarray of Emscripten HEAP, or allocated from some other source.
              node.contentMode = (buffer.buffer === HEAP8.buffer) ? MEMFS.CONTENT_OWNING : MEMFS.CONTENT_FIXED;
            } else {
              node.contents = new Uint8Array(buffer.subarray(offset, offset+length));
              node.contentMode = MEMFS.CONTENT_FIXED;
            }
            return length;
          }
          MEMFS.ensureFlexible(node);
          var contents = node.contents;
          while (contents.length < position) contents.push(0);
          for (var i = 0; i < length; i++) {
            contents[position + i] = buffer[offset + i];
          }
          return length;
        },llseek:function (stream, offset, whence) {
          var position = offset;
          if (whence === 1) {  // SEEK_CUR.
            position += stream.position;
          } else if (whence === 2) {  // SEEK_END.
            if (FS.isFile(stream.node.mode)) {
              position += stream.node.contents.length;
            }
          }
          if (position < 0) {
            throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
          }
          stream.ungotten = [];
          stream.position = position;
          return position;
        },allocate:function (stream, offset, length) {
          MEMFS.ensureFlexible(stream.node);
          var contents = stream.node.contents;
          var limit = offset + length;
          while (limit > contents.length) contents.push(0);
        },mmap:function (stream, buffer, offset, length, position, prot, flags) {
          if (!FS.isFile(stream.node.mode)) {
            throw new FS.ErrnoError(ERRNO_CODES.ENODEV);
          }
          var ptr;
          var allocated;
          var contents = stream.node.contents;
          // Only make a new copy when MAP_PRIVATE is specified.
          if ( !(flags & 2) &&
                (contents.buffer === buffer || contents.buffer === buffer.buffer) ) {
            // We can't emulate MAP_SHARED when the file is not backed by the buffer
            // we're mapping to (e.g. the HEAP buffer).
            allocated = false;
            ptr = contents.byteOffset;
          } else {
            // Try to avoid unnecessary slices.
            if (position > 0 || position + length < contents.length) {
              if (contents.subarray) {
                contents = contents.subarray(position, position + length);
              } else {
                contents = Array.prototype.slice.call(contents, position, position + length);
              }
            }
            allocated = true;
            ptr = _malloc(length);
            if (!ptr) {
              throw new FS.ErrnoError(ERRNO_CODES.ENOMEM);
            }
            buffer.set(contents, ptr);
          }
          return { ptr: ptr, allocated: allocated };
        }}};
  var IDBFS={dbs:{},indexedDB:function () {
        return window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
      },DB_VERSION:20,DB_STORE_NAME:"FILE_DATA",mount:function (mount) {
        return MEMFS.mount.apply(null, arguments);
      },syncfs:function (mount, populate, callback) {
        IDBFS.getLocalSet(mount, function(err, local) {
          if (err) return callback(err);
          IDBFS.getRemoteSet(mount, function(err, remote) {
            if (err) return callback(err);
            var src = populate ? remote : local;
            var dst = populate ? local : remote;
            IDBFS.reconcile(src, dst, callback);
          });
        });
      },reconcile:function (src, dst, callback) {
        var total = 0;
        var create = {};
        for (var key in src.files) {
          if (!src.files.hasOwnProperty(key)) continue;
          var e = src.files[key];
          var e2 = dst.files[key];
          if (!e2 || e.timestamp > e2.timestamp) {
            create[key] = e;
            total++;
          }
        }
        var remove = {};
        for (var key in dst.files) {
          if (!dst.files.hasOwnProperty(key)) continue;
          var e = dst.files[key];
          var e2 = src.files[key];
          if (!e2) {
            remove[key] = e;
            total++;
          }
        }
        if (!total) {
          // early out
          return callback(null);
        }
        var completed = 0;
        function done(err) {
          if (err) return callback(err);
          if (++completed >= total) {
            return callback(null);
          }
        };
        // create a single transaction to handle and IDB reads / writes we'll need to do
        var db = src.type === 'remote' ? src.db : dst.db;
        var transaction = db.transaction([IDBFS.DB_STORE_NAME], 'readwrite');
        transaction.onerror = function transaction_onerror() { callback(this.error); };
        var store = transaction.objectStore(IDBFS.DB_STORE_NAME);
        for (var path in create) {
          if (!create.hasOwnProperty(path)) continue;
          var entry = create[path];
          if (dst.type === 'local') {
            // save file to local
            try {
              if (FS.isDir(entry.mode)) {
                FS.mkdir(path, entry.mode);
              } else if (FS.isFile(entry.mode)) {
                var stream = FS.open(path, 'w+', 0666);
                FS.write(stream, entry.contents, 0, entry.contents.length, 0, true /* canOwn */);
                FS.close(stream);
              }
              done(null);
            } catch (e) {
              return done(e);
            }
          } else {
            // save file to IDB
            var req = store.put(entry, path);
            req.onsuccess = function req_onsuccess() { done(null); };
            req.onerror = function req_onerror() { done(this.error); };
          }
        }
        for (var path in remove) {
          if (!remove.hasOwnProperty(path)) continue;
          var entry = remove[path];
          if (dst.type === 'local') {
            // delete file from local
            try {
              if (FS.isDir(entry.mode)) {
                // TODO recursive delete?
                FS.rmdir(path);
              } else if (FS.isFile(entry.mode)) {
                FS.unlink(path);
              }
              done(null);
            } catch (e) {
              return done(e);
            }
          } else {
            // delete file from IDB
            var req = store.delete(path);
            req.onsuccess = function req_onsuccess() { done(null); };
            req.onerror = function req_onerror() { done(this.error); };
          }
        }
      },getLocalSet:function (mount, callback) {
        var files = {};
        function isRealDir(p) {
          return p !== '.' && p !== '..';
        };
        function toAbsolute(root) {
          return function(p) {
            return PATH.join2(root, p);
          }
        };
        var check = FS.readdir(mount.mountpoint)
          .filter(isRealDir)
          .map(toAbsolute(mount.mountpoint));
        while (check.length) {
          var path = check.pop();
          var stat, node;
          try {
            var lookup = FS.lookupPath(path);
            node = lookup.node;
            stat = FS.stat(path);
          } catch (e) {
            return callback(e);
          }
          if (FS.isDir(stat.mode)) {
            check.push.apply(check, FS.readdir(path)
              .filter(isRealDir)
              .map(toAbsolute(path)));
            files[path] = { mode: stat.mode, timestamp: stat.mtime };
          } else if (FS.isFile(stat.mode)) {
            files[path] = { contents: node.contents, mode: stat.mode, timestamp: stat.mtime };
          } else {
            return callback(new Error('node type not supported'));
          }
        }
        return callback(null, { type: 'local', files: files });
      },getDB:function (name, callback) {
        // look it up in the cache
        var db = IDBFS.dbs[name];
        if (db) {
          return callback(null, db);
        }
        var req;
        try {
          req = IDBFS.indexedDB().open(name, IDBFS.DB_VERSION);
        } catch (e) {
          return onerror(e);
        }
        req.onupgradeneeded = function req_onupgradeneeded() {
          db = req.result;
          db.createObjectStore(IDBFS.DB_STORE_NAME);
        };
        req.onsuccess = function req_onsuccess() {
          db = req.result;
          // add to the cache
          IDBFS.dbs[name] = db;
          callback(null, db);
        };
        req.onerror = function req_onerror() {
          callback(this.error);
        };
      },getRemoteSet:function (mount, callback) {
        var files = {};
        IDBFS.getDB(mount.mountpoint, function(err, db) {
          if (err) return callback(err);
          var transaction = db.transaction([IDBFS.DB_STORE_NAME], 'readonly');
          transaction.onerror = function transaction_onerror() { callback(this.error); };
          var store = transaction.objectStore(IDBFS.DB_STORE_NAME);
          store.openCursor().onsuccess = function store_openCursor_onsuccess(event) {
            var cursor = event.target.result;
            if (!cursor) {
              return callback(null, { type: 'remote', db: db, files: files });
            }
            files[cursor.key] = cursor.value;
            cursor.continue();
          };
        });
      }};
  var NODEFS={isWindows:false,staticInit:function () {
        NODEFS.isWindows = !!process.platform.match(/^win/);
      },mount:function (mount) {
        assert(ENVIRONMENT_IS_NODE);
        return NODEFS.createNode(null, '/', NODEFS.getMode(mount.opts.root), 0);
      },createNode:function (parent, name, mode, dev) {
        if (!FS.isDir(mode) && !FS.isFile(mode) && !FS.isLink(mode)) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        var node = FS.createNode(parent, name, mode);
        node.node_ops = NODEFS.node_ops;
        node.stream_ops = NODEFS.stream_ops;
        return node;
      },getMode:function (path) {
        var stat;
        try {
          stat = fs.lstatSync(path);
          if (NODEFS.isWindows) {
            // On Windows, directories return permission bits 'rw-rw-rw-', even though they have 'rwxrwxrwx', so 
            // propagate write bits to execute bits.
            stat.mode = stat.mode | ((stat.mode & 146) >> 1);
          }
        } catch (e) {
          if (!e.code) throw e;
          throw new FS.ErrnoError(ERRNO_CODES[e.code]);
        }
        return stat.mode;
      },realPath:function (node) {
        var parts = [];
        while (node.parent !== node) {
          parts.push(node.name);
          node = node.parent;
        }
        parts.push(node.mount.opts.root);
        parts.reverse();
        return PATH.join.apply(null, parts);
      },flagsToPermissionStringMap:{0:"r",1:"r+",2:"r+",64:"r",65:"r+",66:"r+",129:"rx+",193:"rx+",514:"w+",577:"w",578:"w+",705:"wx",706:"wx+",1024:"a",1025:"a",1026:"a+",1089:"a",1090:"a+",1153:"ax",1154:"ax+",1217:"ax",1218:"ax+",4096:"rs",4098:"rs+"},flagsToPermissionString:function (flags) {
        if (flags in NODEFS.flagsToPermissionStringMap) {
          return NODEFS.flagsToPermissionStringMap[flags];
        } else {
          return flags;
        }
      },node_ops:{getattr:function (node) {
          var path = NODEFS.realPath(node);
          var stat;
          try {
            stat = fs.lstatSync(path);
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
          // node.js v0.10.20 doesn't report blksize and blocks on Windows. Fake them with default blksize of 4096.
          // See http://support.microsoft.com/kb/140365
          if (NODEFS.isWindows && !stat.blksize) {
            stat.blksize = 4096;
          }
          if (NODEFS.isWindows && !stat.blocks) {
            stat.blocks = (stat.size+stat.blksize-1)/stat.blksize|0;
          }
          return {
            dev: stat.dev,
            ino: stat.ino,
            mode: stat.mode,
            nlink: stat.nlink,
            uid: stat.uid,
            gid: stat.gid,
            rdev: stat.rdev,
            size: stat.size,
            atime: stat.atime,
            mtime: stat.mtime,
            ctime: stat.ctime,
            blksize: stat.blksize,
            blocks: stat.blocks
          };
        },setattr:function (node, attr) {
          var path = NODEFS.realPath(node);
          try {
            if (attr.mode !== undefined) {
              fs.chmodSync(path, attr.mode);
              // update the common node structure mode as well
              node.mode = attr.mode;
            }
            if (attr.timestamp !== undefined) {
              var date = new Date(attr.timestamp);
              fs.utimesSync(path, date, date);
            }
            if (attr.size !== undefined) {
              fs.truncateSync(path, attr.size);
            }
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        },lookup:function (parent, name) {
          var path = PATH.join2(NODEFS.realPath(parent), name);
          var mode = NODEFS.getMode(path);
          return NODEFS.createNode(parent, name, mode);
        },mknod:function (parent, name, mode, dev) {
          var node = NODEFS.createNode(parent, name, mode, dev);
          // create the backing node for this in the fs root as well
          var path = NODEFS.realPath(node);
          try {
            if (FS.isDir(node.mode)) {
              fs.mkdirSync(path, node.mode);
            } else {
              fs.writeFileSync(path, '', { mode: node.mode });
            }
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
          return node;
        },rename:function (oldNode, newDir, newName) {
          var oldPath = NODEFS.realPath(oldNode);
          var newPath = PATH.join2(NODEFS.realPath(newDir), newName);
          try {
            fs.renameSync(oldPath, newPath);
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        },unlink:function (parent, name) {
          var path = PATH.join2(NODEFS.realPath(parent), name);
          try {
            fs.unlinkSync(path);
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        },rmdir:function (parent, name) {
          var path = PATH.join2(NODEFS.realPath(parent), name);
          try {
            fs.rmdirSync(path);
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        },readdir:function (node) {
          var path = NODEFS.realPath(node);
          try {
            return fs.readdirSync(path);
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        },symlink:function (parent, newName, oldPath) {
          var newPath = PATH.join2(NODEFS.realPath(parent), newName);
          try {
            fs.symlinkSync(oldPath, newPath);
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        },readlink:function (node) {
          var path = NODEFS.realPath(node);
          try {
            return fs.readlinkSync(path);
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        }},stream_ops:{open:function (stream) {
          var path = NODEFS.realPath(stream.node);
          try {
            if (FS.isFile(stream.node.mode)) {
              stream.nfd = fs.openSync(path, NODEFS.flagsToPermissionString(stream.flags));
            }
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        },close:function (stream) {
          try {
            if (FS.isFile(stream.node.mode) && stream.nfd) {
              fs.closeSync(stream.nfd);
            }
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        },read:function (stream, buffer, offset, length, position) {
          // FIXME this is terrible.
          var nbuffer = new Buffer(length);
          var res;
          try {
            res = fs.readSync(stream.nfd, nbuffer, 0, length, position);
          } catch (e) {
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
          if (res > 0) {
            for (var i = 0; i < res; i++) {
              buffer[offset + i] = nbuffer[i];
            }
          }
          return res;
        },write:function (stream, buffer, offset, length, position) {
          // FIXME this is terrible.
          var nbuffer = new Buffer(buffer.subarray(offset, offset + length));
          var res;
          try {
            res = fs.writeSync(stream.nfd, nbuffer, 0, length, position);
          } catch (e) {
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
          return res;
        },llseek:function (stream, offset, whence) {
          var position = offset;
          if (whence === 1) {  // SEEK_CUR.
            position += stream.position;
          } else if (whence === 2) {  // SEEK_END.
            if (FS.isFile(stream.node.mode)) {
              try {
                var stat = fs.fstatSync(stream.nfd);
                position += stat.size;
              } catch (e) {
                throw new FS.ErrnoError(ERRNO_CODES[e.code]);
              }
            }
          }
          if (position < 0) {
            throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
          }
          stream.position = position;
          return position;
        }}};
  var _stdin=allocate(1, "i32*", ALLOC_STATIC);
  var _stdout=allocate(1, "i32*", ALLOC_STATIC);
  var _stderr=allocate(1, "i32*", ALLOC_STATIC);
  function _fflush(stream) {
      // int fflush(FILE *stream);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/fflush.html
      // we don't currently perform any user-space buffering of data
    }var FS={root:null,mounts:[],devices:[null],streams:[null],nextInode:1,nameTable:null,currentPath:"/",initialized:false,ignorePermissions:true,ErrnoError:null,genericErrors:{},handleFSError:function (e) {
        if (!(e instanceof FS.ErrnoError)) throw e + ' : ' + stackTrace();
        return ___setErrNo(e.errno);
      },lookupPath:function (path, opts) {
        path = PATH.resolve(FS.cwd(), path);
        opts = opts || { recurse_count: 0 };
        if (opts.recurse_count > 8) {  // max recursive lookup of 8
          throw new FS.ErrnoError(ERRNO_CODES.ELOOP);
        }
        // split the path
        var parts = PATH.normalizeArray(path.split('/').filter(function(p) {
          return !!p;
        }), false);
        // start at the root
        var current = FS.root;
        var current_path = '/';
        for (var i = 0; i < parts.length; i++) {
          var islast = (i === parts.length-1);
          if (islast && opts.parent) {
            // stop resolving
            break;
          }
          current = FS.lookupNode(current, parts[i]);
          current_path = PATH.join2(current_path, parts[i]);
          // jump to the mount's root node if this is a mountpoint
          if (FS.isMountpoint(current)) {
            current = current.mount.root;
          }
          // follow symlinks
          // by default, lookupPath will not follow a symlink if it is the final path component.
          // setting opts.follow = true will override this behavior.
          if (!islast || opts.follow) {
            var count = 0;
            while (FS.isLink(current.mode)) {
              var link = FS.readlink(current_path);
              current_path = PATH.resolve(PATH.dirname(current_path), link);
              var lookup = FS.lookupPath(current_path, { recurse_count: opts.recurse_count });
              current = lookup.node;
              if (count++ > 40) {  // limit max consecutive symlinks to 40 (SYMLOOP_MAX).
                throw new FS.ErrnoError(ERRNO_CODES.ELOOP);
              }
            }
          }
        }
        return { path: current_path, node: current };
      },getPath:function (node) {
        var path;
        while (true) {
          if (FS.isRoot(node)) {
            var mount = node.mount.mountpoint;
            if (!path) return mount;
            return mount[mount.length-1] !== '/' ? mount + '/' + path : mount + path;
          }
          path = path ? node.name + '/' + path : node.name;
          node = node.parent;
        }
      },hashName:function (parentid, name) {
        var hash = 0;
        for (var i = 0; i < name.length; i++) {
          hash = ((hash << 5) - hash + name.charCodeAt(i)) | 0;
        }
        return ((parentid + hash) >>> 0) % FS.nameTable.length;
      },hashAddNode:function (node) {
        var hash = FS.hashName(node.parent.id, node.name);
        node.name_next = FS.nameTable[hash];
        FS.nameTable[hash] = node;
      },hashRemoveNode:function (node) {
        var hash = FS.hashName(node.parent.id, node.name);
        if (FS.nameTable[hash] === node) {
          FS.nameTable[hash] = node.name_next;
        } else {
          var current = FS.nameTable[hash];
          while (current) {
            if (current.name_next === node) {
              current.name_next = node.name_next;
              break;
            }
            current = current.name_next;
          }
        }
      },lookupNode:function (parent, name) {
        var err = FS.mayLookup(parent);
        if (err) {
          throw new FS.ErrnoError(err);
        }
        var hash = FS.hashName(parent.id, name);
        for (var node = FS.nameTable[hash]; node; node = node.name_next) {
          var nodeName = node.name;
          if (node.parent.id === parent.id && nodeName === name) {
            return node;
          }
        }
        // if we failed to find it in the cache, call into the VFS
        return FS.lookup(parent, name);
      },createNode:function (parent, name, mode, rdev) {
        if (!FS.FSNode) {
          FS.FSNode = function(parent, name, mode, rdev) {
            this.id = FS.nextInode++;
            this.name = name;
            this.mode = mode;
            this.node_ops = {};
            this.stream_ops = {};
            this.rdev = rdev;
            this.parent = null;
            this.mount = null;
            if (!parent) {
              parent = this;  // root node sets parent to itself
            }
            this.parent = parent;
            this.mount = parent.mount;
            FS.hashAddNode(this);
          };
          // compatibility
          var readMode = 292 | 73;
          var writeMode = 146;
          FS.FSNode.prototype = {};
          // NOTE we must use Object.defineProperties instead of individual calls to
          // Object.defineProperty in order to make closure compiler happy
          Object.defineProperties(FS.FSNode.prototype, {
            read: {
              get: function() { return (this.mode & readMode) === readMode; },
              set: function(val) { val ? this.mode |= readMode : this.mode &= ~readMode; }
            },
            write: {
              get: function() { return (this.mode & writeMode) === writeMode; },
              set: function(val) { val ? this.mode |= writeMode : this.mode &= ~writeMode; }
            },
            isFolder: {
              get: function() { return FS.isDir(this.mode); },
            },
            isDevice: {
              get: function() { return FS.isChrdev(this.mode); },
            },
          });
        }
        return new FS.FSNode(parent, name, mode, rdev);
      },destroyNode:function (node) {
        FS.hashRemoveNode(node);
      },isRoot:function (node) {
        return node === node.parent;
      },isMountpoint:function (node) {
        return node.mounted;
      },isFile:function (mode) {
        return (mode & 61440) === 32768;
      },isDir:function (mode) {
        return (mode & 61440) === 16384;
      },isLink:function (mode) {
        return (mode & 61440) === 40960;
      },isChrdev:function (mode) {
        return (mode & 61440) === 8192;
      },isBlkdev:function (mode) {
        return (mode & 61440) === 24576;
      },isFIFO:function (mode) {
        return (mode & 61440) === 4096;
      },isSocket:function (mode) {
        return (mode & 49152) === 49152;
      },flagModes:{"r":0,"rs":1052672,"r+":2,"w":577,"wx":705,"xw":705,"w+":578,"wx+":706,"xw+":706,"a":1089,"ax":1217,"xa":1217,"a+":1090,"ax+":1218,"xa+":1218},modeStringToFlags:function (str) {
        var flags = FS.flagModes[str];
        if (typeof flags === 'undefined') {
          throw new Error('Unknown file open mode: ' + str);
        }
        return flags;
      },flagsToPermissionString:function (flag) {
        var accmode = flag & 2097155;
        var perms = ['r', 'w', 'rw'][accmode];
        if ((flag & 512)) {
          perms += 'w';
        }
        return perms;
      },nodePermissions:function (node, perms) {
        if (FS.ignorePermissions) {
          return 0;
        }
        // return 0 if any user, group or owner bits are set.
        if (perms.indexOf('r') !== -1 && !(node.mode & 292)) {
          return ERRNO_CODES.EACCES;
        } else if (perms.indexOf('w') !== -1 && !(node.mode & 146)) {
          return ERRNO_CODES.EACCES;
        } else if (perms.indexOf('x') !== -1 && !(node.mode & 73)) {
          return ERRNO_CODES.EACCES;
        }
        return 0;
      },mayLookup:function (dir) {
        return FS.nodePermissions(dir, 'x');
      },mayCreate:function (dir, name) {
        try {
          var node = FS.lookupNode(dir, name);
          return ERRNO_CODES.EEXIST;
        } catch (e) {
        }
        return FS.nodePermissions(dir, 'wx');
      },mayDelete:function (dir, name, isdir) {
        var node;
        try {
          node = FS.lookupNode(dir, name);
        } catch (e) {
          return e.errno;
        }
        var err = FS.nodePermissions(dir, 'wx');
        if (err) {
          return err;
        }
        if (isdir) {
          if (!FS.isDir(node.mode)) {
            return ERRNO_CODES.ENOTDIR;
          }
          if (FS.isRoot(node) || FS.getPath(node) === FS.cwd()) {
            return ERRNO_CODES.EBUSY;
          }
        } else {
          if (FS.isDir(node.mode)) {
            return ERRNO_CODES.EISDIR;
          }
        }
        return 0;
      },mayOpen:function (node, flags) {
        if (!node) {
          return ERRNO_CODES.ENOENT;
        }
        if (FS.isLink(node.mode)) {
          return ERRNO_CODES.ELOOP;
        } else if (FS.isDir(node.mode)) {
          if ((flags & 2097155) !== 0 ||  // opening for write
              (flags & 512)) {
            return ERRNO_CODES.EISDIR;
          }
        }
        return FS.nodePermissions(node, FS.flagsToPermissionString(flags));
      },MAX_OPEN_FDS:4096,nextfd:function (fd_start, fd_end) {
        fd_start = fd_start || 1;
        fd_end = fd_end || FS.MAX_OPEN_FDS;
        for (var fd = fd_start; fd <= fd_end; fd++) {
          if (!FS.streams[fd]) {
            return fd;
          }
        }
        throw new FS.ErrnoError(ERRNO_CODES.EMFILE);
      },getStream:function (fd) {
        return FS.streams[fd];
      },createStream:function (stream, fd_start, fd_end) {
        if (!FS.FSStream) {
          FS.FSStream = function(){};
          FS.FSStream.prototype = {};
          // compatibility
          Object.defineProperties(FS.FSStream.prototype, {
            object: {
              get: function() { return this.node; },
              set: function(val) { this.node = val; }
            },
            isRead: {
              get: function() { return (this.flags & 2097155) !== 1; }
            },
            isWrite: {
              get: function() { return (this.flags & 2097155) !== 0; }
            },
            isAppend: {
              get: function() { return (this.flags & 1024); }
            }
          });
        }
        if (stream.__proto__) {
          // reuse the object
          stream.__proto__ = FS.FSStream.prototype;
        } else {
          var newStream = new FS.FSStream();
          for (var p in stream) {
            newStream[p] = stream[p];
          }
          stream = newStream;
        }
        var fd = FS.nextfd(fd_start, fd_end);
        stream.fd = fd;
        FS.streams[fd] = stream;
        return stream;
      },closeStream:function (fd) {
        FS.streams[fd] = null;
      },chrdev_stream_ops:{open:function (stream) {
          var device = FS.getDevice(stream.node.rdev);
          // override node's stream ops with the device's
          stream.stream_ops = device.stream_ops;
          // forward the open call
          if (stream.stream_ops.open) {
            stream.stream_ops.open(stream);
          }
        },llseek:function () {
          throw new FS.ErrnoError(ERRNO_CODES.ESPIPE);
        }},major:function (dev) {
        return ((dev) >> 8);
      },minor:function (dev) {
        return ((dev) & 0xff);
      },makedev:function (ma, mi) {
        return ((ma) << 8 | (mi));
      },registerDevice:function (dev, ops) {
        FS.devices[dev] = { stream_ops: ops };
      },getDevice:function (dev) {
        return FS.devices[dev];
      },syncfs:function (populate, callback) {
        if (typeof(populate) === 'function') {
          callback = populate;
          populate = false;
        }
        var completed = 0;
        var total = FS.mounts.length;
        function done(err) {
          if (err) {
            return callback(err);
          }
          if (++completed >= total) {
            callback(null);
          }
        };
        // sync all mounts
        for (var i = 0; i < FS.mounts.length; i++) {
          var mount = FS.mounts[i];
          if (!mount.type.syncfs) {
            done(null);
            continue;
          }
          mount.type.syncfs(mount, populate, done);
        }
      },mount:function (type, opts, mountpoint) {
        var lookup;
        if (mountpoint) {
          lookup = FS.lookupPath(mountpoint, { follow: false });
          mountpoint = lookup.path;  // use the absolute path
        }
        var mount = {
          type: type,
          opts: opts,
          mountpoint: mountpoint,
          root: null
        };
        // create a root node for the fs
        var root = type.mount(mount);
        root.mount = mount;
        mount.root = root;
        // assign the mount info to the mountpoint's node
        if (lookup) {
          lookup.node.mount = mount;
          lookup.node.mounted = true;
          // compatibility update FS.root if we mount to /
          if (mountpoint === '/') {
            FS.root = mount.root;
          }
        }
        // add to our cached list of mounts
        FS.mounts.push(mount);
        return root;
      },lookup:function (parent, name) {
        return parent.node_ops.lookup(parent, name);
      },mknod:function (path, mode, dev) {
        var lookup = FS.lookupPath(path, { parent: true });
        var parent = lookup.node;
        var name = PATH.basename(path);
        var err = FS.mayCreate(parent, name);
        if (err) {
          throw new FS.ErrnoError(err);
        }
        if (!parent.node_ops.mknod) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        return parent.node_ops.mknod(parent, name, mode, dev);
      },create:function (path, mode) {
        mode = mode !== undefined ? mode : 0666;
        mode &= 4095;
        mode |= 32768;
        return FS.mknod(path, mode, 0);
      },mkdir:function (path, mode) {
        mode = mode !== undefined ? mode : 0777;
        mode &= 511 | 512;
        mode |= 16384;
        return FS.mknod(path, mode, 0);
      },mkdev:function (path, mode, dev) {
        if (typeof(dev) === 'undefined') {
          dev = mode;
          mode = 0666;
        }
        mode |= 8192;
        return FS.mknod(path, mode, dev);
      },symlink:function (oldpath, newpath) {
        var lookup = FS.lookupPath(newpath, { parent: true });
        var parent = lookup.node;
        var newname = PATH.basename(newpath);
        var err = FS.mayCreate(parent, newname);
        if (err) {
          throw new FS.ErrnoError(err);
        }
        if (!parent.node_ops.symlink) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        return parent.node_ops.symlink(parent, newname, oldpath);
      },rename:function (old_path, new_path) {
        var old_dirname = PATH.dirname(old_path);
        var new_dirname = PATH.dirname(new_path);
        var old_name = PATH.basename(old_path);
        var new_name = PATH.basename(new_path);
        // parents must exist
        var lookup, old_dir, new_dir;
        try {
          lookup = FS.lookupPath(old_path, { parent: true });
          old_dir = lookup.node;
          lookup = FS.lookupPath(new_path, { parent: true });
          new_dir = lookup.node;
        } catch (e) {
          throw new FS.ErrnoError(ERRNO_CODES.EBUSY);
        }
        // need to be part of the same mount
        if (old_dir.mount !== new_dir.mount) {
          throw new FS.ErrnoError(ERRNO_CODES.EXDEV);
        }
        // source must exist
        var old_node = FS.lookupNode(old_dir, old_name);
        // old path should not be an ancestor of the new path
        var relative = PATH.relative(old_path, new_dirname);
        if (relative.charAt(0) !== '.') {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        // new path should not be an ancestor of the old path
        relative = PATH.relative(new_path, old_dirname);
        if (relative.charAt(0) !== '.') {
          throw new FS.ErrnoError(ERRNO_CODES.ENOTEMPTY);
        }
        // see if the new path already exists
        var new_node;
        try {
          new_node = FS.lookupNode(new_dir, new_name);
        } catch (e) {
          // not fatal
        }
        // early out if nothing needs to change
        if (old_node === new_node) {
          return;
        }
        // we'll need to delete the old entry
        var isdir = FS.isDir(old_node.mode);
        var err = FS.mayDelete(old_dir, old_name, isdir);
        if (err) {
          throw new FS.ErrnoError(err);
        }
        // need delete permissions if we'll be overwriting.
        // need create permissions if new doesn't already exist.
        err = new_node ?
          FS.mayDelete(new_dir, new_name, isdir) :
          FS.mayCreate(new_dir, new_name);
        if (err) {
          throw new FS.ErrnoError(err);
        }
        if (!old_dir.node_ops.rename) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        if (FS.isMountpoint(old_node) || (new_node && FS.isMountpoint(new_node))) {
          throw new FS.ErrnoError(ERRNO_CODES.EBUSY);
        }
        // if we are going to change the parent, check write permissions
        if (new_dir !== old_dir) {
          err = FS.nodePermissions(old_dir, 'w');
          if (err) {
            throw new FS.ErrnoError(err);
          }
        }
        // remove the node from the lookup hash
        FS.hashRemoveNode(old_node);
        // do the underlying fs rename
        try {
          old_dir.node_ops.rename(old_node, new_dir, new_name);
        } catch (e) {
          throw e;
        } finally {
          // add the node back to the hash (in case node_ops.rename
          // changed its name)
          FS.hashAddNode(old_node);
        }
      },rmdir:function (path) {
        var lookup = FS.lookupPath(path, { parent: true });
        var parent = lookup.node;
        var name = PATH.basename(path);
        var node = FS.lookupNode(parent, name);
        var err = FS.mayDelete(parent, name, true);
        if (err) {
          throw new FS.ErrnoError(err);
        }
        if (!parent.node_ops.rmdir) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        if (FS.isMountpoint(node)) {
          throw new FS.ErrnoError(ERRNO_CODES.EBUSY);
        }
        parent.node_ops.rmdir(parent, name);
        FS.destroyNode(node);
      },readdir:function (path) {
        var lookup = FS.lookupPath(path, { follow: true });
        var node = lookup.node;
        if (!node.node_ops.readdir) {
          throw new FS.ErrnoError(ERRNO_CODES.ENOTDIR);
        }
        return node.node_ops.readdir(node);
      },unlink:function (path) {
        var lookup = FS.lookupPath(path, { parent: true });
        var parent = lookup.node;
        var name = PATH.basename(path);
        var node = FS.lookupNode(parent, name);
        var err = FS.mayDelete(parent, name, false);
        if (err) {
          // POSIX says unlink should set EPERM, not EISDIR
          if (err === ERRNO_CODES.EISDIR) err = ERRNO_CODES.EPERM;
          throw new FS.ErrnoError(err);
        }
        if (!parent.node_ops.unlink) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        if (FS.isMountpoint(node)) {
          throw new FS.ErrnoError(ERRNO_CODES.EBUSY);
        }
        parent.node_ops.unlink(parent, name);
        FS.destroyNode(node);
      },readlink:function (path) {
        var lookup = FS.lookupPath(path, { follow: false });
        var link = lookup.node;
        if (!link.node_ops.readlink) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        return link.node_ops.readlink(link);
      },stat:function (path, dontFollow) {
        var lookup = FS.lookupPath(path, { follow: !dontFollow });
        var node = lookup.node;
        if (!node.node_ops.getattr) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        return node.node_ops.getattr(node);
      },lstat:function (path) {
        return FS.stat(path, true);
      },chmod:function (path, mode, dontFollow) {
        var node;
        if (typeof path === 'string') {
          var lookup = FS.lookupPath(path, { follow: !dontFollow });
          node = lookup.node;
        } else {
          node = path;
        }
        if (!node.node_ops.setattr) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        node.node_ops.setattr(node, {
          mode: (mode & 4095) | (node.mode & ~4095),
          timestamp: Date.now()
        });
      },lchmod:function (path, mode) {
        FS.chmod(path, mode, true);
      },fchmod:function (fd, mode) {
        var stream = FS.getStream(fd);
        if (!stream) {
          throw new FS.ErrnoError(ERRNO_CODES.EBADF);
        }
        FS.chmod(stream.node, mode);
      },chown:function (path, uid, gid, dontFollow) {
        var node;
        if (typeof path === 'string') {
          var lookup = FS.lookupPath(path, { follow: !dontFollow });
          node = lookup.node;
        } else {
          node = path;
        }
        if (!node.node_ops.setattr) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        node.node_ops.setattr(node, {
          timestamp: Date.now()
          // we ignore the uid / gid for now
        });
      },lchown:function (path, uid, gid) {
        FS.chown(path, uid, gid, true);
      },fchown:function (fd, uid, gid) {
        var stream = FS.getStream(fd);
        if (!stream) {
          throw new FS.ErrnoError(ERRNO_CODES.EBADF);
        }
        FS.chown(stream.node, uid, gid);
      },truncate:function (path, len) {
        if (len < 0) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        var node;
        if (typeof path === 'string') {
          var lookup = FS.lookupPath(path, { follow: true });
          node = lookup.node;
        } else {
          node = path;
        }
        if (!node.node_ops.setattr) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        if (FS.isDir(node.mode)) {
          throw new FS.ErrnoError(ERRNO_CODES.EISDIR);
        }
        if (!FS.isFile(node.mode)) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        var err = FS.nodePermissions(node, 'w');
        if (err) {
          throw new FS.ErrnoError(err);
        }
        node.node_ops.setattr(node, {
          size: len,
          timestamp: Date.now()
        });
      },ftruncate:function (fd, len) {
        var stream = FS.getStream(fd);
        if (!stream) {
          throw new FS.ErrnoError(ERRNO_CODES.EBADF);
        }
        if ((stream.flags & 2097155) === 0) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        FS.truncate(stream.node, len);
      },utime:function (path, atime, mtime) {
        var lookup = FS.lookupPath(path, { follow: true });
        var node = lookup.node;
        node.node_ops.setattr(node, {
          timestamp: Math.max(atime, mtime)
        });
      },open:function (path, flags, mode, fd_start, fd_end) {
        flags = typeof flags === 'string' ? FS.modeStringToFlags(flags) : flags;
        mode = typeof mode === 'undefined' ? 0666 : mode;
        if ((flags & 64)) {
          mode = (mode & 4095) | 32768;
        } else {
          mode = 0;
        }
        var node;
        if (typeof path === 'object') {
          node = path;
        } else {
          path = PATH.normalize(path);
          try {
            var lookup = FS.lookupPath(path, {
              follow: !(flags & 131072)
            });
            node = lookup.node;
          } catch (e) {
            // ignore
          }
        }
        // perhaps we need to create the node
        if ((flags & 64)) {
          if (node) {
            // if O_CREAT and O_EXCL are set, error out if the node already exists
            if ((flags & 128)) {
              throw new FS.ErrnoError(ERRNO_CODES.EEXIST);
            }
          } else {
            // node doesn't exist, try to create it
            node = FS.mknod(path, mode, 0);
          }
        }
        if (!node) {
          throw new FS.ErrnoError(ERRNO_CODES.ENOENT);
        }
        // can't truncate a device
        if (FS.isChrdev(node.mode)) {
          flags &= ~512;
        }
        // check permissions
        var err = FS.mayOpen(node, flags);
        if (err) {
          throw new FS.ErrnoError(err);
        }
        // do truncation if necessary
        if ((flags & 512)) {
          FS.truncate(node, 0);
        }
        // we've already handled these, don't pass down to the underlying vfs
        flags &= ~(128 | 512);
        // register the stream with the filesystem
        var stream = FS.createStream({
          node: node,
          path: FS.getPath(node),  // we want the absolute path to the node
          flags: flags,
          seekable: true,
          position: 0,
          stream_ops: node.stream_ops,
          // used by the file family libc calls (fopen, fwrite, ferror, etc.)
          ungotten: [],
          error: false
        }, fd_start, fd_end);
        // call the new stream's open function
        if (stream.stream_ops.open) {
          stream.stream_ops.open(stream);
        }
        if (Module['logReadFiles'] && !(flags & 1)) {
          if (!FS.readFiles) FS.readFiles = {};
          if (!(path in FS.readFiles)) {
            FS.readFiles[path] = 1;
            Module['printErr']('read file: ' + path);
          }
        }
        return stream;
      },close:function (stream) {
        try {
          if (stream.stream_ops.close) {
            stream.stream_ops.close(stream);
          }
        } catch (e) {
          throw e;
        } finally {
          FS.closeStream(stream.fd);
        }
      },llseek:function (stream, offset, whence) {
        if (!stream.seekable || !stream.stream_ops.llseek) {
          throw new FS.ErrnoError(ERRNO_CODES.ESPIPE);
        }
        return stream.stream_ops.llseek(stream, offset, whence);
      },read:function (stream, buffer, offset, length, position) {
        if (length < 0 || position < 0) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        if ((stream.flags & 2097155) === 1) {
          throw new FS.ErrnoError(ERRNO_CODES.EBADF);
        }
        if (FS.isDir(stream.node.mode)) {
          throw new FS.ErrnoError(ERRNO_CODES.EISDIR);
        }
        if (!stream.stream_ops.read) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        var seeking = true;
        if (typeof position === 'undefined') {
          position = stream.position;
          seeking = false;
        } else if (!stream.seekable) {
          throw new FS.ErrnoError(ERRNO_CODES.ESPIPE);
        }
        var bytesRead = stream.stream_ops.read(stream, buffer, offset, length, position);
        if (!seeking) stream.position += bytesRead;
        return bytesRead;
      },write:function (stream, buffer, offset, length, position, canOwn) {
        if (length < 0 || position < 0) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        if ((stream.flags & 2097155) === 0) {
          throw new FS.ErrnoError(ERRNO_CODES.EBADF);
        }
        if (FS.isDir(stream.node.mode)) {
          throw new FS.ErrnoError(ERRNO_CODES.EISDIR);
        }
        if (!stream.stream_ops.write) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        var seeking = true;
        if (typeof position === 'undefined') {
          position = stream.position;
          seeking = false;
        } else if (!stream.seekable) {
          throw new FS.ErrnoError(ERRNO_CODES.ESPIPE);
        }
        if (stream.flags & 1024) {
          // seek to the end before writing in append mode
          FS.llseek(stream, 0, 2);
        }
        var bytesWritten = stream.stream_ops.write(stream, buffer, offset, length, position, canOwn);
        if (!seeking) stream.position += bytesWritten;
        return bytesWritten;
      },allocate:function (stream, offset, length) {
        if (offset < 0 || length <= 0) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        if ((stream.flags & 2097155) === 0) {
          throw new FS.ErrnoError(ERRNO_CODES.EBADF);
        }
        if (!FS.isFile(stream.node.mode) && !FS.isDir(node.mode)) {
          throw new FS.ErrnoError(ERRNO_CODES.ENODEV);
        }
        if (!stream.stream_ops.allocate) {
          throw new FS.ErrnoError(ERRNO_CODES.EOPNOTSUPP);
        }
        stream.stream_ops.allocate(stream, offset, length);
      },mmap:function (stream, buffer, offset, length, position, prot, flags) {
        // TODO if PROT is PROT_WRITE, make sure we have write access
        if ((stream.flags & 2097155) === 1) {
          throw new FS.ErrnoError(ERRNO_CODES.EACCES);
        }
        if (!stream.stream_ops.mmap) {
          throw new FS.ErrnoError(ERRNO_CODES.ENODEV);
        }
        return stream.stream_ops.mmap(stream, buffer, offset, length, position, prot, flags);
      },ioctl:function (stream, cmd, arg) {
        if (!stream.stream_ops.ioctl) {
          throw new FS.ErrnoError(ERRNO_CODES.ENOTTY);
        }
        return stream.stream_ops.ioctl(stream, cmd, arg);
      },readFile:function (path, opts) {
        opts = opts || {};
        opts.flags = opts.flags || 'r';
        opts.encoding = opts.encoding || 'binary';
        var ret;
        var stream = FS.open(path, opts.flags);
        var stat = FS.stat(path);
        var length = stat.size;
        var buf = new Uint8Array(length);
        FS.read(stream, buf, 0, length, 0);
        if (opts.encoding === 'utf8') {
          ret = '';
          var utf8 = new Runtime.UTF8Processor();
          for (var i = 0; i < length; i++) {
            ret += utf8.processCChar(buf[i]);
          }
        } else if (opts.encoding === 'binary') {
          ret = buf;
        } else {
          throw new Error('Invalid encoding type "' + opts.encoding + '"');
        }
        FS.close(stream);
        return ret;
      },writeFile:function (path, data, opts) {
        opts = opts || {};
        opts.flags = opts.flags || 'w';
        opts.encoding = opts.encoding || 'utf8';
        var stream = FS.open(path, opts.flags, opts.mode);
        if (opts.encoding === 'utf8') {
          var utf8 = new Runtime.UTF8Processor();
          var buf = new Uint8Array(utf8.processJSString(data));
          FS.write(stream, buf, 0, buf.length, 0);
        } else if (opts.encoding === 'binary') {
          FS.write(stream, data, 0, data.length, 0);
        } else {
          throw new Error('Invalid encoding type "' + opts.encoding + '"');
        }
        FS.close(stream);
      },cwd:function () {
        return FS.currentPath;
      },chdir:function (path) {
        var lookup = FS.lookupPath(path, { follow: true });
        if (!FS.isDir(lookup.node.mode)) {
          throw new FS.ErrnoError(ERRNO_CODES.ENOTDIR);
        }
        var err = FS.nodePermissions(lookup.node, 'x');
        if (err) {
          throw new FS.ErrnoError(err);
        }
        FS.currentPath = lookup.path;
      },createDefaultDirectories:function () {
        FS.mkdir('/tmp');
      },createDefaultDevices:function () {
        // create /dev
        FS.mkdir('/dev');
        // setup /dev/null
        FS.registerDevice(FS.makedev(1, 3), {
          read: function() { return 0; },
          write: function() { return 0; }
        });
        FS.mkdev('/dev/null', FS.makedev(1, 3));
        // setup /dev/tty and /dev/tty1
        // stderr needs to print output using Module['printErr']
        // so we register a second tty just for it.
        TTY.register(FS.makedev(5, 0), TTY.default_tty_ops);
        TTY.register(FS.makedev(6, 0), TTY.default_tty1_ops);
        FS.mkdev('/dev/tty', FS.makedev(5, 0));
        FS.mkdev('/dev/tty1', FS.makedev(6, 0));
        // we're not going to emulate the actual shm device,
        // just create the tmp dirs that reside in it commonly
        FS.mkdir('/dev/shm');
        FS.mkdir('/dev/shm/tmp');
      },createStandardStreams:function () {
        // TODO deprecate the old functionality of a single
        // input / output callback and that utilizes FS.createDevice
        // and instead require a unique set of stream ops
        // by default, we symlink the standard streams to the
        // default tty devices. however, if the standard streams
        // have been overwritten we create a unique device for
        // them instead.
        if (Module['stdin']) {
          FS.createDevice('/dev', 'stdin', Module['stdin']);
        } else {
          FS.symlink('/dev/tty', '/dev/stdin');
        }
        if (Module['stdout']) {
          FS.createDevice('/dev', 'stdout', null, Module['stdout']);
        } else {
          FS.symlink('/dev/tty', '/dev/stdout');
        }
        if (Module['stderr']) {
          FS.createDevice('/dev', 'stderr', null, Module['stderr']);
        } else {
          FS.symlink('/dev/tty1', '/dev/stderr');
        }
        // open default streams for the stdin, stdout and stderr devices
        var stdin = FS.open('/dev/stdin', 'r');
        HEAP32[((_stdin)>>2)]=stdin.fd;
        assert(stdin.fd === 1, 'invalid handle for stdin (' + stdin.fd + ')');
        var stdout = FS.open('/dev/stdout', 'w');
        HEAP32[((_stdout)>>2)]=stdout.fd;
        assert(stdout.fd === 2, 'invalid handle for stdout (' + stdout.fd + ')');
        var stderr = FS.open('/dev/stderr', 'w');
        HEAP32[((_stderr)>>2)]=stderr.fd;
        assert(stderr.fd === 3, 'invalid handle for stderr (' + stderr.fd + ')');
      },ensureErrnoError:function () {
        if (FS.ErrnoError) return;
        FS.ErrnoError = function ErrnoError(errno) {
          this.errno = errno;
          for (var key in ERRNO_CODES) {
            if (ERRNO_CODES[key] === errno) {
              this.code = key;
              break;
            }
          }
          this.message = ERRNO_MESSAGES[errno];
        };
        FS.ErrnoError.prototype = new Error();
        FS.ErrnoError.prototype.constructor = FS.ErrnoError;
        // Some errors may happen quite a bit, to avoid overhead we reuse them (and suffer a lack of stack info)
        [ERRNO_CODES.ENOENT].forEach(function(code) {
          FS.genericErrors[code] = new FS.ErrnoError(code);
          FS.genericErrors[code].stack = '<generic error, no stack>';
        });
      },staticInit:function () {
        FS.ensureErrnoError();
        FS.nameTable = new Array(4096);
        FS.root = FS.createNode(null, '/', 16384 | 0777, 0);
        FS.mount(MEMFS, {}, '/');
        FS.createDefaultDirectories();
        FS.createDefaultDevices();
      },init:function (input, output, error) {
        assert(!FS.init.initialized, 'FS.init was previously called. If you want to initialize later with custom parameters, remove any earlier calls (note that one is automatically added to the generated code)');
        FS.init.initialized = true;
        FS.ensureErrnoError();
        // Allow Module.stdin etc. to provide defaults, if none explicitly passed to us here
        Module['stdin'] = input || Module['stdin'];
        Module['stdout'] = output || Module['stdout'];
        Module['stderr'] = error || Module['stderr'];
        FS.createStandardStreams();
      },quit:function () {
        FS.init.initialized = false;
        for (var i = 0; i < FS.streams.length; i++) {
          var stream = FS.streams[i];
          if (!stream) {
            continue;
          }
          FS.close(stream);
        }
      },getMode:function (canRead, canWrite) {
        var mode = 0;
        if (canRead) mode |= 292 | 73;
        if (canWrite) mode |= 146;
        return mode;
      },joinPath:function (parts, forceRelative) {
        var path = PATH.join.apply(null, parts);
        if (forceRelative && path[0] == '/') path = path.substr(1);
        return path;
      },absolutePath:function (relative, base) {
        return PATH.resolve(base, relative);
      },standardizePath:function (path) {
        return PATH.normalize(path);
      },findObject:function (path, dontResolveLastLink) {
        var ret = FS.analyzePath(path, dontResolveLastLink);
        if (ret.exists) {
          return ret.object;
        } else {
          ___setErrNo(ret.error);
          return null;
        }
      },analyzePath:function (path, dontResolveLastLink) {
        // operate from within the context of the symlink's target
        try {
          var lookup = FS.lookupPath(path, { follow: !dontResolveLastLink });
          path = lookup.path;
        } catch (e) {
        }
        var ret = {
          isRoot: false, exists: false, error: 0, name: null, path: null, object: null,
          parentExists: false, parentPath: null, parentObject: null
        };
        try {
          var lookup = FS.lookupPath(path, { parent: true });
          ret.parentExists = true;
          ret.parentPath = lookup.path;
          ret.parentObject = lookup.node;
          ret.name = PATH.basename(path);
          lookup = FS.lookupPath(path, { follow: !dontResolveLastLink });
          ret.exists = true;
          ret.path = lookup.path;
          ret.object = lookup.node;
          ret.name = lookup.node.name;
          ret.isRoot = lookup.path === '/';
        } catch (e) {
          ret.error = e.errno;
        };
        return ret;
      },createFolder:function (parent, name, canRead, canWrite) {
        var path = PATH.join2(typeof parent === 'string' ? parent : FS.getPath(parent), name);
        var mode = FS.getMode(canRead, canWrite);
        return FS.mkdir(path, mode);
      },createPath:function (parent, path, canRead, canWrite) {
        parent = typeof parent === 'string' ? parent : FS.getPath(parent);
        var parts = path.split('/').reverse();
        while (parts.length) {
          var part = parts.pop();
          if (!part) continue;
          var current = PATH.join2(parent, part);
          try {
            FS.mkdir(current);
          } catch (e) {
            // ignore EEXIST
          }
          parent = current;
        }
        return current;
      },createFile:function (parent, name, properties, canRead, canWrite) {
        var path = PATH.join2(typeof parent === 'string' ? parent : FS.getPath(parent), name);
        var mode = FS.getMode(canRead, canWrite);
        return FS.create(path, mode);
      },createDataFile:function (parent, name, data, canRead, canWrite, canOwn) {
        var path = name ? PATH.join2(typeof parent === 'string' ? parent : FS.getPath(parent), name) : parent;
        var mode = FS.getMode(canRead, canWrite);
        var node = FS.create(path, mode);
        if (data) {
          if (typeof data === 'string') {
            var arr = new Array(data.length);
            for (var i = 0, len = data.length; i < len; ++i) arr[i] = data.charCodeAt(i);
            data = arr;
          }
          // make sure we can write to the file
          FS.chmod(node, mode | 146);
          var stream = FS.open(node, 'w');
          FS.write(stream, data, 0, data.length, 0, canOwn);
          FS.close(stream);
          FS.chmod(node, mode);
        }
        return node;
      },createDevice:function (parent, name, input, output) {
        var path = PATH.join2(typeof parent === 'string' ? parent : FS.getPath(parent), name);
        var mode = FS.getMode(!!input, !!output);
        if (!FS.createDevice.major) FS.createDevice.major = 64;
        var dev = FS.makedev(FS.createDevice.major++, 0);
        // Create a fake device that a set of stream ops to emulate
        // the old behavior.
        FS.registerDevice(dev, {
          open: function(stream) {
            stream.seekable = false;
          },
          close: function(stream) {
            // flush any pending line data
            if (output && output.buffer && output.buffer.length) {
              output(10);
            }
          },
          read: function(stream, buffer, offset, length, pos /* ignored */) {
            var bytesRead = 0;
            for (var i = 0; i < length; i++) {
              var result;
              try {
                result = input();
              } catch (e) {
                throw new FS.ErrnoError(ERRNO_CODES.EIO);
              }
              if (result === undefined && bytesRead === 0) {
                throw new FS.ErrnoError(ERRNO_CODES.EAGAIN);
              }
              if (result === null || result === undefined) break;
              bytesRead++;
              buffer[offset+i] = result;
            }
            if (bytesRead) {
              stream.node.timestamp = Date.now();
            }
            return bytesRead;
          },
          write: function(stream, buffer, offset, length, pos) {
            for (var i = 0; i < length; i++) {
              try {
                output(buffer[offset+i]);
              } catch (e) {
                throw new FS.ErrnoError(ERRNO_CODES.EIO);
              }
            }
            if (length) {
              stream.node.timestamp = Date.now();
            }
            return i;
          }
        });
        return FS.mkdev(path, mode, dev);
      },createLink:function (parent, name, target, canRead, canWrite) {
        var path = PATH.join2(typeof parent === 'string' ? parent : FS.getPath(parent), name);
        return FS.symlink(target, path);
      },forceLoadFile:function (obj) {
        if (obj.isDevice || obj.isFolder || obj.link || obj.contents) return true;
        var success = true;
        if (typeof XMLHttpRequest !== 'undefined') {
          throw new Error("Lazy loading should have been performed (contents set) in createLazyFile, but it was not. Lazy loading only works in web workers. Use --embed-file or --preload-file in emcc on the main thread.");
        } else if (Module['read']) {
          // Command-line.
          try {
            // WARNING: Can't read binary files in V8's d8 or tracemonkey's js, as
            //          read() will try to parse UTF8.
            obj.contents = intArrayFromString(Module['read'](obj.url), true);
          } catch (e) {
            success = false;
          }
        } else {
          throw new Error('Cannot load without read() or XMLHttpRequest.');
        }
        if (!success) ___setErrNo(ERRNO_CODES.EIO);
        return success;
      },createLazyFile:function (parent, name, url, canRead, canWrite) {
        if (typeof XMLHttpRequest !== 'undefined') {
          if (!ENVIRONMENT_IS_WORKER) throw 'Cannot do synchronous binary XHRs outside webworkers in modern browsers. Use --embed-file or --preload-file in emcc';
          // Lazy chunked Uint8Array (implements get and length from Uint8Array). Actual getting is abstracted away for eventual reuse.
          function LazyUint8Array() {
            this.lengthKnown = false;
            this.chunks = []; // Loaded chunks. Index is the chunk number
          }
          LazyUint8Array.prototype.get = function LazyUint8Array_get(idx) {
            if (idx > this.length-1 || idx < 0) {
              return undefined;
            }
            var chunkOffset = idx % this.chunkSize;
            var chunkNum = Math.floor(idx / this.chunkSize);
            return this.getter(chunkNum)[chunkOffset];
          }
          LazyUint8Array.prototype.setDataGetter = function LazyUint8Array_setDataGetter(getter) {
            this.getter = getter;
          }
          LazyUint8Array.prototype.cacheLength = function LazyUint8Array_cacheLength() {
              // Find length
              var xhr = new XMLHttpRequest();
              xhr.open('HEAD', url, false);
              xhr.send(null);
              if (!(xhr.status >= 200 && xhr.status < 300 || xhr.status === 304)) throw new Error("Couldn't load " + url + ". Status: " + xhr.status);
              var datalength = Number(xhr.getResponseHeader("Content-length"));
              var header;
              var hasByteServing = (header = xhr.getResponseHeader("Accept-Ranges")) && header === "bytes";
              var chunkSize = 1024*1024; // Chunk size in bytes
              if (!hasByteServing) chunkSize = datalength;
              // Function to get a range from the remote URL.
              var doXHR = (function(from, to) {
                if (from > to) throw new Error("invalid range (" + from + ", " + to + ") or no bytes requested!");
                if (to > datalength-1) throw new Error("only " + datalength + " bytes available! programmer error!");
                // TODO: Use mozResponseArrayBuffer, responseStream, etc. if available.
                var xhr = new XMLHttpRequest();
                xhr.open('GET', url, false);
                if (datalength !== chunkSize) xhr.setRequestHeader("Range", "bytes=" + from + "-" + to);
                // Some hints to the browser that we want binary data.
                if (typeof Uint8Array != 'undefined') xhr.responseType = 'arraybuffer';
                if (xhr.overrideMimeType) {
                  xhr.overrideMimeType('text/plain; charset=x-user-defined');
                }
                xhr.send(null);
                if (!(xhr.status >= 200 && xhr.status < 300 || xhr.status === 304)) throw new Error("Couldn't load " + url + ". Status: " + xhr.status);
                if (xhr.response !== undefined) {
                  return new Uint8Array(xhr.response || []);
                } else {
                  return intArrayFromString(xhr.responseText || '', true);
                }
              });
              var lazyArray = this;
              lazyArray.setDataGetter(function(chunkNum) {
                var start = chunkNum * chunkSize;
                var end = (chunkNum+1) * chunkSize - 1; // including this byte
                end = Math.min(end, datalength-1); // if datalength-1 is selected, this is the last block
                if (typeof(lazyArray.chunks[chunkNum]) === "undefined") {
                  lazyArray.chunks[chunkNum] = doXHR(start, end);
                }
                if (typeof(lazyArray.chunks[chunkNum]) === "undefined") throw new Error("doXHR failed!");
                return lazyArray.chunks[chunkNum];
              });
              this._length = datalength;
              this._chunkSize = chunkSize;
              this.lengthKnown = true;
          }
          var lazyArray = new LazyUint8Array();
          Object.defineProperty(lazyArray, "length", {
              get: function() {
                  if(!this.lengthKnown) {
                      this.cacheLength();
                  }
                  return this._length;
              }
          });
          Object.defineProperty(lazyArray, "chunkSize", {
              get: function() {
                  if(!this.lengthKnown) {
                      this.cacheLength();
                  }
                  return this._chunkSize;
              }
          });
          var properties = { isDevice: false, contents: lazyArray };
        } else {
          var properties = { isDevice: false, url: url };
        }
        var node = FS.createFile(parent, name, properties, canRead, canWrite);
        // This is a total hack, but I want to get this lazy file code out of the
        // core of MEMFS. If we want to keep this lazy file concept I feel it should
        // be its own thin LAZYFS proxying calls to MEMFS.
        if (properties.contents) {
          node.contents = properties.contents;
        } else if (properties.url) {
          node.contents = null;
          node.url = properties.url;
        }
        // override each stream op with one that tries to force load the lazy file first
        var stream_ops = {};
        var keys = Object.keys(node.stream_ops);
        keys.forEach(function(key) {
          var fn = node.stream_ops[key];
          stream_ops[key] = function forceLoadLazyFile() {
            if (!FS.forceLoadFile(node)) {
              throw new FS.ErrnoError(ERRNO_CODES.EIO);
            }
            return fn.apply(null, arguments);
          };
        });
        // use a custom read function
        stream_ops.read = function stream_ops_read(stream, buffer, offset, length, position) {
          if (!FS.forceLoadFile(node)) {
            throw new FS.ErrnoError(ERRNO_CODES.EIO);
          }
          var contents = stream.node.contents;
          if (position >= contents.length)
            return 0;
          var size = Math.min(contents.length - position, length);
          assert(size >= 0);
          if (contents.slice) { // normal array
            for (var i = 0; i < size; i++) {
              buffer[offset + i] = contents[position + i];
            }
          } else {
            for (var i = 0; i < size; i++) { // LazyUint8Array from sync binary XHR
              buffer[offset + i] = contents.get(position + i);
            }
          }
          return size;
        };
        node.stream_ops = stream_ops;
        return node;
      },createPreloadedFile:function (parent, name, url, canRead, canWrite, onload, onerror, dontCreateFile, canOwn) {
        Browser.init();
        // TODO we should allow people to just pass in a complete filename instead
        // of parent and name being that we just join them anyways
        var fullname = name ? PATH.resolve(PATH.join2(parent, name)) : parent;
        function processData(byteArray) {
          function finish(byteArray) {
            if (!dontCreateFile) {
              FS.createDataFile(parent, name, byteArray, canRead, canWrite, canOwn);
            }
            if (onload) onload();
            removeRunDependency('cp ' + fullname);
          }
          var handled = false;
          Module['preloadPlugins'].forEach(function(plugin) {
            if (handled) return;
            if (plugin['canHandle'](fullname)) {
              plugin['handle'](byteArray, fullname, finish, function() {
                if (onerror) onerror();
                removeRunDependency('cp ' + fullname);
              });
              handled = true;
            }
          });
          if (!handled) finish(byteArray);
        }
        addRunDependency('cp ' + fullname);
        if (typeof url == 'string') {
          Browser.asyncLoad(url, function(byteArray) {
            processData(byteArray);
          }, onerror);
        } else {
          processData(url);
        }
      },indexedDB:function () {
        return window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
      },DB_NAME:function () {
        return 'EM_FS_' + window.location.pathname;
      },DB_VERSION:20,DB_STORE_NAME:"FILE_DATA",saveFilesToDB:function (paths, onload, onerror) {
        onload = onload || function(){};
        onerror = onerror || function(){};
        var indexedDB = FS.indexedDB();
        try {
          var openRequest = indexedDB.open(FS.DB_NAME(), FS.DB_VERSION);
        } catch (e) {
          return onerror(e);
        }
        openRequest.onupgradeneeded = function openRequest_onupgradeneeded() {
          console.log('creating db');
          var db = openRequest.result;
          db.createObjectStore(FS.DB_STORE_NAME);
        };
        openRequest.onsuccess = function openRequest_onsuccess() {
          var db = openRequest.result;
          var transaction = db.transaction([FS.DB_STORE_NAME], 'readwrite');
          var files = transaction.objectStore(FS.DB_STORE_NAME);
          var ok = 0, fail = 0, total = paths.length;
          function finish() {
            if (fail == 0) onload(); else onerror();
          }
          paths.forEach(function(path) {
            var putRequest = files.put(FS.analyzePath(path).object.contents, path);
            putRequest.onsuccess = function putRequest_onsuccess() { ok++; if (ok + fail == total) finish() };
            putRequest.onerror = function putRequest_onerror() { fail++; if (ok + fail == total) finish() };
          });
          transaction.onerror = onerror;
        };
        openRequest.onerror = onerror;
      },loadFilesFromDB:function (paths, onload, onerror) {
        onload = onload || function(){};
        onerror = onerror || function(){};
        var indexedDB = FS.indexedDB();
        try {
          var openRequest = indexedDB.open(FS.DB_NAME(), FS.DB_VERSION);
        } catch (e) {
          return onerror(e);
        }
        openRequest.onupgradeneeded = onerror; // no database to load from
        openRequest.onsuccess = function openRequest_onsuccess() {
          var db = openRequest.result;
          try {
            var transaction = db.transaction([FS.DB_STORE_NAME], 'readonly');
          } catch(e) {
            onerror(e);
            return;
          }
          var files = transaction.objectStore(FS.DB_STORE_NAME);
          var ok = 0, fail = 0, total = paths.length;
          function finish() {
            if (fail == 0) onload(); else onerror();
          }
          paths.forEach(function(path) {
            var getRequest = files.get(path);
            getRequest.onsuccess = function getRequest_onsuccess() {
              if (FS.analyzePath(path).exists) {
                FS.unlink(path);
              }
              FS.createDataFile(PATH.dirname(path), PATH.basename(path), getRequest.result, true, true, true);
              ok++;
              if (ok + fail == total) finish();
            };
            getRequest.onerror = function getRequest_onerror() { fail++; if (ok + fail == total) finish() };
          });
          transaction.onerror = onerror;
        };
        openRequest.onerror = onerror;
      }};
  var _mkport=undefined;var SOCKFS={mount:function (mount) {
        return FS.createNode(null, '/', 16384 | 0777, 0);
      },createSocket:function (family, type, protocol) {
        var streaming = type == 1;
        if (protocol) {
          assert(streaming == (protocol == 6)); // if SOCK_STREAM, must be tcp
        }
        // create our internal socket structure
        var sock = {
          family: family,
          type: type,
          protocol: protocol,
          server: null,
          peers: {},
          pending: [],
          recv_queue: [],
          sock_ops: SOCKFS.websocket_sock_ops
        };
        // create the filesystem node to store the socket structure
        var name = SOCKFS.nextname();
        var node = FS.createNode(SOCKFS.root, name, 49152, 0);
        node.sock = sock;
        // and the wrapping stream that enables library functions such
        // as read and write to indirectly interact with the socket
        var stream = FS.createStream({
          path: name,
          node: node,
          flags: FS.modeStringToFlags('r+'),
          seekable: false,
          stream_ops: SOCKFS.stream_ops
        });
        // map the new stream to the socket structure (sockets have a 1:1
        // relationship with a stream)
        sock.stream = stream;
        return sock;
      },getSocket:function (fd) {
        var stream = FS.getStream(fd);
        if (!stream || !FS.isSocket(stream.node.mode)) {
          return null;
        }
        return stream.node.sock;
      },stream_ops:{poll:function (stream) {
          var sock = stream.node.sock;
          return sock.sock_ops.poll(sock);
        },ioctl:function (stream, request, varargs) {
          var sock = stream.node.sock;
          return sock.sock_ops.ioctl(sock, request, varargs);
        },read:function (stream, buffer, offset, length, position /* ignored */) {
          var sock = stream.node.sock;
          var msg = sock.sock_ops.recvmsg(sock, length);
          if (!msg) {
            // socket is closed
            return 0;
          }
          buffer.set(msg.buffer, offset);
          return msg.buffer.length;
        },write:function (stream, buffer, offset, length, position /* ignored */) {
          var sock = stream.node.sock;
          return sock.sock_ops.sendmsg(sock, buffer, offset, length);
        },close:function (stream) {
          var sock = stream.node.sock;
          sock.sock_ops.close(sock);
        }},nextname:function () {
        if (!SOCKFS.nextname.current) {
          SOCKFS.nextname.current = 0;
        }
        return 'socket[' + (SOCKFS.nextname.current++) + ']';
      },websocket_sock_ops:{createPeer:function (sock, addr, port) {
          var ws;
          if (typeof addr === 'object') {
            ws = addr;
            addr = null;
            port = null;
          }
          if (ws) {
            // for sockets that've already connected (e.g. we're the server)
            // we can inspect the _socket property for the address
            if (ws._socket) {
              addr = ws._socket.remoteAddress;
              port = ws._socket.remotePort;
            }
            // if we're just now initializing a connection to the remote,
            // inspect the url property
            else {
              var result = /ws[s]?:\/\/([^:]+):(\d+)/.exec(ws.url);
              if (!result) {
                throw new Error('WebSocket URL must be in the format ws(s)://address:port');
              }
              addr = result[1];
              port = parseInt(result[2], 10);
            }
          } else {
            // create the actual websocket object and connect
            try {
              var url = 'ws://' + addr + ':' + port;
              // the node ws library API is slightly different than the browser's
              var opts = ENVIRONMENT_IS_NODE ? {headers: {'websocket-protocol': ['binary']}} : ['binary'];
              // If node we use the ws library.
              var WebSocket = ENVIRONMENT_IS_NODE ? require('ws') : window['WebSocket'];
              ws = new WebSocket(url, opts);
              ws.binaryType = 'arraybuffer';
            } catch (e) {
              throw new FS.ErrnoError(ERRNO_CODES.EHOSTUNREACH);
            }
          }
          var peer = {
            addr: addr,
            port: port,
            socket: ws,
            dgram_send_queue: []
          };
          SOCKFS.websocket_sock_ops.addPeer(sock, peer);
          SOCKFS.websocket_sock_ops.handlePeerEvents(sock, peer);
          // if this is a bound dgram socket, send the port number first to allow
          // us to override the ephemeral port reported to us by remotePort on the
          // remote end.
          if (sock.type === 2 && typeof sock.sport !== 'undefined') {
            peer.dgram_send_queue.push(new Uint8Array([
                255, 255, 255, 255,
                'p'.charCodeAt(0), 'o'.charCodeAt(0), 'r'.charCodeAt(0), 't'.charCodeAt(0),
                ((sock.sport & 0xff00) >> 8) , (sock.sport & 0xff)
            ]));
          }
          return peer;
        },getPeer:function (sock, addr, port) {
          return sock.peers[addr + ':' + port];
        },addPeer:function (sock, peer) {
          sock.peers[peer.addr + ':' + peer.port] = peer;
        },removePeer:function (sock, peer) {
          delete sock.peers[peer.addr + ':' + peer.port];
        },handlePeerEvents:function (sock, peer) {
          var first = true;
          var handleOpen = function () {
            try {
              var queued = peer.dgram_send_queue.shift();
              while (queued) {
                peer.socket.send(queued);
                queued = peer.dgram_send_queue.shift();
              }
            } catch (e) {
              // not much we can do here in the way of proper error handling as we've already
              // lied and said this data was sent. shut it down.
              peer.socket.close();
            }
          };
          function handleMessage(data) {
            assert(typeof data !== 'string' && data.byteLength !== undefined);  // must receive an ArrayBuffer
            data = new Uint8Array(data);  // make a typed array view on the array buffer
            // if this is the port message, override the peer's port with it
            var wasfirst = first;
            first = false;
            if (wasfirst &&
                data.length === 10 &&
                data[0] === 255 && data[1] === 255 && data[2] === 255 && data[3] === 255 &&
                data[4] === 'p'.charCodeAt(0) && data[5] === 'o'.charCodeAt(0) && data[6] === 'r'.charCodeAt(0) && data[7] === 't'.charCodeAt(0)) {
              // update the peer's port and it's key in the peer map
              var newport = ((data[8] << 8) | data[9]);
              SOCKFS.websocket_sock_ops.removePeer(sock, peer);
              peer.port = newport;
              SOCKFS.websocket_sock_ops.addPeer(sock, peer);
              return;
            }
            sock.recv_queue.push({ addr: peer.addr, port: peer.port, data: data });
          };
          if (ENVIRONMENT_IS_NODE) {
            peer.socket.on('open', handleOpen);
            peer.socket.on('message', function(data, flags) {
              if (!flags.binary) {
                return;
              }
              handleMessage((new Uint8Array(data)).buffer);  // copy from node Buffer -> ArrayBuffer
            });
            peer.socket.on('error', function() {
              // don't throw
            });
          } else {
            peer.socket.onopen = handleOpen;
            peer.socket.onmessage = function peer_socket_onmessage(event) {
              handleMessage(event.data);
            };
          }
        },poll:function (sock) {
          if (sock.type === 1 && sock.server) {
            // listen sockets should only say they're available for reading
            // if there are pending clients.
            return sock.pending.length ? (64 | 1) : 0;
          }
          var mask = 0;
          var dest = sock.type === 1 ?  // we only care about the socket state for connection-based sockets
            SOCKFS.websocket_sock_ops.getPeer(sock, sock.daddr, sock.dport) :
            null;
          if (sock.recv_queue.length ||
              !dest ||  // connection-less sockets are always ready to read
              (dest && dest.socket.readyState === dest.socket.CLOSING) ||
              (dest && dest.socket.readyState === dest.socket.CLOSED)) {  // let recv return 0 once closed
            mask |= (64 | 1);
          }
          if (!dest ||  // connection-less sockets are always ready to write
              (dest && dest.socket.readyState === dest.socket.OPEN)) {
            mask |= 4;
          }
          if ((dest && dest.socket.readyState === dest.socket.CLOSING) ||
              (dest && dest.socket.readyState === dest.socket.CLOSED)) {
            mask |= 16;
          }
          return mask;
        },ioctl:function (sock, request, arg) {
          switch (request) {
            case 21531:
              var bytes = 0;
              if (sock.recv_queue.length) {
                bytes = sock.recv_queue[0].data.length;
              }
              HEAP32[((arg)>>2)]=bytes;
              return 0;
            default:
              return ERRNO_CODES.EINVAL;
          }
        },close:function (sock) {
          // if we've spawned a listen server, close it
          if (sock.server) {
            try {
              sock.server.close();
            } catch (e) {
            }
            sock.server = null;
          }
          // close any peer connections
          var peers = Object.keys(sock.peers);
          for (var i = 0; i < peers.length; i++) {
            var peer = sock.peers[peers[i]];
            try {
              peer.socket.close();
            } catch (e) {
            }
            SOCKFS.websocket_sock_ops.removePeer(sock, peer);
          }
          return 0;
        },bind:function (sock, addr, port) {
          if (typeof sock.saddr !== 'undefined' || typeof sock.sport !== 'undefined') {
            throw new FS.ErrnoError(ERRNO_CODES.EINVAL);  // already bound
          }
          sock.saddr = addr;
          sock.sport = port || _mkport();
          // in order to emulate dgram sockets, we need to launch a listen server when
          // binding on a connection-less socket
          // note: this is only required on the server side
          if (sock.type === 2) {
            // close the existing server if it exists
            if (sock.server) {
              sock.server.close();
              sock.server = null;
            }
            // swallow error operation not supported error that occurs when binding in the
            // browser where this isn't supported
            try {
              sock.sock_ops.listen(sock, 0);
            } catch (e) {
              if (!(e instanceof FS.ErrnoError)) throw e;
              if (e.errno !== ERRNO_CODES.EOPNOTSUPP) throw e;
            }
          }
        },connect:function (sock, addr, port) {
          if (sock.server) {
            throw new FS.ErrnoError(ERRNO_CODS.EOPNOTSUPP);
          }
          // TODO autobind
          // if (!sock.addr && sock.type == 2) {
          // }
          // early out if we're already connected / in the middle of connecting
          if (typeof sock.daddr !== 'undefined' && typeof sock.dport !== 'undefined') {
            var dest = SOCKFS.websocket_sock_ops.getPeer(sock, sock.daddr, sock.dport);
            if (dest) {
              if (dest.socket.readyState === dest.socket.CONNECTING) {
                throw new FS.ErrnoError(ERRNO_CODES.EALREADY);
              } else {
                throw new FS.ErrnoError(ERRNO_CODES.EISCONN);
              }
            }
          }
          // add the socket to our peer list and set our
          // destination address / port to match
          var peer = SOCKFS.websocket_sock_ops.createPeer(sock, addr, port);
          sock.daddr = peer.addr;
          sock.dport = peer.port;
          // always "fail" in non-blocking mode
          throw new FS.ErrnoError(ERRNO_CODES.EINPROGRESS);
        },listen:function (sock, backlog) {
          if (!ENVIRONMENT_IS_NODE) {
            throw new FS.ErrnoError(ERRNO_CODES.EOPNOTSUPP);
          }
          if (sock.server) {
             throw new FS.ErrnoError(ERRNO_CODES.EINVAL);  // already listening
          }
          var WebSocketServer = require('ws').Server;
          var host = sock.saddr;
          sock.server = new WebSocketServer({
            host: host,
            port: sock.sport
            // TODO support backlog
          });
          sock.server.on('connection', function(ws) {
            if (sock.type === 1) {
              var newsock = SOCKFS.createSocket(sock.family, sock.type, sock.protocol);
              // create a peer on the new socket
              var peer = SOCKFS.websocket_sock_ops.createPeer(newsock, ws);
              newsock.daddr = peer.addr;
              newsock.dport = peer.port;
              // push to queue for accept to pick up
              sock.pending.push(newsock);
            } else {
              // create a peer on the listen socket so calling sendto
              // with the listen socket and an address will resolve
              // to the correct client
              SOCKFS.websocket_sock_ops.createPeer(sock, ws);
            }
          });
          sock.server.on('closed', function() {
            sock.server = null;
          });
          sock.server.on('error', function() {
            // don't throw
          });
        },accept:function (listensock) {
          if (!listensock.server) {
            throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
          }
          var newsock = listensock.pending.shift();
          newsock.stream.flags = listensock.stream.flags;
          return newsock;
        },getname:function (sock, peer) {
          var addr, port;
          if (peer) {
            if (sock.daddr === undefined || sock.dport === undefined) {
              throw new FS.ErrnoError(ERRNO_CODES.ENOTCONN);
            }
            addr = sock.daddr;
            port = sock.dport;
          } else {
            // TODO saddr and sport will be set for bind()'d UDP sockets, but what
            // should we be returning for TCP sockets that've been connect()'d?
            addr = sock.saddr || 0;
            port = sock.sport || 0;
          }
          return { addr: addr, port: port };
        },sendmsg:function (sock, buffer, offset, length, addr, port) {
          if (sock.type === 2) {
            // connection-less sockets will honor the message address,
            // and otherwise fall back to the bound destination address
            if (addr === undefined || port === undefined) {
              addr = sock.daddr;
              port = sock.dport;
            }
            // if there was no address to fall back to, error out
            if (addr === undefined || port === undefined) {
              throw new FS.ErrnoError(ERRNO_CODES.EDESTADDRREQ);
            }
          } else {
            // connection-based sockets will only use the bound
            addr = sock.daddr;
            port = sock.dport;
          }
          // find the peer for the destination address
          var dest = SOCKFS.websocket_sock_ops.getPeer(sock, addr, port);
          // early out if not connected with a connection-based socket
          if (sock.type === 1) {
            if (!dest || dest.socket.readyState === dest.socket.CLOSING || dest.socket.readyState === dest.socket.CLOSED) {
              throw new FS.ErrnoError(ERRNO_CODES.ENOTCONN);
            } else if (dest.socket.readyState === dest.socket.CONNECTING) {
              throw new FS.ErrnoError(ERRNO_CODES.EAGAIN);
            }
          }
          // create a copy of the incoming data to send, as the WebSocket API
          // doesn't work entirely with an ArrayBufferView, it'll just send
          // the entire underlying buffer
          var data;
          if (buffer instanceof Array || buffer instanceof ArrayBuffer) {
            data = buffer.slice(offset, offset + length);
          } else {  // ArrayBufferView
            data = buffer.buffer.slice(buffer.byteOffset + offset, buffer.byteOffset + offset + length);
          }
          // if we're emulating a connection-less dgram socket and don't have
          // a cached connection, queue the buffer to send upon connect and
          // lie, saying the data was sent now.
          if (sock.type === 2) {
            if (!dest || dest.socket.readyState !== dest.socket.OPEN) {
              // if we're not connected, open a new connection
              if (!dest || dest.socket.readyState === dest.socket.CLOSING || dest.socket.readyState === dest.socket.CLOSED) {
                dest = SOCKFS.websocket_sock_ops.createPeer(sock, addr, port);
              }
              dest.dgram_send_queue.push(data);
              return length;
            }
          }
          try {
            // send the actual data
            dest.socket.send(data);
            return length;
          } catch (e) {
            throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
          }
        },recvmsg:function (sock, length) {
          // http://pubs.opengroup.org/onlinepubs/7908799/xns/recvmsg.html
          if (sock.type === 1 && sock.server) {
            // tcp servers should not be recv()'ing on the listen socket
            throw new FS.ErrnoError(ERRNO_CODES.ENOTCONN);
          }
          var queued = sock.recv_queue.shift();
          if (!queued) {
            if (sock.type === 1) {
              var dest = SOCKFS.websocket_sock_ops.getPeer(sock, sock.daddr, sock.dport);
              if (!dest) {
                // if we have a destination address but are not connected, error out
                throw new FS.ErrnoError(ERRNO_CODES.ENOTCONN);
              }
              else if (dest.socket.readyState === dest.socket.CLOSING || dest.socket.readyState === dest.socket.CLOSED) {
                // return null if the socket has closed
                return null;
              }
              else {
                // else, our socket is in a valid state but truly has nothing available
                throw new FS.ErrnoError(ERRNO_CODES.EAGAIN);
              }
            } else {
              throw new FS.ErrnoError(ERRNO_CODES.EAGAIN);
            }
          }
          // queued.data will be an ArrayBuffer if it's unadulterated, but if it's
          // requeued TCP data it'll be an ArrayBufferView
          var queuedLength = queued.data.byteLength || queued.data.length;
          var queuedOffset = queued.data.byteOffset || 0;
          var queuedBuffer = queued.data.buffer || queued.data;
          var bytesRead = Math.min(length, queuedLength);
          var res = {
            buffer: new Uint8Array(queuedBuffer, queuedOffset, bytesRead),
            addr: queued.addr,
            port: queued.port
          };
          // push back any unread data for TCP connections
          if (sock.type === 1 && bytesRead < queuedLength) {
            var bytesRemaining = queuedLength - bytesRead;
            queued.data = new Uint8Array(queuedBuffer, queuedOffset + bytesRead, bytesRemaining);
            sock.recv_queue.unshift(queued);
          }
          return res;
        }}};function _send(fd, buf, len, flags) {
      var sock = SOCKFS.getSocket(fd);
      if (!sock) {
        ___setErrNo(ERRNO_CODES.EBADF);
        return -1;
      }
      // TODO honor flags
      return _write(fd, buf, len);
    }
  function _pwrite(fildes, buf, nbyte, offset) {
      // ssize_t pwrite(int fildes, const void *buf, size_t nbyte, off_t offset);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/write.html
      var stream = FS.getStream(fildes);
      if (!stream) {
        ___setErrNo(ERRNO_CODES.EBADF);
        return -1;
      }
      try {
        var slab = HEAP8;
        return FS.write(stream, slab, buf, nbyte, offset);
      } catch (e) {
        FS.handleFSError(e);
        return -1;
      }
    }function _write(fildes, buf, nbyte) {
      // ssize_t write(int fildes, const void *buf, size_t nbyte);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/write.html
      var stream = FS.getStream(fildes);
      if (!stream) {
        ___setErrNo(ERRNO_CODES.EBADF);
        return -1;
      }
      try {
        var slab = HEAP8;
        return FS.write(stream, slab, buf, nbyte);
      } catch (e) {
        FS.handleFSError(e);
        return -1;
      }
    }function _fwrite(ptr, size, nitems, stream) {
      // size_t fwrite(const void *restrict ptr, size_t size, size_t nitems, FILE *restrict stream);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/fwrite.html
      var bytesToWrite = nitems * size;
      if (bytesToWrite == 0) return 0;
      var bytesWritten = _write(stream, ptr, bytesToWrite);
      if (bytesWritten == -1) {
        var streamObj = FS.getStream(stream);
        if (streamObj) streamObj.error = true;
        return 0;
      } else {
        return Math.floor(bytesWritten / size);
      }
    }
  Module["_strlen"] = _strlen;
  function __reallyNegative(x) {
      return x < 0 || (x === 0 && (1/x) === -Infinity);
    }function __formatString(format, varargs) {
      var textIndex = format;
      var argIndex = 0;
      function getNextArg(type) {
        // NOTE: Explicitly ignoring type safety. Otherwise this fails:
        //       int x = 4; printf("%c\n", (char)x);
        var ret;
        if (type === 'double') {
          ret = HEAPF64[(((varargs)+(argIndex))>>3)];
        } else if (type == 'i64') {
          ret = [HEAP32[(((varargs)+(argIndex))>>2)],
                 HEAP32[(((varargs)+(argIndex+8))>>2)]];
          argIndex += 8; // each 32-bit chunk is in a 64-bit block
        } else {
          type = 'i32'; // varargs are always i32, i64, or double
          ret = HEAP32[(((varargs)+(argIndex))>>2)];
        }
        argIndex += Math.max(Runtime.getNativeFieldSize(type), Runtime.getAlignSize(type, null, true));
        return ret;
      }
      var ret = [];
      var curr, next, currArg;
      while(1) {
        var startTextIndex = textIndex;
        curr = HEAP8[(textIndex)];
        if (curr === 0) break;
        next = HEAP8[((textIndex+1)|0)];
        if (curr == 37) {
          // Handle flags.
          var flagAlwaysSigned = false;
          var flagLeftAlign = false;
          var flagAlternative = false;
          var flagZeroPad = false;
          var flagPadSign = false;
          flagsLoop: while (1) {
            switch (next) {
              case 43:
                flagAlwaysSigned = true;
                break;
              case 45:
                flagLeftAlign = true;
                break;
              case 35:
                flagAlternative = true;
                break;
              case 48:
                if (flagZeroPad) {
                  break flagsLoop;
                } else {
                  flagZeroPad = true;
                  break;
                }
              case 32:
                flagPadSign = true;
                break;
              default:
                break flagsLoop;
            }
            textIndex++;
            next = HEAP8[((textIndex+1)|0)];
          }
          // Handle width.
          var width = 0;
          if (next == 42) {
            width = getNextArg('i32');
            textIndex++;
            next = HEAP8[((textIndex+1)|0)];
          } else {
            while (next >= 48 && next <= 57) {
              width = width * 10 + (next - 48);
              textIndex++;
              next = HEAP8[((textIndex+1)|0)];
            }
          }
          // Handle precision.
          var precisionSet = false, precision = -1;
          if (next == 46) {
            precision = 0;
            precisionSet = true;
            textIndex++;
            next = HEAP8[((textIndex+1)|0)];
            if (next == 42) {
              precision = getNextArg('i32');
              textIndex++;
            } else {
              while(1) {
                var precisionChr = HEAP8[((textIndex+1)|0)];
                if (precisionChr < 48 ||
                    precisionChr > 57) break;
                precision = precision * 10 + (precisionChr - 48);
                textIndex++;
              }
            }
            next = HEAP8[((textIndex+1)|0)];
          }
          if (precision === -1) {
            precision = 6; // Standard default.
            precisionSet = false;
          }
          // Handle integer sizes. WARNING: These assume a 32-bit architecture!
          var argSize;
          switch (String.fromCharCode(next)) {
            case 'h':
              var nextNext = HEAP8[((textIndex+2)|0)];
              if (nextNext == 104) {
                textIndex++;
                argSize = 1; // char (actually i32 in varargs)
              } else {
                argSize = 2; // short (actually i32 in varargs)
              }
              break;
            case 'l':
              var nextNext = HEAP8[((textIndex+2)|0)];
              if (nextNext == 108) {
                textIndex++;
                argSize = 8; // long long
              } else {
                argSize = 4; // long
              }
              break;
            case 'L': // long long
            case 'q': // int64_t
            case 'j': // intmax_t
              argSize = 8;
              break;
            case 'z': // size_t
            case 't': // ptrdiff_t
            case 'I': // signed ptrdiff_t or unsigned size_t
              argSize = 4;
              break;
            default:
              argSize = null;
          }
          if (argSize) textIndex++;
          next = HEAP8[((textIndex+1)|0)];
          // Handle type specifier.
          switch (String.fromCharCode(next)) {
            case 'd': case 'i': case 'u': case 'o': case 'x': case 'X': case 'p': {
              // Integer.
              var signed = next == 100 || next == 105;
              argSize = argSize || 4;
              var currArg = getNextArg('i' + (argSize * 8));
              var origArg = currArg;
              var argText;
              // Flatten i64-1 [low, high] into a (slightly rounded) double
              if (argSize == 8) {
                currArg = Runtime.makeBigInt(currArg[0], currArg[1], next == 117);
              }
              // Truncate to requested size.
              if (argSize <= 4) {
                var limit = Math.pow(256, argSize) - 1;
                currArg = (signed ? reSign : unSign)(currArg & limit, argSize * 8);
              }
              // Format the number.
              var currAbsArg = Math.abs(currArg);
              var prefix = '';
              if (next == 100 || next == 105) {
                if (argSize == 8 && i64Math) argText = i64Math.stringify(origArg[0], origArg[1], null); else
                argText = reSign(currArg, 8 * argSize, 1).toString(10);
              } else if (next == 117) {
                if (argSize == 8 && i64Math) argText = i64Math.stringify(origArg[0], origArg[1], true); else
                argText = unSign(currArg, 8 * argSize, 1).toString(10);
                currArg = Math.abs(currArg);
              } else if (next == 111) {
                argText = (flagAlternative ? '0' : '') + currAbsArg.toString(8);
              } else if (next == 120 || next == 88) {
                prefix = (flagAlternative && currArg != 0) ? '0x' : '';
                if (argSize == 8 && i64Math) {
                  if (origArg[1]) {
                    argText = (origArg[1]>>>0).toString(16);
                    var lower = (origArg[0]>>>0).toString(16);
                    while (lower.length < 8) lower = '0' + lower;
                    argText += lower;
                  } else {
                    argText = (origArg[0]>>>0).toString(16);
                  }
                } else
                if (currArg < 0) {
                  // Represent negative numbers in hex as 2's complement.
                  currArg = -currArg;
                  argText = (currAbsArg - 1).toString(16);
                  var buffer = [];
                  for (var i = 0; i < argText.length; i++) {
                    buffer.push((0xF - parseInt(argText[i], 16)).toString(16));
                  }
                  argText = buffer.join('');
                  while (argText.length < argSize * 2) argText = 'f' + argText;
                } else {
                  argText = currAbsArg.toString(16);
                }
                if (next == 88) {
                  prefix = prefix.toUpperCase();
                  argText = argText.toUpperCase();
                }
              } else if (next == 112) {
                if (currAbsArg === 0) {
                  argText = '(nil)';
                } else {
                  prefix = '0x';
                  argText = currAbsArg.toString(16);
                }
              }
              if (precisionSet) {
                while (argText.length < precision) {
                  argText = '0' + argText;
                }
              }
              // Add sign if needed
              if (currArg >= 0) {
                if (flagAlwaysSigned) {
                  prefix = '+' + prefix;
                } else if (flagPadSign) {
                  prefix = ' ' + prefix;
                }
              }
              // Move sign to prefix so we zero-pad after the sign
              if (argText.charAt(0) == '-') {
                prefix = '-' + prefix;
                argText = argText.substr(1);
              }
              // Add padding.
              while (prefix.length + argText.length < width) {
                if (flagLeftAlign) {
                  argText += ' ';
                } else {
                  if (flagZeroPad) {
                    argText = '0' + argText;
                  } else {
                    prefix = ' ' + prefix;
                  }
                }
              }
              // Insert the result into the buffer.
              argText = prefix + argText;
              argText.split('').forEach(function(chr) {
                ret.push(chr.charCodeAt(0));
              });
              break;
            }
            case 'f': case 'F': case 'e': case 'E': case 'g': case 'G': {
              // Float.
              var currArg = getNextArg('double');
              var argText;
              if (isNaN(currArg)) {
                argText = 'nan';
                flagZeroPad = false;
              } else if (!isFinite(currArg)) {
                argText = (currArg < 0 ? '-' : '') + 'inf';
                flagZeroPad = false;
              } else {
                var isGeneral = false;
                var effectivePrecision = Math.min(precision, 20);
                // Convert g/G to f/F or e/E, as per:
                // http://pubs.opengroup.org/onlinepubs/9699919799/functions/printf.html
                if (next == 103 || next == 71) {
                  isGeneral = true;
                  precision = precision || 1;
                  var exponent = parseInt(currArg.toExponential(effectivePrecision).split('e')[1], 10);
                  if (precision > exponent && exponent >= -4) {
                    next = ((next == 103) ? 'f' : 'F').charCodeAt(0);
                    precision -= exponent + 1;
                  } else {
                    next = ((next == 103) ? 'e' : 'E').charCodeAt(0);
                    precision--;
                  }
                  effectivePrecision = Math.min(precision, 20);
                }
                if (next == 101 || next == 69) {
                  argText = currArg.toExponential(effectivePrecision);
                  // Make sure the exponent has at least 2 digits.
                  if (/[eE][-+]\d$/.test(argText)) {
                    argText = argText.slice(0, -1) + '0' + argText.slice(-1);
                  }
                } else if (next == 102 || next == 70) {
                  argText = currArg.toFixed(effectivePrecision);
                  if (currArg === 0 && __reallyNegative(currArg)) {
                    argText = '-' + argText;
                  }
                }
                var parts = argText.split('e');
                if (isGeneral && !flagAlternative) {
                  // Discard trailing zeros and periods.
                  while (parts[0].length > 1 && parts[0].indexOf('.') != -1 &&
                         (parts[0].slice(-1) == '0' || parts[0].slice(-1) == '.')) {
                    parts[0] = parts[0].slice(0, -1);
                  }
                } else {
                  // Make sure we have a period in alternative mode.
                  if (flagAlternative && argText.indexOf('.') == -1) parts[0] += '.';
                  // Zero pad until required precision.
                  while (precision > effectivePrecision++) parts[0] += '0';
                }
                argText = parts[0] + (parts.length > 1 ? 'e' + parts[1] : '');
                // Capitalize 'E' if needed.
                if (next == 69) argText = argText.toUpperCase();
                // Add sign.
                if (currArg >= 0) {
                  if (flagAlwaysSigned) {
                    argText = '+' + argText;
                  } else if (flagPadSign) {
                    argText = ' ' + argText;
                  }
                }
              }
              // Add padding.
              while (argText.length < width) {
                if (flagLeftAlign) {
                  argText += ' ';
                } else {
                  if (flagZeroPad && (argText[0] == '-' || argText[0] == '+')) {
                    argText = argText[0] + '0' + argText.slice(1);
                  } else {
                    argText = (flagZeroPad ? '0' : ' ') + argText;
                  }
                }
              }
              // Adjust case.
              if (next < 97) argText = argText.toUpperCase();
              // Insert the result into the buffer.
              argText.split('').forEach(function(chr) {
                ret.push(chr.charCodeAt(0));
              });
              break;
            }
            case 's': {
              // String.
              var arg = getNextArg('i8*');
              var argLength = arg ? _strlen(arg) : '(null)'.length;
              if (precisionSet) argLength = Math.min(argLength, precision);
              if (!flagLeftAlign) {
                while (argLength < width--) {
                  ret.push(32);
                }
              }
              if (arg) {
                for (var i = 0; i < argLength; i++) {
                  ret.push(HEAPU8[((arg++)|0)]);
                }
              } else {
                ret = ret.concat(intArrayFromString('(null)'.substr(0, argLength), true));
              }
              if (flagLeftAlign) {
                while (argLength < width--) {
                  ret.push(32);
                }
              }
              break;
            }
            case 'c': {
              // Character.
              if (flagLeftAlign) ret.push(getNextArg('i8'));
              while (--width > 0) {
                ret.push(32);
              }
              if (!flagLeftAlign) ret.push(getNextArg('i8'));
              break;
            }
            case 'n': {
              // Write the length written so far to the next parameter.
              var ptr = getNextArg('i32*');
              HEAP32[((ptr)>>2)]=ret.length
              break;
            }
            case '%': {
              // Literal percent sign.
              ret.push(curr);
              break;
            }
            default: {
              // Unknown specifiers remain untouched.
              for (var i = startTextIndex; i < textIndex + 2; i++) {
                ret.push(HEAP8[(i)]);
              }
            }
          }
          textIndex += 2;
          // TODO: Support a/A (hex float) and m (last error) specifiers.
          // TODO: Support %1${specifier} for arg selection.
        } else {
          ret.push(curr);
          textIndex += 1;
        }
      }
      return ret;
    }function _fprintf(stream, format, varargs) {
      // int fprintf(FILE *restrict stream, const char *restrict format, ...);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/printf.html
      var result = __formatString(format, varargs);
      var stack = Runtime.stackSave();
      var ret = _fwrite(allocate(result, 'i8', ALLOC_STACK), 1, result.length, stream);
      Runtime.stackRestore(stack);
      return ret;
    }
  function __exit(status) {
      // void _exit(int status);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/exit.html
      Module['exit'](status);
    }function _exit(status) {
      __exit(status);
    }
  var Browser={mainLoop:{scheduler:null,shouldPause:false,paused:false,queue:[],pause:function () {
          Browser.mainLoop.shouldPause = true;
        },resume:function () {
          if (Browser.mainLoop.paused) {
            Browser.mainLoop.paused = false;
            Browser.mainLoop.scheduler();
          }
          Browser.mainLoop.shouldPause = false;
        },updateStatus:function () {
          if (Module['setStatus']) {
            var message = Module['statusMessage'] || 'Please wait...';
            var remaining = Browser.mainLoop.remainingBlockers;
            var expected = Browser.mainLoop.expectedBlockers;
            if (remaining) {
              if (remaining < expected) {
                Module['setStatus'](message + ' (' + (expected - remaining) + '/' + expected + ')');
              } else {
                Module['setStatus'](message);
              }
            } else {
              Module['setStatus']('');
            }
          }
        }},isFullScreen:false,pointerLock:false,moduleContextCreatedCallbacks:[],workers:[],init:function () {
        if (!Module["preloadPlugins"]) Module["preloadPlugins"] = []; // needs to exist even in workers
        if (Browser.initted || ENVIRONMENT_IS_WORKER) return;
        Browser.initted = true;
        try {
          new Blob();
          Browser.hasBlobConstructor = true;
        } catch(e) {
          Browser.hasBlobConstructor = false;
          console.log("warning: no blob constructor, cannot create blobs with mimetypes");
        }
        Browser.BlobBuilder = typeof MozBlobBuilder != "undefined" ? MozBlobBuilder : (typeof WebKitBlobBuilder != "undefined" ? WebKitBlobBuilder : (!Browser.hasBlobConstructor ? console.log("warning: no BlobBuilder") : null));
        Browser.URLObject = typeof window != "undefined" ? (window.URL ? window.URL : window.webkitURL) : undefined;
        if (!Module.noImageDecoding && typeof Browser.URLObject === 'undefined') {
          console.log("warning: Browser does not support creating object URLs. Built-in browser image decoding will not be available.");
          Module.noImageDecoding = true;
        }
        // Support for plugins that can process preloaded files. You can add more of these to
        // your app by creating and appending to Module.preloadPlugins.
        //
        // Each plugin is asked if it can handle a file based on the file's name. If it can,
        // it is given the file's raw data. When it is done, it calls a callback with the file's
        // (possibly modified) data. For example, a plugin might decompress a file, or it
        // might create some side data structure for use later (like an Image element, etc.).
        var imagePlugin = {};
        imagePlugin['canHandle'] = function imagePlugin_canHandle(name) {
          return !Module.noImageDecoding && /\.(jpg|jpeg|png|bmp)$/i.test(name);
        };
        imagePlugin['handle'] = function imagePlugin_handle(byteArray, name, onload, onerror) {
          var b = null;
          if (Browser.hasBlobConstructor) {
            try {
              b = new Blob([byteArray], { type: Browser.getMimetype(name) });
              if (b.size !== byteArray.length) { // Safari bug #118630
                // Safari's Blob can only take an ArrayBuffer
                b = new Blob([(new Uint8Array(byteArray)).buffer], { type: Browser.getMimetype(name) });
              }
            } catch(e) {
              Runtime.warnOnce('Blob constructor present but fails: ' + e + '; falling back to blob builder');
            }
          }
          if (!b) {
            var bb = new Browser.BlobBuilder();
            bb.append((new Uint8Array(byteArray)).buffer); // we need to pass a buffer, and must copy the array to get the right data range
            b = bb.getBlob();
          }
          var url = Browser.URLObject.createObjectURL(b);
          var img = new Image();
          img.onload = function img_onload() {
            assert(img.complete, 'Image ' + name + ' could not be decoded');
            var canvas = document.createElement('canvas');
            canvas.width = img.width;
            canvas.height = img.height;
            var ctx = canvas.getContext('2d');
            ctx.drawImage(img, 0, 0);
            Module["preloadedImages"][name] = canvas;
            Browser.URLObject.revokeObjectURL(url);
            if (onload) onload(byteArray);
          };
          img.onerror = function img_onerror(event) {
            console.log('Image ' + url + ' could not be decoded');
            if (onerror) onerror();
          };
          img.src = url;
        };
        Module['preloadPlugins'].push(imagePlugin);
        var audioPlugin = {};
        audioPlugin['canHandle'] = function audioPlugin_canHandle(name) {
          return !Module.noAudioDecoding && name.substr(-4) in { '.ogg': 1, '.wav': 1, '.mp3': 1 };
        };
        audioPlugin['handle'] = function audioPlugin_handle(byteArray, name, onload, onerror) {
          var done = false;
          function finish(audio) {
            if (done) return;
            done = true;
            Module["preloadedAudios"][name] = audio;
            if (onload) onload(byteArray);
          }
          function fail() {
            if (done) return;
            done = true;
            Module["preloadedAudios"][name] = new Audio(); // empty shim
            if (onerror) onerror();
          }
          if (Browser.hasBlobConstructor) {
            try {
              var b = new Blob([byteArray], { type: Browser.getMimetype(name) });
            } catch(e) {
              return fail();
            }
            var url = Browser.URLObject.createObjectURL(b); // XXX we never revoke this!
            var audio = new Audio();
            audio.addEventListener('canplaythrough', function() { finish(audio) }, false); // use addEventListener due to chromium bug 124926
            audio.onerror = function audio_onerror(event) {
              if (done) return;
              console.log('warning: browser could not fully decode audio ' + name + ', trying slower base64 approach');
              function encode64(data) {
                var BASE = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
                var PAD = '=';
                var ret = '';
                var leftchar = 0;
                var leftbits = 0;
                for (var i = 0; i < data.length; i++) {
                  leftchar = (leftchar << 8) | data[i];
                  leftbits += 8;
                  while (leftbits >= 6) {
                    var curr = (leftchar >> (leftbits-6)) & 0x3f;
                    leftbits -= 6;
                    ret += BASE[curr];
                  }
                }
                if (leftbits == 2) {
                  ret += BASE[(leftchar&3) << 4];
                  ret += PAD + PAD;
                } else if (leftbits == 4) {
                  ret += BASE[(leftchar&0xf) << 2];
                  ret += PAD;
                }
                return ret;
              }
              audio.src = 'data:audio/x-' + name.substr(-3) + ';base64,' + encode64(byteArray);
              finish(audio); // we don't wait for confirmation this worked - but it's worth trying
            };
            audio.src = url;
            // workaround for chrome bug 124926 - we do not always get oncanplaythrough or onerror
            Browser.safeSetTimeout(function() {
              finish(audio); // try to use it even though it is not necessarily ready to play
            }, 10000);
          } else {
            return fail();
          }
        };
        Module['preloadPlugins'].push(audioPlugin);
        // Canvas event setup
        var canvas = Module['canvas'];
        canvas.requestPointerLock = canvas['requestPointerLock'] ||
                                    canvas['mozRequestPointerLock'] ||
                                    canvas['webkitRequestPointerLock'];
        canvas.exitPointerLock = document['exitPointerLock'] ||
                                 document['mozExitPointerLock'] ||
                                 document['webkitExitPointerLock'] ||
                                 function(){}; // no-op if function does not exist
        canvas.exitPointerLock = canvas.exitPointerLock.bind(document);
        function pointerLockChange() {
          Browser.pointerLock = document['pointerLockElement'] === canvas ||
                                document['mozPointerLockElement'] === canvas ||
                                document['webkitPointerLockElement'] === canvas;
        }
        document.addEventListener('pointerlockchange', pointerLockChange, false);
        document.addEventListener('mozpointerlockchange', pointerLockChange, false);
        document.addEventListener('webkitpointerlockchange', pointerLockChange, false);
        if (Module['elementPointerLock']) {
          canvas.addEventListener("click", function(ev) {
            if (!Browser.pointerLock && canvas.requestPointerLock) {
              canvas.requestPointerLock();
              ev.preventDefault();
            }
          }, false);
        }
      },createContext:function (canvas, useWebGL, setInModule, webGLContextAttributes) {
        var ctx;
        try {
          if (useWebGL) {
            var contextAttributes = {
              antialias: false,
              alpha: false
            };
            if (webGLContextAttributes) {
              for (var attribute in webGLContextAttributes) {
                contextAttributes[attribute] = webGLContextAttributes[attribute];
              }
            }
            var errorInfo = '?';
            function onContextCreationError(event) {
              errorInfo = event.statusMessage || errorInfo;
            }
            canvas.addEventListener('webglcontextcreationerror', onContextCreationError, false);
            try {
              ['experimental-webgl', 'webgl'].some(function(webglId) {
                return ctx = canvas.getContext(webglId, contextAttributes);
              });
            } finally {
              canvas.removeEventListener('webglcontextcreationerror', onContextCreationError, false);
            }
          } else {
            ctx = canvas.getContext('2d');
          }
          if (!ctx) throw ':(';
        } catch (e) {
          Module.print('Could not create canvas: ' + [errorInfo, e]);
          return null;
        }
        if (useWebGL) {
          // Set the background of the WebGL canvas to black
          canvas.style.backgroundColor = "black";
          // Warn on context loss
          canvas.addEventListener('webglcontextlost', function(event) {
            alert('WebGL context lost. You will need to reload the page.');
          }, false);
        }
        if (setInModule) {
          GLctx = Module.ctx = ctx;
          Module.useWebGL = useWebGL;
          Browser.moduleContextCreatedCallbacks.forEach(function(callback) { callback() });
          Browser.init();
        }
        return ctx;
      },destroyContext:function (canvas, useWebGL, setInModule) {},fullScreenHandlersInstalled:false,lockPointer:undefined,resizeCanvas:undefined,requestFullScreen:function (lockPointer, resizeCanvas) {
        Browser.lockPointer = lockPointer;
        Browser.resizeCanvas = resizeCanvas;
        if (typeof Browser.lockPointer === 'undefined') Browser.lockPointer = true;
        if (typeof Browser.resizeCanvas === 'undefined') Browser.resizeCanvas = false;
        var canvas = Module['canvas'];
        function fullScreenChange() {
          Browser.isFullScreen = false;
          if ((document['webkitFullScreenElement'] || document['webkitFullscreenElement'] ||
               document['mozFullScreenElement'] || document['mozFullscreenElement'] ||
               document['fullScreenElement'] || document['fullscreenElement']) === canvas) {
            canvas.cancelFullScreen = document['cancelFullScreen'] ||
                                      document['mozCancelFullScreen'] ||
                                      document['webkitCancelFullScreen'];
            canvas.cancelFullScreen = canvas.cancelFullScreen.bind(document);
            if (Browser.lockPointer) canvas.requestPointerLock();
            Browser.isFullScreen = true;
            if (Browser.resizeCanvas) Browser.setFullScreenCanvasSize();
          } else if (Browser.resizeCanvas){
            Browser.setWindowedCanvasSize();
          }
          if (Module['onFullScreen']) Module['onFullScreen'](Browser.isFullScreen);
        }
        if (!Browser.fullScreenHandlersInstalled) {
          Browser.fullScreenHandlersInstalled = true;
          document.addEventListener('fullscreenchange', fullScreenChange, false);
          document.addEventListener('mozfullscreenchange', fullScreenChange, false);
          document.addEventListener('webkitfullscreenchange', fullScreenChange, false);
        }
        canvas.requestFullScreen = canvas['requestFullScreen'] ||
                                   canvas['mozRequestFullScreen'] ||
                                   (canvas['webkitRequestFullScreen'] ? function() { canvas['webkitRequestFullScreen'](Element['ALLOW_KEYBOARD_INPUT']) } : null);
        canvas.requestFullScreen();
      },requestAnimationFrame:function requestAnimationFrame(func) {
        if (typeof window === 'undefined') { // Provide fallback to setTimeout if window is undefined (e.g. in Node.js)
          setTimeout(func, 1000/60);
        } else {
          if (!window.requestAnimationFrame) {
            window.requestAnimationFrame = window['requestAnimationFrame'] ||
                                           window['mozRequestAnimationFrame'] ||
                                           window['webkitRequestAnimationFrame'] ||
                                           window['msRequestAnimationFrame'] ||
                                           window['oRequestAnimationFrame'] ||
                                           window['setTimeout'];
          }
          window.requestAnimationFrame(func);
        }
      },safeCallback:function (func) {
        return function() {
          if (!ABORT) return func.apply(null, arguments);
        };
      },safeRequestAnimationFrame:function (func) {
        return Browser.requestAnimationFrame(function() {
          if (!ABORT) func();
        });
      },safeSetTimeout:function (func, timeout) {
        return setTimeout(function() {
          if (!ABORT) func();
        }, timeout);
      },safeSetInterval:function (func, timeout) {
        return setInterval(function() {
          if (!ABORT) func();
        }, timeout);
      },getMimetype:function (name) {
        return {
          'jpg': 'image/jpeg',
          'jpeg': 'image/jpeg',
          'png': 'image/png',
          'bmp': 'image/bmp',
          'ogg': 'audio/ogg',
          'wav': 'audio/wav',
          'mp3': 'audio/mpeg'
        }[name.substr(name.lastIndexOf('.')+1)];
      },getUserMedia:function (func) {
        if(!window.getUserMedia) {
          window.getUserMedia = navigator['getUserMedia'] ||
                                navigator['mozGetUserMedia'];
        }
        window.getUserMedia(func);
      },getMovementX:function (event) {
        return event['movementX'] ||
               event['mozMovementX'] ||
               event['webkitMovementX'] ||
               0;
      },getMovementY:function (event) {
        return event['movementY'] ||
               event['mozMovementY'] ||
               event['webkitMovementY'] ||
               0;
      },mouseX:0,mouseY:0,mouseMovementX:0,mouseMovementY:0,calculateMouseEvent:function (event) { // event should be mousemove, mousedown or mouseup
        if (Browser.pointerLock) {
          // When the pointer is locked, calculate the coordinates
          // based on the movement of the mouse.
          // Workaround for Firefox bug 764498
          if (event.type != 'mousemove' &&
              ('mozMovementX' in event)) {
            Browser.mouseMovementX = Browser.mouseMovementY = 0;
          } else {
            Browser.mouseMovementX = Browser.getMovementX(event);
            Browser.mouseMovementY = Browser.getMovementY(event);
          }
          // check if SDL is available
          if (typeof SDL != "undefined") {
          	Browser.mouseX = SDL.mouseX + Browser.mouseMovementX;
          	Browser.mouseY = SDL.mouseY + Browser.mouseMovementY;
          } else {
          	// just add the mouse delta to the current absolut mouse position
          	// FIXME: ideally this should be clamped against the canvas size and zero
          	Browser.mouseX += Browser.mouseMovementX;
          	Browser.mouseY += Browser.mouseMovementY;
          }        
        } else {
          // Otherwise, calculate the movement based on the changes
          // in the coordinates.
          var rect = Module["canvas"].getBoundingClientRect();
          var x, y;
          // Neither .scrollX or .pageXOffset are defined in a spec, but
          // we prefer .scrollX because it is currently in a spec draft.
          // (see: http://www.w3.org/TR/2013/WD-cssom-view-20131217/)
          var scrollX = ((typeof window.scrollX !== 'undefined') ? window.scrollX : window.pageXOffset);
          var scrollY = ((typeof window.scrollY !== 'undefined') ? window.scrollY : window.pageYOffset);
          if (event.type == 'touchstart' ||
              event.type == 'touchend' ||
              event.type == 'touchmove') {
            var t = event.touches.item(0);
            if (t) {
              x = t.pageX - (scrollX + rect.left);
              y = t.pageY - (scrollY + rect.top);
            } else {
              return;
            }
          } else {
            x = event.pageX - (scrollX + rect.left);
            y = event.pageY - (scrollY + rect.top);
          }
          // the canvas might be CSS-scaled compared to its backbuffer;
          // SDL-using content will want mouse coordinates in terms
          // of backbuffer units.
          var cw = Module["canvas"].width;
          var ch = Module["canvas"].height;
          x = x * (cw / rect.width);
          y = y * (ch / rect.height);
          Browser.mouseMovementX = x - Browser.mouseX;
          Browser.mouseMovementY = y - Browser.mouseY;
          Browser.mouseX = x;
          Browser.mouseY = y;
        }
      },xhrLoad:function (url, onload, onerror) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.responseType = 'arraybuffer';
        xhr.onload = function xhr_onload() {
          if (xhr.status == 200 || (xhr.status == 0 && xhr.response)) { // file URLs can return 0
            onload(xhr.response);
          } else {
            onerror();
          }
        };
        xhr.onerror = onerror;
        xhr.send(null);
      },asyncLoad:function (url, onload, onerror, noRunDep) {
        Browser.xhrLoad(url, function(arrayBuffer) {
          assert(arrayBuffer, 'Loading data file "' + url + '" failed (no arrayBuffer).');
          onload(new Uint8Array(arrayBuffer));
          if (!noRunDep) removeRunDependency('al ' + url);
        }, function(event) {
          if (onerror) {
            onerror();
          } else {
            throw 'Loading data file "' + url + '" failed.';
          }
        });
        if (!noRunDep) addRunDependency('al ' + url);
      },resizeListeners:[],updateResizeListeners:function () {
        var canvas = Module['canvas'];
        Browser.resizeListeners.forEach(function(listener) {
          listener(canvas.width, canvas.height);
        });
      },setCanvasSize:function (width, height, noUpdates) {
        var canvas = Module['canvas'];
        canvas.width = width;
        canvas.height = height;
        if (!noUpdates) Browser.updateResizeListeners();
      },windowedWidth:0,windowedHeight:0,setFullScreenCanvasSize:function () {
        var canvas = Module['canvas'];
        this.windowedWidth = canvas.width;
        this.windowedHeight = canvas.height;
        canvas.width = screen.width;
        canvas.height = screen.height;
        // check if SDL is available   
        if (typeof SDL != "undefined") {
        	var flags = HEAPU32[((SDL.screen+Runtime.QUANTUM_SIZE*0)>>2)];
        	flags = flags | 0x00800000; // set SDL_FULLSCREEN flag
        	HEAP32[((SDL.screen+Runtime.QUANTUM_SIZE*0)>>2)]=flags
        }
        Browser.updateResizeListeners();
      },setWindowedCanvasSize:function () {
        var canvas = Module['canvas'];
        canvas.width = this.windowedWidth;
        canvas.height = this.windowedHeight;
        // check if SDL is available       
        if (typeof SDL != "undefined") {
        	var flags = HEAPU32[((SDL.screen+Runtime.QUANTUM_SIZE*0)>>2)];
        	flags = flags & ~0x00800000; // clear SDL_FULLSCREEN flag
        	HEAP32[((SDL.screen+Runtime.QUANTUM_SIZE*0)>>2)]=flags
        }
        Browser.updateResizeListeners();
      }};var SDL={defaults:{width:320,height:200,copyOnLock:true},version:null,surfaces:{},canvasPool:[],events:[],fonts:[null],audios:[null],rwops:[null],music:{audio:null,volume:1},mixerFrequency:22050,mixerFormat:32784,mixerNumChannels:2,mixerChunkSize:1024,channelMinimumNumber:0,GL:false,glAttributes:{0:3,1:3,2:2,3:0,4:0,5:1,6:16,7:0,8:0,9:0,10:0,11:0,12:0,13:0,14:0,15:1,16:0,17:0,18:0},keyboardState:null,keyboardMap:{},canRequestFullscreen:false,isRequestingFullscreen:false,textInput:false,startTime:null,initFlags:0,buttonState:0,modState:0,DOMButtons:[0,0,0],DOMEventToSDLEvent:{},keyCodes:{16:1249,17:1248,18:1250,33:1099,34:1102,37:1104,38:1106,39:1103,40:1105,46:127,96:1112,97:1113,98:1114,99:1115,100:1116,101:1117,102:1118,103:1119,104:1120,105:1121,112:1082,113:1083,114:1084,115:1085,116:1086,117:1087,118:1088,119:1089,120:1090,121:1091,122:1092,123:1093,173:45,188:44,190:46,191:47,192:96},scanCodes:{8:42,9:43,13:40,27:41,32:44,44:54,46:55,47:56,48:39,49:30,50:31,51:32,52:33,53:34,54:35,55:36,56:37,57:38,59:51,61:46,91:47,92:49,93:48,96:52,97:4,98:5,99:6,100:7,101:8,102:9,103:10,104:11,105:12,106:13,107:14,108:15,109:16,110:17,111:18,112:19,113:20,114:21,115:22,116:23,117:24,118:25,119:26,120:27,121:28,122:29,305:224,308:226},loadRect:function (rect) {
        return {
          x: HEAP32[((rect + 0)>>2)],
          y: HEAP32[((rect + 4)>>2)],
          w: HEAP32[((rect + 8)>>2)],
          h: HEAP32[((rect + 12)>>2)]
        };
      },loadColorToCSSRGB:function (color) {
        var rgba = HEAP32[((color)>>2)];
        return 'rgb(' + (rgba&255) + ',' + ((rgba >> 8)&255) + ',' + ((rgba >> 16)&255) + ')';
      },loadColorToCSSRGBA:function (color) {
        var rgba = HEAP32[((color)>>2)];
        return 'rgba(' + (rgba&255) + ',' + ((rgba >> 8)&255) + ',' + ((rgba >> 16)&255) + ',' + (((rgba >> 24)&255)/255) + ')';
      },translateColorToCSSRGBA:function (rgba) {
        return 'rgba(' + (rgba&0xff) + ',' + (rgba>>8 & 0xff) + ',' + (rgba>>16 & 0xff) + ',' + (rgba>>>24)/0xff + ')';
      },translateRGBAToCSSRGBA:function (r, g, b, a) {
        return 'rgba(' + (r&0xff) + ',' + (g&0xff) + ',' + (b&0xff) + ',' + (a&0xff)/255 + ')';
      },translateRGBAToColor:function (r, g, b, a) {
        return r | g << 8 | b << 16 | a << 24;
      },makeSurface:function (width, height, flags, usePageCanvas, source, rmask, gmask, bmask, amask) {
        flags = flags || 0;
        var surf = _malloc(60);  // SDL_Surface has 15 fields of quantum size
        var buffer = _malloc(width*height*4); // TODO: only allocate when locked the first time
        var pixelFormat = _malloc(44);
        flags |= 1; // SDL_HWSURFACE - this tells SDL_MUSTLOCK that this needs to be locked
        //surface with SDL_HWPALETTE flag is 8bpp surface (1 byte)
        var is_SDL_HWPALETTE = flags & 0x00200000;  
        var bpp = is_SDL_HWPALETTE ? 1 : 4;
        HEAP32[((surf)>>2)]=flags         // SDL_Surface.flags
        HEAP32[(((surf)+(4))>>2)]=pixelFormat // SDL_Surface.format TODO
        HEAP32[(((surf)+(8))>>2)]=width         // SDL_Surface.w
        HEAP32[(((surf)+(12))>>2)]=height        // SDL_Surface.h
        HEAP32[(((surf)+(16))>>2)]=width * bpp       // SDL_Surface.pitch, assuming RGBA or indexed for now,
                                                                                 // since that is what ImageData gives us in browsers
        HEAP32[(((surf)+(20))>>2)]=buffer      // SDL_Surface.pixels
        HEAP32[(((surf)+(36))>>2)]=0      // SDL_Surface.offset
        HEAP32[(((surf)+(56))>>2)]=1
        HEAP32[((pixelFormat)>>2)]=0 /* XXX missing C define SDL_PIXELFORMAT_RGBA8888 */ // SDL_PIXELFORMAT_RGBA8888
        HEAP32[(((pixelFormat)+(4))>>2)]=0 // TODO
        HEAP8[(((pixelFormat)+(8))|0)]=bpp * 8
        HEAP8[(((pixelFormat)+(9))|0)]=bpp
        HEAP32[(((pixelFormat)+(12))>>2)]=rmask || 0x000000ff
        HEAP32[(((pixelFormat)+(16))>>2)]=gmask || 0x0000ff00
        HEAP32[(((pixelFormat)+(20))>>2)]=bmask || 0x00ff0000
        HEAP32[(((pixelFormat)+(24))>>2)]=amask || 0xff000000
        // Decide if we want to use WebGL or not
        var useWebGL = (flags & 0x04000000) != 0; // SDL_OPENGL
        SDL.GL = SDL.GL || useWebGL;
        var canvas;
        if (!usePageCanvas) {
          if (SDL.canvasPool.length > 0) {
            canvas = SDL.canvasPool.pop();
          } else {
            canvas = document.createElement('canvas');
          }
          canvas.width = width;
          canvas.height = height;
        } else {
          canvas = Module['canvas'];
        }
        var webGLContextAttributes = {
          antialias: ((SDL.glAttributes[13 /*SDL_GL_MULTISAMPLEBUFFERS*/] != 0) && (SDL.glAttributes[14 /*SDL_GL_MULTISAMPLESAMPLES*/] > 1)),
          depth: (SDL.glAttributes[6 /*SDL_GL_DEPTH_SIZE*/] > 0),
          stencil: (SDL.glAttributes[7 /*SDL_GL_STENCIL_SIZE*/] > 0)
        };
        var ctx = Browser.createContext(canvas, useWebGL, usePageCanvas, webGLContextAttributes);
        SDL.surfaces[surf] = {
          width: width,
          height: height,
          canvas: canvas,
          ctx: ctx,
          surf: surf,
          buffer: buffer,
          pixelFormat: pixelFormat,
          alpha: 255,
          flags: flags,
          locked: 0,
          usePageCanvas: usePageCanvas,
          source: source,
          isFlagSet: function(flag) {
            return flags & flag;
          }
        };
        return surf;
      },copyIndexedColorData:function (surfData, rX, rY, rW, rH) {
        // HWPALETTE works with palette
        // setted by SDL_SetColors
        if (!surfData.colors) {
          return;
        }
        var fullWidth  = Module['canvas'].width;
        var fullHeight = Module['canvas'].height;
        var startX  = rX || 0;
        var startY  = rY || 0;
        var endX    = (rW || (fullWidth - startX)) + startX;
        var endY    = (rH || (fullHeight - startY)) + startY;
        var buffer  = surfData.buffer;
        var data    = surfData.image.data;
        var colors  = surfData.colors;
        for (var y = startY; y < endY; ++y) {
          var indexBase = y * fullWidth;
          var colorBase = indexBase * 4;
          for (var x = startX; x < endX; ++x) {
            // HWPALETTE have only 256 colors (not rgba)
            var index = HEAPU8[((buffer + indexBase + x)|0)] * 3;
            var colorOffset = colorBase + x * 4;
            data[colorOffset   ] = colors[index   ];
            data[colorOffset +1] = colors[index +1];
            data[colorOffset +2] = colors[index +2];
            //unused: data[colorOffset +3] = color[index +3];
          }
        }
      },freeSurface:function (surf) {
        var refcountPointer = surf + 56;
        var refcount = HEAP32[((refcountPointer)>>2)];
        if (refcount > 1) {
          HEAP32[((refcountPointer)>>2)]=refcount - 1;
          return;
        }
        var info = SDL.surfaces[surf];
        if (!info.usePageCanvas && info.canvas) SDL.canvasPool.push(info.canvas);
        _free(info.buffer);
        _free(info.pixelFormat);
        _free(surf);
        SDL.surfaces[surf] = null;
      },touchX:0,touchY:0,savedKeydown:null,receiveEvent:function (event) {
        switch(event.type) {
          case 'touchstart':
            event.preventDefault();
            var touch = event.touches[0];
            touchX = touch.pageX;
            touchY = touch.pageY;
            var event = {
              type: 'mousedown',
              button: 0,
              pageX: touchX,
              pageY: touchY
            };
            SDL.DOMButtons[0] = 1;
            SDL.events.push(event);
            break;
          case 'touchmove':
            event.preventDefault();
            var touch = event.touches[0];
            touchX = touch.pageX;
            touchY = touch.pageY;
            event = {
              type: 'mousemove',
              button: 0,
              pageX: touchX,
              pageY: touchY
            };
            SDL.events.push(event);
            break;
          case 'touchend':
            event.preventDefault();
            event = {
              type: 'mouseup',
              button: 0,
              pageX: touchX,
              pageY: touchY
            };
            SDL.DOMButtons[0] = 0;
            SDL.events.push(event);
            break;
          case 'mousemove':
            if (Browser.pointerLock) {
              // workaround for firefox bug 750111
              if ('mozMovementX' in event) {
                event['movementX'] = event['mozMovementX'];
                event['movementY'] = event['mozMovementY'];
              }
              // workaround for Firefox bug 782777
              if (event['movementX'] == 0 && event['movementY'] == 0) {
                // ignore a mousemove event if it doesn't contain any movement info
                // (without pointer lock, we infer movement from pageX/pageY, so this check is unnecessary)
                event.preventDefault();
                return;
              }
            }
            // fall through
          case 'keydown': case 'keyup': case 'keypress': case 'mousedown': case 'mouseup': case 'DOMMouseScroll': case 'mousewheel':
            // If we preventDefault on keydown events, the subsequent keypress events
            // won't fire. However, it's fine (and in some cases necessary) to
            // preventDefault for keys that don't generate a character. Otherwise,
            // preventDefault is the right thing to do in general.
            if (event.type !== 'keydown' || (event.keyCode === 8 /* backspace */ || event.keyCode === 9 /* tab */)) {
              event.preventDefault();
            }
            if (event.type == 'DOMMouseScroll' || event.type == 'mousewheel') {
              var button = (event.type == 'DOMMouseScroll' ? event.detail : -event.wheelDelta) > 0 ? 4 : 3;
              var event2 = {
                type: 'mousedown',
                button: button,
                pageX: event.pageX,
                pageY: event.pageY
              };
              SDL.events.push(event2);
              event = {
                type: 'mouseup',
                button: button,
                pageX: event.pageX,
                pageY: event.pageY
              };
            } else if (event.type == 'mousedown') {
              SDL.DOMButtons[event.button] = 1;
            } else if (event.type == 'mouseup') {
              // ignore extra ups, can happen if we leave the canvas while pressing down, then return,
              // since we add a mouseup in that case
              if (!SDL.DOMButtons[event.button]) {
                return;
              }
              SDL.DOMButtons[event.button] = 0;
            }
            // We can only request fullscreen as the result of user input.
            // Due to this limitation, we toggle a boolean on keydown which
            // SDL_WM_ToggleFullScreen will check and subsequently set another
            // flag indicating for us to request fullscreen on the following
            // keyup. This isn't perfect, but it enables SDL_WM_ToggleFullScreen
            // to work as the result of a keypress (which is an extremely
            // common use case).
            if (event.type === 'keydown') {
              SDL.canRequestFullscreen = true;
            } else if (event.type === 'keyup') {
              if (SDL.isRequestingFullscreen) {
                Module['requestFullScreen'](true, true);
                SDL.isRequestingFullscreen = false;
              }
              SDL.canRequestFullscreen = false;
            }
            // SDL expects a unicode character to be passed to its keydown events.
            // Unfortunately, the browser APIs only provide a charCode property on
            // keypress events, so we must backfill in keydown events with their
            // subsequent keypress event's charCode.
            if (event.type === 'keypress' && SDL.savedKeydown) {
              // charCode is read-only
              SDL.savedKeydown.keypressCharCode = event.charCode;
              SDL.savedKeydown = null;
            } else if (event.type === 'keydown') {
              SDL.savedKeydown = event;
            }
            // Don't push keypress events unless SDL_StartTextInput has been called.
            if (event.type !== 'keypress' || SDL.textInput) {
              SDL.events.push(event);
            }
            break;
          case 'mouseout':
            // Un-press all pressed mouse buttons, because we might miss the release outside of the canvas
            for (var i = 0; i < 3; i++) {
              if (SDL.DOMButtons[i]) {
                SDL.events.push({
                  type: 'mouseup',
                  button: i,
                  pageX: event.pageX,
                  pageY: event.pageY
                });
                SDL.DOMButtons[i] = 0;
              }
            }
            event.preventDefault();
            break;
          case 'blur':
          case 'visibilitychange': {
            // Un-press all pressed keys: TODO
            for (var code in SDL.keyboardMap) {
              SDL.events.push({
                type: 'keyup',
                keyCode: SDL.keyboardMap[code]
              });
            }
            event.preventDefault();
            break;
          }
          case 'unload':
            if (Browser.mainLoop.runner) {
              SDL.events.push(event);
              // Force-run a main event loop, since otherwise this event will never be caught!
              Browser.mainLoop.runner();
            }
            return;
          case 'resize':
            SDL.events.push(event);
            // manually triggered resize event doesn't have a preventDefault member
            if (event.preventDefault) {
              event.preventDefault();
            }
            break;
        }
        if (SDL.events.length >= 10000) {
          Module.printErr('SDL event queue full, dropping events');
          SDL.events = SDL.events.slice(0, 10000);
        }
        return;
      },handleEvent:function (event) {
        if (event.handled) return;
        event.handled = true;
        switch (event.type) {
          case 'keydown': case 'keyup': {
            var down = event.type === 'keydown';
            var code = event.keyCode;
            if (code >= 65 && code <= 90) {
              code += 32; // make lowercase for SDL
            } else {
              code = SDL.keyCodes[event.keyCode] || event.keyCode;
            }
            HEAP8[(((SDL.keyboardState)+(code))|0)]=down;
            // TODO: lmeta, rmeta, numlock, capslock, KMOD_MODE, KMOD_RESERVED
            SDL.modState = (HEAP8[(((SDL.keyboardState)+(1248))|0)] ? 0x0040 | 0x0080 : 0) | // KMOD_LCTRL & KMOD_RCTRL
              (HEAP8[(((SDL.keyboardState)+(1249))|0)] ? 0x0001 | 0x0002 : 0) | // KMOD_LSHIFT & KMOD_RSHIFT
              (HEAP8[(((SDL.keyboardState)+(1250))|0)] ? 0x0100 | 0x0200 : 0); // KMOD_LALT & KMOD_RALT
            if (down) {
              SDL.keyboardMap[code] = event.keyCode; // save the DOM input, which we can use to unpress it during blur
            } else {
              delete SDL.keyboardMap[code];
            }
            break;
          }
          case 'mousedown': case 'mouseup':
            if (event.type == 'mousedown') {
              // SDL_BUTTON(x) is defined as (1 << ((x)-1)).  SDL buttons are 1-3,
              // and DOM buttons are 0-2, so this means that the below formula is
              // correct.
              SDL.buttonState |= 1 << event.button;
            } else if (event.type == 'mouseup') {
              SDL.buttonState &= ~(1 << event.button);
            }
            // fall through
          case 'mousemove': {
            Browser.calculateMouseEvent(event);
            break;
          }
        }
      },makeCEvent:function (event, ptr) {
        if (typeof event === 'number') {
          // This is a pointer to a native C event that was SDL_PushEvent'ed
          _memcpy(ptr, event, 28); // XXX
          return;
        }
        SDL.handleEvent(event);
        switch (event.type) {
          case 'keydown': case 'keyup': {
            var down = event.type === 'keydown';
            //Module.print('Received key event: ' + event.keyCode);
            var key = event.keyCode;
            if (key >= 65 && key <= 90) {
              key += 32; // make lowercase for SDL
            } else {
              key = SDL.keyCodes[event.keyCode] || event.keyCode;
            }
            var scan;
            if (key >= 1024) {
              scan = key - 1024;
            } else {
              scan = SDL.scanCodes[key] || key;
            }
            HEAP32[((ptr)>>2)]=SDL.DOMEventToSDLEvent[event.type]
            HEAP8[(((ptr)+(8))|0)]=down ? 1 : 0
            HEAP8[(((ptr)+(9))|0)]=0 // TODO
            HEAP32[(((ptr)+(12))>>2)]=scan
            HEAP32[(((ptr)+(16))>>2)]=key
            HEAP16[(((ptr)+(20))>>1)]=SDL.modState
            // some non-character keys (e.g. backspace and tab) won't have keypressCharCode set, fill in with the keyCode.
            HEAP32[(((ptr)+(24))>>2)]=event.keypressCharCode || key
            break;
          }
          case 'keypress': {
            HEAP32[((ptr)>>2)]=SDL.DOMEventToSDLEvent[event.type]
            // Not filling in windowID for now
            var cStr = intArrayFromString(String.fromCharCode(event.charCode));
            for (var i = 0; i < cStr.length; ++i) {
              HEAP8[(((ptr)+(8 + i))|0)]=cStr[i];
            }
            break;
          }
          case 'mousedown': case 'mouseup': case 'mousemove': {
            if (event.type != 'mousemove') {
              var down = event.type === 'mousedown';
              HEAP32[((ptr)>>2)]=SDL.DOMEventToSDLEvent[event.type];
              HEAP8[(((ptr)+(8))|0)]=event.button+1; // DOM buttons are 0-2, SDL 1-3
              HEAP8[(((ptr)+(9))|0)]=down ? 1 : 0;
              HEAP32[(((ptr)+(12))>>2)]=Browser.mouseX;
              HEAP32[(((ptr)+(16))>>2)]=Browser.mouseY;
            } else {
              HEAP32[((ptr)>>2)]=SDL.DOMEventToSDLEvent[event.type];
              HEAP8[(((ptr)+(8))|0)]=SDL.buttonState;
              HEAP32[(((ptr)+(12))>>2)]=Browser.mouseX;
              HEAP32[(((ptr)+(16))>>2)]=Browser.mouseY;
              HEAP32[(((ptr)+(20))>>2)]=Browser.mouseMovementX;
              HEAP32[(((ptr)+(24))>>2)]=Browser.mouseMovementY;
            }
            break;
          }
          case 'unload': {
            HEAP32[((ptr)>>2)]=SDL.DOMEventToSDLEvent[event.type];
            break;
          }
          case 'resize': {
            HEAP32[((ptr)>>2)]=SDL.DOMEventToSDLEvent[event.type];
            HEAP32[(((ptr)+(4))>>2)]=event.w;
            HEAP32[(((ptr)+(8))>>2)]=event.h;
            break;
          }
          case 'joystick_button_up': case 'joystick_button_down': {
            var state = event.type === 'joystick_button_up' ? 0 : 1;
            HEAP32[((ptr)>>2)]=SDL.DOMEventToSDLEvent[event.type];
            HEAP8[(((ptr)+(4))|0)]=event.index;
            HEAP8[(((ptr)+(5))|0)]=event.button;
            HEAP8[(((ptr)+(6))|0)]=state;
            break;
          }
          case 'joystick_axis_motion': {
            HEAP32[((ptr)>>2)]=SDL.DOMEventToSDLEvent[event.type];
            HEAP8[(((ptr)+(4))|0)]=event.index;
            HEAP8[(((ptr)+(5))|0)]=event.axis;
            HEAP32[(((ptr)+(8))>>2)]=SDL.joystickAxisValueConversion(event.value);
            break;
          }
          default: throw 'Unhandled SDL event: ' + event.type;
        }
      },estimateTextWidth:function (fontData, text) {
        var h = fontData.size;
        var fontString = h + 'px ' + fontData.name;
        var tempCtx = SDL.ttfContext;
        tempCtx.save();
        tempCtx.font = fontString;
        var ret = tempCtx.measureText(text).width | 0;
        tempCtx.restore();
        return ret;
      },allocateChannels:function (num) { // called from Mix_AllocateChannels and init
        if (SDL.numChannels && SDL.numChannels >= num && num != 0) return;
        SDL.numChannels = num;
        SDL.channels = [];
        for (var i = 0; i < num; i++) {
          SDL.channels[i] = {
            audio: null,
            volume: 1.0
          };
        }
      },setGetVolume:function (info, volume) {
        if (!info) return 0;
        var ret = info.volume * 128; // MIX_MAX_VOLUME
        if (volume != -1) {
          info.volume = volume / 128;
          if (info.audio) info.audio.volume = info.volume;
        }
        return ret;
      },debugSurface:function (surfData) {
        console.log('dumping surface ' + [surfData.surf, surfData.source, surfData.width, surfData.height]);
        var image = surfData.ctx.getImageData(0, 0, surfData.width, surfData.height);
        var data = image.data;
        var num = Math.min(surfData.width, surfData.height);
        for (var i = 0; i < num; i++) {
          console.log('   diagonal ' + i + ':' + [data[i*surfData.width*4 + i*4 + 0], data[i*surfData.width*4 + i*4 + 1], data[i*surfData.width*4 + i*4 + 2], data[i*surfData.width*4 + i*4 + 3]]);
        }
      },joystickEventState:1,lastJoystickState:{},joystickNamePool:{},recordJoystickState:function (joystick, state) {
        // Standardize button state.
        var buttons = new Array(state.buttons.length);
        for (var i = 0; i < state.buttons.length; i++) {
          buttons[i] = SDL.getJoystickButtonState(state.buttons[i]);
        }
        SDL.lastJoystickState[joystick] = {
          buttons: buttons,
          axes: state.axes.slice(0),
          timestamp: state.timestamp,
          index: state.index,
          id: state.id
        };
      },getJoystickButtonState:function (button) {
        if (typeof button === 'object') {
          // Current gamepad API editor's draft (Firefox Nightly)
          // https://dvcs.w3.org/hg/gamepad/raw-file/default/gamepad.html#idl-def-GamepadButton
          return button.pressed;
        } else {
          // Current gamepad API working draft (Firefox / Chrome Stable)
          // http://www.w3.org/TR/2012/WD-gamepad-20120529/#gamepad-interface
          return button > 0;
        }
      },queryJoysticks:function () {
        for (var joystick in SDL.lastJoystickState) {
          var state = SDL.getGamepad(joystick - 1);
          var prevState = SDL.lastJoystickState[joystick];
          // Check only if the timestamp has differed.
          // NOTE: Timestamp is not available in Firefox.
          if (typeof state.timestamp !== 'number' || state.timestamp !== prevState.timestamp) {
            var i;
            for (i = 0; i < state.buttons.length; i++) {
              var buttonState = SDL.getJoystickButtonState(state.buttons[i]);
              // NOTE: The previous state already has a boolean representation of
              //       its button, so no need to standardize its button state here.
              if (buttonState !== prevState.buttons[i]) {
                // Insert button-press event.
                SDL.events.push({
                  type: buttonState ? 'joystick_button_down' : 'joystick_button_up',
                  joystick: joystick,
                  index: joystick - 1,
                  button: i
                });
              }
            }
            for (i = 0; i < state.axes.length; i++) {
              if (state.axes[i] !== prevState.axes[i]) {
                // Insert axes-change event.
                SDL.events.push({
                  type: 'joystick_axis_motion',
                  joystick: joystick,
                  index: joystick - 1,
                  axis: i,
                  value: state.axes[i]
                });
              }
            }
            SDL.recordJoystickState(joystick, state);
          }
        }
      },joystickAxisValueConversion:function (value) {
        // Ensures that 0 is 0, 1 is 32767, and -1 is 32768.
        return Math.ceil(((value+1) * 32767.5) - 32768);
      },getGamepads:function () {
        var fcn = navigator.getGamepads || navigator.webkitGamepads || navigator.mozGamepads || navigator.gamepads || navigator.webkitGetGamepads;
        if (fcn !== undefined) {
          // The function must be applied on the navigator object.
          return fcn.apply(navigator);
        } else {
          return [];
        }
      },getGamepad:function (deviceIndex) {
        var gamepads = SDL.getGamepads();
        if (gamepads.length > deviceIndex && deviceIndex >= 0) {
          return gamepads[deviceIndex];
        }
        return null;
      }};function _SDL_LockSurface(surf) {
      var surfData = SDL.surfaces[surf];
      surfData.locked++;
      if (surfData.locked > 1) return 0;
      // Mark in C/C++-accessible SDL structure
      // SDL_Surface has the following fields: Uint32 flags, SDL_PixelFormat *format; int w, h; Uint16 pitch; void *pixels; ...
      // So we have fields all of the same size, and 5 of them before us.
      // TODO: Use macros like in library.js
      HEAP32[(((surf)+(20))>>2)]=surfData.buffer;
      if (surf == SDL.screen && Module.screenIsReadOnly && surfData.image) return 0;
      surfData.image = surfData.ctx.getImageData(0, 0, surfData.width, surfData.height);
      if (surf == SDL.screen) {
        var data = surfData.image.data;
        var num = data.length;
        for (var i = 0; i < num/4; i++) {
          data[i*4+3] = 255; // opacity, as canvases blend alpha
        }
      }
      if (SDL.defaults.copyOnLock) {
        // Copy pixel data to somewhere accessible to 'C/C++'
        if (surfData.isFlagSet(0x00200000 /* SDL_HWPALETTE */)) {
          // If this is neaded then
          // we should compact the data from 32bpp to 8bpp index.
          // I think best way to implement this is use
          // additional colorMap hash (color->index).
          // Something like this:
          //
          // var size = surfData.width * surfData.height;
          // var data = '';
          // for (var i = 0; i<size; i++) {
          //   var color = SDL.translateRGBAToColor(
          //     surfData.image.data[i*4   ], 
          //     surfData.image.data[i*4 +1], 
          //     surfData.image.data[i*4 +2], 
          //     255);
          //   var index = surfData.colorMap[color];
          //   HEAP8[(((surfData.buffer)+(i))|0)]=index;
          // }
          throw 'CopyOnLock is not supported for SDL_LockSurface with SDL_HWPALETTE flag set' + new Error().stack;
        } else {
        HEAPU8.set(surfData.image.data, surfData.buffer);
        }
      }
      return 0;
    }
  Module["_memcpy"] = _memcpy;var _llvm_memcpy_p0i8_p0i8_i32=_memcpy;
  function _SDL_UnlockSurface(surf) {
      assert(!SDL.GL); // in GL mode we do not keep around 2D canvases and contexts
      var surfData = SDL.surfaces[surf];
      surfData.locked--;
      if (surfData.locked > 0) return;
      // Copy pixel data to image
      if (surfData.isFlagSet(0x00200000 /* SDL_HWPALETTE */)) {
        SDL.copyIndexedColorData(surfData);
      } else if (!surfData.colors) {
        var data = surfData.image.data;
        var buffer = surfData.buffer;
        assert(buffer % 4 == 0, 'Invalid buffer offset: ' + buffer);
        var src = buffer >> 2;
        var dst = 0;
        var isScreen = surf == SDL.screen;
        var num;
        if (typeof CanvasPixelArray !== 'undefined' && data instanceof CanvasPixelArray) {
          // IE10/IE11: ImageData objects are backed by the deprecated CanvasPixelArray,
          // not UInt8ClampedArray. These don't have buffers, so we need to revert
          // to copying a byte at a time. We do the undefined check because modern
          // browsers do not define CanvasPixelArray anymore.
          num = data.length;
          while (dst < num) {
            var val = HEAP32[src]; // This is optimized. Instead, we could do HEAP32[(((buffer)+(dst))>>2)];
            data[dst  ] = val & 0xff;
            data[dst+1] = (val >> 8) & 0xff;
            data[dst+2] = (val >> 16) & 0xff;
            data[dst+3] = isScreen ? 0xff : ((val >> 24) & 0xff);
            src++;
            dst += 4;
          }
        } else {
          var data32 = new Uint32Array(data.buffer);
          num = data32.length;
          if (isScreen) {
            while (dst < num) {
              // HEAP32[src++] is an optimization. Instead, we could do HEAP32[(((buffer)+(dst))>>2)];
              data32[dst++] = HEAP32[src++] | 0xff000000;
            }
          } else {
            while (dst < num) {
              data32[dst++] = HEAP32[src++];
            }
          }
        }
      } else {
        var width = Module['canvas'].width;
        var height = Module['canvas'].height;
        var s = surfData.buffer;
        var data = surfData.image.data;
        var colors = surfData.colors;
        for (var y = 0; y < height; y++) {
          var base = y*width*4;
          for (var x = 0; x < width; x++) {
            // See comment above about signs
            var val = HEAPU8[((s++)|0)] * 3;
            var start = base + x*4;
            data[start]   = colors[val];
            data[start+1] = colors[val+1];
            data[start+2] = colors[val+2];
          }
          s += width*3;
        }
      }
      // Copy to canvas
      surfData.ctx.putImageData(surfData.image, 0, 0);
      // Note that we save the image, so future writes are fast. But, memory is not yet released
    }
  function _SDL_UpdateRect(surf, x, y, w, h) {
      // We actually do the whole screen in Unlock...
    }
  function _SDL_GetModState() {
      return SDL.modState;
    }
  function _toupper(chr) {
      if (chr >= 97 && chr <= 122) {
        return chr - 97 + 65;
      } else {
        return chr;
      }
    }
  function _atexit(func, arg) {
      __ATEXIT__.unshift({ func: func, arg: arg });
    }var ___cxa_atexit=_atexit;
  function _SDL_PollEvent(ptr) {
      if (SDL.initFlags & 0x200 && SDL.joystickEventState) {
        // If SDL_INIT_JOYSTICK was supplied AND the joystick system is configured
        // to automatically query for events, query for joystick events.
        SDL.queryJoysticks();
      }
      if (SDL.events.length === 0) return 0;
      if (ptr) {
        SDL.makeCEvent(SDL.events.shift(), ptr);
      }
      return 1;
    }
  function _SDL_SetVideoMode(width, height, depth, flags) {
      ['mousedown', 'mouseup', 'mousemove', 'DOMMouseScroll', 'mousewheel', 'mouseout'].forEach(function(event) {
        Module['canvas'].addEventListener(event, SDL.receiveEvent, true);
      });
      // (0,0) means 'use fullscreen' in native; in Emscripten, use the current canvas size.
      if (width == 0 && height == 0) {
        var canvas = Module['canvas'];
        width = canvas.width;
        height = canvas.height;
      }
      Browser.setCanvasSize(width, height, true);
      // Free the old surface first.
      if (SDL.screen) {
        SDL.freeSurface(SDL.screen);
        SDL.screen = null;
      }
      SDL.screen = SDL.makeSurface(width, height, flags, true, 'screen');
      if (!SDL.addedResizeListener) {
        SDL.addedResizeListener = true;
        Browser.resizeListeners.push(function(w, h) {
          SDL.receiveEvent({
            type: 'resize',
            w: w,
            h: h
          });
        });
      }
      return SDL.screen;
    }
  function _SDL_Init(initFlags) {
      SDL.startTime = Date.now();
      SDL.initFlags = initFlags;
      // capture all key events. we just keep down and up, but also capture press to prevent default actions
      if (!Module['doNotCaptureKeyboard']) {
        document.addEventListener("keydown", SDL.receiveEvent);
        document.addEventListener("keyup", SDL.receiveEvent);
        document.addEventListener("keypress", SDL.receiveEvent);
        window.addEventListener("blur", SDL.receiveEvent);
        document.addEventListener("visibilitychange", SDL.receiveEvent);
      }
      if (initFlags & 0x200) {
        // SDL_INIT_JOYSTICK
        // Firefox will not give us Joystick data unless we register this NOP
        // callback.
        // https://bugzilla.mozilla.org/show_bug.cgi?id=936104
        addEventListener("gamepadconnected", function() {});
      }
      window.addEventListener("unload", SDL.receiveEvent);
      SDL.keyboardState = _malloc(0x10000); // Our SDL needs 512, but 64K is safe for older SDLs
      _memset(SDL.keyboardState, 0, 0x10000);
      // Initialize this structure carefully for closure
      SDL.DOMEventToSDLEvent['keydown'] = 0x300 /* SDL_KEYDOWN */;
      SDL.DOMEventToSDLEvent['keyup'] = 0x301 /* SDL_KEYUP */;
      SDL.DOMEventToSDLEvent['keypress'] = 0x303 /* SDL_TEXTINPUT */;
      SDL.DOMEventToSDLEvent['mousedown'] = 0x401 /* SDL_MOUSEBUTTONDOWN */;
      SDL.DOMEventToSDLEvent['mouseup'] = 0x402 /* SDL_MOUSEBUTTONUP */;
      SDL.DOMEventToSDLEvent['mousemove'] = 0x400 /* SDL_MOUSEMOTION */;
      SDL.DOMEventToSDLEvent['unload'] = 0x100 /* SDL_QUIT */;
      SDL.DOMEventToSDLEvent['resize'] = 0x7001 /* SDL_VIDEORESIZE/SDL_EVENT_COMPAT2 */;
      // These are not technically DOM events; the HTML gamepad API is poll-based.
      // However, we define them here, as the rest of the SDL code assumes that
      // all SDL events originate as DOM events.
      SDL.DOMEventToSDLEvent['joystick_axis_motion'] = 0x600 /* SDL_JOYAXISMOTION */;
      SDL.DOMEventToSDLEvent['joystick_button_down'] = 0x603 /* SDL_JOYBUTTONDOWN */;
      SDL.DOMEventToSDLEvent['joystick_button_up'] = 0x604 /* SDL_JOYBUTTONUP */;
      return 0; // success
    }
  function _SDL_EnableUNICODE(on) {
      var ret = SDL.unicode || 0;
      SDL.unicode = on;
      return ret;
    }
  function _emscripten_set_main_loop(func, fps, simulateInfiniteLoop) {
      Module['noExitRuntime'] = true;
      Browser.mainLoop.runner = function Browser_mainLoop_runner() {
        if (ABORT) return;
        if (Browser.mainLoop.queue.length > 0) {
          var start = Date.now();
          var blocker = Browser.mainLoop.queue.shift();
          blocker.func(blocker.arg);
          if (Browser.mainLoop.remainingBlockers) {
            var remaining = Browser.mainLoop.remainingBlockers;
            var next = remaining%1 == 0 ? remaining-1 : Math.floor(remaining);
            if (blocker.counted) {
              Browser.mainLoop.remainingBlockers = next;
            } else {
              // not counted, but move the progress along a tiny bit
              next = next + 0.5; // do not steal all the next one's progress
              Browser.mainLoop.remainingBlockers = (8*remaining + next)/9;
            }
          }
          console.log('main loop blocker "' + blocker.name + '" took ' + (Date.now() - start) + ' ms'); //, left: ' + Browser.mainLoop.remainingBlockers);
          Browser.mainLoop.updateStatus();
          setTimeout(Browser.mainLoop.runner, 0);
          return;
        }
        if (Browser.mainLoop.shouldPause) {
          // catch pauses from non-main loop sources
          Browser.mainLoop.paused = true;
          Browser.mainLoop.shouldPause = false;
          return;
        }
        // Signal GL rendering layer that processing of a new frame is about to start. This helps it optimize
        // VBO double-buffering and reduce GPU stalls.
        if (Module['preMainLoop']) {
          Module['preMainLoop']();
        }
        try {
          Runtime.dynCall('v', func);
        } catch (e) {
          if (e instanceof ExitStatus) {
            return;
          } else {
            if (e && typeof e === 'object' && e.stack) Module.printErr('exception thrown: ' + [e, e.stack]);
            throw e;
          }
        }
        if (Module['postMainLoop']) {
          Module['postMainLoop']();
        }
        if (Browser.mainLoop.shouldPause) {
          // catch pauses from the main loop itself
          Browser.mainLoop.paused = true;
          Browser.mainLoop.shouldPause = false;
          return;
        }
        Browser.mainLoop.scheduler();
      }
      if (fps && fps > 0) {
        Browser.mainLoop.scheduler = function Browser_mainLoop_scheduler() {
          setTimeout(Browser.mainLoop.runner, 1000/fps); // doing this each time means that on exception, we stop
        }
      } else {
        Browser.mainLoop.scheduler = function Browser_mainLoop_scheduler() {
          Browser.requestAnimationFrame(Browser.mainLoop.runner);
        }
      }
      Browser.mainLoop.scheduler();
      if (simulateInfiniteLoop) {
        throw 'SimulateInfiniteLoop';
      }
    }
  function _llvm_eh_exception() {
      return HEAP32[((_llvm_eh_exception.buf)>>2)];
    }
  function __ZSt18uncaught_exceptionv() { // std::uncaught_exception()
      return !!__ZSt18uncaught_exceptionv.uncaught_exception;
    }
  function ___cxa_is_number_type(type) {
      var isNumber = false;
      try { if (type == __ZTIi) isNumber = true } catch(e){}
      try { if (type == __ZTIj) isNumber = true } catch(e){}
      try { if (type == __ZTIl) isNumber = true } catch(e){}
      try { if (type == __ZTIm) isNumber = true } catch(e){}
      try { if (type == __ZTIx) isNumber = true } catch(e){}
      try { if (type == __ZTIy) isNumber = true } catch(e){}
      try { if (type == __ZTIf) isNumber = true } catch(e){}
      try { if (type == __ZTId) isNumber = true } catch(e){}
      try { if (type == __ZTIe) isNumber = true } catch(e){}
      try { if (type == __ZTIc) isNumber = true } catch(e){}
      try { if (type == __ZTIa) isNumber = true } catch(e){}
      try { if (type == __ZTIh) isNumber = true } catch(e){}
      try { if (type == __ZTIs) isNumber = true } catch(e){}
      try { if (type == __ZTIt) isNumber = true } catch(e){}
      return isNumber;
    }function ___cxa_does_inherit(definiteType, possibilityType, possibility) {
      if (possibility == 0) return false;
      if (possibilityType == 0 || possibilityType == definiteType)
        return true;
      var possibility_type_info;
      if (___cxa_is_number_type(possibilityType)) {
        possibility_type_info = possibilityType;
      } else {
        var possibility_type_infoAddr = HEAP32[((possibilityType)>>2)] - 8;
        possibility_type_info = HEAP32[((possibility_type_infoAddr)>>2)];
      }
      switch (possibility_type_info) {
      case 0: // possibility is a pointer
        // See if definite type is a pointer
        var definite_type_infoAddr = HEAP32[((definiteType)>>2)] - 8;
        var definite_type_info = HEAP32[((definite_type_infoAddr)>>2)];
        if (definite_type_info == 0) {
          // Also a pointer; compare base types of pointers
          var defPointerBaseAddr = definiteType+8;
          var defPointerBaseType = HEAP32[((defPointerBaseAddr)>>2)];
          var possPointerBaseAddr = possibilityType+8;
          var possPointerBaseType = HEAP32[((possPointerBaseAddr)>>2)];
          return ___cxa_does_inherit(defPointerBaseType, possPointerBaseType, possibility);
        } else
          return false; // one pointer and one non-pointer
      case 1: // class with no base class
        return false;
      case 2: // class with base class
        var parentTypeAddr = possibilityType + 8;
        var parentType = HEAP32[((parentTypeAddr)>>2)];
        return ___cxa_does_inherit(definiteType, parentType, possibility);
      default:
        return false; // some unencountered type
      }
    }
  function ___resumeException(ptr) {
      if (HEAP32[((_llvm_eh_exception.buf)>>2)] == 0) HEAP32[((_llvm_eh_exception.buf)>>2)]=ptr;
      throw ptr + " - Exception catching is disabled, this exception cannot be caught. Compile with -s DISABLE_EXCEPTION_CATCHING=0 or DISABLE_EXCEPTION_CATCHING=2 to catch.";;
    }function ___cxa_find_matching_catch(thrown, throwntype) {
      if (thrown == -1) thrown = HEAP32[((_llvm_eh_exception.buf)>>2)];
      if (throwntype == -1) throwntype = HEAP32[(((_llvm_eh_exception.buf)+(4))>>2)];
      var typeArray = Array.prototype.slice.call(arguments, 2);
      // If throwntype is a pointer, this means a pointer has been
      // thrown. When a pointer is thrown, actually what's thrown
      // is a pointer to the pointer. We'll dereference it.
      if (throwntype != 0 && !___cxa_is_number_type(throwntype)) {
        var throwntypeInfoAddr= HEAP32[((throwntype)>>2)] - 8;
        var throwntypeInfo= HEAP32[((throwntypeInfoAddr)>>2)];
        if (throwntypeInfo == 0)
          thrown = HEAP32[((thrown)>>2)];
      }
      // The different catch blocks are denoted by different types.
      // Due to inheritance, those types may not precisely match the
      // type of the thrown object. Find one which matches, and
      // return the type of the catch block which should be called.
      for (var i = 0; i < typeArray.length; i++) {
        if (___cxa_does_inherit(typeArray[i], throwntype, thrown))
          return ((asm["setTempRet0"](typeArray[i]),thrown)|0);
      }
      // Shouldn't happen unless we have bogus data in typeArray
      // or encounter a type for which emscripten doesn't have suitable
      // typeinfo defined. Best-efforts match just in case.
      return ((asm["setTempRet0"](throwntype),thrown)|0);
    }function ___gxx_personality_v0() {
    }
  function _fputs(s, stream) {
      // int fputs(const char *restrict s, FILE *restrict stream);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/fputs.html
      return _write(stream, s, _strlen(s));
    }
  function _fputc(c, stream) {
      // int fputc(int c, FILE *stream);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/fputc.html
      var chr = unSign(c & 0xFF);
      HEAP8[((_fputc.ret)|0)]=chr
      var ret = _write(stream, _fputc.ret, 1);
      if (ret == -1) {
        var streamObj = FS.getStream(stream);
        if (streamObj) streamObj.error = true;
        return -1;
      } else {
        return chr;
      }
    }function _puts(s) {
      // int puts(const char *s);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/puts.html
      // NOTE: puts() always writes an extra newline.
      var stdout = HEAP32[((_stdout)>>2)];
      var ret = _fputs(s, stdout);
      if (ret < 0) {
        return ret;
      } else {
        var newlineRet = _fputc(10, stdout);
        return (newlineRet < 0) ? -1 : ret + 1;
      }
    }
  function _strerror_r(errnum, strerrbuf, buflen) {
      if (errnum in ERRNO_MESSAGES) {
        if (ERRNO_MESSAGES[errnum].length > buflen - 1) {
          return ___setErrNo(ERRNO_CODES.ERANGE);
        } else {
          var msg = ERRNO_MESSAGES[errnum];
          writeAsciiToMemory(msg, strerrbuf);
          return 0;
        }
      } else {
        return ___setErrNo(ERRNO_CODES.EINVAL);
      }
    }function _strerror(errnum) {
      if (!_strerror.buffer) _strerror.buffer = _malloc(256);
      _strerror_r(errnum, _strerror.buffer, 256);
      return _strerror.buffer;
    }
  function ___errno_location() {
      return ___errno_state;
    }function _perror(s) {
      // void perror(const char *s);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/perror.html
      var stdout = HEAP32[((_stdout)>>2)];
      if (s) {
        _fputs(s, stdout);
        _fputc(58, stdout);
        _fputc(32, stdout);
      }
      var errnum = HEAP32[((___errno_location())>>2)];
      _puts(_strerror(errnum));
    }
  function ___cxa_allocate_exception(size) {
      return _malloc(size);
    }
  function ___cxa_free_exception(ptr) {
      try {
        return _free(ptr);
      } catch(e) { // XXX FIXME
      }
    }
  function ___cxa_throw(ptr, type, destructor) {
      if (!___cxa_throw.initialized) {
        try {
          HEAP32[((__ZTVN10__cxxabiv119__pointer_type_infoE)>>2)]=0; // Workaround for libcxxabi integration bug
        } catch(e){}
        try {
          HEAP32[((__ZTVN10__cxxabiv117__class_type_infoE)>>2)]=1; // Workaround for libcxxabi integration bug
        } catch(e){}
        try {
          HEAP32[((__ZTVN10__cxxabiv120__si_class_type_infoE)>>2)]=2; // Workaround for libcxxabi integration bug
        } catch(e){}
        ___cxa_throw.initialized = true;
      }
      HEAP32[((_llvm_eh_exception.buf)>>2)]=ptr
      HEAP32[(((_llvm_eh_exception.buf)+(4))>>2)]=type
      HEAP32[(((_llvm_eh_exception.buf)+(8))>>2)]=destructor
      if (!("uncaught_exception" in __ZSt18uncaught_exceptionv)) {
        __ZSt18uncaught_exceptionv.uncaught_exception = 1;
      } else {
        __ZSt18uncaught_exceptionv.uncaught_exception++;
      }
      throw ptr + " - Exception catching is disabled, this exception cannot be caught. Compile with -s DISABLE_EXCEPTION_CATCHING=0 or DISABLE_EXCEPTION_CATCHING=2 to catch.";;
    }
  function _fmin(x, y) {
      return isNaN(x) ? y : isNaN(y) ? x : Math.min(x, y);
    }
  function _fmax(x, y) {
      return isNaN(x) ? y : isNaN(y) ? x : Math.max(x, y);
    }
  function _modf(x, intpart) {
      HEAPF64[((intpart)>>3)]=Math.floor(x)
      return x - HEAPF64[((intpart)>>3)];
    }
  function _round(x) {
      return (x < 0) ? -Math.round(-x) : Math.round(x);
    }
  function __getFloat(text) {
      return /^[+-]?[0-9]*\.?[0-9]+([eE][+-]?[0-9]+)?/.exec(text);
    }function __scanString(format, get, unget, varargs) {
      if (!__scanString.whiteSpace) {
        __scanString.whiteSpace = {};
        __scanString.whiteSpace[32] = 1;
        __scanString.whiteSpace[9] = 1;
        __scanString.whiteSpace[10] = 1;
        __scanString.whiteSpace[11] = 1;
        __scanString.whiteSpace[12] = 1;
        __scanString.whiteSpace[13] = 1;
      }
      // Supports %x, %4x, %d.%d, %lld, %s, %f, %lf.
      // TODO: Support all format specifiers.
      format = Pointer_stringify(format);
      var soFar = 0;
      if (format.indexOf('%n') >= 0) {
        // need to track soFar
        var _get = get;
        get = function get() {
          soFar++;
          return _get();
        }
        var _unget = unget;
        unget = function unget() {
          soFar--;
          return _unget();
        }
      }
      var formatIndex = 0;
      var argsi = 0;
      var fields = 0;
      var argIndex = 0;
      var next;
      mainLoop:
      for (var formatIndex = 0; formatIndex < format.length;) {
        if (format[formatIndex] === '%' && format[formatIndex+1] == 'n') {
          var argPtr = HEAP32[(((varargs)+(argIndex))>>2)];
          argIndex += Runtime.getAlignSize('void*', null, true);
          HEAP32[((argPtr)>>2)]=soFar;
          formatIndex += 2;
          continue;
        }
        if (format[formatIndex] === '%') {
          var nextC = format.indexOf('c', formatIndex+1);
          if (nextC > 0) {
            var maxx = 1;
            if (nextC > formatIndex+1) {
              var sub = format.substring(formatIndex+1, nextC);
              maxx = parseInt(sub);
              if (maxx != sub) maxx = 0;
            }
            if (maxx) {
              var argPtr = HEAP32[(((varargs)+(argIndex))>>2)];
              argIndex += Runtime.getAlignSize('void*', null, true);
              fields++;
              for (var i = 0; i < maxx; i++) {
                next = get();
                HEAP8[((argPtr++)|0)]=next;
              }
              formatIndex += nextC - formatIndex + 1;
              continue;
            }
          }
        }
        // handle %[...]
        if (format[formatIndex] === '%' && format.indexOf('[', formatIndex+1) > 0) {
          var match = /\%([0-9]*)\[(\^)?(\]?[^\]]*)\]/.exec(format.substring(formatIndex));
          if (match) {
            var maxNumCharacters = parseInt(match[1]) || Infinity;
            var negateScanList = (match[2] === '^');
            var scanList = match[3];
            // expand "middle" dashs into character sets
            var middleDashMatch;
            while ((middleDashMatch = /([^\-])\-([^\-])/.exec(scanList))) {
              var rangeStartCharCode = middleDashMatch[1].charCodeAt(0);
              var rangeEndCharCode = middleDashMatch[2].charCodeAt(0);
              for (var expanded = ''; rangeStartCharCode <= rangeEndCharCode; expanded += String.fromCharCode(rangeStartCharCode++));
              scanList = scanList.replace(middleDashMatch[1] + '-' + middleDashMatch[2], expanded);
            }
            var argPtr = HEAP32[(((varargs)+(argIndex))>>2)];
            argIndex += Runtime.getAlignSize('void*', null, true);
            fields++;
            for (var i = 0; i < maxNumCharacters; i++) {
              next = get();
              if (negateScanList) {
                if (scanList.indexOf(String.fromCharCode(next)) < 0) {
                  HEAP8[((argPtr++)|0)]=next;
                } else {
                  unget();
                  break;
                }
              } else {
                if (scanList.indexOf(String.fromCharCode(next)) >= 0) {
                  HEAP8[((argPtr++)|0)]=next;
                } else {
                  unget();
                  break;
                }
              }
            }
            // write out null-terminating character
            HEAP8[((argPtr++)|0)]=0;
            formatIndex += match[0].length;
            continue;
          }
        }      
        // remove whitespace
        while (1) {
          next = get();
          if (next == 0) return fields;
          if (!(next in __scanString.whiteSpace)) break;
        }
        unget();
        if (format[formatIndex] === '%') {
          formatIndex++;
          var suppressAssignment = false;
          if (format[formatIndex] == '*') {
            suppressAssignment = true;
            formatIndex++;
          }
          var maxSpecifierStart = formatIndex;
          while (format[formatIndex].charCodeAt(0) >= 48 &&
                 format[formatIndex].charCodeAt(0) <= 57) {
            formatIndex++;
          }
          var max_;
          if (formatIndex != maxSpecifierStart) {
            max_ = parseInt(format.slice(maxSpecifierStart, formatIndex), 10);
          }
          var long_ = false;
          var half = false;
          var longLong = false;
          if (format[formatIndex] == 'l') {
            long_ = true;
            formatIndex++;
            if (format[formatIndex] == 'l') {
              longLong = true;
              formatIndex++;
            }
          } else if (format[formatIndex] == 'h') {
            half = true;
            formatIndex++;
          }
          var type = format[formatIndex];
          formatIndex++;
          var curr = 0;
          var buffer = [];
          // Read characters according to the format. floats are trickier, they may be in an unfloat state in the middle, then be a valid float later
          if (type == 'f' || type == 'e' || type == 'g' ||
              type == 'F' || type == 'E' || type == 'G') {
            next = get();
            while (next > 0 && (!(next in __scanString.whiteSpace)))  {
              buffer.push(String.fromCharCode(next));
              next = get();
            }
            var m = __getFloat(buffer.join(''));
            var last = m ? m[0].length : 0;
            for (var i = 0; i < buffer.length - last + 1; i++) {
              unget();
            }
            buffer.length = last;
          } else {
            next = get();
            var first = true;
            // Strip the optional 0x prefix for %x.
            if ((type == 'x' || type == 'X') && (next == 48)) {
              var peek = get();
              if (peek == 120 || peek == 88) {
                next = get();
              } else {
                unget();
              }
            }
            while ((curr < max_ || isNaN(max_)) && next > 0) {
              if (!(next in __scanString.whiteSpace) && // stop on whitespace
                  (type == 's' ||
                   ((type === 'd' || type == 'u' || type == 'i') && ((next >= 48 && next <= 57) ||
                                                                     (first && next == 45))) ||
                   ((type === 'x' || type === 'X') && (next >= 48 && next <= 57 ||
                                     next >= 97 && next <= 102 ||
                                     next >= 65 && next <= 70))) &&
                  (formatIndex >= format.length || next !== format[formatIndex].charCodeAt(0))) { // Stop when we read something that is coming up
                buffer.push(String.fromCharCode(next));
                next = get();
                curr++;
                first = false;
              } else {
                break;
              }
            }
            unget();
          }
          if (buffer.length === 0) return 0;  // Failure.
          if (suppressAssignment) continue;
          var text = buffer.join('');
          var argPtr = HEAP32[(((varargs)+(argIndex))>>2)];
          argIndex += Runtime.getAlignSize('void*', null, true);
          switch (type) {
            case 'd': case 'u': case 'i':
              if (half) {
                HEAP16[((argPtr)>>1)]=parseInt(text, 10);
              } else if (longLong) {
                (tempI64 = [parseInt(text, 10)>>>0,(tempDouble=parseInt(text, 10),(+(Math_abs(tempDouble))) >= (+1) ? (tempDouble > (+0) ? ((Math_min((+(Math_floor((tempDouble)/(+4294967296)))), (+4294967295)))|0)>>>0 : (~~((+(Math_ceil((tempDouble - +(((~~(tempDouble)))>>>0))/(+4294967296))))))>>>0) : 0)],HEAP32[((argPtr)>>2)]=tempI64[0],HEAP32[(((argPtr)+(4))>>2)]=tempI64[1]);
              } else {
                HEAP32[((argPtr)>>2)]=parseInt(text, 10);
              }
              break;
            case 'X':
            case 'x':
              HEAP32[((argPtr)>>2)]=parseInt(text, 16)
              break;
            case 'F':
            case 'f':
            case 'E':
            case 'e':
            case 'G':
            case 'g':
            case 'E':
              // fallthrough intended
              if (long_) {
                HEAPF64[((argPtr)>>3)]=parseFloat(text)
              } else {
                HEAPF32[((argPtr)>>2)]=parseFloat(text)
              }
              break;
            case 's':
              var array = intArrayFromString(text);
              for (var j = 0; j < array.length; j++) {
                HEAP8[(((argPtr)+(j))|0)]=array[j]
              }
              break;
          }
          fields++;
        } else if (format[formatIndex].charCodeAt(0) in __scanString.whiteSpace) {
          next = get();
          while (next in __scanString.whiteSpace) {
            if (next <= 0) break mainLoop;  // End of input.
            next = get();
          }
          unget(next);
          formatIndex++;
        } else {
          // Not a specifier.
          next = get();
          if (format[formatIndex].charCodeAt(0) !== next) {
            unget(next);
            break mainLoop;
          }
          formatIndex++;
        }
      }
      return fields;
    }function _sscanf(s, format, varargs) {
      // int sscanf(const char *restrict s, const char *restrict format, ... );
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/scanf.html
      var index = 0;
      function get() { return HEAP8[(((s)+(index++))|0)]; };
      function unget() { index--; };
      return __scanString(format, get, unget, varargs);
    }
  function _stat(path, buf, dontResolveLastLink) {
      // http://pubs.opengroup.org/onlinepubs/7908799/xsh/stat.html
      // int stat(const char *path, struct stat *buf);
      // NOTE: dontResolveLastLink is a shortcut for lstat(). It should never be
      //       used in client code.
      path = typeof path !== 'string' ? Pointer_stringify(path) : path;
      try {
        var stat = dontResolveLastLink ? FS.lstat(path) : FS.stat(path);
        HEAP32[((buf)>>2)]=stat.dev;
        HEAP32[(((buf)+(4))>>2)]=0;
        HEAP32[(((buf)+(8))>>2)]=stat.ino;
        HEAP32[(((buf)+(12))>>2)]=stat.mode
        HEAP32[(((buf)+(16))>>2)]=stat.nlink
        HEAP32[(((buf)+(20))>>2)]=stat.uid
        HEAP32[(((buf)+(24))>>2)]=stat.gid
        HEAP32[(((buf)+(28))>>2)]=stat.rdev
        HEAP32[(((buf)+(32))>>2)]=0;
        HEAP32[(((buf)+(36))>>2)]=stat.size
        HEAP32[(((buf)+(40))>>2)]=4096
        HEAP32[(((buf)+(44))>>2)]=stat.blocks
        HEAP32[(((buf)+(48))>>2)]=Math.floor(stat.atime.getTime() / 1000)
        HEAP32[(((buf)+(52))>>2)]=0
        HEAP32[(((buf)+(56))>>2)]=Math.floor(stat.mtime.getTime() / 1000)
        HEAP32[(((buf)+(60))>>2)]=0
        HEAP32[(((buf)+(64))>>2)]=Math.floor(stat.ctime.getTime() / 1000)
        HEAP32[(((buf)+(68))>>2)]=0
        HEAP32[(((buf)+(72))>>2)]=stat.ino
        return 0;
      } catch (e) {
        FS.handleFSError(e);
        return -1;
      }
    }function _fstat(fildes, buf) {
      // int fstat(int fildes, struct stat *buf);
      // http://pubs.opengroup.org/onlinepubs/7908799/xsh/fstat.html
      var stream = FS.getStream(fildes);
      if (!stream) {
        ___setErrNo(ERRNO_CODES.EBADF);
        return -1;
      }
      return _stat(stream.path, buf);
    }
  function _mmap(start, num, prot, flags, stream, offset) {
      /* FIXME: Since mmap is normally implemented at the kernel level,
       * this implementation simply uses malloc underneath the call to
       * mmap.
       */
      var MAP_PRIVATE = 2;
      var ptr;
      var allocated = false;
      if (!_mmap.mappings) _mmap.mappings = {};
      if (stream == -1) {
        ptr = _malloc(num);
        if (!ptr) return -1;
        _memset(ptr, 0, num);
        allocated = true;
      } else {
        var info = FS.getStream(stream);
        if (!info) return -1;
        try {
          var res = FS.mmap(info, HEAPU8, start, num, offset, prot, flags);
          ptr = res.ptr;
          allocated = res.allocated;
        } catch (e) {
          FS.handleFSError(e);
          return -1;
        }
      }
      _mmap.mappings[ptr] = { malloc: ptr, num: num, allocated: allocated };
      return ptr;
    }
  function _close(fildes) {
      // int close(int fildes);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/close.html
      var stream = FS.getStream(fildes);
      if (!stream) {
        ___setErrNo(ERRNO_CODES.EBADF);
        return -1;
      }
      try {
        FS.close(stream);
        return 0;
      } catch (e) {
        FS.handleFSError(e);
        return -1;
      }
    }
  function _strncmp(px, py, n) {
      var i = 0;
      while (i < n) {
        var x = HEAPU8[(((px)+(i))|0)];
        var y = HEAPU8[(((py)+(i))|0)];
        if (x == y && x == 0) return 0;
        if (x == 0) return -1;
        if (y == 0) return 1;
        if (x == y) {
          i ++;
          continue;
        } else {
          return x > y ? 1 : -1;
        }
      }
      return 0;
    }function _strcmp(px, py) {
      return _strncmp(px, py, TOTAL_MEMORY);
    }
  function _open(path, oflag, varargs) {
      // int open(const char *path, int oflag, ...);
      // http://pubs.opengroup.org/onlinepubs/009695399/functions/open.html
      var mode = HEAP32[((varargs)>>2)];
      path = Pointer_stringify(path);
      try {
        var stream = FS.open(path, oflag, mode);
        return stream.fd;
      } catch (e) {
        FS.handleFSError(e);
        return -1;
      }
    }
  function _recv(fd, buf, len, flags) {
      var sock = SOCKFS.getSocket(fd);
      if (!sock) {
        ___setErrNo(ERRNO_CODES.EBADF);
        return -1;
      }
      // TODO honor flags
      return _read(fd, buf, len);
    }
  function _pread(fildes, buf, nbyte, offset) {
      // ssize_t pread(int fildes, void *buf, size_t nbyte, off_t offset);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/read.html
      var stream = FS.getStream(fildes);
      if (!stream) {
        ___setErrNo(ERRNO_CODES.EBADF);
        return -1;
      }
      try {
        var slab = HEAP8;
        return FS.read(stream, slab, buf, nbyte, offset);
      } catch (e) {
        FS.handleFSError(e);
        return -1;
      }
    }function _read(fildes, buf, nbyte) {
      // ssize_t read(int fildes, void *buf, size_t nbyte);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/read.html
      var stream = FS.getStream(fildes);
      if (!stream) {
        ___setErrNo(ERRNO_CODES.EBADF);
        return -1;
      }
      try {
        var slab = HEAP8;
        return FS.read(stream, slab, buf, nbyte);
      } catch (e) {
        FS.handleFSError(e);
        return -1;
      }
    }
  Module["_memcmp"] = _memcmp;
  function ___cxa_guard_acquire(variable) {
      if (!HEAP8[(variable)]) { // ignore SAFE_HEAP stuff because llvm mixes i64 and i8 here
        HEAP8[(variable)]=1;
        return 1;
      }
      return 0;
    }
  function ___cxa_guard_release() {}
  function _strdup(ptr) {
      var len = _strlen(ptr);
      var newStr = _malloc(len + 1);
      (_memcpy(newStr, ptr, len)|0);
      HEAP8[(((newStr)+(len))|0)]=0;
      return newStr;
    }
  Module["_memset"] = _memset;var _llvm_memset_p0i8_i32=_memset;
  function _hypot(a, b) {
       return Math.sqrt(a*a + b*b);
    }var _hypotf=_hypot;
  var _fabsf=Math_abs;
  var _fminf=_fmin;
  var _fmaxf=_fmax;
  var _floorf=Math_floor;
  var _ceilf=Math_ceil;
  var _sqrtf=Math_sqrt;
  function _rint(x) {
      if (Math.abs(x % 1) !== 0.5) return Math.round(x);
      return x + x % 2 + ((x < 0) ? 1 : -1);
    }var _lrint=_rint;
  var _lrintf=_rint;
  var _llvm_va_start=undefined;
  function _snprintf(s, n, format, varargs) {
      // int snprintf(char *restrict s, size_t n, const char *restrict format, ...);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/printf.html
      var result = __formatString(format, varargs);
      var limit = (n === undefined) ? result.length
                                    : Math.min(result.length, Math.max(n - 1, 0));
      if (s < 0) {
        s = -s;
        var buf = _malloc(limit+1);
        HEAP32[((s)>>2)]=buf;
        s = buf;
      }
      for (var i = 0; i < limit; i++) {
        HEAP8[(((s)+(i))|0)]=result[i];
      }
      if (limit < n || (n === undefined)) HEAP8[(((s)+(i))|0)]=0;
      return result.length;
    }function _sprintf(s, format, varargs) {
      // int sprintf(char *restrict s, const char *restrict format, ...);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/printf.html
      return _snprintf(s, undefined, format, varargs);
    }function _vsprintf(s, format, va_arg) {
      return _sprintf(s, format, HEAP32[((va_arg)>>2)]);
    }
  function _llvm_va_end() {}
  function _isspace(chr) {
      return (chr == 32) || (chr >= 9 && chr <= 13);
    }function __parseInt(str, endptr, base, min, max, bits, unsign) {
      // Skip space.
      while (_isspace(HEAP8[(str)])) str++;
      // Check for a plus/minus sign.
      var multiplier = 1;
      if (HEAP8[(str)] == 45) {
        multiplier = -1;
        str++;
      } else if (HEAP8[(str)] == 43) {
        str++;
      }
      // Find base.
      var finalBase = base;
      if (!finalBase) {
        if (HEAP8[(str)] == 48) {
          if (HEAP8[((str+1)|0)] == 120 ||
              HEAP8[((str+1)|0)] == 88) {
            finalBase = 16;
            str += 2;
          } else {
            finalBase = 8;
            str++;
          }
        }
      } else if (finalBase==16) {
        if (HEAP8[(str)] == 48) {
          if (HEAP8[((str+1)|0)] == 120 ||
              HEAP8[((str+1)|0)] == 88) {
            str += 2;
          }
        }
      }
      if (!finalBase) finalBase = 10;
      // Get digits.
      var chr;
      var ret = 0;
      while ((chr = HEAP8[(str)]) != 0) {
        var digit = parseInt(String.fromCharCode(chr), finalBase);
        if (isNaN(digit)) {
          break;
        } else {
          ret = ret * finalBase + digit;
          str++;
        }
      }
      // Apply sign.
      ret *= multiplier;
      // Set end pointer.
      if (endptr) {
        HEAP32[((endptr)>>2)]=str
      }
      // Unsign if needed.
      if (unsign) {
        if (Math.abs(ret) > max) {
          ret = max;
          ___setErrNo(ERRNO_CODES.ERANGE);
        } else {
          ret = unSign(ret, bits);
        }
      }
      // Validate range.
      if (ret > max || ret < min) {
        ret = ret > max ? max : min;
        ___setErrNo(ERRNO_CODES.ERANGE);
      }
      if (bits == 64) {
        return ((asm["setTempRet0"]((tempDouble=ret,(+(Math_abs(tempDouble))) >= (+1) ? (tempDouble > (+0) ? ((Math_min((+(Math_floor((tempDouble)/(+4294967296)))), (+4294967295)))|0)>>>0 : (~~((+(Math_ceil((tempDouble - +(((~~(tempDouble)))>>>0))/(+4294967296))))))>>>0) : 0)),ret>>>0)|0);
      }
      return ret;
    }function _strtoul(str, endptr, base) {
      return __parseInt(str, endptr, base, 0, 4294967295, 32, true);  // ULONG_MAX.
    }
  var ___strtok_state=0;
  function _strtok_r(s, delim, lasts) {
      var skip_leading_delim = 1;
      var spanp;
      var c, sc;
      var tok;
      if (s == 0 && (s = getValue(lasts, 'i8*')) == 0) {
        return 0;
      }
      cont: while (1) {
        c = getValue(s++, 'i8');
        for (spanp = delim; (sc = getValue(spanp++, 'i8')) != 0;) {
          if (c == sc) {
            if (skip_leading_delim) {
              continue cont;
            } else {
              setValue(lasts, s, 'i8*');
              setValue(s - 1, 0, 'i8');
              return s - 1;
            }
          }
        }
        break;
      }
      if (c == 0) {
        setValue(lasts, 0, 'i8*');
        return 0;
      }
      tok = s - 1;
      for (;;) {
        c = getValue(s++, 'i8');
        spanp = delim;
        do {
          if ((sc = getValue(spanp++, 'i8')) == c) {
            if (c == 0) {
              s = 0;
            } else {
              setValue(s - 1, 0, 'i8');
            }
            setValue(lasts, s, 'i8*');
            return tok;
          }
        } while (sc != 0);
      }
      abort('strtok_r error!');
    }function _strtok(s, delim) {
      return _strtok_r(s, delim, ___strtok_state);
    }
  Module["_tolower"] = _tolower;
  function _log10(x) {
      return Math.log(x) / Math.LN10;
    }
  var _floor=Math_floor;
  var _llvm_pow_f64=Math_pow;
  var _log10l=_log10;
  function _fmod(x, y) {
      return x % y;
    }var _fmodl=_fmod;
  var _fabsl=Math_abs;
  var _floorl=Math_floor;
  var ___tm_current=allocate(44, "i8", ALLOC_STATIC);
  var ___tm_timezone=allocate(intArrayFromString("GMT"), "i8", ALLOC_STATIC);function _gmtime_r(time, tmPtr) {
      var date = new Date(HEAP32[((time)>>2)]*1000);
      HEAP32[((tmPtr)>>2)]=date.getUTCSeconds()
      HEAP32[(((tmPtr)+(4))>>2)]=date.getUTCMinutes()
      HEAP32[(((tmPtr)+(8))>>2)]=date.getUTCHours()
      HEAP32[(((tmPtr)+(12))>>2)]=date.getUTCDate()
      HEAP32[(((tmPtr)+(16))>>2)]=date.getUTCMonth()
      HEAP32[(((tmPtr)+(20))>>2)]=date.getUTCFullYear()-1900
      HEAP32[(((tmPtr)+(24))>>2)]=date.getUTCDay()
      HEAP32[(((tmPtr)+(36))>>2)]=0
      HEAP32[(((tmPtr)+(32))>>2)]=0
      var start = new Date(date); // define date using UTC, start from Jan 01 00:00:00 UTC
      start.setUTCDate(1);
      start.setUTCMonth(0);
      start.setUTCHours(0);
      start.setUTCMinutes(0);
      start.setUTCSeconds(0);
      start.setUTCMilliseconds(0);
      var yday = Math.floor((date.getTime() - start.getTime()) / (1000 * 60 * 60 * 24));
      HEAP32[(((tmPtr)+(28))>>2)]=yday
      HEAP32[(((tmPtr)+(40))>>2)]=___tm_timezone
      return tmPtr;
    }function _gmtime(time) {
      return _gmtime_r(time, ___tm_current);
    }
  function __isLeapYear(year) {
        return year%4 === 0 && (year%100 !== 0 || year%400 === 0);
    }
  function __arraySum(array, index) {
      var sum = 0;
      for (var i = 0; i <= index; sum += array[i++]);
      return sum;
    }
  var __MONTH_DAYS_LEAP=[31,29,31,30,31,30,31,31,30,31,30,31];
  var __MONTH_DAYS_REGULAR=[31,28,31,30,31,30,31,31,30,31,30,31];function __addDays(date, days) {
      var newDate = new Date(date.getTime());
      while(days > 0) {
        var leap = __isLeapYear(newDate.getFullYear());
        var currentMonth = newDate.getMonth();
        var daysInCurrentMonth = (leap ? __MONTH_DAYS_LEAP : __MONTH_DAYS_REGULAR)[currentMonth];
        if (days > daysInCurrentMonth-newDate.getDate()) {
          // we spill over to next month
          days -= (daysInCurrentMonth-newDate.getDate()+1);
          newDate.setDate(1);
          if (currentMonth < 11) {
            newDate.setMonth(currentMonth+1)
          } else {
            newDate.setMonth(0);
            newDate.setFullYear(newDate.getFullYear()+1);
          }
        } else {
          // we stay in current month 
          newDate.setDate(newDate.getDate()+days);
          return newDate;
        }
      }
      return newDate;
    }function _strftime(s, maxsize, format, tm) {
      // size_t strftime(char *restrict s, size_t maxsize, const char *restrict format, const struct tm *restrict timeptr);
      // http://pubs.opengroup.org/onlinepubs/009695399/functions/strftime.html
      var date = {
        tm_sec: HEAP32[((tm)>>2)],
        tm_min: HEAP32[(((tm)+(4))>>2)],
        tm_hour: HEAP32[(((tm)+(8))>>2)],
        tm_mday: HEAP32[(((tm)+(12))>>2)],
        tm_mon: HEAP32[(((tm)+(16))>>2)],
        tm_year: HEAP32[(((tm)+(20))>>2)],
        tm_wday: HEAP32[(((tm)+(24))>>2)],
        tm_yday: HEAP32[(((tm)+(28))>>2)],
        tm_isdst: HEAP32[(((tm)+(32))>>2)]
      };
      var pattern = Pointer_stringify(format);
      // expand format
      var EXPANSION_RULES_1 = {
        '%c': '%a %b %d %H:%M:%S %Y',     // Replaced by the locale's appropriate date and time representation - e.g., Mon Aug  3 14:02:01 2013
        '%D': '%m/%d/%y',                 // Equivalent to %m / %d / %y
        '%F': '%Y-%m-%d',                 // Equivalent to %Y - %m - %d
        '%h': '%b',                       // Equivalent to %b
        '%r': '%I:%M:%S %p',              // Replaced by the time in a.m. and p.m. notation
        '%R': '%H:%M',                    // Replaced by the time in 24-hour notation
        '%T': '%H:%M:%S',                 // Replaced by the time
        '%x': '%m/%d/%y',                 // Replaced by the locale's appropriate date representation
        '%X': '%H:%M:%S',                 // Replaced by the locale's appropriate date representation
      };
      for (var rule in EXPANSION_RULES_1) {
        pattern = pattern.replace(new RegExp(rule, 'g'), EXPANSION_RULES_1[rule]);
      }
      var WEEKDAYS = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
      var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
      function leadingSomething(value, digits, character) {
        var str = typeof value === 'number' ? value.toString() : (value || '');
        while (str.length < digits) {
          str = character[0]+str;
        }
        return str;
      };
      function leadingNulls(value, digits) {
        return leadingSomething(value, digits, '0');
      };
      function compareByDay(date1, date2) {
        function sgn(value) {
          return value < 0 ? -1 : (value > 0 ? 1 : 0);
        };
        var compare;
        if ((compare = sgn(date1.getFullYear()-date2.getFullYear())) === 0) {
          if ((compare = sgn(date1.getMonth()-date2.getMonth())) === 0) {
            compare = sgn(date1.getDate()-date2.getDate());
          }
        }
        return compare;
      };
      function getFirstWeekStartDate(janFourth) {
          switch (janFourth.getDay()) {
            case 0: // Sunday
              return new Date(janFourth.getFullYear()-1, 11, 29);
            case 1: // Monday
              return janFourth;
            case 2: // Tuesday
              return new Date(janFourth.getFullYear(), 0, 3);
            case 3: // Wednesday
              return new Date(janFourth.getFullYear(), 0, 2);
            case 4: // Thursday
              return new Date(janFourth.getFullYear(), 0, 1);
            case 5: // Friday
              return new Date(janFourth.getFullYear()-1, 11, 31);
            case 6: // Saturday
              return new Date(janFourth.getFullYear()-1, 11, 30);
          }
      };
      function getWeekBasedYear(date) {
          var thisDate = __addDays(new Date(date.tm_year+1900, 0, 1), date.tm_yday);
          var janFourthThisYear = new Date(thisDate.getFullYear(), 0, 4);
          var janFourthNextYear = new Date(thisDate.getFullYear()+1, 0, 4);
          var firstWeekStartThisYear = getFirstWeekStartDate(janFourthThisYear);
          var firstWeekStartNextYear = getFirstWeekStartDate(janFourthNextYear);
          if (compareByDay(firstWeekStartThisYear, thisDate) <= 0) {
            // this date is after the start of the first week of this year
            if (compareByDay(firstWeekStartNextYear, thisDate) <= 0) {
              return thisDate.getFullYear()+1;
            } else {
              return thisDate.getFullYear();
            }
          } else { 
            return thisDate.getFullYear()-1;
          }
      };
      var EXPANSION_RULES_2 = {
        '%a': function(date) {
          return WEEKDAYS[date.tm_wday].substring(0,3);
        },
        '%A': function(date) {
          return WEEKDAYS[date.tm_wday];
        },
        '%b': function(date) {
          return MONTHS[date.tm_mon].substring(0,3);
        },
        '%B': function(date) {
          return MONTHS[date.tm_mon];
        },
        '%C': function(date) {
          var year = date.tm_year+1900;
          return leadingNulls(Math.floor(year/100),2);
        },
        '%d': function(date) {
          return leadingNulls(date.tm_mday, 2);
        },
        '%e': function(date) {
          return leadingSomething(date.tm_mday, 2, ' ');
        },
        '%g': function(date) {
          // %g, %G, and %V give values according to the ISO 8601:2000 standard week-based year. 
          // In this system, weeks begin on a Monday and week 1 of the year is the week that includes 
          // January 4th, which is also the week that includes the first Thursday of the year, and 
          // is also the first week that contains at least four days in the year. 
          // If the first Monday of January is the 2nd, 3rd, or 4th, the preceding days are part of 
          // the last week of the preceding year; thus, for Saturday 2nd January 1999, 
          // %G is replaced by 1998 and %V is replaced by 53. If December 29th, 30th, 
          // or 31st is a Monday, it and any following days are part of week 1 of the following year. 
          // Thus, for Tuesday 30th December 1997, %G is replaced by 1998 and %V is replaced by 01.
          return getWeekBasedYear(date).toString().substring(2);
        },
        '%G': function(date) {
          return getWeekBasedYear(date);
        },
        '%H': function(date) {
          return leadingNulls(date.tm_hour, 2);
        },
        '%I': function(date) {
          return leadingNulls(date.tm_hour < 13 ? date.tm_hour : date.tm_hour-12, 2);
        },
        '%j': function(date) {
          // Day of the year (001-366)
          return leadingNulls(date.tm_mday+__arraySum(__isLeapYear(date.tm_year+1900) ? __MONTH_DAYS_LEAP : __MONTH_DAYS_REGULAR, date.tm_mon-1), 3);
        },
        '%m': function(date) {
          return leadingNulls(date.tm_mon+1, 2);
        },
        '%M': function(date) {
          return leadingNulls(date.tm_min, 2);
        },
        '%n': function() {
          return '\n';
        },
        '%p': function(date) {
          if (date.tm_hour > 0 && date.tm_hour < 13) {
            return 'AM';
          } else {
            return 'PM';
          }
        },
        '%S': function(date) {
          return leadingNulls(date.tm_sec, 2);
        },
        '%t': function() {
          return '\t';
        },
        '%u': function(date) {
          var day = new Date(date.tm_year+1900, date.tm_mon+1, date.tm_mday, 0, 0, 0, 0);
          return day.getDay() || 7;
        },
        '%U': function(date) {
          // Replaced by the week number of the year as a decimal number [00,53]. 
          // The first Sunday of January is the first day of week 1; 
          // days in the new year before this are in week 0. [ tm_year, tm_wday, tm_yday]
          var janFirst = new Date(date.tm_year+1900, 0, 1);
          var firstSunday = janFirst.getDay() === 0 ? janFirst : __addDays(janFirst, 7-janFirst.getDay());
          var endDate = new Date(date.tm_year+1900, date.tm_mon, date.tm_mday);
          // is target date after the first Sunday?
          if (compareByDay(firstSunday, endDate) < 0) {
            // calculate difference in days between first Sunday and endDate
            var februaryFirstUntilEndMonth = __arraySum(__isLeapYear(endDate.getFullYear()) ? __MONTH_DAYS_LEAP : __MONTH_DAYS_REGULAR, endDate.getMonth()-1)-31;
            var firstSundayUntilEndJanuary = 31-firstSunday.getDate();
            var days = firstSundayUntilEndJanuary+februaryFirstUntilEndMonth+endDate.getDate();
            return leadingNulls(Math.ceil(days/7), 2);
          }
          return compareByDay(firstSunday, janFirst) === 0 ? '01': '00';
        },
        '%V': function(date) {
          // Replaced by the week number of the year (Monday as the first day of the week) 
          // as a decimal number [01,53]. If the week containing 1 January has four 
          // or more days in the new year, then it is considered week 1. 
          // Otherwise, it is the last week of the previous year, and the next week is week 1. 
          // Both January 4th and the first Thursday of January are always in week 1. [ tm_year, tm_wday, tm_yday]
          var janFourthThisYear = new Date(date.tm_year+1900, 0, 4);
          var janFourthNextYear = new Date(date.tm_year+1901, 0, 4);
          var firstWeekStartThisYear = getFirstWeekStartDate(janFourthThisYear);
          var firstWeekStartNextYear = getFirstWeekStartDate(janFourthNextYear);
          var endDate = __addDays(new Date(date.tm_year+1900, 0, 1), date.tm_yday);
          if (compareByDay(endDate, firstWeekStartThisYear) < 0) {
            // if given date is before this years first week, then it belongs to the 53rd week of last year
            return '53';
          } 
          if (compareByDay(firstWeekStartNextYear, endDate) <= 0) {
            // if given date is after next years first week, then it belongs to the 01th week of next year
            return '01';
          }
          // given date is in between CW 01..53 of this calendar year
          var daysDifference;
          if (firstWeekStartThisYear.getFullYear() < date.tm_year+1900) {
            // first CW of this year starts last year
            daysDifference = date.tm_yday+32-firstWeekStartThisYear.getDate()
          } else {
            // first CW of this year starts this year
            daysDifference = date.tm_yday+1-firstWeekStartThisYear.getDate();
          }
          return leadingNulls(Math.ceil(daysDifference/7), 2);
        },
        '%w': function(date) {
          var day = new Date(date.tm_year+1900, date.tm_mon+1, date.tm_mday, 0, 0, 0, 0);
          return day.getDay();
        },
        '%W': function(date) {
          // Replaced by the week number of the year as a decimal number [00,53]. 
          // The first Monday of January is the first day of week 1; 
          // days in the new year before this are in week 0. [ tm_year, tm_wday, tm_yday]
          var janFirst = new Date(date.tm_year, 0, 1);
          var firstMonday = janFirst.getDay() === 1 ? janFirst : __addDays(janFirst, janFirst.getDay() === 0 ? 1 : 7-janFirst.getDay()+1);
          var endDate = new Date(date.tm_year+1900, date.tm_mon, date.tm_mday);
          // is target date after the first Monday?
          if (compareByDay(firstMonday, endDate) < 0) {
            var februaryFirstUntilEndMonth = __arraySum(__isLeapYear(endDate.getFullYear()) ? __MONTH_DAYS_LEAP : __MONTH_DAYS_REGULAR, endDate.getMonth()-1)-31;
            var firstMondayUntilEndJanuary = 31-firstMonday.getDate();
            var days = firstMondayUntilEndJanuary+februaryFirstUntilEndMonth+endDate.getDate();
            return leadingNulls(Math.ceil(days/7), 2);
          }
          return compareByDay(firstMonday, janFirst) === 0 ? '01': '00';
        },
        '%y': function(date) {
          // Replaced by the last two digits of the year as a decimal number [00,99]. [ tm_year]
          return (date.tm_year+1900).toString().substring(2);
        },
        '%Y': function(date) {
          // Replaced by the year as a decimal number (for example, 1997). [ tm_year]
          return date.tm_year+1900;
        },
        '%z': function(date) {
          // Replaced by the offset from UTC in the ISO 8601:2000 standard format ( +hhmm or -hhmm ),
          // or by no characters if no timezone is determinable. 
          // For example, "-0430" means 4 hours 30 minutes behind UTC (west of Greenwich). 
          // If tm_isdst is zero, the standard time offset is used. 
          // If tm_isdst is greater than zero, the daylight savings time offset is used. 
          // If tm_isdst is negative, no characters are returned. 
          // FIXME: we cannot determine time zone (or can we?)
          return '';
        },
        '%Z': function(date) {
          // Replaced by the timezone name or abbreviation, or by no bytes if no timezone information exists. [ tm_isdst]
          // FIXME: we cannot determine time zone (or can we?)
          return '';
        },
        '%%': function() {
          return '%';
        }
      };
      for (var rule in EXPANSION_RULES_2) {
        if (pattern.indexOf(rule) >= 0) {
          pattern = pattern.replace(new RegExp(rule, 'g'), EXPANSION_RULES_2[rule](date));
        }
      }
      var bytes = intArrayFromString(pattern, false);
      if (bytes.length > maxsize) {
        return 0;
      } 
      writeArrayToMemory(bytes, s);
      return bytes.length-1;
    }
  var _sqrt=Math_sqrt;
  var _llvm_memset_p0i8_i64=_memset;
  function _llvm_lifetime_start() {}
  function _llvm_lifetime_end() {}
  function _pthread_mutex_lock() {}
  function _pthread_mutex_unlock() {}
  function ___cxa_begin_catch(ptr) {
      __ZSt18uncaught_exceptionv.uncaught_exception--;
      return ptr;
    }
  function __ZSt9terminatev() {
      _exit(-1234);
    }
  function _pthread_cond_broadcast() {
      return 0;
    }
  function _pthread_cond_wait() {
      return 0;
    }
  function ___cxa_end_catch() {
      if (___cxa_end_catch.rethrown) {
        ___cxa_end_catch.rethrown = false;
        return;
      }
      // Clear state flag.
      asm['setThrew'](0);
      // Clear type.
      HEAP32[(((_llvm_eh_exception.buf)+(4))>>2)]=0
      // Call destructor if one is registered then clear it.
      var ptr = HEAP32[((_llvm_eh_exception.buf)>>2)];
      var destructor = HEAP32[(((_llvm_eh_exception.buf)+(8))>>2)];
      if (destructor) {
        Runtime.dynCall('vi', destructor, [ptr]);
        HEAP32[(((_llvm_eh_exception.buf)+(8))>>2)]=0
      }
      // Free ptr if it isn't null.
      if (ptr) {
        ___cxa_free_exception(ptr);
        HEAP32[((_llvm_eh_exception.buf)>>2)]=0
      }
    }
  function _ungetc(c, stream) {
      // int ungetc(int c, FILE *stream);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/ungetc.html
      stream = FS.getStream(stream);
      if (!stream) {
        return -1;
      }
      if (c === -1) {
        // do nothing for EOF character
        return c;
      }
      c = unSign(c & 0xFF);
      stream.ungotten.push(c);
      stream.eof = false;
      return c;
    }
  function _fread(ptr, size, nitems, stream) {
      // size_t fread(void *restrict ptr, size_t size, size_t nitems, FILE *restrict stream);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/fread.html
      var bytesToRead = nitems * size;
      if (bytesToRead == 0) {
        return 0;
      }
      var bytesRead = 0;
      var streamObj = FS.getStream(stream);
      if (!streamObj) {
        ___setErrNo(ERRNO_CODES.EBADF);
        return 0;
      }
      while (streamObj.ungotten.length && bytesToRead > 0) {
        HEAP8[((ptr++)|0)]=streamObj.ungotten.pop()
        bytesToRead--;
        bytesRead++;
      }
      var err = _read(stream, ptr, bytesToRead);
      if (err == -1) {
        if (streamObj) streamObj.error = true;
        return 0;
      }
      bytesRead += err;
      if (bytesRead < bytesToRead) streamObj.eof = true;
      return Math.floor(bytesRead / size);
    }function _fgetc(stream) {
      // int fgetc(FILE *stream);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/fgetc.html
      var streamObj = FS.getStream(stream);
      if (!streamObj) return -1;
      if (streamObj.eof || streamObj.error) return -1;
      var ret = _fread(_fgetc.ret, 1, 1, stream);
      if (ret == 0) {
        return -1;
      } else if (ret == -1) {
        streamObj.error = true;
        return -1;
      } else {
        return HEAPU8[((_fgetc.ret)|0)];
      }
    }var _getc=_fgetc;
  function _abort() {
      Module['abort']();
    }
  Module["_memmove"] = _memmove;var _llvm_memmove_p0i8_p0i8_i32=_memmove;
  function ___cxa_rethrow() {
      ___cxa_end_catch.rethrown = true;
      throw HEAP32[((_llvm_eh_exception.buf)>>2)] + " - Exception catching is disabled, this exception cannot be caught. Compile with -s DISABLE_EXCEPTION_CATCHING=0 or DISABLE_EXCEPTION_CATCHING=2 to catch.";;
    }
  function _sysconf(name) {
      // long sysconf(int name);
      // http://pubs.opengroup.org/onlinepubs/009695399/functions/sysconf.html
      switch(name) {
        case 30: return PAGE_SIZE;
        case 132:
        case 133:
        case 12:
        case 137:
        case 138:
        case 15:
        case 235:
        case 16:
        case 17:
        case 18:
        case 19:
        case 20:
        case 149:
        case 13:
        case 10:
        case 236:
        case 153:
        case 9:
        case 21:
        case 22:
        case 159:
        case 154:
        case 14:
        case 77:
        case 78:
        case 139:
        case 80:
        case 81:
        case 79:
        case 82:
        case 68:
        case 67:
        case 164:
        case 11:
        case 29:
        case 47:
        case 48:
        case 95:
        case 52:
        case 51:
        case 46:
          return 200809;
        case 27:
        case 246:
        case 127:
        case 128:
        case 23:
        case 24:
        case 160:
        case 161:
        case 181:
        case 182:
        case 242:
        case 183:
        case 184:
        case 243:
        case 244:
        case 245:
        case 165:
        case 178:
        case 179:
        case 49:
        case 50:
        case 168:
        case 169:
        case 175:
        case 170:
        case 171:
        case 172:
        case 97:
        case 76:
        case 32:
        case 173:
        case 35:
          return -1;
        case 176:
        case 177:
        case 7:
        case 155:
        case 8:
        case 157:
        case 125:
        case 126:
        case 92:
        case 93:
        case 129:
        case 130:
        case 131:
        case 94:
        case 91:
          return 1;
        case 74:
        case 60:
        case 69:
        case 70:
        case 4:
          return 1024;
        case 31:
        case 42:
        case 72:
          return 32;
        case 87:
        case 26:
        case 33:
          return 2147483647;
        case 34:
        case 1:
          return 47839;
        case 38:
        case 36:
          return 99;
        case 43:
        case 37:
          return 2048;
        case 0: return 2097152;
        case 3: return 65536;
        case 28: return 32768;
        case 44: return 32767;
        case 75: return 16384;
        case 39: return 1000;
        case 89: return 700;
        case 71: return 256;
        case 40: return 255;
        case 2: return 100;
        case 180: return 64;
        case 25: return 20;
        case 5: return 16;
        case 6: return 6;
        case 73: return 4;
        case 84: return 1;
      }
      ___setErrNo(ERRNO_CODES.EINVAL);
      return -1;
    }
  function ___cxa_guard_abort() {}
  function _isxdigit(chr) {
      return (chr >= 48 && chr <= 57) ||
             (chr >= 97 && chr <= 102) ||
             (chr >= 65 && chr <= 70);
    }var _isxdigit_l=_isxdigit;
  function _isdigit(chr) {
      return chr >= 48 && chr <= 57;
    }var _isdigit_l=_isdigit;
  function _catopen() { throw 'TODO: ' + aborter }
  function _catgets() { throw 'TODO: ' + aborter }
  function _catclose() { throw 'TODO: ' + aborter }
  function _newlocale(mask, locale, base) {
      return _malloc(4);
    }
  function _freelocale(locale) {
      _free(locale);
    }
  function _isascii(chr) {
      return chr >= 0 && (chr & 0x80) == 0;
    }
  function ___ctype_b_loc() {
      // http://refspecs.freestandards.org/LSB_3.0.0/LSB-Core-generic/LSB-Core-generic/baselib---ctype-b-loc.html
      var me = ___ctype_b_loc;
      if (!me.ret) {
        var values = [
          0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
          0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
          0,0,0,0,0,0,0,0,0,0,2,2,2,2,2,2,2,2,2,8195,8194,8194,8194,8194,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,24577,49156,49156,49156,
          49156,49156,49156,49156,49156,49156,49156,49156,49156,49156,49156,49156,55304,55304,55304,55304,55304,55304,55304,55304,
          55304,55304,49156,49156,49156,49156,49156,49156,49156,54536,54536,54536,54536,54536,54536,50440,50440,50440,50440,50440,
          50440,50440,50440,50440,50440,50440,50440,50440,50440,50440,50440,50440,50440,50440,50440,49156,49156,49156,49156,49156,
          49156,54792,54792,54792,54792,54792,54792,50696,50696,50696,50696,50696,50696,50696,50696,50696,50696,50696,50696,50696,
          50696,50696,50696,50696,50696,50696,50696,49156,49156,49156,49156,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
          0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
          0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
        ];
        var i16size = 2;
        var arr = _malloc(values.length * i16size);
        for (var i = 0; i < values.length; i++) {
          HEAP16[(((arr)+(i * i16size))>>1)]=values[i]
        }
        me.ret = allocate([arr + 128 * i16size], 'i16*', ALLOC_NORMAL);
      }
      return me.ret;
    }
  function ___ctype_tolower_loc() {
      // http://refspecs.freestandards.org/LSB_3.1.1/LSB-Core-generic/LSB-Core-generic/libutil---ctype-tolower-loc.html
      var me = ___ctype_tolower_loc;
      if (!me.ret) {
        var values = [
          128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,
          158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,
          188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,
          218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,
          248,249,250,251,252,253,254,-1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,
          33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,97,98,99,100,101,102,103,
          104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,91,92,93,94,95,96,97,98,99,100,101,102,103,
          104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,
          134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,
          164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,
          194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,
          224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,
          254,255
        ];
        var i32size = 4;
        var arr = _malloc(values.length * i32size);
        for (var i = 0; i < values.length; i++) {
          HEAP32[(((arr)+(i * i32size))>>2)]=values[i]
        }
        me.ret = allocate([arr + 128 * i32size], 'i32*', ALLOC_NORMAL);
      }
      return me.ret;
    }
  function ___ctype_toupper_loc() {
      // http://refspecs.freestandards.org/LSB_3.1.1/LSB-Core-generic/LSB-Core-generic/libutil---ctype-toupper-loc.html
      var me = ___ctype_toupper_loc;
      if (!me.ret) {
        var values = [
          128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,
          158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,
          188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,
          218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,
          248,249,250,251,252,253,254,-1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,
          33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,
          73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,
          81,82,83,84,85,86,87,88,89,90,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,
          145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,
          175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,
          205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,
          235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255
        ];
        var i32size = 4;
        var arr = _malloc(values.length * i32size);
        for (var i = 0; i < values.length; i++) {
          HEAP32[(((arr)+(i * i32size))>>2)]=values[i]
        }
        me.ret = allocate([arr + 128 * i32size], 'i32*', ALLOC_NORMAL);
      }
      return me.ret;
    }
  var _strftime_l=_strftime;
  function __parseInt64(str, endptr, base, min, max, unsign) {
      var isNegative = false;
      // Skip space.
      while (_isspace(HEAP8[(str)])) str++;
      // Check for a plus/minus sign.
      if (HEAP8[(str)] == 45) {
        str++;
        isNegative = true;
      } else if (HEAP8[(str)] == 43) {
        str++;
      }
      // Find base.
      var ok = false;
      var finalBase = base;
      if (!finalBase) {
        if (HEAP8[(str)] == 48) {
          if (HEAP8[((str+1)|0)] == 120 ||
              HEAP8[((str+1)|0)] == 88) {
            finalBase = 16;
            str += 2;
          } else {
            finalBase = 8;
            ok = true; // we saw an initial zero, perhaps the entire thing is just "0"
          }
        }
      } else if (finalBase==16) {
        if (HEAP8[(str)] == 48) {
          if (HEAP8[((str+1)|0)] == 120 ||
              HEAP8[((str+1)|0)] == 88) {
            str += 2;
          }
        }
      }
      if (!finalBase) finalBase = 10;
      var start = str;
      // Get digits.
      var chr;
      while ((chr = HEAP8[(str)]) != 0) {
        var digit = parseInt(String.fromCharCode(chr), finalBase);
        if (isNaN(digit)) {
          break;
        } else {
          str++;
          ok = true;
        }
      }
      if (!ok) {
        ___setErrNo(ERRNO_CODES.EINVAL);
        return ((asm["setTempRet0"](0),0)|0);
      }
      // Set end pointer.
      if (endptr) {
        HEAP32[((endptr)>>2)]=str
      }
      try {
        var numberString = isNegative ? '-'+Pointer_stringify(start, str - start) : Pointer_stringify(start, str - start);
        i64Math.fromString(numberString, finalBase, min, max, unsign);
      } catch(e) {
        ___setErrNo(ERRNO_CODES.ERANGE); // not quite correct
      }
      return ((asm["setTempRet0"](((HEAP32[(((tempDoublePtr)+(4))>>2)])|0)),((HEAP32[((tempDoublePtr)>>2)])|0))|0);
    }function _strtoull(str, endptr, base) {
      return __parseInt64(str, endptr, base, 0, '18446744073709551615', true);  // ULONG_MAX.
    }var _strtoull_l=_strtoull;
  function _strtoll(str, endptr, base) {
      return __parseInt64(str, endptr, base, '-9223372036854775808', '9223372036854775807');  // LLONG_MIN, LLONG_MAX.
    }var _strtoll_l=_strtoll;
  function _uselocale(locale) {
      return 0;
    }
  function _asprintf(s, format, varargs) {
      return _sprintf(-s, format, varargs);
    }function _vasprintf(s, format, va_arg) {
      return _asprintf(s, format, HEAP32[((va_arg)>>2)]);
    }
  function _vsnprintf(s, n, format, va_arg) {
      return _snprintf(s, n, format, HEAP32[((va_arg)>>2)]);
    }
  function _vsscanf(s, format, va_arg) {
      return _sscanf(s, format, HEAP32[((va_arg)>>2)]);
    }
  Module["_strncasecmp"] = _strncasecmp;
  var _fabs=Math_abs;
  function _sbrk(bytes) {
      // Implement a Linux-like 'memory area' for our 'process'.
      // Changes the size of the memory area by |bytes|; returns the
      // address of the previous top ('break') of the memory area
      // We control the "dynamic" memory - DYNAMIC_BASE to DYNAMICTOP
      var self = _sbrk;
      if (!self.called) {
        DYNAMICTOP = alignMemoryPage(DYNAMICTOP); // make sure we start out aligned
        self.called = true;
        assert(Runtime.dynamicAlloc);
        self.alloc = Runtime.dynamicAlloc;
        Runtime.dynamicAlloc = function() { abort('cannot dynamically allocate, sbrk now has control') };
      }
      var ret = DYNAMICTOP;
      if (bytes != 0) self.alloc(bytes);
      return ret;  // Previous break location.
    }
  function _time(ptr) {
      var ret = Math.floor(Date.now()/1000);
      if (ptr) {
        HEAP32[((ptr)>>2)]=ret
      }
      return ret;
    }
  function ___cxa_call_unexpected(exception) {
      Module.printErr('Unexpected exception thrown, this is not properly supported - aborting');
      ABORT = true;
      throw exception;
    }
  function _copysign(a, b) {
      return __reallyNegative(a) === __reallyNegative(b) ? a : -a;
    }var _copysignl=_copysign;
FS.staticInit();__ATINIT__.unshift({ func: function() { if (!Module["noFSInit"] && !FS.init.initialized) FS.init() } });__ATMAIN__.push({ func: function() { FS.ignorePermissions = false } });__ATEXIT__.push({ func: function() { FS.quit() } });Module["FS_createFolder"] = FS.createFolder;Module["FS_createPath"] = FS.createPath;Module["FS_createDataFile"] = FS.createDataFile;Module["FS_createPreloadedFile"] = FS.createPreloadedFile;Module["FS_createLazyFile"] = FS.createLazyFile;Module["FS_createLink"] = FS.createLink;Module["FS_createDevice"] = FS.createDevice;
___errno_state = Runtime.staticAlloc(4); HEAP32[((___errno_state)>>2)]=0;
__ATINIT__.unshift({ func: function() { TTY.init() } });__ATEXIT__.push({ func: function() { TTY.shutdown() } });TTY.utf8 = new Runtime.UTF8Processor();
if (ENVIRONMENT_IS_NODE) { var fs = require("fs"); NODEFS.staticInit(); }
__ATINIT__.push({ func: function() { SOCKFS.root = FS.mount(SOCKFS, {}, null); } });
Module["requestFullScreen"] = function Module_requestFullScreen(lockPointer, resizeCanvas) { Browser.requestFullScreen(lockPointer, resizeCanvas) };
  Module["requestAnimationFrame"] = function Module_requestAnimationFrame(func) { Browser.requestAnimationFrame(func) };
  Module["setCanvasSize"] = function Module_setCanvasSize(width, height, noUpdates) { Browser.setCanvasSize(width, height, noUpdates) };
  Module["pauseMainLoop"] = function Module_pauseMainLoop() { Browser.mainLoop.pause() };
  Module["resumeMainLoop"] = function Module_resumeMainLoop() { Browser.mainLoop.resume() };
  Module["getUserMedia"] = function Module_getUserMedia() { Browser.getUserMedia() }
_llvm_eh_exception.buf = allocate(12, "void*", ALLOC_STATIC);
_fputc.ret = allocate([0], "i8", ALLOC_STATIC);
___strtok_state = Runtime.staticAlloc(4);
_fgetc.ret = allocate([0], "i8", ALLOC_STATIC);
STACK_BASE = STACKTOP = Runtime.alignMemory(STATICTOP);
staticSealed = true; // seal the static portion of memory
STACK_MAX = STACK_BASE + 5242880;
DYNAMIC_BASE = DYNAMICTOP = Runtime.alignMemory(STACK_MAX);
assert(DYNAMIC_BASE < TOTAL_MEMORY, "TOTAL_MEMORY not big enough for stack");
 var ctlz_i8 = allocate([8,7,6,6,5,5,5,5,4,4,4,4,4,4,4,4,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], "i8", ALLOC_DYNAMIC);
 var cttz_i8 = allocate([8,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,4,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,5,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,4,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,6,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,4,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,5,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,4,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,7,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,4,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,5,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,4,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,6,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,4,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,5,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,4,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0], "i8", ALLOC_DYNAMIC);
var Math_min = Math.min;
function invoke_viiiii(index,a1,a2,a3,a4,a5) {
  try {
    Module["dynCall_viiiii"](index,a1,a2,a3,a4,a5);
  } catch(e) {
    if (typeof e !== 'number' && e !== 'longjmp') throw e;
    asm["setThrew"](1, 0);
  }
}
function invoke_viiiiiii(index,a1,a2,a3,a4,a5,a6,a7) {
  try {
    Module["dynCall_viiiiiii"](index,a1,a2,a3,a4,a5,a6,a7);
  } catch(e) {
    if (typeof e !== 'number' && e !== 'longjmp') throw e;
    asm["setThrew"](1, 0);
  }
}
function invoke_vi(index,a1) {
  try {
    Module["dynCall_vi"](index,a1);
  } catch(e) {
    if (typeof e !== 'number' && e !== 'longjmp') throw e;
    asm["setThrew"](1, 0);
  }
}
function invoke_vii(index,a1,a2) {
  try {
    Module["dynCall_vii"](index,a1,a2);
  } catch(e) {
    if (typeof e !== 'number' && e !== 'longjmp') throw e;
    asm["setThrew"](1, 0);
  }
}
function invoke_iii(index,a1,a2) {
  try {
    return Module["dynCall_iii"](index,a1,a2);
  } catch(e) {
    if (typeof e !== 'number' && e !== 'longjmp') throw e;
    asm["setThrew"](1, 0);
  }
}
function invoke_iiiiii(index,a1,a2,a3,a4,a5) {
  try {
    return Module["dynCall_iiiiii"](index,a1,a2,a3,a4,a5);
  } catch(e) {
    if (typeof e !== 'number' && e !== 'longjmp') throw e;
    asm["setThrew"](1, 0);
  }
}
function invoke_viiiiiid(index,a1,a2,a3,a4,a5,a6,a7) {
  try {
    Module["dynCall_viiiiiid"](index,a1,a2,a3,a4,a5,a6,a7);
  } catch(e) {
    if (typeof e !== 'number' && e !== 'longjmp') throw e;
    asm["setThrew"](1, 0);
  }
}
function invoke_ii(index,a1) {
  try {
    return Module["dynCall_ii"](index,a1);
  } catch(e) {
    if (typeof e !== 'number' && e !== 'longjmp') throw e;
    asm["setThrew"](1, 0);
  }
}
function invoke_iiii(index,a1,a2,a3) {
  try {
    return Module["dynCall_iiii"](index,a1,a2,a3);
  } catch(e) {
    if (typeof e !== 'number' && e !== 'longjmp') throw e;
    asm["setThrew"](1, 0);
  }
}
function invoke_viii(index,a1,a2,a3) {
  try {
    Module["dynCall_viii"](index,a1,a2,a3);
  } catch(e) {
    if (typeof e !== 'number' && e !== 'longjmp') throw e;
    asm["setThrew"](1, 0);
  }
}
function invoke_viiiiid(index,a1,a2,a3,a4,a5,a6) {
  try {
    Module["dynCall_viiiiid"](index,a1,a2,a3,a4,a5,a6);
  } catch(e) {
    if (typeof e !== 'number' && e !== 'longjmp') throw e;
    asm["setThrew"](1, 0);
  }
}
function invoke_v(index) {
  try {
    Module["dynCall_v"](index);
  } catch(e) {
    if (typeof e !== 'number' && e !== 'longjmp') throw e;
    asm["setThrew"](1, 0);
  }
}
function invoke_iiiiiiiii(index,a1,a2,a3,a4,a5,a6,a7,a8) {
  try {
    return Module["dynCall_iiiiiiiii"](index,a1,a2,a3,a4,a5,a6,a7,a8);
  } catch(e) {
    if (typeof e !== 'number' && e !== 'longjmp') throw e;
    asm["setThrew"](1, 0);
  }
}
function invoke_viiiiiiiii(index,a1,a2,a3,a4,a5,a6,a7,a8,a9) {
  try {
    Module["dynCall_viiiiiiiii"](index,a1,a2,a3,a4,a5,a6,a7,a8,a9);
  } catch(e) {
    if (typeof e !== 'number' && e !== 'longjmp') throw e;
    asm["setThrew"](1, 0);
  }
}
function invoke_viiiiii(index,a1,a2,a3,a4,a5,a6) {
  try {
    Module["dynCall_viiiiii"](index,a1,a2,a3,a4,a5,a6);
  } catch(e) {
    if (typeof e !== 'number' && e !== 'longjmp') throw e;
    asm["setThrew"](1, 0);
  }
}
function invoke_iiiii(index,a1,a2,a3,a4) {
  try {
    return Module["dynCall_iiiii"](index,a1,a2,a3,a4);
  } catch(e) {
    if (typeof e !== 'number' && e !== 'longjmp') throw e;
    asm["setThrew"](1, 0);
  }
}
function invoke_viiiiiiii(index,a1,a2,a3,a4,a5,a6,a7,a8) {
  try {
    Module["dynCall_viiiiiiii"](index,a1,a2,a3,a4,a5,a6,a7,a8);
  } catch(e) {
    if (typeof e !== 'number' && e !== 'longjmp') throw e;
    asm["setThrew"](1, 0);
  }
}
function invoke_viiii(index,a1,a2,a3,a4) {
  try {
    Module["dynCall_viiii"](index,a1,a2,a3,a4);
  } catch(e) {
    if (typeof e !== 'number' && e !== 'longjmp') throw e;
    asm["setThrew"](1, 0);
  }
}
function asmPrintInt(x, y) {
  Module.print('int ' + x + ',' + y);// + ' ' + new Error().stack);
}
function asmPrintFloat(x, y) {
  Module.print('float ' + x + ',' + y);// + ' ' + new Error().stack);
}
// EMSCRIPTEN_START_ASM
var asm=(function(global,env,buffer){"use asm";var a=new global.Int8Array(buffer);var b=new global.Int16Array(buffer);var c=new global.Int32Array(buffer);var d=new global.Uint8Array(buffer);var e=new global.Uint16Array(buffer);var f=new global.Uint32Array(buffer);var g=new global.Float32Array(buffer);var h=new global.Float64Array(buffer);var i=env.STACKTOP|0;var j=env.STACK_MAX|0;var k=env.tempDoublePtr|0;var l=env.ABORT|0;var m=env.cttz_i8|0;var n=env.ctlz_i8|0;var o=env._stdin|0;var p=env.__ZTVN10__cxxabiv117__class_type_infoE|0;var q=env.__ZTVN10__cxxabiv120__si_class_type_infoE|0;var r=env._stderr|0;var s=env.___fsmu8|0;var t=env._stdout|0;var u=env.___dso_handle|0;var v=+env.NaN;var w=+env.Infinity;var x=0;var y=0;var z=0;var A=0;var B=0,C=0,D=0,E=0,F=0.0,G=0,H=0,I=0,J=0.0;var K=0;var L=0;var M=0;var N=0;var O=0;var P=0;var Q=0;var R=0;var S=0;var T=0;var U=global.Math.floor;var V=global.Math.abs;var W=global.Math.sqrt;var X=global.Math.pow;var Y=global.Math.cos;var Z=global.Math.sin;var _=global.Math.tan;var $=global.Math.acos;var aa=global.Math.asin;var ba=global.Math.atan;var ca=global.Math.atan2;var da=global.Math.exp;var ea=global.Math.log;var fa=global.Math.ceil;var ga=global.Math.imul;var ha=env.abort;var ia=env.assert;var ja=env.asmPrintInt;var ka=env.asmPrintFloat;var la=env.min;var ma=env.invoke_viiiii;var na=env.invoke_viiiiiii;var oa=env.invoke_vi;var pa=env.invoke_vii;var qa=env.invoke_iii;var ra=env.invoke_iiiiii;var sa=env.invoke_viiiiiid;var ta=env.invoke_ii;var ua=env.invoke_iiii;var va=env.invoke_viii;var wa=env.invoke_viiiiid;var xa=env.invoke_v;var ya=env.invoke_iiiiiiiii;var za=env.invoke_viiiiiiiii;var Aa=env.invoke_viiiiii;var Ba=env.invoke_iiiii;var Ca=env.invoke_viiiiiiii;var Da=env.invoke_viiii;var Ea=env._llvm_lifetime_end;var Fa=env.__scanString;var Ga=env._pthread_mutex_lock;var Ha=env.___cxa_end_catch;var Ia=env._strtoul;var Ja=env._strtoull;var Ka=env._fflush;var La=env._fputc;var Ma=env._strtok;var Na=env._fwrite;var Oa=env._llvm_eh_exception;var Pa=env._fputs;var Qa=env._isspace;var Ra=env._read;var Sa=env._SDL_UnlockSurface;var Ta=env._perror;var Ua=env.___cxa_guard_abort;var Va=env._newlocale;var Wa=env.___gxx_personality_v0;var Xa=env._pthread_cond_wait;var Ya=env.___cxa_rethrow;var Za=env._modf;var _a=env.___resumeException;var $a=env._strcmp;var ab=env._strncmp;var bb=env._vsscanf;var cb=env._snprintf;var db=env._fgetc;var eb=env._stat;var fb=env.__getFloat;var gb=env._atexit;var hb=env._hypot;var ib=env.___cxa_free_exception;var jb=env._close;var kb=env.___setErrNo;var lb=env._isxdigit;var mb=env._exit;var nb=env._sprintf;var ob=env.___ctype_b_loc;var pb=env._freelocale;var qb=env._catgets;var rb=env._fmod;var sb=env.__isLeapYear;var tb=env._fmax;var ub=env._asprintf;var vb=env.___cxa_is_number_type;var wb=env._gmtime;var xb=env.___cxa_does_inherit;var yb=env.___cxa_guard_acquire;var zb=env.___cxa_begin_catch;var Ab=env._recv;var Bb=env.__parseInt64;var Cb=env.__ZSt18uncaught_exceptionv;var Db=env._SDL_PollEvent;var Eb=env.___cxa_call_unexpected;var Fb=env._SDL_Init;var Gb=env._round;var Hb=env._copysign;var Ib=env._fmin;var Jb=env._mmap;var Kb=env.__exit;var Lb=env._strftime;var Mb=env._llvm_va_end;var Nb=env._fabsf;var Ob=env._floorf;var Pb=env._rint;var Qb=env.___cxa_throw;var Rb=env._floorl;var Sb=env._fabsl;var Tb=env._send;var Ub=env._toupper;var Vb=env._pread;var Wb=env._SDL_SetVideoMode;var Xb=env._SDL_LockSurface;var Yb=env._open;var Zb=env._sqrtf;var _b=env.__arraySum;var $b=env._puts;var ac=env.___cxa_find_matching_catch;var bc=env._strdup;var cc=env._log10;var dc=env.__formatString;var ec=env._pthread_cond_broadcast;var fc=env.__ZSt9terminatev;var gc=env._isascii;var hc=env._pthread_mutex_unlock;var ic=env._llvm_pow_f64;var jc=env._sbrk;var kc=env.___errno_location;var lc=env._strerror;var mc=env._fstat;var nc=env._catclose;var oc=env._llvm_lifetime_start;var pc=env.__parseInt;var qc=env.___cxa_guard_release;var rc=env._ungetc;var sc=env._vsprintf;var tc=env._SDL_EnableUNICODE;var uc=env._uselocale;var vc=env._gmtime_r;var wc=env._vsnprintf;var xc=env._sscanf;var yc=env._sysconf;var zc=env._fread;var Ac=env._strtok_r;var Bc=env._abort;var Cc=env._fprintf;var Dc=env._isdigit;var Ec=env._strtoll;var Fc=env.__reallyNegative;var Gc=env.__addDays;var Hc=env._fabs;var Ic=env._floor;var Jc=env._sqrt;var Kc=env._write;var Lc=env._SDL_UpdateRect;var Mc=env._SDL_GetModState;var Nc=env.___cxa_allocate_exception;var Oc=env._ceilf;var Pc=env._vasprintf;var Qc=env._emscripten_set_main_loop;var Rc=env._catopen;var Sc=env.___ctype_toupper_loc;var Tc=env.___ctype_tolower_loc;var Uc=env._pwrite;var Vc=env._strerror_r;var Wc=env._time;var Xc=0.0;
// EMSCRIPTEN_START_FUNCS
function od(a){a=a|0;var b=0;b=i;i=i+a|0;i=i+7&-8;return b|0}function pd(){return i|0}function qd(a){a=a|0;i=a}function rd(a,b){a=a|0;b=b|0;if((x|0)==0){x=a;y=b}}function sd(b){b=b|0;a[k]=a[b];a[k+1|0]=a[b+1|0];a[k+2|0]=a[b+2|0];a[k+3|0]=a[b+3|0]}function td(b){b=b|0;a[k]=a[b];a[k+1|0]=a[b+1|0];a[k+2|0]=a[b+2|0];a[k+3|0]=a[b+3|0];a[k+4|0]=a[b+4|0];a[k+5|0]=a[b+5|0];a[k+6|0]=a[b+6|0];a[k+7|0]=a[b+7|0]}function ud(a){a=a|0;K=a}function vd(a){a=a|0;L=a}function wd(a){a=a|0;M=a}function xd(a){a=a|0;N=a}function yd(a){a=a|0;O=a}function zd(a){a=a|0;P=a}function Ad(a){a=a|0;Q=a}function Bd(a){a=a|0;R=a}function Cd(a){a=a|0;S=a}function Dd(a){a=a|0;T=a}function Ed(){c[2540]=p+8;c[2542]=p+8;c[2544]=q+8;c[2548]=q+8;c[2552]=q+8;c[2556]=q+8;c[2560]=q+8;c[2564]=p+8;c[2598]=q+8;c[2602]=q+8;c[2666]=q+8;c[2670]=q+8;c[2690]=p+8;c[2692]=q+8;c[2728]=q+8;c[2732]=q+8;c[2768]=q+8;c[2772]=q+8;c[2792]=p+8;c[2794]=p+8;c[2796]=q+8;c[2800]=q+8;c[2804]=q+8;c[2808]=p+8;c[2810]=p+8;c[2812]=p+8;c[2814]=p+8;c[2816]=p+8;c[2818]=p+8;c[2820]=p+8;c[2846]=q+8;c[2850]=p+8;c[2852]=q+8;c[2856]=q+8;c[2860]=q+8;c[2864]=p+8;c[2866]=p+8;c[2868]=p+8;c[2870]=p+8;c[2904]=p+8;c[2906]=p+8;c[2908]=p+8;c[2910]=q+8;c[2914]=q+8;c[2918]=q+8;c[2922]=q+8;c[2926]=q+8;c[2930]=q+8}function Fd(a){a=a|0;var b=0;c[8692]=4136;c[8693]=4128;c[8694]=4128;c[8695]=3192;c[8696]=3e3;c[8698]=2536;c[8699]=1760;c[8700]=1440;c[8701]=3192;c[8702]=800;c[8703]=504;c[8704]=256;c[8705]=4120;c[8706]=3864;c[8707]=3464;c[8708]=3360;c[8709]=3288;c[8710]=3264;c[8711]=3248;c[8712]=4136;c[8713]=4136;c[8714]=4136;c[8715]=3232;c[8716]=3208;c[8717]=3200;c[8718]=4136;c[8719]=4136;c[8720]=4136;c[8721]=0;c[8722]=0;c[8723]=4136;c[8724]=3184;c[8725]=3232;c[8726]=3208;c[8727]=3176;c[8728]=3152;c[8729]=3144;c[8730]=3136;c[8731]=0;c[8732]=3120;c[8733]=3104;c[8734]=3072;c[8735]=3048;c[8736]=3008;c[8737]=2984;c[8738]=2936;c[8739]=2880;c[8740]=2856;c[8741]=2792;c[8742]=2760;c[8743]=2664;c[8744]=2640;c[8745]=2584;c[8746]=2568;c[8747]=2520;ko(34992,0,16)|0;c[8752]=3208;c[8753]=3208;c[8754]=3120;c[8755]=3e3;if(a>>>0>63>>>0){b=0;return b|0}b=c[34768+(a<<2)>>2]|0;return b|0}function Gd(a,b){a=a|0;b=b|0;var d=0;Cc(c[t>>2]|0,2480,(d=i,i=i+16|0,c[d>>2]=c[a>>2],c[d+8>>2]=1832,d)|0)|0;i=d;mb(b|0)}function Hd(f,g,j){f=f|0;g=g|0;j=j|0;var k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0.0,B=0.0,D=0,E=0.0,F=0.0,G=0.0,H=0.0,I=0.0,J=0.0,K=0.0,L=0.0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0;k=i;i=i+32|0;l=k|0;if((c[8686]|0)==0){c[8684]=16;c[8686]=Ln(512)|0}m=me(g,0)|0;n=(m|0)==0;if(!n){o=f+4|0;p=f+6|0;q=f+8|0;r=f|0;s=g+8|0;t=g|0;u=g+84|0;v=g+76|0;g=l|0;w=l+8|0;x=l+16|0;y=l+24|0;z=0;do{A=+((e[o>>1]|0)>>>0);B=+((e[p>>1]|0)>>>0);D=c[8686]|0;E=+h[D+(z<<5)>>3];F=+h[D+(z<<5)+8>>3];G=+h[D+(z<<5)+16>>3];H=+h[D+(z<<5)+24>>3];if(A<E|G<0.0|B<F|H<0.0){I=0.0;J=0.0;K=0.0;L=0.0}else{I=E<0.0?0.0:E;J=F<0.0?0.0:F;K=A<G?A:G;L=B<H?B:H}H=K-I;do{if(H>0.0){B=L-J;if(B<=0.0){break}L14:do{if(B>0.0){if(H<=0.0){D=0;while(1){D=D+1|0;if(+(D>>>0>>>0)>=B){break L14}}}D=(c[r>>2]|0)+(~~(I+J*+(e[q>>1]|0|0))<<2)|0;M=(c[t>>2]|0)+(~~(I+J*+(e[s>>1]|0|0))<<2)|0;N=0;while(1){O=0;do{P=M+(O<<2)|0;Q=D+(O<<2)|0;C=d[P]|d[P+1|0]<<8|d[P+2|0]<<16|d[P+3|0]<<24|0;a[Q]=C;C=C>>8;a[Q+1|0]=C;C=C>>8;a[Q+2|0]=C;C=C>>8;a[Q+3|0]=C;O=O+1|0;}while(+(O>>>0>>>0)<H);O=N+1|0;if(+(O>>>0>>>0)<B){D=D+((e[q>>1]|0)<<2)|0;M=M+((e[s>>1]|0)<<2)|0;N=O}else{break}}}}while(0);if((c[u>>2]|0)==0){break}else{R=0}do{N=c[(c[v>>2]|0)+(R<<2)>>2]|0;h[g>>3]=I;h[w>>3]=J;h[x>>3]=K;h[y>>3]=L;ne(f,N,l);R=R+1|0;}while(R>>>0<(c[u>>2]|0)>>>0)}}while(0);z=z+1|0;}while(z>>>0<m>>>0)}z=c[8686]|0;Xb(j|0)|0;if(n){Sa(j|0);i=k;return}u=j+20|0;R=j+16|0;l=f|0;y=f+8|0;f=0;x=c[R>>2]|0;w=b[y>>1]|0;while(1){L=+h[z+(f<<5)>>3];K=+h[z+(f<<5)+8>>3];g=z+(f<<5)+16|0;J=+h[g+8>>3]-K;L36:do{if(J>0.0){I=+h[g>>3]-L;if(I>0.0){S=(c[u>>2]|0)+~~(L*4.0+K*+(x|0))|0;T=(c[l>>2]|0)+(~~(L+K*+(w&65535|0))<<2)|0;U=0}else{v=0;while(1){s=v+1|0;if(+(s>>>0>>>0)<J){v=s}else{V=x;W=w;break L36}}}while(1){v=0;do{s=T+(v<<2)|0;q=S+(v<<2)|0;C=d[s]|d[s+1|0]<<8|d[s+2|0]<<16|d[s+3|0]<<24|0;a[q]=C;C=C>>8;a[q+1|0]=C;C=C>>8;a[q+2|0]=C;C=C>>8;a[q+3|0]=C;v=v+1|0;}while(+(v>>>0>>>0)<I);v=c[R>>2]|0;q=b[y>>1]|0;s=U+1|0;if(+(s>>>0>>>0)<J){S=S+(v>>>2<<2)|0;T=T+((q&65535)<<2)|0;U=s}else{V=v;W=q;break}}}else{V=x;W=w}}while(0);g=f+1|0;if(g>>>0<m>>>0){f=g;x=V;w=W}else{break}}Sa(j|0);if(n){i=k;return}else{X=0}do{J=+h[z+(X<<5)>>3];K=+h[z+(X<<5)+8>>3];Lc(j|0,~~J|0,~~K|0,~~(+h[z+(X<<5)+16>>3]-J)|0,~~(+h[z+(X<<5)+24>>3]-K)|0);X=X+1|0;}while(X>>>0<m>>>0);i=k;return}function Id(a){a=a|0;var b=0,d=0,e=0,f=0;b=c[a+1404>>2]|0;d=b;if((b|0)!=0){e=a+1408|0;f=c[e>>2]|0;if((b|0)!=(f|0)){c[e>>2]=f+(~(((f-520+(-d|0)|0)>>>0)/520|0)*520|0)}Tn(b)}b=c[a+552>>2]|0;if((b|0)==0){return}d=a+556|0;a=c[d>>2]|0;if((b|0)!=(a|0)){c[d>>2]=a+(~(((a-88+(-b|0)|0)>>>0)/88|0)*88|0)}Tn(b);return}function Jd(){var f=0,g=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0.0,N=0.0,O=0,P=0,Q=0.0,R=0.0,S=0.0,T=0.0,U=0.0,V=0.0,W=0.0,X=0.0,Y=0.0,Z=0.0,_=0,$=0,aa=0,ba=0.0,ca=0.0,da=0.0,ea=0.0,fa=0,ha=0,ia=0,ja=0,ka=0,la=0,ma=0,na=0.0,oa=0.0,pa=0.0,qa=0.0,ra=0.0,sa=0.0,ta=0.0,ua=0.0,va=0.0,wa=0.0,xa=0.0,ya=0,za=0,Aa=0;f=i;i=i+192|0;g=f|0;j=f+32|0;k=f+64|0;l=f+72|0;m=f+104|0;n=f+112|0;o=f+144|0;if((Db(o|0)|0)==0){i=f;return}p=o|0;q=o+4|0;r=o+8|0;s=r;t=r;r=o+12|0;u=o+16|0;v=o+12|0;w=g;x=j;y=l;z=n;A=k;B=m;D=g+8|0;E=g|0;F=g+24|0;G=g+16|0;H=o+24|0;do{I=c[p>>2]|0;L6:do{if((I|0)==1024){J=a[t]|0;if(J<<24>>24==0){K=c[v>>2]&65535;L=c[u>>2]&65535;M=+(K<<16>>16|0);N=+(L<<16>>16|0);if(+h[3809]>N&((+h[3808]<=M|+h[3806]>M|+h[3807]>N)^1)){de(K,L);ee(M,N);break}N=+h[3858];if(N==+h[3860]){break}M=+h[3859];if(M==+h[3861]){break}ee(N,M);break}if((J&1)==0){break}if(!(a[4456]|0)){a[4456]=1;b[17380]=c[v>>2];b[17376]=c[u>>2];break}J=c[v>>2]|0;L=c[u>>2]|0;K=b[17380]|0;O=b[17376]|0;P=L-(O<<16>>16)|0;if((a[30761]&1)==0){M=+(J|0);N=+(L|0);if(+h[3814]>N&((+h[3813]<=M|+h[3811]>M|+h[3812]>N)^1)){c[7567]=2;h[3856]=+h[3786];h[3857]=+h[3787]}else{c[7567]=1;a[30160]=1}a[30761]=1;b[15400]=K;b[15401]=O;break}O=c[7567]|0;if((O|0)==1){K=a[30088]&1;a[30088]=0;N=+(b[15400]|0);M=+(b[15401]|0);Q=+((J&65535)<<16>>16|0);R=+((L&65535)<<16>>16|0);S=+Ib(+N,+Q);T=+Ib(+M,+R);U=+tb(+N,+Q);Q=+tb(+M,+R);R=+h[3794];M=+h[3795];N=+h[3796];V=+h[3797];ko(w|0,0,32)|0;if(!(U<R|N<S|Q<M|V<T)){h[D>>3]=T>M?T:M;h[E>>3]=S>R?S:R;h[F>>3]=Q<V?Q:V;h[G>>3]=U<N?U:N}c[x>>2]=c[7692];c[x+4>>2]=c[7693];c[x+8>>2]=c[7694];c[x+12>>2]=c[7695];c[x+16>>2]=c[7696];c[x+20>>2]=c[7697];c[x+24>>2]=c[7698];c[x+28>>2]=c[7699];c[k>>2]=0;c[y>>2]=c[7692];c[y+4>>2]=c[7693];c[y+8>>2]=c[7694];c[y+12>>2]=c[7695];c[y+16>>2]=c[7696];c[y+20>>2]=c[7697];c[y+24>>2]=c[7698];c[y+28>>2]=c[7699];ae(30072,A,l,1);be(c[7517]|0,j);c[m>>2]=-5592406;c[z>>2]=c[w>>2];c[z+4>>2]=c[w+4>>2];c[z+8>>2]=c[w+8>>2];c[z+12>>2]=c[w+12>>2];c[z+16>>2]=c[w+16>>2];c[z+20>>2]=c[w+20>>2];c[z+24>>2]=c[w+24>>2];c[z+28>>2]=c[w+28>>2];ae(30072,B,n,1);be(c[7517]|0,g);a[30088]=K;N=+(b[15400]|0);U=+(b[15401]|0);V=+(J|0);Q=+(L|0);R=+Ib(+N,+V);S=+Ib(+U,+Q);M=+tb(+N,+V);V=+tb(+U,+Q);Q=+h[3794];U=+h[3795];N=+h[3796];T=+h[3797];if(M<Q|N<R|V<U|T<S){W=0.0;X=0.0;Y=0.0;Z=0.0}else{W=R>Q?R:Q;X=S>U?S:U;Y=M<N?M:N;Z=V<T?V:T}h[3846]=W;h[3847]=X;h[3848]=Y;h[3849]=Z;_=c[7567]|0}else{_=O}if((_|0)!=2){break}a[30336]=0;T=+h[3857];V=+h[3856];N=T-V;if(+(b[15401]|0)<(+h[3814]- +h[3812])*.5){M=N*.005;U=M+ +h[3786];S=T- +(P|0)*M;h[3787]=U<S?S:U;Ld();break}else{U=N*.005;N=+h[3787]-U;S=V- +(P|0)*U;h[3786]=S<N?S:N;Ld();break}}else if((I|0)==256){a[31144]=0}else if((I|0)==28673){P=c[q>>2]|0;b[14956]=P;O=c[s>>2]|0;b[14957]=O;c[7784]=Wb(P&65535|0,O&65535|0,32,150994945)|0;O=b[14956]|0;P=b[14957]|0;Mn(c[7430]|0);L=Nn(ga(P&65535,O&65535)|0,4)|0;J=L;c[7430]=J;if((L|0)==0){$=b[14862]|0;aa=b[14863]|0;ba=+h[3718];ca=+h[3720];da=+h[3719];ea=+h[3721]}else{b[14864]=O;b[14862]=O;b[14863]=P;N=+((O&65535)>>>0);S=+((P&65535)>>>0);ko(29744,0,16)|0;h[3720]=N;h[3721]=S;$=O;aa=P;ba=0.0;ca=N;da=0.0;ea=S}S=ba>0.0?+(~~ba|0):0.0;P=~~(S>ca?ca:S);S=+($&65535|0);N=+(~~(S<ba?ba:S)|0);O=~~(N>ca?ca:N);N=da>0.0?+(~~da|0):0.0;L=~~(N>ea?ea:N);N=+(aa&65535|0);S=+(~~(N<da?da:N)|0);K=~~(S>ea?ea:S);L46:do{if((L|0)<(K|0)&(P|0)<(O|0)){fa=L;ha=J;while(1){ia=ha+((ga(e[14864]|0,fa)|0)<<2)|0;ja=P;while(1){ka=ia;C=-16777216;a[ka]=C;C=C>>8;a[ka+1|0]=C;C=C>>8;a[ka+2|0]=C;C=C>>8;a[ka+3|0]=C;ka=ja+1|0;if((ka|0)<(O|0)){ia=ia+4|0;ja=ka}else{break}}ja=fa+1|0;if((ja|0)>=(K|0)){break L46}fa=ja;ha=c[7430]|0}}}while(0);K=b[14956]|0;O=b[14957]|0;Mn(c[7798]|0);P=Nn(ga(O&65535,K&65535)|0,4)|0;J=P;c[7798]=J;if((P|0)==0){la=b[15598]|0;ma=b[15599]|0;na=+h[3902];oa=+h[3904];pa=+h[3903];qa=+h[3905]}else{b[15600]=K;b[15598]=K;b[15599]=O;S=+((K&65535)>>>0);N=+((O&65535)>>>0);ko(31216,0,16)|0;h[3904]=S;h[3905]=N;la=K;ma=O;na=0.0;oa=S;pa=0.0;qa=N}N=na>0.0?+(~~na|0):0.0;O=~~(N>oa?oa:N);N=+(la&65535|0);S=+(~~(N<na?na:N)|0);K=~~(S>oa?oa:S);S=pa>0.0?+(~~pa|0):0.0;P=~~(S>qa?qa:S);S=+(ma&65535|0);N=+(~~(S<pa?pa:S)|0);L=~~(N>qa?qa:N);L58:do{if((P|0)<(L|0)&(O|0)<(K|0)){ha=P;fa=J;while(1){ja=fa+((ga(e[15600]|0,ha)|0)<<2)|0;ia=O;while(1){ka=ja;C=-16777216;a[ka]=C;C=C>>8;a[ka+1|0]=C;C=C>>8;a[ka+2|0]=C;C=C>>8;a[ka+3|0]=C;ka=ia+1|0;if((ka|0)<(K|0)){ja=ja+4|0;ia=ka}else{break}}ia=ha+1|0;if((ia|0)>=(L|0)){break L58}ha=ia;fa=c[7798]|0}}}while(0);L=(e[14956]|0)-10|0;K=(e[14957]|0)-10|0;h[3750]=5.0;h[3751]=5.0;h[3752]=((L|0)<0?0.0:+(L|0))+5.0;h[3753]=((K|0)<0?0.0:+(K|0))+5.0;Kd(3e4);Ld()}else if((I|0)==1025){K=c[r>>2]&65535;L=c[u>>2]&65535;if((a[t]|0)!=1){break}N=+(K<<16>>16|0);S=+(L<<16>>16|0);U=+h[3806];V=+h[3807];M=+h[3808];T=+h[3809];if(!(T>S&((M<=N|U>N|V>S)^1))){break}Q=+h[3802];R=+h[3803];ra=R+(S-V)*(+h[3805]-R)/(T-V);h[3851]=Q+(N-U)*(+h[3804]-Q)/(M-U);h[3852]=ra;de(K,L)}else if((I|0)==1026){if(a[4456]|0){a[4456]=0}ra=+((c[r>>2]&65535)<<16>>16|0);U=+((c[u>>2]&65535)<<16>>16|0);if((a[30761]&1)!=0){if((c[7567]|0)==1){a[30160]=0;M=+(b[15400]|0);Q=+(b[15401]|0);N=+Ib(+M,+ra);V=+Ib(+Q,+U);T=+tb(+M,+ra);M=+tb(+Q,+U);Q=+h[3806];R=+h[3807];S=+h[3802];sa=+h[3803];ta=+h[3804]-S;ua=+h[3808]-Q;va=S+(N-Q)*ta/ua;N=+h[3805]-sa;wa=+h[3809]-R;xa=sa+(V-R)*N/wa;V=S+(T-Q)*ta/ua;ta=sa+(M-R)*N/wa;N=+Ib(+va,+V);R=+Ib(+xa,+ta);M=+tb(+va,+V);V=+tb(+xa,+ta);L=Ln(104)|0;c[L>>2]=c[7620];c[L+72>>2]=c[7566];K=L+8|0;c[K>>2]=c[7604];c[K+4>>2]=c[7605];c[K+8>>2]=c[7606];c[K+12>>2]=c[7607];c[K+16>>2]=c[7608];c[K+20>>2]=c[7609];c[K+24>>2]=c[7610];c[K+28>>2]=c[7611];K=L+40|0;c[K>>2]=c[7612];c[K+4>>2]=c[7613];c[K+8>>2]=c[7614];c[K+12>>2]=c[7615];c[K+16>>2]=c[7616];c[K+20>>2]=c[7617];c[K+24>>2]=c[7618];c[K+28>>2]=c[7619];h[L+80>>3]=+h[3789];h[L+88>>3]=+h[3790];h[L+96>>3]=+h[3791];c[7566]=Ln(~~(ua*wa*24.0))|0;c[7620]=L;h[3858]=-1.0;h[3859]=-1.0;h[3802]=N;h[3803]=R;h[3804]=M;h[3805]=V;$d();Ld()}a[30761]=0;c[7567]=0;break}L=d[t]|0;if((L|0)==1){if(+h[3809]>U&((+h[3808]<=ra|+h[3806]>ra|+h[3807]>U)^1)){h[3858]=ra;h[3859]=U;break}else{h[3858]=-1.0;h[3859]=-1.0;break}}else if((L|0)!=3){break}L=c[7620]|0;if((L|0)!=0){c[7620]=c[L>>2];h[3858]=-1.0;h[3859]=-1.0;if(+h[L+40>>3]==+h[3806]&+h[L+48>>3]==+h[3807]&+h[L+56>>3]==+h[3808]&+h[L+64>>3]==+h[3809]){Mn(c[7566]|0);K=L+8|0;c[7604]=c[K>>2];c[7605]=c[K+4>>2];c[7606]=c[K+8>>2];c[7607]=c[K+12>>2];c[7608]=c[K+16>>2];c[7609]=c[K+20>>2];c[7610]=c[K+24>>2];c[7611]=c[K+28>>2];c[7566]=c[L+72>>2];h[3789]=+h[L+80>>3];h[3790]=+h[L+88>>3];h[3791]=+h[L+96>>3]}else{K=L+8|0;c[7604]=c[K>>2];c[7605]=c[K+4>>2];c[7606]=c[K+8>>2];c[7607]=c[K+12>>2];c[7608]=c[K+16>>2];c[7609]=c[K+20>>2];c[7610]=c[K+24>>2];c[7611]=c[K+28>>2];Mn(c[L+72>>2]|0);$d()}Ld();Mn(L)}Ld()}else if((I|0)==768){if((c[u>>2]|0)==27){a[31144]=0;break}L=c[H>>2]|0;do{if((L|0)!=0&(L&65408|0)==0){K=L&127;if(((Mc()|0)&3|0)==0){ya=K&255;break}else{ya=(Ub(K|0)|0)&255;break}}else{ya=0}}while(0);L=(lo(ya<<24>>24|0)|0)&255;K=L<<24>>24;O=a[30760]&1;if(L<<24>>24==109){a[30760]=O^1;a[30256]=a[30256]&1^1;Td();break}if(O<<24>>24==0){if((K|0)==103){a[30834]=a[30834]&1^1;Ld();break}else if((K|0)==97){a[30832]=a[30832]&1^1;Zd();a[30833]=a[30833]&1^1;Zd();Ld();break}else if((K|0)==120){a[31104]=a[31104]&1^1;Zd();Ld();break}else if((K|0)==121){a[31105]=a[31105]&1^1;Zd();Ld();break}else{break}}K=c[7689]|0;O=ho(K|0)|0;if((L<<24>>24|0)==8|(L<<24>>24|0)==127){if((O|0)!=0){a[K+(O-1)|0]=0;Td();break}J=c[7688]|0;if(J>>>0<=1>>>0){break}c[7688]=J-1;Td();break}if(L<<24>>24<33|O>>>0>127>>>0){break}a[K+O|0]=L;L=c[30720+((c[7688]|0)-1<<2)>>2]|0;K=L+4|0;do{if((c[K>>2]|0)==0){za=c[7689]|0}else{J=L|0;P=0;fa=0;ha=0;do{ia=c[(c[J>>2]|0)+(P*28|0)+4>>2]|0;ja=(kn(ia,c[7689]|0)|0)==(ia|0);fa=(ja&1)+fa|0;ha=ja?P:ha;P=P+1|0;}while(P>>>0<(c[K>>2]|0)>>>0);if(fa>>>0>1>>>0){Td();break L6}P=c[7689]|0;if((fa|0)==0){za=P;break}ko(P|0,0,128)|0;P=c[J>>2]|0;ja=P+(ha*28|0)|0;ia=c[ja>>2]|0;if((ia|0)==0){ka=c[P+(ha*28|0)+8>>2]|0;if((ka|0)!=0){hd[ka&3]()}c[7688]=1;Aa=c[ja>>2]|0}else{Aa=ia}do{if((Aa|0)==1){ia=c[7688]|0;if(ia>>>0>=8>>>0){break}c[7688]=ia+1;c[30720+(ia<<2)>>2]=P+(ha*28|0)+12}}while(0);Td();break L6}}while(0);a[za+O|0]=0}}while(0);Hd(31192,29720,c[7784]|0);}while((Db(o|0)|0)!=0);i=f;return}function Kd(a){a=a|0;var d=0,e=0,f=0,g=0,i=0,j=0;d=a;c[7588]=c[d>>2];c[7589]=c[d+4>>2];c[7590]=c[d+8>>2];c[7591]=c[d+12>>2];c[7592]=c[d+16>>2];c[7593]=c[d+20>>2];c[7594]=c[d+24>>2];c[7595]=c[d+28>>2];d=a|0;e=a+8|0;f=a+16|0;g=a+24|0;a=~~(+h[f>>3]- +h[d>>3]);i=~~(+h[g>>3]- +h[e>>3]);Mn(c[7518]|0);j=Nn(ga(i&65535,a&65535)|0,4)|0;c[7518]=j;if((j|0)!=0){b[15040]=a;b[15038]=a;b[15039]=i;ko(30096,0,16)|0;h[3764]=+((a&65535)>>>0);h[3765]=+((i&65535)>>>0)}i=~~(+h[f>>3]- +h[d>>3]);d=~~(+h[g>>3]- +h[e>>3]);Mn(c[7542]|0);e=Nn(ga(d&65535,i&65535)|0,4)|0;c[7542]=e;if((e|0)==0){Zd();return}b[15088]=i;b[15086]=i;b[15087]=d;ko(30192,0,16)|0;h[3776]=+((i&65535)>>>0);h[3777]=+((d&65535)>>>0);Zd();return}function Ld(){var f=0,g=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0.0,r=0.0,s=0.0,t=0.0,u=0.0,v=0,w=0.0,x=0,y=0.0,z=0,A=0.0,B=0,D=0.0,E=0,F=0.0,G=0.0,H=0.0,I=0.0,J=0.0,K=0.0,L=0.0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0.0,X=0,Y=0,Z=0,_=0,$=0.0,aa=0.0,ba=0.0,ca=0.0,da=0.0,ea=0.0,fa=0.0,ha=0.0,ia=0.0,ja=0.0,ka=0.0,la=0.0,ma=0.0,na=0.0,oa=0.0,pa=0.0,qa=0.0,ra=0.0,sa=0,ta=0,ua=0,va=0;f=i;i=i+16|0;g=f|0;j=f+8|0;k=j;l=i;i=i+32|0;m=i;i=i+32|0;n=i;i=i+32|0;o=i;i=i+32|0;p=i;i=i+32|0;if((a[30336]&1)==0){q=+h[3787];r=+h[3786]}else{s=+h[3790];h[3787]=s;t=+h[3789];h[3786]=t;q=s;r=t}do{if((c[7586]|0)==1){t=+h[3788];if(t<=0.0){u=r;break}s=q-r;u=r+(s- +Ib(+s,+t))}else{u=r}}while(0);v=c[7517]|0;r=+h[3794];t=+h[3795];s=+h[3796];w=+h[3797];x=v+24|0;y=+h[x>>3];z=v+32|0;A=+h[z>>3];B=v+40|0;D=+h[B>>3];E=v+48|0;F=+h[E>>3];G=+((e[v+4>>1]|0)>>>0);H=+((e[v+6>>1]|0)>>>0);if(s<0.0|G<r|w<0.0|H<t){I=0.0;J=0.0;K=0.0;L=0.0}else{I=r>0.0?r:0.0;J=t>0.0?t:0.0;K=s<G?s:G;L=w<H?w:H}h[x>>3]=I;h[z>>3]=J;h[B>>3]=K;h[E>>3]=L;E=c[7517]|0;L=+h[3794];B=~~L;K=+h[3795];z=~~K;J=+h[E+24>>3];I=+h[E+40>>3];H=+(B|0);w=+(~~(H<J?J:H)|0);x=~~(w>I?I:w);w=+(~~(+h[3796]-L)+B|0);L=+(~~(w<J?J:w)|0);v=~~(L>I?I:L);L=+h[E+32>>3];I=+h[E+48>>3];w=+(z|0);J=+(~~(w<L?L:w)|0);M=~~(J>I?I:J);J=+(~~(+h[3797]-K)+z|0);K=+(~~(J<L?L:J)|0);z=~~(K>I?I:K);do{if((M|0)<(z|0)){N=E|0;O=E+8|0;if((x|0)>=(v|0)){P=E;break}Q=v-x<<2;R=M;S=0;while(1){ko((c[N>>2]|0)+(B+(ga(M+S|0,e[O>>1]|0)|0)<<2)|0,0,Q|0)|0;T=R+1|0;if((T|0)<(z|0)){R=T;S=S+1|0}else{break}}P=c[7517]|0}else{P=E}}while(0);c[j>>2]=c[7707];j=c[7706]|0;K=+(j>>>0>>>0);I=+h[3806]-K;J=+h[3807]-K;L=+h[3808]+K;w=+h[3809]+K;h[l>>3]=+Ib(+I,+L);h[l+8>>3]=+Ib(+J,+w);h[l+16>>3]=+tb(+I,+L);h[l+24>>3]=+tb(+J,+w);ae(P,k,l,j&65535);w=+h[3808];J=+h[3809];j=~~+h[3807];L19:do{if(+(j|0)<J){l=~~+h[3806];k=u!=q;L=q-u;P=j;E=c[7566]|0;while(1){z=c[7517]|0;if((P|0)>=(e[z+6>>1]|0)){break L19}I=+(e[z+4>>1]|0);M=~~(w<I?w:I);if((l|0)<(M|0)){B=ga(e[z+8>>1]|0,P)|0;x=(c[z>>2]|0)+(B+l<<2)|0;B=l;z=E;while(1){if((c[7587]|0)==0){if(k){U=(Pb(+(+Ib(+(+tb(+((+h[z>>3]-u)/L),+0.0)),+1.0)*16383.0))|0)&65535}else{U=0}v=(c[7621]|0)+(U<<2)|0;S=x;C=d[v]|d[v+1|0]<<8|d[v+2|0]<<16|d[v+3|0]<<24|0;a[S]=C;C=C>>8;a[S+1|0]=C;C=C>>8;a[S+2|0]=C;C=C>>8;a[S+3|0]=C}else{if(k){V=+Ib(+(+tb(+((+h[z>>3]-u)/L),+0.0)),+1.0)}else{V=0.0}I=+W(q);K=(-0.0- +h[z+16>>3])/I;H=+h[z+8>>3]/I;I=+tb(+0.0,+(+Ib(+255.0,+((V+H*1.13983)*255.0))));G=K;K=+tb(+0.0,+(+Ib(+255.0,+((V-G*.39465-H*.5806)*255.0))));H=+tb(+0.0,+(+Ib(+255.0,+((V+G*2.03211)*255.0))));S=Pb(+I)|0;v=Pb(+K)|0;R=x;C=((((S&255)*255|0)>>>0)/255|0)&255|((((v&255)*255|0)>>>0)/255|0)<<8&65280|(((((Pb(+H)|0)&255)*255|0)>>>0)/255|0)<<16|-16777216;a[R]=C;C=C>>8;a[R+1|0]=C;C=C>>8;a[R+2|0]=C;C=C>>8;a[R+3|0]=C}R=B+1|0;if((R|0)<(M|0)){x=x+4|0;B=R;z=z+24|0}else{break}}X=E+((M-l|0)*24|0)|0}else{X=E}z=P+1|0;if(+(z|0)<J){P=z;E=X}else{break}}}}while(0);Td();if((a[30832]&1)==0){if((a[30834]&1)!=0){Y=0;Z=32}}else{Y=(a[31104]&1)==0;Z=32}if((Z|0)==32){X=c[7517]|0;U=m;c[U>>2]=c[7612];c[U+4>>2]=c[7613];c[U+8>>2]=c[7614];c[U+12>>2]=c[7615];c[U+16>>2]=c[7616];c[U+20>>2]=c[7617];c[U+24>>2]=c[7618];c[U+28>>2]=c[7619];U=n;c[U>>2]=c[7604];c[U+4>>2]=c[7605];c[U+8>>2]=c[7606];c[U+12>>2]=c[7607];c[U+16>>2]=c[7608];c[U+20>>2]=c[7609];c[U+24>>2]=c[7610];c[U+28>>2]=c[7611];U=c[7706]|0;j=Fd(b[15142]|0)|0;ie(X,m,n,U,j,Y,(a[30834]&1)!=0)}if((a[30833]&1)==0){if((a[30834]&1)!=0){_=0;Z=36}}else{_=(a[31105]&1)==0;Z=36}if((Z|0)==36){Z=c[7517]|0;Y=o;c[Y>>2]=c[7612];c[Y+4>>2]=c[7613];c[Y+8>>2]=c[7614];c[Y+12>>2]=c[7615];c[Y+16>>2]=c[7616];c[Y+20>>2]=c[7617];c[Y+24>>2]=c[7618];c[Y+28>>2]=c[7619];Y=p;c[Y>>2]=c[7604];c[Y+4>>2]=c[7605];c[Y+8>>2]=c[7606];c[Y+12>>2]=c[7607];c[Y+16>>2]=c[7608];c[Y+20>>2]=c[7609];c[Y+24>>2]=c[7610];c[Y+28>>2]=c[7611];Y=c[7706]|0;j=Fd(b[15143]|0)|0;je(Z,o,p,Y,j,_,(a[30834]&1)!=0)}ee(+h[3860],+h[3861]);if((a[30840]&1)!=0){J=+h[3812];_=~~J;V=+(_|0);q=+h[3814];if(V<q){j=g;Y=_;u=V;V=q;w=J;while(1){_=(c[7621]|0)+(~~(16383.0-(u-w)*16383.0/(V-w))<<2)|0;p=c[7517]|0;o=~~+h[3811];Z=~~+h[3813];c[g>>2]=d[_]|d[_+1|0]<<8|d[_+2|0]<<16|d[_+3|0]<<24;Yd(p,j,o,Z,Y&65535,-1);Z=Y+1|0;L=+(Z|0);H=+h[3814];K=+h[3812];if(L<H){Y=Z;u=L;V=H;w=K}else{$=H;aa=K;break}}}else{$=q;aa=J}Y=c[7517]|0;J=+h[3811];q=+h[3813];j=Y+68|0;g=c[j>>2]|0;Z=Y+64|0;if((g|0)==(c[Z>>2]|0)){c[Z>>2]=g<<1;Z=Y+60|0;c[Z>>2]=On(c[Z>>2]|0,g<<6)|0}if((c[Y+72>>2]|0)==0){ba=0.0;ca=0.0}else{ba=+(b[Y+58>>1]|0);ca=+(b[Y+56>>1]|0)}g=c[j>>2]|0;c[j>>2]=g+1;j=c[Y+60>>2]|0;w=ca+ +((e[Y+4>>1]|0)>>>0);V=ba+ +((e[Y+6>>1]|0)>>>0);if(q<ca|w<J|$<ba|V<aa){da=0.0;ea=0.0;fa=0.0;ha=0.0}else{da=J>ca?J:ca;ea=aa>ba?aa:ba;fa=q<w?q:w;ha=$<V?$:V}h[j+(g<<5)>>3]=da;h[j+(g<<5)+8>>3]=ea;h[j+(g<<5)+16>>3]=fa;h[j+(g<<5)+24>>3]=ha}g=c[7517]|0;ha=+((e[g+4>>1]|0)>>>0);fa=+((e[g+6>>1]|0)>>>0);if(D<0.0|ha<y|F<0.0|fa<A){ia=0.0;ja=0.0;ka=0.0;la=0.0}else{ia=y>0.0?y:0.0;ja=A>0.0?A:0.0;ka=D<ha?D:ha;la=F<fa?F:fa}h[g+24>>3]=ia;h[g+32>>3]=ja;h[g+40>>3]=ka;h[g+48>>3]=la;g=c[7517]|0;la=+h[3794];ka=+h[3795];ja=+h[3796];ia=+h[3797];j=g+68|0;Y=c[j>>2]|0;Z=g+64|0;if((Y|0)==(c[Z>>2]|0)){c[Z>>2]=Y<<1;Z=g+60|0;c[Z>>2]=On(c[Z>>2]|0,Y<<6)|0}if((c[g+72>>2]|0)==0){ma=0.0;na=0.0}else{ma=+(b[g+58>>1]|0);na=+(b[g+56>>1]|0)}Y=c[j>>2]|0;c[j>>2]=Y+1;j=c[g+60>>2]|0;fa=na+ +((e[g+4>>1]|0)>>>0);F=ma+ +((e[g+6>>1]|0)>>>0);if(ja<na|fa<la|ia<ma|F<ka){oa=0.0;pa=0.0;qa=0.0;ra=0.0;sa=j+(Y<<5)|0;h[sa>>3]=oa;ta=j+(Y<<5)+8|0;h[ta>>3]=pa;ua=j+(Y<<5)+16|0;h[ua>>3]=qa;va=j+(Y<<5)+24|0;h[va>>3]=ra;i=f;return}oa=la>na?la:na;pa=ka>ma?ka:ma;qa=ja<fa?ja:fa;ra=ia<F?ia:F;sa=j+(Y<<5)|0;h[sa>>3]=oa;ta=j+(Y<<5)+8|0;h[ta>>3]=pa;ua=j+(Y<<5)+16|0;h[ua>>3]=qa;va=j+(Y<<5)+24|0;h[va>>3]=ra;i=f;return}function Md(d,f){d=d|0;f=f|0;var j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,s=0,t=0,u=0,v=0,x=0,y=0,z=0,A=0,B=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0,Z=0,_=0,$=0,aa=0,ba=0,ca=0,da=0,ea=0,fa=0,ha=0,ia=0,ja=0,ka=0.0,la=0.0,ma=0.0,na=0.0,oa=0.0,pa=0.0,qa=0,ra=0,sa=0,ta=0,ua=0,va=0,wa=0,xa=0,ya=0.0,za=0,Aa=0,Ba=0.0,Ca=0.0,Da=0,Ea=0,Fa=0,Ga=0.0,Ha=0.0,Ja=0.0,Ka=0.0,La=0,Oa=0.0,Pa=0.0,Qa=0.0,Sa=0.0,Ua=0.0,Va=0.0,Wa=0,Xa=0,Ya=0,_a=0;j=i;i=i+1440|0;k=j|0;l=j+88|0;m=j+96|0;n=j+176|0;o=j+688|0;p=j+696|0;q=j+1216|0;s=j+1232|0;t=j+1248|0;u=j+1264|0;v=j+1280|0;x=j+1296|0;y=j+1312|0;z=j+1328|0;A=j+1344|0;B=j+1360|0;D=j+1376|0;E=j+1392|0;F=j+1408|0;L1:do{if((d|0)>1){G=0;H=0;I=0;J=800;K=800;L=20;M=f;N=1;L2:while(1){O=M+4|0;P=c[O>>2]|0;L4:do{if(($a(P|0,1824)|0)==0){Q=L;R=K;S=J;T=I;U=1;V=G}else{do{if((ab(P|0,1808,8)|0)==0){Ma(P|0,248)|0;W=Ma(0,248)|0;if((W|0)==0){X=c[O>>2]|0;break}else{Ia(W|0,0,0)|0;Q=L;R=K;S=J;T=I;U=H;V=G;break L4}}else{X=P}}while(0);do{if((ab(X|0,1800,5)|0)==0){Ma(X|0,248)|0;W=Ma(0,248)|0;if((W|0)==0){Y=c[O>>2]|0;break}else{Ia(W|0,0,0)|0;Q=L;R=K;S=J;T=I;U=H;V=G;break L4}}else{Y=X}}while(0);do{if((ab(Y|0,1792,6)|0)==0){Ma(Y|0,248)|0;W=Ma(0,248)|0;if((W|0)==0){Z=c[O>>2]|0;break}else{Ia(W|0,0,0)|0;Q=L;R=K;S=J;T=I;U=H;V=G;break L4}}else{Z=Y}}while(0);do{if((ab(Z|0,1784,7)|0)==0){Ma(Z|0,248)|0;W=Ma(0,248)|0;if((W|0)==0){_=c[O>>2]|0;break}else{Q=L;R=K;S=(Ia(W|0,0,0)|0)&65535;T=I;U=H;V=G;break L4}}else{_=Z}}while(0);do{if((ab(_|0,1768,8)|0)==0){Ma(_|0,248)|0;W=Ma(0,248)|0;if((W|0)==0){$=c[O>>2]|0;break}else{Q=L;R=(Ia(W|0,0,0)|0)&65535;S=J;T=I;U=H;V=G;break L4}}else{$=_}}while(0);do{if((ab($|0,1744,8)|0)==0){Ma($|0,248)|0;W=Ma(0,248)|0;if((W|0)==0){aa=c[O>>2]|0;break}else{Q=(Ia(W|0,0,0)|0)&65535;R=K;S=J;T=I;U=H;V=G;break L4}}else{aa=$}}while(0);if(($a(aa|0,1728)|0)==0){Q=L;R=K;S=J;T=I;U=H;V=1;break}if(($a(aa|0,1704)|0)==0){Q=L;R=K;S=J;T=I;U=H;V=G;break}if(($a(aa|0,1672)|0)==0){Q=L;R=K;S=J;T=1;U=H;V=G}else{break L2}}}while(0);P=N+1|0;if((P|0)<(d|0)){G=V;H=U;I=T;J=S;K=R;L=Q;M=O;N=P}else{ba=V;ca=U;da=T;ea=S;fa=R;ha=Q;break L1}}if(($a(aa|0,1664)|0)==0){Gd(f,0);return 0}if(($a(aa|0,1648)|0)!=0){ba=G;ca=H;da=I;ea=J;fa=K;ha=L;break}Gd(f,0);return 0}else{ba=0;ca=0;da=0;ea=800;fa=800;ha=20}}while(0);Nd((Fb(65535)|0)!=-1,(f=i,i=i+1|0,i=i+7&-8,c[f>>2]=0,f)|0);i=f;aa=ea&65535;Q=fa&65535;c[7784]=Wb(aa|0,Q|0,32,150994945)|0;tc(1)|0;c[7816]=0;c[7801]=0;a[31208]=1;a[31280]=1;b[15625]=0;b[15624]=0;c[7814]=32;c[7813]=Ln(1024)|0;c[7815]=0;c[7818]=8;c[7817]=Nn(8,4)|0;c[7819]=0;R=ga(Q,aa)|0;aa=Nn(R,4)|0;Q=aa;c[7798]=Q;if((aa|0)==0){ia=b[15598]|0;ja=b[15599]|0;ka=+h[3902];la=+h[3904];ma=+h[3903];na=+h[3905]}else{b[15600]=ea;b[15598]=ea;b[15599]=fa;oa=+((ea&65535)>>>0);pa=+((fa&65535)>>>0);ko(31216,0,16)|0;h[3904]=oa;h[3905]=pa;ia=ea;ja=fa;ka=0.0;la=oa;ma=0.0;na=pa}pa=ka>0.0?+(~~ka|0):0.0;aa=~~(pa>la?la:pa);pa=+(ia&65535|0);oa=+(~~(pa<ka?ka:pa)|0);ia=~~(oa>la?la:oa);oa=ma>0.0?+(~~ma|0):0.0;S=~~(oa>na?na:oa);oa=+(ja&65535|0);la=+(~~(oa<ma?ma:oa)|0);ja=~~(la>na?na:la);L55:do{if((S|0)<(ja|0)&(aa|0)<(ia|0)){T=S;U=Q;while(1){V=U+((ga(e[15600]|0,T)|0)<<2)|0;d=aa;while(1){$=V;C=-16777216;a[$]=C;C=C>>8;a[$+1|0]=C;C=C>>8;a[$+2|0]=C;C=C>>8;a[$+3|0]=C;$=d+1|0;if(($|0)<(ia|0)){V=V+4|0;d=$}else{break}}d=T+1|0;if((d|0)>=(ja|0)){break L55}T=d;U=c[7798]|0}}}while(0);ja=F;a[29920]=0;b[14961]=-1;b[14962]=-1;a[29964]=1;a[29965]=0;c[7479]=bc(31288)|0;a[31122]=0;b[14956]=ea;b[14957]=fa;b[15560]=20;c[7448]=0;c[7433]=0;a[29736]=1;a[29808]=1;b[14889]=0;b[14888]=0;c[7446]=32;c[7445]=Ln(1024)|0;c[7447]=0;c[7450]=8;c[7449]=Nn(8,4)|0;c[7451]=0;ia=Nn(R,4)|0;c[7430]=ia;if((ia|0)==0){qa=b[14862]|0;ra=b[14863]|0}else{b[14864]=ea;b[14862]=ea;b[14863]=fa;ko(29744,0,16)|0;h[3720]=+((ea&65535)>>>0);h[3721]=+((fa&65535)>>>0);qa=ea;ra=fa}ko(3e4,0,32)|0;c[ja>>2]=c[7500];c[ja+4>>2]=c[7501];c[ja+8>>2]=c[7502];c[ja+12>>2]=c[7503];c[ja+16>>2]=c[7504];c[ja+20>>2]=c[7505];c[ja+24>>2]=c[7506];c[ja+28>>2]=c[7507];ja=q;q=s;s=t;t=u;u=v;v=x;x=y;y=z;z=A;A=B;B=D;D=E;c[7516]=1;c[7566]=0;c[7585]=1;c[7586]=0;c[7587]=0;c[7706]=1;c[7707]=-5592406;a[30832]=0;a[30833]=0;a[30834]=0;b[15142]=-1;b[15143]=-1;c[7709]=20;h[3851]=w;h[3852]=w;ko(30304,0,16)|0;h[3790]=1.0;a[30336]=1;ko(30384,0,32)|0;c[7517]=29720;c[7620]=0;c[7724]=29720;c[7725]=-14022648;c[7726]=-5592406;c[7727]=1145324612;c[7728]=-1;c[7729]=4136;c[7730]=4136;c[7748]=1;a[30996]=0;a[30997]=0;c[7750]=29720;c[7751]=-14022648;c[7752]=-5592406;c[7753]=1145324612;c[7754]=-1;c[7755]=4136;c[7756]=4136;c[7774]=1;a[31100]=0;a[31101]=0;h[3860]=-1.0;h[3861]=-1.0;h[3858]=-1.0;h[3859]=-1.0;a[31104]=0;a[31105]=0;c[7778]=140;c[7777]=100;a[30840]=1;c[7536]=0;c[7521]=0;a[30088]=1;a[30160]=1;b[15065]=0;b[15064]=0;c[7534]=32;c[7533]=Ln(1024)|0;c[7535]=0;c[7538]=8;c[7537]=Nn(8,4)|0;c[7539]=0;E=ga(qa&65535,ra&65535)|0;ia=Nn(E,4)|0;c[7518]=ia;if((ia|0)!=0){b[15040]=qa;b[15038]=qa;b[15039]=ra;ko(30096,0,16)|0;h[3764]=+((qa&65535)>>>0);h[3765]=+((ra&65535)>>>0)}a[30160]=0;c[7560]=0;c[7545]=0;a[30184]=1;a[30256]=1;b[15113]=0;b[15112]=0;c[7558]=32;c[7557]=Ln(1024)|0;c[7559]=0;c[7562]=8;c[7561]=Nn(8,4)|0;c[7563]=0;ia=Nn(E,4)|0;c[7542]=ia;if((ia|0)!=0){b[15088]=qa;b[15086]=qa;b[15087]=ra;ko(30192,0,16)|0;h[3776]=+((qa&65535)>>>0);h[3777]=+((ra&65535)>>>0)}a[30256]=0;ra=c[7451]|0;L73:do{if((ra|0)==(c[7450]|0)){c[7450]=ra<<1;qa=On(c[7449]|0,ra<<3)|0;c[7449]=qa;ia=c[7450]|0;if(ra>>>0>=ia>>>0){break}c[qa+(ra<<2)>>2]=0;E=ra+1|0;if(E>>>0>=ia>>>0){break}c[qa+(E<<2)>>2]=0;E=ra+2|0;if(E>>>0>=ia>>>0){break}c[qa+(E<<2)>>2]=0;E=ra+3|0;if(E>>>0<ia>>>0){sa=E;ta=qa}else{break}while(1){c[ta+(sa<<2)>>2]=0;qa=sa+1|0;if(qa>>>0>=(c[7450]|0)>>>0){break L73}sa=qa;ta=c[7449]|0}}}while(0);c[7560]=29720;ta=c[7451]|0;c[7451]=ta+1;c[(c[7449]|0)+(ta<<2)>>2]=30168;ta=c[7517]|0;sa=ta+84|0;ra=c[sa>>2]|0;qa=ta+80|0;do{if((ra|0)==(c[qa>>2]|0)){c[qa>>2]=ra<<1;E=ta+76|0;ia=On(c[E>>2]|0,ra<<3)|0;c[E>>2]=ia;if(ra>>>0>=(c[qa>>2]|0)>>>0){ua=E;break}c[ia+(ra<<2)>>2]=0;ia=ra+1|0;if(ia>>>0<(c[qa>>2]|0)>>>0){va=ia}else{ua=E;break}while(1){c[(c[E>>2]|0)+(va<<2)>>2]=0;ia=va+1|0;if(ia>>>0<(c[qa>>2]|0)>>>0){va=ia}else{ua=E;break}}}else{ua=ta+76|0}}while(0);c[7536]=ta;ta=c[sa>>2]|0;c[sa>>2]=ta+1;c[(c[ua>>2]|0)+(ta<<2)>>2]=30072;a[30760]=0;c[7654]=30168;c[7657]=-390744651;c[7658]=-14544623;c[7659]=-2147483648;c[7660]=-8355712;ko(30648,0,32)|0;c[7671]=10;c[7672]=25;g[7670]=4.0;c[7688]=0;c[7689]=Nn(129,1)|0;ko(v|0,0,16)|0;ta=bc(576)|0;ko(x|0,0,16)|0;ua=bc(552)|0;ko(y|0,0,16)|0;sa=bc(528)|0;ko(z|0,0,16)|0;va=bc(488)|0;ko(A|0,0,16)|0;qa=bc(464)|0;ko(B|0,0,16)|0;ra=bc(440)|0;ko(D|0,0,16)|0;E=bc(424)|0;ia=bc(600)|0;R=Ln(196)|0;c[R>>2]=0;c[R+4>>2]=ta;c[R+8>>2]=0;ta=R+12|0;c[ta>>2]=c[v>>2];c[ta+4>>2]=c[v+4>>2];c[ta+8>>2]=c[v+8>>2];c[ta+12>>2]=c[v+12>>2];c[R+28>>2]=0;c[R+32>>2]=ua;c[R+36>>2]=0;ua=R+40|0;c[ua>>2]=c[x>>2];c[ua+4>>2]=c[x+4>>2];c[ua+8>>2]=c[x+8>>2];c[ua+12>>2]=c[x+12>>2];c[R+56>>2]=0;c[R+60>>2]=sa;c[R+64>>2]=0;sa=R+68|0;c[sa>>2]=c[y>>2];c[sa+4>>2]=c[y+4>>2];c[sa+8>>2]=c[y+8>>2];c[sa+12>>2]=c[y+12>>2];c[R+84>>2]=0;c[R+88>>2]=va;c[R+92>>2]=0;va=R+96|0;c[va>>2]=c[z>>2];c[va+4>>2]=c[z+4>>2];c[va+8>>2]=c[z+8>>2];c[va+12>>2]=c[z+12>>2];c[R+112>>2]=0;c[R+116>>2]=qa;c[R+120>>2]=0;qa=R+124|0;c[qa>>2]=c[A>>2];c[qa+4>>2]=c[A+4>>2];c[qa+8>>2]=c[A+8>>2];c[qa+12>>2]=c[A+12>>2];c[R+140>>2]=0;c[R+144>>2]=ra;c[R+148>>2]=0;ra=R+152|0;c[ra>>2]=c[B>>2];c[ra+4>>2]=c[B+4>>2];c[ra+8>>2]=c[B+8>>2];c[ra+12>>2]=c[B+12>>2];c[R+168>>2]=0;c[R+172>>2]=E;c[R+176>>2]=0;E=R+180|0;c[E>>2]=c[D>>2];c[E+4>>2]=c[D+4>>2];c[E+8>>2]=c[D+8>>2];c[E+12>>2]=c[D+12>>2];ko(ja|0,0,16)|0;D=bc(392)|0;ko(q|0,0,16)|0;E=bc(376)|0;ko(s|0,0,16)|0;B=bc(360)|0;ko(t|0,0,16)|0;ra=bc(336)|0;ko(u|0,0,16)|0;A=bc(320)|0;qa=bc(31288)|0;z=Ln(168)|0;c[z>>2]=1;c[z+4>>2]=ia;c[z+8>>2]=0;c[z+12>>2]=R;c[z+16>>2]=7;c[z+20>>2]=-1;c[z+24>>2]=-1;c[z+28>>2]=0;c[z+32>>2]=D;c[z+36>>2]=0;D=z+40|0;c[D>>2]=c[ja>>2];c[D+4>>2]=c[ja+4>>2];c[D+8>>2]=c[ja+8>>2];c[D+12>>2]=c[ja+12>>2];c[z+56>>2]=0;c[z+60>>2]=E;c[z+64>>2]=0;E=z+68|0;c[E>>2]=c[q>>2];c[E+4>>2]=c[q+4>>2];c[E+8>>2]=c[q+8>>2];c[E+12>>2]=c[q+12>>2];c[z+84>>2]=0;c[z+88>>2]=B;c[z+92>>2]=0;B=z+96|0;c[B>>2]=c[s>>2];c[B+4>>2]=c[s+4>>2];c[B+8>>2]=c[s+8>>2];c[B+12>>2]=c[s+12>>2];c[z+112>>2]=0;c[z+116>>2]=ra;c[z+120>>2]=0;ra=z+124|0;c[ra>>2]=c[t>>2];c[ra+4>>2]=c[t+4>>2];c[ra+8>>2]=c[t+8>>2];c[ra+12>>2]=c[t+12>>2];c[z+140>>2]=0;c[z+144>>2]=A;c[z+148>>2]=0;A=z+152|0;c[A>>2]=c[u>>2];c[A+4>>2]=c[u+4>>2];c[A+8>>2]=c[u+8>>2];c[A+12>>2]=c[u+12>>2];c[7673]=1;c[7674]=qa;c[7675]=0;c[7676]=z;c[7677]=6;c[7678]=-1;c[7679]=-1;c[7680]=30704;c[7688]=1;Td();a[30761]=0;ko(30768,0,32)|0;c[7621]=Od(1)|0;Kd(F);b[15560]=ha;if(ca<<24>>24!=0){c[7621]=Od(0)|0}if(ba<<24>>24!=0){a[30840]=0}c[7586]=1;if(da<<24>>24!=0){c[7586]=0;c[7587]=1}da=n|0;ba=p;ca=Yb(1472,0,(f=i,i=i+8|0,c[f>>2]=432,f)|0)|0;i=f;L98:do{if((ca|0)<0){wa=74}else{ha=512;while(1){F=Ra(ca|0,n+(512-ha)|0,ha|0)|0;if((F|0)<0){wa=70;break}if((ha|0)==(F|0)){break}else{ha=ha-F|0}}if((wa|0)==70){jb(ca|0)|0;wa=74;break}if((jo(da|0,664,4)|0)!=0){wa=74;break}if((jo(n+4|0,640,4)|0)!=0){wa=74;break}ha=(jo(n+8|0,640,4)|0)==0;F=ha?ca:-1;if((F|0)<0){wa=74;break}mc(F|0,m|0)|0;ha=c[m+36>>2]|0;if((ha|0)<512){z=c[r>>2]|0;Cc(z|0,760,(f=i,i=i+8|0,c[f>>2]=1472,f)|0)|0;i=f;xa=z;wa=81;break}z=Jb(0,ha|0,1,1,F|0,0)|0;jb(F|0)|0;if((z|0)==-1){F=c[r>>2]|0;Cc(F|0,704,(f=i,i=i+8|0,c[f>>2]=1472,f)|0)|0;i=f;xa=F;wa=81;break}F=n+64|0;c[F>>2]=z;c[F+4>>2]=0;F=n+72|0;c[F>>2]=ha;c[F+4>>2]=(ha|0)<0|0?-1:0;ha=z+~~+h[n+32>>3]|0;if((ha|0)==0){xa=c[r>>2]|0;wa=81;break}if((c[n+48>>2]|0)!=2e3){Na(1320,45,1,c[r>>2]|0)|0;break}la=+h[n+56>>3];z=c[n+160>>2]|0;L120:do{if((z|0)<1){ya=0.0}else{F=n+164|0;qa=z;while(1){if((a[F]|0)==0){ya=0.0;break L120}h[l>>3]=0.0;u=xc(F|0,904,(f=i,i=i+8|0,c[f>>2]=l,f)|0)|0;i=f;if((u|0)==1){break}u=F+((ho(F|0)|0)+1)|0;A=qa-1-(ho(u|0)|0)|0;if((A|0)<1){ya=0.0;break L120}else{F=u;qa=A}}ya=+h[l>>3]}}while(0);na=ya+ +Gb(+(+Za(+la,o|0)*1.0e6))/1.0e6;do{if((c[7782]|0)==(c[7781]|0)){if((c[n+296>>2]|0)==1){h[3744]=+h[o>>3];break}else{h[3744]=0.0;break}}}while(0);z=c[n+276>>2]|0;qa=(~~+h[n+40>>3]>>>0)/(z>>>0)|0;F=a[n+52|0]|0;A=F<<24>>24;switch(A|0){case 67:{za=2;break};case 86:{za=3;break};case 81:{za=4;break};case 77:{za=9;break};case 84:{za=16;break};case 83:{za=1;break};default:{Cc(c[r>>2]|0,1032,(f=i,i=i+8|0,c[f>>2]=A,f)|0)|0;i=f;mb(1);return 0}}A=a[n+53|0]|0;switch(A|0){case 73:{Aa=2;break};case 76:{Aa=4;break};case 88:{Aa=8;break};case 70:{Aa=4;break};case 68:{Aa=8;break};case 65:case 66:case 79:{Aa=1;break};default:{Cc(c[r>>2]|0,960,(f=i,i=i+8|0,c[f>>2]=A,f)|0)|0;i=f;mb(1);return 0}}u=(qa>>>0)/((ga(Aa,za)|0)>>>0)|0;qa=c[n+296>>2]|0;if((qa|0)==1){Ba=na+(+h[o>>3]- +h[3744])}else{Ba=0.0}la=+h[n+280>>3]+Ba;oa=+h[n+256>>3];ma=+(z>>>0>>>0);pa=+(u>>>0>>>0);ka=oa+ma*+h[n+264>>3];Ca=la+pa*+h[n+288>>3];u=c[n+272>>2]|0;z=u&65535;t=qa&65535;ra=k;s=c[7569]|0;B=b[15142]|0;do{if((s|0)==(c[7568]|0)){if(B<<16>>16<0){b[15142]=z;Da=z}else{Da=B}if((b[15143]|0)>=0){Ea=Da;break}b[15143]=t;Ea=Da}else{Ea=B}}while(0);do{if((u&65535|0)==(Ea<<16>>16|0)){if((qa&65535|0)!=(b[15143]|0)){break}B=c[7516]|0;c[7516]=B+1;q=k|0;c[q>>2]=B;B=k+8|0;h[B>>3]=oa;E=k+16|0;h[E>>3]=la;ja=k+24|0;h[ja>>3]=ka;D=k+32|0;h[D>>3]=Ca;ko(k+40|0,0,16)|0;h[k+56>>3]=ma;h[k+64>>3]=pa;b[k+72>>1]=z;b[k+74>>1]=t;c[k+76>>2]=ha;R=F<<24>>24==83;c[k+84>>2]=R?1:2;if(!((F<<24>>24|0)==83|(F<<24>>24|0)==67)){Na(1112,46,1,c[r>>2]|0)|0;break L98}switch(A|0){case 66:{c[k+80>>2]=R?0:6;break};case 73:{c[k+80>>2]=R?1:7;break};case 76:{c[k+80>>2]=R?2:8;break};case 88:{c[k+80>>2]=R?3:9;break};case 70:{c[k+80>>2]=R?4:10;break};case 68:{c[k+80>>2]=R?5:11;break};default:{}}if((s|0)==(c[7570]|0)){Rd(30272,k);Fa=c[7569]|0;Ga=+h[B>>3];Ha=+h[E>>3];Ja=+h[ja>>3];Ka=+h[D>>3]}else{if((s|0)==0){La=0}else{io(s|0,ra|0,88)|0;La=c[7569]|0}D=La+88|0;c[7569]=D;Fa=D;Ga=oa;Ha=la;Ja=ka;Ka=Ca}na=+h[3799];Oa=+h[3800];Pa=+h[3801];Qa=+Ib(+(+h[3798]),+Ga);Sa=+Ib(+na,+Ha);na=+tb(+Oa,+Ja);Oa=+tb(+Pa,+Ka);Pa=+Ib(+Qa,+na);Ua=+Ib(+Sa,+Oa);Va=+tb(+Qa,+na);na=+tb(+Sa,+Oa);h[3798]=Pa;h[3799]=Ua;h[3800]=Va;h[3801]=na;if((Fa-(c[7568]|0)|0)==88){c[7604]=c[7596];c[7605]=c[7597];c[7606]=c[7598];c[7607]=c[7599];c[7608]=c[7600];c[7609]=c[7601];c[7610]=c[7602];c[7611]=c[7603]}D=c[q>>2]|0;if((D|0)==0){break L98}c[p>>2]=D;io(p+8|0,da|0,512)|0;D=c[7782]|0;if((D|0)==(c[7783]|0)){Qd(31124,p);break L98}if((D|0)==0){Wa=0}else{io(D|0,ba|0,520)|0;Wa=c[7782]|0}c[7782]=Wa+520;break L98}}while(0);Na(1192,62,1,c[r>>2]|0)|0}}while(0);if((wa|0)==74){Wa=c[r>>2]|0;Cc(Wa|0,824,(f=i,i=i+8|0,c[f>>2]=1472,f)|0)|0;i=f;xa=Wa;wa=81}if((wa|0)==81){Cc(xa|0,1408,(f=i,i=i+8|0,c[f>>2]=1472,f)|0)|0;i=f;Ta(1368)}c[7604]=c[7596];c[7605]=c[7597];c[7606]=c[7598];c[7607]=c[7599];c[7608]=c[7600];c[7609]=c[7601];c[7610]=c[7602];c[7611]=c[7603];Ld();f=(e[14956]|0)-10|0;xa=(e[14957]|0)-10|0;h[3750]=5.0;h[3751]=5.0;h[3752]=((f|0)<0?0.0:+(f|0))+5.0;h[3753]=((xa|0)<0?0.0:+(xa|0))+5.0;Kd(3e4);b[14956]=ea;b[14957]=fa;Ld();Hd(31192,29720,c[7784]|0);Qc(2,0,1);c[7517]=0;ko(30352,0,64)|0;c[7604]=c[7596];c[7605]=c[7597];c[7606]=c[7598];c[7607]=c[7599];c[7608]=c[7600];c[7609]=c[7601];c[7610]=c[7602];c[7611]=c[7603];c[7621]=0;fa=c[7620]|0;if((fa|0)!=0){ea=fa;while(1){Mn(c[ea+72>>2]|0);fa=c[ea>>2]|0;Mn(ea);if((fa|0)==0){break}else{ea=fa}}}ea=c[7568]|0;fa=c[7569]|0;if((ea|0)==(fa|0)){Xa=c[7566]|0;Ya=Xa;Mn(Ya);_a=c[7689]|0;Mn(_a);Pd(30692);i=j;return 0}c[7569]=fa+(~(((fa-88+(-ea|0)|0)>>>0)/88|0)*88|0);Xa=c[7566]|0;Ya=Xa;Mn(Ya);_a=c[7689]|0;Mn(_a);Pd(30692);i=j;return 0}function Nd(a,b){a=a|0;b=b|0;var d=0,e=0;d=i;i=i+2064|0;e=d+2048|0;if(a){i=d;return}else{a=e;c[a>>2]=b;c[a+4>>2]=0;a=d|0;sc(a|0,1528,e|0)|0;Cc(c[r>>2]|0,272,(e=i,i=i+32|0,c[e>>2]=1624,c[e+8>>2]=260,c[e+16>>2]=1584,c[e+24>>2]=a,e)|0)|0;i=e;mb(1)}}function Od(b){b=b|0;var d=0,e=0,f=0,g=0;do{if((a[36592]|0)==0){if((yb(36592)|0)==0){break}a[33952]=0;a[33953]=0;a[33954]=0;a[33955]=-1;h[4245]=0.0;a[33968]=45;a[33969]=45;a[33970]=45;a[33971]=-1;h[4247]=.176;a[33984]=91;a[33985]=91;a[33986]=91;a[33987]=-1;h[4249]=.356;a[34e3]=-116;a[34001]=-116;a[34002]=-116;a[34003]=-1;h[4251]=.549;a[34016]=-73;a[34017]=-73;a[34018]=-73;a[34019]=-1;h[4253]=.718;a[34032]=-35;a[34033]=-35;a[34034]=-35;a[34035]=-1;h[4255]=.867;c[8512]=-1;h[4257]=1.0}}while(0);do{if((a[36600]|0)==0){if((yb(36600)|0)==0){break}a[34064]=0;a[34065]=0;a[34066]=0;a[34067]=-1;h[4259]=0.0;a[34080]=86;a[34081]=0;a[34082]=-103;a[34083]=-1;h[4261]=.176;a[34096]=-109;a[34097]=51;a[34098]=119;a[34099]=-1;h[4263]=.356;a[34112]=-30;a[34113]=51;a[34114]=71;a[34115]=-1;h[4265]=.549;a[34128]=-1;a[34129]=109;a[34130]=0;a[34131]=-1;h[4267]=.718;a[34144]=-1;a[34145]=-73;a[34146]=0;a[34147]=-1;h[4269]=.867;a[34160]=-1;a[34161]=-1;a[34162]=-63;a[34163]=-1;h[4271]=1.0}}while(0);do{if((a[36608]|0)==0){if((yb(36608)|0)==0){break}a[34184]=0;a[34185]=0;a[34186]=0;a[34187]=-1;h[4274]=0.0;a[34200]=0;a[34201]=0;a[34202]=127;a[34203]=-1;h[4276]=.176;a[34216]=0;a[34217]=63;a[34218]=-103;a[34219]=-1;h[4278]=.356;a[34232]=76;a[34233]=114;a[34234]=-78;a[34235]=-1;h[4280]=.549;a[34248]=-103;a[34249]=-91;a[34250]=-52;a[34251]=-1;h[4282]=.718;a[34264]=-52;a[34265]=-40;a[34266]=-27;a[34267]=-1;h[4284]=.867;c[8570]=-1;h[4286]=1.0}}while(0);do{if((a[36624]|0)==0){if((yb(36624)|0)==0){break}a[34368]=0;a[34369]=0;a[34370]=-113;a[34371]=-1;h[4297]=0.0;a[34384]=0;a[34385]=0;a[34386]=-1;a[34387]=-1;h[4299]=.125;a[34400]=0;a[34401]=-1;a[34402]=-1;a[34403]=-1;h[4301]=.375;a[34416]=-1;a[34417]=-1;a[34418]=0;a[34419]=-1;h[4303]=.625;a[34432]=-1;a[34433]=0;a[34434]=0;a[34435]=-1;h[4305]=.875;a[34448]=-128;a[34449]=0;a[34450]=0;a[34451]=-1;h[4307]=1.0}}while(0);do{if((a[36616]|0)==0){if((yb(36616)|0)==0){break}a[34304]=0;a[34305]=0;a[34306]=1;a[34307]=-1;h[4289]=0.0;a[34320]=85;a[34321]=86;a[34322]=117;a[34323]=-1;h[4291]=.375;a[34336]=-88;a[34337]=-58;a[34338]=-58;a[34339]=-1;h[4293]=.734;c[8588]=-1;h[4295]=1.0}}while(0);d=c[8574]|0;if((d|0)==0){e=Ln(65536)|0;c[8574]=e;f=e}else{f=d}do{if((c[8544]|0)==(b|0)){if(a[4448]|0){g=f}else{break}return g|0}}while(0);switch(b|0){case 3:{Sd(f,34368,6);break};case 4:{Sd(f,34304,4);break};case 2:{Sd(f,34184,7);break};case 1:{Sd(f,34064,7);break};case 0:{Sd(f,33952,7);break};default:{}}c[8544]=b;a[4448]=1;g=c[8574]|0;return g|0}function Pd(a){a=a|0;var b=0,d=0,e=0,f=0,g=0,h=0;Mn(c[a+4>>2]|0);if((c[a>>2]|0)!=1){return}b=a+16|0;d=a+12|0;a=c[d>>2]|0;if((c[b>>2]|0)==0){e=a}else{f=0;g=a;while(1){Pd(g+(f*28|0)|0);a=f+1|0;h=c[d>>2]|0;if(a>>>0<(c[b>>2]|0)>>>0){f=a;g=h}else{e=h;break}}}Mn(e);return}function Qd(a,b){a=a|0;b=b|0;var d=0,e=0,f=0,g=0,h=0,i=0,j=0,k=0,l=0,m=0,n=0,o=0;d=a+4|0;e=a|0;f=c[e>>2]|0;g=f;h=(c[d>>2]|0)-g|0;i=(h|0)/520|0;j=i+1|0;if(j>>>0>8259552>>>0){Qk(0)}k=a+8|0;a=((c[k>>2]|0)-g|0)/520|0;if(a>>>0>4129775>>>0){l=8259552;m=5}else{g=a<<1;a=g>>>0<j>>>0?j:g;if((a|0)==0){n=0;o=0}else{l=a;m=5}}if((m|0)==5){n=Rn(l*520|0)|0;o=l}l=n+(i*520|0)|0;if((l|0)!=0){io(l|0,b|0,520)|0}b=n+((((h|0)/-520|0)+i|0)*520|0)|0;i=f;io(b|0,i|0,h)|0;c[e>>2]=b;c[d>>2]=n+(j*520|0);c[k>>2]=n+(o*520|0);if((f|0)==0){return}Tn(i);return}function Rd(a,b){a=a|0;b=b|0;var d=0,e=0,f=0,g=0,h=0,i=0,j=0,k=0,l=0,m=0,n=0,o=0;d=a+4|0;e=a|0;f=c[e>>2]|0;g=f;h=(c[d>>2]|0)-g|0;i=(h|0)/88|0;j=i+1|0;if(j>>>0>48806446>>>0){Qk(0)}k=a+8|0;a=((c[k>>2]|0)-g|0)/88|0;if(a>>>0>24403222>>>0){l=48806446;m=5}else{g=a<<1;a=g>>>0<j>>>0?j:g;if((a|0)==0){n=0;o=0}else{l=a;m=5}}if((m|0)==5){n=Rn(l*88|0)|0;o=l}l=n+(i*88|0)|0;if((l|0)!=0){io(l|0,b|0,88)|0}b=n+((((h|0)/-88|0)+i|0)*88|0)|0;i=f;io(b|0,i|0,h)|0;c[e>>2]=b;c[d>>2]=n+(j*88|0);c[k>>2]=n+(o*88|0);if((f|0)==0){return}Tn(i);return}function Sd(b,c,e){b=b|0;c=c|0;e=e|0;var f=0,g=0,i=0,j=0,k=0,l=0,m=0,n=0.0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0;f=e-1|0;if((f|0)!=0){e=0;g=b;while(1){i=e+1|0;j=~~(+h[c+(i<<4)+8>>3]*16383.0);k=~~(+h[c+(e<<4)+8>>3]*16383.0);l=j-k|0;if((j|0)==(k|0)){m=g}else{k=c+(e<<4)|0;j=c+(i<<4)|0;n=+(l>>>0>>>0);o=c+(e<<4)+1|0;p=c+(i<<4)+1|0;q=c+(e<<4)+2|0;r=c+(i<<4)+2|0;s=g;t=0;while(1){u=l-t|0;v=ga(d[k]|0,u)|0;a[s|0]=~~(+(((ga(d[j]|0,t)|0)+v|0)>>>0>>>0)/n);v=ga(d[o]|0,u)|0;a[s+1|0]=~~(+(((ga(d[p]|0,t)|0)+v|0)>>>0>>>0)/n);v=ga(d[q]|0,u)|0;a[s+2|0]=~~(+(((ga(d[r]|0,t)|0)+v|0)>>>0>>>0)/n);a[s+3|0]=-1;v=t+1|0;if(v>>>0<l>>>0){s=s+4|0;t=v}else{break}}m=g+(l<<2)|0}if(i>>>0<f>>>0){e=i;g=m}else{break}}}m=c+(f<<4)|0;f=b+65532|0;C=d[m]|d[m+1|0]<<8|d[m+2|0]<<16|d[m+3|0]<<24|0;a[f]=C;C=C>>8;a[f+1|0]=C;C=C>>8;a[f+2|0]=C;C=C>>8;a[f+3|0]=C;return}function Td(){var f=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0.0,B=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0.0,V=0.0,W=0.0,X=0.0,Y=0.0,Z=0.0,_=0.0,$=0.0,aa=0.0,ba=0,ca=0,da=0,ea=0,fa=0,ha=0,ia=0,ja=0,ka=0,la=0,ma=0,na=0,oa=0.0,pa=0.0,qa=0.0,ra=0.0,sa=0,ta=0,ua=0,va=0,wa=0,xa=0,ya=0,za=0,Aa=0,Ba=0,Ca=0,Da=0,Ea=0,Fa=0,Ga=0,Ha=0,Ia=0.0,Ja=0.0,Ka=0,La=0,Ma=0,Na=0,Oa=0.0,Pa=0.0,Qa=0.0,Ra=0.0,Sa=0,Ta=0,Ua=0.0,Va=0.0,Wa=0.0,Xa=0.0,Ya=0.0,Za=0.0;f=i;i=i+128|0;j=f|0;k=f+8|0;l=f+16|0;m=f+24|0;n=f+32|0;o=f+40|0;p=f+48|0;q=f+56|0;r=f+64|0;s=f+72|0;t=f+80|0;u=f+88|0;v=f+96|0;w=f+104|0;x=f+112|0;y=f+120|0;z=c[7671]|0;A=+g[7670];B=c[7688]|0;if((B|0)==0){i=f;return}D=c[7672]|0;E=0;F=0;G=0;do{H=c[30720+(E<<2)>>2]|0;I=c[H>>2]|0;J=c[H+4>>2]|0;H=(E|0)!=0?8:0;if((J|0)==0){K=0}else{L=0;M=0;while(1){N=c[I+(L*28|0)+4>>2]|0;O=ho(N|0)|0;if((O|0)==0){P=0}else{Q=0;R=0;while(1){S=(b[21240+((a[N+Q|0]|0)-32<<4)>>1]|0)+R&65535;T=Q+1|0;if(T>>>0<O>>>0){Q=T;R=S}else{P=S;break}}}R=(P&65535|0)>(M<<16>>16|0)?P:M;Q=L+1|0;if(Q>>>0<J>>>0){L=Q;M=R}else{K=R;break}}}M=(ga(D,J)|0)&65535;G=(G+H&65535)+K&65535;F=M<<16>>16>F<<16>>16?M:F;E=E+1|0;}while(E>>>0<B>>>0);E=~~(+((c[7655]|0)>>>0>>>0)- +(G<<16>>16|0)*.5);K=F<<16>>16;D=~~(+((c[7656]|0)>>>0>>>0)- +(F<<16>>16|0)*.5);U=+((E-z|0)>>>0>>>0)-A+-1.0;V=+((D-z|0)>>>0>>>0)-A+-1.0;W=(+(z>>>0>>>0)+A+1.0)*2.0;A=W+ +(G<<16>>16|0)+U;X=W+ +(F<<16>>16|0)+V;F=c[7654]|0;W=+h[3831];z=~~W;Y=+h[3832];P=~~Y;Z=+h[F+24>>3];_=+h[F+40>>3];$=+(z|0);aa=+(~~($<Z?Z:$)|0);M=~~(aa>_?_:aa);aa=+(~~(+h[3833]-W)+z|0);W=+(~~(aa<Z?Z:aa)|0);L=~~(W>_?_:W);W=+h[F+32>>3];_=+h[F+48>>3];aa=+(P|0);Z=+(~~(aa<W?W:aa)|0);I=~~(Z>_?_:Z);Z=+(~~(+h[3834]-Y)+P|0);Y=+(~~(Z<W?W:Z)|0);P=~~(Y>_?_:Y);do{if((I|0)<(P|0)){R=F|0;Q=F+8|0;if((M|0)>=(L|0)){ba=B;break}O=L-M<<2;N=I;S=0;while(1){ko((c[R>>2]|0)+(z+(ga(I+S|0,e[Q>>1]|0)|0)<<2)|0,0,O|0)|0;T=N+1|0;if((T|0)<(P|0)){N=T;S=S+1|0}else{break}}ba=c[7688]|0}else{ba=B}}while(0);B=E&65535;E=D&65535;if((ba|0)==0){ca=E<<16>>16}else{ba=y|0;Y=+(E<<16>>16|0);_=+(~~Y|0);P=E<<16>>16;I=w;z=x;M=y+1|0;L=y+2|0;F=y+3|0;S=v;N=B;O=0;while(1){Q=(O|0)!=0?8:0;R=c[30720+(O<<2)>>2]|0;H=c[R>>2]|0;J=c[R+4>>2]|0;R=Q&65535;T=(J|0)==0;if(T){da=0}else{ea=0;fa=0;while(1){ha=c[H+(ea*28|0)+4>>2]|0;ia=ho(ha|0)|0;if((ia|0)==0){ja=0}else{ka=0;la=0;while(1){ma=(b[21240+((a[ha+ka|0]|0)-32<<4)>>1]|0)+la&65535;na=ka+1|0;if(na>>>0<ia>>>0){ka=na;la=ma}else{ja=ma;break}}}la=(ja&65535|0)>(fa<<16>>16|0)?ja:fa;ka=ea+1|0;if(ka>>>0<J>>>0){ea=ka;fa=la}else{da=la;break}}}fa=da+R&65535;ea=(ga(c[7672]|0,J)|0)&65535;la=c[7654]|0;ka=c[7657]|0;Z=+(N<<16>>16|0);W=+h[la+24>>3];aa=+h[la+40>>3];$=+(~~Z|0);oa=+(~~($<W?W:$)|0);ia=~~(oa>aa?aa:oa);ha=ia&65535;oa=+(~~(Z+ +(fa<<16>>16|0))|0);Z=+(~~(oa<W?W:oa)|0);W=+h[la+32>>3];pa=+h[la+48>>3];qa=+(~~(_<W?W:_)|0);ma=~~(qa>pa?pa:qa);qa=+(~~(Y+ +(ea<<16>>16|0))|0);ra=+(~~(qa<W?W:qa)|0);na=~~(ra>pa?pa:ra)&65535;if((ma&65535)>>>0<na>>>0){sa=~~(Z>aa?aa:Z)&65535;ta=(ia&65535)>>>0<sa>>>0;ia=la|0;ua=la+8|0;va=la+16|0;la=ka&65280;wa=ka>>>24;xa=wa^255;ya=ma&65535;do{if(ta){ma=ya&65535;za=ha;do{Aa=(c[ia>>2]|0)+((ga(e[ua>>1]|0,ma)|0)+(za&65535)<<2)|0;if((a[va]&1)==0){Ba=ka}else{Ca=d[Aa]|d[Aa+1|0]<<8|d[Aa+2|0]<<16|d[Aa+3|0]<<24|0;Ba=((ga(Ca>>>24,xa)|0)>>>8)+wa<<24|(ga(Ca>>>8&255,xa)|0)+la&65280|((ga(Ca&255,xa)|0)>>>8)+ka&255|((ga(Ca>>>16&255,xa)|0)>>>8<<16)+ka&16711680}C=Ba;a[Aa]=C;C=C>>8;a[Aa+1|0]=C;C=C>>8;a[Aa+2|0]=C;C=C>>8;a[Aa+3|0]=C;za=za+1&65535;}while((za&65535)>>>0<sa>>>0)}ya=ya+1&65535;}while((ya&65535)>>>0<na>>>0)}na=ho(c[7689]|0)|0;if(!T){ya=(na|0)==0;sa=N&65535;ka=Q+sa&65535;xa=J-1|0;la=sa+2+Q&65535;sa=Q+(N<<16>>16)|0;wa=sa&65535;va=(N-2&65535)+fa&65535;ua=0;ia=P;while(1){ha=c[H+(ua*28|0)+4>>2]|0;ta=ho(ha|0)|0;if((ta|0)==0){Da=32767;Ea=0}else{R=0;za=32767;ma=0;while(1){Aa=(a[ha+R|0]|0)-32|0;Ca=b[21236+(Aa<<4)>>1]|0;Fa=Ca-(b[21234+(Aa<<4)>>1]|0)&65535;Aa=Ca<<16>>16>ma<<16>>16?Ca:ma;Ca=Fa<<16>>16<za<<16>>16?Fa:za;Fa=R+1|0;if(Fa>>>0<ta>>>0){R=Fa;za=Ca;ma=Aa}else{Da=Ca;Ea=Aa;break}}}ma=Ea-Da&65535;do{if(ya){Ga=38}else{if((kn(ha,c[7689]|0)|0)!=(ha|0)){Ga=38;break}za=c[7654]|0;a[ba]=0;a[M]=-1;a[L]=0;a[F]=-1;Z=+(ia|0);aa=+((ma&65535)>>>1|0);Xd(21232,za,y,wa,~~(Z+ +((c[7672]|0)>>>0>>>0)*.5-aa),ha,na);za=ho(ha|0)|0;R=za>>>0<na>>>0?za:na;if((R|0)==0){Ha=0}else{za=0;ta=0;do{ta=(b[21240+((a[ha+za|0]|0)-32<<4)>>1]|0)+ta&65535;za=za+1|0;}while(za>>>0<R>>>0);Ha=ta&65535}R=c[7654]|0;za=~~(Z+ +((c[7672]|0)>>>0>>>0)*.5-aa);Aa=ha+na|0;c[v>>2]=c[7658];Xd(21232,R,S,Ha+sa&65535,za,Aa,ho(Aa|0)|0);}}while(0);if((Ga|0)==38){Ga=0;Aa=c[7654]|0;za=~~(+(ia|0)+ +((c[7672]|0)>>>0>>>0)*.5- +((ma&65535)>>>1|0));c[w>>2]=c[7658];Xd(21232,Aa,I,ka,za,ha,ho(ha|0)|0);}if(ua>>>0<xa>>>0){za=c[7654]|0;Aa=(c[7672]|0)+ia&65535;c[x>>2]=c[7660];Yd(za,z,la,va,Aa,-1);}Aa=ua+1|0;if(Aa>>>0<J>>>0){ua=Aa;ia=(c[7672]|0)+ia|0}else{break}}}ia=c[7654]|0;ua=c[7657]|0;J=ea<<16>>16;ra=+(J+D|0);pa=+h[ia+24>>3];qa=+h[ia+40>>3];W=+(~~($<pa?pa:$)|0);va=~~(W>qa?qa:W);la=va&65535;W=+(~~(oa<pa?pa:oa)|0);pa=+h[ia+32>>3];Ia=+h[ia+48>>3];Ja=+(~~(ra<pa?pa:ra)|0);xa=~~(Ja>Ia?Ia:Ja);Ja=+(~~(ra+ +(K-J|0))|0);ra=+(~~(Ja<pa?pa:Ja)|0);J=~~(ra>Ia?Ia:ra)&65535;if((xa&65535)>>>0<J>>>0){ka=~~(W>qa?qa:W)&65535;sa=(va&65535)>>>0<ka>>>0;va=ia|0;na=ia+8|0;wa=ia+16|0;ia=ua&65280;ya=ua>>>24;H=ya^255;Q=xa&65535;do{if(sa){xa=Q&65535;T=la;do{Aa=(c[va>>2]|0)+((ga(e[na>>1]|0,xa)|0)+(T&65535)<<2)|0;if((a[wa]&1)==0){Ka=ua}else{za=d[Aa]|d[Aa+1|0]<<8|d[Aa+2|0]<<16|d[Aa+3|0]<<24|0;Ka=((ga(za>>>24,H)|0)>>>8)+ya<<24|(ga(za>>>8&255,H)|0)+ia&65280|((ga(za&255,H)|0)>>>8)+ua&255|((ga(za>>>16&255,H)|0)>>>8<<16)+ua&16711680}C=Ka;a[Aa]=C;C=C>>8;a[Aa+1|0]=C;C=C>>8;a[Aa+2|0]=C;C=C>>8;a[Aa+3|0]=C;T=T+1&65535;}while((T&65535)>>>0<ka>>>0)}Q=Q+1&65535;}while((Q&65535)>>>0<J>>>0)}J=O+1|0;if(J>>>0<(c[7688]|0)>>>0){N=fa+N&65535;O=J}else{ca=P;break}}}P=n;O=o;N=p;Ka=q;D=r;z=s;x=t;I=u;w=c[7671]&65535;Ga=~~+g[7670];Ha=c[7654]|0;c[j>>2]=c[7657];S=B<<16>>16;v=G<<16>>16;G=S+v|0;y=w<<16>>16;F=G-y&65535;Ud(Ha,j,F,E,w,1);j=c[7654]|0;c[k>>2]=c[7657];Ha=S-y|0;S=Ha-1|0;L=S&65535;Ud(j,k,L,E,w,2);E=c[7654]|0;c[l>>2]=c[7657];k=y<<1;j=K+65535+ca-k&65535;Ud(E,l,L,j,w,4);L=c[7654]|0;c[m>>2]=c[7657];Ud(L,m,F,j,w,8);w=c[7654]|0;j=c[7657]|0;F=ca+1+y|0;m=y+1|0;Y=+h[w+24>>3];_=+h[w+40>>3];oa=+(S|0);$=+(~~(oa<Y?Y:oa)|0);S=~~($>_?_:$);L=S&65535;$=+(~~+(B<<16>>16|0)|0);oa=+(~~($<Y?Y:$)|0);$=+h[w+32>>3];W=+h[w+48>>3];qa=+(F|0);ra=+(~~(qa<$?$:qa)|0);B=~~(ra>W?W:ra);ra=+(F+K-(m<<1)|0);Ia=+(~~(ra<$?$:ra)|0);F=~~(Ia>W?W:Ia)&65535;if((B&65535)>>>0<F>>>0){l=~~(oa>_?_:oa)&65535;E=(S&65535)>>>0<l>>>0;S=w|0;M=w+8|0;ba=w+16|0;Da=j&65280;Ea=j>>>24;Ba=Ea^255;da=B&65535;do{if(E){B=da&65535;ja=L;do{J=(c[S>>2]|0)+((ga(e[M>>1]|0,B)|0)+(ja&65535)<<2)|0;if((a[ba]&1)==0){La=j}else{Q=d[J]|d[J+1|0]<<8|d[J+2|0]<<16|d[J+3|0]<<24|0;La=((ga(Q>>>24,Ba)|0)>>>8)+Ea<<24|(ga(Q>>>8&255,Ba)|0)+Da&65280|((ga(Q&255,Ba)|0)>>>8)+j&255|((ga(Q>>>16&255,Ba)|0)>>>8<<16)+j&16711680}C=La;a[J]=C;C=C>>8;a[J+1|0]=C;C=C>>8;a[J+2|0]=C;C=C>>8;a[J+3|0]=C;ja=ja+1&65535;}while((ja&65535)>>>0<l>>>0)}da=da+1&65535;}while((da&65535)>>>0<F>>>0);F=c[7654]|0;Ma=F;Na=c[7657]|0;Oa=+h[F+24>>3];Pa=+h[F+40>>3];Qa=+h[F+32>>3];Ra=+h[F+48>>3]}else{Ma=w;Na=j;Oa=Y;Pa=_;Qa=$;Ra=W}W=+(G|0);$=+(~~(W<Oa?Oa:W)|0);j=~~($>Pa?Pa:$);w=j&65535;$=+(m+G|0);W=+(~~($<Oa?Oa:$)|0);$=+(~~(qa<Qa?Qa:qa)|0);G=~~($>Ra?Ra:$);$=+(~~(ra<Qa?Qa:ra)|0);m=~~($>Ra?Ra:$)&65535;if((G&65535)>>>0<m>>>0){F=~~(W>Pa?Pa:W)&65535;da=(j&65535)>>>0<F>>>0;j=Ma|0;l=Ma+8|0;La=Ma+16|0;Ba=Na&65280;Da=Na>>>24;Ea=Da^255;ba=G&65535;do{if(da){G=ba&65535;M=w;do{S=(c[j>>2]|0)+((ga(e[l>>1]|0,G)|0)+(M&65535)<<2)|0;if((a[La]&1)==0){Sa=Na}else{L=d[S]|d[S+1|0]<<8|d[S+2|0]<<16|d[S+3|0]<<24|0;Sa=((ga(L>>>24,Ea)|0)>>>8)+Da<<24|(ga(L>>>8&255,Ea)|0)+Ba&65280|((ga(L&255,Ea)|0)>>>8)+Na&255|((ga(L>>>16&255,Ea)|0)>>>8<<16)+Na&16711680}C=Sa;a[S]=C;C=C>>8;a[S+1|0]=C;C=C>>8;a[S+2|0]=C;C=C>>8;a[S+3|0]=C;M=M+1&65535;}while((M&65535)>>>0<F>>>0)}ba=ba+1&65535;}while((ba&65535)>>>0<m>>>0);Ta=c[7654]|0}else{Ta=Ma}Ma=Ga<<16>>16;m=(Ma|0)/2|0;ba=Ha-m-1|0;Ha=ca-m|0;ca=m+y&65535;c[n>>2]=c[7659];n=k+v+ba|0;v=ca<<16>>16;k=v<<1;y=Ma+1+n|0;W=+(y-k|0);Pa=+(Ha|0);$=+(ca<<16>>16|0);Ra=+(Ga<<16>>16|0);Vd(Ta,P,W,Pa,$,Ra,1);P=c[7654]|0;c[o>>2]=c[7659];ra=+(ba|0);Vd(P,O,ra,Pa,$,Ra,2);O=c[7654]|0;c[p>>2]=c[7659];p=Ma+K+Ha|0;Qa=+(p-(k|1)|0);Vd(O,N,ra,Qa,$,Ra,4);N=c[7654]|0;c[q>>2]=c[7659];Vd(N,Ka,W,Qa,$,Ra,8);Ka=c[7654]|0;c[r>>2]=c[7659];$=+(v+ba|0);Qa=+(Ma-v+n|0);Wd(Ka,D,$,Pa,Qa,Pa,Ra);D=c[7654]|0;c[s>>2]=c[7659];Pa=+(p-1|0);Wd(D,z,$,Pa,Qa,Pa,Ra);z=c[7654]|0;c[t>>2]=c[7659];Pa=+(v+Ha|0);Qa=+(p-2-v|0);Wd(z,x,ra,Pa,ra,Qa,Ra);x=c[7654]|0;c[u>>2]=c[7659];ra=+(y|0);Wd(x,I,ra,Pa,ra,Qa,Ra);I=c[7654]|0;Ra=+h[3832];Qa=+h[3833];ra=+h[3834];Pa=+Ib(+U,+(+h[3831]));$=+Ib(+V,+Ra);Ra=+tb(+A,+Qa);Qa=+tb(+X,+ra);ra=+Ib(+Pa,+Ra);W=+Ib(+$,+Qa);qa=+tb(+Pa,+Ra);Ra=+tb(+$,+Qa);x=I+68|0;y=c[x>>2]|0;u=I+64|0;if((y|0)==(c[u>>2]|0)){c[u>>2]=y<<1;u=I+60|0;c[u>>2]=On(c[u>>2]|0,y<<6)|0}if((c[I+72>>2]|0)==0){Ua=0.0;Va=0.0}else{Ua=+(b[I+58>>1]|0);Va=+(b[I+56>>1]|0)}y=c[x>>2]|0;c[x>>2]=y+1;x=c[I+60>>2]|0;Qa=Va+ +((e[I+4>>1]|0)>>>0);$=Ua+ +((e[I+6>>1]|0)>>>0);if(qa<Va|Qa<ra|Ra<Ua|$<W){Wa=0.0;Xa=0.0;Ya=0.0;Za=0.0}else{Wa=ra>Va?ra:Va;Xa=W>Ua?W:Ua;Ya=qa<Qa?qa:Qa;Za=Ra<$?Ra:$}h[x+(y<<5)>>3]=Wa;h[x+(y<<5)+8>>3]=Xa;h[x+(y<<5)+16>>3]=Ya;h[x+(y<<5)+24>>3]=Za;h[3831]=U;h[3832]=V;h[3833]=A;h[3834]=X;i=f;return}function Ud(b,f,g,j,k,l){b=b|0;f=f|0;g=g|0;j=j|0;k=k|0;l=l|0;var m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0.0,z=0.0,A=0,B=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,V=0,X=0,Y=0,Z=0,_=0,$=0,aa=0,ba=0.0;m=i;n=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[n>>2];n=f;f=d[n]|d[n+1|0]<<8|d[n+2|0]<<16|d[n+3|0]<<24|0;n=k&65535;if(k<<16>>16==0){i=m;return}o=f>>>24;p=k+g&65535;q=k+j&65535;r=(((f&255)*255|0)>>>0)/(o>>>0)|0;s=(((f>>>8&255)*255|0)>>>0)/(o>>>0)|0;t=(((f>>>16&255)*255|0)>>>0)/(o>>>0)|0;u=l&255;l=(u&1|0)!=0;v=u&8;w=(v|0)==0;do{if(!(w&(l^1))){x=p+k&65535;y=+(x<<16>>16|0);z=+(q<<16>>16|0);if(!(+h[b+48>>3]>z&((+h[b+40>>3]<=y|+h[b+24>>3]>y|+h[b+32>>3]>z)^1))){break}A=(c[b>>2]|0)+((ga(e[b+8>>1]|0,q&65535)|0)+(x&65535)<<2)|0;if((a[b+16|0]&1)==0){C=f;a[A]=C;C=C>>8;a[A+1|0]=C;C=C>>8;a[A+2|0]=C;C=C>>8;a[A+3|0]=C;break}else{x=d[A]|d[A+1|0]<<8|d[A+2|0]<<16|d[A+3|0]<<24|0;B=o^255;C=((ga(x>>>24,B)|0)>>>8)+o<<24|(ga(x>>>8&255,B)|0)+(f&65280)&65280|((ga(x&255,B)|0)>>>8)+f&255|((ga(x>>>16&255,B)|0)>>>8<<16)+f&16711680;a[A]=C;C=C>>8;a[A+1|0]=C;C=C>>8;a[A+2|0]=C;C=C>>8;a[A+3|0]=C;break}}}while(0);A=(u&2|0)!=0;B=u&4;do{if(!((B|0)==0&(A^1))){z=+(g<<16>>16|0);y=+(q<<16>>16|0);if(!(+h[b+48>>3]>y&((+h[b+40>>3]<=z|+h[b+24>>3]>z|+h[b+32>>3]>y)^1))){break}x=(c[b>>2]|0)+((ga(e[b+8>>1]|0,q&65535)|0)+(g&65535)<<2)|0;if((a[b+16|0]&1)==0){C=f;a[x]=C;C=C>>8;a[x+1|0]=C;C=C>>8;a[x+2|0]=C;C=C>>8;a[x+3|0]=C;break}else{D=d[x]|d[x+1|0]<<8|d[x+2|0]<<16|d[x+3|0]<<24|0;E=o^255;C=((ga(D>>>24,E)|0)>>>8)+o<<24|(ga(D>>>8&255,E)|0)+(f&65280)&65280|((ga(D&255,E)|0)>>>8)+f&255|((ga(D>>>16&255,E)|0)>>>8<<16)+f&16711680;a[x]=C;C=C>>8;a[x+1|0]=C;C=C>>8;a[x+2|0]=C;C=C>>8;a[x+3|0]=C;break}}}while(0);g=(B|0)!=0;do{if(!(w&(g^1))){B=q+k&65535;y=+(p<<16>>16|0);z=+(B<<16>>16|0);if(!(+h[b+48>>3]>z&((+h[b+40>>3]<=y|+h[b+24>>3]>y|+h[b+32>>3]>z)^1))){break}x=(c[b>>2]|0)+((ga(e[b+8>>1]|0,B&65535)|0)+(p&65535)<<2)|0;if((a[b+16|0]&1)==0){C=f;a[x]=C;C=C>>8;a[x+1|0]=C;C=C>>8;a[x+2|0]=C;C=C>>8;a[x+3|0]=C;break}else{B=d[x]|d[x+1|0]<<8|d[x+2|0]<<16|d[x+3|0]<<24|0;E=o^255;C=((ga(B>>>24,E)|0)>>>8)+o<<24|(ga(B>>>8&255,E)|0)+(f&65280)&65280|((ga(B&255,E)|0)>>>8)+f&255|((ga(B>>>16&255,E)|0)>>>8<<16)+f&16711680;a[x]=C;C=C>>8;a[x+1|0]=C;C=C>>8;a[x+2|0]=C;C=C>>8;a[x+3|0]=C;break}}}while(0);w=(u&3|0)==0;do{if(w){F=b+24|0;G=b+32|0;H=b+40|0;I=b+48|0}else{z=+(p<<16>>16|0);y=+(j<<16>>16|0);x=b+24|0;E=b+32|0;B=b+40|0;D=b+48|0;if(!(+h[D>>3]>y&((+h[B>>3]<=z|+h[x>>3]>z|+h[E>>3]>y)^1))){F=x;G=E;H=B;I=D;break}J=(c[b>>2]|0)+((ga(e[b+8>>1]|0,j&65535)|0)+(p&65535)<<2)|0;if((a[b+16|0]&1)==0){C=f;a[J]=C;C=C>>8;a[J+1|0]=C;C=C>>8;a[J+2|0]=C;C=C>>8;a[J+3|0]=C;F=x;G=E;H=B;I=D;break}else{K=d[J]|d[J+1|0]<<8|d[J+2|0]<<16|d[J+3|0]<<24|0;L=o^255;C=((ga(K>>>24,L)|0)>>>8)+o<<24|(ga(K>>>8&255,L)|0)+(f&65280)&65280|((ga(K&255,L)|0)>>>8)+f&255|((ga(K>>>16&255,L)|0)>>>8<<16)+f&16711680;a[J]=C;C=C>>8;a[J+1|0]=C;C=C>>8;a[J+2|0]=C;C=C>>8;a[J+3|0]=C;F=x;G=E;H=B;I=D;break}}}while(0);f=(Pb(+(+((k&65535)>>>0)/1.4142135623730951))|0)&65535;j=(v|0)!=0;v=t&255;t=s&255;s=r&255;r=b|0;D=b+8|0;B=b+16|0;b=((ga(s,o)|0)>>>0)/255|0;E=(((ga(t,o)|0)>>>0)/255|0)<<8;x=b&255|o<<24|E&65280|(((ga(v,o)|0)>>>0)/255|0)<<16&16711680;J=o^255;L=0;K=0;M=k;while(1){N=K+1&65535;O=N&65535;y=+W(+(ga(O+n|0,n-O|0)|0));O=(Pb(+(+U((+fa(y)-y)*255.0+.5)))|0)&255;P=((L>>>0>O>>>0)<<31>>31)+M&65535;if(P<<16>>16==N<<16>>16){Q=N<<16>>16!=f<<16>>16|0}else{Q=0}if((Q|(P&65535)>>>0<(N&65535)>>>0|0)!=0){R=0;S=k;T=0;break}V=(ga(O^255,o)|0)>>>8;do{if(j){X=P+p&65535;Y=N+q&65535;y=+(X<<16>>16|0);z=+(Y<<16>>16|0);if(!(+h[I>>3]>z&((+h[H>>3]<=y|+h[F>>3]>y|+h[G>>3]>z)^1))){break}Z=((ga(V,s)|0)>>>0)/255|0;_=(((ga(V,t)|0)>>>0)/255|0)<<8;$=Z&255|V<<24|_&65280|(((ga(V,v)|0)>>>0)/255|0)<<16&16711680;aa=(c[r>>2]|0)+((ga(e[D>>1]|0,Y&65535)|0)+(X&65535)<<2)|0;if((a[B]&1)==0){C=$;a[aa]=C;C=C>>8;a[aa+1|0]=C;C=C>>8;a[aa+2|0]=C;C=C>>8;a[aa+3|0]=C;break}else{X=d[aa]|d[aa+1|0]<<8|d[aa+2|0]<<16|d[aa+3|0]<<24|0;Y=V^255;C=((ga(X>>>24,Y)|0)>>>8)+V<<24|(ga(X>>>8&255,Y)|0)+_&65280|((ga(X&255,Y)|0)>>>8)+Z&255|((ga(X>>>16&255,Y)|0)>>>8<<16)+$&16711680;a[aa]=C;C=C>>8;a[aa+1|0]=C;C=C>>8;a[aa+2|0]=C;C=C>>8;a[aa+3|0]=C;break}}}while(0);do{if(l){aa=P+p&65535;$=q-N&65535;z=+(aa<<16>>16|0);y=+($<<16>>16|0);if(!(+h[I>>3]>y&((+h[H>>3]<=z|+h[F>>3]>z|+h[G>>3]>y)^1))){break}Y=((ga(V,s)|0)>>>0)/255|0;X=(((ga(V,t)|0)>>>0)/255|0)<<8;Z=Y&255|V<<24|X&65280|(((ga(V,v)|0)>>>0)/255|0)<<16&16711680;_=(c[r>>2]|0)+((ga(e[D>>1]|0,$&65535)|0)+(aa&65535)<<2)|0;if((a[B]&1)==0){C=Z;a[_]=C;C=C>>8;a[_+1|0]=C;C=C>>8;a[_+2|0]=C;C=C>>8;a[_+3|0]=C;break}else{aa=d[_]|d[_+1|0]<<8|d[_+2|0]<<16|d[_+3|0]<<24|0;$=V^255;C=((ga(aa>>>24,$)|0)>>>8)+V<<24|(ga(aa>>>8&255,$)|0)+X&65280|((ga(aa&255,$)|0)>>>8)+Y&255|((ga(aa>>>16&255,$)|0)>>>8<<16)+Z&16711680;a[_]=C;C=C>>8;a[_+1|0]=C;C=C>>8;a[_+2|0]=C;C=C>>8;a[_+3|0]=C;break}}}while(0);do{if(g){_=p-P&65535;Z=N+q&65535;y=+(_<<16>>16|0);z=+(Z<<16>>16|0);if(!(+h[I>>3]>z&((+h[H>>3]<=y|+h[F>>3]>y|+h[G>>3]>z)^1))){break}$=((ga(V,s)|0)>>>0)/255|0;aa=(((ga(V,t)|0)>>>0)/255|0)<<8;Y=$&255|V<<24|aa&65280|(((ga(V,v)|0)>>>0)/255|0)<<16&16711680;X=(c[r>>2]|0)+((ga(e[D>>1]|0,Z&65535)|0)+(_&65535)<<2)|0;if((a[B]&1)==0){C=Y;a[X]=C;C=C>>8;a[X+1|0]=C;C=C>>8;a[X+2|0]=C;C=C>>8;a[X+3|0]=C;break}else{_=d[X]|d[X+1|0]<<8|d[X+2|0]<<16|d[X+3|0]<<24|0;Z=V^255;C=((ga(_>>>24,Z)|0)>>>8)+V<<24|(ga(_>>>8&255,Z)|0)+aa&65280|((ga(_&255,Z)|0)>>>8)+$&255|((ga(_>>>16&255,Z)|0)>>>8<<16)+Y&16711680;a[X]=C;C=C>>8;a[X+1|0]=C;C=C>>8;a[X+2|0]=C;C=C>>8;a[X+3|0]=C;break}}}while(0);do{if(A){X=p-P&65535;Y=q-N&65535;z=+(X<<16>>16|0);y=+(Y<<16>>16|0);if(!(+h[I>>3]>y&((+h[H>>3]<=z|+h[F>>3]>z|+h[G>>3]>y)^1))){break}Z=((ga(V,s)|0)>>>0)/255|0;_=(((ga(V,t)|0)>>>0)/255|0)<<8;$=Z&255|V<<24|_&65280|(((ga(V,v)|0)>>>0)/255|0)<<16&16711680;aa=(c[r>>2]|0)+((ga(e[D>>1]|0,Y&65535)|0)+(X&65535)<<2)|0;if((a[B]&1)==0){C=$;a[aa]=C;C=C>>8;a[aa+1|0]=C;C=C>>8;a[aa+2|0]=C;C=C>>8;a[aa+3|0]=C;break}else{X=d[aa]|d[aa+1|0]<<8|d[aa+2|0]<<16|d[aa+3|0]<<24|0;Y=V^255;C=((ga(X>>>24,Y)|0)>>>8)+V<<24|(ga(X>>>8&255,Y)|0)+_&65280|((ga(X&255,Y)|0)>>>8)+Z&255|((ga(X>>>16&255,Y)|0)>>>8<<16)+$&16711680;a[aa]=C;C=C>>8;a[aa+1|0]=C;C=C>>8;a[aa+2|0]=C;C=C>>8;a[aa+3|0]=C;break}}}while(0);V=P-1&65535;if(V<<16>>16>0){aa=N+q&65535;y=+(aa<<16>>16|0);$=aa&65535;aa=q-N&65535;z=+(aa<<16>>16|0);Y=aa&65535;aa=V;do{do{if(j){V=aa+p&65535;ba=+(V<<16>>16|0);if(!(+h[I>>3]>y&((+h[H>>3]<=ba|+h[F>>3]>ba|+h[G>>3]>y)^1))){break}X=(c[r>>2]|0)+((ga(e[D>>1]|0,$)|0)+(V&65535)<<2)|0;if((a[B]&1)==0){C=x;a[X]=C;C=C>>8;a[X+1|0]=C;C=C>>8;a[X+2|0]=C;C=C>>8;a[X+3|0]=C;break}else{V=d[X]|d[X+1|0]<<8|d[X+2|0]<<16|d[X+3|0]<<24|0;C=((ga(V>>>24,J)|0)>>>8)+o<<24|(ga(V>>>8&255,J)|0)+E&65280|((ga(V&255,J)|0)>>>8)+b&255|((ga(V>>>16&255,J)|0)>>>8<<16)+x&16711680;a[X]=C;C=C>>8;a[X+1|0]=C;C=C>>8;a[X+2|0]=C;C=C>>8;a[X+3|0]=C;break}}}while(0);do{if(l){X=aa+p&65535;ba=+(X<<16>>16|0);if(!(+h[I>>3]>z&((+h[H>>3]<=ba|+h[F>>3]>ba|+h[G>>3]>z)^1))){break}V=(c[r>>2]|0)+((ga(e[D>>1]|0,Y)|0)+(X&65535)<<2)|0;if((a[B]&1)==0){C=x;a[V]=C;C=C>>8;a[V+1|0]=C;C=C>>8;a[V+2|0]=C;C=C>>8;a[V+3|0]=C;break}else{X=d[V]|d[V+1|0]<<8|d[V+2|0]<<16|d[V+3|0]<<24|0;C=((ga(X>>>24,J)|0)>>>8)+o<<24|(ga(X>>>8&255,J)|0)+E&65280|((ga(X&255,J)|0)>>>8)+b&255|((ga(X>>>16&255,J)|0)>>>8<<16)+x&16711680;a[V]=C;C=C>>8;a[V+1|0]=C;C=C>>8;a[V+2|0]=C;C=C>>8;a[V+3|0]=C;break}}}while(0);do{if(g){V=p-aa&65535;ba=+(V<<16>>16|0);if(!(+h[I>>3]>y&((+h[H>>3]<=ba|+h[F>>3]>ba|+h[G>>3]>y)^1))){break}X=(c[r>>2]|0)+((ga(e[D>>1]|0,$)|0)+(V&65535)<<2)|0;if((a[B]&1)==0){C=x;a[X]=C;C=C>>8;a[X+1|0]=C;C=C>>8;a[X+2|0]=C;C=C>>8;a[X+3|0]=C;break}else{V=d[X]|d[X+1|0]<<8|d[X+2|0]<<16|d[X+3|0]<<24|0;C=((ga(V>>>24,J)|0)>>>8)+o<<24|(ga(V>>>8&255,J)|0)+E&65280|((ga(V&255,J)|0)>>>8)+b&255|((ga(V>>>16&255,J)|0)>>>8<<16)+x&16711680;a[X]=C;C=C>>8;a[X+1|0]=C;C=C>>8;a[X+2|0]=C;C=C>>8;a[X+3|0]=C;break}}}while(0);do{if(A){X=p-aa&65535;ba=+(X<<16>>16|0);if(!(+h[I>>3]>z&((+h[H>>3]<=ba|+h[F>>3]>ba|+h[G>>3]>z)^1))){break}V=(c[r>>2]|0)+((ga(e[D>>1]|0,Y)|0)+(X&65535)<<2)|0;if((a[B]&1)==0){C=x;a[V]=C;C=C>>8;a[V+1|0]=C;C=C>>8;a[V+2|0]=C;C=C>>8;a[V+3|0]=C;break}else{X=d[V]|d[V+1|0]<<8|d[V+2|0]<<16|d[V+3|0]<<24|0;C=((ga(X>>>24,J)|0)>>>8)+o<<24|(ga(X>>>8&255,J)|0)+E&65280|((ga(X&255,J)|0)>>>8)+b&255|((ga(X>>>16&255,J)|0)>>>8<<16)+x&16711680;a[V]=C;C=C>>8;a[V+1|0]=C;C=C>>8;a[V+2|0]=C;C=C>>8;a[V+3|0]=C;break}}}while(0);aa=aa-1&65535;}while(aa<<16>>16>0)}if((P&65535)>>>0>(N&65535)>>>0){L=O;K=N;M=P}else{R=0;S=k;T=0;break}}L100:while(1){M=T+1&65535;K=M&65535;z=+W(+(ga(K+n|0,n-K|0)|0));K=(Pb(+(+U((+fa(z)-z)*255.0+.5)))|0)&255;L=R>>>0>K>>>0;Q=(L<<31>>31)+S&65535;if((Q&65535)>>>0<=(M&65535)>>>0){break}f=(ga(K^255,o)|0)>>>8;do{if(j){aa=M+p&65535;Y=Q+q&65535;z=+(aa<<16>>16|0);y=+(Y<<16>>16|0);if(!(+h[I>>3]>y&((+h[H>>3]<=z|+h[F>>3]>z|+h[G>>3]>y)^1))){break}$=((ga(f,s)|0)>>>0)/255|0;V=(((ga(f,t)|0)>>>0)/255|0)<<8;X=$&255|f<<24|V&65280|(((ga(f,v)|0)>>>0)/255|0)<<16&16711680;Z=(c[r>>2]|0)+((ga(e[D>>1]|0,Y&65535)|0)+(aa&65535)<<2)|0;if((a[B]&1)==0){C=X;a[Z]=C;C=C>>8;a[Z+1|0]=C;C=C>>8;a[Z+2|0]=C;C=C>>8;a[Z+3|0]=C;break}else{aa=d[Z]|d[Z+1|0]<<8|d[Z+2|0]<<16|d[Z+3|0]<<24|0;Y=f^255;C=((ga(aa>>>24,Y)|0)>>>8)+f<<24|(ga(aa>>>8&255,Y)|0)+V&65280|((ga(aa&255,Y)|0)>>>8)+$&255|((ga(aa>>>16&255,Y)|0)>>>8<<16)+X&16711680;a[Z]=C;C=C>>8;a[Z+1|0]=C;C=C>>8;a[Z+2|0]=C;C=C>>8;a[Z+3|0]=C;break}}}while(0);do{if(l){P=M+p&65535;N=q-Q&65535;y=+(P<<16>>16|0);z=+(N<<16>>16|0);if(!(+h[I>>3]>z&((+h[H>>3]<=y|+h[F>>3]>y|+h[G>>3]>z)^1))){break}O=((ga(f,s)|0)>>>0)/255|0;Z=(((ga(f,t)|0)>>>0)/255|0)<<8;X=O&255|f<<24|Z&65280|(((ga(f,v)|0)>>>0)/255|0)<<16&16711680;Y=(c[r>>2]|0)+((ga(e[D>>1]|0,N&65535)|0)+(P&65535)<<2)|0;if((a[B]&1)==0){C=X;a[Y]=C;C=C>>8;a[Y+1|0]=C;C=C>>8;a[Y+2|0]=C;C=C>>8;a[Y+3|0]=C;break}else{P=d[Y]|d[Y+1|0]<<8|d[Y+2|0]<<16|d[Y+3|0]<<24|0;N=f^255;C=((ga(P>>>24,N)|0)>>>8)+f<<24|(ga(P>>>8&255,N)|0)+Z&65280|((ga(P&255,N)|0)>>>8)+O&255|((ga(P>>>16&255,N)|0)>>>8<<16)+X&16711680;a[Y]=C;C=C>>8;a[Y+1|0]=C;C=C>>8;a[Y+2|0]=C;C=C>>8;a[Y+3|0]=C;break}}}while(0);do{if(g){Y=p-M&65535;X=Q+q&65535;z=+(Y<<16>>16|0);y=+(X<<16>>16|0);if(!(+h[I>>3]>y&((+h[H>>3]<=z|+h[F>>3]>z|+h[G>>3]>y)^1))){break}N=((ga(f,s)|0)>>>0)/255|0;P=(((ga(f,t)|0)>>>0)/255|0)<<8;O=N&255|f<<24|P&65280|(((ga(f,v)|0)>>>0)/255|0)<<16&16711680;Z=(c[r>>2]|0)+((ga(e[D>>1]|0,X&65535)|0)+(Y&65535)<<2)|0;if((a[B]&1)==0){C=O;a[Z]=C;C=C>>8;a[Z+1|0]=C;C=C>>8;a[Z+2|0]=C;C=C>>8;a[Z+3|0]=C;break}else{Y=d[Z]|d[Z+1|0]<<8|d[Z+2|0]<<16|d[Z+3|0]<<24|0;X=f^255;C=((ga(Y>>>24,X)|0)>>>8)+f<<24|(ga(Y>>>8&255,X)|0)+P&65280|((ga(Y&255,X)|0)>>>8)+N&255|((ga(Y>>>16&255,X)|0)>>>8<<16)+O&16711680;a[Z]=C;C=C>>8;a[Z+1|0]=C;C=C>>8;a[Z+2|0]=C;C=C>>8;a[Z+3|0]=C;break}}}while(0);do{if(A){Z=p-M&65535;O=q-Q&65535;y=+(Z<<16>>16|0);z=+(O<<16>>16|0);if(!(+h[I>>3]>z&((+h[H>>3]<=y|+h[F>>3]>y|+h[G>>3]>z)^1))){break}X=((ga(f,s)|0)>>>0)/255|0;Y=(((ga(f,t)|0)>>>0)/255|0)<<8;N=X&255|f<<24|Y&65280|(((ga(f,v)|0)>>>0)/255|0)<<16&16711680;P=(c[r>>2]|0)+((ga(e[D>>1]|0,O&65535)|0)+(Z&65535)<<2)|0;if((a[B]&1)==0){C=N;a[P]=C;C=C>>8;a[P+1|0]=C;C=C>>8;a[P+2|0]=C;C=C>>8;a[P+3|0]=C;break}else{Z=d[P]|d[P+1|0]<<8|d[P+2|0]<<16|d[P+3|0]<<24|0;O=f^255;C=((ga(Z>>>24,O)|0)>>>8)+f<<24|(ga(Z>>>8&255,O)|0)+Y&65280|((ga(Z&255,O)|0)>>>8)+X&255|((ga(Z>>>16&255,O)|0)>>>8<<16)+N&16711680;a[P]=C;C=C>>8;a[P+1|0]=C;C=C>>8;a[P+2|0]=C;C=C>>8;a[P+3|0]=C;break}}}while(0);if(!(L&T<<16>>16>0)){R=K;S=Q;T=M;continue}f=Q+q&65535;z=+(f<<16>>16|0);P=f&65535;f=q-Q&65535;y=+(f<<16>>16|0);N=f&65535;f=T;while(1){do{if(j){O=f+p&65535;ba=+(O<<16>>16|0);if(!(+h[I>>3]>z&((+h[H>>3]<=ba|+h[F>>3]>ba|+h[G>>3]>z)^1))){break}Z=(c[r>>2]|0)+((ga(e[D>>1]|0,P)|0)+(O&65535)<<2)|0;if((a[B]&1)==0){C=x;a[Z]=C;C=C>>8;a[Z+1|0]=C;C=C>>8;a[Z+2|0]=C;C=C>>8;a[Z+3|0]=C;break}else{O=d[Z]|d[Z+1|0]<<8|d[Z+2|0]<<16|d[Z+3|0]<<24|0;C=((ga(O>>>24,J)|0)>>>8)+o<<24|(ga(O>>>8&255,J)|0)+E&65280|((ga(O&255,J)|0)>>>8)+b&255|((ga(O>>>16&255,J)|0)>>>8<<16)+x&16711680;a[Z]=C;C=C>>8;a[Z+1|0]=C;C=C>>8;a[Z+2|0]=C;C=C>>8;a[Z+3|0]=C;break}}}while(0);do{if(l){Z=f+p&65535;ba=+(Z<<16>>16|0);if(!(+h[I>>3]>y&((+h[H>>3]<=ba|+h[F>>3]>ba|+h[G>>3]>y)^1))){break}O=(c[r>>2]|0)+((ga(e[D>>1]|0,N)|0)+(Z&65535)<<2)|0;if((a[B]&1)==0){C=x;a[O]=C;C=C>>8;a[O+1|0]=C;C=C>>8;a[O+2|0]=C;C=C>>8;a[O+3|0]=C;break}else{Z=d[O]|d[O+1|0]<<8|d[O+2|0]<<16|d[O+3|0]<<24|0;C=((ga(Z>>>24,J)|0)>>>8)+o<<24|(ga(Z>>>8&255,J)|0)+E&65280|((ga(Z&255,J)|0)>>>8)+b&255|((ga(Z>>>16&255,J)|0)>>>8<<16)+x&16711680;a[O]=C;C=C>>8;a[O+1|0]=C;C=C>>8;a[O+2|0]=C;C=C>>8;a[O+3|0]=C;break}}}while(0);do{if(g){O=p-f&65535;ba=+(O<<16>>16|0);if(!(+h[I>>3]>z&((+h[H>>3]<=ba|+h[F>>3]>ba|+h[G>>3]>z)^1))){break}Z=(c[r>>2]|0)+((ga(e[D>>1]|0,P)|0)+(O&65535)<<2)|0;if((a[B]&1)==0){C=x;a[Z]=C;C=C>>8;a[Z+1|0]=C;C=C>>8;a[Z+2|0]=C;C=C>>8;a[Z+3|0]=C;break}else{O=d[Z]|d[Z+1|0]<<8|d[Z+2|0]<<16|d[Z+3|0]<<24|0;C=((ga(O>>>24,J)|0)>>>8)+o<<24|(ga(O>>>8&255,J)|0)+E&65280|((ga(O&255,J)|0)>>>8)+b&255|((ga(O>>>16&255,J)|0)>>>8<<16)+x&16711680;a[Z]=C;C=C>>8;a[Z+1|0]=C;C=C>>8;a[Z+2|0]=C;C=C>>8;a[Z+3|0]=C;break}}}while(0);do{if(A){Z=p-f&65535;ba=+(Z<<16>>16|0);if(!(+h[I>>3]>y&((+h[H>>3]<=ba|+h[F>>3]>ba|+h[G>>3]>y)^1))){break}O=(c[r>>2]|0)+((ga(e[D>>1]|0,N)|0)+(Z&65535)<<2)|0;if((a[B]&1)==0){C=x;a[O]=C;C=C>>8;a[O+1|0]=C;C=C>>8;a[O+2|0]=C;C=C>>8;a[O+3|0]=C;break}else{Z=d[O]|d[O+1|0]<<8|d[O+2|0]<<16|d[O+3|0]<<24|0;C=((ga(Z>>>24,J)|0)>>>8)+o<<24|(ga(Z>>>8&255,J)|0)+E&65280|((ga(Z&255,J)|0)>>>8)+b&255|((ga(Z>>>16&255,J)|0)>>>8<<16)+x&16711680;a[O]=C;C=C>>8;a[O+1|0]=C;C=C>>8;a[O+2|0]=C;C=C>>8;a[O+3|0]=C;break}}}while(0);O=f-1&65535;if(O<<16>>16>0){f=O}else{R=K;S=Q;T=M;continue L100}}}y=+(p<<16>>16|0);z=+(q<<16>>16|0);do{if(+h[I>>3]>z&((+h[H>>3]<=y|+h[F>>3]>y|+h[G>>3]>z)^1)){T=(c[r>>2]|0)+((ga(e[D>>1]|0,q&65535)|0)+(p&65535)<<2)|0;if((a[B]&1)==0){C=x;a[T]=C;C=C>>8;a[T+1|0]=C;C=C>>8;a[T+2|0]=C;C=C>>8;a[T+3|0]=C;break}else{S=d[T]|d[T+1|0]<<8|d[T+2|0]<<16|d[T+3|0]<<24|0;C=((ga(S>>>24,J)|0)>>>8)+o<<24|(ga(S>>>8&255,J)|0)+E&65280|((ga(S&255,J)|0)>>>8)+b&255|((ga(S>>>16&255,J)|0)>>>8<<16)+x&16711680;a[T]=C;C=C>>8;a[T+1|0]=C;C=C>>8;a[T+2|0]=C;C=C>>8;a[T+3|0]=C;break}}}while(0);if((k&65535)>>>0<=1>>>0){i=m;return}k=(u&9|0)==0;T=(u&6|0)==0;S=(u&12|0)==0;u=q&65535;q=p&65535;p=1;do{do{if(!k){R=p+q|0;ba=+((R&65535)<<16>>16|0);if(!(+h[I>>3]>z&((+h[H>>3]<=ba|+h[F>>3]>ba|+h[G>>3]>z)^1))){break}A=(c[r>>2]|0)+((ga(e[D>>1]|0,u)|0)+(R&65535)<<2)|0;if((a[B]&1)==0){C=x;a[A]=C;C=C>>8;a[A+1|0]=C;C=C>>8;a[A+2|0]=C;C=C>>8;a[A+3|0]=C;break}else{R=d[A]|d[A+1|0]<<8|d[A+2|0]<<16|d[A+3|0]<<24|0;C=((ga(R>>>24,J)|0)>>>8)+o<<24|(ga(R>>>8&255,J)|0)+E&65280|((ga(R&255,J)|0)>>>8)+b&255|((ga(R>>>16&255,J)|0)>>>8<<16)+x&16711680;a[A]=C;C=C>>8;a[A+1|0]=C;C=C>>8;a[A+2|0]=C;C=C>>8;a[A+3|0]=C;break}}}while(0);do{if(!T){A=q-p|0;ba=+((A&65535)<<16>>16|0);if(!(+h[I>>3]>z&((+h[H>>3]<=ba|+h[F>>3]>ba|+h[G>>3]>z)^1))){break}R=(c[r>>2]|0)+((ga(e[D>>1]|0,u)|0)+(A&65535)<<2)|0;if((a[B]&1)==0){C=x;a[R]=C;C=C>>8;a[R+1|0]=C;C=C>>8;a[R+2|0]=C;C=C>>8;a[R+3|0]=C;break}else{A=d[R]|d[R+1|0]<<8|d[R+2|0]<<16|d[R+3|0]<<24|0;C=((ga(A>>>24,J)|0)>>>8)+o<<24|(ga(A>>>8&255,J)|0)+E&65280|((ga(A&255,J)|0)>>>8)+b&255|((ga(A>>>16&255,J)|0)>>>8<<16)+x&16711680;a[R]=C;C=C>>8;a[R+1|0]=C;C=C>>8;a[R+2|0]=C;C=C>>8;a[R+3|0]=C;break}}}while(0);do{if(!S){R=p+u|0;ba=+((R&65535)<<16>>16|0);if(!(+h[I>>3]>ba&((+h[H>>3]<=y|+h[F>>3]>y|+h[G>>3]>ba)^1))){break}A=(c[r>>2]|0)+((ga(e[D>>1]|0,R&65535)|0)+q<<2)|0;if((a[B]&1)==0){C=x;a[A]=C;C=C>>8;a[A+1|0]=C;C=C>>8;a[A+2|0]=C;C=C>>8;a[A+3|0]=C;break}else{R=d[A]|d[A+1|0]<<8|d[A+2|0]<<16|d[A+3|0]<<24|0;C=((ga(R>>>24,J)|0)>>>8)+o<<24|(ga(R>>>8&255,J)|0)+E&65280|((ga(R&255,J)|0)>>>8)+b&255|((ga(R>>>16&255,J)|0)>>>8<<16)+x&16711680;a[A]=C;C=C>>8;a[A+1|0]=C;C=C>>8;a[A+2|0]=C;C=C>>8;a[A+3|0]=C;break}}}while(0);do{if(!w){A=u-p|0;ba=+((A&65535)<<16>>16|0);if(!(+h[I>>3]>ba&((+h[H>>3]<=y|+h[F>>3]>y|+h[G>>3]>ba)^1))){break}R=(c[r>>2]|0)+((ga(e[D>>1]|0,A&65535)|0)+q<<2)|0;if((a[B]&1)==0){C=x;a[R]=C;C=C>>8;a[R+1|0]=C;C=C>>8;a[R+2|0]=C;C=C>>8;a[R+3|0]=C;break}else{A=d[R]|d[R+1|0]<<8|d[R+2|0]<<16|d[R+3|0]<<24|0;C=((ga(A>>>24,J)|0)>>>8)+o<<24|(ga(A>>>8&255,J)|0)+E&65280|((ga(A&255,J)|0)>>>8)+b&255|((ga(A>>>16&255,J)|0)>>>8<<16)+x&16711680;a[R]=C;C=C>>8;a[R+1|0]=C;C=C>>8;a[R+2|0]=C;C=C>>8;a[R+3|0]=C;break}}}while(0);p=p+1|0;}while(p>>>0<n>>>0);i=m;return}function Vd(b,f,g,j,k,l,m){b=b|0;f=f|0;g=+g;j=+j;k=+k;l=+l;m=m|0;var n=0,o=0,p=0.0,q=0.0,r=0.0,s=0.0,t=0.0,u=0.0,v=0.0,w=0.0,x=0,y=0,z=0,A=0,B=0,D=0.0,E=0.0,F=0.0,G=0.0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,V=0,X=0,Y=0;n=i;o=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[o>>2];if(l<=0.0){i=n;return}p=g+k;g=j+k;j=l+2.0;o=a[f+3|0]|0;l=j*.5;q=l+k;r=k-l;if(j<2.0){s=1.0/(j*.5+1.0)}else{s=.5}j=+h[b+24>>3];l=+h[b+40>>3];k=+(~~j|0);t=j>0.0?k:0.0;u=+(~~(t>l?l:t)>>>0>>>0);t=j>1.0e7?k:1.0e7;k=+(~~(t>l?l:t)>>>0>>>0);t=+h[b+32>>3];l=+h[b+48>>3];j=+(~~t|0);v=t>0.0?j:0.0;w=+(~~(v>l?l:v)>>>0>>>0);v=t>1.0e7?j:1.0e7;j=+(~~(v>l?l:v)>>>0>>>0);x=(m&1)!=0;y=(m&255)>>>1;z=(m&255)>>>2;A=(m&255)>>>3;if(x){if((A&1)==0){B=8}else{B=6}}else{B=6}do{if((B|0)==6){if((y&1)==0){D=j;break}if((z&1)==0){B=8}else{D=j}}}while(0);if((B|0)==8){D=+Ib(+(g+1.0),+j)}m=(A&1)!=0;do{if(x|m^1){if((z&1)==0){E=w;break}if((y&1)==0){B=12}else{E=w}}else{B=12}}while(0);if((B|0)==12){E=+tb(+g,+w)}A=(y&1)!=0;if(x|A^1){if((z&1)==0|m){F=k}else{B=15}}else{B=15}if((B|0)==15){F=+Ib(+(p+1.0),+k)}do{if(A|x^1){if(!m){G=u;break}if((z&1)==0){B=19}else{G=u}}else{B=19}}while(0);if((B|0)==19){G=+tb(+p,+u)}B=f;f=d[B]|d[B+1|0]<<8|d[B+2|0]<<16|d[B+3|0]<<24|0;B=f>>>24;z=~~+tb(+(+U(g-q)),+E);m=~~+Ib(+(+fa(g+q)+1.0),+D);if((z|0)>=(m|0)){i=n;return}D=q*q;E=r*r;x=o&255;o=((((f&255)*255|0)>>>0)/(B>>>0)|0)&255;A=((((f>>>8&255)*255|0)>>>0)/(B>>>0)|0)&255;y=((((f>>>16&255)*255|0)>>>0)/(B>>>0)|0)&255;B=b|0;f=b+8|0;H=b+16|0;b=z;do{u=+(b|0)-g;k=u*u;w=D-k;j=E-k;k=+W(w);z=~~+tb(+(+Ib(+(+U(p-k)),+F)),+G);v=+W(j);I=~~+tb(+(+Ib(+(+U(p-v)+1.0),+F)),+G);J=~~+tb(+(+Ib(+(+fa(p+v)),+F)),+G);K=~~+tb(+(+Ib(+(+fa(p+k)+1.0),+F)),+G);L37:do{if(w>0.0){if(j<0.0){if((z|0)>=(K|0)){break}L=b&65535;M=z;while(1){k=+hb(+(+(M|0)-p),+u);v=+tb(+(s*(q-k)),+0.0);l=+tb(+(s*(k-r)),+0.0);k=+Ib(+v,+1.0);N=(ga(d[28136+(~~(+Ib(+k,+(+Ib(+l,+1.0)))*63.0)&255)|0]|0,x)|0)>>>8;O=((ga(N,o)|0)>>>0)/255|0;P=((ga(N,A)|0)>>>0)/255|0;Q=((ga(N,y)|0)>>>0)/255|0;R=(c[B>>2]|0)+((ga(e[f>>1]|0,L)|0)+(M&65535)<<2)|0;if((a[H]&1)==0){S=N<<24|O&255|P<<8&65280|Q<<16&16711680}else{T=d[R]|d[R+1|0]<<8|d[R+2|0]<<16|d[R+3|0]<<24|0;V=P<<8;P=N^255;S=((ga(T>>>24,P)|0)>>>8)+N<<24|(ga(T>>>8&255,P)|0)+V&65280|((ga(T&255,P)|0)>>>8)+O&255|((ga(T>>>16&255,P)|0)>>>8<<16)+(V&65280|O&255|Q<<16)&16711680}C=S;a[R]=C;C=C>>8;a[R+1|0]=C;C=C>>8;a[R+2|0]=C;C=C>>8;a[R+3|0]=C;M=M+1|0;if((M|0)>=(K|0)){break L37}}}if((z|0)<(I|0)){M=b&65535;L=z;do{l=+hb(+(+(L|0)-p),+u);k=+tb(+(s*(q-l)),+0.0);v=+tb(+(s*(l-r)),+0.0);l=+Ib(+k,+1.0);R=(ga(d[28136+(~~(+Ib(+l,+(+Ib(+v,+1.0)))*63.0)&255)|0]|0,x)|0)>>>8;Q=((ga(R,o)|0)>>>0)/255|0;O=((ga(R,A)|0)>>>0)/255|0;V=((ga(R,y)|0)>>>0)/255|0;P=(c[B>>2]|0)+((ga(e[f>>1]|0,M)|0)+(L&65535)<<2)|0;if((a[H]&1)==0){X=R<<24|Q&255|O<<8&65280|V<<16&16711680}else{T=d[P]|d[P+1|0]<<8|d[P+2|0]<<16|d[P+3|0]<<24|0;N=O<<8;O=R^255;X=((ga(T>>>24,O)|0)>>>8)+R<<24|(ga(T>>>8&255,O)|0)+N&65280|((ga(T&255,O)|0)>>>8)+Q&255|((ga(T>>>16&255,O)|0)>>>8<<16)+(N&65280|Q&255|V<<16)&16711680}C=X;a[P]=C;C=C>>8;a[P+1|0]=C;C=C>>8;a[P+2|0]=C;C=C>>8;a[P+3|0]=C;L=L+1|0;}while((L|0)<(I|0))}if((J|0)>=(K|0)){break}L=b&65535;M=J;do{v=+hb(+(+(M|0)-p),+u);l=+tb(+(s*(q-v)),+0.0);k=+tb(+(s*(v-r)),+0.0);v=+Ib(+l,+1.0);P=(ga(d[28136+(~~(+Ib(+v,+(+Ib(+k,+1.0)))*63.0)&255)|0]|0,x)|0)>>>8;V=((ga(P,o)|0)>>>0)/255|0;Q=((ga(P,A)|0)>>>0)/255|0;N=((ga(P,y)|0)>>>0)/255|0;O=(c[B>>2]|0)+((ga(e[f>>1]|0,L)|0)+(M&65535)<<2)|0;if((a[H]&1)==0){Y=P<<24|V&255|Q<<8&65280|N<<16&16711680}else{T=d[O]|d[O+1|0]<<8|d[O+2|0]<<16|d[O+3|0]<<24|0;R=Q<<8;Q=P^255;Y=((ga(T>>>24,Q)|0)>>>8)+P<<24|(ga(T>>>8&255,Q)|0)+R&65280|((ga(T&255,Q)|0)>>>8)+V&255|((ga(T>>>16&255,Q)|0)>>>8<<16)+(R&65280|V&255|N<<16)&16711680}C=Y;a[O]=C;C=C>>8;a[O+1|0]=C;C=C>>8;a[O+2|0]=C;C=C>>8;a[O+3|0]=C;M=M+1|0;}while((M|0)<(K|0))}}while(0);b=b+1|0;}while((b|0)<(m|0));i=n;return}function Wd(f,j,k,l,m,n,o){f=f|0;j=j|0;k=+k;l=+l;m=+m;n=+n;o=+o;var p=0,q=0,r=0,s=0,t=0,u=0,v=0.0,x=0.0,y=0.0,z=0.0,A=0.0,B=0.0,D=0.0,E=0.0,F=0.0,G=0.0,H=0.0,I=0.0,J=0.0,K=0.0,L=0.0,M=0.0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,W=0,X=0,Y=0,Z=0,_=0,$=0,aa=0,ba=0,ca=0,da=0,ea=0,fa=0,ha=0,ia=0,ja=0,ka=0,la=0,ma=0.0,na=0.0,oa=0.0,pa=0,qa=0.0,ra=0.0,sa=0.0,ta=0,ua=0,va=0,wa=0,xa=0,ya=0,za=0,Aa=0,Ba=0.0,Ca=0.0,Da=0.0,Ea=0.0,Fa=0.0,Ga=0.0,Ha=0.0,Ia=0.0,Ja=0.0,Ka=0.0,La=0.0,Ma=0.0,Na=0.0,Oa=0.0,Pa=0.0,Qa=0,Ra=0.0,Sa=0.0,Ta=0.0,Ua=0.0,Va=0,Wa=0.0,Xa=0.0,Ya=0.0,Za=0.0,_a=0.0,$a=0.0,ab=0.0,bb=0.0,cb=0.0,db=0.0,eb=0.0,fb=0.0,gb=0.0,ib=0.0,jb=0,kb=0.0,lb=0.0,mb=0.0,nb=0.0,ob=0.0,pb=0.0,qb=0.0,rb=0.0,sb=0.0,tb=0.0,ub=0.0,vb=0.0,wb=0.0,xb=0.0,yb=0.0,zb=0.0,Ab=0,Bb=0.0,Cb=0.0,Db=0.0,Eb=0.0,Fb=0.0,Gb=0.0,Hb=0.0,Ib=0.0,Jb=0.0,Kb=0.0,Lb=0.0,Mb=0.0,Nb=0.0,Ob=0.0,Pb=0.0,Qb=0.0,Rb=0.0,Sb=0.0,Tb=0.0,Ub=0.0,Vb=0.0,Wb=0.0,Xb=0,Yb=0.0,Zb=0.0,_b=0.0,$b=0.0;p=i;i=i+64|0;q=j;j=i;i=i+4|0;i=i+7&-8;c[j>>2]=c[q>>2];q=p|0;r=p+16|0;s=p+32|0;t=p+48|0;u=a[j+3|0]|0;v=o>0.0?o:1.0;o=m-k;x=n-l;y=+hb(+o,+x);if(y==0.0){i=p;return}z=o/y;o=x/y;y=v*.5+1.0;x=y;A=x*o;B=k-A;D=m-A;E=A+m;F=A+k;A=x*z;x=A+l;G=A+n;H=n-A;I=l-A;if(v<2.0){J=1.0/y}else{J=.5}y=z*J;z=B<w?B:w;A=B>-w?B:-w;K=x<w?x:w;L=x>-w?x:-w;M=D<z?D:z;z=D>A?D:A;A=G<K?G:K;K=G>L?G:L;L=E<M?E:M;M=E>z?E:z;z=H<A?H:A;A=H>K?H:K;K=F<L?F:L;L=F>M?F:M;M=I<z?I:z;z=I>A?I:A;A=o*J;N=(K>=0.0)+~~K|0;O=~~L-(L<0.0)|0;P=(M>=0.0)+~~M|0;Q=~~z-(z<0.0)|0;R=q|0;g[R>>2]=A;S=q+4|0;z=-0.0-A;g[S>>2]=z;T=q+8|0;M=-0.0-y;g[T>>2]=M;U=q+12|0;g[U>>2]=y;q=r|0;g[q>>2]=M;W=r+4|0;g[W>>2]=y;X=r+8|0;g[X>>2]=z;Y=r+12|0;g[Y>>2]=A;L=(v*.5+1.0)*J;r=j;j=d[r]|d[r+1|0]<<8|d[r+2|0]<<16|d[r+3|0]<<24|0;r=j>>>24;Z=(((j&255)*255|0)>>>0)/(r>>>0)|0;_=(((j>>>8&255)*255|0)>>>0)/(r>>>0)|0;$=(((j>>>16&255)*255|0)>>>0)/(r>>>0)|0;if(y==0.0|A==0.0){v=+(P|0);K=+h[f+32>>3];r=~~(v>K?v:K);K=+(Q|0);v=+h[f+48>>3]+-1.0;j=~~(K<v?K:v);v=+(N|0);K=+h[f+24>>3];aa=~~(v>K?v:K);K=+(O|0);v=+h[f+40>>3]+-1.0;ba=~~(K<v?K:v);v=A*(m- +(aa|0))-y*(n- +(r|0));if((r|0)>(j|0)){i=p;return}ca=(aa|0)>(ba|0);K=+(1-aa+ba|0);da=u&255;ea=Z&255;fa=_&255;ha=$&255;ia=f|0;ja=f+8|0;ka=f+16|0;la=r;o=L-v;ma=L+v;while(1){if(ca){na=o;oa=ma}else{r=la&65535;pa=aa;v=o;qa=ma;while(1){ra=v<1.0?v:1.0;sa=qa<1.0?qa:1.0;ta=(ga(d[28136+~~((ra<sa?ra:sa)*63.0)|0]|0,da)|0)>>>8;ua=((ga(ta,ea)|0)>>>0)/255|0;va=((ga(ta,fa)|0)>>>0)/255|0;wa=((ga(ta,ha)|0)>>>0)/255|0;xa=(c[ia>>2]|0)+((ga(e[ja>>1]|0,r)|0)+(pa&65535)<<2)|0;if((a[ka]&1)==0){ya=ta<<24|ua&255|va<<8&65280|wa<<16&16711680}else{za=d[xa]|d[xa+1|0]<<8|d[xa+2|0]<<16|d[xa+3|0]<<24|0;Aa=va<<8;va=ta^255;ya=((ga(za>>>24,va)|0)>>>8)+ta<<24|(ga(za>>>8&255,va)|0)+Aa&65280|((ga(za&255,va)|0)>>>8)+ua&255|((ga(za>>>16&255,va)|0)>>>8<<16)+(Aa&65280|ua&255|wa<<16)&16711680}C=ya;a[xa]=C;C=C>>8;a[xa+1|0]=C;C=C>>8;a[xa+2|0]=C;C=C>>8;a[xa+3|0]=C;sa=A+v;ra=qa-A;if((pa|0)<(ba|0)){pa=pa+1|0;v=sa;qa=ra}else{na=sa;oa=ra;break}}}if((la|0)<(j|0)){la=la+1|0;o=na+(M-K*A);ma=oa+(y-K*z)}else{break}}i=p;return}K=+V(+A);oa=+V(+y);if(K>=oa){ma=(G-x)/(D-B);na=(H-I)/(E-F);o=G-D*ma;qa=H-E*na;if(A<0.0){Ba=na;Ca=ma;Da=qa;Ea=o}else{Ba=ma;Ca=na;Da=o;Ea=qa}qa=1.0/Ba;Ba=1.0/Ca;Ca=+(P|0);o=(Ca-Da)*qa;na=+((o>=0.0)+~~o|0);o=m-na;ma=n-Ca;v=A*o-y*ma;la=s|0;ra=L-v;g[la>>2]=ra;j=s+4|0;sa=L+v;g[j>>2]=sa;ba=s+8|0;v=J+(A*ma+y*o);g[ba>>2]=v;ya=s+12|0;o=J+(-0.0-A*(l-Ca)-y*(k-na));g[ya>>2]=o;s=f+8|0;if((P|0)>(Q|0)){i=p;return}ka=b[s>>1]|0;ja=ga(ka&65535,P)|0;ia=f+24|0;ha=f+32|0;fa=f+40|0;ea=f+48|0;da=u&255;aa=Z&255;ca=_&255;pa=$&255;r=~~na;xa=(c[f>>2]|0)+(ja<<2)|0;ja=P;na=A;Ca=ra;ra=z;ma=sa;sa=M;Fa=v;v=y;Ga=o;P=ka;o=M;Ha=y;Ia=z;Ja=A;while(1){Ka=+(ja|0);La=qa*(Ka-Da);ka=(La>=0.0)+~~La|0;La=Ba*(Ka-Ea);wa=r-ka|0;Ka=+(((wa|0)>-1?wa:-wa|0)|0);Ma=Ca-Ka*na;g[la>>2]=Ma;Na=ma-Ka*ra;g[j>>2]=Na;Oa=Fa-Ka*sa;g[ba>>2]=Oa;Pa=Ga-Ka*v;g[ya>>2]=Pa;wa=~~La-(La<0.0)|0;if((ka|0)>(wa|0)){Qa=ka;Ra=na;Sa=ra;Ta=sa;Ua=v;Va=P;Wa=o;Xa=Ma;Ya=Ha;Za=Na;_a=Ia;$a=Oa;ab=Ja;bb=Pa}else{La=+(ja|0);ua=(wa|0)>(ka|0)?wa:ka;Aa=ka;va=xa+(ka<<2)|0;Ka=Ma;Ma=Na;Na=Oa;Oa=Pa;Pa=na;cb=ra;db=sa;eb=v;while(1){if(Ka>=1.0|Ka>0.0){fb=Ka<1.0?Ka:1.0}else{fb=0.0}if(Ma>=1.0|Ma>0.0){gb=Ma<1.0?Ma:1.0}else{gb=0.0}ib=+(Aa|0);if(+h[ea>>3]>La&((+h[fa>>3]<=ib|+h[ia>>3]>ib|+h[ha>>3]>La)^1)){ka=(ga(d[28136+~~((fb<gb?fb:gb)*63.0)|0]|0,da)|0)>>>8;za=va;ta=d[za]|d[za+1|0]<<8|d[za+2|0]<<16|d[za+3|0]<<24|0;jb=ka^255;C=((ga(jb,ta>>>24)|0)>>>8)+ka<<24|(ga(jb,ta>>>8&255)|0)+((((ga(ka,ca)|0)>>>0)/255|0)<<8)&65280|((ga(jb,ta&255)|0)>>>8)+(((ga(ka,aa)|0)>>>0)/255|0)&255|((ga(jb,ta>>>16&255)|0)>>>8)+(((ga(ka,pa)|0)>>>0)/255|0)<<16&16711680;a[za]=C;C=C>>8;a[za+1|0]=C;C=C>>8;a[za+2|0]=C;C=C>>8;a[za+3|0]=C;kb=+g[R>>2];lb=+g[la>>2];mb=+g[S>>2];nb=+g[j>>2];ob=+g[T>>2];pb=+g[ba>>2];qb=+g[U>>2];rb=+g[ya>>2]}else{kb=Pa;lb=Ka;mb=cb;nb=Ma;ob=db;pb=Na;qb=eb;rb=Oa}sb=kb+lb;g[la>>2]=sb;tb=mb+nb;g[j>>2]=tb;ub=ob+pb;g[ba>>2]=ub;vb=qb+rb;g[ya>>2]=vb;if((Aa|0)<(wa|0)){Aa=Aa+1|0;va=va+4|0;Ka=sb;Ma=tb;Na=ub;Oa=vb;Pa=kb;cb=mb;db=ob;eb=qb}else{break}}Qa=ua+1|0;Ra=kb;Sa=mb;Ta=ob;Ua=qb;Va=b[s>>1]|0;Wa=+g[q>>2];Xa=sb;Ya=+g[W>>2];Za=tb;_a=+g[X>>2];$a=ub;ab=+g[Y>>2];bb=vb}eb=Wa+Xa;g[la>>2]=eb;db=Ya+Za;g[j>>2]=db;cb=_a+$a;g[ba>>2]=cb;Pa=ab+bb;g[ya>>2]=Pa;if((ja|0)<(Q|0)){r=Qa;xa=xa+((Va&65535)<<2)|0;ja=ja+1|0;na=Ra;Ca=eb;ra=Sa;ma=db;sa=Ta;Fa=cb;v=Ua;Ga=Pa;P=Va;o=Wa;Ha=Ya;Ia=_a;Ja=ab}else{break}}i=p;return}if(K>=oa){i=p;return}oa=(G-x)/(D-B);B=(H-I)/(E-F);F=G-D*oa;D=H-E*B;if(y>0.0){wb=F;xb=D;yb=oa;zb=B}else{wb=D;xb=F;yb=B;zb=oa}oa=+(N|0);B=oa*zb+xb;F=+((B>=0.0)+~~B|0);B=m-oa;m=n-F;n=A*B-y*m;Va=t|0;D=L-n;g[Va>>2]=D;P=t+4|0;E=L+n;g[P>>2]=E;ja=t+8|0;n=J+(y*B+A*m);g[ja>>2]=n;xa=t+12|0;m=J+(-0.0-A*(l-F)-y*(k-oa));g[xa>>2]=m;if((N|0)>(O|0)){i=p;return}t=f+8|0;Qa=f+24|0;r=f+32|0;Q=f+40|0;ya=f+48|0;ba=u&255;u=Z&255;Z=_&255;_=$&255;$=~~F;j=(c[f>>2]|0)+(N<<2)|0;f=N;F=M;oa=D;D=y;k=E;E=z;l=n;n=A;J=m;m=A;A=z;z=M;M=y;while(1){y=+(f|0);B=xb+zb*y;N=(B>=0.0)+~~B|0;B=wb+yb*y;la=$-N|0;y=+(((la|0)>-1?la:-la|0)|0);L=oa-y*F;g[Va>>2]=L;H=k-y*D;g[P>>2]=H;G=l-y*E;g[ja>>2]=G;I=J-y*n;g[xa>>2]=I;la=~~B-(B<0.0)|0;if((N|0)>(la|0)){Ab=N;Bb=F;Cb=D;Db=E;Eb=n;Fb=m;Gb=L;Hb=A;Ib=H;Jb=z;Kb=G;Lb=M;Mb=I}else{s=b[t>>1]|0;pa=j+((ga(s&65535,N)|0)<<2)|0;B=+(f|0);aa=(la|0)>(N|0)?la:N;ca=N;N=pa;y=L;L=H;H=G;G=I;I=F;x=D;K=E;ab=n;pa=s;while(1){do{if(y>=1.0|y>0.0){if(y>=1.0){Nb=1.0;break}Nb=y}else{Nb=0.0}}while(0);do{if(L>=1.0|L>0.0){if(L>=1.0){Ob=1.0;break}Ob=L}else{Ob=0.0}}while(0);Ja=+(ca|0);if(+h[ya>>3]>Ja&((+h[Q>>3]<=B|+h[Qa>>3]>B|+h[r>>3]>Ja)^1)){s=(ga(d[28136+~~((Nb<Ob?Nb:Ob)*63.0)|0]|0,ba)|0)>>>8;da=N;ha=d[da]|d[da+1|0]<<8|d[da+2|0]<<16|d[da+3|0]<<24|0;ia=s^255;C=((ga(ia,ha>>>24)|0)>>>8)+s<<24|(ga(ia,ha>>>8&255)|0)+((((ga(s,Z)|0)>>>0)/255|0)<<8)&65280|((ga(ia,ha&255)|0)>>>8)+(((ga(s,u)|0)>>>0)/255|0)&255|((ga(ia,ha>>>16&255)|0)>>>8)+(((ga(s,_)|0)>>>0)/255|0)<<16&16711680;a[da]=C;C=C>>8;a[da+1|0]=C;C=C>>8;a[da+2|0]=C;C=C>>8;a[da+3|0]=C;Pb=+g[q>>2];Qb=+g[Va>>2];Rb=+g[W>>2];Sb=+g[P>>2];Tb=+g[X>>2];Ub=+g[ja>>2];Vb=+g[Y>>2];Wb=+g[xa>>2];Xb=b[t>>1]|0}else{Pb=I;Qb=y;Rb=x;Sb=L;Tb=K;Ub=H;Vb=ab;Wb=G;Xb=pa}Yb=Pb+Qb;g[Va>>2]=Yb;Zb=Rb+Sb;g[P>>2]=Zb;_b=Tb+Ub;g[ja>>2]=_b;$b=Vb+Wb;g[xa>>2]=$b;if((ca|0)<(la|0)){ca=ca+1|0;N=N+((Xb&65535)<<2)|0;y=Yb;L=Zb;H=_b;G=$b;I=Pb;x=Rb;K=Tb;ab=Vb;pa=Xb}else{break}}Ab=aa+1|0;Bb=Pb;Cb=Rb;Db=Tb;Eb=Vb;Fb=+g[R>>2];Gb=Yb;Hb=+g[S>>2];Ib=Zb;Jb=+g[T>>2];Kb=_b;Lb=+g[U>>2];Mb=$b}ab=Fb+Gb;g[Va>>2]=ab;K=Hb+Ib;g[P>>2]=K;x=Jb+Kb;g[ja>>2]=x;I=Lb+Mb;g[xa>>2]=I;if((f|0)<(O|0)){$=Ab;j=j+4|0;f=f+1|0;F=Bb;oa=ab;D=Cb;k=K;E=Db;l=x;n=Eb;J=I;m=Fb;A=Hb;z=Jb;M=Lb}else{break}}i=p;return}function Xd(f,g,j,k,l,m,n){f=f|0;g=g|0;j=j|0;k=k|0;l=l|0;m=m|0;n=n|0;var o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,D=0,E=0,F=0,G=0,H=0.0,I=0.0,J=0,K=0,L=0,M=0.0,N=0.0,O=0.0,P=0.0,Q=0.0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0,Z=0,_=0,$=0;o=i;p=j;j=i;i=i+4|0;i=i+7&-8;c[j>>2]=c[p>>2];p=ho(m|0)|0;q=p>>>0<n>>>0?p:n;n=j;p=d[n]|d[n+1|0]<<8|d[n+2|0]<<16|d[n+3|0]<<24|0;n=p>>>24;r=(((p&255)*255|0)>>>0)/(n>>>0)|0;s=(((p>>>8&255)*255|0)>>>0)/(n>>>0)|0;t=(((p>>>16&255)*255|0)>>>0)/(n>>>0)|0;n=ho(m|0)|0;p=n>>>0<q>>>0?n:q;if((p|0)==0){u=0}else{n=0;v=0;while(1){w=b[f+((a[m+n|0]|0)-32<<4)+4>>1]|0;x=w<<16>>16>v<<16>>16?w:v;w=n+1|0;if(w>>>0<p>>>0){n=w;v=x}else{u=x;break}}}v=j+3|0;if((q|0)==0){i=o;return}j=u+l&65535;l=g+32|0;u=g+48|0;n=g+24|0;p=g+40|0;x=d[v]|0;w=r&255;r=s&255;s=t&255;t=g|0;y=g+8|0;z=g+16|0;g=0;A=k;while(1){k=(a[m+g|0]|0)-32|0;B=b[f+(k<<4)+8>>1]|0;D=j-(b[f+(k<<4)+4>>1]|0)&65535;E=D+(b[f+(k<<4)+2>>1]|0)&65535;L10:do{if((D&65535)>>>0<(E&65535)>>>0){F=(b[f+(k<<4)+6>>1]|0)+(A&65535)|0;G=e[f+(k<<4)>>1]|0;H=+(F&65535|0);I=+(F+G&65535|0);J=c[f+(k<<4)+12>>2]|0;K=D;while(1){L=K&65535;M=+h[l>>3];N=+h[u>>3];O=+(L|0);P=+(~~(O<M?M:O)|0);do{if((~~(P>N?N:P)|0)==(L|0)){O=+h[n>>3];M=+h[p>>3];Q=+(~~(H<O?O:H)|0);R=~~(Q>M?M:Q);Q=+(~~(I<O?O:I)|0);S=R&65535;T=~~(Q>M?M:Q)&65535;if(S>>>0>=T>>>0){break}U=R&65535;R=S;do{S=(ga(d[J+(R-F)|0]|0,x)|0)>>>8;a[v]=S;V=((ga(S,w)|0)>>>0)/255|0;W=((ga(S,r)|0)>>>0)/255|0;X=((ga(S,s)|0)>>>0)/255|0;Y=(c[t>>2]|0)+((ga(e[y>>1]|0,L)|0)+R<<2)|0;if((a[z]&1)==0){Z=S<<24|V&255|W<<8&65280|X<<16&16711680}else{_=d[Y]|d[Y+1|0]<<8|d[Y+2|0]<<16|d[Y+3|0]<<24|0;$=W<<8;W=S^255;Z=((ga(_>>>24,W)|0)>>>8)+S<<24|(ga(_>>>8&255,W)|0)+$&65280|((ga(_&255,W)|0)>>>8)+V&255|((ga(_>>>16&255,W)|0)>>>8<<16)+($&65280|V&255|X<<16)&16711680}C=Z;a[Y]=C;C=C>>8;a[Y+1|0]=C;C=C>>8;a[Y+2|0]=C;C=C>>8;a[Y+3|0]=C;U=U+1&65535;R=U&65535;}while(R>>>0<T>>>0)}}while(0);L=K+1&65535;if((L&65535)>>>0>=(E&65535)>>>0){break L10}J=J+G|0;K=L}}}while(0);E=g+1|0;if(E>>>0<q>>>0){g=E;A=B+A&65535}else{break}}i=o;return}function Yd(b,f,g,j,k,l){b=b|0;f=f|0;g=g|0;j=j|0;k=k|0;l=l|0;var m=0,n=0,o=0.0,p=0.0,q=0.0,r=0.0,s=0,t=0.0,u=0.0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0;m=i;n=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[n>>2];n=g<<16>>16>j<<16>>16;o=+h[b+24>>3];p=+h[b+40>>3];q=+((n?j:g)<<16>>16|0);r=+(~~(q<o?o:q)|0);s=~~(r>p?p:r);r=+(((n?g:j)<<16>>16)+1|0);q=+(~~(r<o?o:r)|0);r=+h[b+32>>3];o=+h[b+48>>3];t=+(k<<16>>16|0);u=+(~~(t<r?r:t)|0);j=~~(u>o?o:u);g=j&65535;u=+((k<<16>>16)+1|0);t=+(~~(u<r?r:u)|0);k=s&65535;n=~~(q>p?p:q)&65535;if(k>>>0>=n>>>0){i=m;return}v=l<<16>>16==-1;w=l&65535;l=j&65535;j=~~(t>o?o:t)&65535;x=l>>>0<j>>>0;y=f;f=b|0;z=b+8|0;A=b+16|0;b=s&65535;s=k;do{if(v){if(x){B=6}}else{if(!((1<<(s&15)&w|0)==0|x^1)){B=6}}if((B|0)==6){B=0;k=d[y]|d[y+1|0]<<8|d[y+2|0]<<16|d[y+3|0]<<24|0;D=k&65280;E=k>>>24;F=E^255;G=g;H=l;do{I=(c[f>>2]|0)+((ga(e[z>>1]|0,H)|0)+s<<2)|0;if((a[A]&1)==0){J=k}else{K=d[I]|d[I+1|0]<<8|d[I+2|0]<<16|d[I+3|0]<<24|0;J=((ga(K>>>24,F)|0)>>>8)+E<<24|(ga(K>>>8&255,F)|0)+D&65280|((ga(K&255,F)|0)>>>8)+k&255|((ga(K>>>16&255,F)|0)>>>8<<16)+k&16711680}C=J;a[I]=C;C=C>>8;a[I+1|0]=C;C=C>>8;a[I+2|0]=C;C=C>>8;a[I+3|0]=C;G=G+1&65535;H=G&65535;}while(H>>>0<j>>>0)}b=b+1&65535;s=b&65535;}while(s>>>0<n>>>0);i=m;return}function Zd(){var b=0,d=0,e=0,f=0.0,g=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0.0,s=0.0,t=0.0,u=0.0,v=0.0,w=0.0,x=0.0,y=0.0,z=0.0,A=0.0,B=0.0,C=0,D=0.0,E=0.0,F=0,G=0.0,H=0.0,I=0.0,J=0.0,K=0.0,L=0.0,M=0,N=0,O=0.0,P=0.0,Q=0.0,R=0.0,S=0.0,T=0.0,U=0.0,V=0,W=0,X=0;b=i;i=i+64|0;d=b|0;e=b+32|0;f=+h[3794];g=~~f;j=~~+h[3795];k=~~+h[3796];l=~~+h[3797]-(c[7709]|0)|0;if((a[30833]&1)==0){if((a[31104]&1)==0){m=g}else{n=3}}else{n=3}if((n|0)==3){m=(a[31105]&1)==0?g+48|0:g}g=a[31105]|0;if((a[30832]&1)==0){if((g&1)==0){o=l}else{n=6}}else{n=6}if((n|0)==6){o=(a[31104]&1)==0?l-15|0:l}l=(g&1)==0;if(l){p=m}else{p=m+3+(c[7778]|0)|0}m=a[31104]|0;if((m&1)==0){q=o}else{q=o-3-(c[7777]|0)|0}o=(a[30840]&1)==0?k:k-18|0;r=+(j|0);k=~~+tb(+r,+(+(q|0)));q=~~+Ib(+(+(o|0)),+(+(p|0)));s=+(q|0);t=s+ +(o-q|0);u=r+ +(k-j|0);v=+((c[7706]|0)>>>0>>>0);w=+Ib(+t,+(s+v));x=+Ib(+u,+(r+v));y=+tb(+s,+(t-v));z=+tb(+r,+(u-v));v=+Ib(+w,+y);A=+Ib(+x,+z);B=+tb(+w,+y);y=+tb(+x,+z);h[3806]=v;h[3807]=A;h[3808]=B;h[3809]=y;c[7655]=~~(v+ +((~~(B-v)|0)/2|0|0));c[7656]=~~(A+ +((~~(y-A)|0)/2|0|0));if(l){C=m}else{A=f+ +((c[7778]|0)>>>0>>>0);h[3819]=f;h[3820]=r;h[3821]=A;h[3822]=r+(u-r+15.0);m=d;c[m>>2]=c[7638];c[m+4>>2]=c[7639];c[m+8>>2]=c[7640];c[m+12>>2]=c[7641];c[m+16>>2]=c[7642];c[m+20>>2]=c[7643];c[m+24>>2]=c[7644];c[m+28>>2]=c[7645];_d(31e3,d);C=a[31104]|0}if((C&1)!=0){u=s+-48.0;A=+(k+3|0);f=A+ +((c[7777]|0)>>>0>>>0);h[3815]=u;h[3816]=A;h[3817]=u+(t-s+48.0);h[3818]=f;C=e;c[C>>2]=c[7630];c[C+4>>2]=c[7631];c[C+8>>2]=c[7632];c[C+12>>2]=c[7633];c[C+16>>2]=c[7634];c[C+20>>2]=c[7635];c[C+24>>2]=c[7636];c[C+28>>2]=c[7637];_d(30896,e)}if((a[30840]&1)==0){D=+h[3794];E=+h[3797];F=c[7709]|0;G=+(F>>>0>>>0);H=E-G;I=+h[3796];J=I-D;K=D+J;L=G+H;h[3823]=D;h[3824]=H;h[3825]=K;h[3826]=L;M=c[7566]|0;N=M;Mn(N);O=+h[3806];P=+h[3808];Q=P-O;R=+h[3807];S=+h[3809];T=S-R;U=Q*T;V=~~U;W=Nn(V,24)|0;X=W;c[7566]=X;$d();i=b;return}f=+(o+3|0);h[3811]=f;h[3812]=r;h[3813]=f+ +(o+13|0);h[3814]=r+ +(k|0);D=+h[3794];E=+h[3797];F=c[7709]|0;G=+(F>>>0>>>0);H=E-G;I=+h[3796];J=I-D;K=D+J;L=G+H;h[3823]=D;h[3824]=H;h[3825]=K;h[3826]=L;M=c[7566]|0;N=M;Mn(N);O=+h[3806];P=+h[3808];Q=P-O;R=+h[3807];S=+h[3809];T=S-R;U=Q*T;V=~~U;W=Nn(V,24)|0;X=W;c[7566]=X;$d();i=b;return}function _d(a,b){a=a|0;b=b|0;var d=0,e=0,f=0,g=0,j=0.0,k=0.0,l=0.0,m=0.0,n=0.0,o=0.0,p=0.0,q=0.0;d=i;e=b;b=i;i=i+32|0;io(b,e,32)|0;e=a+32|0;f=b;c[e>>2]=c[f>>2];c[e+4>>2]=c[f+4>>2];c[e+8>>2]=c[f+8>>2];c[e+12>>2]=c[f+12>>2];c[e+16>>2]=c[f+16>>2];c[e+20>>2]=c[f+20>>2];c[e+24>>2]=c[f+24>>2];c[e+28>>2]=c[f+28>>2];f=~~+h[a+40>>3];e=~~+h[a+48>>3];b=~~+h[a+32>>3]+48&65535;g=~~+h[a+56>>3]-15&65535;j=+((b<<16>>16>e<<16>>16?e:b)<<16>>16|0);k=+(f<<16>>16|0);l=+(e<<16>>16|0);m=+((g<<16>>16<f<<16>>16?f:g)<<16>>16|0);n=+Ib(+j,+l);o=+Ib(+k,+m);p=+tb(+j,+l);l=+tb(+k,+m);m=+((c[a+96>>2]|0)>>>0>>>0);k=+Ib(+p,+(n+m));j=+Ib(+l,+(o+m));q=+tb(+n,+(p-m));p=+tb(+o,+(l-m));m=+Ib(+k,+q);l=+Ib(+j,+p);o=+tb(+k,+q);q=+tb(+j,+p);h[a+64>>3]=m;h[a+72>>3]=l;h[a+80>>3]=o;h[a+88>>3]=q;i=d;return}function $d(){var d=0,e=0,f=0.0,i=0.0,j=0.0,k=0.0,l=0.0,m=0.0,n=0.0,o=0.0,p=0,q=0,r=0.0,s=0.0,t=0,u=0.0,v=0.0,x=0.0,y=0.0,z=0,A=0.0,B=0.0,C=0.0,D=0.0,E=0,F=0.0,G=0.0,H=0.0,I=0.0,J=0.0,K=0.0,L=0.0,M=0.0,N=0.0,O=0.0,P=0.0,Q=0.0,R=0,S=0,T=0,V=0,W=0,X=0,Y=0,Z=0.0,_=0.0,$=0.0,aa=0.0,ba=0,ca=0.0,da=0.0,ea=0.0,fa=0.0,ha=0,ia=0,ja=0,ka=0.0,la=0.0,ma=0,na=0.0,oa=0.0,pa=0,qa=0,ra=0,sa=0.0,ta=0.0,ua=0,va=0,wa=0,xa=0,ya=0,za=0,Aa=0,Ba=0.0,Ca=0,Da=0,Ea=0,Fa=0,Ga=0.0,Ha=0,Ia=0,Ja=0.0,Ka=0,La=0,Ma=0,Na=0.0,Oa=0,Pa=0,Qa=0.0,Ra=0,Sa=0,Ta=0.0,Ua=0,Va=0,Wa=0.0,Xa=0.0,Ya=0,Za=0.0,_a=0.0,$a=0.0,ab=0.0;if(+h[3804]- +h[3802]<=0.0){return}if(+h[3805]- +h[3803]<=0.0){return}ko(c[7566]|0,0,~~((+h[3808]- +h[3806])*(+h[3809]- +h[3807])*24.0)|0)|0;h[3790]=-w;h[3789]=w;d=c[7568]|0;if((d|0)!=(c[7569]|0)){e=d;do{f=+h[e+8>>3];i=+h[e+16>>3];j=+h[e+24>>3];k=+h[e+32>>3];l=+h[e+40>>3];m=+h[e+48>>3];n=+h[e+56>>3];o=+h[e+64>>3];d=c[e+76>>2]|0;p=c[e+80>>2]|0;q=c[e+84>>2]|0;r=+h[3806];s=+h[3808];t=~~(s-r);u=+h[3802];v=+h[3803];x=+h[3804];y=+h[3805];z=j<u|x<f|k<v|y<i;if(z){A=0.0;B=0.0;C=0.0;D=0.0}else{A=f>u?f:u;B=i>v?i:v;C=j<x?j:x;D=k<y?k:y}E=~~r;F=x-u;G=+((~~s<<16>>16)-(E<<16>>16)|0);s=+(E<<16>>16|0);H=+(~~(+Gb(+(s+G*((A-u)/F)))-r)<<16>>16|0);I=+(~~(+Gb(+(s+G*((C-u)/F)))-r)<<16>>16|0);r=+h[3807];E=~~r;F=y-v;G=+((~~+h[3809]<<16>>16)-(E<<16>>16)|0);s=+(E<<16>>16|0);J=+(~~(+Gb(+(s+(B-v)/F*G))-r)<<16>>16|0);K=I-H;do{if(K>0.0){I=+(~~(+Gb(+(s+G*((D-v)/F)))-r)<<16>>16|0)-J;if(I<=0.0){break}if(z){L=0.0;M=0.0;N=0.0;O=0.0}else{L=f>u?f:u;M=i>v?i:v;N=j<x?j:x;O=k<y?k:y}P=N-L;if(P<=0.0){break}Q=O-M;if(Q<=0.0){break}E=c[7566]|0;R=~~(H+ +(t>>>0>>>0)*J);S=E+(R*24|0)|0;T=~~(n-l);V=~~(o-m);L21:do{switch(p|0){case 1:case 7:{W=~~K;X=~~I;Y=d;if((X|0)<=0){break L21}Z=+(X|0);_=+(V|0);$=k-i;aa=+(ga(V,T)|0);ba=(W|0)>0;ca=+(W|0);da=+(T|0);ea=j-f;fa=+(q|0);ha=(q|0)==2;ia=0;while(1){ja=ia+1|0;ka=_*(M+Q*+(ia|0)/Z-i)/$;la=_*(M+Q*+(ja|0)/Z-i)/$;ma=ka>la;na=ma?la:ka;oa=ma?ka:la;L26:do{if(ba){ma=(ga(ia,t)|0)+R|0;pa=0;while(1){qa=pa+1|0;la=da*(L+P*+(pa|0)/ca-f)/ea;ka=da*(L+P*+(qa|0)/ca-f)/ea;ra=la>ka;sa=ra?la:ka;ta=ra?ka:la;do{if(sa>=0.0&ta<da){la=+U(na);ka=ta+da*la;if(ka<0.0){break}if(ka>=aa){break L26}ra=~~la;ua=~~+U(oa);va=((ra|0)==(ua|0))+ua|0;la=+U(_);if(+(va|0)>la){wa=~~la-1|0}else{wa=va}if((ra|0)>=(wa|0)){break}va=ma+pa|0;ua=E+(va*24|0)|0;xa=E+(va*24|0)+8|0;ya=E+(va*24|0)+16|0;va=ra;do{la=da*+(va|0);ra=~~(fa*+U(ta+la));za=~~(fa*+U(sa+la));Aa=((za|0)==(ra|0)?q:0)+za|0;if((ra|0)<(Aa|0)){za=ra;do{la=+(b[Y+(za<<1)>>1]|0);if(ha){ra=za+1|0;Ba=+(b[Y+(ra<<1)>>1]|0);Ca=ra}else{Ba=0.0;Ca=za}ka=la*la+Ba*Ba;if(ka>+h[ua>>3]){h[ua>>3]=ka;h[xa>>3]=la;h[ya>>3]=Ba}za=Ca+1|0;}while((za|0)<(Aa|0))}va=va+1|0;}while((va|0)<(wa|0))}}while(0);if((qa|0)<(W|0)){pa=qa}else{break}}}}while(0);if((ja|0)<(X|0)){ia=ja}else{break}}break};case 2:case 8:{ia=~~K;X=~~I;W=d;if((X|0)<=0){break L21}fa=+(X|0);da=+(V|0);_=k-i;aa=+(ga(V,T)|0);Y=(ia|0)>0;ea=+(ia|0);ca=+(T|0);$=j-f;Z=+(q|0);ha=(q|0)==2;ba=0;while(1){pa=ba+1|0;oa=da*(M+Q*+(ba|0)/fa-i)/_;na=da*(M+Q*+(pa|0)/fa-i)/_;ma=oa>na;sa=ma?na:oa;ta=ma?oa:na;L56:do{if(Y){ma=(ga(ba,t)|0)+R|0;va=0;while(1){ya=va+1|0;na=ca*(L+P*+(va|0)/ea-f)/$;oa=ca*(L+P*+(ya|0)/ea-f)/$;xa=na>oa;la=xa?na:oa;ka=xa?oa:na;do{if(la>=0.0&ka<ca){na=+U(sa);oa=ka+ca*na;if(oa<0.0){break}if(oa>=aa){break L56}xa=~~na;ua=~~+U(ta);Aa=((xa|0)==(ua|0))+ua|0;na=+U(da);if(+(Aa|0)>na){Da=~~na-1|0}else{Da=Aa}if((xa|0)>=(Da|0)){break}Aa=ma+va|0;ua=E+(Aa*24|0)|0;za=E+(Aa*24|0)+8|0;ra=E+(Aa*24|0)+16|0;Aa=xa;do{na=ca*+(Aa|0);xa=~~(Z*+U(ka+na));Ea=~~(Z*+U(la+na));Fa=((Ea|0)==(xa|0)?q:0)+Ea|0;if((xa|0)<(Fa|0)){Ea=xa;do{na=+(c[W+(Ea<<2)>>2]|0);if(ha){xa=Ea+1|0;Ga=+(c[W+(xa<<2)>>2]|0);Ha=xa}else{Ga=0.0;Ha=Ea}oa=na*na+Ga*Ga;if(oa>+h[ua>>3]){h[ua>>3]=oa;h[za>>3]=na;h[ra>>3]=Ga}Ea=Ha+1|0;}while((Ea|0)<(Fa|0))}Aa=Aa+1|0;}while((Aa|0)<(Da|0))}}while(0);if((ya|0)<(ia|0)){va=ya}else{break}}}}while(0);if((pa|0)<(X|0)){ba=pa}else{break}}break};case 5:case 11:{ba=~~K;X=~~I;ia=d;if((X|0)<=0){break L21}Z=+(X|0);ca=+(V|0);da=k-i;aa=+(ga(V,T)|0);W=(ba|0)>0;$=+(ba|0);ea=+(T|0);_=j-f;fa=+(q|0);ha=(q|0)==2;Y=0;while(1){ja=Y+1|0;ta=ca*(M+Q*+(Y|0)/Z-i)/da;sa=ca*(M+Q*+(ja|0)/Z-i)/da;va=ta>sa;la=va?sa:ta;ka=va?ta:sa;L86:do{if(W){va=(ga(Y,t)|0)+R|0;ma=0;while(1){qa=ma+1|0;sa=ea*(L+P*+(ma|0)/$-f)/_;ta=ea*(L+P*+(qa|0)/$-f)/_;Aa=sa>ta;na=Aa?sa:ta;oa=Aa?ta:sa;do{if(na>=0.0&oa<ea){sa=+U(la);ta=oa+ea*sa;if(ta<0.0){break}if(ta>=aa){break L86}Aa=~~sa;ra=~~+U(ka);za=((Aa|0)==(ra|0))+ra|0;sa=+U(ca);if(+(za|0)>sa){Ia=~~sa-1|0}else{Ia=za}if((Aa|0)>=(Ia|0)){break}za=va+ma|0;ra=E+(za*24|0)|0;ua=E+(za*24|0)+8|0;Fa=E+(za*24|0)+16|0;za=Aa;do{sa=ea*+(za|0);Aa=~~(fa*+U(oa+sa));Ea=~~(fa*+U(na+sa));xa=((Ea|0)==(Aa|0)?q:0)+Ea|0;if((Aa|0)<(xa|0)){Ea=Aa;do{sa=+h[ia+(Ea<<3)>>3];if(ha){Aa=Ea+1|0;Ja=+h[ia+(Aa<<3)>>3];Ka=Aa}else{Ja=0.0;Ka=Ea}ta=sa*sa+Ja*Ja;if(ta>+h[ra>>3]){h[ra>>3]=ta;h[ua>>3]=sa;h[Fa>>3]=Ja}Ea=Ka+1|0;}while((Ea|0)<(xa|0))}za=za+1|0;}while((za|0)<(Ia|0))}}while(0);if((qa|0)<(ba|0)){ma=qa}else{break}}}}while(0);if((ja|0)<(X|0)){Y=ja}else{break}}break};case 3:case 9:{Y=~~K;X=~~I;ba=d;if((X|0)<=0){break L21}fa=+(X|0);ea=+(V|0);ca=k-i;aa=+(ga(V,T)|0);ia=(Y|0)>0;_=+(Y|0);$=+(T|0);da=j-f;Z=+(q|0);ha=(q|0)==2;W=0;while(1){pa=W+1|0;ka=ea*(M+Q*+(W|0)/fa-i)/ca;la=ea*(M+Q*+(pa|0)/fa-i)/ca;ma=ka>la;na=ma?la:ka;oa=ma?ka:la;L116:do{if(ia){ma=(ga(W,t)|0)+R|0;va=0;while(1){ya=va+1|0;la=$*(L+P*+(va|0)/_-f)/da;ka=$*(L+P*+(ya|0)/_-f)/da;za=la>ka;sa=za?la:ka;ta=za?ka:la;do{if(sa>=0.0&ta<$){la=+U(na);ka=ta+$*la;if(ka<0.0){break}if(ka>=aa){break L116}za=~~la;Fa=~~+U(oa);ua=((za|0)==(Fa|0))+Fa|0;la=+U(ea);if(+(ua|0)>la){La=~~la-1|0}else{La=ua}if((za|0)>=(La|0)){break}ua=ma+va|0;Fa=E+(ua*24|0)|0;ra=E+(ua*24|0)+8|0;xa=E+(ua*24|0)+16|0;ua=za;do{la=$*+(ua|0);za=~~(Z*+U(ta+la));Ea=~~(Z*+U(sa+la));Aa=((Ea|0)==(za|0)?q:0)+Ea|0;if((za|0)<(Aa|0)){Ea=za;do{za=ba+(Ea<<3)|0;la=+((c[za>>2]|0)>>>0)+ +(c[za+4>>2]|0)*4294967296.0;if(ha){za=Ea+1|0;Ma=ba+(za<<3)|0;Na=+((c[Ma>>2]|0)>>>0)+ +(c[Ma+4>>2]|0)*4294967296.0;Oa=za}else{Na=0.0;Oa=Ea}ka=la*la+Na*Na;if(ka>+h[Fa>>3]){h[Fa>>3]=ka;h[ra>>3]=la;h[xa>>3]=Na}Ea=Oa+1|0;}while((Ea|0)<(Aa|0))}ua=ua+1|0;}while((ua|0)<(La|0))}}while(0);if((ya|0)<(Y|0)){va=ya}else{break}}}}while(0);if((pa|0)<(X|0)){W=pa}else{break}}break};case 4:case 10:{W=~~K;X=~~I;Y=d;if((X|0)<=0){break L21}Z=+(X|0);$=+(V|0);ea=k-i;aa=+(ga(V,T)|0);ba=(W|0)>0;da=+(W|0);_=+(T|0);ca=j-f;fa=+(q|0);ha=(q|0)==2;ia=0;while(1){ja=ia+1|0;oa=$*(M+Q*+(ia|0)/Z-i)/ea;na=$*(M+Q*+(ja|0)/Z-i)/ea;va=oa>na;sa=va?na:oa;ta=va?oa:na;L146:do{if(ba){va=(ga(ia,t)|0)+R|0;ma=0;while(1){qa=ma+1|0;na=_*(L+P*+(ma|0)/da-f)/ca;oa=_*(L+P*+(qa|0)/da-f)/ca;ua=na>oa;la=ua?na:oa;ka=ua?oa:na;do{if(la>=0.0&ka<_){na=+U(sa);oa=ka+_*na;if(oa<0.0){break}if(oa>=aa){break L146}ua=~~na;xa=~~+U(ta);ra=((ua|0)==(xa|0))+xa|0;na=+U($);if(+(ra|0)>na){Pa=~~na-1|0}else{Pa=ra}if((ua|0)>=(Pa|0)){break}ra=va+ma|0;xa=E+(ra*24|0)|0;Fa=E+(ra*24|0)+8|0;Aa=E+(ra*24|0)+16|0;ra=ua;do{na=_*+(ra|0);ua=~~(fa*+U(ka+na));Ea=~~(fa*+U(la+na));za=((Ea|0)==(ua|0)?q:0)+Ea|0;if((ua|0)<(za|0)){Ea=ua;do{na=+g[Y+(Ea<<2)>>2];if(ha){ua=Ea+1|0;Qa=+g[Y+(ua<<2)>>2];Ra=ua}else{Qa=0.0;Ra=Ea}oa=na*na+Qa*Qa;if(oa>+h[xa>>3]){h[xa>>3]=oa;h[Fa>>3]=na;h[Aa>>3]=Qa}Ea=Ra+1|0;}while((Ea|0)<(za|0))}ra=ra+1|0;}while((ra|0)<(Pa|0))}}while(0);if((qa|0)<(W|0)){ma=qa}else{break}}}}while(0);if((ja|0)<(X|0)){ia=ja}else{break}}break};case 0:case 6:{ia=~~K;X=~~I;if((X|0)<=0){break L21}fa=+(X|0);_=+(V|0);$=k-i;aa=+(ga(V,T)|0);W=(ia|0)>0;ca=+(ia|0);da=+(T|0);ea=j-f;Z=+(q|0);Y=(q|0)==2;ha=0;while(1){ba=ha+1|0;ta=_*(M+Q*+(ha|0)/fa-i)/$;sa=_*(M+Q*+(ba|0)/fa-i)/$;pa=ta>sa;la=pa?sa:ta;ka=pa?ta:sa;L176:do{if(W){pa=(ga(ha,t)|0)+R|0;ma=0;while(1){va=ma+1|0;sa=da*(L+P*+(ma|0)/ca-f)/ea;ta=da*(L+P*+(va|0)/ca-f)/ea;ya=sa>ta;na=ya?sa:ta;oa=ya?ta:sa;do{if(na>=0.0&oa<da){sa=+U(la);ta=oa+da*sa;if(ta<0.0){break}if(ta>=aa){break L176}ya=~~sa;ra=~~+U(ka);Aa=((ya|0)==(ra|0))+ra|0;sa=+U(_);if(+(Aa|0)>sa){Sa=~~sa-1|0}else{Sa=Aa}if((ya|0)>=(Sa|0)){break}Aa=pa+ma|0;ra=E+(Aa*24|0)|0;Fa=E+(Aa*24|0)+8|0;xa=E+(Aa*24|0)+16|0;Aa=ya;do{sa=da*+(Aa|0);ya=~~(Z*+U(oa+sa));za=~~(Z*+U(na+sa));Ea=((za|0)==(ya|0)?q:0)+za|0;if((ya|0)<(Ea|0)){za=ya;do{sa=+(a[d+za|0]|0);if(Y){ya=za+1|0;Ta=+(a[d+ya|0]|0);Ua=ya}else{Ta=0.0;Ua=za}ta=sa*sa+Ta*Ta;if(ta>+h[ra>>3]){h[ra>>3]=ta;h[Fa>>3]=sa;h[xa>>3]=Ta}za=Ua+1|0;}while((za|0)<(Ea|0))}Aa=Aa+1|0;}while((Aa|0)<(Sa|0))}}while(0);if((va|0)<(ia|0)){ma=va}else{break}}}}while(0);if((ba|0)<(X|0)){ha=ba}else{break}}break};default:{}}}while(0);if(I<=0.0){break}E=K>0.0;R=S;T=0;while(1){if(E){V=0;P=+h[3790];Q=+h[3789];do{ha=R+(V*24|0)|0;P=+tb(+P,+(+h[ha>>3]));h[3790]=P;Q=+Ib(+Q,+(+h[ha>>3]));h[3789]=Q;V=V+1|0;}while(+(V|0)<K)}V=T+1|0;if(+(V|0)<I){R=R+(t*24|0)|0;T=V}else{break}}}}while(0);e=e+88|0;}while((e|0)!=(c[7569]|0))}if((c[7586]|0)!=1){return}Ta=+h[3807];L=+h[3809];L216:do{if(L-Ta>0.0){e=0;Sa=1;M=Ta;Qa=L;Na=+h[3806];Ja=+h[3808];while(1){if(Ja-Na>0.0){Ua=e;Pa=1;while(1){Ga=+cc(+(+h[(c[7566]|0)+(Ua*24|0)>>3]+1.0e-20))*10.0;h[(c[7566]|0)+(Ua*24|0)>>3]=Ga;Va=Ua+1|0;Wa=+h[3806];Xa=+h[3808];if(+(Pa|0)>=Xa-Wa){break}Ua=Va;Pa=Pa+1|0}Ya=Va;Za=+h[3807];_a=+h[3809];$a=Wa;ab=Xa}else{Ya=e;Za=M;_a=Qa;$a=Na;ab=Ja}if(+(Sa|0)>=_a-Za){break L216}e=Ya;Sa=Sa+1|0;M=Za;Qa=_a;Na=$a;Ja=ab}}}while(0);h[3789]=+cc(+(+h[3789]+1.0e-20))*10.0;h[3790]=+cc(+(+h[3790]+1.0e-20))*10.0;h[3791]=+cc(+(+h[3791]+1.0e-20))*10.0;return}function ae(b,f,g,j){b=b|0;f=f|0;g=g|0;j=j|0;var k=0,l=0,m=0,n=0,o=0,p=0.0,q=0.0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0;k=i;i=i+32|0;l=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[l>>2];l=g;g=i;i=i+32|0;io(g,l,32)|0;l=k|0;m=k+8|0;n=k+16|0;o=k+24|0;p=+h[g>>3];q=+h[g+8>>3];r=~~(+h[g+16>>3]-p);s=~~(+h[g+24>>3]-q);g=(r|0)>(s|0)?s:r;t=g>>>1&32767;if((j&65535)>>>0>t>>>0){u=t+(g&1)&65535}else{u=j}j=u&65535;if(u<<16>>16==0){i=k;return}u=f;f=m;g=l;t=n;v=o;w=b+24|0;x=b+32|0;y=b+40|0;z=b+48|0;A=b|0;B=b+8|0;D=b+16|0;E=s;s=0;F=r;r=~~q;G=~~p;while(1){H=G&65535;I=F+65535+G&65535;J=r&65535;K=r+E|0;if((F|0)>1){L=d[u]|d[u+1|0]<<8|d[u+2|0]<<16|d[u+3|0]<<24|0;c[m>>2]=L;Yd(b,f,H,I,J,-1);c[l>>2]=L;Yd(b,g,I,H,K+65535&65535,-1);}do{if((E|0)>2){L=d[u]|d[u+1|0]<<8|d[u+2|0]<<16|d[u+3|0]<<24|0;M=r+1&65535;N=K+65534&65535;c[n>>2]=L;ce(b,t,I,M,N,-1);c[o>>2]=L;ce(b,v,H,M,N,-1);}else{if(!((E|0)==1&(F|0)==1)){break}p=+((H&65535)>>>0);q=+((J&65535)>>>0);if(!(+h[z>>3]>q&((+h[y>>3]<=p|+h[w>>3]>p|+h[x>>3]>q)^1))){break}N=d[u]|d[u+1|0]<<8|d[u+2|0]<<16|d[u+3|0]<<24|0;M=(c[A>>2]|0)+((ga(e[B>>1]|0,r&65535)|0)+(G&65535)<<2)|0;if((a[D]&1)==0){C=N;a[M]=C;C=C>>8;a[M+1|0]=C;C=C>>8;a[M+2|0]=C;C=C>>8;a[M+3|0]=C;break}else{L=d[M]|d[M+1|0]<<8|d[M+2|0]<<16|d[M+3|0]<<24|0;O=N>>>24;P=O^255;C=((ga(L>>>24,P)|0)>>>8)+O<<24|(ga(L>>>8&255,P)|0)+(N&65280)&65280|((ga(L&255,P)|0)>>>8)+N&255|((ga(L>>>16&255,P)|0)>>>8<<16)+N&16711680;a[M]=C;C=C>>8;a[M+1|0]=C;C=C>>8;a[M+2|0]=C;C=C>>8;a[M+3|0]=C;break}}}while(0);J=s+1|0;if(J>>>0<j>>>0){E=E-2|0;s=J;F=F-2|0;r=r+1|0;G=G+1|0}else{break}}i=k;return}function be(a,d){a=a|0;d=d|0;var f=0,g=0.0,i=0,j=0.0,k=0,l=0,m=0.0,n=0.0,o=0,p=0,q=0,r=0.0,s=0.0,t=0,u=0,v=0,w=0,x=0.0,y=0.0,z=0.0,A=0.0,B=0.0,C=0.0,D=0.0,E=0.0,F=0.0,G=0.0,H=0.0,I=0.0,J=0.0,K=0.0,L=0.0,M=0.0,N=0.0,O=0.0,P=0.0,Q=0.0,R=0.0,S=0.0,T=0.0,U=0.0,V=0,W=0,X=0,Y=0;f=d|0;g=+h[f>>3];i=d+8|0;j=+h[i>>3];k=d+16|0;l=d+24|0;m=g+(+h[k>>3]-g);n=j+1.0;d=a+68|0;o=c[d>>2]|0;p=a+64|0;if((o|0)==(c[p>>2]|0)){c[p>>2]=o<<1;q=a+60|0;c[q>>2]=On(c[q>>2]|0,o<<6)|0}o=a+72|0;if((c[o>>2]|0)==0){r=0.0;s=0.0}else{r=+(b[a+58>>1]|0);s=+(b[a+56>>1]|0)}q=c[d>>2]|0;c[d>>2]=q+1;t=a+60|0;u=c[t>>2]|0;v=a+4|0;w=a+6|0;x=s+ +((e[v>>1]|0)>>>0);y=r+ +((e[w>>1]|0)>>>0);if(m<s|x<g|n<r|y<j){z=0.0;A=0.0;B=0.0;C=0.0}else{z=g>s?g:s;A=j>r?j:r;B=m<x?m:x;C=n<y?n:y}h[u+(q<<5)>>3]=z;h[u+(q<<5)+8>>3]=A;h[u+(q<<5)+16>>3]=B;h[u+(q<<5)+24>>3]=C;C=+h[f>>3];B=+h[i>>3];A=C+1.0;z=B+(+h[l>>3]-B);q=c[d>>2]|0;if((q|0)==(c[p>>2]|0)){c[p>>2]=q<<1;c[t>>2]=On(c[t>>2]|0,q<<6)|0}if((c[o>>2]|0)==0){D=0.0;E=0.0}else{D=+(b[a+58>>1]|0);E=+(b[a+56>>1]|0)}q=c[d>>2]|0;c[d>>2]=q+1;u=c[t>>2]|0;y=E+ +((e[v>>1]|0)>>>0);n=D+ +((e[w>>1]|0)>>>0);if(A<E|y<C|z<D|n<B){F=0.0;G=0.0;H=0.0;I=0.0}else{F=C>E?C:E;G=B>D?B:D;H=A<y?A:y;I=z<n?z:n}h[u+(q<<5)>>3]=F;h[u+(q<<5)+8>>3]=G;h[u+(q<<5)+16>>3]=H;h[u+(q<<5)+24>>3]=I;I=+h[f>>3];H=I+(+h[k>>3]-I)+-1.0;I=+h[i>>3];G=H+1.0;F=I+(+h[l>>3]-I);q=c[d>>2]|0;if((q|0)==(c[p>>2]|0)){c[p>>2]=q<<1;c[t>>2]=On(c[t>>2]|0,q<<6)|0}if((c[o>>2]|0)==0){J=0.0;K=0.0}else{J=+(b[a+58>>1]|0);K=+(b[a+56>>1]|0)}q=c[d>>2]|0;c[d>>2]=q+1;u=c[t>>2]|0;n=K+ +((e[v>>1]|0)>>>0);z=J+ +((e[w>>1]|0)>>>0);if(G<K|n<H|F<J|z<I){L=0.0;M=0.0;N=0.0;O=0.0}else{L=H>K?H:K;M=I>J?I:J;N=G<n?G:n;O=F<z?F:z}h[u+(q<<5)>>3]=L;h[u+(q<<5)+8>>3]=M;h[u+(q<<5)+16>>3]=N;h[u+(q<<5)+24>>3]=O;O=+h[f>>3];N=+h[i>>3];M=N+(+h[l>>3]-N)+-1.0;N=O+(+h[k>>3]-O);L=M+1.0;k=c[d>>2]|0;if((k|0)==(c[p>>2]|0)){c[p>>2]=k<<1;c[t>>2]=On(c[t>>2]|0,k<<6)|0}if((c[o>>2]|0)==0){P=0.0;Q=0.0}else{P=+(b[a+58>>1]|0);Q=+(b[a+56>>1]|0)}a=c[d>>2]|0;c[d>>2]=a+1;d=c[t>>2]|0;z=Q+ +((e[v>>1]|0)>>>0);F=P+ +((e[w>>1]|0)>>>0);if(N<Q|z<O|L<P|F<M){R=0.0;S=0.0;T=0.0;U=0.0;V=d+(a<<5)|0;h[V>>3]=R;W=d+(a<<5)+8|0;h[W>>3]=S;X=d+(a<<5)+16|0;h[X>>3]=T;Y=d+(a<<5)+24|0;h[Y>>3]=U;return}R=O>Q?O:Q;S=M>P?M:P;T=N<z?N:z;U=L<F?L:F;V=d+(a<<5)|0;h[V>>3]=R;W=d+(a<<5)+8|0;h[W>>3]=S;X=d+(a<<5)+16|0;h[X>>3]=T;Y=d+(a<<5)+24|0;h[Y>>3]=U;return}function ce(b,f,g,j,k,l){b=b|0;f=f|0;g=g|0;j=j|0;k=k|0;l=l|0;var m=0,n=0,o=0.0,p=0.0,q=0.0,r=0.0,s=0,t=0,u=0.0,v=0.0,w=0,x=0,y=0,z=0,A=0,B=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0;m=i;n=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[n>>2];n=j<<16>>16>k<<16>>16;o=+h[b+24>>3];p=+h[b+40>>3];q=+(g<<16>>16|0);r=+(~~(q<o?o:q)|0);s=~~(r>p?p:r);t=s&65535;r=+((g<<16>>16)+1|0);q=+(~~(r<o?o:r)|0);r=+h[b+32>>3];o=+h[b+48>>3];u=+((n?k:j)<<16>>16|0);v=+(~~(u<r?r:u)|0);g=~~(v>o?o:v);v=+(((n?j:k)<<16>>16)+1|0);u=+(~~(v<r?r:v)|0);k=g&65535;j=~~(u>o?o:u)&65535;if(k>>>0>=j>>>0){i=m;return}n=l<<16>>16==-1;w=l&65535;l=s&65535;s=~~(q>p?p:q)&65535;x=l>>>0<s>>>0;y=f;f=b|0;z=b+8|0;A=b+16|0;b=g&65535;g=k;do{if(n){if(x){B=6}}else{if(!((1<<(g&15)&w|0)==0|x^1)){B=6}}if((B|0)==6){B=0;k=d[y]|d[y+1|0]<<8|d[y+2|0]<<16|d[y+3|0]<<24|0;D=k&65280;E=k>>>24;F=E^255;G=t;H=l;do{I=(c[f>>2]|0)+((ga(e[z>>1]|0,g)|0)+H<<2)|0;if((a[A]&1)==0){J=k}else{K=d[I]|d[I+1|0]<<8|d[I+2|0]<<16|d[I+3|0]<<24|0;J=((ga(K>>>24,F)|0)>>>8)+E<<24|(ga(K>>>8&255,F)|0)+D&65280|((ga(K&255,F)|0)>>>8)+k&255|((ga(K>>>16&255,F)|0)>>>8<<16)+k&16711680}C=J;a[I]=C;C=C>>8;a[I+1|0]=C;C=C>>8;a[I+2|0]=C;C=C>>8;a[I+3|0]=C;G=G+1&65535;H=G&65535;}while(H>>>0<s>>>0)}b=b+1&65535;g=b&65535;}while(g>>>0<j>>>0);i=m;return}function de(d,f){d=d|0;f=f|0;var g=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0.0,q=0.0,r=0.0,s=0.0,t=0,u=0.0,v=0,x=0.0,y=0,z=0.0,A=0,B=0.0,D=0.0,E=0.0,F=0.0,G=0.0,H=0.0,I=0.0,J=0,K=0,L=0,M=0,N=0,O=0,P=0.0,Q=0.0,R=0.0,S=0.0,T=0.0,U=0.0,V=0.0,W=0.0,X=0.0,Y=0.0,Z=0,_=0,$=0,aa=0;g=i;i=i+1064|0;j=g|0;k=g+8|0;l=g+1032|0;m=g+1040|0;n=g+1048|0;o=c[7517]|0;p=+h[3823];q=+h[3824];r=+h[3825];s=+h[3826];t=o+24|0;u=+h[t>>3];v=o+32|0;x=+h[v>>3];y=o+40|0;z=+h[y>>3];A=o+48|0;B=+h[A>>3];D=+((e[o+4>>1]|0)>>>0);E=+((e[o+6>>1]|0)>>>0);if(r<0.0|D<p|s<0.0|E<q){F=0.0;G=0.0;H=0.0;I=0.0}else{F=p>0.0?p:0.0;G=q>0.0?q:0.0;H=r<D?r:D;I=s<E?s:E}h[t>>3]=F;h[v>>3]=G;h[y>>3]=H;h[A>>3]=I;I=+h[3806];H=+h[3807];G=+h[3802];F=+h[3803];E=G+(+(d<<16>>16|0)-I)*(+h[3804]-G)/(+h[3808]-I);I=F+(+(f<<16>>16|0)-H)*(+h[3805]-F)/(+h[3809]-H);f=k|0;d=Fd(b[15142]|0)|0;A=Fd(b[15143]|0)|0;if((b[15143]|0)==1){y=nb(f|0,3712,(J=i,i=i+16|0,h[J>>3]=E,c[J+8>>2]=d,J)|0)|0;i=J;H=+Za(+I,l|0);c[m>>2]=~~(+h[l>>3]+-631152.0e3);l=(Lb(k+y|0,1024-y|0,3632,wb(m|0)|0)|0)+y|0;nb(n|0,3608,(J=i,i=i+8|0,h[J>>3]=H,J)|0)|0;i=J;y=n+1|0;n=ho(y|0)|0;io(k+l|0,y|0,n+1|0)|0;K=n+l|0}else{l=nb(f|0,3568,(J=i,i=i+32|0,h[J>>3]=E,c[J+8>>2]=d,h[J+16>>3]=I,c[J+24>>2]=A,J)|0)|0;i=J;K=l}l=k+K|0;H=+h[3851];do{if(H<w){F=+h[3852];if(F>=w){break}nb(l|0,3520,(J=i,i=i+32|0,h[J>>3]=E-H,c[J+8>>2]=d,h[J+16>>3]=I-F,c[J+24>>2]=A,J)|0)|0;i=J}}while(0);J=~~(+h[3823]+5.0);A=~~(+h[3824]+5.0);d=ho(f|0)|0;if((d|0)==0){L=32767;M=0;N=0}else{l=0;K=32767;n=0;y=0;while(1){m=(a[k+l|0]|0)-32|0;v=b[28204+(m<<4)>>1]|0;t=(b[28208+(m<<4)>>1]|0)+y&65535;o=v-(b[28202+(m<<4)>>1]|0)&65535;m=v<<16>>16>n<<16>>16?v:n;v=o<<16>>16<K<<16>>16?o:K;o=l+1|0;if(o>>>0<d>>>0){l=o;K=v;n=m;y=t}else{L=v;M=m;N=t;break}}}y=M-L&65535;L=c[7517]|0;I=+((J&65535)>>>0);H=+((A&65535)>>>0);E=+h[L+24>>3];F=+h[L+40>>3];G=+(~~I|0);s=+(~~(G<E?E:G)|0);M=~~(s>F?F:s);n=M&65535;s=+(~~(I+ +(b[17360]|0))|0);I=+(~~(s<E?E:s)|0);s=+h[L+32>>3];E=+h[L+48>>3];G=+(~~H|0);D=+(~~(G<s?s:G)|0);K=~~(D>E?E:D);D=+(~~(H+ +(b[17364]|0))|0);H=+(~~(D<s?s:D)|0);l=~~(H>E?E:H)&65535;if((K&65535)>>>0<l>>>0){d=~~(I>F?F:I)&65535;k=(M&65535)>>>0<d>>>0;M=L|0;t=L+8|0;m=K&65535;do{if(k){K=m&65535;v=n;do{o=(c[M>>2]|0)+((ga(e[t>>1]|0,K)|0)+(v&65535)<<2)|0;C=-16777216;a[o]=C;C=C>>8;a[o+1|0]=C;C=C>>8;a[o+2|0]=C;C=C>>8;a[o+3|0]=C;v=v+1&65535;}while((v&65535)>>>0<d>>>0)}m=m+1&65535;}while((m&65535)>>>0<l>>>0);O=c[7517]|0}else{O=L}c[j>>2]=-1;Xd(28200,O,j,J,A,f,ho(f|0)|0);f=c[7517]|0;I=+h[3823];F=+h[3824];H=+h[3825];E=+h[3826];A=f+68|0;J=c[A>>2]|0;j=f+64|0;if((J|0)==(c[j>>2]|0)){c[j>>2]=J<<1;j=f+60|0;c[j>>2]=On(c[j>>2]|0,J<<6)|0}if((c[f+72>>2]|0)==0){P=0.0;Q=0.0}else{P=+(b[f+58>>1]|0);Q=+(b[f+56>>1]|0)}J=c[A>>2]|0;c[A>>2]=J+1;A=c[f+60>>2]|0;D=Q+ +((e[f+4>>1]|0)>>>0);s=P+ +((e[f+6>>1]|0)>>>0);if(H<Q|D<I|E<P|s<F){R=0.0;S=0.0;T=0.0;U=0.0}else{R=I>Q?I:Q;S=F>P?F:P;T=H<D?H:D;U=E<s?E:s}h[A+(J<<5)>>3]=R;h[A+(J<<5)+8>>3]=S;h[A+(J<<5)+16>>3]=T;h[A+(J<<5)+24>>3]=U;b[17360]=N;b[17364]=y;y=c[7517]|0;U=+((e[y+4>>1]|0)>>>0);T=+((e[y+6>>1]|0)>>>0);if(z<0.0|U<u|B<0.0|T<x){V=0.0;W=0.0;X=0.0;Y=0.0;Z=y+24|0;h[Z>>3]=V;_=y+32|0;h[_>>3]=W;$=y+40|0;h[$>>3]=X;aa=y+48|0;h[aa>>3]=Y;i=g;return}V=u>0.0?u:0.0;W=x>0.0?x:0.0;X=z<U?z:U;Y=B<T?B:T;Z=y+24|0;h[Z>>3]=V;_=y+32|0;h[_>>3]=W;$=y+40|0;h[$>>3]=X;aa=y+48|0;h[aa>>3]=Y;i=g;return}function ee(d,f){d=+d;f=+f;var g=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0.0,y=0.0,z=0.0,A=0.0,B=0.0,C=0.0,D=0.0,E=0.0,F=0.0,G=0.0,H=0.0,I=0.0,J=0,K=0,L=0,M=0.0,N=0.0,O=0.0,P=0.0,Q=0.0,R=0.0,S=0.0,T=0.0,U=0.0,V=0.0,W=0.0,X=0.0,Y=0.0,Z=0.0,_=0.0,$=0.0,aa=0.0,ba=0,ca=0,da=0,ea=0,fa=0,ga=0,ha=0,ia=0,ja=0,ka=0,la=0,ma=0,na=0,oa=0.0,pa=0.0,qa=0.0,ra=0.0,sa=0.0,ta=0.0,ua=0.0,va=0.0,wa=0,xa=0;g=i;i=i+104|0;j=g|0;k=g+32|0;l=g+64|0;m=g+96|0;n=m;o=i;i=i+32|0;p=i;i=i+4|0;i=i+7&-8;q=p;r=i;i=i+32|0;s=i;i=i+4|0;i=i+7&-8;t=s;u=i;i=i+32|0;v=i;i=i+4|0;i=i+7&-8;w=v;h[3860]=d;h[3861]=f;x=+h[3786];y=+h[3787];if((a[31104]&1)==0){z=0.0;A=0.0;B=0.0}else{c[7729]=Fd(b[15142]|0)|0;C=+h[3802];D=C+(+h[3804]-C);E=y-(y-x);h[j>>3]=C;h[j+8>>3]=y;h[j+16>>3]=D;h[j+24>>3]=E;fe(30896,j);j=c[7517]|0;F=+h[3815];G=+h[3816];H=+h[3817];I=+h[3818];J=j+68|0;K=c[J>>2]|0;L=j+64|0;if((K|0)==(c[L>>2]|0)){c[L>>2]=K<<1;L=j+60|0;c[L>>2]=On(c[L>>2]|0,K<<6)|0}if((c[j+72>>2]|0)==0){M=0.0;N=0.0}else{M=+(b[j+58>>1]|0);N=+(b[j+56>>1]|0)}K=c[J>>2]|0;c[J>>2]=K+1;J=c[j+60>>2]|0;O=N+ +((e[j+4>>1]|0)>>>0);P=M+ +((e[j+6>>1]|0)>>>0);if(H<N|O<F|I<M|P<G){Q=0.0;R=0.0;S=0.0;T=0.0}else{Q=F>N?F:N;R=G>M?G:M;S=H<O?H:O;T=I<P?I:P}h[J+(K<<5)>>3]=Q;h[J+(K<<5)+8>>3]=R;h[J+(K<<5)+16>>3]=S;h[J+(K<<5)+24>>3]=T;z=C;A=D;B=E}if((a[31105]&1)==0){U=0.0;V=0.0;W=0.0}else{c[7756]=Fd(b[15143]|0)|0;E=+h[3803];D=x+(y-x);C=E+(+h[3805]-E);h[k>>3]=x;h[k+8>>3]=E;h[k+16>>3]=D;h[k+24>>3]=C;fe(31e3,k);k=c[7517]|0;T=+h[3819];S=+h[3820];R=+h[3821];Q=+h[3822];K=k+68|0;J=c[K>>2]|0;j=k+64|0;if((J|0)==(c[j>>2]|0)){c[j>>2]=J<<1;j=k+60|0;c[j>>2]=On(c[j>>2]|0,J<<6)|0}if((c[k+72>>2]|0)==0){X=0.0;Y=0.0}else{X=+(b[k+58>>1]|0);Y=+(b[k+56>>1]|0)}J=c[K>>2]|0;c[K>>2]=J+1;K=c[k+60>>2]|0;P=Y+ +((e[k+4>>1]|0)>>>0);I=X+ +((e[k+6>>1]|0)>>>0);if(R<Y|P<T|Q<X|I<S){Z=0.0;_=0.0;$=0.0;aa=0.0}else{Z=T>Y?T:Y;_=S>X?S:X;$=R<P?R:P;aa=Q<I?Q:I}h[K+(J<<5)>>3]=Z;h[K+(J<<5)+8>>3]=_;h[K+(J<<5)+16>>3]=$;h[K+(J<<5)+24>>3]=aa;U=E;V=D;W=C}C=d- +h[3806];d=f- +h[3807];if(C<0.0|d<0.0){i=g;return}J=c[7568]|0;if((J|0)==(c[7569]|0)){i=g;return}K=o|0;k=o+8|0;j=o+16|0;L=o+24|0;ba=l|0;ca=l+8|0;da=l+16|0;ea=l+24|0;fa=r|0;ga=r+8|0;ha=r+16|0;ia=r+24|0;ja=u|0;ka=u+8|0;la=u+16|0;ma=u+24|0;na=J;do{f=+h[na+8>>3];D=+h[na+16>>3];E=+h[na+24>>3];aa=+h[na+32>>3];$=+h[3802];_=+h[3803];Z=+h[3804];I=+h[3805];if(E<$|Z<f|aa<_|I<D){oa=0.0;pa=0.0;qa=0.0;ra=0.0;sa=0.0;ta=0.0;ua=0.0;va=0.0}else{oa=f>$?f:$;pa=D>_?D:_;qa=E<Z?E:Z;ra=aa<I?aa:I;sa=aa<I?aa:I;ta=E<Z?E:Z;ua=D>_?D:_;va=f>$?f:$}f=+h[3806];J=~~f;D=+h[3808];E=Z-$;Z=+((~~D<<16>>16)-(J<<16>>16)|0);aa=+(J<<16>>16|0);Q=+(~~(+Gb(+(aa+(oa-$)/E*Z))-f)<<16>>16|0);P=+(~~(+Gb(+(aa+(qa-$)/E*Z))-f)<<16>>16|0);Z=+h[3807];J=~~Z;E=I-_;I=+((~~+h[3809]<<16>>16)-(J<<16>>16)|0);$=+(J<<16>>16|0);aa=+(~~(+Gb(+($+(pa-_)/E*I))-Z)<<16>>16|0);R=+(~~(+Gb(+($+(ra-_)/E*I))-Z)<<16>>16|0);J=P>C;wa=aa<=d;xa=R>d;do{if(Q<=C&J&wa&xa){if((a[31104]&1)!=0){h[ba>>3]=z;h[ca>>3]=y;h[da>>3]=A;h[ea>>3]=B;c[m>>2]=-3368449;Z=P-Q;ge(l,n,(c[7566]|0)+(~~(Q+d*(D-f))*24|0)|0,~~Z,va,(ta-va)/Z)}if((a[31105]&1)==0){break}h[K>>3]=x;h[k>>3]=U;h[j>>3]=V;h[L>>3]=W;c[p>>2]=-3368449;Z=+h[3808]- +h[3806];I=R-aa;he(o,q,(c[7566]|0)+(~~(C+aa*Z)*24|0)|0,~~(Z*24.0*.125),~~I,ua,(sa-ua)/I)}else{if(wa&xa){if((a[31104]&1)==0){break}h[fa>>3]=z;h[ga>>3]=y;h[ha>>3]=A;h[ia>>3]=B;c[s>>2]=-26215;I=P-Q;ge(r,t,(c[7566]|0)+(~~(Q+d*(D-f))*24|0)|0,~~I,va,(ta-va)/I);break}if(Q>C|J^1){break}if((a[31105]&1)==0){break}h[ja>>3]=x;h[ka>>3]=U;h[la>>3]=V;h[ma>>3]=W;c[v>>2]=-26215;I=D-f;Z=R-aa;he(u,w,(c[7566]|0)+(~~(C+aa*I)*24|0)|0,~~(I*24.0*.125),~~Z,ua,(sa-ua)/Z)}}while(0);na=na+88|0;}while((na|0)!=(c[7569]|0));i=g;return}function fe(b,f){b=b|0;f=f|0;var g=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0.0,t=0,u=0,v=0.0,w=0,x=0,y=0,z=0,A=0.0,B=0,D=0.0,E=0.0,F=0.0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0.0,S=0.0,T=0.0,U=0.0,V=0.0,W=0.0,X=0.0,Y=0.0,Z=0,_=0,$=0,aa=0.0,ba=0.0,ca=0.0,da=0.0;g=i;i=i+8|0;j=f;f=i;i=i+32|0;io(f,j,32)|0;j=g|0;k=j;l=i;i=i+32|0;m=i;i=i+32|0;n=i;i=i+32|0;o=i;i=i+32|0;p=i;i=i+32|0;q=c[b>>2]|0;r=b+32|0;s=+h[r>>3];t=~~s;u=b+40|0;v=+h[u>>3];w=~~v;x=b+48|0;y=b+56|0;z=q+24|0;A=+h[z>>3];B=q+40|0;D=+h[B>>3];E=+(t|0);F=+(~~(E<A?A:E)|0);G=~~(F>D?D:F);F=+(~~(+h[x>>3]-s)+t|0);s=+(~~(F<A?A:F)|0);H=~~(s>D?D:s);I=q+32|0;s=+h[I>>3];J=q+48|0;D=+h[J>>3];F=+(w|0);A=+(~~(F<s?s:F)|0);K=~~(A>D?D:A);A=+(~~(+h[y>>3]-v)+w|0);v=+(~~(A<s?s:A)|0);w=~~(v>D?D:v);do{if((K|0)<(w|0)){L=q|0;M=q+8|0;if((G|0)>=(H|0)){break}N=H-G<<2;O=K;P=0;while(1){ko((c[L>>2]|0)+(t+(ga(K+P|0,e[M>>1]|0)|0)<<2)|0,0,N|0)|0;Q=O+1|0;if((Q|0)<(w|0)){O=Q;P=P+1|0}else{break}}}}while(0);w=b+8|0;c[j>>2]=d[w]|d[w+1|0]<<8|d[w+2|0]<<16|d[w+3|0]<<24;w=b+64|0;j=b+64|0;K=b+72|0;t=b+80|0;G=b+88|0;H=b+96|0;P=c[H>>2]|0;v=+(P>>>0>>>0);D=+h[j>>3]-v;A=+h[K>>3]-v;s=+h[t>>3]+v;F=+h[G>>3]+v;h[l>>3]=+Ib(+D,+s);h[l+8>>3]=+Ib(+A,+F);h[l+16>>3]=+tb(+D,+s);h[l+24>>3]=+tb(+A,+F);ae(q,k,l,P&65535);F=+h[r>>3];A=+h[u>>3];s=+h[x>>3];D=+h[y>>3];v=+h[z>>3];E=+h[I>>3];R=+h[B>>3];S=+h[J>>3];y=q+4|0;T=+((e[y>>1]|0)>>>0);x=q+6|0;U=+((e[x>>1]|0)>>>0);if(s<0.0|T<F|D<0.0|U<A){V=0.0;W=0.0;X=0.0;Y=0.0}else{V=F>0.0?F:0.0;W=A>0.0?A:0.0;X=s<T?s:T;Y=D<U?D:U}h[z>>3]=V;h[I>>3]=W;h[B>>3]=X;h[J>>3]=Y;u=b+4|0;r=d[u]|d[u+1|0]<<8|d[u+2|0]<<16|d[u+3|0]<<24|0;U=+(~~+h[j>>3]|0);D=+(~~(U<V?V:U)|0);j=~~(D>X?X:D);u=j&65535;D=+(~~+h[t>>3]|0);U=+(~~(D<V?V:D)|0);D=+(~~+h[K>>3]|0);V=+(~~(D<W?W:D)|0);K=~~(V>Y?Y:V);V=+(~~+h[G>>3]|0);D=+(~~(V<W?W:V)|0);G=~~(D>Y?Y:D)&65535;if((K&65535)>>>0<G>>>0){t=~~(U>X?X:U)&65535;P=(j&65535)>>>0<t>>>0;j=q|0;l=q+8|0;k=q+16|0;O=r&65280;N=r>>>24;M=N^255;L=K&65535;do{if(P){K=L&65535;Q=u;do{Z=(c[j>>2]|0)+((ga(e[l>>1]|0,K)|0)+(Q&65535)<<2)|0;if((a[k]&1)==0){_=r}else{$=d[Z]|d[Z+1|0]<<8|d[Z+2|0]<<16|d[Z+3|0]<<24|0;_=((ga($>>>24,M)|0)>>>8)+N<<24|(ga($>>>8&255,M)|0)+O&65280|((ga($&255,M)|0)>>>8)+r&255|((ga($>>>16&255,M)|0)>>>8<<16)+r&16711680}C=_;a[Z]=C;C=C>>8;a[Z+1|0]=C;C=C>>8;a[Z+2|0]=C;C=C>>8;a[Z+3|0]=C;Q=Q+1&65535;}while((Q&65535)>>>0<t>>>0)}L=L+1&65535;}while((L&65535)>>>0<G>>>0)}G=m;c[G>>2]=c[w>>2];c[G+4>>2]=c[w+4>>2];c[G+8>>2]=c[w+8>>2];c[G+12>>2]=c[w+12>>2];c[G+16>>2]=c[w+16>>2];c[G+20>>2]=c[w+20>>2];c[G+24>>2]=c[w+24>>2];c[G+28>>2]=c[w+28>>2];G=n;L=f;c[G>>2]=c[L>>2];c[G+4>>2]=c[L+4>>2];c[G+8>>2]=c[L+8>>2];c[G+12>>2]=c[L+12>>2];c[G+16>>2]=c[L+16>>2];c[G+20>>2]=c[L+20>>2];c[G+24>>2]=c[L+24>>2];c[G+28>>2]=c[L+28>>2];ie(q,m,n,c[H>>2]|0,c[b+20>>2]|0,1,1);n=o;c[n>>2]=c[w>>2];c[n+4>>2]=c[w+4>>2];c[n+8>>2]=c[w+8>>2];c[n+12>>2]=c[w+12>>2];c[n+16>>2]=c[w+16>>2];c[n+20>>2]=c[w+20>>2];c[n+24>>2]=c[w+24>>2];c[n+28>>2]=c[w+28>>2];w=p;c[w>>2]=c[L>>2];c[w+4>>2]=c[L+4>>2];c[w+8>>2]=c[L+8>>2];c[w+12>>2]=c[L+12>>2];c[w+16>>2]=c[L+16>>2];c[w+20>>2]=c[L+20>>2];c[w+24>>2]=c[L+24>>2];c[w+28>>2]=c[L+28>>2];je(q,o,p,c[H>>2]|0,c[b+24>>2]|0,1,1);U=+((e[y>>1]|0)>>>0);X=+((e[x>>1]|0)>>>0);if(R<0.0|U<v|S<0.0|X<E){aa=0.0;ba=0.0;ca=0.0;da=0.0;h[z>>3]=aa;h[I>>3]=ba;h[B>>3]=ca;h[J>>3]=da;i=g;return}aa=v>0.0?v:0.0;ba=E>0.0?E:0.0;ca=R<U?R:U;da=S<X?S:X;h[z>>3]=aa;h[I>>3]=ba;h[B>>3]=ca;h[J>>3]=da;i=g;return}function ge(a,b,f,g,j,k){a=a|0;b=b|0;f=f|0;g=g|0;j=+j;k=+k;var l=0,m=0,n=0,o=0,p=0,q=0.0,r=0.0,s=0.0,t=0.0,u=0,v=0,w=0.0,x=0,y=0.0,z=0,A=0.0,B=0,C=0.0,D=0.0,E=0.0,F=0.0,G=0.0,H=0.0,I=0.0,J=0,K=0,L=0,M=0,N=0,O=0.0,P=0,Q=0.0,R=0.0,S=0.0,T=0,U=0.0,V=0.0,W=0.0,X=0.0,Y=0.0,Z=0.0,_=0,$=0.0,aa=0.0,ba=0.0,ca=0,da=0.0,ea=0,fa=0.0,ga=0.0,ha=0.0,ia=0.0,ja=0.0,ka=0,la=0,ma=0,na=0;l=i;i=i+72|0;m=a;a=i;i=i+32|0;io(a,m,32)|0;m=b;b=i;i=i+4|0;i=i+7&-8;c[b>>2]=c[m>>2];m=l|0;n=l+32|0;o=l+64|0;p=o;q=+h[3870];r=+h[3871];s=+h[3872];t=+h[3873];u=c[7724]|0;v=u+24|0;w=+h[v>>3];x=u+32|0;y=+h[x>>3];z=u+40|0;A=+h[z>>3];B=u+48|0;C=+h[B>>3];D=+((e[u+4>>1]|0)>>>0);E=+((e[u+6>>1]|0)>>>0);if(s<0.0|D<q|t<0.0|E<r){F=0.0;G=0.0;H=0.0;I=0.0}else{F=q>0.0?q:0.0;G=r>0.0?r:0.0;H=s<D?s:D;I=t<E?t:E}h[v>>3]=F;h[x>>3]=G;h[z>>3]=H;h[B>>3]=I;B=g-1|0;L4:do{if((B|0)>0){I=+h[a>>3];H=+h[a+8>>3];G=s-q;F=+h[a+16>>3]-I;E=t-r;D=+h[a+24>>3]-H;g=m|0;z=m+8|0;x=m+16|0;v=m+24|0;u=n|0;J=n+8|0;K=n+16|0;L=n+24|0;M=b;N=0;O=j;P=1;while(1){Q=q+G*(O-I)/F;R=r+E*(+h[f+(N<<3)>>3]-H)/D;S=O+k;T=N+3|0;U=q+G*(S-I)/F-Q;V=r+E*(+h[f+(T<<3)>>3]-H)/D-R;W=-0.0-U;h[g>>3]=W;h[z>>3]=U;h[x>>3]=-0.0-V;h[v>>3]=V;X=Q-q;h[u>>3]=X;h[J>>3]=s-Q;h[K>>3]=R-r;h[L>>3]=t-R;Y=0.0;Z=1.0;_=1;$=W;W=X;L8:while(1){do{if($==0.0){if(W<0.0){break L8}else{aa=Z;ba=Y}}else{X=W/$;if($<0.0&X>Z){break L8}ca=$>0.0;if(ca&X<Y){break L8}if($>0.0){da=Y}else{da=+tb(+X,+Y)}if(!ca){aa=Z;ba=da;break}aa=+Ib(+X,+Z);ba=da}}while(0);if(_>>>0>=4>>>0){ea=16;break}X=+h[m+(_<<3)>>3];fa=+h[n+(_<<3)>>3];Y=ba;Z=aa;_=_+1|0;$=X;W=fa}if((ea|0)==16){ea=0;_=c[7724]|0;c[o>>2]=d[M]|d[M+1|0]<<8|d[M+2|0]<<16|d[M+3|0]<<24;Wd(_,p,Q+U*ba,R+V*ba,Q+U*aa,R+V*aa,1.0)}if((P|0)>=(B|0)){break L4}N=T;O=S;P=P+1|0}}}while(0);B=c[7724]|0;aa=+((e[B+4>>1]|0)>>>0);ba=+((e[B+6>>1]|0)>>>0);if(A<0.0|aa<w|C<0.0|ba<y){ga=0.0;ha=0.0;ia=0.0;ja=0.0;ka=B+24|0;h[ka>>3]=ga;la=B+32|0;h[la>>3]=ha;ma=B+40|0;h[ma>>3]=ia;na=B+48|0;h[na>>3]=ja;i=l;return}ga=w>0.0?w:0.0;ha=y>0.0?y:0.0;ia=A<aa?A:aa;ja=C<ba?C:ba;ka=B+24|0;h[ka>>3]=ga;la=B+32|0;h[la>>3]=ha;ma=B+40|0;h[ma>>3]=ia;na=B+48|0;h[na>>3]=ja;i=l;return}function he(a,b,f,g,j,k,l){a=a|0;b=b|0;f=f|0;g=g|0;j=j|0;k=+k;l=+l;var m=0,n=0,o=0,p=0,q=0,r=0.0,s=0.0,t=0.0,u=0.0,v=0,w=0,x=0.0,y=0,z=0.0,A=0,B=0.0,C=0,D=0.0,E=0.0,F=0.0,G=0.0,H=0.0,I=0.0,J=0.0,K=0,L=0,M=0,N=0,O=0,P=0.0,Q=0,R=0.0,S=0.0,T=0,U=0.0,V=0.0,W=0.0,X=0.0,Y=0.0,Z=0.0,_=0.0,$=0,aa=0.0,ba=0.0,ca=0.0,da=0,ea=0.0,fa=0,ga=0.0,ha=0.0,ia=0.0,ja=0.0,ka=0.0,la=0,ma=0,na=0,oa=0;m=i;i=i+72|0;n=a;a=i;i=i+32|0;io(a,n,32)|0;n=b;b=i;i=i+4|0;i=i+7&-8;c[b>>2]=c[n>>2];n=m|0;o=m+32|0;p=m+64|0;q=p;r=+h[3883];s=+h[3884];t=+h[3885];u=+h[3886];v=c[7750]|0;w=v+24|0;x=+h[w>>3];y=v+32|0;z=+h[y>>3];A=v+40|0;B=+h[A>>3];C=v+48|0;D=+h[C>>3];E=+((e[v+4>>1]|0)>>>0);F=+((e[v+6>>1]|0)>>>0);if(t<0.0|E<r|u<0.0|F<s){G=0.0;H=0.0;I=0.0;J=0.0}else{G=r>0.0?r:0.0;H=s>0.0?s:0.0;I=t<E?t:E;J=u<F?u:F}h[w>>3]=G;h[y>>3]=H;h[A>>3]=I;h[C>>3]=J;C=j-1|0;L4:do{if((C|0)>0){J=+h[a>>3];I=+h[a+8>>3];H=t-r;G=+h[a+16>>3]-J;F=u-s;E=+h[a+24>>3]-I;j=n|0;A=n+8|0;y=n+16|0;w=n+24|0;v=o|0;K=o+8|0;L=o+16|0;M=o+24|0;N=b;O=0;P=k;Q=1;while(1){R=r+H*(+h[f+(O<<3)>>3]-J)/G;S=s+F*(P-I)/E;T=O+g|0;U=P+l;V=r+H*(+h[f+(T<<3)>>3]-J)/G-R;W=s+F*(U-I)/E-S;X=-0.0-V;h[j>>3]=X;h[A>>3]=V;h[y>>3]=-0.0-W;h[w>>3]=W;Y=R-r;h[v>>3]=Y;h[K>>3]=t-R;h[L>>3]=S-s;h[M>>3]=u-S;Z=0.0;_=1.0;$=1;aa=X;X=Y;L8:while(1){do{if(aa==0.0){if(X<0.0){break L8}else{ba=_;ca=Z}}else{Y=X/aa;if(aa<0.0&Y>_){break L8}da=aa>0.0;if(da&Y<Z){break L8}if(aa>0.0){ea=Z}else{ea=+tb(+Y,+Z)}if(!da){ba=_;ca=ea;break}ba=+Ib(+Y,+_);ca=ea}}while(0);if($>>>0>=4>>>0){fa=16;break}Y=+h[n+($<<3)>>3];ga=+h[o+($<<3)>>3];Z=ca;_=ba;$=$+1|0;aa=Y;X=ga}if((fa|0)==16){fa=0;$=c[7750]|0;c[p>>2]=d[N]|d[N+1|0]<<8|d[N+2|0]<<16|d[N+3|0]<<24;Wd($,q,R+V*ca,S+W*ca,R+V*ba,S+W*ba,1.0)}if((Q|0)>=(C|0)){break L4}O=T;P=U;Q=Q+1|0}}}while(0);C=c[7750]|0;ba=+((e[C+4>>1]|0)>>>0);ca=+((e[C+6>>1]|0)>>>0);if(B<0.0|ba<x|D<0.0|ca<z){ha=0.0;ia=0.0;ja=0.0;ka=0.0;la=C+24|0;h[la>>3]=ha;ma=C+32|0;h[ma>>3]=ia;na=C+40|0;h[na>>3]=ja;oa=C+48|0;h[oa>>3]=ka;i=m;return}ha=x>0.0?x:0.0;ia=z>0.0?z:0.0;ja=B<ba?B:ba;ka=D<ca?D:ca;la=C+24|0;h[la>>3]=ha;ma=C+32|0;h[ma>>3]=ia;na=C+40|0;h[na>>3]=ja;oa=C+48|0;h[oa>>3]=ka;i=m;return}function ie(d,e,f,g,j,k,l){d=d|0;e=e|0;f=f|0;g=g|0;j=j|0;k=k|0;l=l|0;var m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0.0,y=0.0,z=0.0,A=0.0,B=0,C=0.0,D=0.0,E=0,F=0.0,G=0.0,H=0.0,I=0,J=0.0,K=0,L=0.0,M=0,N=0,O=0,P=0;m=i;i=i+72|0;n=e;e=i;i=i+32|0;io(e,n,32)|0;n=f;f=i;i=i+32|0;io(f,n,32)|0;n=m|0;o=m+8|0;p=m+16|0;q=m+24|0;r=m+32|0;s=m+40|0;t=m+48|0;u=m+56|0;v=m+64|0;w=v;x=+h[f>>3];y=+h[f+16>>3]-x;z=+h[e>>3];f=e+8|0;A=+h[e+16>>3];B=e+24|0;C=A-z;ke(x,y,C,42.0,q,r,s,t,u);e=c[t>>2]|0;le(34592,e,j);j=ho(34592)|0;if((j|0)==0){D=0.0}else{t=0;E=0;do{E=(b[28208+((a[34592+t|0]|0)-32<<4)>>1]|0)+E&65535;t=t+1|0;}while(t>>>0<j>>>0);D=+(E&65535|0)}if(k){E=~~(+h[B>>3]+3.0+3.0);c[o>>2]=-1;Xd(28200,d,o,~~(+(g|0)+A-D),E,34592,ho(34592)|0);}E=c[s>>2]|0;if((E|0)<=0){i=m;return}F=+h[q>>3];G=+h[r>>3];r=c[u>>2]|0;u=(r|0)<0?0:r;r=(e|0)==0;q=~~z;H=A-D;D=+h[B>>3];B=~~D;s=~~+h[f>>3];f=~~(D+-1.0);g=B&65535;o=B+3&65535;j=n;t=B+6&65535;B=p;D=+(e|0);e=0;do{A=F+ +(e|0)*G;I=~~+Gb(+((A-x)*C/y));do{if((I|0)>=0){J=+(I|0);if(J>C){break}if(r){nb(34592,200,(K=i,i=i+16|0,c[K>>2]=u,h[K+8>>3]=A,K)|0)|0;i=K}else{L=A/+X(+10.0,+D);nb(34592,200,(K=i,i=i+16|0,c[K>>2]=u,h[K+8>>3]=L,K)|0)|0;i=K}M=ho(34592)|0;if((M|0)==0){N=0}else{O=0;P=0;do{P=(b[28208+((a[34592+O|0]|0)-32<<4)>>1]|0)+P&65535;O=O+1|0;}while(O>>>0<M>>>0);N=P&65535}L=J+z;M=~~(L- +(N|0)*.5);O=+(M|0)<z?q:M;if(+(O|0)<=z){break}if(+(O+N|0)>=H){break}if(l){c[v>>2]=1717986918;ce(d,w,~~L,s,f,-13108)}if(!k){break}c[n>>2]=-1;ce(d,j,~~L,g,o,-1);c[p>>2]=-1;Xd(28200,d,B,O&65535,t,34592,ho(34592)|0);}}while(0);e=e+1|0;}while((e|0)<(E|0));i=m;return}function je(d,e,f,g,j,k,l){d=d|0;e=e|0;f=f|0;g=g|0;j=j|0;k=k|0;l=l|0;var m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0.0,y=0.0,z=0.0,A=0,B=0.0,C=0.0,D=0,E=0,F=0.0,G=0,H=0,I=0,J=0,K=0,L=0,M=0.0,N=0.0,O=0.0,P=0.0,Q=0.0,R=0,S=0.0,T=0,U=0,V=0,W=0,Y=0,Z=0,_=0.0,$=0.0,aa=0.0;m=i;i=i+72|0;n=e;e=i;i=i+32|0;io(e,n,32)|0;n=f;f=i;i=i+32|0;io(f,n,32)|0;n=m|0;o=m+8|0;p=m+16|0;q=m+24|0;r=m+32|0;s=m+40|0;t=m+48|0;u=m+56|0;v=m+64|0;w=v;x=+h[f+8>>3];y=+h[f+24>>3]-x;f=e|0;z=+h[e+8>>3];A=e+16|0;B=+h[e+24>>3];C=B-z;ke(x,y,C,15.0,q,r,s,t,u);e=c[t>>2]|0;le(34464,e,j);j=ho(34464)|0;if((j|0)==0){D=32767;E=0;F=0.0}else{t=0;G=32767;H=0;I=0;do{J=(a[34464+t|0]|0)-32|0;K=b[28204+(J<<4)>>1]|0;I=(b[28208+(J<<4)>>1]|0)+I&65535;L=K-(b[28202+(J<<4)>>1]|0)&65535;H=K<<16>>16>H<<16>>16?K:H;G=L<<16>>16<G<<16>>16?L:G;t=t+1|0;}while(t>>>0<j>>>0);D=G;E=H;F=+(I&65535|0)}if(k){M=+(g|0);I=~~(+h[f>>3]-M+-3.0+-3.0-F);c[o>>2]=-1;Xd(28200,d,o,I,~~(z-M),34464,ho(34464)|0);}I=c[s>>2]|0;if((I|0)<=0){i=m;return}M=+h[q>>3];F=+h[r>>3];r=c[u>>2]|0;u=(r|0)<0?0:r;r=(e|0)==0;q=~~z;N=+(E-D&65535|0);O=+h[f>>3];f=~~(O- +(g|0)+-3.0);g=~~O;D=~~(+h[A>>3]+-1.0);A=f&65535;E=f+3&65535;s=n;o=p;O=+(e|0);e=0;do{P=M+ +(e|0)*F;H=~~+Gb(+((P-x)*C/y));do{if((H|0)>=0){Q=+(H|0);if(Q>=C){break}if(r){nb(34464,200,(R=i,i=i+16|0,c[R>>2]=u,h[R+8>>3]=P,R)|0)|0;i=R}else{S=P/+X(+10.0,+O);nb(34464,200,(R=i,i=i+16|0,c[R>>2]=u,h[R+8>>3]=S,R)|0)|0;i=R}G=ho(34464)|0;if((G|0)==0){T=32767;U=0;V=0}else{j=0;t=32767;L=0;K=0;while(1){J=(a[34464+j|0]|0)-32|0;W=b[28204+(J<<4)>>1]|0;Y=(b[28208+(J<<4)>>1]|0)+K&65535;Z=W-(b[28202+(J<<4)>>1]|0)&65535;J=W<<16>>16>L<<16>>16?W:L;W=Z<<16>>16<t<<16>>16?Z:t;Z=j+1|0;if(Z>>>0<G>>>0){j=Z;t=W;L=J;K=Y}else{T=W;U=J;V=Y;break}}}S=Q+z;_=+(U-T&65535|0);$=_*.5;K=~~(S-$);L=+(K|0)<z?q:K;aa=+(L|0);if(aa<=N+($+z)+1.0){break}if(aa>=B-_){break}if(l){c[v>>2]=1717986918;Yd(d,w,g,D,~~S,-13108)}if(!k){break}c[n>>2]=-1;Yd(d,s,A,E,~~S,-1);c[p>>2]=-1;Xd(28200,d,o,((V&65535)>>>0>42>>>0?65491:65533-(V&65535)|0)+f&65535,L&65535,34464,ho(34464)|0);}}while(0);e=e+1|0;}while((e|0)<(I|0));i=m;return}function ke(a,b,d,e,f,g,i,j,k){a=+a;b=+b;d=+d;e=+e;f=f|0;g=g|0;i=i|0;j=j|0;k=k|0;var l=0.0,m=0.0,n=0.0,o=0.0,p=0.0;if(b<0.0){l=a+b;m=-0.0-b}else{l=a;m=b}if(d/e>12.0){n=d/12.0}else{n=e}e=m*n/d;d=+X(+10.0,+(+U(+cc(+(e+1.0e-100)))));do{if(d<e){n=d*2.0;if(n>=e){o=n;break}n=d*5.0;if(n>=e){o=n;break}o=d*10.0}else{o=d}}while(0);d=+rb(+l,+o);if(d<0.0){p=o+d}else{p=d}d=l-p;h[f>>3]=d;h[g>>3]=o;c[i>>2]=~~(m/o+2.0);p=+V(+d);l=+V(+(m+d));i=~~+U(+cc(+((l>p?l:p)+1.0e-100)));while(1){if(((i|0)%3|0|0)==0){break}else{i=i-1|0}}g=~~(+(i|0)- +U(+cc(+(o+1.0e-100))));c[j>>2]=i;c[k>>2]=(g|0)>30?30:g;return}function le(a,b,d){a=a|0;b=b|0;d=d|0;var e=0,f=0;e=i;switch(b|0){case-24:{nb(a|0,176,(f=i,i=i+8|0,c[f>>2]=d,f)|0)|0;i=f;i=e;return};case-15:{nb(a|0,128,(f=i,i=i+8|0,c[f>>2]=d,f)|0)|0;i=f;i=e;return};case 12:{nb(a|0,4032,(f=i,i=i+8|0,c[f>>2]=d,f)|0)|0;i=f;i=e;return};case 15:{nb(a|0,3992,(f=i,i=i+8|0,c[f>>2]=d,f)|0)|0;i=f;i=e;return};case-12:{nb(a|0,112,(f=i,i=i+8|0,c[f>>2]=d,f)|0)|0;i=f;i=e;return};case-18:{nb(a|0,144,(f=i,i=i+8|0,c[f>>2]=d,f)|0)|0;i=f;i=e;return};case-9:{nb(a|0,56,(f=i,i=i+8|0,c[f>>2]=d,f)|0)|0;i=f;i=e;return};case 18:{nb(a|0,3952,(f=i,i=i+8|0,c[f>>2]=d,f)|0)|0;i=f;i=e;return};case-6:{nb(a|0,4112,(f=i,i=i+8|0,c[f>>2]=d,f)|0)|0;i=f;i=e;return};case-3:{nb(a|0,4096,(f=i,i=i+8|0,c[f>>2]=d,f)|0)|0;i=f;i=e;return};case 0:{io(a|0,d|0,(ho(d|0)|0)+1|0)|0;i=e;return};case 3:{nb(a|0,4072,(f=i,i=i+8|0,c[f>>2]=d,f)|0)|0;i=f;i=e;return};case 9:{nb(a|0,4040,(f=i,i=i+8|0,c[f>>2]=d,f)|0)|0;i=f;i=e;return};case 21:{nb(a|0,3912,(f=i,i=i+8|0,c[f>>2]=d,f)|0)|0;i=f;i=e;return};case 24:{nb(a|0,3856,(f=i,i=i+8|0,c[f>>2]=d,f)|0)|0;i=f;i=e;return};case-21:{nb(a|0,160,(f=i,i=i+8|0,c[f>>2]=d,f)|0)|0;i=f;i=e;return};case 6:{nb(a|0,4056,(f=i,i=i+8|0,c[f>>2]=d,f)|0)|0;i=f;i=e;return};default:{nb(a|0,3800,(f=i,i=i+16|0,c[f>>2]=b,c[f+8>>2]=d,f)|0)|0;i=f;i=e;return}}}function me(a,b){a=a|0;b=b|0;var d=0,e=0,f=0,g=0,h=0,i=0,j=0,k=0,l=0;d=a+68|0;if((c[d>>2]|0)==0){e=b}else{f=a+60|0;g=b;b=0;while(1){if((g|0)==(c[8684]|0)){c[8684]=g<<1;h=On(c[8686]|0,g<<6)|0;c[8686]=h;i=h}else{i=c[8686]|0}h=g+1|0;j=i+(g<<5)|0;k=(c[f>>2]|0)+(b<<5)|0;c[j>>2]=c[k>>2];c[j+4>>2]=c[k+4>>2];c[j+8>>2]=c[k+8>>2];c[j+12>>2]=c[k+12>>2];c[j+16>>2]=c[k+16>>2];c[j+20>>2]=c[k+20>>2];c[j+24>>2]=c[k+24>>2];c[j+28>>2]=c[k+28>>2];k=b+1|0;if(k>>>0<(c[d>>2]|0)>>>0){g=h;b=k}else{e=h;break}}}b=a+84|0;if((c[b>>2]|0)==0){l=e}else{g=a+76|0;f=e;e=0;while(1){i=me(c[(c[g>>2]|0)+(e<<2)>>2]|0,f)|0;h=e+1|0;if(h>>>0<(c[b>>2]|0)>>>0){f=i;e=h}else{l=i;break}}}e=a+64|0;if((c[e>>2]|0)==0){c[d>>2]=0;return l|0}f=a+60|0;a=0;while(1){b=a+1|0;ko((c[f>>2]|0)+(a<<5)|0,0,32)|0;if(b>>>0<(c[e>>2]|0)>>>0){a=b}else{break}}c[d>>2]=0;return l|0}function ne(f,g,j){f=f|0;g=g|0;j=j|0;var k=0,l=0,m=0.0,n=0.0,o=0.0,p=0.0,q=0,r=0.0,s=0.0,t=0.0,u=0.0,v=0.0,w=0.0,x=0.0,y=0.0,z=0,A=0,B=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0;k=i;i=i+32|0;l=j;j=i;i=i+32|0;io(j,l,32)|0;l=k|0;if((a[g+88|0]&1)==0){i=k;return}m=+(b[g+56>>1]|0);n=+(b[g+58>>1]|0);o=m+ +((e[g+4>>1]|0)>>>0);p=n+ +((e[g+6>>1]|0)>>>0);q=j;r=+h[j>>3];s=+h[j+8>>3];t=+h[j+16>>3];u=+h[j+24>>3];if(o<r|t<m|p<s|u<n){v=0.0;w=0.0;x=0.0;y=0.0}else{v=m>r?m:r;w=n>s?n:s;x=o<t?o:t;y=p<u?p:u}j=f+8|0;z=g+8|0;u=y-w;L7:do{if(u>0.0){y=x-v;if(y>0.0){A=(c[f>>2]|0)+(~~(v+w*+(e[j>>1]|0))<<2)|0;B=(c[g>>2]|0)+(~~((w-n)*+(e[z>>1]|0)+(v-m))<<2)|0;D=0}else{E=0;while(1){E=E+1|0;if(+(E>>>0>>>0)>=u){break L7}}}while(1){E=0;do{F=A+(E<<2)|0;G=d[F]|d[F+1|0]<<8|d[F+2|0]<<16|d[F+3|0]<<24|0;H=B+(E<<2)|0;I=d[H]|d[H+1|0]<<8|d[H+2|0]<<16|d[H+3|0]<<24|0;H=I>>>24;J=H^255;C=((ga(J,G>>>24)|0)>>>8)+H<<24|(ga(J,G>>>8&255)|0)+(I&65280)&65280|((ga(J,G&255)|0)>>>8)+I&255|((ga(J,G>>>16&255)|0)>>>8<<16)+I&16711680;a[F]=C;C=C>>8;a[F+1|0]=C;C=C>>8;a[F+2|0]=C;C=C>>8;a[F+3|0]=C;E=E+1|0;}while(+(E>>>0>>>0)<y);E=D+1|0;if(+(E>>>0>>>0)<u){A=A+(e[j>>1]<<2)|0;B=B+(e[z>>1]<<2)|0;D=E}else{break}}}}while(0);D=g+84|0;if((c[D>>2]|0)==0){i=k;return}z=l;B=0;do{c[z>>2]=c[q>>2];c[z+4>>2]=c[q+4>>2];c[z+8>>2]=c[q+8>>2];c[z+12>>2]=c[q+12>>2];c[z+16>>2]=c[q+16>>2];c[z+20>>2]=c[q+20>>2];c[z+24>>2]=c[q+24>>2];c[z+28>>2]=c[q+28>>2];ne(f,g,l);B=B+1|0;}while(B>>>0<(c[D>>2]|0)>>>0);i=k;return}function oe(){c[7568]=0;c[7569]=0;c[7570]=0;c[7781]=0;c[7782]=0;c[7783]=0;gb(120,29720,u|0)|0;return}function pe(a){a=a|0;zb(a|0)|0;fc()}function qe(b){b=b|0;var d=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0;b=i;i=i+32|0;d=b|0;e=b+8|0;f=b+16|0;g=b+24|0;h=c[o>>2]|0;Me(35272,h,35400);c[9066]=6516;c[9068]=6536;c[9067]=0;Qf(36272,35272);c[9086]=0;c[9087]=-1;j=c[t>>2]|0;c[8794]=6296;Wk(35180);ko(35184,0,24)|0;c[8794]=6664;c[8802]=j;Xk(g,35180);k=Zk(g,35600)|0;l=k;Yk(g);c[8803]=l;c[8804]=35408;a[35220]=(dd[c[(c[k>>2]|0)+28>>2]&127](l)|0)&1;c[9e3]=6420;c[9001]=6440;Qf(36004,35176);c[9019]=0;c[9020]=-1;l=c[r>>2]|0;c[8806]=6296;Wk(35228);ko(35232,0,24)|0;c[8806]=6664;c[8814]=l;Xk(f,35228);k=Zk(f,35600)|0;g=k;Yk(f);c[8815]=g;c[8816]=35416;a[35268]=(dd[c[(c[k>>2]|0)+28>>2]&127](g)|0)&1;c[9044]=6420;c[9045]=6440;Qf(36180,35224);c[9063]=0;c[9064]=-1;g=c[(c[(c[9044]|0)-12>>2]|0)+36200>>2]|0;c[9022]=6420;c[9023]=6440;Qf(36092,g);c[9041]=0;c[9042]=-1;c[(c[(c[9066]|0)-12>>2]|0)+36336>>2]=36e3;g=(c[(c[9044]|0)-12>>2]|0)+36180|0;c[g>>2]=c[g>>2]|8192;c[(c[(c[9044]|0)-12>>2]|0)+36248>>2]=36e3;ye(35120,h,35424);c[8978]=6468;c[8980]=6488;c[8979]=0;Qf(35920,35120);c[8998]=0;c[8999]=-1;c[8756]=6224;Wk(35028);ko(35032,0,24)|0;c[8756]=6592;c[8764]=j;Xk(e,35028);j=Zk(e,35592)|0;h=j;Yk(e);c[8765]=h;c[8766]=35432;a[35068]=(dd[c[(c[j>>2]|0)+28>>2]&127](h)|0)&1;c[8908]=6372;c[8909]=6392;Qf(35636,35024);c[8927]=0;c[8928]=-1;c[8768]=6224;Wk(35076);ko(35080,0,24)|0;c[8768]=6592;c[8776]=l;Xk(d,35076);l=Zk(d,35592)|0;h=l;Yk(d);c[8777]=h;c[8778]=35440;a[35116]=(dd[c[(c[l>>2]|0)+28>>2]&127](h)|0)&1;c[8952]=6372;c[8953]=6392;Qf(35812,35072);c[8971]=0;c[8972]=-1;h=c[(c[(c[8952]|0)-12>>2]|0)+35832>>2]|0;c[8930]=6372;c[8931]=6392;Qf(35724,h);c[8949]=0;c[8950]=-1;c[(c[(c[8978]|0)-12>>2]|0)+35984>>2]=35632;h=(c[(c[8952]|0)-12>>2]|0)+35812|0;c[h>>2]=c[h>>2]|8192;c[(c[(c[8952]|0)-12>>2]|0)+35880>>2]=35632;i=b;return}function re(a){a=a|0;vg(36e3)|0;vg(36088)|0;Ag(35632)|0;Ag(35720)|0;return}function se(a){a=a|0;c[a>>2]=6224;Yk(a+4|0);return}function te(a){a=a|0;c[a>>2]=6224;Yk(a+4|0);Tn(a);return}function ue(b,d){b=b|0;d=d|0;var e=0;dd[c[(c[b>>2]|0)+24>>2]&127](b)|0;e=Zk(d,35592)|0;d=e;c[b+36>>2]=d;a[b+44|0]=(dd[c[(c[e>>2]|0)+28>>2]&127](d)|0)&1;return}function ve(a){a=a|0;var b=0,d=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0;b=i;i=i+16|0;d=b|0;e=b+8|0;f=a+36|0;g=a+40|0;h=d|0;j=d+8|0;k=d;d=a+32|0;while(1){a=c[f>>2]|0;l=bd[c[(c[a>>2]|0)+20>>2]&31](a,c[g>>2]|0,h,j,e)|0;a=(c[e>>2]|0)-k|0;if((Na(h|0,1,a|0,c[d>>2]|0)|0)!=(a|0)){m=-1;n=6;break}if((l|0)==2){m=-1;n=8;break}else if((l|0)!=1){n=4;break}}if((n|0)==6){i=b;return m|0}else if((n|0)==8){i=b;return m|0}else if((n|0)==4){m=((Ka(c[d>>2]|0)|0)!=0)<<31>>31;i=b;return m|0}return 0}function we(b,d,e){b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0,i=0,j=0;if((a[b+44|0]&1)!=0){f=Na(d|0,4,e|0,c[b+32>>2]|0)|0;return f|0}g=b;if((e|0)>0){h=d;i=0}else{f=0;return f|0}while(1){if((ad[c[(c[g>>2]|0)+52>>2]&31](b,c[h>>2]|0)|0)==-1){f=i;j=9;break}d=i+1|0;if((d|0)<(e|0)){h=h+4|0;i=d}else{f=d;j=10;break}}if((j|0)==9){return f|0}else if((j|0)==10){return f|0}return 0}function xe(b,d){b=b|0;d=d|0;var e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0;e=i;i=i+32|0;f=e|0;g=e+8|0;h=e+16|0;j=e+24|0;k=(d|0)==-1;L1:do{if(!k){c[g>>2]=d;if((a[b+44|0]&1)!=0){if((Na(g|0,4,1,c[b+32>>2]|0)|0)==1){break}else{l=-1}i=e;return l|0}m=f|0;c[h>>2]=m;n=g+4|0;o=b+36|0;p=b+40|0;q=f+8|0;r=f;s=b+32|0;t=g;while(1){u=c[o>>2]|0;v=id[c[(c[u>>2]|0)+12>>2]&31](u,c[p>>2]|0,t,n,j,m,q,h)|0;if((c[j>>2]|0)==(t|0)){l=-1;w=16;break}if((v|0)==3){w=7;break}u=(v|0)==1;if(v>>>0>=2>>>0){l=-1;w=13;break}v=(c[h>>2]|0)-r|0;if((Na(m|0,1,v|0,c[s>>2]|0)|0)!=(v|0)){l=-1;w=14;break}if(u){t=u?c[j>>2]|0:t}else{break L1}}if((w|0)==13){i=e;return l|0}else if((w|0)==14){i=e;return l|0}else if((w|0)==16){i=e;return l|0}else if((w|0)==7){if((Na(t|0,1,1,c[s>>2]|0)|0)==1){break}else{l=-1}i=e;return l|0}}}while(0);l=k?0:d;i=e;return l|0}function ye(b,d,e){b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0,j=0;f=i;i=i+8|0;g=f|0;h=b|0;c[h>>2]=6224;j=b+4|0;Wk(j);ko(b+8|0,0,24)|0;c[h>>2]=6992;c[b+32>>2]=d;c[b+40>>2]=e;c[b+48>>2]=-1;a[b+52|0]=0;Xk(g,j);j=Zk(g,35592)|0;e=j;d=b+36|0;c[d>>2]=e;h=b+44|0;c[h>>2]=dd[c[(c[j>>2]|0)+24>>2]&127](e)|0;e=c[d>>2]|0;a[b+53|0]=(dd[c[(c[e>>2]|0)+28>>2]&127](e)|0)&1;if((c[h>>2]|0)<=8){Yk(g);i=f;return}gk(64);Yk(g);i=f;return}function ze(a){a=a|0;c[a>>2]=6224;Yk(a+4|0);return}function Ae(a){a=a|0;c[a>>2]=6224;Yk(a+4|0);Tn(a);return}function Be(b,d){b=b|0;d=d|0;var e=0,f=0,g=0;e=Zk(d,35592)|0;d=e;f=b+36|0;c[f>>2]=d;g=b+44|0;c[g>>2]=dd[c[(c[e>>2]|0)+24>>2]&127](d)|0;d=c[f>>2]|0;a[b+53|0]=(dd[c[(c[d>>2]|0)+28>>2]&127](d)|0)&1;if((c[g>>2]|0)<=8){return}gk(64);return}function Ce(a){a=a|0;return Fe(a,0)|0}function De(a){a=a|0;return Fe(a,1)|0}function Ee(b,d){b=b|0;d=d|0;var e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0;e=i;i=i+32|0;f=e|0;g=e+8|0;h=e+16|0;j=e+24|0;k=b+52|0;l=(a[k]&1)!=0;if((d|0)==-1){if(l){m=-1;i=e;return m|0}n=c[b+48>>2]|0;a[k]=(n|0)!=-1|0;m=n;i=e;return m|0}n=b+48|0;L8:do{if(l){c[h>>2]=c[n>>2];o=c[b+36>>2]|0;p=f|0;q=id[c[(c[o>>2]|0)+12>>2]&31](o,c[b+40>>2]|0,h,h+4|0,j,p,f+8|0,g)|0;if((q|0)==2|(q|0)==1){m=-1;i=e;return m|0}else if((q|0)==3){a[p]=c[n>>2];c[g>>2]=f+1}q=b+32|0;while(1){o=c[g>>2]|0;if(o>>>0<=p>>>0){break L8}r=o-1|0;c[g>>2]=r;if((rc(a[r]|0,c[q>>2]|0)|0)==-1){m=-1;break}}i=e;return m|0}}while(0);c[n>>2]=d;a[k]=1;m=d;i=e;return m|0}function Fe(b,d){b=b|0;d=d|0;var e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0;e=i;i=i+32|0;f=e|0;g=e+8|0;h=e+16|0;j=e+24|0;k=b+52|0;if((a[k]&1)!=0){l=b+48|0;m=c[l>>2]|0;if(!d){n=m;i=e;return n|0}c[l>>2]=-1;a[k]=0;n=m;i=e;return n|0}m=c[b+44>>2]|0;k=(m|0)>1?m:1;L8:do{if((k|0)>0){m=b+32|0;l=0;while(1){o=db(c[m>>2]|0)|0;if((o|0)==-1){n=-1;break}a[f+l|0]=o;l=l+1|0;if((l|0)>=(k|0)){break L8}}i=e;return n|0}}while(0);L15:do{if((a[b+53|0]&1)==0){l=b+40|0;m=b+36|0;o=f|0;p=g+4|0;q=b+32|0;r=k;while(1){s=c[l>>2]|0;t=s;u=c[t>>2]|0;v=c[t+4>>2]|0;t=c[m>>2]|0;w=f+r|0;x=id[c[(c[t>>2]|0)+16>>2]&31](t,s,o,w,h,g,p,j)|0;if((x|0)==3){y=14;break}else if((x|0)==2){n=-1;y=26;break}else if((x|0)!=1){z=r;break L15}x=c[l>>2]|0;c[x>>2]=u;c[x+4>>2]=v;if((r|0)==8){n=-1;y=31;break}v=db(c[q>>2]|0)|0;if((v|0)==-1){n=-1;y=29;break}a[w]=v;r=r+1|0}if((y|0)==14){c[g>>2]=a[o]|0;z=r;break}else if((y|0)==26){i=e;return n|0}else if((y|0)==29){i=e;return n|0}else if((y|0)==31){i=e;return n|0}}else{c[g>>2]=a[f|0]|0;z=k}}while(0);if(d){d=c[g>>2]|0;c[b+48>>2]=d;n=d;i=e;return n|0}d=b+32|0;b=z;while(1){if((b|0)<=0){break}z=b-1|0;if((rc(a[f+z|0]|0,c[d>>2]|0)|0)==-1){n=-1;y=27;break}else{b=z}}if((y|0)==27){i=e;return n|0}n=c[g>>2]|0;i=e;return n|0}function Ge(a){a=a|0;c[a>>2]=6296;Yk(a+4|0);return}function He(a){a=a|0;c[a>>2]=6296;Yk(a+4|0);Tn(a);return}function Ie(b,d){b=b|0;d=d|0;var e=0;dd[c[(c[b>>2]|0)+24>>2]&127](b)|0;e=Zk(d,35600)|0;d=e;c[b+36>>2]=d;a[b+44|0]=(dd[c[(c[e>>2]|0)+28>>2]&127](d)|0)&1;return}function Je(a){a=a|0;var b=0,d=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0;b=i;i=i+16|0;d=b|0;e=b+8|0;f=a+36|0;g=a+40|0;h=d|0;j=d+8|0;k=d;d=a+32|0;while(1){a=c[f>>2]|0;l=bd[c[(c[a>>2]|0)+20>>2]&31](a,c[g>>2]|0,h,j,e)|0;a=(c[e>>2]|0)-k|0;if((Na(h|0,1,a|0,c[d>>2]|0)|0)!=(a|0)){m=-1;n=8;break}if((l|0)==2){m=-1;n=7;break}else if((l|0)!=1){n=4;break}}if((n|0)==4){m=((Ka(c[d>>2]|0)|0)!=0)<<31>>31;i=b;return m|0}else if((n|0)==8){i=b;return m|0}else if((n|0)==7){i=b;return m|0}return 0}function Ke(b,e,f){b=b|0;e=e|0;f=f|0;var g=0,h=0,i=0,j=0,k=0;if((a[b+44|0]&1)!=0){g=Na(e|0,1,f|0,c[b+32>>2]|0)|0;return g|0}h=b;if((f|0)>0){i=e;j=0}else{g=0;return g|0}while(1){if((ad[c[(c[h>>2]|0)+52>>2]&31](b,d[i]|0)|0)==-1){g=j;k=7;break}e=j+1|0;if((e|0)<(f|0)){i=i+1|0;j=e}else{g=e;k=9;break}}if((k|0)==7){return g|0}else if((k|0)==9){return g|0}return 0}function Le(b,d){b=b|0;d=d|0;var e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0;e=i;i=i+32|0;f=e|0;g=e+8|0;h=e+16|0;j=e+24|0;k=(d|0)==-1;L1:do{if(!k){a[g]=d;if((a[b+44|0]&1)!=0){if((Na(g|0,1,1,c[b+32>>2]|0)|0)==1){break}else{l=-1}i=e;return l|0}m=f|0;c[h>>2]=m;n=g+1|0;o=b+36|0;p=b+40|0;q=f+8|0;r=f;s=b+32|0;t=g;while(1){u=c[o>>2]|0;v=id[c[(c[u>>2]|0)+12>>2]&31](u,c[p>>2]|0,t,n,j,m,q,h)|0;if((c[j>>2]|0)==(t|0)){l=-1;w=13;break}if((v|0)==3){w=7;break}u=(v|0)==1;if(v>>>0>=2>>>0){l=-1;w=14;break}v=(c[h>>2]|0)-r|0;if((Na(m|0,1,v|0,c[s>>2]|0)|0)!=(v|0)){l=-1;w=15;break}if(u){t=u?c[j>>2]|0:t}else{break L1}}if((w|0)==13){i=e;return l|0}else if((w|0)==14){i=e;return l|0}else if((w|0)==15){i=e;return l|0}else if((w|0)==7){if((Na(t|0,1,1,c[s>>2]|0)|0)==1){break}else{l=-1}i=e;return l|0}}}while(0);l=k?0:d;i=e;return l|0}function Me(b,d,e){b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0,j=0;f=i;i=i+8|0;g=f|0;h=b|0;c[h>>2]=6296;j=b+4|0;Wk(j);ko(b+8|0,0,24)|0;c[h>>2]=7064;c[b+32>>2]=d;c[b+40>>2]=e;c[b+48>>2]=-1;a[b+52|0]=0;Xk(g,j);j=Zk(g,35600)|0;e=j;d=b+36|0;c[d>>2]=e;h=b+44|0;c[h>>2]=dd[c[(c[j>>2]|0)+24>>2]&127](e)|0;e=c[d>>2]|0;a[b+53|0]=(dd[c[(c[e>>2]|0)+28>>2]&127](e)|0)&1;if((c[h>>2]|0)<=8){Yk(g);i=f;return}gk(64);Yk(g);i=f;return}function Ne(a){a=a|0;c[a>>2]=6296;Yk(a+4|0);return}function Oe(a){a=a|0;c[a>>2]=6296;Yk(a+4|0);Tn(a);return}function Pe(b,d){b=b|0;d=d|0;var e=0,f=0,g=0;e=Zk(d,35600)|0;d=e;f=b+36|0;c[f>>2]=d;g=b+44|0;c[g>>2]=dd[c[(c[e>>2]|0)+24>>2]&127](d)|0;d=c[f>>2]|0;a[b+53|0]=(dd[c[(c[d>>2]|0)+28>>2]&127](d)|0)&1;if((c[g>>2]|0)<=8){return}gk(64);return}function Qe(a){a=a|0;return Te(a,0)|0}function Re(a){a=a|0;return Te(a,1)|0}function Se(b,d){b=b|0;d=d|0;var e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0;e=i;i=i+32|0;f=e|0;g=e+8|0;h=e+16|0;j=e+24|0;k=b+52|0;l=(a[k]&1)!=0;if((d|0)==-1){if(l){m=-1;i=e;return m|0}n=c[b+48>>2]|0;a[k]=(n|0)!=-1|0;m=n;i=e;return m|0}n=b+48|0;L8:do{if(l){a[h]=c[n>>2];o=c[b+36>>2]|0;p=f|0;q=id[c[(c[o>>2]|0)+12>>2]&31](o,c[b+40>>2]|0,h,h+1|0,j,p,f+8|0,g)|0;if((q|0)==3){a[p]=c[n>>2];c[g>>2]=f+1}else if((q|0)==2|(q|0)==1){m=-1;i=e;return m|0}q=b+32|0;while(1){o=c[g>>2]|0;if(o>>>0<=p>>>0){break L8}r=o-1|0;c[g>>2]=r;if((rc(a[r]|0,c[q>>2]|0)|0)==-1){m=-1;break}}i=e;return m|0}}while(0);c[n>>2]=d;a[k]=1;m=d;i=e;return m|0}function Te(b,e){b=b|0;e=e|0;var f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0;f=i;i=i+32|0;g=f|0;h=f+8|0;j=f+16|0;k=f+24|0;l=b+52|0;if((a[l]&1)!=0){m=b+48|0;n=c[m>>2]|0;if(!e){o=n;i=f;return o|0}c[m>>2]=-1;a[l]=0;o=n;i=f;return o|0}n=c[b+44>>2]|0;l=(n|0)>1?n:1;L8:do{if((l|0)>0){n=b+32|0;m=0;while(1){p=db(c[n>>2]|0)|0;if((p|0)==-1){o=-1;break}a[g+m|0]=p;m=m+1|0;if((m|0)>=(l|0)){break L8}}i=f;return o|0}}while(0);L15:do{if((a[b+53|0]&1)==0){m=b+40|0;n=b+36|0;p=g|0;q=h+1|0;r=b+32|0;s=l;while(1){t=c[m>>2]|0;u=t;v=c[u>>2]|0;w=c[u+4>>2]|0;u=c[n>>2]|0;x=g+s|0;y=id[c[(c[u>>2]|0)+16>>2]&31](u,t,p,x,j,h,q,k)|0;if((y|0)==2){o=-1;z=29;break}else if((y|0)==3){z=14;break}else if((y|0)!=1){A=s;break L15}y=c[m>>2]|0;c[y>>2]=v;c[y+4>>2]=w;if((s|0)==8){o=-1;z=30;break}w=db(c[r>>2]|0)|0;if((w|0)==-1){o=-1;z=31;break}a[x]=w;s=s+1|0}if((z|0)==29){i=f;return o|0}else if((z|0)==30){i=f;return o|0}else if((z|0)==31){i=f;return o|0}else if((z|0)==14){a[h]=a[p]|0;A=s;break}}else{a[h]=a[g|0]|0;A=l}}while(0);do{if(e){l=a[h]|0;c[b+48>>2]=l&255;B=l}else{l=b+32|0;k=A;while(1){if((k|0)<=0){z=21;break}j=k-1|0;if((rc(d[g+j|0]|0|0,c[l>>2]|0)|0)==-1){o=-1;z=25;break}else{k=j}}if((z|0)==25){i=f;return o|0}else if((z|0)==21){B=a[h]|0;break}}}while(0);o=B&255;i=f;return o|0}function Ue(){qe(0);gb(142,36352,u|0)|0;return}function Ve(a){a=a|0;return}function We(a){a=a|0;var b=0;b=a+4|0;I=c[b>>2]|0,c[b>>2]=I+1,I;return}function Xe(a){a=a|0;var b=0,d=0;b=a+4|0;if(((I=c[b>>2]|0,c[b>>2]=I+ -1,I)|0)!=0){d=0;return d|0}_c[c[(c[a>>2]|0)+8>>2]&511](a);d=1;return d|0}function Ye(a,b){a=a|0;b=b|0;var d=0,e=0,f=0;c[a>>2]=4600;d=ho(b|0)|0;e=Sn(d+13|0)|0;c[e+4>>2]=d;c[e>>2]=d;f=e+12|0;c[a+4>>2]=f;c[e+8>>2]=0;io(f|0,b|0,d+1|0)|0;return}function Ze(a){a=a|0;var b=0,d=0,e=0;c[a>>2]=4600;b=a+4|0;d=(c[b>>2]|0)-4|0;if(((I=c[d>>2]|0,c[d>>2]=I+ -1,I)-1|0)>=0){e=a;Tn(e);return}Un((c[b>>2]|0)-12|0);e=a;Tn(e);return}function _e(a){a=a|0;var b=0;c[a>>2]=4600;b=a+4|0;a=(c[b>>2]|0)-4|0;if(((I=c[a>>2]|0,c[a>>2]=I+ -1,I)-1|0)>=0){return}Un((c[b>>2]|0)-12|0);return}function $e(a){a=a|0;return c[a+4>>2]|0}function af(b,d){b=b|0;d=d|0;var e=0,f=0,g=0;c[b>>2]=4536;if((a[d]&1)==0){e=d+1|0}else{e=c[d+8>>2]|0}d=ho(e|0)|0;f=Sn(d+13|0)|0;c[f+4>>2]=d;c[f>>2]=d;g=f+12|0;c[b+4>>2]=g;c[f+8>>2]=0;io(g|0,e|0,d+1|0)|0;return}function bf(a,b){a=a|0;b=b|0;var d=0,e=0,f=0;c[a>>2]=4536;d=ho(b|0)|0;e=Sn(d+13|0)|0;c[e+4>>2]=d;c[e>>2]=d;f=e+12|0;c[a+4>>2]=f;c[e+8>>2]=0;io(f|0,b|0,d+1|0)|0;return}function cf(a){a=a|0;var b=0,d=0,e=0;c[a>>2]=4536;b=a+4|0;d=(c[b>>2]|0)-4|0;if(((I=c[d>>2]|0,c[d>>2]=I+ -1,I)-1|0)>=0){e=a;Tn(e);return}Un((c[b>>2]|0)-12|0);e=a;Tn(e);return}function df(a){a=a|0;var b=0;c[a>>2]=4536;b=a+4|0;a=(c[b>>2]|0)-4|0;if(((I=c[a>>2]|0,c[a>>2]=I+ -1,I)-1|0)>=0){return}Un((c[b>>2]|0)-12|0);return}function ef(a){a=a|0;return c[a+4>>2]|0}function ff(a){a=a|0;var b=0,d=0,e=0;c[a>>2]=4600;b=a+4|0;d=(c[b>>2]|0)-4|0;if(((I=c[d>>2]|0,c[d>>2]=I+ -1,I)-1|0)>=0){e=a;Tn(e);return}Un((c[b>>2]|0)-12|0);e=a;Tn(e);return}function gf(a){a=a|0;return}function hf(a,b,d){a=a|0;b=b|0;d=d|0;c[a>>2]=d;c[a+4>>2]=b;return}function jf(a,b,d){a=a|0;b=b|0;d=d|0;var e=0,f=0,g=0;e=i;i=i+8|0;f=e|0;fd[c[(c[a>>2]|0)+12>>2]&7](f,a,b);if((c[f+4>>2]|0)!=(c[d+4>>2]|0)){g=0;i=e;return g|0}g=(c[f>>2]|0)==(c[d>>2]|0);i=e;return g|0}function kf(a,b,d){a=a|0;b=b|0;d=d|0;var e=0;if((c[b+4>>2]|0)!=(a|0)){e=0;return e|0}e=(c[b>>2]|0)==(d|0);return e|0}function lf(b,d,e){b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0,i=0;d=lc(e|0)|0;e=ho(d|0)|0;if(e>>>0>4294967279>>>0){rf(0)}if(e>>>0<11>>>0){a[b]=e<<1;f=b+1|0;io(f|0,d|0,e)|0;g=f+e|0;a[g]=0;return}else{h=e+16&-16;i=Rn(h)|0;c[b+8>>2]=i;c[b>>2]=h|1;c[b+4>>2]=e;f=i;io(f|0,d|0,e)|0;g=f+e|0;a[g]=0;return}}function mf(b,e,f){b=b|0;e=e|0;f=f|0;var g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0;g=i;h=f;j=i;i=i+12|0;i=i+7&-8;k=e|0;l=c[k>>2]|0;do{if((l|0)!=0){m=d[h]|0;if((m&1|0)==0){n=m>>>1}else{n=c[f+4>>2]|0}if((n|0)==0){o=l}else{Bf(f,3128,2)|0;o=c[k>>2]|0}m=c[e+4>>2]|0;fd[c[(c[m>>2]|0)+24>>2]&7](j,m,o);m=j;p=a[m]|0;if((p&1)==0){q=j+1|0}else{q=c[j+8>>2]|0}r=p&255;if((r&1|0)==0){s=r>>>1}else{s=c[j+4>>2]|0}Bf(f,q,s)|0;if((a[m]&1)==0){break}Tn(c[j+8>>2]|0)}}while(0);j=b;c[j>>2]=c[h>>2];c[j+4>>2]=c[h+4>>2];c[j+8>>2]=c[h+8>>2];ko(h|0,0,12)|0;i=g;return}function nf(b,d,e){b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0,j=0,k=0,l=0,m=0;f=i;i=i+32|0;g=d;d=i;i=i+8|0;c[d>>2]=c[g>>2];c[d+4>>2]=c[g+4>>2];g=f|0;h=f+16|0;j=ho(e|0)|0;if(j>>>0>4294967279>>>0){rf(0)}if(j>>>0<11>>>0){a[h]=j<<1;k=h+1|0}else{l=j+16&-16;m=Rn(l)|0;c[h+8>>2]=m;c[h>>2]=l|1;c[h+4>>2]=j;k=m}io(k|0,e|0,j)|0;a[k+j|0]=0;mf(g,d,h);af(b|0,g);if((a[g]&1)!=0){Tn(c[g+8>>2]|0)}if((a[h]&1)!=0){Tn(c[h+8>>2]|0)}c[b>>2]=6560;h=d;d=b+8|0;b=c[h+4>>2]|0;c[d>>2]=c[h>>2];c[d+4>>2]=b;i=f;return}function of(a){a=a|0;df(a|0);Tn(a);return}function pf(a){a=a|0;df(a|0);return}function qf(a,b,d){a=a|0;b=b|0;d=d|0;var e=0,f=0,g=0,h=0,i=0;e;if((c[a>>2]|0)==1){do{Xa(35352,35328)|0;}while((c[a>>2]|0)==1)}if((c[a>>2]|0)!=0){f;return}c[a>>2]=1;g;_c[d&511](b);h;c[a>>2]=-1;i;ec(35352)|0;return}function rf(a){a=a|0;a=Nc(8)|0;Ye(a,448);c[a>>2]=4568;Qb(a|0,10224,36)}function sf(b,d){b=b|0;d=d|0;var e=0,f=0,g=0,h=0;e=d;if((a[e]&1)==0){f=b;c[f>>2]=c[e>>2];c[f+4>>2]=c[e+4>>2];c[f+8>>2]=c[e+8>>2];return}e=c[d+8>>2]|0;f=c[d+4>>2]|0;if(f>>>0>4294967279>>>0){rf(0)}if(f>>>0<11>>>0){a[b]=f<<1;g=b+1|0}else{d=f+16&-16;h=Rn(d)|0;c[b+8>>2]=h;c[b>>2]=d|1;c[b+4>>2]=f;g=h}io(g|0,e|0,f)|0;a[g+f|0]=0;return}function tf(b,d,e){b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0,i=0;if(e>>>0>4294967279>>>0){rf(0)}if(e>>>0<11>>>0){a[b]=e<<1;f=b+1|0;io(f|0,d|0,e)|0;g=f+e|0;a[g]=0;return}else{h=e+16&-16;i=Rn(h)|0;c[b+8>>2]=i;c[b>>2]=h|1;c[b+4>>2]=e;f=i;io(f|0,d|0,e)|0;g=f+e|0;a[g]=0;return}}function uf(b,d,e){b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0;if(d>>>0>4294967279>>>0){rf(0)}if(d>>>0<11>>>0){a[b]=d<<1;f=b+1|0}else{g=d+16&-16;h=Rn(g)|0;c[b+8>>2]=h;c[b>>2]=g|1;c[b+4>>2]=d;f=h}ko(f|0,e|0,d|0)|0;a[f+d|0]=0;return}function vf(b){b=b|0;if((a[b]&1)==0){return}Tn(c[b+8>>2]|0);return}function wf(b,d){b=b|0;d=d|0;var e=0,f=0,g=0,h=0,i=0,j=0,k=0,l=0;e=ho(d|0)|0;f=b;g=b;h=a[g]|0;if((h&1)==0){i=10;j=h}else{h=c[b>>2]|0;i=(h&-2)-1|0;j=h&255}if(i>>>0<e>>>0){h=j&255;if((h&1|0)==0){k=h>>>1}else{k=c[b+4>>2]|0}Cf(b,i,e-i|0,k,0,k,e,d);return b|0}if((j&1)==0){l=f+1|0}else{l=c[b+8>>2]|0}mo(l|0,d|0,e|0)|0;a[l+e|0]=0;if((a[g]&1)==0){a[g]=e<<1;return b|0}else{c[b+4>>2]=e;return b|0}return 0}function xf(b,d,e){b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0,i=0;f=b;g=a[f]|0;h=g&255;if((h&1|0)==0){i=h>>>1}else{i=c[b+4>>2]|0}if(i>>>0<d>>>0){yf(b,d-i|0,e)|0;return}if((g&1)==0){a[b+1+d|0]=0;a[f]=d<<1;return}else{a[(c[b+8>>2]|0)+d|0]=0;c[b+4>>2]=d;return}}function yf(b,d,e){b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0,i=0,j=0,k=0,l=0;if((d|0)==0){return b|0}f=b;g=a[f]|0;if((g&1)==0){h=10;i=g}else{g=c[b>>2]|0;h=(g&-2)-1|0;i=g&255}g=i&255;if((g&1|0)==0){j=g>>>1}else{j=c[b+4>>2]|0}if((h-j|0)>>>0<d>>>0){Df(b,h,d-h+j|0,j,j,0,0);k=a[f]|0}else{k=i}if((k&1)==0){l=b+1|0}else{l=c[b+8>>2]|0}ko(l+j|0,e|0,d|0)|0;e=j+d|0;if((a[f]&1)==0){a[f]=e<<1}else{c[b+4>>2]=e}a[l+e|0]=0;return b|0}function zf(b,d){b=b|0;d=d|0;var e=0,f=0,g=0,h=0,i=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0;if(d>>>0>4294967279>>>0){rf(0)}e=b;f=b;g=a[f]|0;if((g&1)==0){h=10;i=g}else{g=c[b>>2]|0;h=(g&-2)-1|0;i=g&255}g=i&255;if((g&1|0)==0){j=g>>>1}else{j=c[b+4>>2]|0}g=j>>>0>d>>>0?j:d;if(g>>>0<11>>>0){k=11}else{k=g+16&-16}g=k-1|0;if((g|0)==(h|0)){return}if((g|0)==10){l=e+1|0;m=c[b+8>>2]|0;n=1;o=0}else{if(g>>>0>h>>>0){p=Rn(k)|0}else{p=Rn(k)|0}h=i&1;if(h<<24>>24==0){q=e+1|0}else{q=c[b+8>>2]|0}l=p;m=q;n=h<<24>>24!=0;o=1}h=i&255;if((h&1|0)==0){r=h>>>1}else{r=c[b+4>>2]|0}io(l|0,m|0,r+1|0)|0;if(n){Tn(m)}if(o){c[b>>2]=k|1;c[b+4>>2]=j;c[b+8>>2]=l;return}else{a[f]=j<<1;return}}function Af(b,d){b=b|0;d=d|0;var e=0,f=0,g=0,h=0,i=0,j=0,k=0,l=0,m=0;e=b;f=a[e]|0;if((f&1)==0){g=(f&255)>>>1;h=10}else{g=c[b+4>>2]|0;h=(c[b>>2]&-2)-1|0}if((g|0)==(h|0)){Df(b,h,1,h,h,0,0);i=a[e]|0}else{i=f}if((i&1)==0){a[e]=(g<<1)+2;j=b+1|0;k=g+1|0;l=j+g|0;a[l]=d;m=j+k|0;a[m]=0;return}else{e=c[b+8>>2]|0;i=g+1|0;c[b+4>>2]=i;j=e;k=i;l=j+g|0;a[l]=d;m=j+k|0;a[m]=0;return}}function Bf(b,d,e){b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0,i=0,j=0,k=0;f=b;g=a[f]|0;if((g&1)==0){h=10;i=g}else{g=c[b>>2]|0;h=(g&-2)-1|0;i=g&255}g=i&255;if((g&1|0)==0){j=g>>>1}else{j=c[b+4>>2]|0}if((h-j|0)>>>0<e>>>0){Cf(b,h,e-h+j|0,j,j,0,e,d);return b|0}if((e|0)==0){return b|0}if((i&1)==0){k=b+1|0}else{k=c[b+8>>2]|0}io(k+j|0,d|0,e)|0;d=j+e|0;if((a[f]&1)==0){a[f]=d<<1}else{c[b+4>>2]=d}a[k+d|0]=0;return b|0}function Cf(b,d,e,f,g,h,i,j){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;i=i|0;j=j|0;var k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0;if((-18-d|0)>>>0<e>>>0){rf(0)}if((a[b]&1)==0){k=b+1|0}else{k=c[b+8>>2]|0}do{if(d>>>0<2147483623>>>0){l=e+d|0;m=d<<1;n=l>>>0<m>>>0?m:l;if(n>>>0<11>>>0){o=11;break}o=n+16&-16}else{o=-17}}while(0);e=Rn(o)|0;if((g|0)!=0){io(e|0,k|0,g)|0}if((i|0)!=0){io(e+g|0,j|0,i)|0}j=f-h|0;if((j|0)!=(g|0)){io(e+(i+g)|0,k+(h+g)|0,j-g|0)|0}if((d|0)==10){p=b+8|0;c[p>>2]=e;q=o|1;r=b|0;c[r>>2]=q;s=j+i|0;t=b+4|0;c[t>>2]=s;u=e+s|0;a[u]=0;return}Tn(k);p=b+8|0;c[p>>2]=e;q=o|1;r=b|0;c[r>>2]=q;s=j+i|0;t=b+4|0;c[t>>2]=s;u=e+s|0;a[u]=0;return}function Df(b,d,e,f,g,h,i){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;i=i|0;var j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0;if((-17-d|0)>>>0<e>>>0){rf(0)}if((a[b]&1)==0){j=b+1|0}else{j=c[b+8>>2]|0}do{if(d>>>0<2147483623>>>0){k=e+d|0;l=d<<1;m=k>>>0<l>>>0?l:k;if(m>>>0<11>>>0){n=11;break}n=m+16&-16}else{n=-17}}while(0);e=Rn(n)|0;if((g|0)!=0){io(e|0,j|0,g)|0}m=f-h|0;if((m|0)!=(g|0)){io(e+(i+g)|0,j+(h+g)|0,m-g|0)|0}if((d|0)==10){o=b+8|0;c[o>>2]=e;p=n|1;q=b|0;c[q>>2]=p;return}Tn(j);o=b+8|0;c[o>>2]=e;p=n|1;q=b|0;c[q>>2]=p;return}function Ef(b,d,e){b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0,i=0,j=0;if(e>>>0>1073741807>>>0){rf(0)}if(e>>>0<2>>>0){a[b]=e<<1;f=b+4|0;g=mn(f,d,e)|0;h=f+(e<<2)|0;c[h>>2]=0;return}else{i=e+4&-4;j=Rn(i<<2)|0;c[b+8>>2]=j;c[b>>2]=i|1;c[b+4>>2]=e;f=j;g=mn(f,d,e)|0;h=f+(e<<2)|0;c[h>>2]=0;return}}function Ff(b,d,e){b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0,i=0,j=0;if(d>>>0>1073741807>>>0){rf(0)}if(d>>>0<2>>>0){a[b]=d<<1;f=b+4|0;g=on(f,e,d)|0;h=f+(d<<2)|0;c[h>>2]=0;return}else{i=d+4&-4;j=Rn(i<<2)|0;c[b+8>>2]=j;c[b>>2]=i|1;c[b+4>>2]=d;f=j;g=on(f,e,d)|0;h=f+(d<<2)|0;c[h>>2]=0;return}}function Gf(b){b=b|0;if((a[b]&1)==0){return}Tn(c[b+8>>2]|0);return}function Hf(a,b){a=a|0;b=b|0;return If(a,b,ln(b)|0)|0}function If(b,d,e){b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0,i=0,j=0,k=0;f=b;g=a[f]|0;if((g&1)==0){h=1;i=g}else{g=c[b>>2]|0;h=(g&-2)-1|0;i=g&255}if(h>>>0<e>>>0){g=i&255;if((g&1|0)==0){j=g>>>1}else{j=c[b+4>>2]|0}Lf(b,h,e-h|0,j,0,j,e,d);return b|0}if((i&1)==0){k=b+4|0}else{k=c[b+8>>2]|0}nn(k,d,e)|0;c[k+(e<<2)>>2]=0;if((a[f]&1)==0){a[f]=e<<1;return b|0}else{c[b+4>>2]=e;return b|0}return 0}function Jf(b,d){b=b|0;d=d|0;var e=0,f=0,g=0,h=0,i=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0;if(d>>>0>1073741807>>>0){rf(0)}e=b;f=a[e]|0;if((f&1)==0){g=1;h=f}else{f=c[b>>2]|0;g=(f&-2)-1|0;h=f&255}f=h&255;if((f&1|0)==0){i=f>>>1}else{i=c[b+4>>2]|0}f=i>>>0>d>>>0?i:d;if(f>>>0<2>>>0){j=2}else{j=f+4&-4}f=j-1|0;if((f|0)==(g|0)){return}if((f|0)==1){k=b+4|0;l=c[b+8>>2]|0;m=1;n=0}else{d=j<<2;if(f>>>0>g>>>0){o=Rn(d)|0}else{o=Rn(d)|0}d=h&1;if(d<<24>>24==0){p=b+4|0}else{p=c[b+8>>2]|0}k=o;l=p;m=d<<24>>24!=0;n=1}d=k;k=h&255;if((k&1|0)==0){q=k>>>1}else{q=c[b+4>>2]|0}mn(d,l,q+1|0)|0;if(m){Tn(l)}if(n){c[b>>2]=j|1;c[b+4>>2]=i;c[b+8>>2]=d;return}else{a[e]=i<<1;return}}function Kf(b,d){b=b|0;d=d|0;var e=0,f=0,g=0,h=0,i=0,j=0,k=0,l=0,m=0;e=b;f=a[e]|0;if((f&1)==0){g=(f&255)>>>1;h=1}else{g=c[b+4>>2]|0;h=(c[b>>2]&-2)-1|0}if((g|0)==(h|0)){Mf(b,h,1,h,h,0,0);i=a[e]|0}else{i=f}if((i&1)==0){a[e]=(g<<1)+2;j=b+4|0;k=g+1|0;l=j+(g<<2)|0;c[l>>2]=d;m=j+(k<<2)|0;c[m>>2]=0;return}else{e=c[b+8>>2]|0;i=g+1|0;c[b+4>>2]=i;j=e;k=i;l=j+(g<<2)|0;c[l>>2]=d;m=j+(k<<2)|0;c[m>>2]=0;return}}function Lf(b,d,e,f,g,h,i,j){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;i=i|0;j=j|0;var k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0;if((1073741806-d|0)>>>0<e>>>0){rf(0)}if((a[b]&1)==0){k=b+4|0}else{k=c[b+8>>2]|0}do{if(d>>>0<536870887>>>0){l=e+d|0;m=d<<1;n=l>>>0<m>>>0?m:l;if(n>>>0<2>>>0){o=2;break}o=n+4&-4}else{o=1073741807}}while(0);e=Rn(o<<2)|0;if((g|0)!=0){mn(e,k,g)|0}if((i|0)!=0){mn(e+(g<<2)|0,j,i)|0}j=f-h|0;if((j|0)!=(g|0)){mn(e+(i+g<<2)|0,k+(h+g<<2)|0,j-g|0)|0}if((d|0)==1){p=b+8|0;c[p>>2]=e;q=o|1;r=b|0;c[r>>2]=q;s=j+i|0;t=b+4|0;c[t>>2]=s;u=e+(s<<2)|0;c[u>>2]=0;return}Tn(k);p=b+8|0;c[p>>2]=e;q=o|1;r=b|0;c[r>>2]=q;s=j+i|0;t=b+4|0;c[t>>2]=s;u=e+(s<<2)|0;c[u>>2]=0;return}function Mf(b,d,e,f,g,h,i){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;i=i|0;var j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0;if((1073741807-d|0)>>>0<e>>>0){rf(0)}if((a[b]&1)==0){j=b+4|0}else{j=c[b+8>>2]|0}do{if(d>>>0<536870887>>>0){k=e+d|0;l=d<<1;m=k>>>0<l>>>0?l:k;if(m>>>0<2>>>0){n=2;break}n=m+4&-4}else{n=1073741807}}while(0);e=Rn(n<<2)|0;if((g|0)!=0){mn(e,j,g)|0}m=f-h|0;if((m|0)!=(g|0)){mn(e+(i+g<<2)|0,j+(h+g<<2)|0,m-g|0)|0}if((d|0)==1){o=b+8|0;c[o>>2]=e;p=n|1;q=b|0;c[q>>2]=p;return}Tn(j);o=b+8|0;c[o>>2]=e;p=n|1;q=b|0;c[q>>2]=p;return}function Nf(b,d){b=b|0;d=d|0;var e=0,f=0,g=0;e=i;i=i+8|0;f=e|0;g=(c[b+24>>2]|0)==0;if(g){c[b+16>>2]=d|1}else{c[b+16>>2]=d}if(((g&1|d)&c[b+20>>2]|0)==0){i=e;return}e=Nc(16)|0;do{if((a[36472]|0)==0){if((yb(36472)|0)==0){break}c[8324]=6064;gb(68,33296,u|0)|0}}while(0);b=qo(33296,0,32)|0;c[f>>2]=b|1;c[f+4>>2]=K;nf(e,f,3160);c[e>>2]=5248;Qb(e|0,10768,32)}function Of(a){a=a|0;var b=0,d=0,e=0,f=0;c[a>>2]=5224;b=c[a+40>>2]|0;d=a+32|0;e=a+36|0;if((b|0)!=0){f=b;do{f=f-1|0;fd[c[(c[d>>2]|0)+(f<<2)>>2]&7](0,a,c[(c[e>>2]|0)+(f<<2)>>2]|0);}while((f|0)!=0)}Yk(a+28|0);Mn(c[d>>2]|0);Mn(c[e>>2]|0);Mn(c[a+48>>2]|0);Mn(c[a+60>>2]|0);return}function Pf(a,b){a=a|0;b=b|0;Xk(a,b+28|0);return}function Qf(a,b){a=a|0;b=b|0;c[a+24>>2]=b;c[a+16>>2]=(b|0)==0;c[a+20>>2]=0;c[a+4>>2]=4098;c[a+12>>2]=0;c[a+8>>2]=6;ko(a+32|0,0,40)|0;Wk(a+28|0);return}function Rf(a){a=a|0;c[a>>2]=6296;Yk(a+4|0);Tn(a);return}function Sf(a){a=a|0;c[a>>2]=6296;Yk(a+4|0);return}function Tf(a,b){a=a|0;b=b|0;return}function Uf(a,b,c){a=a|0;b=b|0;c=c|0;return a|0}function Vf(a,b,d,e,f,g){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;g=a;c[g>>2]=0;c[g+4>>2]=0;g=a+8|0;c[g>>2]=-1;c[g+4>>2]=-1;return}function Wf(a,b,d,e){a=a|0;b=b|0;d=d|0;e=e|0;e=i;b=d;d=i;i=i+16|0;c[d>>2]=c[b>>2];c[d+4>>2]=c[b+4>>2];c[d+8>>2]=c[b+8>>2];c[d+12>>2]=c[b+12>>2];b=a;c[b>>2]=0;c[b+4>>2]=0;b=a+8|0;c[b>>2]=-1;c[b+4>>2]=-1;i=e;return}function Xf(a){a=a|0;return 0}function Yf(a){a=a|0;return 0}function Zf(b,d,e){b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0,i=0,j=0,k=0,l=0,m=0;f=b;if((e|0)<=0){g=0;return g|0}h=b+12|0;i=b+16|0;j=d;d=0;while(1){k=c[h>>2]|0;if(k>>>0<(c[i>>2]|0)>>>0){c[h>>2]=k+1;l=a[k]|0}else{k=dd[c[(c[f>>2]|0)+40>>2]&127](b)|0;if((k|0)==-1){g=d;m=11;break}l=k&255}a[j]=l;k=d+1|0;if((k|0)<(e|0)){j=j+1|0;d=k}else{g=k;m=9;break}}if((m|0)==11){return g|0}else if((m|0)==9){return g|0}return 0}function _f(a){a=a|0;return-1|0}function $f(a){a=a|0;var b=0,e=0;if((dd[c[(c[a>>2]|0)+36>>2]&127](a)|0)==-1){b=-1;return b|0}e=a+12|0;a=c[e>>2]|0;c[e>>2]=a+1;b=d[a]|0;return b|0}function ag(a,b){a=a|0;b=b|0;return-1|0}function bg(b,e,f){b=b|0;e=e|0;f=f|0;var g=0,h=0,i=0,j=0,k=0,l=0,m=0,n=0;g=b;if((f|0)<=0){h=0;return h|0}i=b+24|0;j=b+28|0;k=0;l=e;while(1){e=c[i>>2]|0;if(e>>>0<(c[j>>2]|0)>>>0){m=a[l]|0;c[i>>2]=e+1;a[e]=m}else{if((ad[c[(c[g>>2]|0)+52>>2]&31](b,d[l]|0)|0)==-1){h=k;n=8;break}}m=k+1|0;if((m|0)<(f|0)){k=m;l=l+1|0}else{h=m;n=10;break}}if((n|0)==10){return h|0}else if((n|0)==8){return h|0}return 0}function cg(a,b){a=a|0;b=b|0;return-1|0}function dg(a){a=a|0;c[a>>2]=6224;Yk(a+4|0);Tn(a);return}function eg(a){a=a|0;c[a>>2]=6224;Yk(a+4|0);return}function fg(a,b){a=a|0;b=b|0;return}function gg(a,b,c){a=a|0;b=b|0;c=c|0;return a|0}function hg(a,b,d,e,f,g){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;g=a;c[g>>2]=0;c[g+4>>2]=0;g=a+8|0;c[g>>2]=-1;c[g+4>>2]=-1;return}function ig(a,b,d,e){a=a|0;b=b|0;d=d|0;e=e|0;e=i;b=d;d=i;i=i+16|0;c[d>>2]=c[b>>2];c[d+4>>2]=c[b+4>>2];c[d+8>>2]=c[b+8>>2];c[d+12>>2]=c[b+12>>2];b=a;c[b>>2]=0;c[b+4>>2]=0;b=a+8|0;c[b>>2]=-1;c[b+4>>2]=-1;i=e;return}function jg(a){a=a|0;return 0}function kg(a){a=a|0;return 0}function lg(a,b,d){a=a|0;b=b|0;d=d|0;var e=0,f=0,g=0,h=0,i=0,j=0,k=0,l=0;e=a;if((d|0)<=0){f=0;return f|0}g=a+12|0;h=a+16|0;i=b;b=0;while(1){j=c[g>>2]|0;if(j>>>0<(c[h>>2]|0)>>>0){c[g>>2]=j+4;k=c[j>>2]|0}else{j=dd[c[(c[e>>2]|0)+40>>2]&127](a)|0;if((j|0)==-1){f=b;l=9;break}else{k=j}}c[i>>2]=k;j=b+1|0;if((j|0)<(d|0)){i=i+4|0;b=j}else{f=j;l=8;break}}if((l|0)==9){return f|0}else if((l|0)==8){return f|0}return 0}function mg(a){a=a|0;return-1|0}function ng(a){a=a|0;var b=0,d=0;if((dd[c[(c[a>>2]|0)+36>>2]&127](a)|0)==-1){b=-1;return b|0}d=a+12|0;a=c[d>>2]|0;c[d>>2]=a+4;b=c[a>>2]|0;return b|0}function og(a,b){a=a|0;b=b|0;return-1|0}function pg(a,b,d){a=a|0;b=b|0;d=d|0;var e=0,f=0,g=0,h=0,i=0,j=0,k=0,l=0;e=a;if((d|0)<=0){f=0;return f|0}g=a+24|0;h=a+28|0;i=0;j=b;while(1){b=c[g>>2]|0;if(b>>>0<(c[h>>2]|0)>>>0){k=c[j>>2]|0;c[g>>2]=b+4;c[b>>2]=k}else{if((ad[c[(c[e>>2]|0)+52>>2]&31](a,c[j>>2]|0)|0)==-1){f=i;l=10;break}}k=i+1|0;if((k|0)<(d|0)){i=k;j=j+4|0}else{f=k;l=8;break}}if((l|0)==8){return f|0}else if((l|0)==10){return f|0}return 0}function qg(a,b){a=a|0;b=b|0;return-1|0}function rg(a){a=a|0;Of(a+8|0);Tn(a);return}function sg(a){a=a|0;Of(a+8|0);return}function tg(a){a=a|0;var b=0,d=0;b=a;d=c[(c[a>>2]|0)-12>>2]|0;Of(b+(d+8)|0);Tn(b+d|0);return}function ug(a){a=a|0;Of(a+((c[(c[a>>2]|0)-12>>2]|0)+8)|0);return}function vg(b){b=b|0;var d=0,e=0,f=0,g=0,h=0,j=0,k=0;d=i;i=i+8|0;e=d|0;f=b;g=c[(c[f>>2]|0)-12>>2]|0;h=b;if((c[h+(g+24)>>2]|0)==0){i=d;return b|0}j=e|0;a[j]=0;c[e+4>>2]=b;do{if((c[h+(g+16)>>2]|0)==0){k=c[h+(g+72)>>2]|0;if((k|0)!=0){vg(k)|0}a[j]=1;k=c[h+((c[(c[f>>2]|0)-12>>2]|0)+24)>>2]|0;if((dd[c[(c[k>>2]|0)+24>>2]&127](k)|0)!=-1){break}k=c[(c[f>>2]|0)-12>>2]|0;Nf(h+k|0,c[h+(k+16)>>2]|1)}}while(0);Fg(e);i=d;return b|0}function wg(a){a=a|0;Of(a+8|0);Tn(a);return}function xg(a){a=a|0;Of(a+8|0);return}function yg(a){a=a|0;var b=0,d=0;b=a;d=c[(c[a>>2]|0)-12>>2]|0;Of(b+(d+8)|0);Tn(b+d|0);return}function zg(a){a=a|0;Of(a+((c[(c[a>>2]|0)-12>>2]|0)+8)|0);return}function Ag(b){b=b|0;var d=0,e=0,f=0,g=0,h=0,j=0,k=0;d=i;i=i+8|0;e=d|0;f=b;g=c[(c[f>>2]|0)-12>>2]|0;h=b;if((c[h+(g+24)>>2]|0)==0){i=d;return b|0}j=e|0;a[j]=0;c[e+4>>2]=b;do{if((c[h+(g+16)>>2]|0)==0){k=c[h+(g+72)>>2]|0;if((k|0)!=0){Ag(k)|0}a[j]=1;k=c[h+((c[(c[f>>2]|0)-12>>2]|0)+24)>>2]|0;if((dd[c[(c[k>>2]|0)+24>>2]&127](k)|0)!=-1){break}k=c[(c[f>>2]|0)-12>>2]|0;Nf(h+k|0,c[h+(k+16)>>2]|1)}}while(0);Kg(e);i=d;return b|0}function Bg(a){a=a|0;Of(a+4|0);Tn(a);return}function Cg(a){a=a|0;Of(a+4|0);return}function Dg(a){a=a|0;var b=0,d=0;b=a;d=c[(c[a>>2]|0)-12>>2]|0;Of(b+(d+4)|0);Tn(b+d|0);return}function Eg(a){a=a|0;Of(a+((c[(c[a>>2]|0)-12>>2]|0)+4)|0);return}function Fg(a){a=a|0;var b=0,d=0,e=0;b=a+4|0;a=c[b>>2]|0;d=c[(c[a>>2]|0)-12>>2]|0;e=a;if((c[e+(d+24)>>2]|0)==0){return}if((c[e+(d+16)>>2]|0)!=0){return}if((c[e+(d+4)>>2]&8192|0)==0){return}if(Cb()|0){return}d=c[b>>2]|0;e=c[d+((c[(c[d>>2]|0)-12>>2]|0)+24)>>2]|0;if((dd[c[(c[e>>2]|0)+24>>2]&127](e)|0)!=-1){return}e=c[b>>2]|0;b=c[(c[e>>2]|0)-12>>2]|0;d=e;Nf(d+b|0,c[d+(b+16)>>2]|1);return}function Gg(a){a=a|0;Of(a+4|0);Tn(a);return}function Hg(a){a=a|0;Of(a+4|0);return}function Ig(a){a=a|0;var b=0,d=0;b=a;d=c[(c[a>>2]|0)-12>>2]|0;Of(b+(d+4)|0);Tn(b+d|0);return}function Jg(a){a=a|0;Of(a+((c[(c[a>>2]|0)-12>>2]|0)+4)|0);return}function Kg(a){a=a|0;var b=0,d=0,e=0;b=a+4|0;a=c[b>>2]|0;d=c[(c[a>>2]|0)-12>>2]|0;e=a;if((c[e+(d+24)>>2]|0)==0){return}if((c[e+(d+16)>>2]|0)!=0){return}if((c[e+(d+4)>>2]&8192|0)==0){return}if(Cb()|0){return}d=c[b>>2]|0;e=c[d+((c[(c[d>>2]|0)-12>>2]|0)+24)>>2]|0;if((dd[c[(c[e>>2]|0)+24>>2]&127](e)|0)!=-1){return}e=c[b>>2]|0;b=c[(c[e>>2]|0)-12>>2]|0;d=e;Nf(d+b|0,c[d+(b+16)>>2]|1);return}function Lg(a){a=a|0;return 3312}function Mg(a,b,c){a=a|0;b=b|0;c=c|0;if((c|0)==1){tf(a,3656,35);return}else{lf(a,b|0,c);return}}function Ng(a){a=a|0;gf(a|0);return}function Og(a){a=a|0;pf(a|0);Tn(a);return}function Pg(a){a=a|0;pf(a|0);return}function Qg(a){a=a|0;Of(a);Tn(a);return}function Rg(a){a=a|0;gf(a|0);Tn(a);return}function Sg(a){a=a|0;Ve(a|0);Tn(a);return}function Tg(a){a=a|0;Ve(a|0);return}function Ug(a){a=a|0;Ve(a|0);return}function Vg(b,c,d,e,f){b=b|0;c=c|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,i=0,j=0,k=0,l=0;L1:do{if((e|0)==(f|0)){g=c}else{b=c;h=e;while(1){if((b|0)==(d|0)){i=-1;j=9;break}k=a[b]|0;l=a[h]|0;if(k<<24>>24<l<<24>>24){i=-1;j=11;break}if(l<<24>>24<k<<24>>24){i=1;j=10;break}k=b+1|0;l=h+1|0;if((l|0)==(f|0)){g=k;break L1}else{b=k;h=l}}if((j|0)==10){return i|0}else if((j|0)==11){return i|0}else if((j|0)==9){return i|0}}}while(0);i=(g|0)!=(d|0)|0;return i|0}function Wg(b,d,e,f){b=b|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,i=0,j=0,k=0;d=e;g=f-d|0;if(g>>>0>4294967279>>>0){rf(b)}if(g>>>0<11>>>0){a[b]=g<<1;h=b+1|0}else{i=g+16&-16;j=Rn(i)|0;c[b+8>>2]=j;c[b>>2]=i|1;c[b+4>>2]=g;h=j}if((e|0)==(f|0)){k=h;a[k]=0;return}j=f+(-d|0)|0;d=h;g=e;while(1){a[d]=a[g]|0;e=g+1|0;if((e|0)==(f|0)){break}else{d=d+1|0;g=e}}k=h+j|0;a[k]=0;return}function Xg(b,c,d){b=b|0;c=c|0;d=d|0;var e=0,f=0,g=0,h=0;if((c|0)==(d|0)){e=0;return e|0}else{f=c;g=0}while(1){c=(a[f]|0)+(g<<4)|0;b=c&-268435456;h=(b>>>24|b)^c;c=f+1|0;if((c|0)==(d|0)){e=h;break}else{f=c;g=h}}return e|0}function Yg(a){a=a|0;Ve(a|0);Tn(a);return}function Zg(a){a=a|0;Ve(a|0);return}function _g(a,b,d,e,f){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,i=0,j=0,k=0,l=0;L1:do{if((e|0)==(f|0)){g=b;h=6}else{a=b;i=e;while(1){if((a|0)==(d|0)){j=-1;break L1}k=c[a>>2]|0;l=c[i>>2]|0;if((k|0)<(l|0)){j=-1;break L1}if((l|0)<(k|0)){j=1;break L1}k=a+4|0;l=i+4|0;if((l|0)==(f|0)){g=k;h=6;break}else{a=k;i=l}}}}while(0);if((h|0)==6){j=(g|0)!=(d|0)|0}return j|0}function $g(b,d,e,f){b=b|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,i=0,j=0,k=0;d=e;g=f-d|0;h=g>>2;if(h>>>0>1073741807>>>0){rf(b)}if(h>>>0<2>>>0){a[b]=g>>>1;i=b+4|0}else{g=h+4&-4;j=Rn(g<<2)|0;c[b+8>>2]=j;c[b>>2]=g|1;c[b+4>>2]=h;i=j}if((e|0)==(f|0)){k=i;c[k>>2]=0;return}j=(f-4+(-d|0)|0)>>>2;d=i;h=e;while(1){c[d>>2]=c[h>>2];e=h+4|0;if((e|0)==(f|0)){break}else{d=d+4|0;h=e}}k=i+(j+1<<2)|0;c[k>>2]=0;return}function ah(a,b,d){a=a|0;b=b|0;d=d|0;var e=0,f=0,g=0,h=0;if((b|0)==(d|0)){e=0;return e|0}else{f=b;g=0}while(1){b=(c[f>>2]|0)+(g<<4)|0;a=b&-268435456;h=(a>>>24|a)^b;b=f+4|0;if((b|0)==(d|0)){e=h;break}else{f=b;g=h}}return e|0}function bh(a){a=a|0;Ve(a|0);Tn(a);return}function ch(a){a=a|0;Ve(a|0);return}function dh(b,d,e,f,g,h,j){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;var k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0;k=i;i=i+112|0;l=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[l>>2];l=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[l>>2];l=k|0;m=k+16|0;n=k+32|0;o=k+40|0;p=k+48|0;q=k+56|0;r=k+64|0;s=k+72|0;t=k+80|0;u=k+104|0;if((c[g+4>>2]&1|0)==0){c[n>>2]=-1;v=c[(c[d>>2]|0)+16>>2]|0;w=e|0;c[p>>2]=c[w>>2];c[q>>2]=c[f>>2];Zc[v&127](o,d,p,q,g,h,n);q=c[o>>2]|0;c[w>>2]=q;w=c[n>>2]|0;if((w|0)==0){a[j]=0}else if((w|0)==1){a[j]=1}else{a[j]=1;c[h>>2]=4}c[b>>2]=q;i=k;return}Pf(r,g);q=r|0;r=c[q>>2]|0;if((c[8976]|0)!=-1){c[m>>2]=35904;c[m+4>>2]=14;c[m+8>>2]=0;qf(35904,m,96)}m=(c[8977]|0)-1|0;w=c[r+8>>2]|0;do{if((c[r+12>>2]|0)-w>>2>>>0>m>>>0){n=c[w+(m<<2)>>2]|0;if((n|0)==0){break}o=n;Xe(c[q>>2]|0)|0;Pf(s,g);n=s|0;p=c[n>>2]|0;if((c[8880]|0)!=-1){c[l>>2]=35520;c[l+4>>2]=14;c[l+8>>2]=0;qf(35520,l,96)}d=(c[8881]|0)-1|0;v=c[p+8>>2]|0;do{if((c[p+12>>2]|0)-v>>2>>>0>d>>>0){x=c[v+(d<<2)>>2]|0;if((x|0)==0){break}y=x;Xe(c[n>>2]|0)|0;z=t|0;A=x;$c[c[(c[A>>2]|0)+24>>2]&127](z,y);$c[c[(c[A>>2]|0)+28>>2]&127](t+12|0,y);c[u>>2]=c[f>>2];a[j]=(eh(e,u,z,t+24|0,o,h,1)|0)==(z|0)|0;c[b>>2]=c[e>>2];vf(t+12|0);vf(t|0);i=k;return}}while(0);o=Nc(4)|0;qn(o);Qb(o|0,10192,134)}}while(0);k=Nc(4)|0;qn(k);Qb(k|0,10192,134)}function eh(b,e,f,g,h,j,k){b=b|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;var l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0,Z=0,_=0,$=0,aa=0;l=i;i=i+104|0;m=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[m>>2];m=(g-f|0)/12|0;n=l|0;do{if(m>>>0>100>>>0){o=Ln(m)|0;if((o|0)!=0){p=o;q=o;break}Yn();p=0;q=0}else{p=n;q=0}}while(0);n=(f|0)==(g|0);if(n){r=m;s=0}else{o=m;m=0;t=p;u=f;while(1){v=d[u]|0;if((v&1|0)==0){w=v>>>1}else{w=c[u+4>>2]|0}if((w|0)==0){a[t]=2;x=m+1|0;y=o-1|0}else{a[t]=1;x=m;y=o}v=u+12|0;if((v|0)==(g|0)){r=y;s=x;break}else{o=y;m=x;t=t+1|0;u=v}}}u=b|0;b=e|0;e=h;t=0;x=s;s=r;while(1){r=c[u>>2]|0;do{if((r|0)==0){z=0}else{if((c[r+12>>2]|0)!=(c[r+16>>2]|0)){z=r;break}if((dd[c[(c[r>>2]|0)+36>>2]&127](r)|0)==-1){c[u>>2]=0;z=0;break}else{z=c[u>>2]|0;break}}}while(0);r=(z|0)==0;m=c[b>>2]|0;if((m|0)==0){A=z;B=0}else{do{if((c[m+12>>2]|0)==(c[m+16>>2]|0)){if((dd[c[(c[m>>2]|0)+36>>2]&127](m)|0)!=-1){C=m;break}c[b>>2]=0;C=0}else{C=m}}while(0);A=c[u>>2]|0;B=C}D=(B|0)==0;if(!((r^D)&(s|0)!=0)){break}m=c[A+12>>2]|0;if((m|0)==(c[A+16>>2]|0)){E=(dd[c[(c[A>>2]|0)+36>>2]&127](A)|0)&255}else{E=a[m]|0}if(k){F=E}else{F=ad[c[(c[e>>2]|0)+12>>2]&31](h,E)|0}do{if(n){G=x;H=s}else{m=t+1|0;L46:do{if(k){y=s;o=x;w=p;v=0;I=f;while(1){do{if((a[w]|0)==1){J=I;if((a[J]&1)==0){K=I+1|0}else{K=c[I+8>>2]|0}if(F<<24>>24!=(a[K+t|0]|0)){a[w]=0;L=v;M=o;N=y-1|0;break}O=d[J]|0;if((O&1|0)==0){P=O>>>1}else{P=c[I+4>>2]|0}if((P|0)!=(m|0)){L=1;M=o;N=y;break}a[w]=2;L=1;M=o+1|0;N=y-1|0}else{L=v;M=o;N=y}}while(0);O=I+12|0;if((O|0)==(g|0)){Q=N;R=M;S=L;break L46}y=N;o=M;w=w+1|0;v=L;I=O}}else{I=s;v=x;w=p;o=0;y=f;while(1){do{if((a[w]|0)==1){O=y;if((a[O]&1)==0){T=y+1|0}else{T=c[y+8>>2]|0}if(F<<24>>24!=(ad[c[(c[e>>2]|0)+12>>2]&31](h,a[T+t|0]|0)|0)<<24>>24){a[w]=0;U=o;V=v;W=I-1|0;break}J=d[O]|0;if((J&1|0)==0){X=J>>>1}else{X=c[y+4>>2]|0}if((X|0)!=(m|0)){U=1;V=v;W=I;break}a[w]=2;U=1;V=v+1|0;W=I-1|0}else{U=o;V=v;W=I}}while(0);J=y+12|0;if((J|0)==(g|0)){Q=W;R=V;S=U;break L46}I=W;v=V;w=w+1|0;o=U;y=J}}}while(0);if(!S){G=R;H=Q;break}m=c[u>>2]|0;y=m+12|0;o=c[y>>2]|0;if((o|0)==(c[m+16>>2]|0)){dd[c[(c[m>>2]|0)+40>>2]&127](m)|0}else{c[y>>2]=o+1}if((R+Q|0)>>>0<2>>>0|n){G=R;H=Q;break}o=t+1|0;y=R;m=p;w=f;while(1){do{if((a[m]|0)==2){v=d[w]|0;if((v&1|0)==0){Y=v>>>1}else{Y=c[w+4>>2]|0}if((Y|0)==(o|0)){Z=y;break}a[m]=0;Z=y-1|0}else{Z=y}}while(0);v=w+12|0;if((v|0)==(g|0)){G=Z;H=Q;break}else{y=Z;m=m+1|0;w=v}}}}while(0);t=t+1|0;x=G;s=H}do{if((A|0)==0){_=0}else{if((c[A+12>>2]|0)!=(c[A+16>>2]|0)){_=A;break}if((dd[c[(c[A>>2]|0)+36>>2]&127](A)|0)==-1){c[u>>2]=0;_=0;break}else{_=c[u>>2]|0;break}}}while(0);u=(_|0)==0;do{if(D){$=93}else{if((c[B+12>>2]|0)!=(c[B+16>>2]|0)){if(u){break}else{$=95;break}}if((dd[c[(c[B>>2]|0)+36>>2]&127](B)|0)==-1){c[b>>2]=0;$=93;break}else{if(u^(B|0)==0){break}else{$=95;break}}}}while(0);if(($|0)==93){if(u){$=95}}if(($|0)==95){c[j>>2]=c[j>>2]|2}L125:do{if(n){$=100}else{u=f;B=p;while(1){if((a[B]|0)==2){aa=u;break L125}b=u+12|0;if((b|0)==(g|0)){$=100;break L125}u=b;B=B+1|0}}}while(0);if(($|0)==100){c[j>>2]=c[j>>2]|4;aa=g}if((q|0)==0){i=l;return aa|0}Mn(q);i=l;return aa|0}function fh(a,b,d,e,f,g,h){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0;b=i;i=i+16|0;j=d;d=i;i=i+4|0;i=i+7&-8;c[d>>2]=c[j>>2];j=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[j>>2];j=b|0;k=b+8|0;c[j>>2]=c[d>>2];c[k>>2]=c[e>>2];gh(a,0,j,k,f,g,h);i=b;return}function gh(b,e,f,g,h,j,k){b=b|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;var l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0;e=i;i=i+72|0;l=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[l>>2];l=g;g=i;i=i+4|0;i=i+7&-8;c[g>>2]=c[l>>2];l=e|0;m=e+32|0;n=e+40|0;o=e+56|0;p=o;q=i;i=i+4|0;i=i+7&-8;r=i;i=i+160|0;s=i;i=i+4|0;i=i+7&-8;t=i;i=i+4|0;i=i+7&-8;u=c[h+4>>2]&74;if((u|0)==8){v=16}else if((u|0)==0){v=0}else if((u|0)==64){v=8}else{v=10}u=l|0;Yh(n,h,u,m);ko(p|0,0,12)|0;h=o;xf(o,10,0);if((a[p]&1)==0){l=h+1|0;w=l;x=l;y=o+8|0}else{l=o+8|0;w=c[l>>2]|0;x=h+1|0;y=l}c[q>>2]=w;l=r|0;c[s>>2]=l;c[t>>2]=0;h=f|0;f=g|0;g=o|0;z=o+4|0;A=a[m]|0;m=w;w=c[h>>2]|0;L11:while(1){do{if((w|0)==0){B=0}else{if((c[w+12>>2]|0)!=(c[w+16>>2]|0)){B=w;break}if((dd[c[(c[w>>2]|0)+36>>2]&127](w)|0)!=-1){B=w;break}c[h>>2]=0;B=0}}while(0);C=(B|0)==0;D=c[f>>2]|0;do{if((D|0)==0){E=21}else{if((c[D+12>>2]|0)!=(c[D+16>>2]|0)){if(C){F=D;G=0;break}else{H=m;I=D;J=0;break L11}}if((dd[c[(c[D>>2]|0)+36>>2]&127](D)|0)==-1){c[f>>2]=0;E=21;break}else{K=(D|0)==0;if(C^K){F=D;G=K;break}else{H=m;I=D;J=K;break L11}}}}while(0);if((E|0)==21){E=0;if(C){H=m;I=0;J=1;break}else{F=0;G=1}}D=d[p]|0;K=(D&1|0)==0;if(((c[q>>2]|0)-m|0)==((K?D>>>1:c[z>>2]|0)|0)){if(K){L=D>>>1;M=D>>>1}else{D=c[z>>2]|0;L=D;M=D}xf(o,L<<1,0);if((a[p]&1)==0){N=10}else{N=(c[g>>2]&-2)-1|0}xf(o,N,0);if((a[p]&1)==0){O=x}else{O=c[y>>2]|0}c[q>>2]=O+M;P=O}else{P=m}D=B+12|0;K=c[D>>2]|0;Q=B+16|0;if((K|0)==(c[Q>>2]|0)){R=(dd[c[(c[B>>2]|0)+36>>2]&127](B)|0)&255}else{R=a[K]|0}if((yh(R,v,P,q,t,A,n,l,s,u)|0)!=0){H=P;I=F;J=G;break}K=c[D>>2]|0;if((K|0)==(c[Q>>2]|0)){dd[c[(c[B>>2]|0)+40>>2]&127](B)|0;m=P;w=B;continue}else{c[D>>2]=K+1;m=P;w=B;continue}}w=d[n]|0;if((w&1|0)==0){S=w>>>1}else{S=c[n+4>>2]|0}do{if((S|0)!=0){w=c[s>>2]|0;if((w-r|0)>=160){break}P=c[t>>2]|0;c[s>>2]=w+4;c[w>>2]=P}}while(0);c[k>>2]=Um(H,c[q>>2]|0,j,v)|0;kk(n,l,c[s>>2]|0,j);do{if(C){T=0}else{if((c[B+12>>2]|0)!=(c[B+16>>2]|0)){T=B;break}if((dd[c[(c[B>>2]|0)+36>>2]&127](B)|0)!=-1){T=B;break}c[h>>2]=0;T=0}}while(0);h=(T|0)==0;L71:do{if(J){E=62}else{do{if((c[I+12>>2]|0)==(c[I+16>>2]|0)){if((dd[c[(c[I>>2]|0)+36>>2]&127](I)|0)!=-1){break}c[f>>2]=0;E=62;break L71}}while(0);if(!(h^(I|0)==0)){break}U=b|0;c[U>>2]=T;vf(o);vf(n);i=e;return}}while(0);do{if((E|0)==62){if(h){break}U=b|0;c[U>>2]=T;vf(o);vf(n);i=e;return}}while(0);c[j>>2]=c[j>>2]|2;U=b|0;c[U>>2]=T;vf(o);vf(n);i=e;return}function hh(a,b,d,e,f,g,h){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0;b=i;i=i+16|0;j=d;d=i;i=i+4|0;i=i+7&-8;c[d>>2]=c[j>>2];j=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[j>>2];j=b|0;k=b+8|0;c[j>>2]=c[d>>2];c[k>>2]=c[e>>2];ih(a,0,j,k,f,g,h);i=b;return}function ih(b,e,f,g,h,j,k){b=b|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;var l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0;e=i;i=i+72|0;l=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[l>>2];l=g;g=i;i=i+4|0;i=i+7&-8;c[g>>2]=c[l>>2];l=e|0;m=e+32|0;n=e+40|0;o=e+56|0;p=o;q=i;i=i+4|0;i=i+7&-8;r=i;i=i+160|0;s=i;i=i+4|0;i=i+7&-8;t=i;i=i+4|0;i=i+7&-8;u=c[h+4>>2]&74;if((u|0)==8){v=16}else if((u|0)==0){v=0}else if((u|0)==64){v=8}else{v=10}u=l|0;Yh(n,h,u,m);ko(p|0,0,12)|0;h=o;xf(o,10,0);if((a[p]&1)==0){l=h+1|0;w=l;x=l;y=o+8|0}else{l=o+8|0;w=c[l>>2]|0;x=h+1|0;y=l}c[q>>2]=w;l=r|0;c[s>>2]=l;c[t>>2]=0;h=f|0;f=g|0;g=o|0;z=o+4|0;A=a[m]|0;m=w;w=c[h>>2]|0;L11:while(1){do{if((w|0)==0){B=0}else{if((c[w+12>>2]|0)!=(c[w+16>>2]|0)){B=w;break}if((dd[c[(c[w>>2]|0)+36>>2]&127](w)|0)!=-1){B=w;break}c[h>>2]=0;B=0}}while(0);C=(B|0)==0;D=c[f>>2]|0;do{if((D|0)==0){E=21}else{if((c[D+12>>2]|0)!=(c[D+16>>2]|0)){if(C){F=D;G=0;break}else{H=m;I=D;J=0;break L11}}if((dd[c[(c[D>>2]|0)+36>>2]&127](D)|0)==-1){c[f>>2]=0;E=21;break}else{L=(D|0)==0;if(C^L){F=D;G=L;break}else{H=m;I=D;J=L;break L11}}}}while(0);if((E|0)==21){E=0;if(C){H=m;I=0;J=1;break}else{F=0;G=1}}D=d[p]|0;L=(D&1|0)==0;if(((c[q>>2]|0)-m|0)==((L?D>>>1:c[z>>2]|0)|0)){if(L){M=D>>>1;N=D>>>1}else{D=c[z>>2]|0;M=D;N=D}xf(o,M<<1,0);if((a[p]&1)==0){O=10}else{O=(c[g>>2]&-2)-1|0}xf(o,O,0);if((a[p]&1)==0){P=x}else{P=c[y>>2]|0}c[q>>2]=P+N;Q=P}else{Q=m}D=B+12|0;L=c[D>>2]|0;R=B+16|0;if((L|0)==(c[R>>2]|0)){S=(dd[c[(c[B>>2]|0)+36>>2]&127](B)|0)&255}else{S=a[L]|0}if((yh(S,v,Q,q,t,A,n,l,s,u)|0)!=0){H=Q;I=F;J=G;break}L=c[D>>2]|0;if((L|0)==(c[R>>2]|0)){dd[c[(c[B>>2]|0)+40>>2]&127](B)|0;m=Q;w=B;continue}else{c[D>>2]=L+1;m=Q;w=B;continue}}w=d[n]|0;if((w&1|0)==0){T=w>>>1}else{T=c[n+4>>2]|0}do{if((T|0)!=0){w=c[s>>2]|0;if((w-r|0)>=160){break}Q=c[t>>2]|0;c[s>>2]=w+4;c[w>>2]=Q}}while(0);t=Tm(H,c[q>>2]|0,j,v)|0;c[k>>2]=t;c[k+4>>2]=K;kk(n,l,c[s>>2]|0,j);do{if(C){U=0}else{if((c[B+12>>2]|0)!=(c[B+16>>2]|0)){U=B;break}if((dd[c[(c[B>>2]|0)+36>>2]&127](B)|0)!=-1){U=B;break}c[h>>2]=0;U=0}}while(0);h=(U|0)==0;L71:do{if(J){E=62}else{do{if((c[I+12>>2]|0)==(c[I+16>>2]|0)){if((dd[c[(c[I>>2]|0)+36>>2]&127](I)|0)!=-1){break}c[f>>2]=0;E=62;break L71}}while(0);if(!(h^(I|0)==0)){break}V=b|0;c[V>>2]=U;vf(o);vf(n);i=e;return}}while(0);do{if((E|0)==62){if(h){break}V=b|0;c[V>>2]=U;vf(o);vf(n);i=e;return}}while(0);c[j>>2]=c[j>>2]|2;V=b|0;c[V>>2]=U;vf(o);vf(n);i=e;return}function jh(a,b,d,e,f,g,h){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0;b=i;i=i+16|0;j=d;d=i;i=i+4|0;i=i+7&-8;c[d>>2]=c[j>>2];j=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[j>>2];j=b|0;k=b+8|0;c[j>>2]=c[d>>2];c[k>>2]=c[e>>2];kh(a,0,j,k,f,g,h);i=b;return}function kh(e,f,g,h,j,k,l){e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;l=l|0;var m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0;f=i;i=i+72|0;m=g;g=i;i=i+4|0;i=i+7&-8;c[g>>2]=c[m>>2];m=h;h=i;i=i+4|0;i=i+7&-8;c[h>>2]=c[m>>2];m=f|0;n=f+32|0;o=f+40|0;p=f+56|0;q=p;r=i;i=i+4|0;i=i+7&-8;s=i;i=i+160|0;t=i;i=i+4|0;i=i+7&-8;u=i;i=i+4|0;i=i+7&-8;v=c[j+4>>2]&74;if((v|0)==8){w=16}else if((v|0)==0){w=0}else if((v|0)==64){w=8}else{w=10}v=m|0;Yh(o,j,v,n);ko(q|0,0,12)|0;j=p;xf(p,10,0);if((a[q]&1)==0){m=j+1|0;x=m;y=m;z=p+8|0}else{m=p+8|0;x=c[m>>2]|0;y=j+1|0;z=m}c[r>>2]=x;m=s|0;c[t>>2]=m;c[u>>2]=0;j=g|0;g=h|0;h=p|0;A=p+4|0;B=a[n]|0;n=x;x=c[j>>2]|0;L11:while(1){do{if((x|0)==0){C=0}else{if((c[x+12>>2]|0)!=(c[x+16>>2]|0)){C=x;break}if((dd[c[(c[x>>2]|0)+36>>2]&127](x)|0)!=-1){C=x;break}c[j>>2]=0;C=0}}while(0);D=(C|0)==0;E=c[g>>2]|0;do{if((E|0)==0){F=21}else{if((c[E+12>>2]|0)!=(c[E+16>>2]|0)){if(D){G=E;H=0;break}else{I=n;J=E;K=0;break L11}}if((dd[c[(c[E>>2]|0)+36>>2]&127](E)|0)==-1){c[g>>2]=0;F=21;break}else{L=(E|0)==0;if(D^L){G=E;H=L;break}else{I=n;J=E;K=L;break L11}}}}while(0);if((F|0)==21){F=0;if(D){I=n;J=0;K=1;break}else{G=0;H=1}}E=d[q]|0;L=(E&1|0)==0;if(((c[r>>2]|0)-n|0)==((L?E>>>1:c[A>>2]|0)|0)){if(L){M=E>>>1;N=E>>>1}else{E=c[A>>2]|0;M=E;N=E}xf(p,M<<1,0);if((a[q]&1)==0){O=10}else{O=(c[h>>2]&-2)-1|0}xf(p,O,0);if((a[q]&1)==0){P=y}else{P=c[z>>2]|0}c[r>>2]=P+N;Q=P}else{Q=n}E=C+12|0;L=c[E>>2]|0;R=C+16|0;if((L|0)==(c[R>>2]|0)){S=(dd[c[(c[C>>2]|0)+36>>2]&127](C)|0)&255}else{S=a[L]|0}if((yh(S,w,Q,r,u,B,o,m,t,v)|0)!=0){I=Q;J=G;K=H;break}L=c[E>>2]|0;if((L|0)==(c[R>>2]|0)){dd[c[(c[C>>2]|0)+40>>2]&127](C)|0;n=Q;x=C;continue}else{c[E>>2]=L+1;n=Q;x=C;continue}}x=d[o]|0;if((x&1|0)==0){T=x>>>1}else{T=c[o+4>>2]|0}do{if((T|0)!=0){x=c[t>>2]|0;if((x-s|0)>=160){break}Q=c[u>>2]|0;c[t>>2]=x+4;c[x>>2]=Q}}while(0);b[l>>1]=Sm(I,c[r>>2]|0,k,w)|0;kk(o,m,c[t>>2]|0,k);do{if(D){U=0}else{if((c[C+12>>2]|0)!=(c[C+16>>2]|0)){U=C;break}if((dd[c[(c[C>>2]|0)+36>>2]&127](C)|0)!=-1){U=C;break}c[j>>2]=0;U=0}}while(0);j=(U|0)==0;L71:do{if(K){F=62}else{do{if((c[J+12>>2]|0)==(c[J+16>>2]|0)){if((dd[c[(c[J>>2]|0)+36>>2]&127](J)|0)!=-1){break}c[g>>2]=0;F=62;break L71}}while(0);if(!(j^(J|0)==0)){break}V=e|0;c[V>>2]=U;vf(p);vf(o);i=f;return}}while(0);do{if((F|0)==62){if(j){break}V=e|0;c[V>>2]=U;vf(p);vf(o);i=f;return}}while(0);c[k>>2]=c[k>>2]|2;V=e|0;c[V>>2]=U;vf(p);vf(o);i=f;return}function lh(a,b,d,e,f,g,h){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0;b=i;i=i+16|0;j=d;d=i;i=i+4|0;i=i+7&-8;c[d>>2]=c[j>>2];j=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[j>>2];j=b|0;k=b+8|0;c[j>>2]=c[d>>2];c[k>>2]=c[e>>2];mh(a,0,j,k,f,g,h);i=b;return}function mh(b,e,f,g,h,j,k){b=b|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;var l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0;e=i;i=i+72|0;l=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[l>>2];l=g;g=i;i=i+4|0;i=i+7&-8;c[g>>2]=c[l>>2];l=e|0;m=e+32|0;n=e+40|0;o=e+56|0;p=o;q=i;i=i+4|0;i=i+7&-8;r=i;i=i+160|0;s=i;i=i+4|0;i=i+7&-8;t=i;i=i+4|0;i=i+7&-8;u=c[h+4>>2]&74;if((u|0)==8){v=16}else if((u|0)==64){v=8}else if((u|0)==0){v=0}else{v=10}u=l|0;Yh(n,h,u,m);ko(p|0,0,12)|0;h=o;xf(o,10,0);if((a[p]&1)==0){l=h+1|0;w=l;x=l;y=o+8|0}else{l=o+8|0;w=c[l>>2]|0;x=h+1|0;y=l}c[q>>2]=w;l=r|0;c[s>>2]=l;c[t>>2]=0;h=f|0;f=g|0;g=o|0;z=o+4|0;A=a[m]|0;m=w;w=c[h>>2]|0;L11:while(1){do{if((w|0)==0){B=0}else{if((c[w+12>>2]|0)!=(c[w+16>>2]|0)){B=w;break}if((dd[c[(c[w>>2]|0)+36>>2]&127](w)|0)!=-1){B=w;break}c[h>>2]=0;B=0}}while(0);C=(B|0)==0;D=c[f>>2]|0;do{if((D|0)==0){E=21}else{if((c[D+12>>2]|0)!=(c[D+16>>2]|0)){if(C){F=D;G=0;break}else{H=m;I=D;J=0;break L11}}if((dd[c[(c[D>>2]|0)+36>>2]&127](D)|0)==-1){c[f>>2]=0;E=21;break}else{K=(D|0)==0;if(C^K){F=D;G=K;break}else{H=m;I=D;J=K;break L11}}}}while(0);if((E|0)==21){E=0;if(C){H=m;I=0;J=1;break}else{F=0;G=1}}D=d[p]|0;K=(D&1|0)==0;if(((c[q>>2]|0)-m|0)==((K?D>>>1:c[z>>2]|0)|0)){if(K){L=D>>>1;M=D>>>1}else{D=c[z>>2]|0;L=D;M=D}xf(o,L<<1,0);if((a[p]&1)==0){N=10}else{N=(c[g>>2]&-2)-1|0}xf(o,N,0);if((a[p]&1)==0){O=x}else{O=c[y>>2]|0}c[q>>2]=O+M;P=O}else{P=m}D=B+12|0;K=c[D>>2]|0;Q=B+16|0;if((K|0)==(c[Q>>2]|0)){R=(dd[c[(c[B>>2]|0)+36>>2]&127](B)|0)&255}else{R=a[K]|0}if((yh(R,v,P,q,t,A,n,l,s,u)|0)!=0){H=P;I=F;J=G;break}K=c[D>>2]|0;if((K|0)==(c[Q>>2]|0)){dd[c[(c[B>>2]|0)+40>>2]&127](B)|0;m=P;w=B;continue}else{c[D>>2]=K+1;m=P;w=B;continue}}w=d[n]|0;if((w&1|0)==0){S=w>>>1}else{S=c[n+4>>2]|0}do{if((S|0)!=0){w=c[s>>2]|0;if((w-r|0)>=160){break}P=c[t>>2]|0;c[s>>2]=w+4;c[w>>2]=P}}while(0);c[k>>2]=Rm(H,c[q>>2]|0,j,v)|0;kk(n,l,c[s>>2]|0,j);do{if(C){T=0}else{if((c[B+12>>2]|0)!=(c[B+16>>2]|0)){T=B;break}if((dd[c[(c[B>>2]|0)+36>>2]&127](B)|0)!=-1){T=B;break}c[h>>2]=0;T=0}}while(0);h=(T|0)==0;L71:do{if(J){E=62}else{do{if((c[I+12>>2]|0)==(c[I+16>>2]|0)){if((dd[c[(c[I>>2]|0)+36>>2]&127](I)|0)!=-1){break}c[f>>2]=0;E=62;break L71}}while(0);if(!(h^(I|0)==0)){break}U=b|0;c[U>>2]=T;vf(o);vf(n);i=e;return}}while(0);do{if((E|0)==62){if(h){break}U=b|0;c[U>>2]=T;vf(o);vf(n);i=e;return}}while(0);c[j>>2]=c[j>>2]|2;U=b|0;c[U>>2]=T;vf(o);vf(n);i=e;return}function nh(a,b,d,e,f,g,h){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0;b=i;i=i+16|0;j=d;d=i;i=i+4|0;i=i+7&-8;c[d>>2]=c[j>>2];j=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[j>>2];j=b|0;k=b+8|0;c[j>>2]=c[d>>2];c[k>>2]=c[e>>2];oh(a,0,j,k,f,g,h);i=b;return}function oh(b,e,f,g,h,j,k){b=b|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;var l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0;e=i;i=i+72|0;l=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[l>>2];l=g;g=i;i=i+4|0;i=i+7&-8;c[g>>2]=c[l>>2];l=e|0;m=e+32|0;n=e+40|0;o=e+56|0;p=o;q=i;i=i+4|0;i=i+7&-8;r=i;i=i+160|0;s=i;i=i+4|0;i=i+7&-8;t=i;i=i+4|0;i=i+7&-8;u=c[h+4>>2]&74;if((u|0)==0){v=0}else if((u|0)==64){v=8}else if((u|0)==8){v=16}else{v=10}u=l|0;Yh(n,h,u,m);ko(p|0,0,12)|0;h=o;xf(o,10,0);if((a[p]&1)==0){l=h+1|0;w=l;x=l;y=o+8|0}else{l=o+8|0;w=c[l>>2]|0;x=h+1|0;y=l}c[q>>2]=w;l=r|0;c[s>>2]=l;c[t>>2]=0;h=f|0;f=g|0;g=o|0;z=o+4|0;A=a[m]|0;m=w;w=c[h>>2]|0;L11:while(1){do{if((w|0)==0){B=0}else{if((c[w+12>>2]|0)!=(c[w+16>>2]|0)){B=w;break}if((dd[c[(c[w>>2]|0)+36>>2]&127](w)|0)!=-1){B=w;break}c[h>>2]=0;B=0}}while(0);C=(B|0)==0;D=c[f>>2]|0;do{if((D|0)==0){E=21}else{if((c[D+12>>2]|0)!=(c[D+16>>2]|0)){if(C){F=D;G=0;break}else{H=m;I=D;J=0;break L11}}if((dd[c[(c[D>>2]|0)+36>>2]&127](D)|0)==-1){c[f>>2]=0;E=21;break}else{K=(D|0)==0;if(C^K){F=D;G=K;break}else{H=m;I=D;J=K;break L11}}}}while(0);if((E|0)==21){E=0;if(C){H=m;I=0;J=1;break}else{F=0;G=1}}D=d[p]|0;K=(D&1|0)==0;if(((c[q>>2]|0)-m|0)==((K?D>>>1:c[z>>2]|0)|0)){if(K){L=D>>>1;M=D>>>1}else{D=c[z>>2]|0;L=D;M=D}xf(o,L<<1,0);if((a[p]&1)==0){N=10}else{N=(c[g>>2]&-2)-1|0}xf(o,N,0);if((a[p]&1)==0){O=x}else{O=c[y>>2]|0}c[q>>2]=O+M;P=O}else{P=m}D=B+12|0;K=c[D>>2]|0;Q=B+16|0;if((K|0)==(c[Q>>2]|0)){R=(dd[c[(c[B>>2]|0)+36>>2]&127](B)|0)&255}else{R=a[K]|0}if((yh(R,v,P,q,t,A,n,l,s,u)|0)!=0){H=P;I=F;J=G;break}K=c[D>>2]|0;if((K|0)==(c[Q>>2]|0)){dd[c[(c[B>>2]|0)+40>>2]&127](B)|0;m=P;w=B;continue}else{c[D>>2]=K+1;m=P;w=B;continue}}w=d[n]|0;if((w&1|0)==0){S=w>>>1}else{S=c[n+4>>2]|0}do{if((S|0)!=0){w=c[s>>2]|0;if((w-r|0)>=160){break}P=c[t>>2]|0;c[s>>2]=w+4;c[w>>2]=P}}while(0);c[k>>2]=Qm(H,c[q>>2]|0,j,v)|0;kk(n,l,c[s>>2]|0,j);do{if(C){T=0}else{if((c[B+12>>2]|0)!=(c[B+16>>2]|0)){T=B;break}if((dd[c[(c[B>>2]|0)+36>>2]&127](B)|0)!=-1){T=B;break}c[h>>2]=0;T=0}}while(0);h=(T|0)==0;L71:do{if(J){E=62}else{do{if((c[I+12>>2]|0)==(c[I+16>>2]|0)){if((dd[c[(c[I>>2]|0)+36>>2]&127](I)|0)!=-1){break}c[f>>2]=0;E=62;break L71}}while(0);if(!(h^(I|0)==0)){break}U=b|0;c[U>>2]=T;vf(o);vf(n);i=e;return}}while(0);do{if((E|0)==62){if(h){break}U=b|0;c[U>>2]=T;vf(o);vf(n);i=e;return}}while(0);c[j>>2]=c[j>>2]|2;U=b|0;c[U>>2]=T;vf(o);vf(n);i=e;return}function ph(a,b,d,e,f,g,h){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0;b=i;i=i+16|0;j=d;d=i;i=i+4|0;i=i+7&-8;c[d>>2]=c[j>>2];j=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[j>>2];j=b|0;k=b+8|0;c[j>>2]=c[d>>2];c[k>>2]=c[e>>2];qh(a,0,j,k,f,g,h);i=b;return}function qh(b,e,f,g,h,j,k){b=b|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;var l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0;e=i;i=i+72|0;l=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[l>>2];l=g;g=i;i=i+4|0;i=i+7&-8;c[g>>2]=c[l>>2];l=e|0;m=e+32|0;n=e+40|0;o=e+56|0;p=o;q=i;i=i+4|0;i=i+7&-8;r=i;i=i+160|0;s=i;i=i+4|0;i=i+7&-8;t=i;i=i+4|0;i=i+7&-8;u=c[h+4>>2]&74;if((u|0)==8){v=16}else if((u|0)==0){v=0}else if((u|0)==64){v=8}else{v=10}u=l|0;Yh(n,h,u,m);ko(p|0,0,12)|0;h=o;xf(o,10,0);if((a[p]&1)==0){l=h+1|0;w=l;x=l;y=o+8|0}else{l=o+8|0;w=c[l>>2]|0;x=h+1|0;y=l}c[q>>2]=w;l=r|0;c[s>>2]=l;c[t>>2]=0;h=f|0;f=g|0;g=o|0;z=o+4|0;A=a[m]|0;m=w;w=c[h>>2]|0;L11:while(1){do{if((w|0)==0){B=0}else{if((c[w+12>>2]|0)!=(c[w+16>>2]|0)){B=w;break}if((dd[c[(c[w>>2]|0)+36>>2]&127](w)|0)!=-1){B=w;break}c[h>>2]=0;B=0}}while(0);C=(B|0)==0;D=c[f>>2]|0;do{if((D|0)==0){E=21}else{if((c[D+12>>2]|0)!=(c[D+16>>2]|0)){if(C){F=D;G=0;break}else{H=m;I=D;J=0;break L11}}if((dd[c[(c[D>>2]|0)+36>>2]&127](D)|0)==-1){c[f>>2]=0;E=21;break}else{L=(D|0)==0;if(C^L){F=D;G=L;break}else{H=m;I=D;J=L;break L11}}}}while(0);if((E|0)==21){E=0;if(C){H=m;I=0;J=1;break}else{F=0;G=1}}D=d[p]|0;L=(D&1|0)==0;if(((c[q>>2]|0)-m|0)==((L?D>>>1:c[z>>2]|0)|0)){if(L){M=D>>>1;N=D>>>1}else{D=c[z>>2]|0;M=D;N=D}xf(o,M<<1,0);if((a[p]&1)==0){O=10}else{O=(c[g>>2]&-2)-1|0}xf(o,O,0);if((a[p]&1)==0){P=x}else{P=c[y>>2]|0}c[q>>2]=P+N;Q=P}else{Q=m}D=B+12|0;L=c[D>>2]|0;R=B+16|0;if((L|0)==(c[R>>2]|0)){S=(dd[c[(c[B>>2]|0)+36>>2]&127](B)|0)&255}else{S=a[L]|0}if((yh(S,v,Q,q,t,A,n,l,s,u)|0)!=0){H=Q;I=F;J=G;break}L=c[D>>2]|0;if((L|0)==(c[R>>2]|0)){dd[c[(c[B>>2]|0)+40>>2]&127](B)|0;m=Q;w=B;continue}else{c[D>>2]=L+1;m=Q;w=B;continue}}w=d[n]|0;if((w&1|0)==0){T=w>>>1}else{T=c[n+4>>2]|0}do{if((T|0)!=0){w=c[s>>2]|0;if((w-r|0)>=160){break}Q=c[t>>2]|0;c[s>>2]=w+4;c[w>>2]=Q}}while(0);t=Pm(H,c[q>>2]|0,j,v)|0;c[k>>2]=t;c[k+4>>2]=K;kk(n,l,c[s>>2]|0,j);do{if(C){U=0}else{if((c[B+12>>2]|0)!=(c[B+16>>2]|0)){U=B;break}if((dd[c[(c[B>>2]|0)+36>>2]&127](B)|0)!=-1){U=B;break}c[h>>2]=0;U=0}}while(0);h=(U|0)==0;L71:do{if(J){E=62}else{do{if((c[I+12>>2]|0)==(c[I+16>>2]|0)){if((dd[c[(c[I>>2]|0)+36>>2]&127](I)|0)!=-1){break}c[f>>2]=0;E=62;break L71}}while(0);if(!(h^(I|0)==0)){break}V=b|0;c[V>>2]=U;vf(o);vf(n);i=e;return}}while(0);do{if((E|0)==62){if(h){break}V=b|0;c[V>>2]=U;vf(o);vf(n);i=e;return}}while(0);c[j>>2]=c[j>>2]|2;V=b|0;c[V>>2]=U;vf(o);vf(n);i=e;return}function rh(a,b,d,e,f,g,h){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0;b=i;i=i+16|0;j=d;d=i;i=i+4|0;i=i+7&-8;c[d>>2]=c[j>>2];j=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[j>>2];j=b|0;k=b+8|0;c[j>>2]=c[d>>2];c[k>>2]=c[e>>2];sh(a,0,j,k,f,g,h);i=b;return}function sh(b,e,f,h,j,k,l){b=b|0;e=e|0;f=f|0;h=h|0;j=j|0;k=k|0;l=l|0;var m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0;e=i;i=i+80|0;m=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[m>>2];m=h;h=i;i=i+4|0;i=i+7&-8;c[h>>2]=c[m>>2];m=e+32|0;n=e+40|0;o=e+48|0;p=e+64|0;q=p;r=i;i=i+4|0;i=i+7&-8;s=i;i=i+160|0;t=i;i=i+4|0;i=i+7&-8;u=i;i=i+4|0;i=i+7&-8;v=i;i=i+1|0;i=i+7&-8;w=i;i=i+1|0;i=i+7&-8;x=e|0;Zh(o,j,x,m,n);ko(q|0,0,12)|0;j=p;xf(p,10,0);if((a[q]&1)==0){y=j+1|0;z=y;A=y;B=p+8|0}else{y=p+8|0;z=c[y>>2]|0;A=j+1|0;B=y}c[r>>2]=z;y=s|0;c[t>>2]=y;c[u>>2]=0;a[v]=1;a[w]=69;j=f|0;f=h|0;h=p|0;C=p+4|0;D=a[m]|0;m=a[n]|0;n=z;z=c[j>>2]|0;L6:while(1){do{if((z|0)==0){E=0}else{if((c[z+12>>2]|0)!=(c[z+16>>2]|0)){E=z;break}if((dd[c[(c[z>>2]|0)+36>>2]&127](z)|0)!=-1){E=z;break}c[j>>2]=0;E=0}}while(0);F=(E|0)==0;G=c[f>>2]|0;do{if((G|0)==0){H=17}else{if((c[G+12>>2]|0)!=(c[G+16>>2]|0)){if(F){I=G;J=0;break}else{K=n;L=G;M=0;break L6}}if((dd[c[(c[G>>2]|0)+36>>2]&127](G)|0)==-1){c[f>>2]=0;H=17;break}else{N=(G|0)==0;if(F^N){I=G;J=N;break}else{K=n;L=G;M=N;break L6}}}}while(0);if((H|0)==17){H=0;if(F){K=n;L=0;M=1;break}else{I=0;J=1}}G=d[q]|0;N=(G&1|0)==0;if(((c[r>>2]|0)-n|0)==((N?G>>>1:c[C>>2]|0)|0)){if(N){O=G>>>1;P=G>>>1}else{G=c[C>>2]|0;O=G;P=G}xf(p,O<<1,0);if((a[q]&1)==0){Q=10}else{Q=(c[h>>2]&-2)-1|0}xf(p,Q,0);if((a[q]&1)==0){R=A}else{R=c[B>>2]|0}c[r>>2]=R+P;S=R}else{S=n}G=E+12|0;N=c[G>>2]|0;T=E+16|0;if((N|0)==(c[T>>2]|0)){U=(dd[c[(c[E>>2]|0)+36>>2]&127](E)|0)&255}else{U=a[N]|0}if((_h(U,v,w,S,r,D,m,o,y,t,u,x)|0)!=0){K=S;L=I;M=J;break}N=c[G>>2]|0;if((N|0)==(c[T>>2]|0)){dd[c[(c[E>>2]|0)+40>>2]&127](E)|0;n=S;z=E;continue}else{c[G>>2]=N+1;n=S;z=E;continue}}z=d[o]|0;if((z&1|0)==0){V=z>>>1}else{V=c[o+4>>2]|0}do{if((V|0)!=0){if((a[v]&1)==0){break}z=c[t>>2]|0;if((z-s|0)>=160){break}S=c[u>>2]|0;c[t>>2]=z+4;c[z>>2]=S}}while(0);g[l>>2]=+Om(K,c[r>>2]|0,k);kk(o,y,c[t>>2]|0,k);do{if(F){W=0}else{if((c[E+12>>2]|0)!=(c[E+16>>2]|0)){W=E;break}if((dd[c[(c[E>>2]|0)+36>>2]&127](E)|0)!=-1){W=E;break}c[j>>2]=0;W=0}}while(0);j=(W|0)==0;L67:do{if(M){H=59}else{do{if((c[L+12>>2]|0)==(c[L+16>>2]|0)){if((dd[c[(c[L>>2]|0)+36>>2]&127](L)|0)!=-1){break}c[f>>2]=0;H=59;break L67}}while(0);if(!(j^(L|0)==0)){break}X=b|0;c[X>>2]=W;vf(p);vf(o);i=e;return}}while(0);do{if((H|0)==59){if(j){break}X=b|0;c[X>>2]=W;vf(p);vf(o);i=e;return}}while(0);c[k>>2]=c[k>>2]|2;X=b|0;c[X>>2]=W;vf(p);vf(o);i=e;return}function th(a,b,d,e,f,g,h){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0;b=i;i=i+16|0;j=d;d=i;i=i+4|0;i=i+7&-8;c[d>>2]=c[j>>2];j=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[j>>2];j=b|0;k=b+8|0;c[j>>2]=c[d>>2];c[k>>2]=c[e>>2];uh(a,0,j,k,f,g,h);i=b;return}function uh(b,e,f,g,j,k,l){b=b|0;e=e|0;f=f|0;g=g|0;j=j|0;k=k|0;l=l|0;var m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0;e=i;i=i+80|0;m=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[m>>2];m=g;g=i;i=i+4|0;i=i+7&-8;c[g>>2]=c[m>>2];m=e+32|0;n=e+40|0;o=e+48|0;p=e+64|0;q=p;r=i;i=i+4|0;i=i+7&-8;s=i;i=i+160|0;t=i;i=i+4|0;i=i+7&-8;u=i;i=i+4|0;i=i+7&-8;v=i;i=i+1|0;i=i+7&-8;w=i;i=i+1|0;i=i+7&-8;x=e|0;Zh(o,j,x,m,n);ko(q|0,0,12)|0;j=p;xf(p,10,0);if((a[q]&1)==0){y=j+1|0;z=y;A=y;B=p+8|0}else{y=p+8|0;z=c[y>>2]|0;A=j+1|0;B=y}c[r>>2]=z;y=s|0;c[t>>2]=y;c[u>>2]=0;a[v]=1;a[w]=69;j=f|0;f=g|0;g=p|0;C=p+4|0;D=a[m]|0;m=a[n]|0;n=z;z=c[j>>2]|0;L6:while(1){do{if((z|0)==0){E=0}else{if((c[z+12>>2]|0)!=(c[z+16>>2]|0)){E=z;break}if((dd[c[(c[z>>2]|0)+36>>2]&127](z)|0)!=-1){E=z;break}c[j>>2]=0;E=0}}while(0);F=(E|0)==0;G=c[f>>2]|0;do{if((G|0)==0){H=17}else{if((c[G+12>>2]|0)!=(c[G+16>>2]|0)){if(F){I=G;J=0;break}else{K=n;L=G;M=0;break L6}}if((dd[c[(c[G>>2]|0)+36>>2]&127](G)|0)==-1){c[f>>2]=0;H=17;break}else{N=(G|0)==0;if(F^N){I=G;J=N;break}else{K=n;L=G;M=N;break L6}}}}while(0);if((H|0)==17){H=0;if(F){K=n;L=0;M=1;break}else{I=0;J=1}}G=d[q]|0;N=(G&1|0)==0;if(((c[r>>2]|0)-n|0)==((N?G>>>1:c[C>>2]|0)|0)){if(N){O=G>>>1;P=G>>>1}else{G=c[C>>2]|0;O=G;P=G}xf(p,O<<1,0);if((a[q]&1)==0){Q=10}else{Q=(c[g>>2]&-2)-1|0}xf(p,Q,0);if((a[q]&1)==0){R=A}else{R=c[B>>2]|0}c[r>>2]=R+P;S=R}else{S=n}G=E+12|0;N=c[G>>2]|0;T=E+16|0;if((N|0)==(c[T>>2]|0)){U=(dd[c[(c[E>>2]|0)+36>>2]&127](E)|0)&255}else{U=a[N]|0}if((_h(U,v,w,S,r,D,m,o,y,t,u,x)|0)!=0){K=S;L=I;M=J;break}N=c[G>>2]|0;if((N|0)==(c[T>>2]|0)){dd[c[(c[E>>2]|0)+40>>2]&127](E)|0;n=S;z=E;continue}else{c[G>>2]=N+1;n=S;z=E;continue}}z=d[o]|0;if((z&1|0)==0){V=z>>>1}else{V=c[o+4>>2]|0}do{if((V|0)!=0){if((a[v]&1)==0){break}z=c[t>>2]|0;if((z-s|0)>=160){break}S=c[u>>2]|0;c[t>>2]=z+4;c[z>>2]=S}}while(0);h[l>>3]=+Nm(K,c[r>>2]|0,k);kk(o,y,c[t>>2]|0,k);do{if(F){W=0}else{if((c[E+12>>2]|0)!=(c[E+16>>2]|0)){W=E;break}if((dd[c[(c[E>>2]|0)+36>>2]&127](E)|0)!=-1){W=E;break}c[j>>2]=0;W=0}}while(0);j=(W|0)==0;L67:do{if(M){H=59}else{do{if((c[L+12>>2]|0)==(c[L+16>>2]|0)){if((dd[c[(c[L>>2]|0)+36>>2]&127](L)|0)!=-1){break}c[f>>2]=0;H=59;break L67}}while(0);if(!(j^(L|0)==0)){break}X=b|0;c[X>>2]=W;vf(p);vf(o);i=e;return}}while(0);do{if((H|0)==59){if(j){break}X=b|0;c[X>>2]=W;vf(p);vf(o);i=e;return}}while(0);c[k>>2]=c[k>>2]|2;X=b|0;c[X>>2]=W;vf(p);vf(o);i=e;return}function vh(a,b,d,e,f,g,h){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0;b=i;i=i+16|0;j=d;d=i;i=i+4|0;i=i+7&-8;c[d>>2]=c[j>>2];j=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[j>>2];j=b|0;k=b+8|0;c[j>>2]=c[d>>2];c[k>>2]=c[e>>2];wh(a,0,j,k,f,g,h);i=b;return}function wh(b,e,f,g,j,k,l){b=b|0;e=e|0;f=f|0;g=g|0;j=j|0;k=k|0;l=l|0;var m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0;e=i;i=i+80|0;m=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[m>>2];m=g;g=i;i=i+4|0;i=i+7&-8;c[g>>2]=c[m>>2];m=e+32|0;n=e+40|0;o=e+48|0;p=e+64|0;q=p;r=i;i=i+4|0;i=i+7&-8;s=i;i=i+160|0;t=i;i=i+4|0;i=i+7&-8;u=i;i=i+4|0;i=i+7&-8;v=i;i=i+1|0;i=i+7&-8;w=i;i=i+1|0;i=i+7&-8;x=e|0;Zh(o,j,x,m,n);ko(q|0,0,12)|0;j=p;xf(p,10,0);if((a[q]&1)==0){y=j+1|0;z=y;A=y;B=p+8|0}else{y=p+8|0;z=c[y>>2]|0;A=j+1|0;B=y}c[r>>2]=z;y=s|0;c[t>>2]=y;c[u>>2]=0;a[v]=1;a[w]=69;j=f|0;f=g|0;g=p|0;C=p+4|0;D=a[m]|0;m=a[n]|0;n=z;z=c[j>>2]|0;L6:while(1){do{if((z|0)==0){E=0}else{if((c[z+12>>2]|0)!=(c[z+16>>2]|0)){E=z;break}if((dd[c[(c[z>>2]|0)+36>>2]&127](z)|0)!=-1){E=z;break}c[j>>2]=0;E=0}}while(0);F=(E|0)==0;G=c[f>>2]|0;do{if((G|0)==0){H=17}else{if((c[G+12>>2]|0)!=(c[G+16>>2]|0)){if(F){I=G;J=0;break}else{K=n;L=G;M=0;break L6}}if((dd[c[(c[G>>2]|0)+36>>2]&127](G)|0)==-1){c[f>>2]=0;H=17;break}else{N=(G|0)==0;if(F^N){I=G;J=N;break}else{K=n;L=G;M=N;break L6}}}}while(0);if((H|0)==17){H=0;if(F){K=n;L=0;M=1;break}else{I=0;J=1}}G=d[q]|0;N=(G&1|0)==0;if(((c[r>>2]|0)-n|0)==((N?G>>>1:c[C>>2]|0)|0)){if(N){O=G>>>1;P=G>>>1}else{G=c[C>>2]|0;O=G;P=G}xf(p,O<<1,0);if((a[q]&1)==0){Q=10}else{Q=(c[g>>2]&-2)-1|0}xf(p,Q,0);if((a[q]&1)==0){R=A}else{R=c[B>>2]|0}c[r>>2]=R+P;S=R}else{S=n}G=E+12|0;N=c[G>>2]|0;T=E+16|0;if((N|0)==(c[T>>2]|0)){U=(dd[c[(c[E>>2]|0)+36>>2]&127](E)|0)&255}else{U=a[N]|0}if((_h(U,v,w,S,r,D,m,o,y,t,u,x)|0)!=0){K=S;L=I;M=J;break}N=c[G>>2]|0;if((N|0)==(c[T>>2]|0)){dd[c[(c[E>>2]|0)+40>>2]&127](E)|0;n=S;z=E;continue}else{c[G>>2]=N+1;n=S;z=E;continue}}z=d[o]|0;if((z&1|0)==0){V=z>>>1}else{V=c[o+4>>2]|0}do{if((V|0)!=0){if((a[v]&1)==0){break}z=c[t>>2]|0;if((z-s|0)>=160){break}S=c[u>>2]|0;c[t>>2]=z+4;c[z>>2]=S}}while(0);h[l>>3]=+Mm(K,c[r>>2]|0,k);kk(o,y,c[t>>2]|0,k);do{if(F){W=0}else{if((c[E+12>>2]|0)!=(c[E+16>>2]|0)){W=E;break}if((dd[c[(c[E>>2]|0)+36>>2]&127](E)|0)!=-1){W=E;break}c[j>>2]=0;W=0}}while(0);j=(W|0)==0;L67:do{if(M){H=59}else{do{if((c[L+12>>2]|0)==(c[L+16>>2]|0)){if((dd[c[(c[L>>2]|0)+36>>2]&127](L)|0)!=-1){break}c[f>>2]=0;H=59;break L67}}while(0);if(!(j^(L|0)==0)){break}X=b|0;c[X>>2]=W;vf(p);vf(o);i=e;return}}while(0);do{if((H|0)==59){if(j){break}X=b|0;c[X>>2]=W;vf(p);vf(o);i=e;return}}while(0);c[k>>2]=c[k>>2]|2;X=b|0;c[X>>2]=W;vf(p);vf(o);i=e;return}function xh(b,e,f,g,h,j,k){b=b|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;var l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0;e=i;i=i+64|0;l=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[l>>2];l=g;g=i;i=i+4|0;i=i+7&-8;c[g>>2]=c[l>>2];l=e|0;m=e+16|0;n=e+48|0;o=i;i=i+4|0;i=i+7&-8;p=i;i=i+12|0;i=i+7&-8;q=i;i=i+4|0;i=i+7&-8;r=i;i=i+160|0;s=i;i=i+4|0;i=i+7&-8;t=i;i=i+4|0;i=i+7&-8;ko(n|0,0,12)|0;u=p;Pf(o,h);h=o|0;o=c[h>>2]|0;if((c[8976]|0)!=-1){c[l>>2]=35904;c[l+4>>2]=14;c[l+8>>2]=0;qf(35904,l,96)}l=(c[8977]|0)-1|0;v=c[o+8>>2]|0;do{if((c[o+12>>2]|0)-v>>2>>>0>l>>>0){w=c[v+(l<<2)>>2]|0;if((w|0)==0){break}x=m|0;ld[c[(c[w>>2]|0)+32>>2]&15](w,11736,11762,x)|0;Xe(c[h>>2]|0)|0;ko(u|0,0,12)|0;w=p;xf(p,10,0);if((a[u]&1)==0){y=w+1|0;z=y;A=y;B=p+8|0}else{y=p+8|0;z=c[y>>2]|0;A=w+1|0;B=y}c[q>>2]=z;y=r|0;c[s>>2]=y;c[t>>2]=0;w=f|0;C=g|0;D=p|0;E=p+4|0;F=z;G=c[w>>2]|0;L14:while(1){do{if((G|0)==0){H=0}else{if((c[G+12>>2]|0)!=(c[G+16>>2]|0)){H=G;break}if((dd[c[(c[G>>2]|0)+36>>2]&127](G)|0)!=-1){H=G;break}c[w>>2]=0;H=0}}while(0);I=(H|0)==0;J=c[C>>2]|0;do{if((J|0)==0){K=25}else{if((c[J+12>>2]|0)!=(c[J+16>>2]|0)){if(I){break}else{L=F;break L14}}if((dd[c[(c[J>>2]|0)+36>>2]&127](J)|0)==-1){c[C>>2]=0;K=25;break}else{if(I^(J|0)==0){break}else{L=F;break L14}}}}while(0);if((K|0)==25){K=0;if(I){L=F;break}}J=d[u]|0;M=(J&1|0)==0;if(((c[q>>2]|0)-F|0)==((M?J>>>1:c[E>>2]|0)|0)){if(M){N=J>>>1;O=J>>>1}else{J=c[E>>2]|0;N=J;O=J}xf(p,N<<1,0);if((a[u]&1)==0){P=10}else{P=(c[D>>2]&-2)-1|0}xf(p,P,0);if((a[u]&1)==0){Q=A}else{Q=c[B>>2]|0}c[q>>2]=Q+O;R=Q}else{R=F}J=H+12|0;M=c[J>>2]|0;S=H+16|0;if((M|0)==(c[S>>2]|0)){T=(dd[c[(c[H>>2]|0)+36>>2]&127](H)|0)&255}else{T=a[M]|0}if((yh(T,16,R,q,t,0,n,y,s,x)|0)!=0){L=R;break}M=c[J>>2]|0;if((M|0)==(c[S>>2]|0)){dd[c[(c[H>>2]|0)+40>>2]&127](H)|0;F=R;G=H;continue}else{c[J>>2]=M+1;F=R;G=H;continue}}a[L+3|0]=0;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);G=zh(L,c[8322]|0,3056,(F=i,i=i+8|0,c[F>>2]=k,F)|0)|0;i=F;if((G|0)!=1){c[j>>2]=4}G=c[w>>2]|0;do{if((G|0)==0){U=0}else{if((c[G+12>>2]|0)!=(c[G+16>>2]|0)){U=G;break}if((dd[c[(c[G>>2]|0)+36>>2]&127](G)|0)!=-1){U=G;break}c[w>>2]=0;U=0}}while(0);w=(U|0)==0;G=c[C>>2]|0;do{if((G|0)==0){K=70}else{if((c[G+12>>2]|0)!=(c[G+16>>2]|0)){if(!w){break}V=b|0;c[V>>2]=U;vf(p);vf(n);i=e;return}if((dd[c[(c[G>>2]|0)+36>>2]&127](G)|0)==-1){c[C>>2]=0;K=70;break}if(!(w^(G|0)==0)){break}V=b|0;c[V>>2]=U;vf(p);vf(n);i=e;return}}while(0);do{if((K|0)==70){if(w){break}V=b|0;c[V>>2]=U;vf(p);vf(n);i=e;return}}while(0);c[j>>2]=c[j>>2]|2;V=b|0;c[V>>2]=U;vf(p);vf(n);i=e;return}}while(0);e=Nc(4)|0;qn(e);Qb(e|0,10192,134)}function yh(b,e,f,g,h,i,j,k,l,m){b=b|0;e=e|0;f=f|0;g=g|0;h=h|0;i=i|0;j=j|0;k=k|0;l=l|0;m=m|0;var n=0,o=0,p=0,q=0,r=0,s=0;n=c[g>>2]|0;o=(n|0)==(f|0);do{if(o){p=(a[m+24|0]|0)==b<<24>>24;if(!p){if((a[m+25|0]|0)!=b<<24>>24){break}}c[g>>2]=f+1;a[f]=p?43:45;c[h>>2]=0;q=0;return q|0}}while(0);p=d[j]|0;if((p&1|0)==0){r=p>>>1}else{r=c[j+4>>2]|0}if((r|0)!=0&b<<24>>24==i<<24>>24){i=c[l>>2]|0;if((i-k|0)>=160){q=0;return q|0}k=c[h>>2]|0;c[l>>2]=i+4;c[i>>2]=k;c[h>>2]=0;q=0;return q|0}k=m+26|0;i=m;while(1){l=i+1|0;if((a[i]|0)==b<<24>>24){s=i;break}if((l|0)==(k|0)){s=k;break}else{i=l}}i=s-m|0;if((i|0)>23){q=-1;return q|0}do{if((e|0)==16){if((i|0)<22){break}if(o){q=-1;return q|0}if((n-f|0)>=3){q=-1;return q|0}if((a[n-1|0]|0)!=48){q=-1;return q|0}c[h>>2]=0;m=a[11736+i|0]|0;s=c[g>>2]|0;c[g>>2]=s+1;a[s]=m;q=0;return q|0}else if((e|0)==8|(e|0)==10){if((i|0)<(e|0)){break}else{q=-1}return q|0}}while(0);e=a[11736+i|0]|0;c[g>>2]=n+1;a[n]=e;c[h>>2]=(c[h>>2]|0)+1;q=0;return q|0}function zh(a,b,d,e){a=a|0;b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0;f=i;i=i+16|0;g=f|0;h=g;c[h>>2]=e;c[h+4>>2]=0;h=uc(b|0)|0;b=bb(a|0,d|0,g|0)|0;if((h|0)==0){i=f;return b|0}uc(h|0)|0;i=f;return b|0}function Ah(a){a=a|0;Ve(a|0);Tn(a);return}function Bh(a){a=a|0;Ve(a|0);return}function Ch(b,d,e,f,g,h,j){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;var k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0;k=i;i=i+112|0;l=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[l>>2];l=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[l>>2];l=k|0;m=k+16|0;n=k+32|0;o=k+40|0;p=k+48|0;q=k+56|0;r=k+64|0;s=k+72|0;t=k+80|0;u=k+104|0;if((c[g+4>>2]&1|0)==0){c[n>>2]=-1;v=c[(c[d>>2]|0)+16>>2]|0;w=e|0;c[p>>2]=c[w>>2];c[q>>2]=c[f>>2];Zc[v&127](o,d,p,q,g,h,n);q=c[o>>2]|0;c[w>>2]=q;w=c[n>>2]|0;if((w|0)==0){a[j]=0}else if((w|0)==1){a[j]=1}else{a[j]=1;c[h>>2]=4}c[b>>2]=q;i=k;return}Pf(r,g);q=r|0;r=c[q>>2]|0;if((c[8974]|0)!=-1){c[m>>2]=35896;c[m+4>>2]=14;c[m+8>>2]=0;qf(35896,m,96)}m=(c[8975]|0)-1|0;w=c[r+8>>2]|0;do{if((c[r+12>>2]|0)-w>>2>>>0>m>>>0){n=c[w+(m<<2)>>2]|0;if((n|0)==0){break}o=n;Xe(c[q>>2]|0)|0;Pf(s,g);n=s|0;p=c[n>>2]|0;if((c[8878]|0)!=-1){c[l>>2]=35512;c[l+4>>2]=14;c[l+8>>2]=0;qf(35512,l,96)}d=(c[8879]|0)-1|0;v=c[p+8>>2]|0;do{if((c[p+12>>2]|0)-v>>2>>>0>d>>>0){x=c[v+(d<<2)>>2]|0;if((x|0)==0){break}y=x;Xe(c[n>>2]|0)|0;z=t|0;A=x;$c[c[(c[A>>2]|0)+24>>2]&127](z,y);$c[c[(c[A>>2]|0)+28>>2]&127](t+12|0,y);c[u>>2]=c[f>>2];a[j]=(Dh(e,u,z,t+24|0,o,h,1)|0)==(z|0)|0;c[b>>2]=c[e>>2];Gf(t+12|0);Gf(t|0);i=k;return}}while(0);o=Nc(4)|0;qn(o);Qb(o|0,10192,134)}}while(0);k=Nc(4)|0;qn(k);Qb(k|0,10192,134)}function Dh(b,e,f,g,h,j,k){b=b|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;var l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0,Z=0,_=0,$=0,aa=0,ba=0,ca=0,da=0;l=i;i=i+104|0;m=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[m>>2];m=(g-f|0)/12|0;n=l|0;do{if(m>>>0>100>>>0){o=Ln(m)|0;if((o|0)!=0){p=o;q=o;break}Yn();p=0;q=0}else{p=n;q=0}}while(0);n=(f|0)==(g|0);if(n){r=m;s=0}else{o=m;m=0;t=p;u=f;while(1){v=d[u]|0;if((v&1|0)==0){w=v>>>1}else{w=c[u+4>>2]|0}if((w|0)==0){a[t]=2;x=m+1|0;y=o-1|0}else{a[t]=1;x=m;y=o}v=u+12|0;if((v|0)==(g|0)){r=y;s=x;break}else{o=y;m=x;t=t+1|0;u=v}}}u=b|0;b=e|0;e=h;t=0;x=s;s=r;while(1){r=c[u>>2]|0;do{if((r|0)==0){z=0}else{m=c[r+12>>2]|0;if((m|0)==(c[r+16>>2]|0)){A=dd[c[(c[r>>2]|0)+36>>2]&127](r)|0}else{A=c[m>>2]|0}if((A|0)==-1){c[u>>2]=0;z=0;break}else{z=c[u>>2]|0;break}}}while(0);r=(z|0)==0;m=c[b>>2]|0;if((m|0)==0){B=z;C=0}else{y=c[m+12>>2]|0;if((y|0)==(c[m+16>>2]|0)){D=dd[c[(c[m>>2]|0)+36>>2]&127](m)|0}else{D=c[y>>2]|0}if((D|0)==-1){c[b>>2]=0;E=0}else{E=m}B=c[u>>2]|0;C=E}F=(C|0)==0;if(!((r^F)&(s|0)!=0)){break}r=c[B+12>>2]|0;if((r|0)==(c[B+16>>2]|0)){G=dd[c[(c[B>>2]|0)+36>>2]&127](B)|0}else{G=c[r>>2]|0}if(k){H=G}else{H=ad[c[(c[e>>2]|0)+28>>2]&31](h,G)|0}do{if(n){I=x;J=s}else{r=t+1|0;L49:do{if(k){m=s;y=x;o=p;w=0;v=f;while(1){do{if((a[o]|0)==1){K=v;if((a[K]&1)==0){L=v+4|0}else{L=c[v+8>>2]|0}if((H|0)!=(c[L+(t<<2)>>2]|0)){a[o]=0;M=w;N=y;O=m-1|0;break}P=d[K]|0;if((P&1|0)==0){Q=P>>>1}else{Q=c[v+4>>2]|0}if((Q|0)!=(r|0)){M=1;N=y;O=m;break}a[o]=2;M=1;N=y+1|0;O=m-1|0}else{M=w;N=y;O=m}}while(0);P=v+12|0;if((P|0)==(g|0)){R=O;S=N;T=M;break L49}m=O;y=N;o=o+1|0;w=M;v=P}}else{v=s;w=x;o=p;y=0;m=f;while(1){do{if((a[o]|0)==1){P=m;if((a[P]&1)==0){U=m+4|0}else{U=c[m+8>>2]|0}if((H|0)!=(ad[c[(c[e>>2]|0)+28>>2]&31](h,c[U+(t<<2)>>2]|0)|0)){a[o]=0;V=y;W=w;X=v-1|0;break}K=d[P]|0;if((K&1|0)==0){Y=K>>>1}else{Y=c[m+4>>2]|0}if((Y|0)!=(r|0)){V=1;W=w;X=v;break}a[o]=2;V=1;W=w+1|0;X=v-1|0}else{V=y;W=w;X=v}}while(0);K=m+12|0;if((K|0)==(g|0)){R=X;S=W;T=V;break L49}v=X;w=W;o=o+1|0;y=V;m=K}}}while(0);if(!T){I=S;J=R;break}r=c[u>>2]|0;m=r+12|0;y=c[m>>2]|0;if((y|0)==(c[r+16>>2]|0)){dd[c[(c[r>>2]|0)+40>>2]&127](r)|0}else{c[m>>2]=y+4}if((S+R|0)>>>0<2>>>0|n){I=S;J=R;break}y=t+1|0;m=S;r=p;o=f;while(1){do{if((a[r]|0)==2){w=d[o]|0;if((w&1|0)==0){Z=w>>>1}else{Z=c[o+4>>2]|0}if((Z|0)==(y|0)){_=m;break}a[r]=0;_=m-1|0}else{_=m}}while(0);w=o+12|0;if((w|0)==(g|0)){I=_;J=R;break}else{m=_;r=r+1|0;o=w}}}}while(0);t=t+1|0;x=I;s=J}do{if((B|0)==0){$=1}else{J=c[B+12>>2]|0;if((J|0)==(c[B+16>>2]|0)){aa=dd[c[(c[B>>2]|0)+36>>2]&127](B)|0}else{aa=c[J>>2]|0}if((aa|0)==-1){c[u>>2]=0;$=1;break}else{$=(c[u>>2]|0)==0;break}}}while(0);do{if(F){ba=95}else{u=c[C+12>>2]|0;if((u|0)==(c[C+16>>2]|0)){ca=dd[c[(c[C>>2]|0)+36>>2]&127](C)|0}else{ca=c[u>>2]|0}if((ca|0)==-1){c[b>>2]=0;ba=95;break}else{if($^(C|0)==0){break}else{ba=97;break}}}}while(0);if((ba|0)==95){if($){ba=97}}if((ba|0)==97){c[j>>2]=c[j>>2]|2}L130:do{if(n){ba=102}else{$=f;C=p;while(1){if((a[C]|0)==2){da=$;break L130}b=$+12|0;if((b|0)==(g|0)){ba=102;break L130}$=b;C=C+1|0}}}while(0);if((ba|0)==102){c[j>>2]=c[j>>2]|4;da=g}if((q|0)==0){i=l;return da|0}Mn(q);i=l;return da|0}function Eh(a,b,d,e,f,g,h){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0;b=i;i=i+16|0;j=d;d=i;i=i+4|0;i=i+7&-8;c[d>>2]=c[j>>2];j=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[j>>2];j=b|0;k=b+8|0;c[j>>2]=c[d>>2];c[k>>2]=c[e>>2];Fh(a,0,j,k,f,g,h);i=b;return}function Fh(b,e,f,g,h,j,k){b=b|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;var l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0;e=i;i=i+144|0;l=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[l>>2];l=g;g=i;i=i+4|0;i=i+7&-8;c[g>>2]=c[l>>2];l=e|0;m=e+104|0;n=e+112|0;o=e+128|0;p=o;q=i;i=i+4|0;i=i+7&-8;r=i;i=i+160|0;s=i;i=i+4|0;i=i+7&-8;t=i;i=i+4|0;i=i+7&-8;u=c[h+4>>2]&74;if((u|0)==0){v=0}else if((u|0)==64){v=8}else if((u|0)==8){v=16}else{v=10}u=l|0;$h(n,h,u,m);ko(p|0,0,12)|0;h=o;xf(o,10,0);if((a[p]&1)==0){l=h+1|0;w=l;x=l;y=o+8|0}else{l=o+8|0;w=c[l>>2]|0;x=h+1|0;y=l}c[q>>2]=w;l=r|0;c[s>>2]=l;c[t>>2]=0;h=f|0;f=g|0;g=o|0;z=o+4|0;A=c[m>>2]|0;m=w;w=c[h>>2]|0;L11:while(1){do{if((w|0)==0){B=0}else{C=c[w+12>>2]|0;if((C|0)==(c[w+16>>2]|0)){D=dd[c[(c[w>>2]|0)+36>>2]&127](w)|0}else{D=c[C>>2]|0}if((D|0)!=-1){B=w;break}c[h>>2]=0;B=0}}while(0);E=(B|0)==0;C=c[f>>2]|0;do{if((C|0)==0){F=22}else{G=c[C+12>>2]|0;if((G|0)==(c[C+16>>2]|0)){H=dd[c[(c[C>>2]|0)+36>>2]&127](C)|0}else{H=c[G>>2]|0}if((H|0)==-1){c[f>>2]=0;F=22;break}else{G=(C|0)==0;if(E^G){I=C;J=G;break}else{K=m;L=C;M=G;break L11}}}}while(0);if((F|0)==22){F=0;if(E){K=m;L=0;M=1;break}else{I=0;J=1}}C=d[p]|0;G=(C&1|0)==0;if(((c[q>>2]|0)-m|0)==((G?C>>>1:c[z>>2]|0)|0)){if(G){N=C>>>1;O=C>>>1}else{C=c[z>>2]|0;N=C;O=C}xf(o,N<<1,0);if((a[p]&1)==0){P=10}else{P=(c[g>>2]&-2)-1|0}xf(o,P,0);if((a[p]&1)==0){Q=x}else{Q=c[y>>2]|0}c[q>>2]=Q+O;R=Q}else{R=m}C=B+12|0;G=c[C>>2]|0;S=B+16|0;if((G|0)==(c[S>>2]|0)){T=dd[c[(c[B>>2]|0)+36>>2]&127](B)|0}else{T=c[G>>2]|0}if((Xh(T,v,R,q,t,A,n,l,s,u)|0)!=0){K=R;L=I;M=J;break}G=c[C>>2]|0;if((G|0)==(c[S>>2]|0)){dd[c[(c[B>>2]|0)+40>>2]&127](B)|0;m=R;w=B;continue}else{c[C>>2]=G+4;m=R;w=B;continue}}w=d[n]|0;if((w&1|0)==0){U=w>>>1}else{U=c[n+4>>2]|0}do{if((U|0)!=0){w=c[s>>2]|0;if((w-r|0)>=160){break}R=c[t>>2]|0;c[s>>2]=w+4;c[w>>2]=R}}while(0);c[k>>2]=Um(K,c[q>>2]|0,j,v)|0;kk(n,l,c[s>>2]|0,j);do{if(E){V=0}else{s=c[B+12>>2]|0;if((s|0)==(c[B+16>>2]|0)){W=dd[c[(c[B>>2]|0)+36>>2]&127](B)|0}else{W=c[s>>2]|0}if((W|0)!=-1){V=B;break}c[h>>2]=0;V=0}}while(0);h=(V|0)==0;do{if(M){F=64}else{B=c[L+12>>2]|0;if((B|0)==(c[L+16>>2]|0)){X=dd[c[(c[L>>2]|0)+36>>2]&127](L)|0}else{X=c[B>>2]|0}if((X|0)==-1){c[f>>2]=0;F=64;break}if(!(h^(L|0)==0)){break}Y=b|0;c[Y>>2]=V;vf(o);vf(n);i=e;return}}while(0);do{if((F|0)==64){if(h){break}Y=b|0;c[Y>>2]=V;vf(o);vf(n);i=e;return}}while(0);c[j>>2]=c[j>>2]|2;Y=b|0;c[Y>>2]=V;vf(o);vf(n);i=e;return}function Gh(a,b,d,e,f,g,h){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0;b=i;i=i+16|0;j=d;d=i;i=i+4|0;i=i+7&-8;c[d>>2]=c[j>>2];j=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[j>>2];j=b|0;k=b+8|0;c[j>>2]=c[d>>2];c[k>>2]=c[e>>2];Hh(a,0,j,k,f,g,h);i=b;return}function Hh(b,e,f,g,h,j,k){b=b|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;var l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0,Z=0;e=i;i=i+144|0;l=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[l>>2];l=g;g=i;i=i+4|0;i=i+7&-8;c[g>>2]=c[l>>2];l=e|0;m=e+104|0;n=e+112|0;o=e+128|0;p=o;q=i;i=i+4|0;i=i+7&-8;r=i;i=i+160|0;s=i;i=i+4|0;i=i+7&-8;t=i;i=i+4|0;i=i+7&-8;u=c[h+4>>2]&74;if((u|0)==8){v=16}else if((u|0)==64){v=8}else if((u|0)==0){v=0}else{v=10}u=l|0;$h(n,h,u,m);ko(p|0,0,12)|0;h=o;xf(o,10,0);if((a[p]&1)==0){l=h+1|0;w=l;x=l;y=o+8|0}else{l=o+8|0;w=c[l>>2]|0;x=h+1|0;y=l}c[q>>2]=w;l=r|0;c[s>>2]=l;c[t>>2]=0;h=f|0;f=g|0;g=o|0;z=o+4|0;A=c[m>>2]|0;m=w;w=c[h>>2]|0;L11:while(1){do{if((w|0)==0){B=0}else{C=c[w+12>>2]|0;if((C|0)==(c[w+16>>2]|0)){D=dd[c[(c[w>>2]|0)+36>>2]&127](w)|0}else{D=c[C>>2]|0}if((D|0)!=-1){B=w;break}c[h>>2]=0;B=0}}while(0);E=(B|0)==0;C=c[f>>2]|0;do{if((C|0)==0){F=22}else{G=c[C+12>>2]|0;if((G|0)==(c[C+16>>2]|0)){H=dd[c[(c[C>>2]|0)+36>>2]&127](C)|0}else{H=c[G>>2]|0}if((H|0)==-1){c[f>>2]=0;F=22;break}else{G=(C|0)==0;if(E^G){I=C;J=G;break}else{L=m;M=C;N=G;break L11}}}}while(0);if((F|0)==22){F=0;if(E){L=m;M=0;N=1;break}else{I=0;J=1}}C=d[p]|0;G=(C&1|0)==0;if(((c[q>>2]|0)-m|0)==((G?C>>>1:c[z>>2]|0)|0)){if(G){O=C>>>1;P=C>>>1}else{C=c[z>>2]|0;O=C;P=C}xf(o,O<<1,0);if((a[p]&1)==0){Q=10}else{Q=(c[g>>2]&-2)-1|0}xf(o,Q,0);if((a[p]&1)==0){R=x}else{R=c[y>>2]|0}c[q>>2]=R+P;S=R}else{S=m}C=B+12|0;G=c[C>>2]|0;T=B+16|0;if((G|0)==(c[T>>2]|0)){U=dd[c[(c[B>>2]|0)+36>>2]&127](B)|0}else{U=c[G>>2]|0}if((Xh(U,v,S,q,t,A,n,l,s,u)|0)!=0){L=S;M=I;N=J;break}G=c[C>>2]|0;if((G|0)==(c[T>>2]|0)){dd[c[(c[B>>2]|0)+40>>2]&127](B)|0;m=S;w=B;continue}else{c[C>>2]=G+4;m=S;w=B;continue}}w=d[n]|0;if((w&1|0)==0){V=w>>>1}else{V=c[n+4>>2]|0}do{if((V|0)!=0){w=c[s>>2]|0;if((w-r|0)>=160){break}S=c[t>>2]|0;c[s>>2]=w+4;c[w>>2]=S}}while(0);t=Tm(L,c[q>>2]|0,j,v)|0;c[k>>2]=t;c[k+4>>2]=K;kk(n,l,c[s>>2]|0,j);do{if(E){W=0}else{s=c[B+12>>2]|0;if((s|0)==(c[B+16>>2]|0)){X=dd[c[(c[B>>2]|0)+36>>2]&127](B)|0}else{X=c[s>>2]|0}if((X|0)!=-1){W=B;break}c[h>>2]=0;W=0}}while(0);h=(W|0)==0;do{if(N){F=64}else{B=c[M+12>>2]|0;if((B|0)==(c[M+16>>2]|0)){Y=dd[c[(c[M>>2]|0)+36>>2]&127](M)|0}else{Y=c[B>>2]|0}if((Y|0)==-1){c[f>>2]=0;F=64;break}if(!(h^(M|0)==0)){break}Z=b|0;c[Z>>2]=W;vf(o);vf(n);i=e;return}}while(0);do{if((F|0)==64){if(h){break}Z=b|0;c[Z>>2]=W;vf(o);vf(n);i=e;return}}while(0);c[j>>2]=c[j>>2]|2;Z=b|0;c[Z>>2]=W;vf(o);vf(n);i=e;return}function Ih(a,b,d,e,f,g,h){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0;b=i;i=i+16|0;j=d;d=i;i=i+4|0;i=i+7&-8;c[d>>2]=c[j>>2];j=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[j>>2];j=b|0;k=b+8|0;c[j>>2]=c[d>>2];c[k>>2]=c[e>>2];Jh(a,0,j,k,f,g,h);i=b;return}function Jh(e,f,g,h,j,k,l){e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;l=l|0;var m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0,Z=0;f=i;i=i+144|0;m=g;g=i;i=i+4|0;i=i+7&-8;c[g>>2]=c[m>>2];m=h;h=i;i=i+4|0;i=i+7&-8;c[h>>2]=c[m>>2];m=f|0;n=f+104|0;o=f+112|0;p=f+128|0;q=p;r=i;i=i+4|0;i=i+7&-8;s=i;i=i+160|0;t=i;i=i+4|0;i=i+7&-8;u=i;i=i+4|0;i=i+7&-8;v=c[j+4>>2]&74;if((v|0)==0){w=0}else if((v|0)==8){w=16}else if((v|0)==64){w=8}else{w=10}v=m|0;$h(o,j,v,n);ko(q|0,0,12)|0;j=p;xf(p,10,0);if((a[q]&1)==0){m=j+1|0;x=m;y=m;z=p+8|0}else{m=p+8|0;x=c[m>>2]|0;y=j+1|0;z=m}c[r>>2]=x;m=s|0;c[t>>2]=m;c[u>>2]=0;j=g|0;g=h|0;h=p|0;A=p+4|0;B=c[n>>2]|0;n=x;x=c[j>>2]|0;L11:while(1){do{if((x|0)==0){C=0}else{D=c[x+12>>2]|0;if((D|0)==(c[x+16>>2]|0)){E=dd[c[(c[x>>2]|0)+36>>2]&127](x)|0}else{E=c[D>>2]|0}if((E|0)!=-1){C=x;break}c[j>>2]=0;C=0}}while(0);F=(C|0)==0;D=c[g>>2]|0;do{if((D|0)==0){G=22}else{H=c[D+12>>2]|0;if((H|0)==(c[D+16>>2]|0)){I=dd[c[(c[D>>2]|0)+36>>2]&127](D)|0}else{I=c[H>>2]|0}if((I|0)==-1){c[g>>2]=0;G=22;break}else{H=(D|0)==0;if(F^H){J=D;K=H;break}else{L=n;M=D;N=H;break L11}}}}while(0);if((G|0)==22){G=0;if(F){L=n;M=0;N=1;break}else{J=0;K=1}}D=d[q]|0;H=(D&1|0)==0;if(((c[r>>2]|0)-n|0)==((H?D>>>1:c[A>>2]|0)|0)){if(H){O=D>>>1;P=D>>>1}else{D=c[A>>2]|0;O=D;P=D}xf(p,O<<1,0);if((a[q]&1)==0){Q=10}else{Q=(c[h>>2]&-2)-1|0}xf(p,Q,0);if((a[q]&1)==0){R=y}else{R=c[z>>2]|0}c[r>>2]=R+P;S=R}else{S=n}D=C+12|0;H=c[D>>2]|0;T=C+16|0;if((H|0)==(c[T>>2]|0)){U=dd[c[(c[C>>2]|0)+36>>2]&127](C)|0}else{U=c[H>>2]|0}if((Xh(U,w,S,r,u,B,o,m,t,v)|0)!=0){L=S;M=J;N=K;break}H=c[D>>2]|0;if((H|0)==(c[T>>2]|0)){dd[c[(c[C>>2]|0)+40>>2]&127](C)|0;n=S;x=C;continue}else{c[D>>2]=H+4;n=S;x=C;continue}}x=d[o]|0;if((x&1|0)==0){V=x>>>1}else{V=c[o+4>>2]|0}do{if((V|0)!=0){x=c[t>>2]|0;if((x-s|0)>=160){break}S=c[u>>2]|0;c[t>>2]=x+4;c[x>>2]=S}}while(0);b[l>>1]=Sm(L,c[r>>2]|0,k,w)|0;kk(o,m,c[t>>2]|0,k);do{if(F){W=0}else{t=c[C+12>>2]|0;if((t|0)==(c[C+16>>2]|0)){X=dd[c[(c[C>>2]|0)+36>>2]&127](C)|0}else{X=c[t>>2]|0}if((X|0)!=-1){W=C;break}c[j>>2]=0;W=0}}while(0);j=(W|0)==0;do{if(N){G=64}else{C=c[M+12>>2]|0;if((C|0)==(c[M+16>>2]|0)){Y=dd[c[(c[M>>2]|0)+36>>2]&127](M)|0}else{Y=c[C>>2]|0}if((Y|0)==-1){c[g>>2]=0;G=64;break}if(!(j^(M|0)==0)){break}Z=e|0;c[Z>>2]=W;vf(p);vf(o);i=f;return}}while(0);do{if((G|0)==64){if(j){break}Z=e|0;c[Z>>2]=W;vf(p);vf(o);i=f;return}}while(0);c[k>>2]=c[k>>2]|2;Z=e|0;c[Z>>2]=W;vf(p);vf(o);i=f;return}function Kh(a,b,d,e,f,g,h){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0;b=i;i=i+16|0;j=d;d=i;i=i+4|0;i=i+7&-8;c[d>>2]=c[j>>2];j=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[j>>2];j=b|0;k=b+8|0;c[j>>2]=c[d>>2];c[k>>2]=c[e>>2];Lh(a,0,j,k,f,g,h);i=b;return}function Lh(b,e,f,g,h,j,k){b=b|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;var l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0;e=i;i=i+144|0;l=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[l>>2];l=g;g=i;i=i+4|0;i=i+7&-8;c[g>>2]=c[l>>2];l=e|0;m=e+104|0;n=e+112|0;o=e+128|0;p=o;q=i;i=i+4|0;i=i+7&-8;r=i;i=i+160|0;s=i;i=i+4|0;i=i+7&-8;t=i;i=i+4|0;i=i+7&-8;u=c[h+4>>2]&74;if((u|0)==8){v=16}else if((u|0)==64){v=8}else if((u|0)==0){v=0}else{v=10}u=l|0;$h(n,h,u,m);ko(p|0,0,12)|0;h=o;xf(o,10,0);if((a[p]&1)==0){l=h+1|0;w=l;x=l;y=o+8|0}else{l=o+8|0;w=c[l>>2]|0;x=h+1|0;y=l}c[q>>2]=w;l=r|0;c[s>>2]=l;c[t>>2]=0;h=f|0;f=g|0;g=o|0;z=o+4|0;A=c[m>>2]|0;m=w;w=c[h>>2]|0;L11:while(1){do{if((w|0)==0){B=0}else{C=c[w+12>>2]|0;if((C|0)==(c[w+16>>2]|0)){D=dd[c[(c[w>>2]|0)+36>>2]&127](w)|0}else{D=c[C>>2]|0}if((D|0)!=-1){B=w;break}c[h>>2]=0;B=0}}while(0);E=(B|0)==0;C=c[f>>2]|0;do{if((C|0)==0){F=22}else{G=c[C+12>>2]|0;if((G|0)==(c[C+16>>2]|0)){H=dd[c[(c[C>>2]|0)+36>>2]&127](C)|0}else{H=c[G>>2]|0}if((H|0)==-1){c[f>>2]=0;F=22;break}else{G=(C|0)==0;if(E^G){I=C;J=G;break}else{K=m;L=C;M=G;break L11}}}}while(0);if((F|0)==22){F=0;if(E){K=m;L=0;M=1;break}else{I=0;J=1}}C=d[p]|0;G=(C&1|0)==0;if(((c[q>>2]|0)-m|0)==((G?C>>>1:c[z>>2]|0)|0)){if(G){N=C>>>1;O=C>>>1}else{C=c[z>>2]|0;N=C;O=C}xf(o,N<<1,0);if((a[p]&1)==0){P=10}else{P=(c[g>>2]&-2)-1|0}xf(o,P,0);if((a[p]&1)==0){Q=x}else{Q=c[y>>2]|0}c[q>>2]=Q+O;R=Q}else{R=m}C=B+12|0;G=c[C>>2]|0;S=B+16|0;if((G|0)==(c[S>>2]|0)){T=dd[c[(c[B>>2]|0)+36>>2]&127](B)|0}else{T=c[G>>2]|0}if((Xh(T,v,R,q,t,A,n,l,s,u)|0)!=0){K=R;L=I;M=J;break}G=c[C>>2]|0;if((G|0)==(c[S>>2]|0)){dd[c[(c[B>>2]|0)+40>>2]&127](B)|0;m=R;w=B;continue}else{c[C>>2]=G+4;m=R;w=B;continue}}w=d[n]|0;if((w&1|0)==0){U=w>>>1}else{U=c[n+4>>2]|0}do{if((U|0)!=0){w=c[s>>2]|0;if((w-r|0)>=160){break}R=c[t>>2]|0;c[s>>2]=w+4;c[w>>2]=R}}while(0);c[k>>2]=Rm(K,c[q>>2]|0,j,v)|0;kk(n,l,c[s>>2]|0,j);do{if(E){V=0}else{s=c[B+12>>2]|0;if((s|0)==(c[B+16>>2]|0)){W=dd[c[(c[B>>2]|0)+36>>2]&127](B)|0}else{W=c[s>>2]|0}if((W|0)!=-1){V=B;break}c[h>>2]=0;V=0}}while(0);h=(V|0)==0;do{if(M){F=64}else{B=c[L+12>>2]|0;if((B|0)==(c[L+16>>2]|0)){X=dd[c[(c[L>>2]|0)+36>>2]&127](L)|0}else{X=c[B>>2]|0}if((X|0)==-1){c[f>>2]=0;F=64;break}if(!(h^(L|0)==0)){break}Y=b|0;c[Y>>2]=V;vf(o);vf(n);i=e;return}}while(0);do{if((F|0)==64){if(h){break}Y=b|0;c[Y>>2]=V;vf(o);vf(n);i=e;return}}while(0);c[j>>2]=c[j>>2]|2;Y=b|0;c[Y>>2]=V;vf(o);vf(n);i=e;return}function Mh(a,b,d,e,f,g,h){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0;b=i;i=i+16|0;j=d;d=i;i=i+4|0;i=i+7&-8;c[d>>2]=c[j>>2];j=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[j>>2];j=b|0;k=b+8|0;c[j>>2]=c[d>>2];c[k>>2]=c[e>>2];Nh(a,0,j,k,f,g,h);i=b;return}function Nh(b,e,f,g,h,j,k){b=b|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;var l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0;e=i;i=i+144|0;l=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[l>>2];l=g;g=i;i=i+4|0;i=i+7&-8;c[g>>2]=c[l>>2];l=e|0;m=e+104|0;n=e+112|0;o=e+128|0;p=o;q=i;i=i+4|0;i=i+7&-8;r=i;i=i+160|0;s=i;i=i+4|0;i=i+7&-8;t=i;i=i+4|0;i=i+7&-8;u=c[h+4>>2]&74;if((u|0)==0){v=0}else if((u|0)==8){v=16}else if((u|0)==64){v=8}else{v=10}u=l|0;$h(n,h,u,m);ko(p|0,0,12)|0;h=o;xf(o,10,0);if((a[p]&1)==0){l=h+1|0;w=l;x=l;y=o+8|0}else{l=o+8|0;w=c[l>>2]|0;x=h+1|0;y=l}c[q>>2]=w;l=r|0;c[s>>2]=l;c[t>>2]=0;h=f|0;f=g|0;g=o|0;z=o+4|0;A=c[m>>2]|0;m=w;w=c[h>>2]|0;L11:while(1){do{if((w|0)==0){B=0}else{C=c[w+12>>2]|0;if((C|0)==(c[w+16>>2]|0)){D=dd[c[(c[w>>2]|0)+36>>2]&127](w)|0}else{D=c[C>>2]|0}if((D|0)!=-1){B=w;break}c[h>>2]=0;B=0}}while(0);E=(B|0)==0;C=c[f>>2]|0;do{if((C|0)==0){F=22}else{G=c[C+12>>2]|0;if((G|0)==(c[C+16>>2]|0)){H=dd[c[(c[C>>2]|0)+36>>2]&127](C)|0}else{H=c[G>>2]|0}if((H|0)==-1){c[f>>2]=0;F=22;break}else{G=(C|0)==0;if(E^G){I=C;J=G;break}else{K=m;L=C;M=G;break L11}}}}while(0);if((F|0)==22){F=0;if(E){K=m;L=0;M=1;break}else{I=0;J=1}}C=d[p]|0;G=(C&1|0)==0;if(((c[q>>2]|0)-m|0)==((G?C>>>1:c[z>>2]|0)|0)){if(G){N=C>>>1;O=C>>>1}else{C=c[z>>2]|0;N=C;O=C}xf(o,N<<1,0);if((a[p]&1)==0){P=10}else{P=(c[g>>2]&-2)-1|0}xf(o,P,0);if((a[p]&1)==0){Q=x}else{Q=c[y>>2]|0}c[q>>2]=Q+O;R=Q}else{R=m}C=B+12|0;G=c[C>>2]|0;S=B+16|0;if((G|0)==(c[S>>2]|0)){T=dd[c[(c[B>>2]|0)+36>>2]&127](B)|0}else{T=c[G>>2]|0}if((Xh(T,v,R,q,t,A,n,l,s,u)|0)!=0){K=R;L=I;M=J;break}G=c[C>>2]|0;if((G|0)==(c[S>>2]|0)){dd[c[(c[B>>2]|0)+40>>2]&127](B)|0;m=R;w=B;continue}else{c[C>>2]=G+4;m=R;w=B;continue}}w=d[n]|0;if((w&1|0)==0){U=w>>>1}else{U=c[n+4>>2]|0}do{if((U|0)!=0){w=c[s>>2]|0;if((w-r|0)>=160){break}R=c[t>>2]|0;c[s>>2]=w+4;c[w>>2]=R}}while(0);c[k>>2]=Qm(K,c[q>>2]|0,j,v)|0;kk(n,l,c[s>>2]|0,j);do{if(E){V=0}else{s=c[B+12>>2]|0;if((s|0)==(c[B+16>>2]|0)){W=dd[c[(c[B>>2]|0)+36>>2]&127](B)|0}else{W=c[s>>2]|0}if((W|0)!=-1){V=B;break}c[h>>2]=0;V=0}}while(0);h=(V|0)==0;do{if(M){F=64}else{B=c[L+12>>2]|0;if((B|0)==(c[L+16>>2]|0)){X=dd[c[(c[L>>2]|0)+36>>2]&127](L)|0}else{X=c[B>>2]|0}if((X|0)==-1){c[f>>2]=0;F=64;break}if(!(h^(L|0)==0)){break}Y=b|0;c[Y>>2]=V;vf(o);vf(n);i=e;return}}while(0);do{if((F|0)==64){if(h){break}Y=b|0;c[Y>>2]=V;vf(o);vf(n);i=e;return}}while(0);c[j>>2]=c[j>>2]|2;Y=b|0;c[Y>>2]=V;vf(o);vf(n);i=e;return}function Oh(a,b,d,e,f,g,h){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0;b=i;i=i+16|0;j=d;d=i;i=i+4|0;i=i+7&-8;c[d>>2]=c[j>>2];j=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[j>>2];j=b|0;k=b+8|0;c[j>>2]=c[d>>2];c[k>>2]=c[e>>2];Ph(a,0,j,k,f,g,h);i=b;return}function Ph(b,e,f,g,h,j,k){b=b|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;var l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0,Z=0;e=i;i=i+144|0;l=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[l>>2];l=g;g=i;i=i+4|0;i=i+7&-8;c[g>>2]=c[l>>2];l=e|0;m=e+104|0;n=e+112|0;o=e+128|0;p=o;q=i;i=i+4|0;i=i+7&-8;r=i;i=i+160|0;s=i;i=i+4|0;i=i+7&-8;t=i;i=i+4|0;i=i+7&-8;u=c[h+4>>2]&74;if((u|0)==8){v=16}else if((u|0)==64){v=8}else if((u|0)==0){v=0}else{v=10}u=l|0;$h(n,h,u,m);ko(p|0,0,12)|0;h=o;xf(o,10,0);if((a[p]&1)==0){l=h+1|0;w=l;x=l;y=o+8|0}else{l=o+8|0;w=c[l>>2]|0;x=h+1|0;y=l}c[q>>2]=w;l=r|0;c[s>>2]=l;c[t>>2]=0;h=f|0;f=g|0;g=o|0;z=o+4|0;A=c[m>>2]|0;m=w;w=c[h>>2]|0;L11:while(1){do{if((w|0)==0){B=0}else{C=c[w+12>>2]|0;if((C|0)==(c[w+16>>2]|0)){D=dd[c[(c[w>>2]|0)+36>>2]&127](w)|0}else{D=c[C>>2]|0}if((D|0)!=-1){B=w;break}c[h>>2]=0;B=0}}while(0);E=(B|0)==0;C=c[f>>2]|0;do{if((C|0)==0){F=22}else{G=c[C+12>>2]|0;if((G|0)==(c[C+16>>2]|0)){H=dd[c[(c[C>>2]|0)+36>>2]&127](C)|0}else{H=c[G>>2]|0}if((H|0)==-1){c[f>>2]=0;F=22;break}else{G=(C|0)==0;if(E^G){I=C;J=G;break}else{L=m;M=C;N=G;break L11}}}}while(0);if((F|0)==22){F=0;if(E){L=m;M=0;N=1;break}else{I=0;J=1}}C=d[p]|0;G=(C&1|0)==0;if(((c[q>>2]|0)-m|0)==((G?C>>>1:c[z>>2]|0)|0)){if(G){O=C>>>1;P=C>>>1}else{C=c[z>>2]|0;O=C;P=C}xf(o,O<<1,0);if((a[p]&1)==0){Q=10}else{Q=(c[g>>2]&-2)-1|0}xf(o,Q,0);if((a[p]&1)==0){R=x}else{R=c[y>>2]|0}c[q>>2]=R+P;S=R}else{S=m}C=B+12|0;G=c[C>>2]|0;T=B+16|0;if((G|0)==(c[T>>2]|0)){U=dd[c[(c[B>>2]|0)+36>>2]&127](B)|0}else{U=c[G>>2]|0}if((Xh(U,v,S,q,t,A,n,l,s,u)|0)!=0){L=S;M=I;N=J;break}G=c[C>>2]|0;if((G|0)==(c[T>>2]|0)){dd[c[(c[B>>2]|0)+40>>2]&127](B)|0;m=S;w=B;continue}else{c[C>>2]=G+4;m=S;w=B;continue}}w=d[n]|0;if((w&1|0)==0){V=w>>>1}else{V=c[n+4>>2]|0}do{if((V|0)!=0){w=c[s>>2]|0;if((w-r|0)>=160){break}S=c[t>>2]|0;c[s>>2]=w+4;c[w>>2]=S}}while(0);t=Pm(L,c[q>>2]|0,j,v)|0;c[k>>2]=t;c[k+4>>2]=K;kk(n,l,c[s>>2]|0,j);do{if(E){W=0}else{s=c[B+12>>2]|0;if((s|0)==(c[B+16>>2]|0)){X=dd[c[(c[B>>2]|0)+36>>2]&127](B)|0}else{X=c[s>>2]|0}if((X|0)!=-1){W=B;break}c[h>>2]=0;W=0}}while(0);h=(W|0)==0;do{if(N){F=64}else{B=c[M+12>>2]|0;if((B|0)==(c[M+16>>2]|0)){Y=dd[c[(c[M>>2]|0)+36>>2]&127](M)|0}else{Y=c[B>>2]|0}if((Y|0)==-1){c[f>>2]=0;F=64;break}if(!(h^(M|0)==0)){break}Z=b|0;c[Z>>2]=W;vf(o);vf(n);i=e;return}}while(0);do{if((F|0)==64){if(h){break}Z=b|0;c[Z>>2]=W;vf(o);vf(n);i=e;return}}while(0);c[j>>2]=c[j>>2]|2;Z=b|0;c[Z>>2]=W;vf(o);vf(n);i=e;return}function Qh(a,b,d,e,f,g,h){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0;b=i;i=i+16|0;j=d;d=i;i=i+4|0;i=i+7&-8;c[d>>2]=c[j>>2];j=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[j>>2];j=b|0;k=b+8|0;c[j>>2]=c[d>>2];c[k>>2]=c[e>>2];Rh(a,0,j,k,f,g,h);i=b;return}function Rh(b,e,f,h,j,k,l){b=b|0;e=e|0;f=f|0;h=h|0;j=j|0;k=k|0;l=l|0;var m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0,Z=0,_=0,$=0;e=i;i=i+176|0;m=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[m>>2];m=h;h=i;i=i+4|0;i=i+7&-8;c[h>>2]=c[m>>2];m=e+128|0;n=e+136|0;o=e+144|0;p=e+160|0;q=p;r=i;i=i+4|0;i=i+7&-8;s=i;i=i+160|0;t=i;i=i+4|0;i=i+7&-8;u=i;i=i+4|0;i=i+7&-8;v=i;i=i+1|0;i=i+7&-8;w=i;i=i+1|0;i=i+7&-8;x=e|0;ai(o,j,x,m,n);ko(q|0,0,12)|0;j=p;xf(p,10,0);if((a[q]&1)==0){y=j+1|0;z=y;A=y;B=p+8|0}else{y=p+8|0;z=c[y>>2]|0;A=j+1|0;B=y}c[r>>2]=z;y=s|0;c[t>>2]=y;c[u>>2]=0;a[v]=1;a[w]=69;j=f|0;f=h|0;h=p|0;C=p+4|0;D=c[m>>2]|0;m=c[n>>2]|0;n=z;z=c[j>>2]|0;L6:while(1){do{if((z|0)==0){E=0}else{F=c[z+12>>2]|0;if((F|0)==(c[z+16>>2]|0)){G=dd[c[(c[z>>2]|0)+36>>2]&127](z)|0}else{G=c[F>>2]|0}if((G|0)!=-1){E=z;break}c[j>>2]=0;E=0}}while(0);H=(E|0)==0;F=c[f>>2]|0;do{if((F|0)==0){I=18}else{J=c[F+12>>2]|0;if((J|0)==(c[F+16>>2]|0)){K=dd[c[(c[F>>2]|0)+36>>2]&127](F)|0}else{K=c[J>>2]|0}if((K|0)==-1){c[f>>2]=0;I=18;break}else{J=(F|0)==0;if(H^J){L=F;M=J;break}else{N=n;O=F;P=J;break L6}}}}while(0);if((I|0)==18){I=0;if(H){N=n;O=0;P=1;break}else{L=0;M=1}}F=d[q]|0;J=(F&1|0)==0;if(((c[r>>2]|0)-n|0)==((J?F>>>1:c[C>>2]|0)|0)){if(J){Q=F>>>1;R=F>>>1}else{F=c[C>>2]|0;Q=F;R=F}xf(p,Q<<1,0);if((a[q]&1)==0){S=10}else{S=(c[h>>2]&-2)-1|0}xf(p,S,0);if((a[q]&1)==0){T=A}else{T=c[B>>2]|0}c[r>>2]=T+R;U=T}else{U=n}F=E+12|0;J=c[F>>2]|0;V=E+16|0;if((J|0)==(c[V>>2]|0)){W=dd[c[(c[E>>2]|0)+36>>2]&127](E)|0}else{W=c[J>>2]|0}if((bi(W,v,w,U,r,D,m,o,y,t,u,x)|0)!=0){N=U;O=L;P=M;break}J=c[F>>2]|0;if((J|0)==(c[V>>2]|0)){dd[c[(c[E>>2]|0)+40>>2]&127](E)|0;n=U;z=E;continue}else{c[F>>2]=J+4;n=U;z=E;continue}}z=d[o]|0;if((z&1|0)==0){X=z>>>1}else{X=c[o+4>>2]|0}do{if((X|0)!=0){if((a[v]&1)==0){break}z=c[t>>2]|0;if((z-s|0)>=160){break}U=c[u>>2]|0;c[t>>2]=z+4;c[z>>2]=U}}while(0);g[l>>2]=+Om(N,c[r>>2]|0,k);kk(o,y,c[t>>2]|0,k);do{if(H){Y=0}else{t=c[E+12>>2]|0;if((t|0)==(c[E+16>>2]|0)){Z=dd[c[(c[E>>2]|0)+36>>2]&127](E)|0}else{Z=c[t>>2]|0}if((Z|0)!=-1){Y=E;break}c[j>>2]=0;Y=0}}while(0);j=(Y|0)==0;do{if(P){I=61}else{E=c[O+12>>2]|0;if((E|0)==(c[O+16>>2]|0)){_=dd[c[(c[O>>2]|0)+36>>2]&127](O)|0}else{_=c[E>>2]|0}if((_|0)==-1){c[f>>2]=0;I=61;break}if(!(j^(O|0)==0)){break}$=b|0;c[$>>2]=Y;vf(p);vf(o);i=e;return}}while(0);do{if((I|0)==61){if(j){break}$=b|0;c[$>>2]=Y;vf(p);vf(o);i=e;return}}while(0);c[k>>2]=c[k>>2]|2;$=b|0;c[$>>2]=Y;vf(p);vf(o);i=e;return}function Sh(a,b,d,e,f,g,h){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0;b=i;i=i+16|0;j=d;d=i;i=i+4|0;i=i+7&-8;c[d>>2]=c[j>>2];j=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[j>>2];j=b|0;k=b+8|0;c[j>>2]=c[d>>2];c[k>>2]=c[e>>2];Th(a,0,j,k,f,g,h);i=b;return}function Th(b,e,f,g,j,k,l){b=b|0;e=e|0;f=f|0;g=g|0;j=j|0;k=k|0;l=l|0;var m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0,Z=0,_=0,$=0;e=i;i=i+176|0;m=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[m>>2];m=g;g=i;i=i+4|0;i=i+7&-8;c[g>>2]=c[m>>2];m=e+128|0;n=e+136|0;o=e+144|0;p=e+160|0;q=p;r=i;i=i+4|0;i=i+7&-8;s=i;i=i+160|0;t=i;i=i+4|0;i=i+7&-8;u=i;i=i+4|0;i=i+7&-8;v=i;i=i+1|0;i=i+7&-8;w=i;i=i+1|0;i=i+7&-8;x=e|0;ai(o,j,x,m,n);ko(q|0,0,12)|0;j=p;xf(p,10,0);if((a[q]&1)==0){y=j+1|0;z=y;A=y;B=p+8|0}else{y=p+8|0;z=c[y>>2]|0;A=j+1|0;B=y}c[r>>2]=z;y=s|0;c[t>>2]=y;c[u>>2]=0;a[v]=1;a[w]=69;j=f|0;f=g|0;g=p|0;C=p+4|0;D=c[m>>2]|0;m=c[n>>2]|0;n=z;z=c[j>>2]|0;L6:while(1){do{if((z|0)==0){E=0}else{F=c[z+12>>2]|0;if((F|0)==(c[z+16>>2]|0)){G=dd[c[(c[z>>2]|0)+36>>2]&127](z)|0}else{G=c[F>>2]|0}if((G|0)!=-1){E=z;break}c[j>>2]=0;E=0}}while(0);H=(E|0)==0;F=c[f>>2]|0;do{if((F|0)==0){I=18}else{J=c[F+12>>2]|0;if((J|0)==(c[F+16>>2]|0)){K=dd[c[(c[F>>2]|0)+36>>2]&127](F)|0}else{K=c[J>>2]|0}if((K|0)==-1){c[f>>2]=0;I=18;break}else{J=(F|0)==0;if(H^J){L=F;M=J;break}else{N=n;O=F;P=J;break L6}}}}while(0);if((I|0)==18){I=0;if(H){N=n;O=0;P=1;break}else{L=0;M=1}}F=d[q]|0;J=(F&1|0)==0;if(((c[r>>2]|0)-n|0)==((J?F>>>1:c[C>>2]|0)|0)){if(J){Q=F>>>1;R=F>>>1}else{F=c[C>>2]|0;Q=F;R=F}xf(p,Q<<1,0);if((a[q]&1)==0){S=10}else{S=(c[g>>2]&-2)-1|0}xf(p,S,0);if((a[q]&1)==0){T=A}else{T=c[B>>2]|0}c[r>>2]=T+R;U=T}else{U=n}F=E+12|0;J=c[F>>2]|0;V=E+16|0;if((J|0)==(c[V>>2]|0)){W=dd[c[(c[E>>2]|0)+36>>2]&127](E)|0}else{W=c[J>>2]|0}if((bi(W,v,w,U,r,D,m,o,y,t,u,x)|0)!=0){N=U;O=L;P=M;break}J=c[F>>2]|0;if((J|0)==(c[V>>2]|0)){dd[c[(c[E>>2]|0)+40>>2]&127](E)|0;n=U;z=E;continue}else{c[F>>2]=J+4;n=U;z=E;continue}}z=d[o]|0;if((z&1|0)==0){X=z>>>1}else{X=c[o+4>>2]|0}do{if((X|0)!=0){if((a[v]&1)==0){break}z=c[t>>2]|0;if((z-s|0)>=160){break}U=c[u>>2]|0;c[t>>2]=z+4;c[z>>2]=U}}while(0);h[l>>3]=+Nm(N,c[r>>2]|0,k);kk(o,y,c[t>>2]|0,k);do{if(H){Y=0}else{t=c[E+12>>2]|0;if((t|0)==(c[E+16>>2]|0)){Z=dd[c[(c[E>>2]|0)+36>>2]&127](E)|0}else{Z=c[t>>2]|0}if((Z|0)!=-1){Y=E;break}c[j>>2]=0;Y=0}}while(0);j=(Y|0)==0;do{if(P){I=61}else{E=c[O+12>>2]|0;if((E|0)==(c[O+16>>2]|0)){_=dd[c[(c[O>>2]|0)+36>>2]&127](O)|0}else{_=c[E>>2]|0}if((_|0)==-1){c[f>>2]=0;I=61;break}if(!(j^(O|0)==0)){break}$=b|0;c[$>>2]=Y;vf(p);vf(o);i=e;return}}while(0);do{if((I|0)==61){if(j){break}$=b|0;c[$>>2]=Y;vf(p);vf(o);i=e;return}}while(0);c[k>>2]=c[k>>2]|2;$=b|0;c[$>>2]=Y;vf(p);vf(o);i=e;return}function Uh(a,b,d,e,f,g,h){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0;b=i;i=i+16|0;j=d;d=i;i=i+4|0;i=i+7&-8;c[d>>2]=c[j>>2];j=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[j>>2];j=b|0;k=b+8|0;c[j>>2]=c[d>>2];c[k>>2]=c[e>>2];Vh(a,0,j,k,f,g,h);i=b;return}function Vh(b,e,f,g,j,k,l){b=b|0;e=e|0;f=f|0;g=g|0;j=j|0;k=k|0;l=l|0;var m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0,Z=0,_=0,$=0;e=i;i=i+176|0;m=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[m>>2];m=g;g=i;i=i+4|0;i=i+7&-8;c[g>>2]=c[m>>2];m=e+128|0;n=e+136|0;o=e+144|0;p=e+160|0;q=p;r=i;i=i+4|0;i=i+7&-8;s=i;i=i+160|0;t=i;i=i+4|0;i=i+7&-8;u=i;i=i+4|0;i=i+7&-8;v=i;i=i+1|0;i=i+7&-8;w=i;i=i+1|0;i=i+7&-8;x=e|0;ai(o,j,x,m,n);ko(q|0,0,12)|0;j=p;xf(p,10,0);if((a[q]&1)==0){y=j+1|0;z=y;A=y;B=p+8|0}else{y=p+8|0;z=c[y>>2]|0;A=j+1|0;B=y}c[r>>2]=z;y=s|0;c[t>>2]=y;c[u>>2]=0;a[v]=1;a[w]=69;j=f|0;f=g|0;g=p|0;C=p+4|0;D=c[m>>2]|0;m=c[n>>2]|0;n=z;z=c[j>>2]|0;L6:while(1){do{if((z|0)==0){E=0}else{F=c[z+12>>2]|0;if((F|0)==(c[z+16>>2]|0)){G=dd[c[(c[z>>2]|0)+36>>2]&127](z)|0}else{G=c[F>>2]|0}if((G|0)!=-1){E=z;break}c[j>>2]=0;E=0}}while(0);H=(E|0)==0;F=c[f>>2]|0;do{if((F|0)==0){I=18}else{J=c[F+12>>2]|0;if((J|0)==(c[F+16>>2]|0)){K=dd[c[(c[F>>2]|0)+36>>2]&127](F)|0}else{K=c[J>>2]|0}if((K|0)==-1){c[f>>2]=0;I=18;break}else{J=(F|0)==0;if(H^J){L=F;M=J;break}else{N=n;O=F;P=J;break L6}}}}while(0);if((I|0)==18){I=0;if(H){N=n;O=0;P=1;break}else{L=0;M=1}}F=d[q]|0;J=(F&1|0)==0;if(((c[r>>2]|0)-n|0)==((J?F>>>1:c[C>>2]|0)|0)){if(J){Q=F>>>1;R=F>>>1}else{F=c[C>>2]|0;Q=F;R=F}xf(p,Q<<1,0);if((a[q]&1)==0){S=10}else{S=(c[g>>2]&-2)-1|0}xf(p,S,0);if((a[q]&1)==0){T=A}else{T=c[B>>2]|0}c[r>>2]=T+R;U=T}else{U=n}F=E+12|0;J=c[F>>2]|0;V=E+16|0;if((J|0)==(c[V>>2]|0)){W=dd[c[(c[E>>2]|0)+36>>2]&127](E)|0}else{W=c[J>>2]|0}if((bi(W,v,w,U,r,D,m,o,y,t,u,x)|0)!=0){N=U;O=L;P=M;break}J=c[F>>2]|0;if((J|0)==(c[V>>2]|0)){dd[c[(c[E>>2]|0)+40>>2]&127](E)|0;n=U;z=E;continue}else{c[F>>2]=J+4;n=U;z=E;continue}}z=d[o]|0;if((z&1|0)==0){X=z>>>1}else{X=c[o+4>>2]|0}do{if((X|0)!=0){if((a[v]&1)==0){break}z=c[t>>2]|0;if((z-s|0)>=160){break}U=c[u>>2]|0;c[t>>2]=z+4;c[z>>2]=U}}while(0);h[l>>3]=+Mm(N,c[r>>2]|0,k);kk(o,y,c[t>>2]|0,k);do{if(H){Y=0}else{t=c[E+12>>2]|0;if((t|0)==(c[E+16>>2]|0)){Z=dd[c[(c[E>>2]|0)+36>>2]&127](E)|0}else{Z=c[t>>2]|0}if((Z|0)!=-1){Y=E;break}c[j>>2]=0;Y=0}}while(0);j=(Y|0)==0;do{if(P){I=61}else{E=c[O+12>>2]|0;if((E|0)==(c[O+16>>2]|0)){_=dd[c[(c[O>>2]|0)+36>>2]&127](O)|0}else{_=c[E>>2]|0}if((_|0)==-1){c[f>>2]=0;I=61;break}if(!(j^(O|0)==0)){break}$=b|0;c[$>>2]=Y;vf(p);vf(o);i=e;return}}while(0);do{if((I|0)==61){if(j){break}$=b|0;c[$>>2]=Y;vf(p);vf(o);i=e;return}}while(0);c[k>>2]=c[k>>2]|2;$=b|0;c[$>>2]=Y;vf(p);vf(o);i=e;return}function Wh(b,e,f,g,h,j,k){b=b|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;var l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0,Z=0;e=i;i=i+136|0;l=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[l>>2];l=g;g=i;i=i+4|0;i=i+7&-8;c[g>>2]=c[l>>2];l=e|0;m=e+16|0;n=e+120|0;o=i;i=i+4|0;i=i+7&-8;p=i;i=i+12|0;i=i+7&-8;q=i;i=i+4|0;i=i+7&-8;r=i;i=i+160|0;s=i;i=i+4|0;i=i+7&-8;t=i;i=i+4|0;i=i+7&-8;ko(n|0,0,12)|0;u=p;Pf(o,h);h=o|0;o=c[h>>2]|0;if((c[8974]|0)!=-1){c[l>>2]=35896;c[l+4>>2]=14;c[l+8>>2]=0;qf(35896,l,96)}l=(c[8975]|0)-1|0;v=c[o+8>>2]|0;do{if((c[o+12>>2]|0)-v>>2>>>0>l>>>0){w=c[v+(l<<2)>>2]|0;if((w|0)==0){break}x=m|0;ld[c[(c[w>>2]|0)+48>>2]&15](w,11736,11762,x)|0;Xe(c[h>>2]|0)|0;ko(u|0,0,12)|0;w=p;xf(p,10,0);if((a[u]&1)==0){y=w+1|0;z=y;A=y;B=p+8|0}else{y=p+8|0;z=c[y>>2]|0;A=w+1|0;B=y}c[q>>2]=z;y=r|0;c[s>>2]=y;c[t>>2]=0;w=f|0;C=g|0;D=p|0;E=p+4|0;F=z;G=c[w>>2]|0;L14:while(1){do{if((G|0)==0){H=0}else{I=c[G+12>>2]|0;if((I|0)==(c[G+16>>2]|0)){J=dd[c[(c[G>>2]|0)+36>>2]&127](G)|0}else{J=c[I>>2]|0}if((J|0)!=-1){H=G;break}c[w>>2]=0;H=0}}while(0);I=(H|0)==0;K=c[C>>2]|0;do{if((K|0)==0){L=26}else{M=c[K+12>>2]|0;if((M|0)==(c[K+16>>2]|0)){N=dd[c[(c[K>>2]|0)+36>>2]&127](K)|0}else{N=c[M>>2]|0}if((N|0)==-1){c[C>>2]=0;L=26;break}else{if(I^(K|0)==0){break}else{O=F;break L14}}}}while(0);if((L|0)==26){L=0;if(I){O=F;break}}K=d[u]|0;M=(K&1|0)==0;if(((c[q>>2]|0)-F|0)==((M?K>>>1:c[E>>2]|0)|0)){if(M){P=K>>>1;Q=K>>>1}else{K=c[E>>2]|0;P=K;Q=K}xf(p,P<<1,0);if((a[u]&1)==0){R=10}else{R=(c[D>>2]&-2)-1|0}xf(p,R,0);if((a[u]&1)==0){S=A}else{S=c[B>>2]|0}c[q>>2]=S+Q;T=S}else{T=F}K=H+12|0;M=c[K>>2]|0;U=H+16|0;if((M|0)==(c[U>>2]|0)){V=dd[c[(c[H>>2]|0)+36>>2]&127](H)|0}else{V=c[M>>2]|0}if((Xh(V,16,T,q,t,0,n,y,s,x)|0)!=0){O=T;break}M=c[K>>2]|0;if((M|0)==(c[U>>2]|0)){dd[c[(c[H>>2]|0)+40>>2]&127](H)|0;F=T;G=H;continue}else{c[K>>2]=M+4;F=T;G=H;continue}}a[O+3|0]=0;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);G=zh(O,c[8322]|0,3056,(F=i,i=i+8|0,c[F>>2]=k,F)|0)|0;i=F;if((G|0)!=1){c[j>>2]=4}G=c[w>>2]|0;do{if((G|0)==0){W=0}else{F=c[G+12>>2]|0;if((F|0)==(c[G+16>>2]|0)){X=dd[c[(c[G>>2]|0)+36>>2]&127](G)|0}else{X=c[F>>2]|0}if((X|0)!=-1){W=G;break}c[w>>2]=0;W=0}}while(0);w=(W|0)==0;G=c[C>>2]|0;do{if((G|0)==0){L=71}else{F=c[G+12>>2]|0;if((F|0)==(c[G+16>>2]|0)){Y=dd[c[(c[G>>2]|0)+36>>2]&127](G)|0}else{Y=c[F>>2]|0}if((Y|0)==-1){c[C>>2]=0;L=71;break}if(!(w^(G|0)==0)){break}Z=b|0;c[Z>>2]=W;vf(p);vf(n);i=e;return}}while(0);do{if((L|0)==71){if(w){break}Z=b|0;c[Z>>2]=W;vf(p);vf(n);i=e;return}}while(0);c[j>>2]=c[j>>2]|2;Z=b|0;c[Z>>2]=W;vf(p);vf(n);i=e;return}}while(0);e=Nc(4)|0;qn(e);Qb(e|0,10192,134)}function Xh(b,e,f,g,h,i,j,k,l,m){b=b|0;e=e|0;f=f|0;g=g|0;h=h|0;i=i|0;j=j|0;k=k|0;l=l|0;m=m|0;var n=0,o=0,p=0,q=0,r=0,s=0;n=c[g>>2]|0;o=(n|0)==(f|0);do{if(o){p=(c[m+96>>2]|0)==(b|0);if(!p){if((c[m+100>>2]|0)!=(b|0)){break}}c[g>>2]=f+1;a[f]=p?43:45;c[h>>2]=0;q=0;return q|0}}while(0);p=d[j]|0;if((p&1|0)==0){r=p>>>1}else{r=c[j+4>>2]|0}if((r|0)!=0&(b|0)==(i|0)){i=c[l>>2]|0;if((i-k|0)>=160){q=0;return q|0}k=c[h>>2]|0;c[l>>2]=i+4;c[i>>2]=k;c[h>>2]=0;q=0;return q|0}k=m+104|0;i=m;while(1){l=i+4|0;if((c[i>>2]|0)==(b|0)){s=i;break}if((l|0)==(k|0)){s=k;break}else{i=l}}i=s-m|0;m=i>>2;if((i|0)>92){q=-1;return q|0}do{if((e|0)==8|(e|0)==10){if((m|0)<(e|0)){break}else{q=-1}return q|0}else if((e|0)==16){if((i|0)<88){break}if(o){q=-1;return q|0}if((n-f|0)>=3){q=-1;return q|0}if((a[n-1|0]|0)!=48){q=-1;return q|0}c[h>>2]=0;s=a[11736+m|0]|0;k=c[g>>2]|0;c[g>>2]=k+1;a[k]=s;q=0;return q|0}}while(0);f=a[11736+m|0]|0;c[g>>2]=n+1;a[n]=f;c[h>>2]=(c[h>>2]|0)+1;q=0;return q|0}function Yh(b,d,e,f){b=b|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0;g=i;i=i+40|0;h=g|0;j=g+16|0;k=g+32|0;Pf(k,d);d=k|0;k=c[d>>2]|0;if((c[8976]|0)!=-1){c[j>>2]=35904;c[j+4>>2]=14;c[j+8>>2]=0;qf(35904,j,96)}j=(c[8977]|0)-1|0;l=c[k+8>>2]|0;do{if((c[k+12>>2]|0)-l>>2>>>0>j>>>0){m=c[l+(j<<2)>>2]|0;if((m|0)==0){break}ld[c[(c[m>>2]|0)+32>>2]&15](m,11736,11762,e)|0;m=c[d>>2]|0;if((c[8880]|0)!=-1){c[h>>2]=35520;c[h+4>>2]=14;c[h+8>>2]=0;qf(35520,h,96)}n=(c[8881]|0)-1|0;o=c[m+8>>2]|0;do{if((c[m+12>>2]|0)-o>>2>>>0>n>>>0){p=c[o+(n<<2)>>2]|0;if((p|0)==0){break}q=p;a[f]=dd[c[(c[p>>2]|0)+16>>2]&127](q)|0;$c[c[(c[p>>2]|0)+20>>2]&127](b,q);Xe(c[d>>2]|0)|0;i=g;return}}while(0);n=Nc(4)|0;qn(n);Qb(n|0,10192,134)}}while(0);g=Nc(4)|0;qn(g);Qb(g|0,10192,134)}function Zh(b,d,e,f,g){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;var h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0;h=i;i=i+40|0;j=h|0;k=h+16|0;l=h+32|0;Pf(l,d);d=l|0;l=c[d>>2]|0;if((c[8976]|0)!=-1){c[k>>2]=35904;c[k+4>>2]=14;c[k+8>>2]=0;qf(35904,k,96)}k=(c[8977]|0)-1|0;m=c[l+8>>2]|0;do{if((c[l+12>>2]|0)-m>>2>>>0>k>>>0){n=c[m+(k<<2)>>2]|0;if((n|0)==0){break}ld[c[(c[n>>2]|0)+32>>2]&15](n,11736,11768,e)|0;n=c[d>>2]|0;if((c[8880]|0)!=-1){c[j>>2]=35520;c[j+4>>2]=14;c[j+8>>2]=0;qf(35520,j,96)}o=(c[8881]|0)-1|0;p=c[n+8>>2]|0;do{if((c[n+12>>2]|0)-p>>2>>>0>o>>>0){q=c[p+(o<<2)>>2]|0;if((q|0)==0){break}r=q;s=q;a[f]=dd[c[(c[s>>2]|0)+12>>2]&127](r)|0;a[g]=dd[c[(c[s>>2]|0)+16>>2]&127](r)|0;$c[c[(c[q>>2]|0)+20>>2]&127](b,r);Xe(c[d>>2]|0)|0;i=h;return}}while(0);o=Nc(4)|0;qn(o);Qb(o|0,10192,134)}}while(0);h=Nc(4)|0;qn(h);Qb(h|0,10192,134)}function _h(b,e,f,g,h,i,j,k,l,m,n,o){b=b|0;e=e|0;f=f|0;g=g|0;h=h|0;i=i|0;j=j|0;k=k|0;l=l|0;m=m|0;n=n|0;o=o|0;var p=0,q=0,r=0,s=0,t=0;if(b<<24>>24==i<<24>>24){if((a[e]&1)==0){p=-1;return p|0}a[e]=0;i=c[h>>2]|0;c[h>>2]=i+1;a[i]=46;i=d[k]|0;if((i&1|0)==0){q=i>>>1}else{q=c[k+4>>2]|0}if((q|0)==0){p=0;return p|0}q=c[m>>2]|0;if((q-l|0)>=160){p=0;return p|0}i=c[n>>2]|0;c[m>>2]=q+4;c[q>>2]=i;p=0;return p|0}do{if(b<<24>>24==j<<24>>24){i=d[k]|0;if((i&1|0)==0){r=i>>>1}else{r=c[k+4>>2]|0}if((r|0)==0){break}if((a[e]&1)==0){p=-1;return p|0}i=c[m>>2]|0;if((i-l|0)>=160){p=0;return p|0}q=c[n>>2]|0;c[m>>2]=i+4;c[i>>2]=q;c[n>>2]=0;p=0;return p|0}}while(0);r=o+32|0;j=o;while(1){q=j+1|0;if((a[j]|0)==b<<24>>24){s=j;break}if((q|0)==(r|0)){s=r;break}else{j=q}}j=s-o|0;if((j|0)>31){p=-1;return p|0}o=a[11736+j|0]|0;if((j|0)==25|(j|0)==24){s=c[h>>2]|0;do{if((s|0)!=(g|0)){if((a[s-1|0]&95|0)==(a[f]&127|0)){break}else{p=-1}return p|0}}while(0);c[h>>2]=s+1;a[s]=o;p=0;return p|0}else if((j|0)==22|(j|0)==23){a[f]=80;s=c[h>>2]|0;c[h>>2]=s+1;a[s]=o;p=0;return p|0}else{s=a[f]|0;do{if((o&95|0)==(s<<24>>24|0)){a[f]=s|-128;if((a[e]&1)==0){break}a[e]=0;g=d[k]|0;if((g&1|0)==0){t=g>>>1}else{t=c[k+4>>2]|0}if((t|0)==0){break}g=c[m>>2]|0;if((g-l|0)>=160){break}r=c[n>>2]|0;c[m>>2]=g+4;c[g>>2]=r}}while(0);m=c[h>>2]|0;c[h>>2]=m+1;a[m]=o;if((j|0)>21){p=0;return p|0}c[n>>2]=(c[n>>2]|0)+1;p=0;return p|0}return 0}function $h(a,b,d,e){a=a|0;b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0;f=i;i=i+40|0;g=f|0;h=f+16|0;j=f+32|0;Pf(j,b);b=j|0;j=c[b>>2]|0;if((c[8974]|0)!=-1){c[h>>2]=35896;c[h+4>>2]=14;c[h+8>>2]=0;qf(35896,h,96)}h=(c[8975]|0)-1|0;k=c[j+8>>2]|0;do{if((c[j+12>>2]|0)-k>>2>>>0>h>>>0){l=c[k+(h<<2)>>2]|0;if((l|0)==0){break}ld[c[(c[l>>2]|0)+48>>2]&15](l,11736,11762,d)|0;l=c[b>>2]|0;if((c[8878]|0)!=-1){c[g>>2]=35512;c[g+4>>2]=14;c[g+8>>2]=0;qf(35512,g,96)}m=(c[8879]|0)-1|0;n=c[l+8>>2]|0;do{if((c[l+12>>2]|0)-n>>2>>>0>m>>>0){o=c[n+(m<<2)>>2]|0;if((o|0)==0){break}p=o;c[e>>2]=dd[c[(c[o>>2]|0)+16>>2]&127](p)|0;$c[c[(c[o>>2]|0)+20>>2]&127](a,p);Xe(c[b>>2]|0)|0;i=f;return}}while(0);m=Nc(4)|0;qn(m);Qb(m|0,10192,134)}}while(0);f=Nc(4)|0;qn(f);Qb(f|0,10192,134)}function ai(a,b,d,e,f){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0;g=i;i=i+40|0;h=g|0;j=g+16|0;k=g+32|0;Pf(k,b);b=k|0;k=c[b>>2]|0;if((c[8974]|0)!=-1){c[j>>2]=35896;c[j+4>>2]=14;c[j+8>>2]=0;qf(35896,j,96)}j=(c[8975]|0)-1|0;l=c[k+8>>2]|0;do{if((c[k+12>>2]|0)-l>>2>>>0>j>>>0){m=c[l+(j<<2)>>2]|0;if((m|0)==0){break}ld[c[(c[m>>2]|0)+48>>2]&15](m,11736,11768,d)|0;m=c[b>>2]|0;if((c[8878]|0)!=-1){c[h>>2]=35512;c[h+4>>2]=14;c[h+8>>2]=0;qf(35512,h,96)}n=(c[8879]|0)-1|0;o=c[m+8>>2]|0;do{if((c[m+12>>2]|0)-o>>2>>>0>n>>>0){p=c[o+(n<<2)>>2]|0;if((p|0)==0){break}q=p;r=p;c[e>>2]=dd[c[(c[r>>2]|0)+12>>2]&127](q)|0;c[f>>2]=dd[c[(c[r>>2]|0)+16>>2]&127](q)|0;$c[c[(c[p>>2]|0)+20>>2]&127](a,q);Xe(c[b>>2]|0)|0;i=g;return}}while(0);n=Nc(4)|0;qn(n);Qb(n|0,10192,134)}}while(0);g=Nc(4)|0;qn(g);Qb(g|0,10192,134)}function bi(b,e,f,g,h,i,j,k,l,m,n,o){b=b|0;e=e|0;f=f|0;g=g|0;h=h|0;i=i|0;j=j|0;k=k|0;l=l|0;m=m|0;n=n|0;o=o|0;var p=0,q=0,r=0,s=0,t=0;if((b|0)==(i|0)){if((a[e]&1)==0){p=-1;return p|0}a[e]=0;i=c[h>>2]|0;c[h>>2]=i+1;a[i]=46;i=d[k]|0;if((i&1|0)==0){q=i>>>1}else{q=c[k+4>>2]|0}if((q|0)==0){p=0;return p|0}q=c[m>>2]|0;if((q-l|0)>=160){p=0;return p|0}i=c[n>>2]|0;c[m>>2]=q+4;c[q>>2]=i;p=0;return p|0}do{if((b|0)==(j|0)){i=d[k]|0;if((i&1|0)==0){r=i>>>1}else{r=c[k+4>>2]|0}if((r|0)==0){break}if((a[e]&1)==0){p=-1;return p|0}i=c[m>>2]|0;if((i-l|0)>=160){p=0;return p|0}q=c[n>>2]|0;c[m>>2]=i+4;c[i>>2]=q;c[n>>2]=0;p=0;return p|0}}while(0);r=o+128|0;j=o;while(1){q=j+4|0;if((c[j>>2]|0)==(b|0)){s=j;break}if((q|0)==(r|0)){s=r;break}else{j=q}}j=s-o|0;o=j>>2;if((j|0)>124){p=-1;return p|0}s=a[11736+o|0]|0;do{if((o|0)==22|(o|0)==23){a[f]=80}else if((o|0)==25|(o|0)==24){r=c[h>>2]|0;do{if((r|0)!=(g|0)){if((a[r-1|0]&95|0)==(a[f]&127|0)){break}else{p=-1}return p|0}}while(0);c[h>>2]=r+1;a[r]=s;p=0;return p|0}else{b=a[f]|0;if((s&95|0)!=(b<<24>>24|0)){break}a[f]=b|-128;if((a[e]&1)==0){break}a[e]=0;b=d[k]|0;if((b&1|0)==0){t=b>>>1}else{t=c[k+4>>2]|0}if((t|0)==0){break}b=c[m>>2]|0;if((b-l|0)>=160){break}q=c[n>>2]|0;c[m>>2]=b+4;c[b>>2]=q}}while(0);m=c[h>>2]|0;c[h>>2]=m+1;a[m]=s;if((j|0)>84){p=0;return p|0}c[n>>2]=(c[n>>2]|0)+1;p=0;return p|0}function ci(a){a=a|0;Ve(a|0);Tn(a);return}function di(a){a=a|0;Ve(a|0);return}function ei(b,d,e,f,g,h){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0;j=i;i=i+48|0;k=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[k>>2];k=j|0;l=j+16|0;m=j+24|0;n=j+32|0;if((c[f+4>>2]&1|0)==0){o=c[(c[d>>2]|0)+24>>2]|0;c[l>>2]=c[e>>2];kd[o&31](b,d,l,f,g,h&1);i=j;return}Pf(m,f);f=m|0;m=c[f>>2]|0;if((c[8880]|0)!=-1){c[k>>2]=35520;c[k+4>>2]=14;c[k+8>>2]=0;qf(35520,k,96)}k=(c[8881]|0)-1|0;g=c[m+8>>2]|0;do{if((c[m+12>>2]|0)-g>>2>>>0>k>>>0){l=c[g+(k<<2)>>2]|0;if((l|0)==0){break}d=l;Xe(c[f>>2]|0)|0;o=c[l>>2]|0;if(h){$c[c[o+24>>2]&127](n,d)}else{$c[c[o+28>>2]&127](n,d)}d=n;o=n;l=a[o]|0;if((l&1)==0){p=d+1|0;q=p;r=p;s=n+8|0}else{p=n+8|0;q=c[p>>2]|0;r=d+1|0;s=p}p=e|0;d=n+4|0;t=q;u=l;while(1){if((u&1)==0){v=r}else{v=c[s>>2]|0}l=u&255;if((t|0)==(v+((l&1|0)==0?l>>>1:c[d>>2]|0)|0)){break}l=a[t]|0;w=c[p>>2]|0;do{if((w|0)!=0){x=w+24|0;y=c[x>>2]|0;if((y|0)!=(c[w+28>>2]|0)){c[x>>2]=y+1;a[y]=l;break}if((ad[c[(c[w>>2]|0)+52>>2]&31](w,l&255)|0)!=-1){break}c[p>>2]=0}}while(0);t=t+1|0;u=a[o]|0}c[b>>2]=c[p>>2];vf(n);i=j;return}}while(0);j=Nc(4)|0;qn(j);Qb(j|0,10192,134)}function fi(b,d,e,f,g,h){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0;d=i;i=i+80|0;j=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[j>>2];j=d|0;k=d+8|0;l=d+24|0;m=d+48|0;n=d+56|0;o=d+64|0;p=d+72|0;q=j|0;a[q]=a[4432]|0;a[q+1|0]=a[4433]|0;a[q+2|0]=a[4434]|0;a[q+3|0]=a[4435]|0;a[q+4|0]=a[4436]|0;a[q+5|0]=a[4437]|0;r=j+1|0;s=f+4|0;t=c[s>>2]|0;if((t&2048|0)==0){u=r}else{a[r]=43;u=j+2|0}if((t&512|0)==0){v=u}else{a[u]=35;v=u+1|0}a[v]=108;u=v+1|0;v=t&74;do{if((v|0)==8){if((t&16384|0)==0){a[u]=120;break}else{a[u]=88;break}}else if((v|0)==64){a[u]=111}else{a[u]=100}}while(0);u=k|0;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);v=gi(u,12,c[8322]|0,q,(q=i,i=i+8|0,c[q>>2]=h,q)|0)|0;i=q;q=k+v|0;h=c[s>>2]&176;do{if((h|0)==32){w=q}else if((h|0)==16){s=a[u]|0;if((s<<24>>24|0)==45|(s<<24>>24|0)==43){w=k+1|0;break}if(!((v|0)>1&s<<24>>24==48)){x=22;break}s=a[k+1|0]|0;if(!((s<<24>>24|0)==120|(s<<24>>24|0)==88)){x=22;break}w=k+2|0}else{x=22}}while(0);if((x|0)==22){w=u}x=l|0;Pf(o,f);hi(u,w,q,x,m,n,o);Xe(c[o>>2]|0)|0;c[p>>2]=c[e>>2];ii(b,p,x,c[m>>2]|0,c[n>>2]|0,f,g);i=d;return}function gi(a,b,d,e,f){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,j=0;g=i;i=i+16|0;h=g|0;j=h;c[j>>2]=f;c[j+4>>2]=0;j=uc(d|0)|0;d=wc(a|0,b|0,e|0,h|0)|0;if((j|0)==0){i=g;return d|0}uc(j|0)|0;i=g;return d|0}function hi(b,e,f,g,h,j,k){b=b|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;var l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0;l=i;i=i+48|0;m=l|0;n=l+16|0;o=l+32|0;p=k|0;k=c[p>>2]|0;if((c[8976]|0)!=-1){c[n>>2]=35904;c[n+4>>2]=14;c[n+8>>2]=0;qf(35904,n,96)}n=(c[8977]|0)-1|0;q=c[k+8>>2]|0;if((c[k+12>>2]|0)-q>>2>>>0<=n>>>0){r=Nc(4)|0;s=r;qn(s);Qb(r|0,10192,134)}k=c[q+(n<<2)>>2]|0;if((k|0)==0){r=Nc(4)|0;s=r;qn(s);Qb(r|0,10192,134)}r=k;s=c[p>>2]|0;if((c[8880]|0)!=-1){c[m>>2]=35520;c[m+4>>2]=14;c[m+8>>2]=0;qf(35520,m,96)}m=(c[8881]|0)-1|0;p=c[s+8>>2]|0;if((c[s+12>>2]|0)-p>>2>>>0<=m>>>0){t=Nc(4)|0;u=t;qn(u);Qb(t|0,10192,134)}s=c[p+(m<<2)>>2]|0;if((s|0)==0){t=Nc(4)|0;u=t;qn(u);Qb(t|0,10192,134)}t=s;$c[c[(c[s>>2]|0)+20>>2]&127](o,t);u=o;m=o;p=d[m]|0;if((p&1|0)==0){v=p>>>1}else{v=c[o+4>>2]|0}do{if((v|0)==0){ld[c[(c[k>>2]|0)+32>>2]&15](r,b,f,g)|0;c[j>>2]=g+(f-b)}else{c[j>>2]=g;p=a[b]|0;if((p<<24>>24|0)==45|(p<<24>>24|0)==43){n=ad[c[(c[k>>2]|0)+28>>2]&31](r,p)|0;p=c[j>>2]|0;c[j>>2]=p+1;a[p]=n;w=b+1|0}else{w=b}do{if((f-w|0)>1){if((a[w]|0)!=48){x=w;break}n=w+1|0;p=a[n]|0;if(!((p<<24>>24|0)==120|(p<<24>>24|0)==88)){x=w;break}p=k;q=ad[c[(c[p>>2]|0)+28>>2]&31](r,48)|0;y=c[j>>2]|0;c[j>>2]=y+1;a[y]=q;q=ad[c[(c[p>>2]|0)+28>>2]&31](r,a[n]|0)|0;n=c[j>>2]|0;c[j>>2]=n+1;a[n]=q;x=w+2|0}else{x=w}}while(0);do{if((x|0)!=(f|0)){q=f-1|0;if(x>>>0<q>>>0){z=x;A=q}else{break}do{q=a[z]|0;a[z]=a[A]|0;a[A]=q;z=z+1|0;A=A-1|0;}while(z>>>0<A>>>0)}}while(0);q=dd[c[(c[s>>2]|0)+16>>2]&127](t)|0;if(x>>>0<f>>>0){n=u+1|0;p=k;y=o+4|0;B=o+8|0;C=0;D=0;E=x;while(1){F=(a[m]&1)==0;do{if((a[(F?n:c[B>>2]|0)+D|0]|0)==0){G=D;H=C}else{if((C|0)!=(a[(F?n:c[B>>2]|0)+D|0]|0)){G=D;H=C;break}I=c[j>>2]|0;c[j>>2]=I+1;a[I]=q;I=d[m]|0;G=(D>>>0<(((I&1|0)==0?I>>>1:c[y>>2]|0)-1|0)>>>0)+D|0;H=0}}while(0);F=ad[c[(c[p>>2]|0)+28>>2]&31](r,a[E]|0)|0;I=c[j>>2]|0;c[j>>2]=I+1;a[I]=F;F=E+1|0;if(F>>>0<f>>>0){C=H+1|0;D=G;E=F}else{break}}}E=g+(x-b)|0;D=c[j>>2]|0;if((E|0)==(D|0)){break}C=D-1|0;if(E>>>0<C>>>0){J=E;K=C}else{break}do{C=a[J]|0;a[J]=a[K]|0;a[K]=C;J=J+1|0;K=K-1|0;}while(J>>>0<K>>>0)}}while(0);if((e|0)==(f|0)){L=c[j>>2]|0;c[h>>2]=L;vf(o);i=l;return}else{L=g+(e-b)|0;c[h>>2]=L;vf(o);i=l;return}}function ii(b,d,e,f,g,h,j){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;var k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0;k=i;i=i+16|0;l=d;d=i;i=i+4|0;i=i+7&-8;c[d>>2]=c[l>>2];l=k|0;m=d|0;d=c[m>>2]|0;if((d|0)==0){c[b>>2]=0;i=k;return}n=g;g=e;o=n-g|0;p=h+12|0;h=c[p>>2]|0;q=(h|0)>(o|0)?h-o|0:0;o=f;h=o-g|0;do{if((h|0)>0){if((ed[c[(c[d>>2]|0)+48>>2]&63](d,e,h)|0)==(h|0)){break}c[m>>2]=0;c[b>>2]=0;i=k;return}}while(0);do{if((q|0)>0){uf(l,q,j);if((a[l]&1)==0){r=l+1|0}else{r=c[l+8>>2]|0}if((ed[c[(c[d>>2]|0)+48>>2]&63](d,r,q)|0)==(q|0)){vf(l);break}c[m>>2]=0;c[b>>2]=0;vf(l);i=k;return}}while(0);l=n-o|0;do{if((l|0)>0){if((ed[c[(c[d>>2]|0)+48>>2]&63](d,f,l)|0)==(l|0)){break}c[m>>2]=0;c[b>>2]=0;i=k;return}}while(0);c[p>>2]=0;c[b>>2]=d;i=k;return}function ji(b,d,e,f,g,h,j){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;var k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0;d=i;i=i+112|0;k=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[k>>2];k=d|0;l=d+8|0;m=d+32|0;n=d+80|0;o=d+88|0;p=d+96|0;q=d+104|0;c[k>>2]=37;c[k+4>>2]=0;r=k;k=r+1|0;s=f+4|0;t=c[s>>2]|0;if((t&2048|0)==0){u=k}else{a[k]=43;u=r+2|0}if((t&512|0)==0){v=u}else{a[u]=35;v=u+1|0}a[v]=108;a[v+1|0]=108;u=v+2|0;v=t&74;do{if((v|0)==64){a[u]=111}else if((v|0)==8){if((t&16384|0)==0){a[u]=120;break}else{a[u]=88;break}}else{a[u]=100}}while(0);u=l|0;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);t=gi(u,22,c[8322]|0,r,(r=i,i=i+16|0,c[r>>2]=h,c[r+8>>2]=j,r)|0)|0;i=r;r=l+t|0;j=c[s>>2]&176;do{if((j|0)==16){s=a[u]|0;if((s<<24>>24|0)==45|(s<<24>>24|0)==43){w=l+1|0;break}if(!((t|0)>1&s<<24>>24==48)){x=22;break}s=a[l+1|0]|0;if(!((s<<24>>24|0)==120|(s<<24>>24|0)==88)){x=22;break}w=l+2|0}else if((j|0)==32){w=r}else{x=22}}while(0);if((x|0)==22){w=u}x=m|0;Pf(p,f);hi(u,w,r,x,n,o,p);Xe(c[p>>2]|0)|0;c[q>>2]=c[e>>2];ii(b,q,x,c[n>>2]|0,c[o>>2]|0,f,g);i=d;return}function ki(b,d,e,f,g,h){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0;d=i;i=i+80|0;j=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[j>>2];j=d|0;k=d+8|0;l=d+24|0;m=d+48|0;n=d+56|0;o=d+64|0;p=d+72|0;q=j|0;a[q]=a[4432]|0;a[q+1|0]=a[4433]|0;a[q+2|0]=a[4434]|0;a[q+3|0]=a[4435]|0;a[q+4|0]=a[4436]|0;a[q+5|0]=a[4437]|0;r=j+1|0;s=f+4|0;t=c[s>>2]|0;if((t&2048|0)==0){u=r}else{a[r]=43;u=j+2|0}if((t&512|0)==0){v=u}else{a[u]=35;v=u+1|0}a[v]=108;u=v+1|0;v=t&74;do{if((v|0)==8){if((t&16384|0)==0){a[u]=120;break}else{a[u]=88;break}}else if((v|0)==64){a[u]=111}else{a[u]=117}}while(0);u=k|0;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);v=gi(u,12,c[8322]|0,q,(q=i,i=i+8|0,c[q>>2]=h,q)|0)|0;i=q;q=k+v|0;h=c[s>>2]&176;do{if((h|0)==16){s=a[u]|0;if((s<<24>>24|0)==45|(s<<24>>24|0)==43){w=k+1|0;break}if(!((v|0)>1&s<<24>>24==48)){x=22;break}s=a[k+1|0]|0;if(!((s<<24>>24|0)==120|(s<<24>>24|0)==88)){x=22;break}w=k+2|0}else if((h|0)==32){w=q}else{x=22}}while(0);if((x|0)==22){w=u}x=l|0;Pf(o,f);hi(u,w,q,x,m,n,o);Xe(c[o>>2]|0)|0;c[p>>2]=c[e>>2];ii(b,p,x,c[m>>2]|0,c[n>>2]|0,f,g);i=d;return}function li(b,d,e,f,g,h,j){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;var k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0;d=i;i=i+112|0;k=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[k>>2];k=d|0;l=d+8|0;m=d+32|0;n=d+80|0;o=d+88|0;p=d+96|0;q=d+104|0;c[k>>2]=37;c[k+4>>2]=0;r=k;k=r+1|0;s=f+4|0;t=c[s>>2]|0;if((t&2048|0)==0){u=k}else{a[k]=43;u=r+2|0}if((t&512|0)==0){v=u}else{a[u]=35;v=u+1|0}a[v]=108;a[v+1|0]=108;u=v+2|0;v=t&74;do{if((v|0)==64){a[u]=111}else if((v|0)==8){if((t&16384|0)==0){a[u]=120;break}else{a[u]=88;break}}else{a[u]=117}}while(0);u=l|0;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);t=gi(u,23,c[8322]|0,r,(r=i,i=i+16|0,c[r>>2]=h,c[r+8>>2]=j,r)|0)|0;i=r;r=l+t|0;j=c[s>>2]&176;do{if((j|0)==16){s=a[u]|0;if((s<<24>>24|0)==45|(s<<24>>24|0)==43){w=l+1|0;break}if(!((t|0)>1&s<<24>>24==48)){x=22;break}s=a[l+1|0]|0;if(!((s<<24>>24|0)==120|(s<<24>>24|0)==88)){x=22;break}w=l+2|0}else if((j|0)==32){w=r}else{x=22}}while(0);if((x|0)==22){w=u}x=m|0;Pf(p,f);hi(u,w,r,x,n,o,p);Xe(c[p>>2]|0)|0;c[q>>2]=c[e>>2];ii(b,q,x,c[n>>2]|0,c[o>>2]|0,f,g);i=d;return}function mi(b,d,e,f,g,j){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;j=+j;var k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0;d=i;i=i+152|0;k=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[k>>2];k=d|0;l=d+8|0;m=d+40|0;n=d+48|0;o=d+112|0;p=d+120|0;q=d+128|0;r=d+136|0;s=d+144|0;c[k>>2]=37;c[k+4>>2]=0;t=k;k=t+1|0;u=f+4|0;v=c[u>>2]|0;if((v&2048|0)==0){w=k}else{a[k]=43;w=t+2|0}if((v&1024|0)==0){x=w}else{a[w]=35;x=w+1|0}w=v&260;k=v>>>14;do{if((w|0)==260){if((k&1|0)==0){a[x]=97;y=0;break}else{a[x]=65;y=0;break}}else{a[x]=46;v=x+2|0;a[x+1|0]=42;if((w|0)==256){if((k&1|0)==0){a[v]=101;y=1;break}else{a[v]=69;y=1;break}}else if((w|0)==4){if((k&1|0)==0){a[v]=102;y=1;break}else{a[v]=70;y=1;break}}else{if((k&1|0)==0){a[v]=103;y=1;break}else{a[v]=71;y=1;break}}}}while(0);k=l|0;c[m>>2]=k;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);l=c[8322]|0;if(y){w=gi(k,30,l,t,(z=i,i=i+16|0,c[z>>2]=c[f+8>>2],h[z+8>>3]=j,z)|0)|0;i=z;A=w}else{w=gi(k,30,l,t,(z=i,i=i+8|0,h[z>>3]=j,z)|0)|0;i=z;A=w}do{if((A|0)>29){w=(a[36464]|0)==0;if(y){do{if(w){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);l=ni(m,c[8322]|0,t,(z=i,i=i+16|0,c[z>>2]=c[f+8>>2],h[z+8>>3]=j,z)|0)|0;i=z;B=l}else{do{if(w){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);w=ni(m,c[8322]|0,t,(z=i,i=i+16|0,c[z>>2]=c[f+8>>2],h[z+8>>3]=j,z)|0)|0;i=z;B=w}w=c[m>>2]|0;if((w|0)!=0){C=B;D=w;E=w;break}Yn();w=c[m>>2]|0;C=B;D=w;E=w}else{C=A;D=0;E=c[m>>2]|0}}while(0);A=E+C|0;B=c[u>>2]&176;do{if((B|0)==16){u=a[E]|0;if((u<<24>>24|0)==45|(u<<24>>24|0)==43){F=E+1|0;break}if(!((C|0)>1&u<<24>>24==48)){G=53;break}u=a[E+1|0]|0;if(!((u<<24>>24|0)==120|(u<<24>>24|0)==88)){G=53;break}F=E+2|0}else if((B|0)==32){F=A}else{G=53}}while(0);if((G|0)==53){F=E}do{if((E|0)==(k|0)){H=n|0;I=0;J=k}else{G=Ln(C<<1)|0;if((G|0)!=0){H=G;I=G;J=E;break}Yn();H=0;I=0;J=c[m>>2]|0}}while(0);Pf(q,f);oi(J,F,A,H,o,p,q);Xe(c[q>>2]|0)|0;q=e|0;c[s>>2]=c[q>>2];ii(r,s,H,c[o>>2]|0,c[p>>2]|0,f,g);g=c[r>>2]|0;c[q>>2]=g;c[b>>2]=g;if((I|0)!=0){Mn(I)}if((D|0)==0){i=d;return}Mn(D);i=d;return}function ni(a,b,d,e){a=a|0;b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0;f=i;i=i+16|0;g=f|0;h=g;c[h>>2]=e;c[h+4>>2]=0;h=uc(b|0)|0;b=Pc(a|0,d|0,g|0)|0;if((h|0)==0){i=f;return b|0}uc(h|0)|0;i=f;return b|0}function oi(b,e,f,g,h,j,k){b=b|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;var l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0;l=i;i=i+48|0;m=l|0;n=l+16|0;o=l+32|0;p=k|0;k=c[p>>2]|0;if((c[8976]|0)!=-1){c[n>>2]=35904;c[n+4>>2]=14;c[n+8>>2]=0;qf(35904,n,96)}n=(c[8977]|0)-1|0;q=c[k+8>>2]|0;if((c[k+12>>2]|0)-q>>2>>>0<=n>>>0){r=Nc(4)|0;s=r;qn(s);Qb(r|0,10192,134)}k=c[q+(n<<2)>>2]|0;if((k|0)==0){r=Nc(4)|0;s=r;qn(s);Qb(r|0,10192,134)}r=k;s=c[p>>2]|0;if((c[8880]|0)!=-1){c[m>>2]=35520;c[m+4>>2]=14;c[m+8>>2]=0;qf(35520,m,96)}m=(c[8881]|0)-1|0;p=c[s+8>>2]|0;if((c[s+12>>2]|0)-p>>2>>>0<=m>>>0){t=Nc(4)|0;u=t;qn(u);Qb(t|0,10192,134)}s=c[p+(m<<2)>>2]|0;if((s|0)==0){t=Nc(4)|0;u=t;qn(u);Qb(t|0,10192,134)}t=s;$c[c[(c[s>>2]|0)+20>>2]&127](o,t);c[j>>2]=g;u=a[b]|0;if((u<<24>>24|0)==45|(u<<24>>24|0)==43){m=ad[c[(c[k>>2]|0)+28>>2]&31](r,u)|0;u=c[j>>2]|0;c[j>>2]=u+1;a[u]=m;v=b+1|0}else{v=b}m=f;L23:do{if((m-v|0)>1){if((a[v]|0)!=48){w=21;break}u=v+1|0;p=a[u]|0;if(!((p<<24>>24|0)==120|(p<<24>>24|0)==88)){w=21;break}p=k;n=ad[c[(c[p>>2]|0)+28>>2]&31](r,48)|0;q=c[j>>2]|0;c[j>>2]=q+1;a[q]=n;n=v+2|0;q=ad[c[(c[p>>2]|0)+28>>2]&31](r,a[u]|0)|0;u=c[j>>2]|0;c[j>>2]=u+1;a[u]=q;if(n>>>0<f>>>0){x=n}else{y=n;z=n;break}while(1){q=a[x]|0;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);u=x+1|0;if((lb(q<<24>>24|0,c[8322]|0)|0)==0){y=x;z=n;break L23}if(u>>>0<f>>>0){x=u}else{y=u;z=n;break}}}else{w=21}}while(0);L38:do{if((w|0)==21){if(v>>>0<f>>>0){A=v}else{y=v;z=v;break}while(1){x=a[A]|0;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);q=A+1|0;if((Dc(x<<24>>24|0,c[8322]|0)|0)==0){y=A;z=v;break L38}if(q>>>0<f>>>0){A=q}else{y=q;z=v;break}}}}while(0);v=o;A=o;w=d[A]|0;if((w&1|0)==0){B=w>>>1}else{B=c[o+4>>2]|0}do{if((B|0)==0){ld[c[(c[k>>2]|0)+32>>2]&15](r,z,y,c[j>>2]|0)|0;c[j>>2]=(c[j>>2]|0)+(y-z)}else{do{if((z|0)!=(y|0)){w=y-1|0;if(z>>>0<w>>>0){C=z;D=w}else{break}do{w=a[C]|0;a[C]=a[D]|0;a[D]=w;C=C+1|0;D=D-1|0;}while(C>>>0<D>>>0)}}while(0);x=dd[c[(c[s>>2]|0)+16>>2]&127](t)|0;if(z>>>0<y>>>0){w=v+1|0;q=o+4|0;n=o+8|0;u=k;p=0;E=0;F=z;while(1){G=(a[A]&1)==0;do{if((a[(G?w:c[n>>2]|0)+E|0]|0)>0){if((p|0)!=(a[(G?w:c[n>>2]|0)+E|0]|0)){H=E;I=p;break}J=c[j>>2]|0;c[j>>2]=J+1;a[J]=x;J=d[A]|0;H=(E>>>0<(((J&1|0)==0?J>>>1:c[q>>2]|0)-1|0)>>>0)+E|0;I=0}else{H=E;I=p}}while(0);G=ad[c[(c[u>>2]|0)+28>>2]&31](r,a[F]|0)|0;J=c[j>>2]|0;c[j>>2]=J+1;a[J]=G;G=F+1|0;if(G>>>0<y>>>0){p=I+1|0;E=H;F=G}else{break}}}F=g+(z-b)|0;E=c[j>>2]|0;if((F|0)==(E|0)){break}p=E-1|0;if(F>>>0<p>>>0){K=F;L=p}else{break}do{p=a[K]|0;a[K]=a[L]|0;a[L]=p;K=K+1|0;L=L-1|0;}while(K>>>0<L>>>0)}}while(0);L78:do{if(y>>>0<f>>>0){L=k;K=y;while(1){z=a[K]|0;if(z<<24>>24==46){break}H=ad[c[(c[L>>2]|0)+28>>2]&31](r,z)|0;z=c[j>>2]|0;c[j>>2]=z+1;a[z]=H;H=K+1|0;if(H>>>0<f>>>0){K=H}else{M=H;break L78}}L=dd[c[(c[s>>2]|0)+12>>2]&127](t)|0;H=c[j>>2]|0;c[j>>2]=H+1;a[H]=L;M=K+1|0}else{M=y}}while(0);ld[c[(c[k>>2]|0)+32>>2]&15](r,M,f,c[j>>2]|0)|0;r=(c[j>>2]|0)+(m-M)|0;c[j>>2]=r;if((e|0)==(f|0)){N=r;c[h>>2]=N;vf(o);i=l;return}N=g+(e-b)|0;c[h>>2]=N;vf(o);i=l;return}function pi(b,d,e,f,g,j){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;j=+j;var k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0;d=i;i=i+152|0;k=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[k>>2];k=d|0;l=d+8|0;m=d+40|0;n=d+48|0;o=d+112|0;p=d+120|0;q=d+128|0;r=d+136|0;s=d+144|0;c[k>>2]=37;c[k+4>>2]=0;t=k;k=t+1|0;u=f+4|0;v=c[u>>2]|0;if((v&2048|0)==0){w=k}else{a[k]=43;w=t+2|0}if((v&1024|0)==0){x=w}else{a[w]=35;x=w+1|0}w=v&260;k=v>>>14;do{if((w|0)==260){a[x]=76;v=x+1|0;if((k&1|0)==0){a[v]=97;y=0;break}else{a[v]=65;y=0;break}}else{a[x]=46;a[x+1|0]=42;a[x+2|0]=76;v=x+3|0;if((w|0)==4){if((k&1|0)==0){a[v]=102;y=1;break}else{a[v]=70;y=1;break}}else if((w|0)==256){if((k&1|0)==0){a[v]=101;y=1;break}else{a[v]=69;y=1;break}}else{if((k&1|0)==0){a[v]=103;y=1;break}else{a[v]=71;y=1;break}}}}while(0);k=l|0;c[m>>2]=k;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);l=c[8322]|0;if(y){w=gi(k,30,l,t,(z=i,i=i+16|0,c[z>>2]=c[f+8>>2],h[z+8>>3]=j,z)|0)|0;i=z;A=w}else{w=gi(k,30,l,t,(z=i,i=i+8|0,h[z>>3]=j,z)|0)|0;i=z;A=w}do{if((A|0)>29){w=(a[36464]|0)==0;if(y){do{if(w){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);l=ni(m,c[8322]|0,t,(z=i,i=i+16|0,c[z>>2]=c[f+8>>2],h[z+8>>3]=j,z)|0)|0;i=z;B=l}else{do{if(w){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);w=ni(m,c[8322]|0,t,(z=i,i=i+8|0,h[z>>3]=j,z)|0)|0;i=z;B=w}w=c[m>>2]|0;if((w|0)!=0){C=B;D=w;E=w;break}Yn();w=c[m>>2]|0;C=B;D=w;E=w}else{C=A;D=0;E=c[m>>2]|0}}while(0);A=E+C|0;B=c[u>>2]&176;do{if((B|0)==32){F=A}else if((B|0)==16){u=a[E]|0;if((u<<24>>24|0)==45|(u<<24>>24|0)==43){F=E+1|0;break}if(!((C|0)>1&u<<24>>24==48)){G=53;break}u=a[E+1|0]|0;if(!((u<<24>>24|0)==120|(u<<24>>24|0)==88)){G=53;break}F=E+2|0}else{G=53}}while(0);if((G|0)==53){F=E}do{if((E|0)==(k|0)){H=n|0;I=0;J=k}else{G=Ln(C<<1)|0;if((G|0)!=0){H=G;I=G;J=E;break}Yn();H=0;I=0;J=c[m>>2]|0}}while(0);Pf(q,f);oi(J,F,A,H,o,p,q);Xe(c[q>>2]|0)|0;q=e|0;c[s>>2]=c[q>>2];ii(r,s,H,c[o>>2]|0,c[p>>2]|0,f,g);g=c[r>>2]|0;c[q>>2]=g;c[b>>2]=g;if((I|0)!=0){Mn(I)}if((D|0)==0){i=d;return}Mn(D);i=d;return}function qi(b,d,e,f,g,h){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0;d=i;i=i+104|0;j=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[j>>2];j=d|0;k=d+24|0;l=d+48|0;m=d+88|0;n=d+96|0;o=d+16|0;a[o]=a[4440]|0;a[o+1|0]=a[4441]|0;a[o+2|0]=a[4442]|0;a[o+3|0]=a[4443]|0;a[o+4|0]=a[4444]|0;a[o+5|0]=a[4445]|0;p=k|0;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);q=gi(p,20,c[8322]|0,o,(o=i,i=i+8|0,c[o>>2]=h,o)|0)|0;i=o;o=k+q|0;h=c[f+4>>2]&176;do{if((h|0)==32){r=o}else if((h|0)==16){s=a[p]|0;if((s<<24>>24|0)==45|(s<<24>>24|0)==43){r=k+1|0;break}if(!((q|0)>1&s<<24>>24==48)){t=12;break}s=a[k+1|0]|0;if(!((s<<24>>24|0)==120|(s<<24>>24|0)==88)){t=12;break}r=k+2|0}else{t=12}}while(0);if((t|0)==12){r=p}Pf(m,f);t=m|0;m=c[t>>2]|0;if((c[8976]|0)!=-1){c[j>>2]=35904;c[j+4>>2]=14;c[j+8>>2]=0;qf(35904,j,96)}j=(c[8977]|0)-1|0;h=c[m+8>>2]|0;do{if((c[m+12>>2]|0)-h>>2>>>0>j>>>0){s=c[h+(j<<2)>>2]|0;if((s|0)==0){break}Xe(c[t>>2]|0)|0;u=l|0;ld[c[(c[s>>2]|0)+32>>2]&15](s,p,o,u)|0;s=l+q|0;if((r|0)==(o|0)){v=s;w=e|0;x=c[w>>2]|0;y=n|0;c[y>>2]=x;ii(b,n,u,v,s,f,g);i=d;return}v=l+(r-k)|0;w=e|0;x=c[w>>2]|0;y=n|0;c[y>>2]=x;ii(b,n,u,v,s,f,g);i=d;return}}while(0);d=Nc(4)|0;qn(d);Qb(d|0,10192,134)}function ri(a){a=a|0;Ve(a|0);Tn(a);return}function si(a){a=a|0;Ve(a|0);return}function ti(b,d,e,f,g,h){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0;j=i;i=i+48|0;k=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[k>>2];k=j|0;l=j+16|0;m=j+24|0;n=j+32|0;if((c[f+4>>2]&1|0)==0){o=c[(c[d>>2]|0)+24>>2]|0;c[l>>2]=c[e>>2];kd[o&31](b,d,l,f,g,h&1);i=j;return}Pf(m,f);f=m|0;m=c[f>>2]|0;if((c[8878]|0)!=-1){c[k>>2]=35512;c[k+4>>2]=14;c[k+8>>2]=0;qf(35512,k,96)}k=(c[8879]|0)-1|0;g=c[m+8>>2]|0;do{if((c[m+12>>2]|0)-g>>2>>>0>k>>>0){l=c[g+(k<<2)>>2]|0;if((l|0)==0){break}d=l;Xe(c[f>>2]|0)|0;o=c[l>>2]|0;if(h){$c[c[o+24>>2]&127](n,d)}else{$c[c[o+28>>2]&127](n,d)}d=n;o=a[d]|0;if((o&1)==0){l=n+4|0;p=l;q=l;r=n+8|0}else{l=n+8|0;p=c[l>>2]|0;q=n+4|0;r=l}l=e|0;s=p;t=o;while(1){if((t&1)==0){u=q}else{u=c[r>>2]|0}o=t&255;if((o&1|0)==0){v=o>>>1}else{v=c[q>>2]|0}if((s|0)==(u+(v<<2)|0)){break}o=c[s>>2]|0;w=c[l>>2]|0;do{if((w|0)!=0){x=w+24|0;y=c[x>>2]|0;if((y|0)==(c[w+28>>2]|0)){z=ad[c[(c[w>>2]|0)+52>>2]&31](w,o)|0}else{c[x>>2]=y+4;c[y>>2]=o;z=o}if((z|0)!=-1){break}c[l>>2]=0}}while(0);s=s+4|0;t=a[d]|0}c[b>>2]=c[l>>2];Gf(n);i=j;return}}while(0);j=Nc(4)|0;qn(j);Qb(j|0,10192,134)}function ui(b,d,e,f,g,h){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0;d=i;i=i+144|0;j=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[j>>2];j=d|0;k=d+8|0;l=d+24|0;m=d+112|0;n=d+120|0;o=d+128|0;p=d+136|0;q=j|0;a[q]=a[4432]|0;a[q+1|0]=a[4433]|0;a[q+2|0]=a[4434]|0;a[q+3|0]=a[4435]|0;a[q+4|0]=a[4436]|0;a[q+5|0]=a[4437]|0;r=j+1|0;s=f+4|0;t=c[s>>2]|0;if((t&2048|0)==0){u=r}else{a[r]=43;u=j+2|0}if((t&512|0)==0){v=u}else{a[u]=35;v=u+1|0}a[v]=108;u=v+1|0;v=t&74;do{if((v|0)==8){if((t&16384|0)==0){a[u]=120;break}else{a[u]=88;break}}else if((v|0)==64){a[u]=111}else{a[u]=100}}while(0);u=k|0;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);v=gi(u,12,c[8322]|0,q,(q=i,i=i+8|0,c[q>>2]=h,q)|0)|0;i=q;q=k+v|0;h=c[s>>2]&176;do{if((h|0)==16){s=a[u]|0;if((s<<24>>24|0)==45|(s<<24>>24|0)==43){w=k+1|0;break}if(!((v|0)>1&s<<24>>24==48)){x=22;break}s=a[k+1|0]|0;if(!((s<<24>>24|0)==120|(s<<24>>24|0)==88)){x=22;break}w=k+2|0}else if((h|0)==32){w=q}else{x=22}}while(0);if((x|0)==22){w=u}x=l|0;Pf(o,f);vi(u,w,q,x,m,n,o);Xe(c[o>>2]|0)|0;c[p>>2]=c[e>>2];wi(b,p,x,c[m>>2]|0,c[n>>2]|0,f,g);i=d;return}function vi(b,e,f,g,h,j,k){b=b|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;var l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0;l=i;i=i+48|0;m=l|0;n=l+16|0;o=l+32|0;p=k|0;k=c[p>>2]|0;if((c[8974]|0)!=-1){c[n>>2]=35896;c[n+4>>2]=14;c[n+8>>2]=0;qf(35896,n,96)}n=(c[8975]|0)-1|0;q=c[k+8>>2]|0;if((c[k+12>>2]|0)-q>>2>>>0<=n>>>0){r=Nc(4)|0;s=r;qn(s);Qb(r|0,10192,134)}k=c[q+(n<<2)>>2]|0;if((k|0)==0){r=Nc(4)|0;s=r;qn(s);Qb(r|0,10192,134)}r=k;s=c[p>>2]|0;if((c[8878]|0)!=-1){c[m>>2]=35512;c[m+4>>2]=14;c[m+8>>2]=0;qf(35512,m,96)}m=(c[8879]|0)-1|0;p=c[s+8>>2]|0;if((c[s+12>>2]|0)-p>>2>>>0<=m>>>0){t=Nc(4)|0;u=t;qn(u);Qb(t|0,10192,134)}s=c[p+(m<<2)>>2]|0;if((s|0)==0){t=Nc(4)|0;u=t;qn(u);Qb(t|0,10192,134)}t=s;$c[c[(c[s>>2]|0)+20>>2]&127](o,t);u=o;m=o;p=d[m]|0;if((p&1|0)==0){v=p>>>1}else{v=c[o+4>>2]|0}do{if((v|0)==0){ld[c[(c[k>>2]|0)+48>>2]&15](r,b,f,g)|0;c[j>>2]=g+(f-b<<2)}else{c[j>>2]=g;p=a[b]|0;if((p<<24>>24|0)==45|(p<<24>>24|0)==43){n=ad[c[(c[k>>2]|0)+44>>2]&31](r,p)|0;p=c[j>>2]|0;c[j>>2]=p+4;c[p>>2]=n;w=b+1|0}else{w=b}do{if((f-w|0)>1){if((a[w]|0)!=48){x=w;break}n=w+1|0;p=a[n]|0;if(!((p<<24>>24|0)==120|(p<<24>>24|0)==88)){x=w;break}p=k;q=ad[c[(c[p>>2]|0)+44>>2]&31](r,48)|0;y=c[j>>2]|0;c[j>>2]=y+4;c[y>>2]=q;q=ad[c[(c[p>>2]|0)+44>>2]&31](r,a[n]|0)|0;n=c[j>>2]|0;c[j>>2]=n+4;c[n>>2]=q;x=w+2|0}else{x=w}}while(0);do{if((x|0)!=(f|0)){q=f-1|0;if(x>>>0<q>>>0){z=x;A=q}else{break}do{q=a[z]|0;a[z]=a[A]|0;a[A]=q;z=z+1|0;A=A-1|0;}while(z>>>0<A>>>0)}}while(0);q=dd[c[(c[s>>2]|0)+16>>2]&127](t)|0;if(x>>>0<f>>>0){n=u+1|0;p=k;y=o+4|0;B=o+8|0;C=0;D=0;E=x;while(1){F=(a[m]&1)==0;do{if((a[(F?n:c[B>>2]|0)+D|0]|0)==0){G=D;H=C}else{if((C|0)!=(a[(F?n:c[B>>2]|0)+D|0]|0)){G=D;H=C;break}I=c[j>>2]|0;c[j>>2]=I+4;c[I>>2]=q;I=d[m]|0;G=(D>>>0<(((I&1|0)==0?I>>>1:c[y>>2]|0)-1|0)>>>0)+D|0;H=0}}while(0);F=ad[c[(c[p>>2]|0)+44>>2]&31](r,a[E]|0)|0;I=c[j>>2]|0;c[j>>2]=I+4;c[I>>2]=F;F=E+1|0;if(F>>>0<f>>>0){C=H+1|0;D=G;E=F}else{break}}}E=g+(x-b<<2)|0;D=c[j>>2]|0;if((E|0)==(D|0)){break}C=D-4|0;if(E>>>0<C>>>0){J=E;K=C}else{break}do{C=c[J>>2]|0;c[J>>2]=c[K>>2];c[K>>2]=C;J=J+4|0;K=K-4|0;}while(J>>>0<K>>>0)}}while(0);if((e|0)==(f|0)){L=c[j>>2]|0;c[h>>2]=L;vf(o);i=l;return}else{L=g+(e-b<<2)|0;c[h>>2]=L;vf(o);i=l;return}}function wi(b,d,e,f,g,h,j){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;var k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0;k=i;i=i+16|0;l=d;d=i;i=i+4|0;i=i+7&-8;c[d>>2]=c[l>>2];l=k|0;m=d|0;d=c[m>>2]|0;if((d|0)==0){c[b>>2]=0;i=k;return}n=g;g=e;o=n-g>>2;p=h+12|0;h=c[p>>2]|0;q=(h|0)>(o|0)?h-o|0:0;o=f;h=o-g|0;g=h>>2;do{if((h|0)>0){if((ed[c[(c[d>>2]|0)+48>>2]&63](d,e,g)|0)==(g|0)){break}c[m>>2]=0;c[b>>2]=0;i=k;return}}while(0);do{if((q|0)>0){Ff(l,q,j);if((a[l]&1)==0){r=l+4|0}else{r=c[l+8>>2]|0}if((ed[c[(c[d>>2]|0)+48>>2]&63](d,r,q)|0)==(q|0)){Gf(l);break}c[m>>2]=0;c[b>>2]=0;Gf(l);i=k;return}}while(0);l=n-o|0;o=l>>2;do{if((l|0)>0){if((ed[c[(c[d>>2]|0)+48>>2]&63](d,f,o)|0)==(o|0)){break}c[m>>2]=0;c[b>>2]=0;i=k;return}}while(0);c[p>>2]=0;c[b>>2]=d;i=k;return}function xi(b,d,e,f,g,h,j){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;var k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0;d=i;i=i+232|0;k=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[k>>2];k=d|0;l=d+8|0;m=d+32|0;n=d+200|0;o=d+208|0;p=d+216|0;q=d+224|0;c[k>>2]=37;c[k+4>>2]=0;r=k;k=r+1|0;s=f+4|0;t=c[s>>2]|0;if((t&2048|0)==0){u=k}else{a[k]=43;u=r+2|0}if((t&512|0)==0){v=u}else{a[u]=35;v=u+1|0}a[v]=108;a[v+1|0]=108;u=v+2|0;v=t&74;do{if((v|0)==64){a[u]=111}else if((v|0)==8){if((t&16384|0)==0){a[u]=120;break}else{a[u]=88;break}}else{a[u]=100}}while(0);u=l|0;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);t=gi(u,22,c[8322]|0,r,(r=i,i=i+16|0,c[r>>2]=h,c[r+8>>2]=j,r)|0)|0;i=r;r=l+t|0;j=c[s>>2]&176;do{if((j|0)==16){s=a[u]|0;if((s<<24>>24|0)==45|(s<<24>>24|0)==43){w=l+1|0;break}if(!((t|0)>1&s<<24>>24==48)){x=22;break}s=a[l+1|0]|0;if(!((s<<24>>24|0)==120|(s<<24>>24|0)==88)){x=22;break}w=l+2|0}else if((j|0)==32){w=r}else{x=22}}while(0);if((x|0)==22){w=u}x=m|0;Pf(p,f);vi(u,w,r,x,n,o,p);Xe(c[p>>2]|0)|0;c[q>>2]=c[e>>2];wi(b,q,x,c[n>>2]|0,c[o>>2]|0,f,g);i=d;return}
function yi(b,d,e,f,g,h){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0;d=i;i=i+144|0;j=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[j>>2];j=d|0;k=d+8|0;l=d+24|0;m=d+112|0;n=d+120|0;o=d+128|0;p=d+136|0;q=j|0;a[q]=a[4432]|0;a[q+1|0]=a[4433]|0;a[q+2|0]=a[4434]|0;a[q+3|0]=a[4435]|0;a[q+4|0]=a[4436]|0;a[q+5|0]=a[4437]|0;r=j+1|0;s=f+4|0;t=c[s>>2]|0;if((t&2048|0)==0){u=r}else{a[r]=43;u=j+2|0}if((t&512|0)==0){v=u}else{a[u]=35;v=u+1|0}a[v]=108;u=v+1|0;v=t&74;do{if((v|0)==64){a[u]=111}else if((v|0)==8){if((t&16384|0)==0){a[u]=120;break}else{a[u]=88;break}}else{a[u]=117}}while(0);u=k|0;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);t=gi(u,12,c[8322]|0,q,(q=i,i=i+8|0,c[q>>2]=h,q)|0)|0;i=q;q=k+t|0;h=c[s>>2]&176;do{if((h|0)==16){s=a[u]|0;if((s<<24>>24|0)==45|(s<<24>>24|0)==43){w=k+1|0;break}if(!((t|0)>1&s<<24>>24==48)){x=22;break}s=a[k+1|0]|0;if(!((s<<24>>24|0)==120|(s<<24>>24|0)==88)){x=22;break}w=k+2|0}else if((h|0)==32){w=q}else{x=22}}while(0);if((x|0)==22){w=u}x=l|0;Pf(o,f);vi(u,w,q,x,m,n,o);Xe(c[o>>2]|0)|0;c[p>>2]=c[e>>2];wi(b,p,x,c[m>>2]|0,c[n>>2]|0,f,g);i=d;return}function zi(b,d,e,f,g,h,j){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;var k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0;d=i;i=i+240|0;k=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[k>>2];k=d|0;l=d+8|0;m=d+32|0;n=d+208|0;o=d+216|0;p=d+224|0;q=d+232|0;c[k>>2]=37;c[k+4>>2]=0;r=k;k=r+1|0;s=f+4|0;t=c[s>>2]|0;if((t&2048|0)==0){u=k}else{a[k]=43;u=r+2|0}if((t&512|0)==0){v=u}else{a[u]=35;v=u+1|0}a[v]=108;a[v+1|0]=108;u=v+2|0;v=t&74;do{if((v|0)==64){a[u]=111}else if((v|0)==8){if((t&16384|0)==0){a[u]=120;break}else{a[u]=88;break}}else{a[u]=117}}while(0);u=l|0;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);t=gi(u,23,c[8322]|0,r,(r=i,i=i+16|0,c[r>>2]=h,c[r+8>>2]=j,r)|0)|0;i=r;r=l+t|0;j=c[s>>2]&176;do{if((j|0)==16){s=a[u]|0;if((s<<24>>24|0)==45|(s<<24>>24|0)==43){w=l+1|0;break}if(!((t|0)>1&s<<24>>24==48)){x=22;break}s=a[l+1|0]|0;if(!((s<<24>>24|0)==120|(s<<24>>24|0)==88)){x=22;break}w=l+2|0}else if((j|0)==32){w=r}else{x=22}}while(0);if((x|0)==22){w=u}x=m|0;Pf(p,f);vi(u,w,r,x,n,o,p);Xe(c[p>>2]|0)|0;c[q>>2]=c[e>>2];wi(b,q,x,c[n>>2]|0,c[o>>2]|0,f,g);i=d;return}function Ai(b,d,e,f,g,j){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;j=+j;var k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0;d=i;i=i+320|0;k=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[k>>2];k=d|0;l=d+8|0;m=d+40|0;n=d+48|0;o=d+280|0;p=d+288|0;q=d+296|0;r=d+304|0;s=d+312|0;c[k>>2]=37;c[k+4>>2]=0;t=k;k=t+1|0;u=f+4|0;v=c[u>>2]|0;if((v&2048|0)==0){w=k}else{a[k]=43;w=t+2|0}if((v&1024|0)==0){x=w}else{a[w]=35;x=w+1|0}w=v&260;k=v>>>14;do{if((w|0)==260){if((k&1|0)==0){a[x]=97;y=0;break}else{a[x]=65;y=0;break}}else{a[x]=46;v=x+2|0;a[x+1|0]=42;if((w|0)==4){if((k&1|0)==0){a[v]=102;y=1;break}else{a[v]=70;y=1;break}}else if((w|0)==256){if((k&1|0)==0){a[v]=101;y=1;break}else{a[v]=69;y=1;break}}else{if((k&1|0)==0){a[v]=103;y=1;break}else{a[v]=71;y=1;break}}}}while(0);k=l|0;c[m>>2]=k;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);l=c[8322]|0;if(y){w=gi(k,30,l,t,(z=i,i=i+16|0,c[z>>2]=c[f+8>>2],h[z+8>>3]=j,z)|0)|0;i=z;A=w}else{w=gi(k,30,l,t,(z=i,i=i+8|0,h[z>>3]=j,z)|0)|0;i=z;A=w}do{if((A|0)>29){w=(a[36464]|0)==0;if(y){do{if(w){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);l=ni(m,c[8322]|0,t,(z=i,i=i+16|0,c[z>>2]=c[f+8>>2],h[z+8>>3]=j,z)|0)|0;i=z;B=l}else{do{if(w){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);w=ni(m,c[8322]|0,t,(z=i,i=i+16|0,c[z>>2]=c[f+8>>2],h[z+8>>3]=j,z)|0)|0;i=z;B=w}w=c[m>>2]|0;if((w|0)!=0){C=B;D=w;E=w;break}Yn();w=c[m>>2]|0;C=B;D=w;E=w}else{C=A;D=0;E=c[m>>2]|0}}while(0);A=E+C|0;B=c[u>>2]&176;do{if((B|0)==16){u=a[E]|0;if((u<<24>>24|0)==45|(u<<24>>24|0)==43){F=E+1|0;break}if(!((C|0)>1&u<<24>>24==48)){G=53;break}u=a[E+1|0]|0;if(!((u<<24>>24|0)==120|(u<<24>>24|0)==88)){G=53;break}F=E+2|0}else if((B|0)==32){F=A}else{G=53}}while(0);if((G|0)==53){F=E}do{if((E|0)==(k|0)){H=n|0;I=0;J=k}else{G=Ln(C<<3)|0;B=G;if((G|0)!=0){H=B;I=B;J=E;break}Yn();H=B;I=B;J=c[m>>2]|0}}while(0);Pf(q,f);Bi(J,F,A,H,o,p,q);Xe(c[q>>2]|0)|0;q=e|0;c[s>>2]=c[q>>2];wi(r,s,H,c[o>>2]|0,c[p>>2]|0,f,g);g=c[r>>2]|0;c[q>>2]=g;c[b>>2]=g;if((I|0)!=0){Mn(I)}if((D|0)==0){i=d;return}Mn(D);i=d;return}function Bi(b,e,f,g,h,j,k){b=b|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;var l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0;l=i;i=i+48|0;m=l|0;n=l+16|0;o=l+32|0;p=k|0;k=c[p>>2]|0;if((c[8974]|0)!=-1){c[n>>2]=35896;c[n+4>>2]=14;c[n+8>>2]=0;qf(35896,n,96)}n=(c[8975]|0)-1|0;q=c[k+8>>2]|0;if((c[k+12>>2]|0)-q>>2>>>0<=n>>>0){r=Nc(4)|0;s=r;qn(s);Qb(r|0,10192,134)}k=c[q+(n<<2)>>2]|0;if((k|0)==0){r=Nc(4)|0;s=r;qn(s);Qb(r|0,10192,134)}r=k;s=c[p>>2]|0;if((c[8878]|0)!=-1){c[m>>2]=35512;c[m+4>>2]=14;c[m+8>>2]=0;qf(35512,m,96)}m=(c[8879]|0)-1|0;p=c[s+8>>2]|0;if((c[s+12>>2]|0)-p>>2>>>0<=m>>>0){t=Nc(4)|0;u=t;qn(u);Qb(t|0,10192,134)}s=c[p+(m<<2)>>2]|0;if((s|0)==0){t=Nc(4)|0;u=t;qn(u);Qb(t|0,10192,134)}t=s;$c[c[(c[s>>2]|0)+20>>2]&127](o,t);c[j>>2]=g;u=a[b]|0;if((u<<24>>24|0)==45|(u<<24>>24|0)==43){m=ad[c[(c[k>>2]|0)+44>>2]&31](r,u)|0;u=c[j>>2]|0;c[j>>2]=u+4;c[u>>2]=m;v=b+1|0}else{v=b}m=f;L23:do{if((m-v|0)>1){if((a[v]|0)!=48){w=21;break}u=v+1|0;p=a[u]|0;if(!((p<<24>>24|0)==120|(p<<24>>24|0)==88)){w=21;break}p=k;n=ad[c[(c[p>>2]|0)+44>>2]&31](r,48)|0;q=c[j>>2]|0;c[j>>2]=q+4;c[q>>2]=n;n=v+2|0;q=ad[c[(c[p>>2]|0)+44>>2]&31](r,a[u]|0)|0;u=c[j>>2]|0;c[j>>2]=u+4;c[u>>2]=q;if(n>>>0<f>>>0){x=n}else{y=n;z=n;break}while(1){q=a[x]|0;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);u=x+1|0;if((lb(q<<24>>24|0,c[8322]|0)|0)==0){y=x;z=n;break L23}if(u>>>0<f>>>0){x=u}else{y=u;z=n;break}}}else{w=21}}while(0);L38:do{if((w|0)==21){if(v>>>0<f>>>0){A=v}else{y=v;z=v;break}while(1){x=a[A]|0;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);q=A+1|0;if((Dc(x<<24>>24|0,c[8322]|0)|0)==0){y=A;z=v;break L38}if(q>>>0<f>>>0){A=q}else{y=q;z=v;break}}}}while(0);v=o;A=o;w=d[A]|0;if((w&1|0)==0){B=w>>>1}else{B=c[o+4>>2]|0}do{if((B|0)==0){ld[c[(c[k>>2]|0)+48>>2]&15](r,z,y,c[j>>2]|0)|0;c[j>>2]=(c[j>>2]|0)+(y-z<<2)}else{do{if((z|0)!=(y|0)){w=y-1|0;if(z>>>0<w>>>0){C=z;D=w}else{break}do{w=a[C]|0;a[C]=a[D]|0;a[D]=w;C=C+1|0;D=D-1|0;}while(C>>>0<D>>>0)}}while(0);x=dd[c[(c[s>>2]|0)+16>>2]&127](t)|0;if(z>>>0<y>>>0){w=v+1|0;q=o+4|0;n=o+8|0;u=k;p=0;E=0;F=z;while(1){G=(a[A]&1)==0;do{if((a[(G?w:c[n>>2]|0)+E|0]|0)>0){if((p|0)!=(a[(G?w:c[n>>2]|0)+E|0]|0)){H=E;I=p;break}J=c[j>>2]|0;c[j>>2]=J+4;c[J>>2]=x;J=d[A]|0;H=(E>>>0<(((J&1|0)==0?J>>>1:c[q>>2]|0)-1|0)>>>0)+E|0;I=0}else{H=E;I=p}}while(0);G=ad[c[(c[u>>2]|0)+44>>2]&31](r,a[F]|0)|0;J=c[j>>2]|0;c[j>>2]=J+4;c[J>>2]=G;G=F+1|0;if(G>>>0<y>>>0){p=I+1|0;E=H;F=G}else{break}}}F=g+(z-b<<2)|0;E=c[j>>2]|0;if((F|0)==(E|0)){break}p=E-4|0;if(F>>>0<p>>>0){K=F;L=p}else{break}do{p=c[K>>2]|0;c[K>>2]=c[L>>2];c[L>>2]=p;K=K+4|0;L=L-4|0;}while(K>>>0<L>>>0)}}while(0);L78:do{if(y>>>0<f>>>0){L=k;K=y;while(1){z=a[K]|0;if(z<<24>>24==46){break}H=ad[c[(c[L>>2]|0)+44>>2]&31](r,z)|0;z=c[j>>2]|0;c[j>>2]=z+4;c[z>>2]=H;H=K+1|0;if(H>>>0<f>>>0){K=H}else{M=H;break L78}}L=dd[c[(c[s>>2]|0)+12>>2]&127](t)|0;H=c[j>>2]|0;c[j>>2]=H+4;c[H>>2]=L;M=K+1|0}else{M=y}}while(0);ld[c[(c[k>>2]|0)+48>>2]&15](r,M,f,c[j>>2]|0)|0;r=(c[j>>2]|0)+(m-M<<2)|0;c[j>>2]=r;if((e|0)==(f|0)){N=r;c[h>>2]=N;vf(o);i=l;return}N=g+(e-b<<2)|0;c[h>>2]=N;vf(o);i=l;return}function Ci(b,d,e,f,g,j){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;j=+j;var k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0;d=i;i=i+320|0;k=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[k>>2];k=d|0;l=d+8|0;m=d+40|0;n=d+48|0;o=d+280|0;p=d+288|0;q=d+296|0;r=d+304|0;s=d+312|0;c[k>>2]=37;c[k+4>>2]=0;t=k;k=t+1|0;u=f+4|0;v=c[u>>2]|0;if((v&2048|0)==0){w=k}else{a[k]=43;w=t+2|0}if((v&1024|0)==0){x=w}else{a[w]=35;x=w+1|0}w=v&260;k=v>>>14;do{if((w|0)==260){a[x]=76;v=x+1|0;if((k&1|0)==0){a[v]=97;y=0;break}else{a[v]=65;y=0;break}}else{a[x]=46;a[x+1|0]=42;a[x+2|0]=76;v=x+3|0;if((w|0)==4){if((k&1|0)==0){a[v]=102;y=1;break}else{a[v]=70;y=1;break}}else if((w|0)==256){if((k&1|0)==0){a[v]=101;y=1;break}else{a[v]=69;y=1;break}}else{if((k&1|0)==0){a[v]=103;y=1;break}else{a[v]=71;y=1;break}}}}while(0);k=l|0;c[m>>2]=k;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);l=c[8322]|0;if(y){w=gi(k,30,l,t,(z=i,i=i+16|0,c[z>>2]=c[f+8>>2],h[z+8>>3]=j,z)|0)|0;i=z;A=w}else{w=gi(k,30,l,t,(z=i,i=i+8|0,h[z>>3]=j,z)|0)|0;i=z;A=w}do{if((A|0)>29){w=(a[36464]|0)==0;if(y){do{if(w){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);l=ni(m,c[8322]|0,t,(z=i,i=i+16|0,c[z>>2]=c[f+8>>2],h[z+8>>3]=j,z)|0)|0;i=z;B=l}else{do{if(w){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);w=ni(m,c[8322]|0,t,(z=i,i=i+8|0,h[z>>3]=j,z)|0)|0;i=z;B=w}w=c[m>>2]|0;if((w|0)!=0){C=B;D=w;E=w;break}Yn();w=c[m>>2]|0;C=B;D=w;E=w}else{C=A;D=0;E=c[m>>2]|0}}while(0);A=E+C|0;B=c[u>>2]&176;do{if((B|0)==16){u=a[E]|0;if((u<<24>>24|0)==45|(u<<24>>24|0)==43){F=E+1|0;break}if(!((C|0)>1&u<<24>>24==48)){G=53;break}u=a[E+1|0]|0;if(!((u<<24>>24|0)==120|(u<<24>>24|0)==88)){G=53;break}F=E+2|0}else if((B|0)==32){F=A}else{G=53}}while(0);if((G|0)==53){F=E}do{if((E|0)==(k|0)){H=n|0;I=0;J=k}else{G=Ln(C<<3)|0;B=G;if((G|0)!=0){H=B;I=B;J=E;break}Yn();H=B;I=B;J=c[m>>2]|0}}while(0);Pf(q,f);Bi(J,F,A,H,o,p,q);Xe(c[q>>2]|0)|0;q=e|0;c[s>>2]=c[q>>2];wi(r,s,H,c[o>>2]|0,c[p>>2]|0,f,g);g=c[r>>2]|0;c[q>>2]=g;c[b>>2]=g;if((I|0)!=0){Mn(I)}if((D|0)==0){i=d;return}Mn(D);i=d;return}function Di(b,d,e,f,g,h){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0;d=i;i=i+216|0;j=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[j>>2];j=d|0;k=d+24|0;l=d+48|0;m=d+200|0;n=d+208|0;o=d+16|0;a[o]=a[4440]|0;a[o+1|0]=a[4441]|0;a[o+2|0]=a[4442]|0;a[o+3|0]=a[4443]|0;a[o+4|0]=a[4444]|0;a[o+5|0]=a[4445]|0;p=k|0;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);q=gi(p,20,c[8322]|0,o,(o=i,i=i+8|0,c[o>>2]=h,o)|0)|0;i=o;o=k+q|0;h=c[f+4>>2]&176;do{if((h|0)==32){r=o}else if((h|0)==16){s=a[p]|0;if((s<<24>>24|0)==45|(s<<24>>24|0)==43){r=k+1|0;break}if(!((q|0)>1&s<<24>>24==48)){t=12;break}s=a[k+1|0]|0;if(!((s<<24>>24|0)==120|(s<<24>>24|0)==88)){t=12;break}r=k+2|0}else{t=12}}while(0);if((t|0)==12){r=p}Pf(m,f);t=m|0;m=c[t>>2]|0;if((c[8974]|0)!=-1){c[j>>2]=35896;c[j+4>>2]=14;c[j+8>>2]=0;qf(35896,j,96)}j=(c[8975]|0)-1|0;h=c[m+8>>2]|0;do{if((c[m+12>>2]|0)-h>>2>>>0>j>>>0){s=c[h+(j<<2)>>2]|0;if((s|0)==0){break}Xe(c[t>>2]|0)|0;u=l|0;ld[c[(c[s>>2]|0)+48>>2]&15](s,p,o,u)|0;s=l+(q<<2)|0;if((r|0)==(o|0)){v=s;w=e|0;x=c[w>>2]|0;y=n|0;c[y>>2]=x;wi(b,n,u,v,s,f,g);i=d;return}v=l+(r-k<<2)|0;w=e|0;x=c[w>>2]|0;y=n|0;c[y>>2]=x;wi(b,n,u,v,s,f,g);i=d;return}}while(0);d=Nc(4)|0;qn(d);Qb(d|0,10192,134)}function Ei(d,e,f,g,h,j,k,l,m){d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;l=l|0;m=m|0;var n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0,Z=0,_=0,$=0,aa=0,ba=0,ca=0,da=0;n=i;i=i+48|0;o=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[o>>2];o=g;g=i;i=i+4|0;i=i+7&-8;c[g>>2]=c[o>>2];o=n|0;p=n+16|0;q=n+24|0;r=n+32|0;s=n+40|0;Pf(p,h);t=p|0;p=c[t>>2]|0;if((c[8976]|0)!=-1){c[o>>2]=35904;c[o+4>>2]=14;c[o+8>>2]=0;qf(35904,o,96)}o=(c[8977]|0)-1|0;u=c[p+8>>2]|0;do{if((c[p+12>>2]|0)-u>>2>>>0>o>>>0){v=c[u+(o<<2)>>2]|0;if((v|0)==0){break}w=v;Xe(c[t>>2]|0)|0;c[j>>2]=0;x=f|0;L7:do{if((l|0)==(m|0)){y=67}else{z=g|0;A=v;B=v+8|0;C=v;D=e;E=r|0;F=s|0;G=q|0;H=l;I=0;L9:while(1){J=I;while(1){if((J|0)!=0){y=67;break L7}K=c[x>>2]|0;do{if((K|0)==0){L=0}else{if((c[K+12>>2]|0)!=(c[K+16>>2]|0)){L=K;break}if((dd[c[(c[K>>2]|0)+36>>2]&127](K)|0)!=-1){L=K;break}c[x>>2]=0;L=0}}while(0);K=(L|0)==0;M=c[z>>2]|0;L19:do{if((M|0)==0){y=20}else{do{if((c[M+12>>2]|0)==(c[M+16>>2]|0)){if((dd[c[(c[M>>2]|0)+36>>2]&127](M)|0)!=-1){break}c[z>>2]=0;y=20;break L19}}while(0);if(K){N=M}else{y=21;break L9}}}while(0);if((y|0)==20){y=0;if(K){y=21;break L9}else{N=0}}if((ed[c[(c[A>>2]|0)+36>>2]&63](w,a[H]|0,0)|0)<<24>>24==37){y=24;break}M=a[H]|0;if(M<<24>>24>-1){O=c[B>>2]|0;if((b[O+(M<<24>>24<<1)>>1]&8192)!=0){P=H;y=35;break}}Q=L+12|0;M=c[Q>>2]|0;R=L+16|0;if((M|0)==(c[R>>2]|0)){S=(dd[c[(c[L>>2]|0)+36>>2]&127](L)|0)&255}else{S=a[M]|0}M=ad[c[(c[C>>2]|0)+12>>2]&31](w,S)|0;if(M<<24>>24==(ad[c[(c[C>>2]|0)+12>>2]&31](w,a[H]|0)|0)<<24>>24){y=62;break}c[j>>2]=4;J=4}L37:do{if((y|0)==62){y=0;J=c[Q>>2]|0;if((J|0)==(c[R>>2]|0)){dd[c[(c[L>>2]|0)+40>>2]&127](L)|0}else{c[Q>>2]=J+1}T=H+1|0}else if((y|0)==24){y=0;J=H+1|0;if((J|0)==(m|0)){y=25;break L9}M=ed[c[(c[A>>2]|0)+36>>2]&63](w,a[J]|0,0)|0;if((M<<24>>24|0)==69|(M<<24>>24|0)==48){U=H+2|0;if((U|0)==(m|0)){y=28;break L9}V=M;W=ed[c[(c[A>>2]|0)+36>>2]&63](w,a[U]|0,0)|0;X=U}else{V=0;W=M;X=J}J=c[(c[D>>2]|0)+36>>2]|0;c[E>>2]=L;c[F>>2]=N;jd[J&7](q,e,r,s,h,j,k,W,V);c[x>>2]=c[G>>2];T=X+1|0}else if((y|0)==35){while(1){y=0;J=P+1|0;if((J|0)==(m|0)){Y=m;break}M=a[J]|0;if(M<<24>>24<=-1){Y=J;break}if((b[O+(M<<24>>24<<1)>>1]&8192)==0){Y=J;break}else{P=J;y=35}}K=L;J=N;while(1){do{if((K|0)==0){Z=0}else{if((c[K+12>>2]|0)!=(c[K+16>>2]|0)){Z=K;break}if((dd[c[(c[K>>2]|0)+36>>2]&127](K)|0)!=-1){Z=K;break}c[x>>2]=0;Z=0}}while(0);M=(Z|0)==0;do{if((J|0)==0){y=48}else{if((c[J+12>>2]|0)!=(c[J+16>>2]|0)){if(M){_=J;break}else{T=Y;break L37}}if((dd[c[(c[J>>2]|0)+36>>2]&127](J)|0)==-1){c[z>>2]=0;y=48;break}else{if(M^(J|0)==0){_=J;break}else{T=Y;break L37}}}}while(0);if((y|0)==48){y=0;if(M){T=Y;break L37}else{_=0}}U=Z+12|0;$=c[U>>2]|0;aa=Z+16|0;if(($|0)==(c[aa>>2]|0)){ba=(dd[c[(c[Z>>2]|0)+36>>2]&127](Z)|0)&255}else{ba=a[$]|0}if(ba<<24>>24<=-1){T=Y;break L37}if((b[(c[B>>2]|0)+(ba<<24>>24<<1)>>1]&8192)==0){T=Y;break L37}$=c[U>>2]|0;if(($|0)==(c[aa>>2]|0)){dd[c[(c[Z>>2]|0)+40>>2]&127](Z)|0;K=Z;J=_;continue}else{c[U>>2]=$+1;K=Z;J=_;continue}}}}while(0);if((T|0)==(m|0)){y=67;break L7}H=T;I=c[j>>2]|0}if((y|0)==21){c[j>>2]=4;ca=L;break}else if((y|0)==28){c[j>>2]=4;ca=L;break}else if((y|0)==25){c[j>>2]=4;ca=L;break}}}while(0);if((y|0)==67){ca=c[x>>2]|0}w=f|0;do{if((ca|0)!=0){if((c[ca+12>>2]|0)!=(c[ca+16>>2]|0)){break}if((dd[c[(c[ca>>2]|0)+36>>2]&127](ca)|0)!=-1){break}c[w>>2]=0}}while(0);x=c[w>>2]|0;v=(x|0)==0;I=g|0;H=c[I>>2]|0;L95:do{if((H|0)==0){y=77}else{do{if((c[H+12>>2]|0)==(c[H+16>>2]|0)){if((dd[c[(c[H>>2]|0)+36>>2]&127](H)|0)!=-1){break}c[I>>2]=0;y=77;break L95}}while(0);if(!v){break}da=d|0;c[da>>2]=x;i=n;return}}while(0);do{if((y|0)==77){if(v){break}da=d|0;c[da>>2]=x;i=n;return}}while(0);c[j>>2]=c[j>>2]|2;da=d|0;c[da>>2]=x;i=n;return}}while(0);n=Nc(4)|0;qn(n);Qb(n|0,10192,134)}function Fi(a){a=a|0;Ve(a|0);Tn(a);return}function Gi(a){a=a|0;Ve(a|0);return}function Hi(a){a=a|0;return 2}function Ii(a,b,d,e,f,g,h){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0,l=0;j=i;i=i+16|0;k=d;d=i;i=i+4|0;i=i+7&-8;c[d>>2]=c[k>>2];k=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[k>>2];k=j|0;l=j+8|0;c[k>>2]=c[d>>2];c[l>>2]=c[e>>2];Ei(a,b,k,l,f,g,h,4424,4432);i=j;return}function Ji(b,d,e,f,g,h,j){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;var k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0;k=i;i=i+16|0;l=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[l>>2];l=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[l>>2];l=k|0;m=k+8|0;n=d+8|0;o=dd[c[(c[n>>2]|0)+20>>2]&127](n)|0;c[l>>2]=c[e>>2];c[m>>2]=c[f>>2];f=o;e=a[o]|0;if((e&1)==0){p=f+1|0;q=f+1|0}else{f=c[o+8>>2]|0;p=f;q=f}f=e&255;if((f&1|0)==0){r=f>>>1}else{r=c[o+4>>2]|0}Ei(b,d,l,m,g,h,j,q,p+r|0);i=k;return}function Ki(a,b,d,e,f,g,h){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0;j=i;i=i+32|0;k=d;d=i;i=i+4|0;i=i+7&-8;c[d>>2]=c[k>>2];k=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[k>>2];k=j|0;l=j+8|0;m=j+24|0;Pf(m,f);f=m|0;m=c[f>>2]|0;if((c[8976]|0)!=-1){c[l>>2]=35904;c[l+4>>2]=14;c[l+8>>2]=0;qf(35904,l,96)}l=(c[8977]|0)-1|0;n=c[m+8>>2]|0;do{if((c[m+12>>2]|0)-n>>2>>>0>l>>>0){o=c[n+(l<<2)>>2]|0;if((o|0)==0){break}Xe(c[f>>2]|0)|0;p=c[e>>2]|0;q=b+8|0;r=dd[c[c[q>>2]>>2]&127](q)|0;c[k>>2]=p;p=(eh(d,k,r,r+168|0,o,g,0)|0)-r|0;if((p|0)>=168){s=4;t=0;u=d|0;v=c[u>>2]|0;w=a|0;c[w>>2]=v;i=j;return}c[h+24>>2]=((p|0)/12|0|0)%7|0;s=4;t=0;u=d|0;v=c[u>>2]|0;w=a|0;c[w>>2]=v;i=j;return}}while(0);j=Nc(4)|0;qn(j);Qb(j|0,10192,134)}function Li(a,b,d,e,f,g,h){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0;j=i;i=i+32|0;k=d;d=i;i=i+4|0;i=i+7&-8;c[d>>2]=c[k>>2];k=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[k>>2];k=j|0;l=j+8|0;m=j+24|0;Pf(m,f);f=m|0;m=c[f>>2]|0;if((c[8976]|0)!=-1){c[l>>2]=35904;c[l+4>>2]=14;c[l+8>>2]=0;qf(35904,l,96)}l=(c[8977]|0)-1|0;n=c[m+8>>2]|0;do{if((c[m+12>>2]|0)-n>>2>>>0>l>>>0){o=c[n+(l<<2)>>2]|0;if((o|0)==0){break}Xe(c[f>>2]|0)|0;p=c[e>>2]|0;q=b+8|0;r=dd[c[(c[q>>2]|0)+4>>2]&127](q)|0;c[k>>2]=p;p=(eh(d,k,r,r+288|0,o,g,0)|0)-r|0;if((p|0)>=288){s=4;t=0;u=d|0;v=c[u>>2]|0;w=a|0;c[w>>2]=v;i=j;return}c[h+16>>2]=((p|0)/12|0|0)%12|0;s=4;t=0;u=d|0;v=c[u>>2]|0;w=a|0;c[w>>2]=v;i=j;return}}while(0);j=Nc(4)|0;qn(j);Qb(j|0,10192,134)}function Mi(a,b,d,e,f,g,h){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0;b=i;i=i+32|0;j=d;d=i;i=i+4|0;i=i+7&-8;c[d>>2]=c[j>>2];j=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[j>>2];j=b|0;k=b+8|0;l=b+24|0;Pf(l,f);f=l|0;l=c[f>>2]|0;if((c[8976]|0)!=-1){c[k>>2]=35904;c[k+4>>2]=14;c[k+8>>2]=0;qf(35904,k,96)}k=(c[8977]|0)-1|0;m=c[l+8>>2]|0;do{if((c[l+12>>2]|0)-m>>2>>>0>k>>>0){n=c[m+(k<<2)>>2]|0;if((n|0)==0){break}Xe(c[f>>2]|0)|0;c[j>>2]=c[e>>2];o=Ri(d,j,g,n,4)|0;if((c[g>>2]&4|0)!=0){p=4;q=0;r=d|0;s=c[r>>2]|0;t=a|0;c[t>>2]=s;i=b;return}if((o|0)<69){u=o+2e3|0}else{u=(o-69|0)>>>0<31>>>0?o+1900|0:o}c[h+20>>2]=u-1900;p=4;q=0;r=d|0;s=c[r>>2]|0;t=a|0;c[t>>2]=s;i=b;return}}while(0);b=Nc(4)|0;qn(b);Qb(b|0,10192,134)}function Ni(b,d,e,f,g,h,j,k,l){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;l=l|0;var m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0,Z=0,_=0,$=0,aa=0,ba=0,ca=0,da=0,ea=0,fa=0,ga=0,ha=0,ia=0,ja=0,ka=0,la=0,ma=0;l=i;i=i+328|0;m=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[m>>2];m=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[m>>2];m=l|0;n=l+8|0;o=l+16|0;p=l+24|0;q=l+32|0;r=l+40|0;s=l+48|0;t=l+56|0;u=l+64|0;v=l+72|0;w=l+80|0;x=l+88|0;y=l+96|0;z=l+112|0;A=l+120|0;B=l+128|0;C=l+136|0;D=l+144|0;E=l+152|0;F=l+160|0;G=l+168|0;H=l+176|0;I=l+184|0;J=l+192|0;K=l+200|0;L=l+208|0;M=l+216|0;N=l+224|0;O=l+232|0;P=l+240|0;Q=l+248|0;R=l+256|0;S=l+264|0;T=l+272|0;U=l+280|0;V=l+288|0;W=l+296|0;X=l+304|0;Y=l+312|0;Z=l+320|0;c[h>>2]=0;Pf(z,g);_=z|0;z=c[_>>2]|0;if((c[8976]|0)!=-1){c[y>>2]=35904;c[y+4>>2]=14;c[y+8>>2]=0;qf(35904,y,96)}y=(c[8977]|0)-1|0;$=c[z+8>>2]|0;do{if((c[z+12>>2]|0)-$>>2>>>0>y>>>0){aa=c[$+(y<<2)>>2]|0;if((aa|0)==0){break}ba=aa;Xe(c[_>>2]|0)|0;L7:do{switch(k<<24>>24|0){case 89:{c[m>>2]=c[f>>2];aa=Ri(e,m,h,ba,4)|0;if((c[h>>2]&4|0)!=0){break L7}c[j+20>>2]=aa-1900;break};case 37:{c[Z>>2]=c[f>>2];Qi(0,e,Z,h,ba);break};case 68:{aa=e|0;c[E>>2]=c[aa>>2];c[F>>2]=c[f>>2];Ei(D,d,E,F,g,h,j,4416,4424);c[aa>>2]=c[D>>2];break};case 70:{aa=e|0;c[H>>2]=c[aa>>2];c[I>>2]=c[f>>2];Ei(G,d,H,I,g,h,j,4408,4416);c[aa>>2]=c[G>>2];break};case 72:{c[u>>2]=c[f>>2];aa=Ri(e,u,h,ba,2)|0;ca=c[h>>2]|0;if((ca&4|0)==0&(aa|0)<24){c[j+8>>2]=aa;break L7}else{c[h>>2]=ca|4;break L7}break};case 73:{ca=j+8|0;c[t>>2]=c[f>>2];aa=Ri(e,t,h,ba,2)|0;da=c[h>>2]|0;do{if((da&4|0)==0){if((aa-1|0)>>>0>=12>>>0){break}c[ca>>2]=aa;break L7}}while(0);c[h>>2]=da|4;break};case 97:case 65:{aa=c[f>>2]|0;ca=d+8|0;ea=dd[c[c[ca>>2]>>2]&127](ca)|0;c[x>>2]=aa;aa=(eh(e,x,ea,ea+168|0,ba,h,0)|0)-ea|0;if((aa|0)>=168){break L7}c[j+24>>2]=((aa|0)/12|0|0)%7|0;break};case 110:case 116:{c[J>>2]=c[f>>2];Oi(0,e,J,h,ba);break};case 112:{c[K>>2]=c[f>>2];Pi(d,j+8|0,e,K,h,ba);break};case 114:{aa=e|0;c[M>>2]=c[aa>>2];c[N>>2]=c[f>>2];Ei(L,d,M,N,g,h,j,4392,4403);c[aa>>2]=c[L>>2];break};case 82:{aa=e|0;c[P>>2]=c[aa>>2];c[Q>>2]=c[f>>2];Ei(O,d,P,Q,g,h,j,4384,4389);c[aa>>2]=c[O>>2];break};case 83:{c[p>>2]=c[f>>2];aa=Ri(e,p,h,ba,2)|0;ea=c[h>>2]|0;if((ea&4|0)==0&(aa|0)<61){c[j>>2]=aa;break L7}else{c[h>>2]=ea|4;break L7}break};case 84:{ea=e|0;c[S>>2]=c[ea>>2];c[T>>2]=c[f>>2];Ei(R,d,S,T,g,h,j,4376,4384);c[ea>>2]=c[R>>2];break};case 119:{c[o>>2]=c[f>>2];ea=Ri(e,o,h,ba,1)|0;aa=c[h>>2]|0;if((aa&4|0)==0&(ea|0)<7){c[j+24>>2]=ea;break L7}else{c[h>>2]=aa|4;break L7}break};case 99:{aa=d+8|0;ea=dd[c[(c[aa>>2]|0)+12>>2]&127](aa)|0;aa=e|0;c[B>>2]=c[aa>>2];c[C>>2]=c[f>>2];ca=ea;fa=a[ea]|0;if((fa&1)==0){ga=ca+1|0;ha=ca+1|0}else{ca=c[ea+8>>2]|0;ga=ca;ha=ca}ca=fa&255;if((ca&1|0)==0){ia=ca>>>1}else{ia=c[ea+4>>2]|0}Ei(A,d,B,C,g,h,j,ha,ga+ia|0);c[aa>>2]=c[A>>2];break};case 100:case 101:{aa=j+12|0;c[v>>2]=c[f>>2];ea=Ri(e,v,h,ba,2)|0;ca=c[h>>2]|0;do{if((ca&4|0)==0){if((ea-1|0)>>>0>=31>>>0){break}c[aa>>2]=ea;break L7}}while(0);c[h>>2]=ca|4;break};case 98:case 66:case 104:{ea=c[f>>2]|0;aa=d+8|0;da=dd[c[(c[aa>>2]|0)+4>>2]&127](aa)|0;c[w>>2]=ea;ea=(eh(e,w,da,da+288|0,ba,h,0)|0)-da|0;if((ea|0)>=288){break L7}c[j+16>>2]=((ea|0)/12|0|0)%12|0;break};case 120:{ea=c[(c[d>>2]|0)+20>>2]|0;c[U>>2]=c[e>>2];c[V>>2]=c[f>>2];Zc[ea&127](b,d,U,V,g,h,j);i=l;return};case 88:{ea=d+8|0;da=dd[c[(c[ea>>2]|0)+24>>2]&127](ea)|0;ea=e|0;c[X>>2]=c[ea>>2];c[Y>>2]=c[f>>2];aa=da;fa=a[da]|0;if((fa&1)==0){ja=aa+1|0;ka=aa+1|0}else{aa=c[da+8>>2]|0;ja=aa;ka=aa}aa=fa&255;if((aa&1|0)==0){la=aa>>>1}else{la=c[da+4>>2]|0}Ei(W,d,X,Y,g,h,j,ka,ja+la|0);c[ea>>2]=c[W>>2];break};case 121:{c[n>>2]=c[f>>2];ea=Ri(e,n,h,ba,4)|0;if((c[h>>2]&4|0)!=0){break L7}if((ea|0)<69){ma=ea+2e3|0}else{ma=(ea-69|0)>>>0<31>>>0?ea+1900|0:ea}c[j+20>>2]=ma-1900;break};case 106:{c[s>>2]=c[f>>2];ea=Ri(e,s,h,ba,3)|0;da=c[h>>2]|0;if((da&4|0)==0&(ea|0)<366){c[j+28>>2]=ea;break L7}else{c[h>>2]=da|4;break L7}break};case 109:{c[r>>2]=c[f>>2];da=Ri(e,r,h,ba,2)|0;ea=c[h>>2]|0;if((ea&4|0)==0&(da|0)<13){c[j+16>>2]=da-1;break L7}else{c[h>>2]=ea|4;break L7}break};case 77:{c[q>>2]=c[f>>2];ea=Ri(e,q,h,ba,2)|0;da=c[h>>2]|0;if((da&4|0)==0&(ea|0)<60){c[j+4>>2]=ea;break L7}else{c[h>>2]=da|4;break L7}break};default:{c[h>>2]=c[h>>2]|4}}}while(0);c[b>>2]=c[e>>2];i=l;return}}while(0);l=Nc(4)|0;qn(l);Qb(l|0,10192,134)}function Oi(d,e,f,g,h){d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0;d=i;j=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[j>>2];j=e|0;e=f|0;f=h+8|0;L1:while(1){h=c[j>>2]|0;do{if((h|0)==0){k=0}else{if((c[h+12>>2]|0)!=(c[h+16>>2]|0)){k=h;break}if((dd[c[(c[h>>2]|0)+36>>2]&127](h)|0)==-1){c[j>>2]=0;k=0;break}else{k=c[j>>2]|0;break}}}while(0);h=(k|0)==0;l=c[e>>2]|0;L10:do{if((l|0)==0){m=12}else{do{if((c[l+12>>2]|0)==(c[l+16>>2]|0)){if((dd[c[(c[l>>2]|0)+36>>2]&127](l)|0)!=-1){break}c[e>>2]=0;m=12;break L10}}while(0);if(h){n=l;o=0}else{p=l;q=0;break L1}}}while(0);if((m|0)==12){m=0;if(h){p=0;q=1;break}else{n=0;o=1}}l=c[j>>2]|0;r=c[l+12>>2]|0;if((r|0)==(c[l+16>>2]|0)){s=(dd[c[(c[l>>2]|0)+36>>2]&127](l)|0)&255}else{s=a[r]|0}if(s<<24>>24<=-1){p=n;q=o;break}if((b[(c[f>>2]|0)+(s<<24>>24<<1)>>1]&8192)==0){p=n;q=o;break}r=c[j>>2]|0;l=r+12|0;t=c[l>>2]|0;if((t|0)==(c[r+16>>2]|0)){dd[c[(c[r>>2]|0)+40>>2]&127](r)|0;continue}else{c[l>>2]=t+1;continue}}o=c[j>>2]|0;do{if((o|0)==0){u=0}else{if((c[o+12>>2]|0)!=(c[o+16>>2]|0)){u=o;break}if((dd[c[(c[o>>2]|0)+36>>2]&127](o)|0)==-1){c[j>>2]=0;u=0;break}else{u=c[j>>2]|0;break}}}while(0);j=(u|0)==0;do{if(q){m=31}else{if((c[p+12>>2]|0)!=(c[p+16>>2]|0)){if(!(j^(p|0)==0)){break}i=d;return}if((dd[c[(c[p>>2]|0)+36>>2]&127](p)|0)==-1){c[e>>2]=0;m=31;break}if(!j){break}i=d;return}}while(0);do{if((m|0)==31){if(j){break}i=d;return}}while(0);c[g>>2]=c[g>>2]|2;i=d;return}function Pi(a,b,e,f,g,h){a=a|0;b=b|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0,l=0,m=0,n=0;j=i;i=i+8|0;k=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[k>>2];k=j|0;l=a+8|0;a=dd[c[(c[l>>2]|0)+8>>2]&127](l)|0;l=d[a]|0;if((l&1|0)==0){m=l>>>1}else{m=c[a+4>>2]|0}l=d[a+12|0]|0;if((l&1|0)==0){n=l>>>1}else{n=c[a+16>>2]|0}if((m|0)==(-n|0)){c[g>>2]=c[g>>2]|4;i=j;return}c[k>>2]=c[f>>2];f=eh(e,k,a,a+24|0,h,g,0)|0;g=f-a|0;do{if((f|0)==(a|0)){if((c[b>>2]|0)!=12){break}c[b>>2]=0;i=j;return}}while(0);if((g|0)!=12){i=j;return}g=c[b>>2]|0;if((g|0)>=12){i=j;return}c[b>>2]=g+12;i=j;return}function Qi(b,d,e,f,g){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;var h=0,j=0,k=0,l=0,m=0,n=0,o=0;b=i;h=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[h>>2];h=d|0;d=c[h>>2]|0;do{if((d|0)==0){j=0}else{if((c[d+12>>2]|0)!=(c[d+16>>2]|0)){j=d;break}if((dd[c[(c[d>>2]|0)+36>>2]&127](d)|0)==-1){c[h>>2]=0;j=0;break}else{j=c[h>>2]|0;break}}}while(0);d=(j|0)==0;j=e|0;e=c[j>>2]|0;L8:do{if((e|0)==0){k=11}else{do{if((c[e+12>>2]|0)==(c[e+16>>2]|0)){if((dd[c[(c[e>>2]|0)+36>>2]&127](e)|0)!=-1){break}c[j>>2]=0;k=11;break L8}}while(0);if(d){l=e;m=0}else{k=12}}}while(0);if((k|0)==11){if(d){k=12}else{l=0;m=1}}if((k|0)==12){c[f>>2]=c[f>>2]|6;i=b;return}d=c[h>>2]|0;e=c[d+12>>2]|0;if((e|0)==(c[d+16>>2]|0)){n=(dd[c[(c[d>>2]|0)+36>>2]&127](d)|0)&255}else{n=a[e]|0}if((ed[c[(c[g>>2]|0)+36>>2]&63](g,n,0)|0)<<24>>24!=37){c[f>>2]=c[f>>2]|4;i=b;return}n=c[h>>2]|0;g=n+12|0;e=c[g>>2]|0;if((e|0)==(c[n+16>>2]|0)){dd[c[(c[n>>2]|0)+40>>2]&127](n)|0}else{c[g>>2]=e+1}e=c[h>>2]|0;do{if((e|0)==0){o=0}else{if((c[e+12>>2]|0)!=(c[e+16>>2]|0)){o=e;break}if((dd[c[(c[e>>2]|0)+36>>2]&127](e)|0)==-1){c[h>>2]=0;o=0;break}else{o=c[h>>2]|0;break}}}while(0);h=(o|0)==0;do{if(m){k=31}else{if((c[l+12>>2]|0)!=(c[l+16>>2]|0)){if(!(h^(l|0)==0)){break}i=b;return}if((dd[c[(c[l>>2]|0)+36>>2]&127](l)|0)==-1){c[j>>2]=0;k=31;break}if(!h){break}i=b;return}}while(0);do{if((k|0)==31){if(h){break}i=b;return}}while(0);c[f>>2]=c[f>>2]|2;i=b;return}function Ri(d,e,f,g,h){d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0;j=i;k=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[k>>2];k=d|0;d=c[k>>2]|0;do{if((d|0)==0){l=0}else{if((c[d+12>>2]|0)!=(c[d+16>>2]|0)){l=d;break}if((dd[c[(c[d>>2]|0)+36>>2]&127](d)|0)==-1){c[k>>2]=0;l=0;break}else{l=c[k>>2]|0;break}}}while(0);d=(l|0)==0;l=e|0;e=c[l>>2]|0;L8:do{if((e|0)==0){m=11}else{do{if((c[e+12>>2]|0)==(c[e+16>>2]|0)){if((dd[c[(c[e>>2]|0)+36>>2]&127](e)|0)!=-1){break}c[l>>2]=0;m=11;break L8}}while(0);if(d){n=e}else{m=12}}}while(0);if((m|0)==11){if(d){m=12}else{n=0}}if((m|0)==12){c[f>>2]=c[f>>2]|6;o=0;i=j;return o|0}d=c[k>>2]|0;e=c[d+12>>2]|0;if((e|0)==(c[d+16>>2]|0)){p=(dd[c[(c[d>>2]|0)+36>>2]&127](d)|0)&255}else{p=a[e]|0}do{if(p<<24>>24>-1){e=g+8|0;if((b[(c[e>>2]|0)+(p<<24>>24<<1)>>1]&2048)==0){break}d=g;q=(ed[c[(c[d>>2]|0)+36>>2]&63](g,p,0)|0)<<24>>24;r=c[k>>2]|0;s=r+12|0;t=c[s>>2]|0;if((t|0)==(c[r+16>>2]|0)){dd[c[(c[r>>2]|0)+40>>2]&127](r)|0;u=q;v=h;w=n}else{c[s>>2]=t+1;u=q;v=h;w=n}while(1){x=u-48|0;q=v-1|0;t=c[k>>2]|0;do{if((t|0)==0){y=0}else{if((c[t+12>>2]|0)!=(c[t+16>>2]|0)){y=t;break}if((dd[c[(c[t>>2]|0)+36>>2]&127](t)|0)==-1){c[k>>2]=0;y=0;break}else{y=c[k>>2]|0;break}}}while(0);t=(y|0)==0;if((w|0)==0){z=y;A=0}else{do{if((c[w+12>>2]|0)==(c[w+16>>2]|0)){if((dd[c[(c[w>>2]|0)+36>>2]&127](w)|0)!=-1){B=w;break}c[l>>2]=0;B=0}else{B=w}}while(0);z=c[k>>2]|0;A=B}C=(A|0)==0;if(!((t^C)&(q|0)>0)){m=41;break}s=c[z+12>>2]|0;if((s|0)==(c[z+16>>2]|0)){D=(dd[c[(c[z>>2]|0)+36>>2]&127](z)|0)&255}else{D=a[s]|0}if(D<<24>>24<=-1){o=x;m=57;break}if((b[(c[e>>2]|0)+(D<<24>>24<<1)>>1]&2048)==0){o=x;m=58;break}s=((ed[c[(c[d>>2]|0)+36>>2]&63](g,D,0)|0)<<24>>24)+(x*10|0)|0;r=c[k>>2]|0;E=r+12|0;F=c[E>>2]|0;if((F|0)==(c[r+16>>2]|0)){dd[c[(c[r>>2]|0)+40>>2]&127](r)|0;u=s;v=q;w=A;continue}else{c[E>>2]=F+1;u=s;v=q;w=A;continue}}if((m|0)==41){do{if((z|0)==0){G=0}else{if((c[z+12>>2]|0)!=(c[z+16>>2]|0)){G=z;break}if((dd[c[(c[z>>2]|0)+36>>2]&127](z)|0)==-1){c[k>>2]=0;G=0;break}else{G=c[k>>2]|0;break}}}while(0);d=(G|0)==0;L65:do{if(C){m=51}else{do{if((c[A+12>>2]|0)==(c[A+16>>2]|0)){if((dd[c[(c[A>>2]|0)+36>>2]&127](A)|0)!=-1){break}c[l>>2]=0;m=51;break L65}}while(0);if(d){o=x}else{break}i=j;return o|0}}while(0);do{if((m|0)==51){if(d){break}else{o=x}i=j;return o|0}}while(0);c[f>>2]=c[f>>2]|2;o=x;i=j;return o|0}else if((m|0)==57){i=j;return o|0}else if((m|0)==58){i=j;return o|0}}}while(0);c[f>>2]=c[f>>2]|4;o=0;i=j;return o|0}function Si(a,b,d,e,f,g,h,j,k){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;var l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0,Z=0,_=0,$=0,aa=0,ba=0,ca=0,da=0,ea=0,fa=0,ga=0;l=i;i=i+48|0;m=d;d=i;i=i+4|0;i=i+7&-8;c[d>>2]=c[m>>2];m=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[m>>2];m=l|0;n=l+16|0;o=l+24|0;p=l+32|0;q=l+40|0;Pf(n,f);r=n|0;n=c[r>>2]|0;if((c[8974]|0)!=-1){c[m>>2]=35896;c[m+4>>2]=14;c[m+8>>2]=0;qf(35896,m,96)}m=(c[8975]|0)-1|0;s=c[n+8>>2]|0;do{if((c[n+12>>2]|0)-s>>2>>>0>m>>>0){t=c[s+(m<<2)>>2]|0;if((t|0)==0){break}u=t;Xe(c[r>>2]|0)|0;c[g>>2]=0;v=d|0;L7:do{if((j|0)==(k|0)){w=71}else{x=e|0;y=t;z=t;A=t;B=b;C=p|0;D=q|0;E=o|0;F=j;G=0;L9:while(1){H=G;while(1){if((H|0)!=0){w=71;break L7}I=c[v>>2]|0;do{if((I|0)==0){J=0}else{K=c[I+12>>2]|0;if((K|0)==(c[I+16>>2]|0)){L=dd[c[(c[I>>2]|0)+36>>2]&127](I)|0}else{L=c[K>>2]|0}if((L|0)!=-1){J=I;break}c[v>>2]=0;J=0}}while(0);I=(J|0)==0;K=c[x>>2]|0;do{if((K|0)==0){w=23}else{M=c[K+12>>2]|0;if((M|0)==(c[K+16>>2]|0)){N=dd[c[(c[K>>2]|0)+36>>2]&127](K)|0}else{N=c[M>>2]|0}if((N|0)==-1){c[x>>2]=0;w=23;break}else{if(I^(K|0)==0){O=K;break}else{w=25;break L9}}}}while(0);if((w|0)==23){w=0;if(I){w=25;break L9}else{O=0}}if((ed[c[(c[y>>2]|0)+52>>2]&63](u,c[F>>2]|0,0)|0)<<24>>24==37){w=28;break}if(ed[c[(c[z>>2]|0)+12>>2]&63](u,8192,c[F>>2]|0)|0){P=F;w=38;break}Q=J+12|0;K=c[Q>>2]|0;R=J+16|0;if((K|0)==(c[R>>2]|0)){S=dd[c[(c[J>>2]|0)+36>>2]&127](J)|0}else{S=c[K>>2]|0}K=ad[c[(c[A>>2]|0)+28>>2]&31](u,S)|0;if((K|0)==(ad[c[(c[A>>2]|0)+28>>2]&31](u,c[F>>2]|0)|0)){w=66;break}c[g>>2]=4;H=4}L41:do{if((w|0)==38){while(1){w=0;H=P+4|0;if((H|0)==(k|0)){T=k;break}if(ed[c[(c[z>>2]|0)+12>>2]&63](u,8192,c[H>>2]|0)|0){P=H;w=38}else{T=H;break}}I=J;H=O;while(1){do{if((I|0)==0){U=0}else{K=c[I+12>>2]|0;if((K|0)==(c[I+16>>2]|0)){V=dd[c[(c[I>>2]|0)+36>>2]&127](I)|0}else{V=c[K>>2]|0}if((V|0)!=-1){U=I;break}c[v>>2]=0;U=0}}while(0);K=(U|0)==0;do{if((H|0)==0){w=53}else{M=c[H+12>>2]|0;if((M|0)==(c[H+16>>2]|0)){W=dd[c[(c[H>>2]|0)+36>>2]&127](H)|0}else{W=c[M>>2]|0}if((W|0)==-1){c[x>>2]=0;w=53;break}else{if(K^(H|0)==0){X=H;break}else{Y=T;break L41}}}}while(0);if((w|0)==53){w=0;if(K){Y=T;break L41}else{X=0}}M=U+12|0;Z=c[M>>2]|0;_=U+16|0;if((Z|0)==(c[_>>2]|0)){$=dd[c[(c[U>>2]|0)+36>>2]&127](U)|0}else{$=c[Z>>2]|0}if(!(ed[c[(c[z>>2]|0)+12>>2]&63](u,8192,$)|0)){Y=T;break L41}Z=c[M>>2]|0;if((Z|0)==(c[_>>2]|0)){dd[c[(c[U>>2]|0)+40>>2]&127](U)|0;I=U;H=X;continue}else{c[M>>2]=Z+4;I=U;H=X;continue}}}else if((w|0)==66){w=0;H=c[Q>>2]|0;if((H|0)==(c[R>>2]|0)){dd[c[(c[J>>2]|0)+40>>2]&127](J)|0}else{c[Q>>2]=H+4}Y=F+4|0}else if((w|0)==28){w=0;H=F+4|0;if((H|0)==(k|0)){w=29;break L9}I=ed[c[(c[y>>2]|0)+52>>2]&63](u,c[H>>2]|0,0)|0;if((I<<24>>24|0)==69|(I<<24>>24|0)==48){Z=F+8|0;if((Z|0)==(k|0)){w=32;break L9}aa=I;ba=ed[c[(c[y>>2]|0)+52>>2]&63](u,c[Z>>2]|0,0)|0;ca=Z}else{aa=0;ba=I;ca=H}H=c[(c[B>>2]|0)+36>>2]|0;c[C>>2]=J;c[D>>2]=O;jd[H&7](o,b,p,q,f,g,h,ba,aa);c[v>>2]=c[E>>2];Y=ca+4|0}}while(0);if((Y|0)==(k|0)){w=71;break L7}F=Y;G=c[g>>2]|0}if((w|0)==25){c[g>>2]=4;da=J;break}else if((w|0)==29){c[g>>2]=4;da=J;break}else if((w|0)==32){c[g>>2]=4;da=J;break}}}while(0);if((w|0)==71){da=c[v>>2]|0}u=d|0;do{if((da|0)!=0){t=c[da+12>>2]|0;if((t|0)==(c[da+16>>2]|0)){ea=dd[c[(c[da>>2]|0)+36>>2]&127](da)|0}else{ea=c[t>>2]|0}if((ea|0)!=-1){break}c[u>>2]=0}}while(0);v=c[u>>2]|0;t=(v|0)==0;G=e|0;F=c[G>>2]|0;do{if((F|0)==0){w=84}else{E=c[F+12>>2]|0;if((E|0)==(c[F+16>>2]|0)){fa=dd[c[(c[F>>2]|0)+36>>2]&127](F)|0}else{fa=c[E>>2]|0}if((fa|0)==-1){c[G>>2]=0;w=84;break}if(!(t^(F|0)==0)){break}ga=a|0;c[ga>>2]=v;i=l;return}}while(0);do{if((w|0)==84){if(t){break}ga=a|0;c[ga>>2]=v;i=l;return}}while(0);c[g>>2]=c[g>>2]|2;ga=a|0;c[ga>>2]=v;i=l;return}}while(0);l=Nc(4)|0;qn(l);Qb(l|0,10192,134)}function Ti(a){a=a|0;Ve(a|0);Tn(a);return}function Ui(a){a=a|0;Ve(a|0);return}function Vi(a){a=a|0;return 2}function Wi(a,b,d,e,f,g,h){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0,l=0;j=i;i=i+16|0;k=d;d=i;i=i+4|0;i=i+7&-8;c[d>>2]=c[k>>2];k=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[k>>2];k=j|0;l=j+8|0;c[k>>2]=c[d>>2];c[l>>2]=c[e>>2];Si(a,b,k,l,f,g,h,4344,4376);i=j;return}function Xi(b,d,e,f,g,h,j){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;var k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0;k=i;i=i+16|0;l=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[l>>2];l=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[l>>2];l=k|0;m=k+8|0;n=d+8|0;o=dd[c[(c[n>>2]|0)+20>>2]&127](n)|0;c[l>>2]=c[e>>2];c[m>>2]=c[f>>2];f=a[o]|0;if((f&1)==0){p=o+4|0;q=o+4|0}else{e=c[o+8>>2]|0;p=e;q=e}e=f&255;if((e&1|0)==0){r=e>>>1}else{r=c[o+4>>2]|0}Si(b,d,l,m,g,h,j,q,p+(r<<2)|0);i=k;return}function Yi(a,b,d,e,f,g,h){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0;j=i;i=i+32|0;k=d;d=i;i=i+4|0;i=i+7&-8;c[d>>2]=c[k>>2];k=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[k>>2];k=j|0;l=j+8|0;m=j+24|0;Pf(m,f);f=m|0;m=c[f>>2]|0;if((c[8974]|0)!=-1){c[l>>2]=35896;c[l+4>>2]=14;c[l+8>>2]=0;qf(35896,l,96)}l=(c[8975]|0)-1|0;n=c[m+8>>2]|0;do{if((c[m+12>>2]|0)-n>>2>>>0>l>>>0){o=c[n+(l<<2)>>2]|0;if((o|0)==0){break}Xe(c[f>>2]|0)|0;p=c[e>>2]|0;q=b+8|0;r=dd[c[c[q>>2]>>2]&127](q)|0;c[k>>2]=p;p=(Dh(d,k,r,r+168|0,o,g,0)|0)-r|0;if((p|0)>=168){s=4;t=0;u=d|0;v=c[u>>2]|0;w=a|0;c[w>>2]=v;i=j;return}c[h+24>>2]=((p|0)/12|0|0)%7|0;s=4;t=0;u=d|0;v=c[u>>2]|0;w=a|0;c[w>>2]=v;i=j;return}}while(0);j=Nc(4)|0;qn(j);Qb(j|0,10192,134)}function Zi(a,b,d,e,f,g,h){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0;j=i;i=i+32|0;k=d;d=i;i=i+4|0;i=i+7&-8;c[d>>2]=c[k>>2];k=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[k>>2];k=j|0;l=j+8|0;m=j+24|0;Pf(m,f);f=m|0;m=c[f>>2]|0;if((c[8974]|0)!=-1){c[l>>2]=35896;c[l+4>>2]=14;c[l+8>>2]=0;qf(35896,l,96)}l=(c[8975]|0)-1|0;n=c[m+8>>2]|0;do{if((c[m+12>>2]|0)-n>>2>>>0>l>>>0){o=c[n+(l<<2)>>2]|0;if((o|0)==0){break}Xe(c[f>>2]|0)|0;p=c[e>>2]|0;q=b+8|0;r=dd[c[(c[q>>2]|0)+4>>2]&127](q)|0;c[k>>2]=p;p=(Dh(d,k,r,r+288|0,o,g,0)|0)-r|0;if((p|0)>=288){s=4;t=0;u=d|0;v=c[u>>2]|0;w=a|0;c[w>>2]=v;i=j;return}c[h+16>>2]=((p|0)/12|0|0)%12|0;s=4;t=0;u=d|0;v=c[u>>2]|0;w=a|0;c[w>>2]=v;i=j;return}}while(0);j=Nc(4)|0;qn(j);Qb(j|0,10192,134)}function _i(a,b,d,e,f,g,h){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0;b=i;i=i+32|0;j=d;d=i;i=i+4|0;i=i+7&-8;c[d>>2]=c[j>>2];j=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[j>>2];j=b|0;k=b+8|0;l=b+24|0;Pf(l,f);f=l|0;l=c[f>>2]|0;if((c[8974]|0)!=-1){c[k>>2]=35896;c[k+4>>2]=14;c[k+8>>2]=0;qf(35896,k,96)}k=(c[8975]|0)-1|0;m=c[l+8>>2]|0;do{if((c[l+12>>2]|0)-m>>2>>>0>k>>>0){n=c[m+(k<<2)>>2]|0;if((n|0)==0){break}Xe(c[f>>2]|0)|0;c[j>>2]=c[e>>2];o=dj(d,j,g,n,4)|0;if((c[g>>2]&4|0)!=0){p=4;q=0;r=d|0;s=c[r>>2]|0;t=a|0;c[t>>2]=s;i=b;return}if((o|0)<69){u=o+2e3|0}else{u=(o-69|0)>>>0<31>>>0?o+1900|0:o}c[h+20>>2]=u-1900;p=4;q=0;r=d|0;s=c[r>>2]|0;t=a|0;c[t>>2]=s;i=b;return}}while(0);b=Nc(4)|0;qn(b);Qb(b|0,10192,134)}function $i(b,d,e,f,g,h,j,k,l){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;l=l|0;var m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0,Z=0,_=0,$=0,aa=0,ba=0,ca=0,da=0,ea=0,fa=0,ga=0,ha=0,ia=0,ja=0,ka=0,la=0,ma=0;l=i;i=i+328|0;m=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[m>>2];m=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[m>>2];m=l|0;n=l+8|0;o=l+16|0;p=l+24|0;q=l+32|0;r=l+40|0;s=l+48|0;t=l+56|0;u=l+64|0;v=l+72|0;w=l+80|0;x=l+88|0;y=l+96|0;z=l+112|0;A=l+120|0;B=l+128|0;C=l+136|0;D=l+144|0;E=l+152|0;F=l+160|0;G=l+168|0;H=l+176|0;I=l+184|0;J=l+192|0;K=l+200|0;L=l+208|0;M=l+216|0;N=l+224|0;O=l+232|0;P=l+240|0;Q=l+248|0;R=l+256|0;S=l+264|0;T=l+272|0;U=l+280|0;V=l+288|0;W=l+296|0;X=l+304|0;Y=l+312|0;Z=l+320|0;c[h>>2]=0;Pf(z,g);_=z|0;z=c[_>>2]|0;if((c[8974]|0)!=-1){c[y>>2]=35896;c[y+4>>2]=14;c[y+8>>2]=0;qf(35896,y,96)}y=(c[8975]|0)-1|0;$=c[z+8>>2]|0;do{if((c[z+12>>2]|0)-$>>2>>>0>y>>>0){aa=c[$+(y<<2)>>2]|0;if((aa|0)==0){break}ba=aa;Xe(c[_>>2]|0)|0;L7:do{switch(k<<24>>24|0){case 112:{c[K>>2]=c[f>>2];bj(d,j+8|0,e,K,h,ba);break};case 114:{aa=e|0;c[M>>2]=c[aa>>2];c[N>>2]=c[f>>2];Si(L,d,M,N,g,h,j,4264,4308);c[aa>>2]=c[L>>2];break};case 82:{aa=e|0;c[P>>2]=c[aa>>2];c[Q>>2]=c[f>>2];Si(O,d,P,Q,g,h,j,4240,4260);c[aa>>2]=c[O>>2];break};case 83:{c[p>>2]=c[f>>2];aa=dj(e,p,h,ba,2)|0;ca=c[h>>2]|0;if((ca&4|0)==0&(aa|0)<61){c[j>>2]=aa;break L7}else{c[h>>2]=ca|4;break L7}break};case 84:{ca=e|0;c[S>>2]=c[ca>>2];c[T>>2]=c[f>>2];Si(R,d,S,T,g,h,j,4208,4240);c[ca>>2]=c[R>>2];break};case 119:{c[o>>2]=c[f>>2];ca=dj(e,o,h,ba,1)|0;aa=c[h>>2]|0;if((aa&4|0)==0&(ca|0)<7){c[j+24>>2]=ca;break L7}else{c[h>>2]=aa|4;break L7}break};case 120:{aa=c[(c[d>>2]|0)+20>>2]|0;c[U>>2]=c[e>>2];c[V>>2]=c[f>>2];Zc[aa&127](b,d,U,V,g,h,j);i=l;return};case 88:{aa=d+8|0;ca=dd[c[(c[aa>>2]|0)+24>>2]&127](aa)|0;aa=e|0;c[X>>2]=c[aa>>2];c[Y>>2]=c[f>>2];da=a[ca]|0;if((da&1)==0){ea=ca+4|0;fa=ca+4|0}else{ga=c[ca+8>>2]|0;ea=ga;fa=ga}ga=da&255;if((ga&1|0)==0){ha=ga>>>1}else{ha=c[ca+4>>2]|0}Si(W,d,X,Y,g,h,j,fa,ea+(ha<<2)|0);c[aa>>2]=c[W>>2];break};case 68:{aa=e|0;c[E>>2]=c[aa>>2];c[F>>2]=c[f>>2];Si(D,d,E,F,g,h,j,4312,4344);c[aa>>2]=c[D>>2];break};case 100:case 101:{aa=j+12|0;c[v>>2]=c[f>>2];ca=dj(e,v,h,ba,2)|0;ga=c[h>>2]|0;do{if((ga&4|0)==0){if((ca-1|0)>>>0>=31>>>0){break}c[aa>>2]=ca;break L7}}while(0);c[h>>2]=ga|4;break};case 99:{ca=d+8|0;aa=dd[c[(c[ca>>2]|0)+12>>2]&127](ca)|0;ca=e|0;c[B>>2]=c[ca>>2];c[C>>2]=c[f>>2];da=a[aa]|0;if((da&1)==0){ia=aa+4|0;ja=aa+4|0}else{ka=c[aa+8>>2]|0;ia=ka;ja=ka}ka=da&255;if((ka&1|0)==0){la=ka>>>1}else{la=c[aa+4>>2]|0}Si(A,d,B,C,g,h,j,ja,ia+(la<<2)|0);c[ca>>2]=c[A>>2];break};case 70:{ca=e|0;c[H>>2]=c[ca>>2];c[I>>2]=c[f>>2];Si(G,d,H,I,g,h,j,4176,4208);c[ca>>2]=c[G>>2];break};case 72:{c[u>>2]=c[f>>2];ca=dj(e,u,h,ba,2)|0;aa=c[h>>2]|0;if((aa&4|0)==0&(ca|0)<24){c[j+8>>2]=ca;break L7}else{c[h>>2]=aa|4;break L7}break};case 106:{c[s>>2]=c[f>>2];aa=dj(e,s,h,ba,3)|0;ca=c[h>>2]|0;if((ca&4|0)==0&(aa|0)<366){c[j+28>>2]=aa;break L7}else{c[h>>2]=ca|4;break L7}break};case 97:case 65:{ca=c[f>>2]|0;aa=d+8|0;ka=dd[c[c[aa>>2]>>2]&127](aa)|0;c[x>>2]=ca;ca=(Dh(e,x,ka,ka+168|0,ba,h,0)|0)-ka|0;if((ca|0)>=168){break L7}c[j+24>>2]=((ca|0)/12|0|0)%7|0;break};case 73:{ca=j+8|0;c[t>>2]=c[f>>2];ka=dj(e,t,h,ba,2)|0;aa=c[h>>2]|0;do{if((aa&4|0)==0){if((ka-1|0)>>>0>=12>>>0){break}c[ca>>2]=ka;break L7}}while(0);c[h>>2]=aa|4;break};case 109:{c[r>>2]=c[f>>2];ka=dj(e,r,h,ba,2)|0;ca=c[h>>2]|0;if((ca&4|0)==0&(ka|0)<13){c[j+16>>2]=ka-1;break L7}else{c[h>>2]=ca|4;break L7}break};case 77:{c[q>>2]=c[f>>2];ca=dj(e,q,h,ba,2)|0;ka=c[h>>2]|0;if((ka&4|0)==0&(ca|0)<60){c[j+4>>2]=ca;break L7}else{c[h>>2]=ka|4;break L7}break};case 110:case 116:{c[J>>2]=c[f>>2];aj(0,e,J,h,ba);break};case 98:case 66:case 104:{ka=c[f>>2]|0;ca=d+8|0;ga=dd[c[(c[ca>>2]|0)+4>>2]&127](ca)|0;c[w>>2]=ka;ka=(Dh(e,w,ga,ga+288|0,ba,h,0)|0)-ga|0;if((ka|0)>=288){break L7}c[j+16>>2]=((ka|0)/12|0|0)%12|0;break};case 121:{c[n>>2]=c[f>>2];ka=dj(e,n,h,ba,4)|0;if((c[h>>2]&4|0)!=0){break L7}if((ka|0)<69){ma=ka+2e3|0}else{ma=(ka-69|0)>>>0<31>>>0?ka+1900|0:ka}c[j+20>>2]=ma-1900;break};case 89:{c[m>>2]=c[f>>2];ka=dj(e,m,h,ba,4)|0;if((c[h>>2]&4|0)!=0){break L7}c[j+20>>2]=ka-1900;break};case 37:{c[Z>>2]=c[f>>2];cj(0,e,Z,h,ba);break};default:{c[h>>2]=c[h>>2]|4}}}while(0);c[b>>2]=c[e>>2];i=l;return}}while(0);l=Nc(4)|0;qn(l);Qb(l|0,10192,134)}function aj(a,b,d,e,f){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0;a=i;g=d;d=i;i=i+4|0;i=i+7&-8;c[d>>2]=c[g>>2];g=b|0;b=d|0;d=f;L1:while(1){h=c[g>>2]|0;do{if((h|0)==0){j=1}else{k=c[h+12>>2]|0;if((k|0)==(c[h+16>>2]|0)){l=dd[c[(c[h>>2]|0)+36>>2]&127](h)|0}else{l=c[k>>2]|0}if((l|0)==-1){c[g>>2]=0;j=1;break}else{j=(c[g>>2]|0)==0;break}}}while(0);h=c[b>>2]|0;do{if((h|0)==0){m=15}else{k=c[h+12>>2]|0;if((k|0)==(c[h+16>>2]|0)){n=dd[c[(c[h>>2]|0)+36>>2]&127](h)|0}else{n=c[k>>2]|0}if((n|0)==-1){c[b>>2]=0;m=15;break}else{k=(h|0)==0;if(j^k){o=h;p=k;break}else{q=h;r=k;break L1}}}}while(0);if((m|0)==15){m=0;if(j){q=0;r=1;break}else{o=0;p=1}}h=c[g>>2]|0;k=c[h+12>>2]|0;if((k|0)==(c[h+16>>2]|0)){s=dd[c[(c[h>>2]|0)+36>>2]&127](h)|0}else{s=c[k>>2]|0}if(!(ed[c[(c[d>>2]|0)+12>>2]&63](f,8192,s)|0)){q=o;r=p;break}k=c[g>>2]|0;h=k+12|0;t=c[h>>2]|0;if((t|0)==(c[k+16>>2]|0)){dd[c[(c[k>>2]|0)+40>>2]&127](k)|0;continue}else{c[h>>2]=t+4;continue}}p=c[g>>2]|0;do{if((p|0)==0){u=1}else{o=c[p+12>>2]|0;if((o|0)==(c[p+16>>2]|0)){v=dd[c[(c[p>>2]|0)+36>>2]&127](p)|0}else{v=c[o>>2]|0}if((v|0)==-1){c[g>>2]=0;u=1;break}else{u=(c[g>>2]|0)==0;break}}}while(0);do{if(r){m=37}else{g=c[q+12>>2]|0;if((g|0)==(c[q+16>>2]|0)){w=dd[c[(c[q>>2]|0)+36>>2]&127](q)|0}else{w=c[g>>2]|0}if((w|0)==-1){c[b>>2]=0;m=37;break}if(!(u^(q|0)==0)){break}i=a;return}}while(0);do{if((m|0)==37){if(u){break}i=a;return}}while(0);c[e>>2]=c[e>>2]|2;i=a;return}function bj(a,b,e,f,g,h){a=a|0;b=b|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0,l=0,m=0,n=0;j=i;i=i+8|0;k=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[k>>2];k=j|0;l=a+8|0;a=dd[c[(c[l>>2]|0)+8>>2]&127](l)|0;l=d[a]|0;if((l&1|0)==0){m=l>>>1}else{m=c[a+4>>2]|0}l=d[a+12|0]|0;if((l&1|0)==0){n=l>>>1}else{n=c[a+16>>2]|0}if((m|0)==(-n|0)){c[g>>2]=c[g>>2]|4;i=j;return}c[k>>2]=c[f>>2];f=Dh(e,k,a,a+24|0,h,g,0)|0;g=f-a|0;do{if((f|0)==(a|0)){if((c[b>>2]|0)!=12){break}c[b>>2]=0;i=j;return}}while(0);if((g|0)!=12){i=j;return}g=c[b>>2]|0;if((g|0)>=12){i=j;return}c[b>>2]=g+12;i=j;return}function cj(a,b,d,e,f){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0;a=i;g=d;d=i;i=i+4|0;i=i+7&-8;c[d>>2]=c[g>>2];g=b|0;b=c[g>>2]|0;do{if((b|0)==0){h=1}else{j=c[b+12>>2]|0;if((j|0)==(c[b+16>>2]|0)){k=dd[c[(c[b>>2]|0)+36>>2]&127](b)|0}else{k=c[j>>2]|0}if((k|0)==-1){c[g>>2]=0;h=1;break}else{h=(c[g>>2]|0)==0;break}}}while(0);k=d|0;d=c[k>>2]|0;do{if((d|0)==0){l=14}else{b=c[d+12>>2]|0;if((b|0)==(c[d+16>>2]|0)){m=dd[c[(c[d>>2]|0)+36>>2]&127](d)|0}else{m=c[b>>2]|0}if((m|0)==-1){c[k>>2]=0;l=14;break}else{b=(d|0)==0;if(h^b){n=d;o=b;break}else{l=16;break}}}}while(0);if((l|0)==14){if(h){l=16}else{n=0;o=1}}if((l|0)==16){c[e>>2]=c[e>>2]|6;i=a;return}h=c[g>>2]|0;d=c[h+12>>2]|0;if((d|0)==(c[h+16>>2]|0)){p=dd[c[(c[h>>2]|0)+36>>2]&127](h)|0}else{p=c[d>>2]|0}if((ed[c[(c[f>>2]|0)+52>>2]&63](f,p,0)|0)<<24>>24!=37){c[e>>2]=c[e>>2]|4;i=a;return}p=c[g>>2]|0;f=p+12|0;d=c[f>>2]|0;if((d|0)==(c[p+16>>2]|0)){dd[c[(c[p>>2]|0)+40>>2]&127](p)|0}else{c[f>>2]=d+4}d=c[g>>2]|0;do{if((d|0)==0){q=1}else{f=c[d+12>>2]|0;if((f|0)==(c[d+16>>2]|0)){r=dd[c[(c[d>>2]|0)+36>>2]&127](d)|0}else{r=c[f>>2]|0}if((r|0)==-1){c[g>>2]=0;q=1;break}else{q=(c[g>>2]|0)==0;break}}}while(0);do{if(o){l=38}else{g=c[n+12>>2]|0;if((g|0)==(c[n+16>>2]|0)){s=dd[c[(c[n>>2]|0)+36>>2]&127](n)|0}else{s=c[g>>2]|0}if((s|0)==-1){c[k>>2]=0;l=38;break}if(!(q^(n|0)==0)){break}i=a;return}}while(0);do{if((l|0)==38){if(q){break}i=a;return}}while(0);c[e>>2]=c[e>>2]|2;i=a;return}function dj(a,b,d,e,f){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0;g=i;h=b;b=i;i=i+4|0;i=i+7&-8;c[b>>2]=c[h>>2];h=a|0;a=c[h>>2]|0;do{if((a|0)==0){j=1}else{k=c[a+12>>2]|0;if((k|0)==(c[a+16>>2]|0)){l=dd[c[(c[a>>2]|0)+36>>2]&127](a)|0}else{l=c[k>>2]|0}if((l|0)==-1){c[h>>2]=0;j=1;break}else{j=(c[h>>2]|0)==0;break}}}while(0);l=b|0;b=c[l>>2]|0;do{if((b|0)==0){m=14}else{a=c[b+12>>2]|0;if((a|0)==(c[b+16>>2]|0)){n=dd[c[(c[b>>2]|0)+36>>2]&127](b)|0}else{n=c[a>>2]|0}if((n|0)==-1){c[l>>2]=0;m=14;break}else{if(j^(b|0)==0){o=b;break}else{m=16;break}}}}while(0);if((m|0)==14){if(j){m=16}else{o=0}}if((m|0)==16){c[d>>2]=c[d>>2]|6;p=0;i=g;return p|0}j=c[h>>2]|0;b=c[j+12>>2]|0;if((b|0)==(c[j+16>>2]|0)){q=dd[c[(c[j>>2]|0)+36>>2]&127](j)|0}else{q=c[b>>2]|0}b=e;if(!(ed[c[(c[b>>2]|0)+12>>2]&63](e,2048,q)|0)){c[d>>2]=c[d>>2]|4;p=0;i=g;return p|0}j=e;n=(ed[c[(c[j>>2]|0)+52>>2]&63](e,q,0)|0)<<24>>24;q=c[h>>2]|0;a=q+12|0;k=c[a>>2]|0;if((k|0)==(c[q+16>>2]|0)){dd[c[(c[q>>2]|0)+40>>2]&127](q)|0;r=n;s=f;t=o}else{c[a>>2]=k+4;r=n;s=f;t=o}while(1){u=r-48|0;o=s-1|0;f=c[h>>2]|0;do{if((f|0)==0){v=0}else{n=c[f+12>>2]|0;if((n|0)==(c[f+16>>2]|0)){w=dd[c[(c[f>>2]|0)+36>>2]&127](f)|0}else{w=c[n>>2]|0}if((w|0)==-1){c[h>>2]=0;v=0;break}else{v=c[h>>2]|0;break}}}while(0);f=(v|0)==0;if((t|0)==0){x=v;y=0}else{n=c[t+12>>2]|0;if((n|0)==(c[t+16>>2]|0)){z=dd[c[(c[t>>2]|0)+36>>2]&127](t)|0}else{z=c[n>>2]|0}if((z|0)==-1){c[l>>2]=0;A=0}else{A=t}x=c[h>>2]|0;y=A}B=(y|0)==0;if(!((f^B)&(o|0)>0)){break}f=c[x+12>>2]|0;if((f|0)==(c[x+16>>2]|0)){C=dd[c[(c[x>>2]|0)+36>>2]&127](x)|0}else{C=c[f>>2]|0}if(!(ed[c[(c[b>>2]|0)+12>>2]&63](e,2048,C)|0)){p=u;m=66;break}f=((ed[c[(c[j>>2]|0)+52>>2]&63](e,C,0)|0)<<24>>24)+(u*10|0)|0;n=c[h>>2]|0;k=n+12|0;a=c[k>>2]|0;if((a|0)==(c[n+16>>2]|0)){dd[c[(c[n>>2]|0)+40>>2]&127](n)|0;r=f;s=o;t=y;continue}else{c[k>>2]=a+4;r=f;s=o;t=y;continue}}if((m|0)==66){i=g;return p|0}do{if((x|0)==0){D=1}else{t=c[x+12>>2]|0;if((t|0)==(c[x+16>>2]|0)){E=dd[c[(c[x>>2]|0)+36>>2]&127](x)|0}else{E=c[t>>2]|0}if((E|0)==-1){c[h>>2]=0;D=1;break}else{D=(c[h>>2]|0)==0;break}}}while(0);do{if(B){m=60}else{h=c[y+12>>2]|0;if((h|0)==(c[y+16>>2]|0)){F=dd[c[(c[y>>2]|0)+36>>2]&127](y)|0}else{F=c[h>>2]|0}if((F|0)==-1){c[l>>2]=0;m=60;break}if(D^(y|0)==0){p=u}else{break}i=g;return p|0}}while(0);do{if((m|0)==60){if(D){break}else{p=u}i=g;return p|0}}while(0);c[d>>2]=c[d>>2]|2;p=u;i=g;return p|0}function ej(b){b=b|0;var d=0,e=0,f=0,g=0;d=b;e=b+8|0;f=c[e>>2]|0;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);if((f|0)==(c[8322]|0)){g=b|0;Ve(g);Tn(d);return}pb(c[e>>2]|0);g=b|0;Ve(g);Tn(d);return}function fj(b){b=b|0;var d=0,e=0,f=0;d=b+8|0;e=c[d>>2]|0;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);if((e|0)==(c[8322]|0)){f=b|0;Ve(f);return}pb(c[d>>2]|0);f=b|0;Ve(f);return}function gj(b,d,e,f,g,h,j,k){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;var l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0;g=i;i=i+112|0;f=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[f>>2];f=g|0;l=g+8|0;m=l|0;n=f|0;a[n]=37;o=f+1|0;a[o]=j;p=f+2|0;a[p]=k;a[f+3|0]=0;if(k<<24>>24!=0){a[o]=k;a[p]=j}j=Lb(m|0,100,n|0,h|0,c[d+8>>2]|0)|0;d=l+j|0;l=c[e>>2]|0;if((j|0)==0){q=l;r=b|0;c[r>>2]=q;i=g;return}else{s=l;t=m}while(1){m=a[t]|0;if((s|0)==0){u=0}else{l=s+24|0;j=c[l>>2]|0;if((j|0)==(c[s+28>>2]|0)){v=ad[c[(c[s>>2]|0)+52>>2]&31](s,m&255)|0}else{c[l>>2]=j+1;a[j]=m;v=m&255}u=(v|0)==-1?0:s}m=t+1|0;if((m|0)==(d|0)){q=u;break}else{s=u;t=m}}r=b|0;c[r>>2]=q;i=g;return}function hj(b){b=b|0;var d=0,e=0,f=0,g=0;d=b;e=b+8|0;f=c[e>>2]|0;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);if((f|0)==(c[8322]|0)){g=b|0;Ve(g);Tn(d);return}pb(c[e>>2]|0);g=b|0;Ve(g);Tn(d);return}function ij(b){b=b|0;var d=0,e=0,f=0;d=b+8|0;e=c[d>>2]|0;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);if((e|0)==(c[8322]|0)){f=b|0;Ve(f);return}pb(c[d>>2]|0);f=b|0;Ve(f);return}function jj(a,b,d,e,f,g,h,j){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;var k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0;f=i;i=i+408|0;e=d;d=i;i=i+4|0;i=i+7&-8;c[d>>2]=c[e>>2];e=f|0;k=f+400|0;l=e|0;c[k>>2]=e+400;kj(b+8|0,l,k,g,h,j);j=c[k>>2]|0;k=c[d>>2]|0;if((l|0)==(j|0)){m=k;n=a|0;c[n>>2]=m;i=f;return}else{o=k;p=l}while(1){l=c[p>>2]|0;if((o|0)==0){q=0}else{k=o+24|0;d=c[k>>2]|0;if((d|0)==(c[o+28>>2]|0)){r=ad[c[(c[o>>2]|0)+52>>2]&31](o,l)|0}else{c[k>>2]=d+4;c[d>>2]=l;r=l}q=(r|0)==-1?0:o}l=p+4|0;if((l|0)==(j|0)){m=q;break}else{o=q;p=l}}n=a|0;c[n>>2]=m;i=f;return}function kj(b,d,e,f,g,h){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0;j=i;i=i+120|0;k=j|0;l=j+112|0;m=i;i=i+4|0;i=i+7&-8;n=j+8|0;o=k|0;a[o]=37;p=k+1|0;a[p]=g;q=k+2|0;a[q]=h;a[k+3|0]=0;if(h<<24>>24!=0){a[p]=h;a[q]=g}g=b|0;Lb(n|0,100,o|0,f|0,c[g>>2]|0)|0;c[l>>2]=0;c[l+4>>2]=0;c[m>>2]=n;n=(c[e>>2]|0)-d>>2;f=uc(c[g>>2]|0)|0;g=en(d,m,n,l)|0;if((f|0)!=0){uc(f|0)|0}if((g|0)==-1){gk(2544)}else{c[e>>2]=d+(g<<2);i=j;return}}function lj(a){a=a|0;Ve(a|0);Tn(a);return}function mj(a){a=a|0;Ve(a|0);return}function nj(a){a=a|0;return 127}function oj(a){a=a|0;return 127}function pj(a,b){a=a|0;b=b|0;ko(a|0,0,12)|0;return}function qj(a,b){a=a|0;b=b|0;ko(a|0,0,12)|0;return}function rj(a,b){a=a|0;b=b|0;ko(a|0,0,12)|0;return}function sj(a,b){a=a|0;b=b|0;uf(a,1,45);return}function tj(a){a=a|0;return 0}function uj(b,c){b=b|0;c=c|0;c=b;C=67109634;a[c]=C;C=C>>8;a[c+1|0]=C;C=C>>8;a[c+2|0]=C;C=C>>8;a[c+3|0]=C;return}function vj(b,c){b=b|0;c=c|0;c=b;C=67109634;a[c]=C;C=C>>8;a[c+1|0]=C;C=C>>8;a[c+2|0]=C;C=C>>8;a[c+3|0]=C;return}function wj(a){a=a|0;Ve(a|0);Tn(a);return}function xj(a){a=a|0;Ve(a|0);return}function yj(a){a=a|0;return 127}function zj(a){a=a|0;return 127}function Aj(a,b){a=a|0;b=b|0;ko(a|0,0,12)|0;return}function Bj(a,b){a=a|0;b=b|0;ko(a|0,0,12)|0;return}function Cj(a,b){a=a|0;b=b|0;ko(a|0,0,12)|0;return}function Dj(a,b){a=a|0;b=b|0;uf(a,1,45);return}function Ej(a){a=a|0;return 0}function Fj(b,c){b=b|0;c=c|0;c=b;C=67109634;a[c]=C;C=C>>8;a[c+1|0]=C;C=C>>8;a[c+2|0]=C;C=C>>8;a[c+3|0]=C;return}function Gj(b,c){b=b|0;c=c|0;c=b;C=67109634;a[c]=C;C=C>>8;a[c+1|0]=C;C=C>>8;a[c+2|0]=C;C=C>>8;a[c+3|0]=C;return}function Hj(a){a=a|0;Ve(a|0);Tn(a);return}function Ij(a){a=a|0;Ve(a|0);return}function Jj(a){a=a|0;return 2147483647}function Kj(a){a=a|0;return 2147483647}function Lj(a,b){a=a|0;b=b|0;ko(a|0,0,12)|0;return}function Mj(a,b){a=a|0;b=b|0;ko(a|0,0,12)|0;return}function Nj(a,b){a=a|0;b=b|0;ko(a|0,0,12)|0;return}function Oj(a,b){a=a|0;b=b|0;Ff(a,1,45);return}function Pj(a){a=a|0;return 0}function Qj(b,c){b=b|0;c=c|0;c=b;C=67109634;a[c]=C;C=C>>8;a[c+1|0]=C;C=C>>8;a[c+2|0]=C;C=C>>8;a[c+3|0]=C;return}function Rj(b,c){b=b|0;c=c|0;c=b;C=67109634;a[c]=C;C=C>>8;a[c+1|0]=C;C=C>>8;a[c+2|0]=C;C=C>>8;a[c+3|0]=C;return}function Sj(a){a=a|0;Ve(a|0);Tn(a);return}function Tj(a){a=a|0;Ve(a|0);return}function Uj(a){a=a|0;return 2147483647}function Vj(a){a=a|0;return 2147483647}function Wj(a,b){a=a|0;b=b|0;ko(a|0,0,12)|0;return}function Xj(a,b){a=a|0;b=b|0;ko(a|0,0,12)|0;return}function Yj(a,b){a=a|0;b=b|0;ko(a|0,0,12)|0;return}function Zj(a,b){a=a|0;b=b|0;Ff(a,1,45);return}function _j(a){a=a|0;return 0}function $j(b,c){b=b|0;c=c|0;c=b;C=67109634;a[c]=C;C=C>>8;a[c+1|0]=C;C=C>>8;a[c+2|0]=C;C=C>>8;a[c+3|0]=C;return}function ak(b,c){b=b|0;c=c|0;c=b;C=67109634;a[c]=C;C=C>>8;a[c+1|0]=C;C=C>>8;a[c+2|0]=C;C=C>>8;a[c+3|0]=C;return}function bk(a){a=a|0;Ve(a|0);Tn(a);return}function ck(a){a=a|0;Ve(a|0);return}function dk(b,d,e,f,g,h,j,k){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;var l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0;d=i;i=i+280|0;l=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[l>>2];l=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[l>>2];l=d|0;m=d+16|0;n=d+120|0;o=d+128|0;p=d+136|0;q=d+144|0;r=d+152|0;s=d+160|0;t=d+176|0;u=n|0;c[u>>2]=m;v=n+4|0;c[v>>2]=164;w=m+100|0;Pf(p,h);m=p|0;x=c[m>>2]|0;if((c[8976]|0)!=-1){c[l>>2]=35904;c[l+4>>2]=14;c[l+8>>2]=0;qf(35904,l,96)}l=(c[8977]|0)-1|0;y=c[x+8>>2]|0;do{if((c[x+12>>2]|0)-y>>2>>>0>l>>>0){z=c[y+(l<<2)>>2]|0;if((z|0)==0){break}A=z;a[q]=0;B=f|0;c[r>>2]=c[B>>2];do{if(fk(e,r,g,p,c[h+4>>2]|0,j,q,A,n,o,w)|0){C=s|0;ld[c[(c[z>>2]|0)+32>>2]&15](A,4160,4170,C)|0;D=t|0;E=c[o>>2]|0;F=c[u>>2]|0;G=E-F|0;do{if((G|0)>98){H=Ln(G+2|0)|0;if((H|0)!=0){I=H;J=H;break}Yn();I=0;J=0}else{I=D;J=0}}while(0);if((a[q]&1)==0){K=I}else{a[I]=45;K=I+1|0}if(F>>>0<E>>>0){G=s+10|0;H=s;L=K;M=F;while(1){N=a[M]|0;O=C;while(1){P=O+1|0;if((a[O]|0)==N<<24>>24){Q=O;break}if((P|0)==(G|0)){Q=G;break}else{O=P}}a[L]=a[4160+(Q-H)|0]|0;O=M+1|0;N=L+1|0;if(O>>>0<(c[o>>2]|0)>>>0){L=N;M=O}else{R=N;break}}}else{R=K}a[R]=0;M=xc(D|0,3328,(L=i,i=i+8|0,c[L>>2]=k,L)|0)|0;i=L;if((M|0)==1){if((J|0)==0){break}Mn(J);break}M=Nc(8)|0;bf(M,3272);Qb(M|0,10208,26)}}while(0);A=e|0;z=c[A>>2]|0;do{if((z|0)==0){S=0}else{if((c[z+12>>2]|0)!=(c[z+16>>2]|0)){S=z;break}if((dd[c[(c[z>>2]|0)+36>>2]&127](z)|0)!=-1){S=z;break}c[A>>2]=0;S=0}}while(0);A=(S|0)==0;z=c[B>>2]|0;do{if((z|0)==0){T=45}else{if((c[z+12>>2]|0)!=(c[z+16>>2]|0)){if(A){break}else{T=47;break}}if((dd[c[(c[z>>2]|0)+36>>2]&127](z)|0)==-1){c[B>>2]=0;T=45;break}else{if(A^(z|0)==0){break}else{T=47;break}}}}while(0);if((T|0)==45){if(A){T=47}}if((T|0)==47){c[j>>2]=c[j>>2]|2}c[b>>2]=S;Xe(c[m>>2]|0)|0;z=c[u>>2]|0;c[u>>2]=0;if((z|0)==0){i=d;return}_c[c[v>>2]&511](z);i=d;return}}while(0);d=Nc(4)|0;qn(d);Qb(d|0,10192,134)}function ek(a){a=a|0;return}function fk(e,f,g,h,j,k,l,m,n,o,p){e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;l=l|0;m=m|0;n=n|0;o=o|0;p=p|0;var q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0,Z=0,_=0,$=0,aa=0,ba=0,ca=0,da=0,ea=0,fa=0,ga=0,ha=0,ia=0,ja=0,ka=0,la=0,ma=0,na=0,oa=0,pa=0,qa=0,ra=0,sa=0,ta=0,ua=0,va=0,wa=0,xa=0,ya=0,za=0,Aa=0,Ba=0,Ca=0,Da=0,Ea=0,Fa=0,Ga=0,Ha=0,Ia=0,Ja=0,Ka=0,La=0,Ma=0,Na=0,Oa=0,Pa=0,Qa=0,Ra=0,Sa=0,Ta=0,Ua=0,Va=0,Wa=0,Xa=0,Ya=0,Za=0,_a=0,$a=0,ab=0,bb=0,cb=0,db=0,eb=0,fb=0,gb=0,hb=0,ib=0,jb=0,kb=0,lb=0,mb=0,nb=0,ob=0,pb=0,qb=0,rb=0,sb=0,tb=0,ub=0,vb=0,wb=0,xb=0,yb=0,zb=0,Ab=0,Bb=0,Cb=0,Db=0,Eb=0,Fb=0,Gb=0,Hb=0,Ib=0,Jb=0,Kb=0,Lb=0,Mb=0,Nb=0,Ob=0,Pb=0,Qb=0,Rb=0,Sb=0,Tb=0;q=i;i=i+440|0;r=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[r>>2];r=q|0;s=q+400|0;t=q+408|0;u=q+416|0;v=q+424|0;w=v;x=i;i=i+12|0;i=i+7&-8;y=i;i=i+12|0;i=i+7&-8;z=i;i=i+12|0;i=i+7&-8;A=i;i=i+12|0;i=i+7&-8;B=i;i=i+4|0;i=i+7&-8;C=i;i=i+4|0;i=i+7&-8;D=r|0;c[s>>2]=0;ko(w|0,0,12)|0;E=x;F=y;G=z;H=A;ko(E|0,0,12)|0;ko(F|0,0,12)|0;ko(G|0,0,12)|0;ko(H|0,0,12)|0;jk(g,h,s,t,u,v,x,y,z,B);h=n|0;c[o>>2]=c[h>>2];g=e|0;e=f|0;f=s;s=m+8|0;m=z+1|0;I=z+4|0;J=z+8|0;K=y+1|0;L=y+4|0;M=y+8|0;N=(j&512|0)!=0;j=x+1|0;O=x+4|0;P=x+8|0;Q=A+1|0;R=A+4|0;S=A+8|0;T=f+3|0;U=v+4|0;V=n+4|0;n=p;p=164;W=D;X=D;D=r+400|0;r=0;Y=0;L2:while(1){Z=c[g>>2]|0;do{if((Z|0)==0){_=0}else{if((c[Z+12>>2]|0)!=(c[Z+16>>2]|0)){_=Z;break}if((dd[c[(c[Z>>2]|0)+36>>2]&127](Z)|0)==-1){c[g>>2]=0;_=0;break}else{_=c[g>>2]|0;break}}}while(0);Z=(_|0)==0;$=c[e>>2]|0;do{if(($|0)==0){aa=15}else{if((c[$+12>>2]|0)!=(c[$+16>>2]|0)){if(Z){ba=$;break}else{ca=p;da=W;ea=X;fa=r;aa=274;break L2}}if((dd[c[(c[$>>2]|0)+36>>2]&127]($)|0)==-1){c[e>>2]=0;aa=15;break}else{if(Z){ba=$;break}else{ca=p;da=W;ea=X;fa=r;aa=274;break L2}}}}while(0);if((aa|0)==15){aa=0;if(Z){ca=p;da=W;ea=X;fa=r;aa=274;break}else{ba=0}}L24:do{switch(a[f+Y|0]|0){case 0:{aa=43;break};case 1:{if((Y|0)==3){ca=p;da=W;ea=X;fa=r;aa=274;break L2}$=c[g>>2]|0;ga=c[$+12>>2]|0;if((ga|0)==(c[$+16>>2]|0)){ha=(dd[c[(c[$>>2]|0)+36>>2]&127]($)|0)&255}else{ha=a[ga]|0}ga=ha<<24>>24;if((gc(ga|0)|0)==0){aa=42;break L2}if((b[(c[s>>2]|0)+(ga<<1)>>1]&8192)==0){aa=42;break L2}ga=c[g>>2]|0;$=ga+12|0;ia=c[$>>2]|0;if((ia|0)==(c[ga+16>>2]|0)){ja=(dd[c[(c[ga>>2]|0)+40>>2]&127](ga)|0)&255}else{c[$>>2]=ia+1;ja=a[ia]|0}Af(A,ja);aa=43;break};case 3:{ia=a[F]|0;$=ia&255;ga=($&1|0)==0?$>>>1:c[L>>2]|0;$=a[G]|0;ka=$&255;la=(ka&1|0)==0?ka>>>1:c[I>>2]|0;if((ga|0)==(-la|0)){ma=r;na=D;oa=X;pa=W;qa=p;ra=n;break L24}ka=(ga|0)==0;ga=c[g>>2]|0;sa=c[ga+12>>2]|0;ta=c[ga+16>>2]|0;ua=(sa|0)==(ta|0);if(!(ka|(la|0)==0)){if(ua){la=(dd[c[(c[ga>>2]|0)+36>>2]&127](ga)|0)&255;va=c[g>>2]|0;wa=la;xa=a[F]|0;ya=va;za=c[va+12>>2]|0;Aa=c[va+16>>2]|0}else{wa=a[sa]|0;xa=ia;ya=ga;za=sa;Aa=ta}ta=ya+12|0;va=(za|0)==(Aa|0);if(wa<<24>>24==(a[(xa&1)==0?K:c[M>>2]|0]|0)){if(va){dd[c[(c[ya>>2]|0)+40>>2]&127](ya)|0}else{c[ta>>2]=za+1}ta=d[F]|0;ma=((ta&1|0)==0?ta>>>1:c[L>>2]|0)>>>0>1>>>0?y:r;na=D;oa=X;pa=W;qa=p;ra=n;break L24}if(va){Ba=(dd[c[(c[ya>>2]|0)+36>>2]&127](ya)|0)&255}else{Ba=a[za]|0}if(Ba<<24>>24!=(a[(a[G]&1)==0?m:c[J>>2]|0]|0)){aa=110;break L2}va=c[g>>2]|0;ta=va+12|0;la=c[ta>>2]|0;if((la|0)==(c[va+16>>2]|0)){dd[c[(c[va>>2]|0)+40>>2]&127](va)|0}else{c[ta>>2]=la+1}a[l]=1;la=d[G]|0;ma=((la&1|0)==0?la>>>1:c[I>>2]|0)>>>0>1>>>0?z:r;na=D;oa=X;pa=W;qa=p;ra=n;break L24}if(ka){if(ua){ka=(dd[c[(c[ga>>2]|0)+36>>2]&127](ga)|0)&255;Ca=ka;Da=a[G]|0}else{Ca=a[sa]|0;Da=$}if(Ca<<24>>24!=(a[(Da&1)==0?m:c[J>>2]|0]|0)){ma=r;na=D;oa=X;pa=W;qa=p;ra=n;break L24}$=c[g>>2]|0;ka=$+12|0;la=c[ka>>2]|0;if((la|0)==(c[$+16>>2]|0)){dd[c[(c[$>>2]|0)+40>>2]&127]($)|0}else{c[ka>>2]=la+1}a[l]=1;la=d[G]|0;ma=((la&1|0)==0?la>>>1:c[I>>2]|0)>>>0>1>>>0?z:r;na=D;oa=X;pa=W;qa=p;ra=n;break L24}if(ua){ua=(dd[c[(c[ga>>2]|0)+36>>2]&127](ga)|0)&255;Ea=ua;Fa=a[F]|0}else{Ea=a[sa]|0;Fa=ia}if(Ea<<24>>24!=(a[(Fa&1)==0?K:c[M>>2]|0]|0)){a[l]=1;ma=r;na=D;oa=X;pa=W;qa=p;ra=n;break L24}ia=c[g>>2]|0;sa=ia+12|0;ua=c[sa>>2]|0;if((ua|0)==(c[ia+16>>2]|0)){dd[c[(c[ia>>2]|0)+40>>2]&127](ia)|0}else{c[sa>>2]=ua+1}ua=d[F]|0;ma=((ua&1|0)==0?ua>>>1:c[L>>2]|0)>>>0>1>>>0?y:r;na=D;oa=X;pa=W;qa=p;ra=n;break};case 2:{if(!((r|0)!=0|Y>>>0<2>>>0)){if((Y|0)==2){Ga=(a[T]|0)!=0}else{Ga=0}if(!(N|Ga)){ma=0;na=D;oa=X;pa=W;qa=p;ra=n;break L24}}ua=a[E]|0;sa=c[P>>2]|0;ia=(ua&1)==0?j:sa;L99:do{if((Y|0)==0){Ha=ia;Ia=ua;Ja=sa}else{if((d[f+(Y-1)|0]|0)>>>0>=2>>>0){Ha=ia;Ia=ua;Ja=sa;break}ga=ua&255;L102:do{if((((ga&1|0)==0?ga>>>1:c[O>>2]|0)|0)==0){Ka=ia;La=ua;Ma=sa}else{la=ia;while(1){ka=a[la]|0;if((gc(ka|0)|0)==0){break}if((b[(c[s>>2]|0)+(ka<<1)>>1]&8192)==0){break}ka=la+1|0;$=a[E]|0;ta=c[P>>2]|0;va=$&255;if((ka|0)==((($&1)==0?j:ta)+((va&1|0)==0?va>>>1:c[O>>2]|0)|0)){Ka=ka;La=$;Ma=ta;break L102}else{la=ka}}Ka=la;La=a[E]|0;Ma=c[P>>2]|0}}while(0);ga=(La&1)==0?j:Ma;ka=Ka-ga|0;ta=a[H]|0;$=ta&255;va=($&1|0)==0?$>>>1:c[R>>2]|0;if(ka>>>0>va>>>0){Ha=ga;Ia=La;Ja=Ma;break}$=(ta&1)==0?Q:c[S>>2]|0;ta=$+va|0;if((Ka|0)==(ga|0)){Ha=Ka;Ia=La;Ja=Ma;break}Na=$+(va-ka)|0;ka=ga;while(1){if((a[Na]|0)!=(a[ka]|0)){Ha=ga;Ia=La;Ja=Ma;break L99}va=Na+1|0;if((va|0)==(ta|0)){Ha=Ka;Ia=La;Ja=Ma;break}else{Na=va;ka=ka+1|0}}}}while(0);ia=Ia&255;L116:do{if((Ha|0)==(((Ia&1)==0?j:Ja)+((ia&1|0)==0?ia>>>1:c[O>>2]|0)|0)){Oa=Ha}else{sa=ba;ua=Ha;while(1){ka=c[g>>2]|0;do{if((ka|0)==0){Pa=0}else{if((c[ka+12>>2]|0)!=(c[ka+16>>2]|0)){Pa=ka;break}if((dd[c[(c[ka>>2]|0)+36>>2]&127](ka)|0)==-1){c[g>>2]=0;Pa=0;break}else{Pa=c[g>>2]|0;break}}}while(0);ka=(Pa|0)==0;do{if((sa|0)==0){aa=141}else{if((c[sa+12>>2]|0)!=(c[sa+16>>2]|0)){if(ka){Qa=sa;break}else{Oa=ua;break L116}}if((dd[c[(c[sa>>2]|0)+36>>2]&127](sa)|0)==-1){c[e>>2]=0;aa=141;break}else{if(ka){Qa=sa;break}else{Oa=ua;break L116}}}}while(0);if((aa|0)==141){aa=0;if(ka){Oa=ua;break L116}else{Qa=0}}la=c[g>>2]|0;Na=c[la+12>>2]|0;if((Na|0)==(c[la+16>>2]|0)){Ra=(dd[c[(c[la>>2]|0)+36>>2]&127](la)|0)&255}else{Ra=a[Na]|0}if(Ra<<24>>24!=(a[ua]|0)){Oa=ua;break L116}Na=c[g>>2]|0;la=Na+12|0;ta=c[la>>2]|0;if((ta|0)==(c[Na+16>>2]|0)){dd[c[(c[Na>>2]|0)+40>>2]&127](Na)|0}else{c[la>>2]=ta+1}ta=ua+1|0;la=a[E]|0;Na=la&255;if((ta|0)==(((la&1)==0?j:c[P>>2]|0)+((Na&1|0)==0?Na>>>1:c[O>>2]|0)|0)){Oa=ta;break}else{sa=Qa;ua=ta}}}}while(0);if(!N){ma=r;na=D;oa=X;pa=W;qa=p;ra=n;break L24}ia=a[E]|0;ua=ia&255;if((Oa|0)==(((ia&1)==0?j:c[P>>2]|0)+((ua&1|0)==0?ua>>>1:c[O>>2]|0)|0)){ma=r;na=D;oa=X;pa=W;qa=p;ra=n}else{aa=154;break L2}break};case 4:{ua=0;ia=D;sa=X;ta=W;Na=p;la=n;L151:while(1){ga=c[g>>2]|0;do{if((ga|0)==0){Sa=0}else{if((c[ga+12>>2]|0)!=(c[ga+16>>2]|0)){Sa=ga;break}if((dd[c[(c[ga>>2]|0)+36>>2]&127](ga)|0)==-1){c[g>>2]=0;Sa=0;break}else{Sa=c[g>>2]|0;break}}}while(0);ga=(Sa|0)==0;va=c[e>>2]|0;do{if((va|0)==0){aa=167}else{if((c[va+12>>2]|0)!=(c[va+16>>2]|0)){if(ga){break}else{break L151}}if((dd[c[(c[va>>2]|0)+36>>2]&127](va)|0)==-1){c[e>>2]=0;aa=167;break}else{if(ga){break}else{break L151}}}}while(0);if((aa|0)==167){aa=0;if(ga){break}}va=c[g>>2]|0;$=c[va+12>>2]|0;if(($|0)==(c[va+16>>2]|0)){Ta=(dd[c[(c[va>>2]|0)+36>>2]&127](va)|0)&255}else{Ta=a[$]|0}$=Ta<<24>>24;do{if((gc($|0)|0)==0){aa=187}else{if((b[(c[s>>2]|0)+($<<1)>>1]&2048)==0){aa=187;break}va=c[o>>2]|0;if((va|0)==(la|0)){Ua=(c[V>>2]|0)!=164;Va=c[h>>2]|0;Wa=la-Va|0;Xa=Wa>>>0<2147483647>>>0?Wa<<1:-1;Ya=On(Ua?Va:0,Xa)|0;if((Ya|0)==0){Yn()}do{if(Ua){c[h>>2]=Ya;Za=Ya}else{Va=c[h>>2]|0;c[h>>2]=Ya;if((Va|0)==0){Za=Ya;break}_c[c[V>>2]&511](Va);Za=c[h>>2]|0}}while(0);c[V>>2]=80;Ya=Za+Wa|0;c[o>>2]=Ya;_a=(c[h>>2]|0)+Xa|0;$a=Ya}else{_a=la;$a=va}c[o>>2]=$a+1;a[$a]=Ta;ab=ua+1|0;bb=ia;cb=sa;db=ta;eb=Na;fb=_a}}while(0);if((aa|0)==187){aa=0;$=d[w]|0;if(((($&1|0)==0?$>>>1:c[U>>2]|0)|0)==0|(ua|0)==0){break}if(Ta<<24>>24!=(a[u]|0)){break}if((sa|0)==(ia|0)){$=sa-ta|0;ga=$>>>0<2147483647>>>0?$<<1:-1;if((Na|0)==164){gb=0}else{gb=ta}Ya=On(gb,ga)|0;Ua=Ya;if((Ya|0)==0){Yn()}hb=Ua+(ga>>>2<<2)|0;ib=Ua+($>>2<<2)|0;jb=Ua;kb=80}else{hb=ia;ib=sa;jb=ta;kb=Na}c[ib>>2]=ua;ab=0;bb=hb;cb=ib+4|0;db=jb;eb=kb;fb=la}Ua=c[g>>2]|0;$=Ua+12|0;ga=c[$>>2]|0;if((ga|0)==(c[Ua+16>>2]|0)){dd[c[(c[Ua>>2]|0)+40>>2]&127](Ua)|0;ua=ab;ia=bb;sa=cb;ta=db;Na=eb;la=fb;continue}else{c[$>>2]=ga+1;ua=ab;ia=bb;sa=cb;ta=db;Na=eb;la=fb;continue}}if((ta|0)==(sa|0)|(ua|0)==0){lb=ia;mb=sa;nb=ta;ob=Na}else{if((sa|0)==(ia|0)){ga=sa-ta|0;$=ga>>>0<2147483647>>>0?ga<<1:-1;if((Na|0)==164){pb=0}else{pb=ta}Ua=On(pb,$)|0;Ya=Ua;if((Ua|0)==0){Yn()}qb=Ya+($>>>2<<2)|0;rb=Ya+(ga>>2<<2)|0;sb=Ya;tb=80}else{qb=ia;rb=sa;sb=ta;tb=Na}c[rb>>2]=ua;lb=qb;mb=rb+4|0;nb=sb;ob=tb}if((c[B>>2]|0)>0){Ya=c[g>>2]|0;do{if((Ya|0)==0){ub=0}else{if((c[Ya+12>>2]|0)!=(c[Ya+16>>2]|0)){ub=Ya;break}if((dd[c[(c[Ya>>2]|0)+36>>2]&127](Ya)|0)==-1){c[g>>2]=0;ub=0;break}else{ub=c[g>>2]|0;break}}}while(0);Ya=(ub|0)==0;ua=c[e>>2]|0;do{if((ua|0)==0){aa=220}else{if((c[ua+12>>2]|0)!=(c[ua+16>>2]|0)){if(Ya){vb=ua;break}else{aa=227;break L2}}if((dd[c[(c[ua>>2]|0)+36>>2]&127](ua)|0)==-1){c[e>>2]=0;aa=220;break}else{if(Ya){vb=ua;break}else{aa=227;break L2}}}}while(0);if((aa|0)==220){aa=0;if(Ya){aa=227;break L2}else{vb=0}}ua=c[g>>2]|0;Na=c[ua+12>>2]|0;if((Na|0)==(c[ua+16>>2]|0)){wb=(dd[c[(c[ua>>2]|0)+36>>2]&127](ua)|0)&255}else{wb=a[Na]|0}if(wb<<24>>24!=(a[t]|0)){aa=227;break L2}Na=c[g>>2]|0;ua=Na+12|0;ta=c[ua>>2]|0;if((ta|0)==(c[Na+16>>2]|0)){dd[c[(c[Na>>2]|0)+40>>2]&127](Na)|0;xb=la;yb=vb}else{c[ua>>2]=ta+1;xb=la;yb=vb}while(1){ta=c[g>>2]|0;do{if((ta|0)==0){zb=0}else{if((c[ta+12>>2]|0)!=(c[ta+16>>2]|0)){zb=ta;break}if((dd[c[(c[ta>>2]|0)+36>>2]&127](ta)|0)==-1){c[g>>2]=0;zb=0;break}else{zb=c[g>>2]|0;break}}}while(0);ta=(zb|0)==0;do{if((yb|0)==0){aa=243}else{if((c[yb+12>>2]|0)!=(c[yb+16>>2]|0)){if(ta){Ab=yb;break}else{aa=252;break L2}}if((dd[c[(c[yb>>2]|0)+36>>2]&127](yb)|0)==-1){c[e>>2]=0;aa=243;break}else{if(ta){Ab=yb;break}else{aa=252;break L2}}}}while(0);if((aa|0)==243){aa=0;if(ta){aa=252;break L2}else{Ab=0}}ua=c[g>>2]|0;Na=c[ua+12>>2]|0;if((Na|0)==(c[ua+16>>2]|0)){Bb=(dd[c[(c[ua>>2]|0)+36>>2]&127](ua)|0)&255}else{Bb=a[Na]|0}Na=Bb<<24>>24;if((gc(Na|0)|0)==0){aa=252;break L2}if((b[(c[s>>2]|0)+(Na<<1)>>1]&2048)==0){aa=252;break L2}Na=c[o>>2]|0;if((Na|0)==(xb|0)){ua=(c[V>>2]|0)!=164;sa=c[h>>2]|0;ia=xb-sa|0;ga=ia>>>0<2147483647>>>0?ia<<1:-1;$=On(ua?sa:0,ga)|0;if(($|0)==0){Yn()}do{if(ua){c[h>>2]=$;Cb=$}else{sa=c[h>>2]|0;c[h>>2]=$;if((sa|0)==0){Cb=$;break}_c[c[V>>2]&511](sa);Cb=c[h>>2]|0}}while(0);c[V>>2]=80;$=Cb+ia|0;c[o>>2]=$;Db=(c[h>>2]|0)+ga|0;Eb=$}else{Db=xb;Eb=Na}$=c[g>>2]|0;ua=c[$+12>>2]|0;if((ua|0)==(c[$+16>>2]|0)){ta=(dd[c[(c[$>>2]|0)+36>>2]&127]($)|0)&255;Fb=ta;Gb=c[o>>2]|0}else{Fb=a[ua]|0;Gb=Eb}c[o>>2]=Gb+1;a[Gb]=Fb;ua=(c[B>>2]|0)-1|0;c[B>>2]=ua;ta=c[g>>2]|0;$=ta+12|0;sa=c[$>>2]|0;if((sa|0)==(c[ta+16>>2]|0)){dd[c[(c[ta>>2]|0)+40>>2]&127](ta)|0}else{c[$>>2]=sa+1}if((ua|0)>0){xb=Db;yb=Ab}else{Hb=Db;break}}}else{Hb=la}if((c[o>>2]|0)==(c[h>>2]|0)){aa=272;break L2}else{ma=r;na=lb;oa=mb;pa=nb;qa=ob;ra=Hb}break};default:{ma=r;na=D;oa=X;pa=W;qa=p;ra=n}}}while(0);L307:do{if((aa|0)==43){aa=0;if((Y|0)==3){ca=p;da=W;ea=X;fa=r;aa=274;break L2}else{Ib=ba}while(1){Z=c[g>>2]|0;do{if((Z|0)==0){Jb=0}else{if((c[Z+12>>2]|0)!=(c[Z+16>>2]|0)){Jb=Z;break}if((dd[c[(c[Z>>2]|0)+36>>2]&127](Z)|0)==-1){c[g>>2]=0;Jb=0;break}else{Jb=c[g>>2]|0;break}}}while(0);Z=(Jb|0)==0;do{if((Ib|0)==0){aa=56}else{if((c[Ib+12>>2]|0)!=(c[Ib+16>>2]|0)){if(Z){Kb=Ib;break}else{ma=r;na=D;oa=X;pa=W;qa=p;ra=n;break L307}}if((dd[c[(c[Ib>>2]|0)+36>>2]&127](Ib)|0)==-1){c[e>>2]=0;aa=56;break}else{if(Z){Kb=Ib;break}else{ma=r;na=D;oa=X;pa=W;qa=p;ra=n;break L307}}}}while(0);if((aa|0)==56){aa=0;if(Z){ma=r;na=D;oa=X;pa=W;qa=p;ra=n;break L307}else{Kb=0}}Na=c[g>>2]|0;ga=c[Na+12>>2]|0;if((ga|0)==(c[Na+16>>2]|0)){Lb=(dd[c[(c[Na>>2]|0)+36>>2]&127](Na)|0)&255}else{Lb=a[ga]|0}ga=Lb<<24>>24;if((gc(ga|0)|0)==0){ma=r;na=D;oa=X;pa=W;qa=p;ra=n;break L307}if((b[(c[s>>2]|0)+(ga<<1)>>1]&8192)==0){ma=r;na=D;oa=X;pa=W;qa=p;ra=n;break L307}ga=c[g>>2]|0;Na=ga+12|0;ia=c[Na>>2]|0;if((ia|0)==(c[ga+16>>2]|0)){Mb=(dd[c[(c[ga>>2]|0)+40>>2]&127](ga)|0)&255}else{c[Na>>2]=ia+1;Mb=a[ia]|0}Af(A,Mb);Ib=Kb}}}while(0);la=Y+1|0;if(la>>>0<4>>>0){n=ra;p=qa;W=pa;X=oa;D=na;r=ma;Y=la}else{ca=qa;da=pa;ea=oa;fa=ma;aa=274;break}}L345:do{if((aa|0)==42){c[k>>2]=c[k>>2]|4;Nb=0;Ob=W;Pb=p}else if((aa|0)==110){c[k>>2]=c[k>>2]|4;Nb=0;Ob=W;Pb=p}else if((aa|0)==154){c[k>>2]=c[k>>2]|4;Nb=0;Ob=W;Pb=p}else if((aa|0)==227){c[k>>2]=c[k>>2]|4;Nb=0;Ob=nb;Pb=ob}else if((aa|0)==252){c[k>>2]=c[k>>2]|4;Nb=0;Ob=nb;Pb=ob}else if((aa|0)==272){c[k>>2]=c[k>>2]|4;Nb=0;Ob=nb;Pb=ob}else if((aa|0)==274){L353:do{if((fa|0)!=0){ma=fa;oa=fa+1|0;pa=fa+8|0;qa=fa+4|0;Y=1;L355:while(1){r=d[ma]|0;if((r&1|0)==0){Qb=r>>>1}else{Qb=c[qa>>2]|0}if(Y>>>0>=Qb>>>0){break L353}r=c[g>>2]|0;do{if((r|0)==0){Rb=0}else{if((c[r+12>>2]|0)!=(c[r+16>>2]|0)){Rb=r;break}if((dd[c[(c[r>>2]|0)+36>>2]&127](r)|0)==-1){c[g>>2]=0;Rb=0;break}else{Rb=c[g>>2]|0;break}}}while(0);r=(Rb|0)==0;Z=c[e>>2]|0;do{if((Z|0)==0){aa=292}else{if((c[Z+12>>2]|0)!=(c[Z+16>>2]|0)){if(r){break}else{break L355}}if((dd[c[(c[Z>>2]|0)+36>>2]&127](Z)|0)==-1){c[e>>2]=0;aa=292;break}else{if(r){break}else{break L355}}}}while(0);if((aa|0)==292){aa=0;if(r){break}}Z=c[g>>2]|0;na=c[Z+12>>2]|0;if((na|0)==(c[Z+16>>2]|0)){Sb=(dd[c[(c[Z>>2]|0)+36>>2]&127](Z)|0)&255}else{Sb=a[na]|0}if((a[ma]&1)==0){Tb=oa}else{Tb=c[pa>>2]|0}if(Sb<<24>>24!=(a[Tb+Y|0]|0)){break}na=Y+1|0;Z=c[g>>2]|0;D=Z+12|0;X=c[D>>2]|0;if((X|0)==(c[Z+16>>2]|0)){dd[c[(c[Z>>2]|0)+40>>2]&127](Z)|0;Y=na;continue}else{c[D>>2]=X+1;Y=na;continue}}c[k>>2]=c[k>>2]|4;Nb=0;Ob=da;Pb=ca;break L345}}while(0);if((da|0)==(ea|0)){Nb=1;Ob=ea;Pb=ca;break}c[C>>2]=0;kk(v,da,ea,C);if((c[C>>2]|0)==0){Nb=1;Ob=da;Pb=ca;break}c[k>>2]=c[k>>2]|4;Nb=0;Ob=da;Pb=ca}}while(0);vf(A);vf(z);vf(y);vf(x);vf(v);if((Ob|0)==0){i=q;return Nb|0}_c[Pb&511](Ob);i=q;return Nb|0}function gk(a){a=a|0;var b=0;b=Nc(8)|0;bf(b,a);Qb(b|0,10208,26)}function hk(b,d,e,f,g,h,j,k){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;var l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0;d=i;i=i+160|0;l=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[l>>2];l=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[l>>2];l=d|0;m=d+16|0;n=d+120|0;o=d+128|0;p=d+136|0;q=d+144|0;r=d+152|0;s=n|0;c[s>>2]=m;t=n+4|0;c[t>>2]=164;u=m+100|0;Pf(p,h);m=p|0;v=c[m>>2]|0;if((c[8976]|0)!=-1){c[l>>2]=35904;c[l+4>>2]=14;c[l+8>>2]=0;qf(35904,l,96)}l=(c[8977]|0)-1|0;w=c[v+8>>2]|0;do{if((c[v+12>>2]|0)-w>>2>>>0>l>>>0){x=c[w+(l<<2)>>2]|0;if((x|0)==0){break}y=x;a[q]=0;z=f|0;A=c[z>>2]|0;c[r>>2]=A;if(fk(e,r,g,p,c[h+4>>2]|0,j,q,y,n,o,u)|0){B=k;if((a[B]&1)==0){a[k+1|0]=0;a[B]=0}else{a[c[k+8>>2]|0]=0;c[k+4>>2]=0}B=x;if((a[q]&1)!=0){Af(k,ad[c[(c[B>>2]|0)+28>>2]&31](y,45)|0)}x=ad[c[(c[B>>2]|0)+28>>2]&31](y,48)|0;y=c[s>>2]|0;B=c[o>>2]|0;C=B-1|0;L20:do{if(y>>>0<C>>>0){D=y;while(1){E=D+1|0;if((a[D]|0)!=x<<24>>24){F=D;break L20}if(E>>>0<C>>>0){D=E}else{F=E;break}}}else{F=y}}while(0);ik(k,F,B)|0}y=e|0;C=c[y>>2]|0;do{if((C|0)==0){G=0}else{if((c[C+12>>2]|0)!=(c[C+16>>2]|0)){G=C;break}if((dd[c[(c[C>>2]|0)+36>>2]&127](C)|0)!=-1){G=C;break}c[y>>2]=0;G=0}}while(0);y=(G|0)==0;do{if((A|0)==0){H=33}else{if((c[A+12>>2]|0)!=(c[A+16>>2]|0)){if(y){break}else{H=35;break}}if((dd[c[(c[A>>2]|0)+36>>2]&127](A)|0)==-1){c[z>>2]=0;H=33;break}else{if(y^(A|0)==0){break}else{H=35;break}}}}while(0);if((H|0)==33){if(y){H=35}}if((H|0)==35){c[j>>2]=c[j>>2]|2}c[b>>2]=G;Xe(c[m>>2]|0)|0;A=c[s>>2]|0;c[s>>2]=0;if((A|0)==0){i=d;return}_c[c[t>>2]&511](A);i=d;return}}while(0);d=Nc(4)|0;qn(d);Qb(d|0,10192,134)}function ik(b,d,e){b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0,i=0,j=0,k=0,l=0,m=0,n=0;f=b;g=d;h=a[f]|0;i=h&255;if((i&1|0)==0){j=i>>>1}else{j=c[b+4>>2]|0}if((h&1)==0){k=10;l=h}else{h=c[b>>2]|0;k=(h&-2)-1|0;l=h&255}h=e-g|0;if((e|0)==(d|0)){return b|0}if((k-j|0)>>>0<h>>>0){Df(b,k,j+h-k|0,j,j,0,0);m=a[f]|0}else{m=l}if((m&1)==0){n=b+1|0}else{n=c[b+8>>2]|0}m=e+(j-g)|0;g=d;d=n+j|0;while(1){a[d]=a[g]|0;l=g+1|0;if((l|0)==(e|0)){break}else{g=l;d=d+1|0}}a[n+m|0]=0;m=j+h|0;if((a[f]&1)==0){a[f]=m<<1;return b|0}else{c[b+4>>2]=m;return b|0}return 0}function jk(b,d,e,f,g,h,j,k,l,m){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;l=l|0;m=m|0;var n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0;n=i;i=i+56|0;o=n|0;p=n+16|0;q=n+32|0;r=n+40|0;s=r;t=i;i=i+12|0;i=i+7&-8;u=t;v=i;i=i+12|0;i=i+7&-8;w=v;x=i;i=i+12|0;i=i+7&-8;y=x;z=i;i=i+4|0;i=i+7&-8;A=i;i=i+12|0;i=i+7&-8;B=A;D=i;i=i+12|0;i=i+7&-8;E=D;F=i;i=i+12|0;i=i+7&-8;G=F;H=i;i=i+12|0;i=i+7&-8;I=H;if(b){b=c[d>>2]|0;if((c[9094]|0)!=-1){c[p>>2]=36376;c[p+4>>2]=14;c[p+8>>2]=0;qf(36376,p,96)}p=(c[9095]|0)-1|0;J=c[b+8>>2]|0;if((c[b+12>>2]|0)-J>>2>>>0<=p>>>0){K=Nc(4)|0;L=K;qn(L);Qb(K|0,10192,134)}b=c[J+(p<<2)>>2]|0;if((b|0)==0){K=Nc(4)|0;L=K;qn(L);Qb(K|0,10192,134)}K=b;$c[c[(c[b>>2]|0)+44>>2]&127](q,K);L=e;C=c[q>>2]|0;a[L]=C;C=C>>8;a[L+1|0]=C;C=C>>8;a[L+2|0]=C;C=C>>8;a[L+3|0]=C;L=b;$c[c[(c[L>>2]|0)+32>>2]&127](r,K);q=l;if((a[q]&1)==0){a[l+1|0]=0;a[q]=0}else{a[c[l+8>>2]|0]=0;c[l+4>>2]=0}zf(l,0);c[q>>2]=c[s>>2];c[q+4>>2]=c[s+4>>2];c[q+8>>2]=c[s+8>>2];ko(s|0,0,12)|0;vf(r);$c[c[(c[L>>2]|0)+28>>2]&127](t,K);r=k;if((a[r]&1)==0){a[k+1|0]=0;a[r]=0}else{a[c[k+8>>2]|0]=0;c[k+4>>2]=0}zf(k,0);c[r>>2]=c[u>>2];c[r+4>>2]=c[u+4>>2];c[r+8>>2]=c[u+8>>2];ko(u|0,0,12)|0;vf(t);t=b;a[f]=dd[c[(c[t>>2]|0)+12>>2]&127](K)|0;a[g]=dd[c[(c[t>>2]|0)+16>>2]&127](K)|0;$c[c[(c[L>>2]|0)+20>>2]&127](v,K);t=h;if((a[t]&1)==0){a[h+1|0]=0;a[t]=0}else{a[c[h+8>>2]|0]=0;c[h+4>>2]=0}zf(h,0);c[t>>2]=c[w>>2];c[t+4>>2]=c[w+4>>2];c[t+8>>2]=c[w+8>>2];ko(w|0,0,12)|0;vf(v);$c[c[(c[L>>2]|0)+24>>2]&127](x,K);L=j;if((a[L]&1)==0){a[j+1|0]=0;a[L]=0}else{a[c[j+8>>2]|0]=0;c[j+4>>2]=0}zf(j,0);c[L>>2]=c[y>>2];c[L+4>>2]=c[y+4>>2];c[L+8>>2]=c[y+8>>2];ko(y|0,0,12)|0;vf(x);M=dd[c[(c[b>>2]|0)+36>>2]&127](K)|0;c[m>>2]=M;i=n;return}else{K=c[d>>2]|0;if((c[9096]|0)!=-1){c[o>>2]=36384;c[o+4>>2]=14;c[o+8>>2]=0;qf(36384,o,96)}o=(c[9097]|0)-1|0;d=c[K+8>>2]|0;if((c[K+12>>2]|0)-d>>2>>>0<=o>>>0){N=Nc(4)|0;O=N;qn(O);Qb(N|0,10192,134)}K=c[d+(o<<2)>>2]|0;if((K|0)==0){N=Nc(4)|0;O=N;qn(O);Qb(N|0,10192,134)}N=K;$c[c[(c[K>>2]|0)+44>>2]&127](z,N);O=e;C=c[z>>2]|0;a[O]=C;C=C>>8;a[O+1|0]=C;C=C>>8;a[O+2|0]=C;C=C>>8;a[O+3|0]=C;O=K;$c[c[(c[O>>2]|0)+32>>2]&127](A,N);z=l;if((a[z]&1)==0){a[l+1|0]=0;a[z]=0}else{a[c[l+8>>2]|0]=0;c[l+4>>2]=0}zf(l,0);c[z>>2]=c[B>>2];c[z+4>>2]=c[B+4>>2];c[z+8>>2]=c[B+8>>2];ko(B|0,0,12)|0;vf(A);$c[c[(c[O>>2]|0)+28>>2]&127](D,N);A=k;if((a[A]&1)==0){a[k+1|0]=0;a[A]=0}else{a[c[k+8>>2]|0]=0;c[k+4>>2]=0}zf(k,0);c[A>>2]=c[E>>2];c[A+4>>2]=c[E+4>>2];c[A+8>>2]=c[E+8>>2];ko(E|0,0,12)|0;vf(D);D=K;a[f]=dd[c[(c[D>>2]|0)+12>>2]&127](N)|0;a[g]=dd[c[(c[D>>2]|0)+16>>2]&127](N)|0;$c[c[(c[O>>2]|0)+20>>2]&127](F,N);D=h;if((a[D]&1)==0){a[h+1|0]=0;a[D]=0}else{a[c[h+8>>2]|0]=0;c[h+4>>2]=0}zf(h,0);c[D>>2]=c[G>>2];c[D+4>>2]=c[G+4>>2];c[D+8>>2]=c[G+8>>2];ko(G|0,0,12)|0;vf(F);$c[c[(c[O>>2]|0)+24>>2]&127](H,N);O=j;if((a[O]&1)==0){a[j+1|0]=0;a[O]=0}else{a[c[j+8>>2]|0]=0;c[j+4>>2]=0}zf(j,0);c[O>>2]=c[I>>2];c[O+4>>2]=c[I+4>>2];c[O+8>>2]=c[I+8>>2];ko(I|0,0,12)|0;vf(H);M=dd[c[(c[K>>2]|0)+36>>2]&127](N)|0;c[m>>2]=M;i=n;return}}function kk(b,d,e,f){b=b|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,i=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0;g=b;h=b;i=a[h]|0;j=i&255;if((j&1|0)==0){k=j>>>1}else{k=c[b+4>>2]|0}if((k|0)==0){return}do{if((d|0)==(e|0)){l=i}else{k=e-4|0;if(k>>>0>d>>>0){m=d;n=k}else{l=i;break}do{k=c[m>>2]|0;c[m>>2]=c[n>>2];c[n>>2]=k;m=m+4|0;n=n-4|0;}while(m>>>0<n>>>0);l=a[h]|0}}while(0);if((l&1)==0){o=g+1|0}else{o=c[b+8>>2]|0}g=l&255;if((g&1|0)==0){p=g>>>1}else{p=c[b+4>>2]|0}b=e-4|0;e=a[o]|0;g=e<<24>>24;l=e<<24>>24<1|e<<24>>24==127;L22:do{if(b>>>0>d>>>0){e=o+p|0;h=o;n=d;m=g;i=l;while(1){if(!i){if((m|0)!=(c[n>>2]|0)){break}}k=(e-h|0)>1?h+1|0:h;j=n+4|0;q=a[k]|0;r=q<<24>>24;s=q<<24>>24<1|q<<24>>24==127;if(j>>>0<b>>>0){h=k;n=j;m=r;i=s}else{t=r;u=s;break L22}}c[f>>2]=4;return}else{t=g;u=l}}while(0);if(u){return}u=c[b>>2]|0;if(!(t>>>0<u>>>0|(u|0)==0)){return}c[f>>2]=4;return}function lk(a){a=a|0;Ve(a|0);Tn(a);return}function mk(a){a=a|0;Ve(a|0);return}function nk(b,d,e,f,g,h,j,k){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;var l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0;d=i;i=i+600|0;l=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[l>>2];l=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[l>>2];l=d|0;m=d+16|0;n=d+416|0;o=d+424|0;p=d+432|0;q=d+440|0;r=d+448|0;s=d+456|0;t=d+496|0;u=n|0;c[u>>2]=m;v=n+4|0;c[v>>2]=164;w=m+400|0;Pf(p,h);m=p|0;x=c[m>>2]|0;if((c[8974]|0)!=-1){c[l>>2]=35896;c[l+4>>2]=14;c[l+8>>2]=0;qf(35896,l,96)}l=(c[8975]|0)-1|0;y=c[x+8>>2]|0;do{if((c[x+12>>2]|0)-y>>2>>>0>l>>>0){z=c[y+(l<<2)>>2]|0;if((z|0)==0){break}A=z;a[q]=0;B=f|0;c[r>>2]=c[B>>2];do{if(ok(e,r,g,p,c[h+4>>2]|0,j,q,A,n,o,w)|0){C=s|0;ld[c[(c[z>>2]|0)+48>>2]&15](A,4144,4154,C)|0;D=t|0;E=c[o>>2]|0;F=c[u>>2]|0;G=E-F|0;do{if((G|0)>392){H=Ln((G>>2)+2|0)|0;if((H|0)!=0){I=H;J=H;break}Yn();I=0;J=0}else{I=D;J=0}}while(0);if((a[q]&1)==0){K=I}else{a[I]=45;K=I+1|0}if(F>>>0<E>>>0){G=s+40|0;H=s;L=K;M=F;while(1){N=c[M>>2]|0;O=C;while(1){P=O+4|0;if((c[O>>2]|0)==(N|0)){Q=O;break}if((P|0)==(G|0)){Q=G;break}else{O=P}}a[L]=a[4144+(Q-H>>2)|0]|0;O=M+4|0;N=L+1|0;if(O>>>0<(c[o>>2]|0)>>>0){L=N;M=O}else{R=N;break}}}else{R=K}a[R]=0;M=xc(D|0,3328,(L=i,i=i+8|0,c[L>>2]=k,L)|0)|0;i=L;if((M|0)==1){if((J|0)==0){break}Mn(J);break}M=Nc(8)|0;bf(M,3272);Qb(M|0,10208,26)}}while(0);A=e|0;z=c[A>>2]|0;do{if((z|0)==0){S=0}else{M=c[z+12>>2]|0;if((M|0)==(c[z+16>>2]|0)){T=dd[c[(c[z>>2]|0)+36>>2]&127](z)|0}else{T=c[M>>2]|0}if((T|0)!=-1){S=z;break}c[A>>2]=0;S=0}}while(0);A=(S|0)==0;z=c[B>>2]|0;do{if((z|0)==0){U=46}else{M=c[z+12>>2]|0;if((M|0)==(c[z+16>>2]|0)){V=dd[c[(c[z>>2]|0)+36>>2]&127](z)|0}else{V=c[M>>2]|0}if((V|0)==-1){c[B>>2]=0;U=46;break}else{if(A^(z|0)==0){break}else{U=48;break}}}}while(0);if((U|0)==46){if(A){U=48}}if((U|0)==48){c[j>>2]=c[j>>2]|2}c[b>>2]=S;Xe(c[m>>2]|0)|0;z=c[u>>2]|0;c[u>>2]=0;if((z|0)==0){i=d;return}_c[c[v>>2]&511](z);i=d;return}}while(0);d=Nc(4)|0;qn(d);Qb(d|0,10192,134)}function ok(b,e,f,g,h,j,k,l,m,n,o){b=b|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;l=l|0;m=m|0;n=n|0;o=o|0;var p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0,Z=0,_=0,$=0,aa=0,ba=0,ca=0,da=0,ea=0,fa=0,ga=0,ha=0,ia=0,ja=0,ka=0,la=0,ma=0,na=0,oa=0,pa=0,qa=0,ra=0,sa=0,ta=0,ua=0,va=0,wa=0,xa=0,ya=0,za=0,Aa=0,Ba=0,Ca=0,Da=0,Ea=0,Fa=0,Ga=0,Ha=0,Ia=0,Ja=0,Ka=0,La=0,Ma=0,Na=0,Oa=0,Pa=0,Qa=0,Ra=0,Sa=0,Ta=0,Ua=0,Va=0,Wa=0,Xa=0,Ya=0,Za=0,_a=0,$a=0,ab=0,bb=0,cb=0,db=0,eb=0,fb=0,gb=0,hb=0,ib=0,jb=0,kb=0,lb=0,mb=0,nb=0,ob=0,pb=0,qb=0,rb=0,sb=0,tb=0,ub=0,vb=0,wb=0,xb=0,yb=0,zb=0,Ab=0,Bb=0,Cb=0,Db=0,Eb=0,Fb=0,Gb=0,Hb=0,Ib=0,Jb=0,Kb=0,Lb=0,Mb=0,Nb=0,Ob=0,Pb=0,Qb=0;p=i;i=i+448|0;q=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[q>>2];q=p|0;r=p+8|0;s=p+408|0;t=p+416|0;u=p+424|0;v=p+432|0;w=v;x=i;i=i+12|0;i=i+7&-8;y=i;i=i+12|0;i=i+7&-8;z=i;i=i+12|0;i=i+7&-8;A=i;i=i+12|0;i=i+7&-8;B=i;i=i+4|0;i=i+7&-8;C=i;i=i+4|0;i=i+7&-8;c[q>>2]=o;o=r|0;c[s>>2]=0;ko(w|0,0,12)|0;D=x;E=y;F=z;G=A;ko(D|0,0,12)|0;ko(E|0,0,12)|0;ko(F|0,0,12)|0;ko(G|0,0,12)|0;rk(f,g,s,t,u,v,x,y,z,B);g=m|0;c[n>>2]=c[g>>2];f=b|0;b=e|0;e=s;s=l;H=z+4|0;I=z+8|0;J=y+4|0;K=y+8|0;L=(h&512|0)!=0;h=x+4|0;M=x+8|0;N=A+4|0;O=A+8|0;P=e+3|0;Q=v+4|0;R=164;S=o;T=o;o=r+400|0;r=0;U=0;L2:while(1){V=c[f>>2]|0;do{if((V|0)==0){W=1}else{X=c[V+12>>2]|0;if((X|0)==(c[V+16>>2]|0)){Y=dd[c[(c[V>>2]|0)+36>>2]&127](V)|0}else{Y=c[X>>2]|0}if((Y|0)==-1){c[f>>2]=0;W=1;break}else{W=(c[f>>2]|0)==0;break}}}while(0);V=c[b>>2]|0;do{if((V|0)==0){Z=16}else{X=c[V+12>>2]|0;if((X|0)==(c[V+16>>2]|0)){_=dd[c[(c[V>>2]|0)+36>>2]&127](V)|0}else{_=c[X>>2]|0}if((_|0)==-1){c[b>>2]=0;Z=16;break}else{if(W^(V|0)==0){$=V;break}else{aa=R;ba=S;ca=T;da=r;Z=256;break L2}}}}while(0);if((Z|0)==16){Z=0;if(W){aa=R;ba=S;ca=T;da=r;Z=256;break}else{$=0}}L26:do{switch(a[e+U|0]|0){case 4:{V=0;X=o;ea=T;fa=S;ga=R;L27:while(1){ha=c[f>>2]|0;do{if((ha|0)==0){ia=1}else{ja=c[ha+12>>2]|0;if((ja|0)==(c[ha+16>>2]|0)){ka=dd[c[(c[ha>>2]|0)+36>>2]&127](ha)|0}else{ka=c[ja>>2]|0}if((ka|0)==-1){c[f>>2]=0;ia=1;break}else{ia=(c[f>>2]|0)==0;break}}}while(0);ha=c[b>>2]|0;do{if((ha|0)==0){Z=164}else{ja=c[ha+12>>2]|0;if((ja|0)==(c[ha+16>>2]|0)){la=dd[c[(c[ha>>2]|0)+36>>2]&127](ha)|0}else{la=c[ja>>2]|0}if((la|0)==-1){c[b>>2]=0;Z=164;break}else{if(ia^(ha|0)==0){break}else{break L27}}}}while(0);if((Z|0)==164){Z=0;if(ia){break}}ha=c[f>>2]|0;ja=c[ha+12>>2]|0;if((ja|0)==(c[ha+16>>2]|0)){ma=dd[c[(c[ha>>2]|0)+36>>2]&127](ha)|0}else{ma=c[ja>>2]|0}if(ed[c[(c[s>>2]|0)+12>>2]&63](l,2048,ma)|0){ja=c[n>>2]|0;if((ja|0)==(c[q>>2]|0)){sk(m,n,q);na=c[n>>2]|0}else{na=ja}c[n>>2]=na+4;c[na>>2]=ma;oa=V+1|0;pa=X;qa=ea;ra=fa;sa=ga}else{ja=d[w]|0;if((((ja&1|0)==0?ja>>>1:c[Q>>2]|0)|0)==0|(V|0)==0){break}if((ma|0)!=(c[u>>2]|0)){break}if((ea|0)==(X|0)){ja=(ga|0)!=164;ha=ea-fa|0;ta=ha>>>0<2147483647>>>0?ha<<1:-1;if(ja){ua=fa}else{ua=0}ja=On(ua,ta)|0;va=ja;if((ja|0)==0){Yn()}wa=va+(ta>>>2<<2)|0;xa=va+(ha>>2<<2)|0;ya=va;za=80}else{wa=X;xa=ea;ya=fa;za=ga}c[xa>>2]=V;oa=0;pa=wa;qa=xa+4|0;ra=ya;sa=za}va=c[f>>2]|0;ha=va+12|0;ta=c[ha>>2]|0;if((ta|0)==(c[va+16>>2]|0)){dd[c[(c[va>>2]|0)+40>>2]&127](va)|0;V=oa;X=pa;ea=qa;fa=ra;ga=sa;continue}else{c[ha>>2]=ta+4;V=oa;X=pa;ea=qa;fa=ra;ga=sa;continue}}if((fa|0)==(ea|0)|(V|0)==0){Aa=X;Ba=ea;Ca=fa;Da=ga}else{if((ea|0)==(X|0)){ta=(ga|0)!=164;ha=ea-fa|0;va=ha>>>0<2147483647>>>0?ha<<1:-1;if(ta){Ea=fa}else{Ea=0}ta=On(Ea,va)|0;ja=ta;if((ta|0)==0){Yn()}Fa=ja+(va>>>2<<2)|0;Ga=ja+(ha>>2<<2)|0;Ha=ja;Ia=80}else{Fa=X;Ga=ea;Ha=fa;Ia=ga}c[Ga>>2]=V;Aa=Fa;Ba=Ga+4|0;Ca=Ha;Da=Ia}ja=c[B>>2]|0;if((ja|0)>0){ha=c[f>>2]|0;do{if((ha|0)==0){Ja=1}else{va=c[ha+12>>2]|0;if((va|0)==(c[ha+16>>2]|0)){Ka=dd[c[(c[ha>>2]|0)+36>>2]&127](ha)|0}else{Ka=c[va>>2]|0}if((Ka|0)==-1){c[f>>2]=0;Ja=1;break}else{Ja=(c[f>>2]|0)==0;break}}}while(0);ha=c[b>>2]|0;do{if((ha|0)==0){Z=213}else{V=c[ha+12>>2]|0;if((V|0)==(c[ha+16>>2]|0)){La=dd[c[(c[ha>>2]|0)+36>>2]&127](ha)|0}else{La=c[V>>2]|0}if((La|0)==-1){c[b>>2]=0;Z=213;break}else{if(Ja^(ha|0)==0){Ma=ha;break}else{Z=219;break L2}}}}while(0);if((Z|0)==213){Z=0;if(Ja){Z=219;break L2}else{Ma=0}}ha=c[f>>2]|0;V=c[ha+12>>2]|0;if((V|0)==(c[ha+16>>2]|0)){Na=dd[c[(c[ha>>2]|0)+36>>2]&127](ha)|0}else{Na=c[V>>2]|0}if((Na|0)!=(c[t>>2]|0)){Z=219;break L2}V=c[f>>2]|0;ha=V+12|0;ga=c[ha>>2]|0;if((ga|0)==(c[V+16>>2]|0)){dd[c[(c[V>>2]|0)+40>>2]&127](V)|0;Oa=Ma;Pa=ja}else{c[ha>>2]=ga+4;Oa=Ma;Pa=ja}while(1){ga=c[f>>2]|0;do{if((ga|0)==0){Qa=1}else{ha=c[ga+12>>2]|0;if((ha|0)==(c[ga+16>>2]|0)){Ra=dd[c[(c[ga>>2]|0)+36>>2]&127](ga)|0}else{Ra=c[ha>>2]|0}if((Ra|0)==-1){c[f>>2]=0;Qa=1;break}else{Qa=(c[f>>2]|0)==0;break}}}while(0);do{if((Oa|0)==0){Z=236}else{ga=c[Oa+12>>2]|0;if((ga|0)==(c[Oa+16>>2]|0)){Sa=dd[c[(c[Oa>>2]|0)+36>>2]&127](Oa)|0}else{Sa=c[ga>>2]|0}if((Sa|0)==-1){c[b>>2]=0;Z=236;break}else{if(Qa^(Oa|0)==0){Ta=Oa;break}else{Z=243;break L2}}}}while(0);if((Z|0)==236){Z=0;if(Qa){Z=243;break L2}else{Ta=0}}ga=c[f>>2]|0;ha=c[ga+12>>2]|0;if((ha|0)==(c[ga+16>>2]|0)){Ua=dd[c[(c[ga>>2]|0)+36>>2]&127](ga)|0}else{Ua=c[ha>>2]|0}if(!(ed[c[(c[s>>2]|0)+12>>2]&63](l,2048,Ua)|0)){Z=243;break L2}if((c[n>>2]|0)==(c[q>>2]|0)){sk(m,n,q)}ha=c[f>>2]|0;ga=c[ha+12>>2]|0;if((ga|0)==(c[ha+16>>2]|0)){Va=dd[c[(c[ha>>2]|0)+36>>2]&127](ha)|0}else{Va=c[ga>>2]|0}ga=c[n>>2]|0;c[n>>2]=ga+4;c[ga>>2]=Va;ga=Pa-1|0;c[B>>2]=ga;ha=c[f>>2]|0;V=ha+12|0;fa=c[V>>2]|0;if((fa|0)==(c[ha+16>>2]|0)){dd[c[(c[ha>>2]|0)+40>>2]&127](ha)|0}else{c[V>>2]=fa+4}if((ga|0)>0){Oa=Ta;Pa=ga}else{break}}}if((c[n>>2]|0)==(c[g>>2]|0)){Z=254;break L2}else{Wa=r;Xa=Aa;Ya=Ba;Za=Ca;_a=Da}break};case 1:{if((U|0)==3){aa=R;ba=S;ca=T;da=r;Z=256;break L2}ja=c[f>>2]|0;ga=c[ja+12>>2]|0;if((ga|0)==(c[ja+16>>2]|0)){$a=dd[c[(c[ja>>2]|0)+36>>2]&127](ja)|0}else{$a=c[ga>>2]|0}if(!(ed[c[(c[s>>2]|0)+12>>2]&63](l,8192,$a)|0)){Z=40;break L2}ga=c[f>>2]|0;ja=ga+12|0;fa=c[ja>>2]|0;if((fa|0)==(c[ga+16>>2]|0)){ab=dd[c[(c[ga>>2]|0)+40>>2]&127](ga)|0}else{c[ja>>2]=fa+4;ab=c[fa>>2]|0}Kf(A,ab);Z=41;break};case 0:{Z=41;break};case 3:{fa=a[E]|0;ja=fa&255;ga=(ja&1|0)==0;V=a[F]|0;ha=V&255;ea=(ha&1|0)==0;if(((ga?ja>>>1:c[J>>2]|0)|0)==(-(ea?ha>>>1:c[H>>2]|0)|0)){Wa=r;Xa=o;Ya=T;Za=S;_a=R;break L26}do{if(((ga?ja>>>1:c[J>>2]|0)|0)!=0){if(((ea?ha>>>1:c[H>>2]|0)|0)==0){break}X=c[f>>2]|0;va=c[X+12>>2]|0;if((va|0)==(c[X+16>>2]|0)){ta=dd[c[(c[X>>2]|0)+36>>2]&127](X)|0;bb=ta;cb=a[E]|0}else{bb=c[va>>2]|0;cb=fa}va=c[f>>2]|0;ta=va+12|0;X=c[ta>>2]|0;db=(X|0)==(c[va+16>>2]|0);if((bb|0)==(c[((cb&1)==0?J:c[K>>2]|0)>>2]|0)){if(db){dd[c[(c[va>>2]|0)+40>>2]&127](va)|0}else{c[ta>>2]=X+4}ta=d[E]|0;Wa=((ta&1|0)==0?ta>>>1:c[J>>2]|0)>>>0>1>>>0?y:r;Xa=o;Ya=T;Za=S;_a=R;break L26}if(db){eb=dd[c[(c[va>>2]|0)+36>>2]&127](va)|0}else{eb=c[X>>2]|0}if((eb|0)!=(c[((a[F]&1)==0?H:c[I>>2]|0)>>2]|0)){Z=106;break L2}X=c[f>>2]|0;va=X+12|0;db=c[va>>2]|0;if((db|0)==(c[X+16>>2]|0)){dd[c[(c[X>>2]|0)+40>>2]&127](X)|0}else{c[va>>2]=db+4}a[k]=1;db=d[F]|0;Wa=((db&1|0)==0?db>>>1:c[H>>2]|0)>>>0>1>>>0?z:r;Xa=o;Ya=T;Za=S;_a=R;break L26}}while(0);ha=c[f>>2]|0;ea=c[ha+12>>2]|0;db=(ea|0)==(c[ha+16>>2]|0);if(((ga?ja>>>1:c[J>>2]|0)|0)==0){if(db){va=dd[c[(c[ha>>2]|0)+36>>2]&127](ha)|0;fb=va;gb=a[F]|0}else{fb=c[ea>>2]|0;gb=V}if((fb|0)!=(c[((gb&1)==0?H:c[I>>2]|0)>>2]|0)){Wa=r;Xa=o;Ya=T;Za=S;_a=R;break L26}va=c[f>>2]|0;X=va+12|0;ta=c[X>>2]|0;if((ta|0)==(c[va+16>>2]|0)){dd[c[(c[va>>2]|0)+40>>2]&127](va)|0}else{c[X>>2]=ta+4}a[k]=1;ta=d[F]|0;Wa=((ta&1|0)==0?ta>>>1:c[H>>2]|0)>>>0>1>>>0?z:r;Xa=o;Ya=T;Za=S;_a=R;break L26}if(db){db=dd[c[(c[ha>>2]|0)+36>>2]&127](ha)|0;hb=db;ib=a[E]|0}else{hb=c[ea>>2]|0;ib=fa}if((hb|0)!=(c[((ib&1)==0?J:c[K>>2]|0)>>2]|0)){a[k]=1;Wa=r;Xa=o;Ya=T;Za=S;_a=R;break L26}ea=c[f>>2]|0;db=ea+12|0;ha=c[db>>2]|0;if((ha|0)==(c[ea+16>>2]|0)){dd[c[(c[ea>>2]|0)+40>>2]&127](ea)|0}else{c[db>>2]=ha+4}ha=d[E]|0;Wa=((ha&1|0)==0?ha>>>1:c[J>>2]|0)>>>0>1>>>0?y:r;Xa=o;Ya=T;Za=S;_a=R;break};case 2:{if(!((r|0)!=0|U>>>0<2>>>0)){if((U|0)==2){jb=(a[P]|0)!=0}else{jb=0}if(!(L|jb)){Wa=0;Xa=o;Ya=T;Za=S;_a=R;break L26}}ha=a[D]|0;db=(ha&1)==0?h:c[M>>2]|0;L242:do{if((U|0)==0){kb=db;lb=ha;mb=$}else{if((d[e+(U-1)|0]|0)>>>0<2>>>0){nb=db;ob=ha}else{kb=db;lb=ha;mb=$;break}while(1){ea=ob&255;if((nb|0)==(((ob&1)==0?h:c[M>>2]|0)+(((ea&1|0)==0?ea>>>1:c[h>>2]|0)<<2)|0)){pb=ob;break}if(!(ed[c[(c[s>>2]|0)+12>>2]&63](l,8192,c[nb>>2]|0)|0)){Z=117;break}nb=nb+4|0;ob=a[D]|0}if((Z|0)==117){Z=0;pb=a[D]|0}ea=(pb&1)==0;ta=nb-(ea?h:c[M>>2]|0)>>2;X=a[G]|0;va=X&255;qb=(va&1|0)==0;L252:do{if(ta>>>0<=(qb?va>>>1:c[N>>2]|0)>>>0){rb=(X&1)==0;sb=(rb?N:c[O>>2]|0)+((qb?va>>>1:c[N>>2]|0)-ta<<2)|0;tb=(rb?N:c[O>>2]|0)+((qb?va>>>1:c[N>>2]|0)<<2)|0;if((sb|0)==(tb|0)){kb=nb;lb=pb;mb=$;break L242}else{ub=sb;vb=ea?h:c[M>>2]|0}while(1){if((c[ub>>2]|0)!=(c[vb>>2]|0)){break L252}sb=ub+4|0;if((sb|0)==(tb|0)){kb=nb;lb=pb;mb=$;break L242}ub=sb;vb=vb+4|0}}}while(0);kb=ea?h:c[M>>2]|0;lb=pb;mb=$}}while(0);L259:while(1){ha=lb&255;if((kb|0)==(((lb&1)==0?h:c[M>>2]|0)+(((ha&1|0)==0?ha>>>1:c[h>>2]|0)<<2)|0)){break}ha=c[f>>2]|0;do{if((ha|0)==0){wb=1}else{db=c[ha+12>>2]|0;if((db|0)==(c[ha+16>>2]|0)){xb=dd[c[(c[ha>>2]|0)+36>>2]&127](ha)|0}else{xb=c[db>>2]|0}if((xb|0)==-1){c[f>>2]=0;wb=1;break}else{wb=(c[f>>2]|0)==0;break}}}while(0);do{if((mb|0)==0){Z=138}else{ha=c[mb+12>>2]|0;if((ha|0)==(c[mb+16>>2]|0)){yb=dd[c[(c[mb>>2]|0)+36>>2]&127](mb)|0}else{yb=c[ha>>2]|0}if((yb|0)==-1){c[b>>2]=0;Z=138;break}else{if(wb^(mb|0)==0){zb=mb;break}else{break L259}}}}while(0);if((Z|0)==138){Z=0;if(wb){break}else{zb=0}}ha=c[f>>2]|0;ea=c[ha+12>>2]|0;if((ea|0)==(c[ha+16>>2]|0)){Ab=dd[c[(c[ha>>2]|0)+36>>2]&127](ha)|0}else{Ab=c[ea>>2]|0}if((Ab|0)!=(c[kb>>2]|0)){break}ea=c[f>>2]|0;ha=ea+12|0;db=c[ha>>2]|0;if((db|0)==(c[ea+16>>2]|0)){dd[c[(c[ea>>2]|0)+40>>2]&127](ea)|0}else{c[ha>>2]=db+4}kb=kb+4|0;lb=a[D]|0;mb=zb}if(!L){Wa=r;Xa=o;Ya=T;Za=S;_a=R;break L26}db=a[D]|0;ha=db&255;if((kb|0)==(((db&1)==0?h:c[M>>2]|0)+(((ha&1|0)==0?ha>>>1:c[h>>2]|0)<<2)|0)){Wa=r;Xa=o;Ya=T;Za=S;_a=R}else{Z=150;break L2}break};default:{Wa=r;Xa=o;Ya=T;Za=S;_a=R}}}while(0);L295:do{if((Z|0)==41){Z=0;if((U|0)==3){aa=R;ba=S;ca=T;da=r;Z=256;break L2}else{Bb=$}while(1){ha=c[f>>2]|0;do{if((ha|0)==0){Cb=1}else{db=c[ha+12>>2]|0;if((db|0)==(c[ha+16>>2]|0)){Db=dd[c[(c[ha>>2]|0)+36>>2]&127](ha)|0}else{Db=c[db>>2]|0}if((Db|0)==-1){c[f>>2]=0;Cb=1;break}else{Cb=(c[f>>2]|0)==0;break}}}while(0);do{if((Bb|0)==0){Z=55}else{ha=c[Bb+12>>2]|0;if((ha|0)==(c[Bb+16>>2]|0)){Eb=dd[c[(c[Bb>>2]|0)+36>>2]&127](Bb)|0}else{Eb=c[ha>>2]|0}if((Eb|0)==-1){c[b>>2]=0;Z=55;break}else{if(Cb^(Bb|0)==0){Fb=Bb;break}else{Wa=r;Xa=o;Ya=T;Za=S;_a=R;break L295}}}}while(0);if((Z|0)==55){Z=0;if(Cb){Wa=r;Xa=o;Ya=T;Za=S;_a=R;break L295}else{Fb=0}}ha=c[f>>2]|0;db=c[ha+12>>2]|0;if((db|0)==(c[ha+16>>2]|0)){Gb=dd[c[(c[ha>>2]|0)+36>>2]&127](ha)|0}else{Gb=c[db>>2]|0}if(!(ed[c[(c[s>>2]|0)+12>>2]&63](l,8192,Gb)|0)){Wa=r;Xa=o;Ya=T;Za=S;_a=R;break L295}db=c[f>>2]|0;ha=db+12|0;ea=c[ha>>2]|0;if((ea|0)==(c[db+16>>2]|0)){Hb=dd[c[(c[db>>2]|0)+40>>2]&127](db)|0}else{c[ha>>2]=ea+4;Hb=c[ea>>2]|0}Kf(A,Hb);Bb=Fb}}}while(0);ea=U+1|0;if(ea>>>0<4>>>0){R=_a;S=Za;T=Ya;o=Xa;r=Wa;U=ea}else{aa=_a;ba=Za;ca=Ya;da=Wa;Z=256;break}}L332:do{if((Z|0)==243){c[j>>2]=c[j>>2]|4;Ib=0;Jb=Ca;Kb=Da}else if((Z|0)==150){c[j>>2]=c[j>>2]|4;Ib=0;Jb=S;Kb=R}else if((Z|0)==219){c[j>>2]=c[j>>2]|4;Ib=0;Jb=Ca;Kb=Da}else if((Z|0)==254){c[j>>2]=c[j>>2]|4;Ib=0;Jb=Ca;Kb=Da}else if((Z|0)==256){L338:do{if((da|0)!=0){Wa=da;Ya=da+4|0;Za=da+8|0;_a=1;L340:while(1){U=d[Wa]|0;if((U&1|0)==0){Lb=U>>>1}else{Lb=c[Ya>>2]|0}if(_a>>>0>=Lb>>>0){break L338}U=c[f>>2]|0;do{if((U|0)==0){Mb=1}else{r=c[U+12>>2]|0;if((r|0)==(c[U+16>>2]|0)){Nb=dd[c[(c[U>>2]|0)+36>>2]&127](U)|0}else{Nb=c[r>>2]|0}if((Nb|0)==-1){c[f>>2]=0;Mb=1;break}else{Mb=(c[f>>2]|0)==0;break}}}while(0);U=c[b>>2]|0;do{if((U|0)==0){Z=275}else{r=c[U+12>>2]|0;if((r|0)==(c[U+16>>2]|0)){Ob=dd[c[(c[U>>2]|0)+36>>2]&127](U)|0}else{Ob=c[r>>2]|0}if((Ob|0)==-1){c[b>>2]=0;Z=275;break}else{if(Mb^(U|0)==0){break}else{break L340}}}}while(0);if((Z|0)==275){Z=0;if(Mb){break}}U=c[f>>2]|0;r=c[U+12>>2]|0;if((r|0)==(c[U+16>>2]|0)){Pb=dd[c[(c[U>>2]|0)+36>>2]&127](U)|0}else{Pb=c[r>>2]|0}if((a[Wa]&1)==0){Qb=Ya}else{Qb=c[Za>>2]|0}if((Pb|0)!=(c[Qb+(_a<<2)>>2]|0)){break}r=_a+1|0;U=c[f>>2]|0;Xa=U+12|0;o=c[Xa>>2]|0;if((o|0)==(c[U+16>>2]|0)){dd[c[(c[U>>2]|0)+40>>2]&127](U)|0;_a=r;continue}else{c[Xa>>2]=o+4;_a=r;continue}}c[j>>2]=c[j>>2]|4;Ib=0;Jb=ba;Kb=aa;break L332}}while(0);if((ba|0)==(ca|0)){Ib=1;Jb=ca;Kb=aa;break}c[C>>2]=0;kk(v,ba,ca,C);if((c[C>>2]|0)==0){Ib=1;Jb=ba;Kb=aa;break}c[j>>2]=c[j>>2]|4;Ib=0;Jb=ba;Kb=aa}else if((Z|0)==40){c[j>>2]=c[j>>2]|4;Ib=0;Jb=S;Kb=R}else if((Z|0)==106){c[j>>2]=c[j>>2]|4;Ib=0;Jb=S;Kb=R}}while(0);Gf(A);Gf(z);Gf(y);Gf(x);vf(v);if((Jb|0)==0){i=p;return Ib|0}_c[Kb&511](Jb);i=p;return Ib|0}function pk(b,d,e,f,g,h,j,k){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;var l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0;d=i;i=i+456|0;l=e;e=i;i=i+4|0;i=i+7&-8;c[e>>2]=c[l>>2];l=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[l>>2];l=d|0;m=d+16|0;n=d+416|0;o=d+424|0;p=d+432|0;q=d+440|0;r=d+448|0;s=n|0;c[s>>2]=m;t=n+4|0;c[t>>2]=164;u=m+400|0;Pf(p,h);m=p|0;v=c[m>>2]|0;if((c[8974]|0)!=-1){c[l>>2]=35896;c[l+4>>2]=14;c[l+8>>2]=0;qf(35896,l,96)}l=(c[8975]|0)-1|0;w=c[v+8>>2]|0;do{if((c[v+12>>2]|0)-w>>2>>>0>l>>>0){x=c[w+(l<<2)>>2]|0;if((x|0)==0){break}y=x;a[q]=0;z=f|0;A=c[z>>2]|0;c[r>>2]=A;if(ok(e,r,g,p,c[h+4>>2]|0,j,q,y,n,o,u)|0){B=k;if((a[B]&1)==0){c[k+4>>2]=0;a[B]=0}else{c[c[k+8>>2]>>2]=0;c[k+4>>2]=0}B=x;if((a[q]&1)!=0){Kf(k,ad[c[(c[B>>2]|0)+44>>2]&31](y,45)|0)}x=ad[c[(c[B>>2]|0)+44>>2]&31](y,48)|0;y=c[s>>2]|0;B=c[o>>2]|0;C=B-4|0;L20:do{if(y>>>0<C>>>0){D=y;while(1){E=D+4|0;if((c[D>>2]|0)!=(x|0)){F=D;break L20}if(E>>>0<C>>>0){D=E}else{F=E;break}}}else{F=y}}while(0);qk(k,F,B)|0}y=e|0;C=c[y>>2]|0;do{if((C|0)==0){G=0}else{x=c[C+12>>2]|0;if((x|0)==(c[C+16>>2]|0)){H=dd[c[(c[C>>2]|0)+36>>2]&127](C)|0}else{H=c[x>>2]|0}if((H|0)!=-1){G=C;break}c[y>>2]=0;G=0}}while(0);y=(G|0)==0;do{if((A|0)==0){I=34}else{C=c[A+12>>2]|0;if((C|0)==(c[A+16>>2]|0)){J=dd[c[(c[A>>2]|0)+36>>2]&127](A)|0}else{J=c[C>>2]|0}if((J|0)==-1){c[z>>2]=0;I=34;break}else{if(y^(A|0)==0){break}else{I=36;break}}}}while(0);if((I|0)==34){if(y){I=36}}if((I|0)==36){c[j>>2]=c[j>>2]|2}c[b>>2]=G;Xe(c[m>>2]|0)|0;A=c[s>>2]|0;c[s>>2]=0;if((A|0)==0){i=d;return}_c[c[t>>2]&511](A);i=d;return}}while(0);d=Nc(4)|0;qn(d);Qb(d|0,10192,134)}function qk(b,d,e){b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0,i=0,j=0,k=0,l=0,m=0,n=0,o=0;f=b;g=d;h=a[f]|0;i=h&255;if((i&1|0)==0){j=i>>>1}else{j=c[b+4>>2]|0}if((h&1)==0){k=1;l=h}else{h=c[b>>2]|0;k=(h&-2)-1|0;l=h&255}h=e-g>>2;if((h|0)==0){return b|0}if((k-j|0)>>>0<h>>>0){Mf(b,k,j+h-k|0,j,j,0,0);m=a[f]|0}else{m=l}if((m&1)==0){n=b+4|0}else{n=c[b+8>>2]|0}m=n+(j<<2)|0;if((d|0)==(e|0)){o=m}else{l=j+((e-4+(-g|0)|0)>>>2)+1|0;g=d;d=m;while(1){c[d>>2]=c[g>>2];m=g+4|0;if((m|0)==(e|0)){break}else{g=m;d=d+4|0}}o=n+(l<<2)|0}c[o>>2]=0;o=j+h|0;if((a[f]&1)==0){a[f]=o<<1;return b|0}else{c[b+4>>2]=o;return b|0}return 0}function rk(b,d,e,f,g,h,j,k,l,m){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;l=l|0;m=m|0;var n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0;n=i;i=i+56|0;o=n|0;p=n+16|0;q=n+32|0;r=n+40|0;s=r;t=i;i=i+12|0;i=i+7&-8;u=t;v=i;i=i+12|0;i=i+7&-8;w=v;x=i;i=i+12|0;i=i+7&-8;y=x;z=i;i=i+4|0;i=i+7&-8;A=i;i=i+12|0;i=i+7&-8;B=A;D=i;i=i+12|0;i=i+7&-8;E=D;F=i;i=i+12|0;i=i+7&-8;G=F;H=i;i=i+12|0;i=i+7&-8;I=H;if(b){b=c[d>>2]|0;if((c[9090]|0)!=-1){c[p>>2]=36360;c[p+4>>2]=14;c[p+8>>2]=0;qf(36360,p,96)}p=(c[9091]|0)-1|0;J=c[b+8>>2]|0;if((c[b+12>>2]|0)-J>>2>>>0<=p>>>0){K=Nc(4)|0;L=K;qn(L);Qb(K|0,10192,134)}b=c[J+(p<<2)>>2]|0;if((b|0)==0){K=Nc(4)|0;L=K;qn(L);Qb(K|0,10192,134)}K=b;$c[c[(c[b>>2]|0)+44>>2]&127](q,K);L=e;C=c[q>>2]|0;a[L]=C;C=C>>8;a[L+1|0]=C;C=C>>8;a[L+2|0]=C;C=C>>8;a[L+3|0]=C;L=b;$c[c[(c[L>>2]|0)+32>>2]&127](r,K);q=l;if((a[q]&1)==0){c[l+4>>2]=0;a[q]=0}else{c[c[l+8>>2]>>2]=0;c[l+4>>2]=0}Jf(l,0);c[q>>2]=c[s>>2];c[q+4>>2]=c[s+4>>2];c[q+8>>2]=c[s+8>>2];ko(s|0,0,12)|0;Gf(r);$c[c[(c[L>>2]|0)+28>>2]&127](t,K);r=k;if((a[r]&1)==0){c[k+4>>2]=0;a[r]=0}else{c[c[k+8>>2]>>2]=0;c[k+4>>2]=0}Jf(k,0);c[r>>2]=c[u>>2];c[r+4>>2]=c[u+4>>2];c[r+8>>2]=c[u+8>>2];ko(u|0,0,12)|0;Gf(t);t=b;c[f>>2]=dd[c[(c[t>>2]|0)+12>>2]&127](K)|0;c[g>>2]=dd[c[(c[t>>2]|0)+16>>2]&127](K)|0;$c[c[(c[b>>2]|0)+20>>2]&127](v,K);b=h;if((a[b]&1)==0){a[h+1|0]=0;a[b]=0}else{a[c[h+8>>2]|0]=0;c[h+4>>2]=0}zf(h,0);c[b>>2]=c[w>>2];c[b+4>>2]=c[w+4>>2];c[b+8>>2]=c[w+8>>2];ko(w|0,0,12)|0;vf(v);$c[c[(c[L>>2]|0)+24>>2]&127](x,K);L=j;if((a[L]&1)==0){c[j+4>>2]=0;a[L]=0}else{c[c[j+8>>2]>>2]=0;c[j+4>>2]=0}Jf(j,0);c[L>>2]=c[y>>2];c[L+4>>2]=c[y+4>>2];c[L+8>>2]=c[y+8>>2];ko(y|0,0,12)|0;Gf(x);M=dd[c[(c[t>>2]|0)+36>>2]&127](K)|0;c[m>>2]=M;i=n;return}else{K=c[d>>2]|0;if((c[9092]|0)!=-1){c[o>>2]=36368;c[o+4>>2]=14;c[o+8>>2]=0;qf(36368,o,96)}o=(c[9093]|0)-1|0;d=c[K+8>>2]|0;if((c[K+12>>2]|0)-d>>2>>>0<=o>>>0){N=Nc(4)|0;O=N;qn(O);Qb(N|0,10192,134)}K=c[d+(o<<2)>>2]|0;if((K|0)==0){N=Nc(4)|0;O=N;qn(O);Qb(N|0,10192,134)}N=K;$c[c[(c[K>>2]|0)+44>>2]&127](z,N);O=e;C=c[z>>2]|0;a[O]=C;C=C>>8;a[O+1|0]=C;C=C>>8;a[O+2|0]=C;C=C>>8;a[O+3|0]=C;O=K;$c[c[(c[O>>2]|0)+32>>2]&127](A,N);z=l;if((a[z]&1)==0){c[l+4>>2]=0;a[z]=0}else{c[c[l+8>>2]>>2]=0;c[l+4>>2]=0}Jf(l,0);c[z>>2]=c[B>>2];c[z+4>>2]=c[B+4>>2];c[z+8>>2]=c[B+8>>2];ko(B|0,0,12)|0;Gf(A);$c[c[(c[O>>2]|0)+28>>2]&127](D,N);A=k;if((a[A]&1)==0){c[k+4>>2]=0;a[A]=0}else{c[c[k+8>>2]>>2]=0;c[k+4>>2]=0}Jf(k,0);c[A>>2]=c[E>>2];c[A+4>>2]=c[E+4>>2];c[A+8>>2]=c[E+8>>2];ko(E|0,0,12)|0;Gf(D);D=K;c[f>>2]=dd[c[(c[D>>2]|0)+12>>2]&127](N)|0;c[g>>2]=dd[c[(c[D>>2]|0)+16>>2]&127](N)|0;$c[c[(c[K>>2]|0)+20>>2]&127](F,N);K=h;if((a[K]&1)==0){a[h+1|0]=0;a[K]=0}else{a[c[h+8>>2]|0]=0;c[h+4>>2]=0}zf(h,0);c[K>>2]=c[G>>2];c[K+4>>2]=c[G+4>>2];c[K+8>>2]=c[G+8>>2];ko(G|0,0,12)|0;vf(F);$c[c[(c[O>>2]|0)+24>>2]&127](H,N);O=j;if((a[O]&1)==0){c[j+4>>2]=0;a[O]=0}else{c[c[j+8>>2]>>2]=0;c[j+4>>2]=0}Jf(j,0);c[O>>2]=c[I>>2];c[O+4>>2]=c[I+4>>2];c[O+8>>2]=c[I+8>>2];ko(I|0,0,12)|0;Gf(H);M=dd[c[(c[D>>2]|0)+36>>2]&127](N)|0;c[m>>2]=M;i=n;return}}function sk(a,b,d){a=a|0;b=b|0;d=d|0;var e=0,f=0,g=0,h=0,i=0,j=0,k=0,l=0;e=a+4|0;f=(c[e>>2]|0)!=164;g=a|0;a=c[g>>2]|0;h=a;i=(c[d>>2]|0)-h|0;j=i>>>0<2147483647>>>0?i<<1:-1;i=(c[b>>2]|0)-h>>2;if(f){k=a}else{k=0}a=On(k,j)|0;k=a;if((a|0)==0){Yn()}do{if(f){c[g>>2]=k;l=k}else{a=c[g>>2]|0;c[g>>2]=k;if((a|0)==0){l=k;break}_c[c[e>>2]&511](a);l=c[g>>2]|0}}while(0);c[e>>2]=80;c[b>>2]=l+(i<<2);c[d>>2]=(c[g>>2]|0)+(j>>>2<<2);return}function tk(a){a=a|0;Ve(a|0);Tn(a);return}function uk(a){a=a|0;Ve(a|0);return}function vk(b,e,f,g,j,k,l){b=b|0;e=e|0;f=f|0;g=g|0;j=j|0;k=k|0;l=+l;var m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0;e=i;i=i+248|0;m=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[m>>2];m=e|0;n=e+120|0;o=e+232|0;p=e+240|0;q=p;r=i;i=i+1|0;i=i+7&-8;s=i;i=i+1|0;i=i+7&-8;t=i;i=i+12|0;i=i+7&-8;u=t;v=i;i=i+12|0;i=i+7&-8;w=v;x=i;i=i+12|0;i=i+7&-8;y=x;z=i;i=i+4|0;i=i+7&-8;A=i;i=i+100|0;i=i+7&-8;B=i;i=i+4|0;i=i+7&-8;C=i;i=i+4|0;i=i+7&-8;D=i;i=i+4|0;i=i+7&-8;E=e+16|0;c[n>>2]=E;F=e+128|0;G=cb(E|0,100,3256,(E=i,i=i+8|0,h[E>>3]=l,E)|0)|0;i=E;do{if(G>>>0>99>>>0){do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);H=ni(n,c[8322]|0,3256,(E=i,i=i+8|0,h[E>>3]=l,E)|0)|0;i=E;I=c[n>>2]|0;if((I|0)==0){Yn();J=c[n>>2]|0}else{J=I}I=Ln(H)|0;if((I|0)!=0){K=I;L=H;M=J;N=I;break}Yn();K=0;L=H;M=J;N=0}else{K=F;L=G;M=0;N=0}}while(0);Pf(o,j);G=o|0;F=c[G>>2]|0;if((c[8976]|0)!=-1){c[m>>2]=35904;c[m+4>>2]=14;c[m+8>>2]=0;qf(35904,m,96)}m=(c[8977]|0)-1|0;J=c[F+8>>2]|0;do{if((c[F+12>>2]|0)-J>>2>>>0>m>>>0){E=c[J+(m<<2)>>2]|0;if((E|0)==0){break}H=E;I=c[n>>2]|0;ld[c[(c[E>>2]|0)+32>>2]&15](H,I,I+L|0,K)|0;if((L|0)==0){O=0}else{O=(a[c[n>>2]|0]|0)==45}c[p>>2]=0;ko(u|0,0,12)|0;ko(w|0,0,12)|0;ko(y|0,0,12)|0;wk(g,O,o,q,r,s,t,v,x,z);I=A|0;E=c[z>>2]|0;if((L|0)>(E|0)){P=d[y]|0;if((P&1|0)==0){Q=P>>>1}else{Q=c[x+4>>2]|0}P=d[w]|0;if((P&1|0)==0){R=P>>>1}else{R=c[v+4>>2]|0}S=(L-E<<1|1)+Q+R|0}else{P=d[y]|0;if((P&1|0)==0){T=P>>>1}else{T=c[x+4>>2]|0}P=d[w]|0;if((P&1|0)==0){U=P>>>1}else{U=c[v+4>>2]|0}S=T+2+U|0}P=S+E|0;do{if(P>>>0>100>>>0){V=Ln(P)|0;if((V|0)!=0){W=V;X=V;break}Yn();W=0;X=0}else{W=I;X=0}}while(0);xk(W,B,C,c[j+4>>2]|0,K,K+L|0,H,O,q,a[r]|0,a[s]|0,t,v,x,E);c[D>>2]=c[f>>2];ii(b,D,W,c[B>>2]|0,c[C>>2]|0,j,k);if((X|0)!=0){Mn(X)}vf(x);vf(v);vf(t);Xe(c[G>>2]|0)|0;if((N|0)!=0){Mn(N)}if((M|0)==0){i=e;return}Mn(M);i=e;return}}while(0);e=Nc(4)|0;qn(e);Qb(e|0,10192,134)}function wk(b,d,e,f,g,h,j,k,l,m){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;l=l|0;m=m|0;var n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0;n=i;i=i+40|0;o=n|0;p=n+16|0;q=n+32|0;r=q;s=i;i=i+12|0;i=i+7&-8;t=s;u=i;i=i+4|0;i=i+7&-8;v=u;w=i;i=i+12|0;i=i+7&-8;x=w;y=i;i=i+12|0;i=i+7&-8;z=y;A=i;i=i+12|0;i=i+7&-8;B=A;D=i;i=i+4|0;i=i+7&-8;E=D;F=i;i=i+12|0;i=i+7&-8;G=F;H=i;i=i+4|0;i=i+7&-8;I=H;J=i;i=i+12|0;i=i+7&-8;K=J;L=i;i=i+12|0;i=i+7&-8;M=L;N=i;i=i+12|0;i=i+7&-8;O=N;P=c[e>>2]|0;if(b){if((c[9094]|0)!=-1){c[p>>2]=36376;c[p+4>>2]=14;c[p+8>>2]=0;qf(36376,p,96)}p=(c[9095]|0)-1|0;b=c[P+8>>2]|0;if((c[P+12>>2]|0)-b>>2>>>0<=p>>>0){Q=Nc(4)|0;R=Q;qn(R);Qb(Q|0,10192,134)}e=c[b+(p<<2)>>2]|0;if((e|0)==0){Q=Nc(4)|0;R=Q;qn(R);Qb(Q|0,10192,134)}Q=e;R=c[e>>2]|0;if(d){$c[c[R+44>>2]&127](r,Q);r=f;C=c[q>>2]|0;a[r]=C;C=C>>8;a[r+1|0]=C;C=C>>8;a[r+2|0]=C;C=C>>8;a[r+3|0]=C;$c[c[(c[e>>2]|0)+32>>2]&127](s,Q);r=l;if((a[r]&1)==0){a[l+1|0]=0;a[r]=0}else{a[c[l+8>>2]|0]=0;c[l+4>>2]=0}zf(l,0);c[r>>2]=c[t>>2];c[r+4>>2]=c[t+4>>2];c[r+8>>2]=c[t+8>>2];ko(t|0,0,12)|0;vf(s)}else{$c[c[R+40>>2]&127](v,Q);v=f;C=c[u>>2]|0;a[v]=C;C=C>>8;a[v+1|0]=C;C=C>>8;a[v+2|0]=C;C=C>>8;a[v+3|0]=C;$c[c[(c[e>>2]|0)+28>>2]&127](w,Q);v=l;if((a[v]&1)==0){a[l+1|0]=0;a[v]=0}else{a[c[l+8>>2]|0]=0;c[l+4>>2]=0}zf(l,0);c[v>>2]=c[x>>2];c[v+4>>2]=c[x+4>>2];c[v+8>>2]=c[x+8>>2];ko(x|0,0,12)|0;vf(w)}w=e;a[g]=dd[c[(c[w>>2]|0)+12>>2]&127](Q)|0;a[h]=dd[c[(c[w>>2]|0)+16>>2]&127](Q)|0;w=e;$c[c[(c[w>>2]|0)+20>>2]&127](y,Q);x=j;if((a[x]&1)==0){a[j+1|0]=0;a[x]=0}else{a[c[j+8>>2]|0]=0;c[j+4>>2]=0}zf(j,0);c[x>>2]=c[z>>2];c[x+4>>2]=c[z+4>>2];c[x+8>>2]=c[z+8>>2];ko(z|0,0,12)|0;vf(y);$c[c[(c[w>>2]|0)+24>>2]&127](A,Q);w=k;if((a[w]&1)==0){a[k+1|0]=0;a[w]=0}else{a[c[k+8>>2]|0]=0;c[k+4>>2]=0}zf(k,0);c[w>>2]=c[B>>2];c[w+4>>2]=c[B+4>>2];c[w+8>>2]=c[B+8>>2];ko(B|0,0,12)|0;vf(A);S=dd[c[(c[e>>2]|0)+36>>2]&127](Q)|0;c[m>>2]=S;i=n;return}else{if((c[9096]|0)!=-1){c[o>>2]=36384;c[o+4>>2]=14;c[o+8>>2]=0;qf(36384,o,96)}o=(c[9097]|0)-1|0;Q=c[P+8>>2]|0;if((c[P+12>>2]|0)-Q>>2>>>0<=o>>>0){T=Nc(4)|0;U=T;qn(U);Qb(T|0,10192,134)}P=c[Q+(o<<2)>>2]|0;if((P|0)==0){T=Nc(4)|0;U=T;qn(U);Qb(T|0,10192,134)}T=P;U=c[P>>2]|0;if(d){$c[c[U+44>>2]&127](E,T);E=f;C=c[D>>2]|0;a[E]=C;C=C>>8;a[E+1|0]=C;C=C>>8;a[E+2|0]=C;C=C>>8;a[E+3|0]=C;$c[c[(c[P>>2]|0)+32>>2]&127](F,T);E=l;if((a[E]&1)==0){a[l+1|0]=0;a[E]=0}else{a[c[l+8>>2]|0]=0;c[l+4>>2]=0}zf(l,0);c[E>>2]=c[G>>2];c[E+4>>2]=c[G+4>>2];c[E+8>>2]=c[G+8>>2];ko(G|0,0,12)|0;vf(F)}else{$c[c[U+40>>2]&127](I,T);I=f;C=c[H>>2]|0;a[I]=C;C=C>>8;a[I+1|0]=C;C=C>>8;a[I+2|0]=C;C=C>>8;a[I+3|0]=C;$c[c[(c[P>>2]|0)+28>>2]&127](J,T);I=l;if((a[I]&1)==0){a[l+1|0]=0;a[I]=0}else{a[c[l+8>>2]|0]=0;c[l+4>>2]=0}zf(l,0);c[I>>2]=c[K>>2];c[I+4>>2]=c[K+4>>2];c[I+8>>2]=c[K+8>>2];ko(K|0,0,12)|0;vf(J)}J=P;a[g]=dd[c[(c[J>>2]|0)+12>>2]&127](T)|0;a[h]=dd[c[(c[J>>2]|0)+16>>2]&127](T)|0;J=P;$c[c[(c[J>>2]|0)+20>>2]&127](L,T);h=j;if((a[h]&1)==0){a[j+1|0]=0;a[h]=0}else{a[c[j+8>>2]|0]=0;c[j+4>>2]=0}zf(j,0);c[h>>2]=c[M>>2];c[h+4>>2]=c[M+4>>2];c[h+8>>2]=c[M+8>>2];ko(M|0,0,12)|0;vf(L);$c[c[(c[J>>2]|0)+24>>2]&127](N,T);J=k;if((a[J]&1)==0){a[k+1|0]=0;a[J]=0}else{a[c[k+8>>2]|0]=0;c[k+4>>2]=0}zf(k,0);c[J>>2]=c[O>>2];c[J+4>>2]=c[O+4>>2];c[J+8>>2]=c[O+8>>2];ko(O|0,0,12)|0;vf(N);S=dd[c[(c[P>>2]|0)+36>>2]&127](T)|0;c[m>>2]=S;i=n;return}}function xk(d,e,f,g,h,i,j,k,l,m,n,o,p,q,r){d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;i=i|0;j=j|0;k=k|0;l=l|0;m=m|0;n=n|0;o=o|0;p=p|0;q=q|0;r=r|0;var s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0,Z=0,_=0,$=0,aa=0,ba=0,ca=0,da=0,ea=0,fa=0,ga=0,ha=0,ia=0,ja=0,ka=0,la=0,ma=0,na=0,oa=0,pa=0,qa=0,ra=0,sa=0,ta=0,ua=0,va=0,wa=0,xa=0,ya=0;c[f>>2]=d;s=j;t=q;u=q+1|0;v=q+8|0;w=q+4|0;q=p;x=(g&512|0)==0;y=p+1|0;z=p+4|0;A=p+8|0;p=(r|0)>0;B=o;C=o+1|0;D=o+8|0;E=o+4|0;o=j+8|0;F=-r|0;G=h;h=0;while(1){L3:do{switch(a[l+h|0]|0){case 4:{H=c[f>>2]|0;I=k?G+1|0:G;L5:do{if(I>>>0<i>>>0){J=I;while(1){K=a[J]|0;if(K<<24>>24<=-1){L=J;break L5}M=J+1|0;if((b[(c[o>>2]|0)+(K<<24>>24<<1)>>1]&2048)==0){L=J;break L5}if(M>>>0<i>>>0){J=M}else{L=M;break}}}else{L=I}}while(0);J=L;if(p){if(L>>>0>I>>>0){M=I+(-J|0)|0;J=M>>>0<F>>>0?F:M;M=J+r|0;K=L;N=r;O=H;while(1){P=K-1|0;Q=a[P]|0;c[f>>2]=O+1;a[O]=Q;Q=N-1|0;R=(Q|0)>0;if(!(P>>>0>I>>>0&R)){break}K=P;N=Q;O=c[f>>2]|0}O=L+J|0;if(R){S=M;T=O;U=34}else{V=0;W=M;X=O}}else{S=r;T=L;U=34}if((U|0)==34){U=0;V=ad[c[(c[s>>2]|0)+28>>2]&31](j,48)|0;W=S;X=T}O=c[f>>2]|0;c[f>>2]=O+1;if((W|0)>0){N=W;K=O;while(1){a[K]=V;Q=N-1|0;P=c[f>>2]|0;c[f>>2]=P+1;if((Q|0)>0){N=Q;K=P}else{Y=P;break}}}else{Y=O}a[Y]=m;Z=X}else{Z=L}if((Z|0)==(I|0)){K=ad[c[(c[s>>2]|0)+28>>2]&31](j,48)|0;N=c[f>>2]|0;c[f>>2]=N+1;a[N]=K}else{K=a[B]|0;N=K&255;if((N&1|0)==0){_=N>>>1}else{_=c[E>>2]|0}if((_|0)==0){$=Z;aa=0;ba=0;ca=-1}else{if((K&1)==0){da=C}else{da=c[D>>2]|0}$=Z;aa=0;ba=0;ca=a[da]|0}while(1){do{if((aa|0)==(ca|0)){K=c[f>>2]|0;c[f>>2]=K+1;a[K]=n;K=ba+1|0;N=a[B]|0;M=N&255;if((M&1|0)==0){ea=M>>>1}else{ea=c[E>>2]|0}if(K>>>0>=ea>>>0){fa=ca;ga=K;ha=0;break}M=(N&1)==0;if(M){ia=C}else{ia=c[D>>2]|0}if((a[ia+K|0]|0)==127){fa=-1;ga=K;ha=0;break}if(M){ja=C}else{ja=c[D>>2]|0}fa=a[ja+K|0]|0;ga=K;ha=0}else{fa=ca;ga=ba;ha=aa}}while(0);K=$-1|0;M=a[K]|0;N=c[f>>2]|0;c[f>>2]=N+1;a[N]=M;if((K|0)==(I|0)){break}else{$=K;aa=ha+1|0;ba=ga;ca=fa}}}O=c[f>>2]|0;if((H|0)==(O|0)){ka=I;break L3}K=O-1|0;if(H>>>0<K>>>0){la=H;ma=K}else{ka=I;break L3}while(1){K=a[la]|0;a[la]=a[ma]|0;a[ma]=K;K=la+1|0;O=ma-1|0;if(K>>>0<O>>>0){la=K;ma=O}else{ka=I;break}}break};case 2:{I=a[q]|0;H=I&255;O=(H&1|0)==0;if(O){na=H>>>1}else{na=c[z>>2]|0}if((na|0)==0|x){ka=G;break L3}if((I&1)==0){oa=y;pa=y}else{I=c[A>>2]|0;oa=I;pa=I}if(O){qa=H>>>1}else{qa=c[z>>2]|0}H=oa+qa|0;O=c[f>>2]|0;if((pa|0)==(H|0)){ra=O}else{I=pa;K=O;while(1){a[K]=a[I]|0;O=I+1|0;M=K+1|0;if((O|0)==(H|0)){ra=M;break}else{I=O;K=M}}}c[f>>2]=ra;ka=G;break};case 0:{c[e>>2]=c[f>>2];ka=G;break};case 1:{c[e>>2]=c[f>>2];K=ad[c[(c[s>>2]|0)+28>>2]&31](j,32)|0;I=c[f>>2]|0;c[f>>2]=I+1;a[I]=K;ka=G;break};case 3:{K=a[t]|0;I=K&255;if((I&1|0)==0){sa=I>>>1}else{sa=c[w>>2]|0}if((sa|0)==0){ka=G;break L3}if((K&1)==0){ta=u}else{ta=c[v>>2]|0}K=a[ta]|0;I=c[f>>2]|0;c[f>>2]=I+1;a[I]=K;ka=G;break};default:{ka=G}}}while(0);K=h+1|0;if(K>>>0<4>>>0){G=ka;h=K}else{break}}h=a[t]|0;t=h&255;ka=(t&1|0)==0;if(ka){ua=t>>>1}else{ua=c[w>>2]|0}if(ua>>>0>1>>>0){if((h&1)==0){va=u;wa=u}else{u=c[v>>2]|0;va=u;wa=u}if(ka){xa=t>>>1}else{xa=c[w>>2]|0}w=va+xa|0;xa=c[f>>2]|0;va=wa+1|0;if((va|0)==(w|0)){ya=xa}else{wa=xa;xa=va;while(1){a[wa]=a[xa]|0;va=wa+1|0;t=xa+1|0;if((t|0)==(w|0)){ya=va;break}else{wa=va;xa=t}}}c[f>>2]=ya}ya=g&176;if((ya|0)==32){c[e>>2]=c[f>>2];return}else if((ya|0)==16){return}else{c[e>>2]=d;return}}function yk(b,e,f,g,h,j,k){b=b|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;var l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0,Z=0,_=0,$=0,aa=0,ba=0,ca=0,da=0;e=i;i=i+32|0;l=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[l>>2];l=e|0;m=e+16|0;n=e+24|0;o=n;p=i;i=i+1|0;i=i+7&-8;q=i;i=i+1|0;i=i+7&-8;r=i;i=i+12|0;i=i+7&-8;s=r;t=i;i=i+12|0;i=i+7&-8;u=t;v=i;i=i+12|0;i=i+7&-8;w=v;x=i;i=i+4|0;i=i+7&-8;y=i;i=i+100|0;i=i+7&-8;z=i;i=i+4|0;i=i+7&-8;A=i;i=i+4|0;i=i+7&-8;B=i;i=i+4|0;i=i+7&-8;Pf(m,h);C=m|0;D=c[C>>2]|0;if((c[8976]|0)!=-1){c[l>>2]=35904;c[l+4>>2]=14;c[l+8>>2]=0;qf(35904,l,96)}l=(c[8977]|0)-1|0;E=c[D+8>>2]|0;do{if((c[D+12>>2]|0)-E>>2>>>0>l>>>0){F=c[E+(l<<2)>>2]|0;if((F|0)==0){break}G=F;H=k;I=k;J=a[I]|0;K=J&255;if((K&1|0)==0){L=K>>>1}else{L=c[k+4>>2]|0}if((L|0)==0){M=0}else{if((J&1)==0){N=H+1|0}else{N=c[k+8>>2]|0}J=a[N]|0;M=J<<24>>24==(ad[c[(c[F>>2]|0)+28>>2]&31](G,45)|0)<<24>>24}c[n>>2]=0;ko(s|0,0,12)|0;ko(u|0,0,12)|0;ko(w|0,0,12)|0;wk(g,M,m,o,p,q,r,t,v,x);F=y|0;J=a[I]|0;K=J&255;O=(K&1|0)==0;if(O){P=K>>>1}else{P=c[k+4>>2]|0}Q=c[x>>2]|0;if((P|0)>(Q|0)){if(O){R=K>>>1}else{R=c[k+4>>2]|0}K=d[w]|0;if((K&1|0)==0){S=K>>>1}else{S=c[v+4>>2]|0}K=d[u]|0;if((K&1|0)==0){T=K>>>1}else{T=c[t+4>>2]|0}U=(R-Q<<1|1)+S+T|0}else{K=d[w]|0;if((K&1|0)==0){V=K>>>1}else{V=c[v+4>>2]|0}K=d[u]|0;if((K&1|0)==0){W=K>>>1}else{W=c[t+4>>2]|0}U=V+2+W|0}K=U+Q|0;do{if(K>>>0>100>>>0){O=Ln(K)|0;if((O|0)!=0){X=O;Y=O;Z=J;break}Yn();X=0;Y=0;Z=a[I]|0}else{X=F;Y=0;Z=J}}while(0);if((Z&1)==0){_=H+1|0;$=H+1|0}else{J=c[k+8>>2]|0;_=J;$=J}J=Z&255;if((J&1|0)==0){aa=J>>>1}else{aa=c[k+4>>2]|0}xk(X,z,A,c[h+4>>2]|0,$,_+aa|0,G,M,o,a[p]|0,a[q]|0,r,t,v,Q);c[B>>2]=c[f>>2];ii(b,B,X,c[z>>2]|0,c[A>>2]|0,h,j);if((Y|0)==0){vf(v);vf(t);vf(r);ba=c[C>>2]|0;ca=ba|0;da=Xe(ca)|0;i=e;return}Mn(Y);vf(v);vf(t);vf(r);ba=c[C>>2]|0;ca=ba|0;da=Xe(ca)|0;i=e;return}}while(0);e=Nc(4)|0;qn(e);Qb(e|0,10192,134)}function zk(a){a=a|0;Ve(a|0);Tn(a);return}function Ak(a){a=a|0;Ve(a|0);return}function Bk(b,e,f,g,j,k,l){b=b|0;e=e|0;f=f|0;g=g|0;j=j|0;k=k|0;l=+l;var m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0;e=i;i=i+544|0;m=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[m>>2];m=e|0;n=e+120|0;o=e+528|0;p=e+536|0;q=p;r=i;i=i+4|0;i=i+7&-8;s=i;i=i+4|0;i=i+7&-8;t=i;i=i+12|0;i=i+7&-8;u=t;v=i;i=i+12|0;i=i+7&-8;w=v;x=i;i=i+12|0;i=i+7&-8;y=x;z=i;i=i+4|0;i=i+7&-8;A=i;i=i+400|0;B=i;i=i+4|0;i=i+7&-8;C=i;i=i+4|0;i=i+7&-8;D=i;i=i+4|0;i=i+7&-8;E=e+16|0;c[n>>2]=E;F=e+128|0;G=cb(E|0,100,3256,(E=i,i=i+8|0,h[E>>3]=l,E)|0)|0;i=E;do{if(G>>>0>99>>>0){do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);H=ni(n,c[8322]|0,3256,(E=i,i=i+8|0,h[E>>3]=l,E)|0)|0;i=E;I=c[n>>2]|0;if((I|0)==0){Yn();J=c[n>>2]|0}else{J=I}I=Ln(H<<2)|0;K=I;if((I|0)!=0){L=K;M=H;N=J;O=K;break}Yn();L=K;M=H;N=J;O=K}else{L=F;M=G;N=0;O=0}}while(0);Pf(o,j);G=o|0;F=c[G>>2]|0;if((c[8974]|0)!=-1){c[m>>2]=35896;c[m+4>>2]=14;c[m+8>>2]=0;qf(35896,m,96)}m=(c[8975]|0)-1|0;J=c[F+8>>2]|0;do{if((c[F+12>>2]|0)-J>>2>>>0>m>>>0){E=c[J+(m<<2)>>2]|0;if((E|0)==0){break}K=E;H=c[n>>2]|0;ld[c[(c[E>>2]|0)+48>>2]&15](K,H,H+M|0,L)|0;if((M|0)==0){P=0}else{P=(a[c[n>>2]|0]|0)==45}c[p>>2]=0;ko(u|0,0,12)|0;ko(w|0,0,12)|0;ko(y|0,0,12)|0;Ck(g,P,o,q,r,s,t,v,x,z);H=A|0;E=c[z>>2]|0;if((M|0)>(E|0)){I=d[y]|0;if((I&1|0)==0){Q=I>>>1}else{Q=c[x+4>>2]|0}I=d[w]|0;if((I&1|0)==0){R=I>>>1}else{R=c[v+4>>2]|0}S=(M-E<<1|1)+Q+R|0}else{I=d[y]|0;if((I&1|0)==0){T=I>>>1}else{T=c[x+4>>2]|0}I=d[w]|0;if((I&1|0)==0){U=I>>>1}else{U=c[v+4>>2]|0}S=T+2+U|0}I=S+E|0;do{if(I>>>0>100>>>0){V=Ln(I<<2)|0;W=V;if((V|0)!=0){X=W;Y=W;break}Yn();X=W;Y=W}else{X=H;Y=0}}while(0);Dk(X,B,C,c[j+4>>2]|0,L,L+(M<<2)|0,K,P,q,c[r>>2]|0,c[s>>2]|0,t,v,x,E);c[D>>2]=c[f>>2];wi(b,D,X,c[B>>2]|0,c[C>>2]|0,j,k);if((Y|0)!=0){Mn(Y)}Gf(x);Gf(v);vf(t);Xe(c[G>>2]|0)|0;if((O|0)!=0){Mn(O)}if((N|0)==0){i=e;return}Mn(N);i=e;return}}while(0);e=Nc(4)|0;qn(e);Qb(e|0,10192,134)}function Ck(b,d,e,f,g,h,j,k,l,m){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;l=l|0;m=m|0;var n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0;n=i;i=i+40|0;o=n|0;p=n+16|0;q=n+32|0;r=q;s=i;i=i+12|0;i=i+7&-8;t=s;u=i;i=i+4|0;i=i+7&-8;v=u;w=i;i=i+12|0;i=i+7&-8;x=w;y=i;i=i+12|0;i=i+7&-8;z=y;A=i;i=i+12|0;i=i+7&-8;B=A;D=i;i=i+4|0;i=i+7&-8;E=D;F=i;i=i+12|0;i=i+7&-8;G=F;H=i;i=i+4|0;i=i+7&-8;I=H;J=i;i=i+12|0;i=i+7&-8;K=J;L=i;i=i+12|0;i=i+7&-8;M=L;N=i;i=i+12|0;i=i+7&-8;O=N;P=c[e>>2]|0;if(b){if((c[9090]|0)!=-1){c[p>>2]=36360;c[p+4>>2]=14;c[p+8>>2]=0;qf(36360,p,96)}p=(c[9091]|0)-1|0;b=c[P+8>>2]|0;if((c[P+12>>2]|0)-b>>2>>>0<=p>>>0){Q=Nc(4)|0;R=Q;qn(R);Qb(Q|0,10192,134)}e=c[b+(p<<2)>>2]|0;if((e|0)==0){Q=Nc(4)|0;R=Q;qn(R);Qb(Q|0,10192,134)}Q=e;R=c[e>>2]|0;if(d){$c[c[R+44>>2]&127](r,Q);r=f;C=c[q>>2]|0;a[r]=C;C=C>>8;a[r+1|0]=C;C=C>>8;a[r+2|0]=C;C=C>>8;a[r+3|0]=C;$c[c[(c[e>>2]|0)+32>>2]&127](s,Q);r=l;if((a[r]&1)==0){c[l+4>>2]=0;a[r]=0}else{c[c[l+8>>2]>>2]=0;c[l+4>>2]=0}Jf(l,0);c[r>>2]=c[t>>2];c[r+4>>2]=c[t+4>>2];c[r+8>>2]=c[t+8>>2];ko(t|0,0,12)|0;Gf(s)}else{$c[c[R+40>>2]&127](v,Q);v=f;C=c[u>>2]|0;a[v]=C;C=C>>8;a[v+1|0]=C;C=C>>8;a[v+2|0]=C;C=C>>8;a[v+3|0]=C;$c[c[(c[e>>2]|0)+28>>2]&127](w,Q);v=l;if((a[v]&1)==0){c[l+4>>2]=0;a[v]=0}else{c[c[l+8>>2]>>2]=0;c[l+4>>2]=0}Jf(l,0);c[v>>2]=c[x>>2];c[v+4>>2]=c[x+4>>2];c[v+8>>2]=c[x+8>>2];ko(x|0,0,12)|0;Gf(w)}w=e;c[g>>2]=dd[c[(c[w>>2]|0)+12>>2]&127](Q)|0;c[h>>2]=dd[c[(c[w>>2]|0)+16>>2]&127](Q)|0;$c[c[(c[e>>2]|0)+20>>2]&127](y,Q);x=j;if((a[x]&1)==0){a[j+1|0]=0;a[x]=0}else{a[c[j+8>>2]|0]=0;c[j+4>>2]=0}zf(j,0);c[x>>2]=c[z>>2];c[x+4>>2]=c[z+4>>2];c[x+8>>2]=c[z+8>>2];ko(z|0,0,12)|0;vf(y);$c[c[(c[e>>2]|0)+24>>2]&127](A,Q);e=k;if((a[e]&1)==0){c[k+4>>2]=0;a[e]=0}else{c[c[k+8>>2]>>2]=0;c[k+4>>2]=0}Jf(k,0);c[e>>2]=c[B>>2];c[e+4>>2]=c[B+4>>2];c[e+8>>2]=c[B+8>>2];ko(B|0,0,12)|0;Gf(A);S=dd[c[(c[w>>2]|0)+36>>2]&127](Q)|0;c[m>>2]=S;i=n;return}else{if((c[9092]|0)!=-1){c[o>>2]=36368;c[o+4>>2]=14;c[o+8>>2]=0;qf(36368,o,96)}o=(c[9093]|0)-1|0;Q=c[P+8>>2]|0;if((c[P+12>>2]|0)-Q>>2>>>0<=o>>>0){T=Nc(4)|0;U=T;qn(U);Qb(T|0,10192,134)}P=c[Q+(o<<2)>>2]|0;if((P|0)==0){T=Nc(4)|0;U=T;qn(U);Qb(T|0,10192,134)}T=P;U=c[P>>2]|0;if(d){$c[c[U+44>>2]&127](E,T);E=f;C=c[D>>2]|0;a[E]=C;C=C>>8;a[E+1|0]=C;C=C>>8;a[E+2|0]=C;C=C>>8;a[E+3|0]=C;$c[c[(c[P>>2]|0)+32>>2]&127](F,T);E=l;if((a[E]&1)==0){c[l+4>>2]=0;a[E]=0}else{c[c[l+8>>2]>>2]=0;c[l+4>>2]=0}Jf(l,0);c[E>>2]=c[G>>2];c[E+4>>2]=c[G+4>>2];c[E+8>>2]=c[G+8>>2];ko(G|0,0,12)|0;Gf(F)}else{$c[c[U+40>>2]&127](I,T);I=f;C=c[H>>2]|0;a[I]=C;C=C>>8;a[I+1|0]=C;C=C>>8;a[I+2|0]=C;C=C>>8;a[I+3|0]=C;$c[c[(c[P>>2]|0)+28>>2]&127](J,T);I=l;if((a[I]&1)==0){c[l+4>>2]=0;a[I]=0}else{c[c[l+8>>2]>>2]=0;c[l+4>>2]=0}Jf(l,0);c[I>>2]=c[K>>2];c[I+4>>2]=c[K+4>>2];c[I+8>>2]=c[K+8>>2];ko(K|0,0,12)|0;Gf(J)}J=P;c[g>>2]=dd[c[(c[J>>2]|0)+12>>2]&127](T)|0;c[h>>2]=dd[c[(c[J>>2]|0)+16>>2]&127](T)|0;$c[c[(c[P>>2]|0)+20>>2]&127](L,T);h=j;if((a[h]&1)==0){a[j+1|0]=0;a[h]=0}else{a[c[j+8>>2]|0]=0;c[j+4>>2]=0}zf(j,0);c[h>>2]=c[M>>2];c[h+4>>2]=c[M+4>>2];c[h+8>>2]=c[M+8>>2];ko(M|0,0,12)|0;vf(L);$c[c[(c[P>>2]|0)+24>>2]&127](N,T);P=k;if((a[P]&1)==0){c[k+4>>2]=0;a[P]=0}else{c[c[k+8>>2]>>2]=0;c[k+4>>2]=0}Jf(k,0);c[P>>2]=c[O>>2];c[P+4>>2]=c[O+4>>2];c[P+8>>2]=c[O+8>>2];ko(O|0,0,12)|0;Gf(N);S=dd[c[(c[J>>2]|0)+36>>2]&127](T)|0;c[m>>2]=S;i=n;return}}function Dk(b,d,e,f,g,h,i,j,k,l,m,n,o,p,q){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;i=i|0;j=j|0;k=k|0;l=l|0;m=m|0;n=n|0;o=o|0;p=p|0;q=q|0;var r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0,Z=0,_=0,$=0,aa=0,ba=0,ca=0,da=0,ea=0,fa=0,ga=0,ha=0,ia=0,ja=0,ka=0,la=0,ma=0,na=0,oa=0,pa=0,qa=0,ra=0,sa=0,ta=0,ua=0,va=0,wa=0;c[e>>2]=b;r=i;s=p;t=p+4|0;u=p+8|0;p=o;v=(f&512|0)==0;w=o+4|0;x=o+8|0;o=(q|0)>0;y=n;z=n+1|0;A=n+8|0;B=n+4|0;n=i;C=g;g=0;while(1){L3:do{switch(a[k+g|0]|0){case 1:{c[d>>2]=c[e>>2];D=ad[c[(c[r>>2]|0)+44>>2]&31](i,32)|0;E=c[e>>2]|0;c[e>>2]=E+4;c[E>>2]=D;F=C;break};case 3:{D=a[s]|0;E=D&255;if((E&1|0)==0){G=E>>>1}else{G=c[t>>2]|0}if((G|0)==0){F=C;break L3}if((D&1)==0){H=t}else{H=c[u>>2]|0}D=c[H>>2]|0;E=c[e>>2]|0;c[e>>2]=E+4;c[E>>2]=D;F=C;break};case 0:{c[d>>2]=c[e>>2];F=C;break};case 2:{D=a[p]|0;E=D&255;I=(E&1|0)==0;if(I){J=E>>>1}else{J=c[w>>2]|0}if((J|0)==0|v){F=C;break L3}if((D&1)==0){K=w;L=w;M=w}else{D=c[x>>2]|0;K=D;L=D;M=D}if(I){N=E>>>1}else{N=c[w>>2]|0}E=K+(N<<2)|0;I=c[e>>2]|0;if((L|0)==(E|0)){O=I}else{D=(K+(N-1<<2)+(-M|0)|0)>>>2;P=L;Q=I;while(1){c[Q>>2]=c[P>>2];R=P+4|0;if((R|0)==(E|0)){break}P=R;Q=Q+4|0}O=I+(D+1<<2)|0}c[e>>2]=O;F=C;break};case 4:{Q=c[e>>2]|0;P=j?C+4|0:C;L37:do{if(P>>>0<h>>>0){E=P;while(1){R=E+4|0;if(!(ed[c[(c[n>>2]|0)+12>>2]&63](i,2048,c[E>>2]|0)|0)){S=E;break L37}if(R>>>0<h>>>0){E=R}else{S=R;break}}}else{S=P}}while(0);if(o){if(S>>>0>P>>>0){D=S;I=q;do{D=D-4|0;E=c[D>>2]|0;R=c[e>>2]|0;c[e>>2]=R+4;c[R>>2]=E;I=I-1|0;T=(I|0)>0;}while(D>>>0>P>>>0&T);if(T){U=I;V=D;W=35}else{X=0;Y=I;Z=D}}else{U=q;V=S;W=35}if((W|0)==35){W=0;X=ad[c[(c[r>>2]|0)+44>>2]&31](i,48)|0;Y=U;Z=V}E=c[e>>2]|0;c[e>>2]=E+4;if((Y|0)>0){R=Y;_=E;while(1){c[_>>2]=X;$=R-1|0;aa=c[e>>2]|0;c[e>>2]=aa+4;if(($|0)>0){R=$;_=aa}else{ba=aa;break}}}else{ba=E}c[ba>>2]=l;ca=Z}else{ca=S}if((ca|0)==(P|0)){_=ad[c[(c[r>>2]|0)+44>>2]&31](i,48)|0;R=c[e>>2]|0;c[e>>2]=R+4;c[R>>2]=_}else{_=a[y]|0;R=_&255;if((R&1|0)==0){da=R>>>1}else{da=c[B>>2]|0}if((da|0)==0){ea=ca;fa=0;ga=0;ha=-1}else{if((_&1)==0){ia=z}else{ia=c[A>>2]|0}ea=ca;fa=0;ga=0;ha=a[ia]|0}while(1){do{if((fa|0)==(ha|0)){_=c[e>>2]|0;c[e>>2]=_+4;c[_>>2]=m;_=ga+1|0;R=a[y]|0;D=R&255;if((D&1|0)==0){ja=D>>>1}else{ja=c[B>>2]|0}if(_>>>0>=ja>>>0){ka=ha;la=_;ma=0;break}D=(R&1)==0;if(D){na=z}else{na=c[A>>2]|0}if((a[na+_|0]|0)==127){ka=-1;la=_;ma=0;break}if(D){oa=z}else{oa=c[A>>2]|0}ka=a[oa+_|0]|0;la=_;ma=0}else{ka=ha;la=ga;ma=fa}}while(0);_=ea-4|0;D=c[_>>2]|0;R=c[e>>2]|0;c[e>>2]=R+4;c[R>>2]=D;if((_|0)==(P|0)){break}else{ea=_;fa=ma+1|0;ga=la;ha=ka}}}E=c[e>>2]|0;if((Q|0)==(E|0)){F=P;break L3}_=E-4|0;if(Q>>>0<_>>>0){pa=Q;qa=_}else{F=P;break L3}while(1){_=c[pa>>2]|0;c[pa>>2]=c[qa>>2];c[qa>>2]=_;_=pa+4|0;E=qa-4|0;if(_>>>0<E>>>0){pa=_;qa=E}else{F=P;break}}break};default:{F=C}}}while(0);P=g+1|0;if(P>>>0<4>>>0){C=F;g=P}else{break}}g=a[s]|0;s=g&255;F=(s&1|0)==0;if(F){ra=s>>>1}else{ra=c[t>>2]|0}if(ra>>>0>1>>>0){if((g&1)==0){sa=t;ta=t;ua=t}else{g=c[u>>2]|0;sa=g;ta=g;ua=g}if(F){va=s>>>1}else{va=c[t>>2]|0}t=sa+(va<<2)|0;s=c[e>>2]|0;F=ta+4|0;if((F|0)==(t|0)){wa=s}else{ta=((sa+(va-2<<2)+(-ua|0)|0)>>>2)+1|0;ua=s;va=F;while(1){c[ua>>2]=c[va>>2];F=va+4|0;if((F|0)==(t|0)){break}else{ua=ua+4|0;va=F}}wa=s+(ta<<2)|0}c[e>>2]=wa}wa=f&176;if((wa|0)==32){c[d>>2]=c[e>>2];return}else if((wa|0)==16){return}else{c[d>>2]=b;return}}function Ek(b,e,f,g,h,j,k){b=b|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;var l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0,Z=0,_=0,$=0,aa=0,ba=0,ca=0,da=0;e=i;i=i+32|0;l=f;f=i;i=i+4|0;i=i+7&-8;c[f>>2]=c[l>>2];l=e|0;m=e+16|0;n=e+24|0;o=n;p=i;i=i+4|0;i=i+7&-8;q=i;i=i+4|0;i=i+7&-8;r=i;i=i+12|0;i=i+7&-8;s=r;t=i;i=i+12|0;i=i+7&-8;u=t;v=i;i=i+12|0;i=i+7&-8;w=v;x=i;i=i+4|0;i=i+7&-8;y=i;i=i+400|0;z=i;i=i+4|0;i=i+7&-8;A=i;i=i+4|0;i=i+7&-8;B=i;i=i+4|0;i=i+7&-8;Pf(m,h);C=m|0;D=c[C>>2]|0;if((c[8974]|0)!=-1){c[l>>2]=35896;c[l+4>>2]=14;c[l+8>>2]=0;qf(35896,l,96)}l=(c[8975]|0)-1|0;E=c[D+8>>2]|0;do{if((c[D+12>>2]|0)-E>>2>>>0>l>>>0){F=c[E+(l<<2)>>2]|0;if((F|0)==0){break}G=F;H=k;I=a[H]|0;J=I&255;if((J&1|0)==0){K=J>>>1}else{K=c[k+4>>2]|0}if((K|0)==0){L=0}else{if((I&1)==0){M=k+4|0}else{M=c[k+8>>2]|0}I=c[M>>2]|0;L=(I|0)==(ad[c[(c[F>>2]|0)+44>>2]&31](G,45)|0)}c[n>>2]=0;ko(s|0,0,12)|0;ko(u|0,0,12)|0;ko(w|0,0,12)|0;Ck(g,L,m,o,p,q,r,t,v,x);F=y|0;I=a[H]|0;J=I&255;N=(J&1|0)==0;if(N){O=J>>>1}else{O=c[k+4>>2]|0}P=c[x>>2]|0;if((O|0)>(P|0)){if(N){Q=J>>>1}else{Q=c[k+4>>2]|0}J=d[w]|0;if((J&1|0)==0){R=J>>>1}else{R=c[v+4>>2]|0}J=d[u]|0;if((J&1|0)==0){S=J>>>1}else{S=c[t+4>>2]|0}T=(Q-P<<1|1)+R+S|0}else{J=d[w]|0;if((J&1|0)==0){U=J>>>1}else{U=c[v+4>>2]|0}J=d[u]|0;if((J&1|0)==0){V=J>>>1}else{V=c[t+4>>2]|0}T=U+2+V|0}J=T+P|0;do{if(J>>>0>100>>>0){N=Ln(J<<2)|0;W=N;if((N|0)!=0){X=W;Y=W;Z=I;break}Yn();X=W;Y=W;Z=a[H]|0}else{X=F;Y=0;Z=I}}while(0);if((Z&1)==0){_=k+4|0;$=k+4|0}else{I=c[k+8>>2]|0;_=I;$=I}I=Z&255;if((I&1|0)==0){aa=I>>>1}else{aa=c[k+4>>2]|0}Dk(X,z,A,c[h+4>>2]|0,$,_+(aa<<2)|0,G,L,o,c[p>>2]|0,c[q>>2]|0,r,t,v,P);c[B>>2]=c[f>>2];wi(b,B,X,c[z>>2]|0,c[A>>2]|0,h,j);if((Y|0)==0){Gf(v);Gf(t);vf(r);ba=c[C>>2]|0;ca=ba|0;da=Xe(ca)|0;i=e;return}Mn(Y);Gf(v);Gf(t);vf(r);ba=c[C>>2]|0;ca=ba|0;da=Xe(ca)|0;i=e;return}}while(0);e=Nc(4)|0;qn(e);Qb(e|0,10192,134)}function Fk(a){a=a|0;Ve(a|0);Tn(a);return}function Gk(a){a=a|0;Ve(a|0);return}function Hk(b,d,e){b=b|0;d=d|0;e=e|0;var f=0;if((a[d]&1)==0){f=d+1|0}else{f=c[d+8>>2]|0}d=Rc(f|0,1)|0;return d>>>(((d|0)!=-1|0)>>>0)|0}function Ik(b,d,e,f,g,h){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0;d=i;i=i+16|0;j=d|0;k=j;ko(k|0,0,12)|0;l=b;m=h;n=a[h]|0;if((n&1)==0){o=m+1|0;p=m+1|0}else{m=c[h+8>>2]|0;o=m;p=m}m=n&255;if((m&1|0)==0){q=m>>>1}else{q=c[h+4>>2]|0}h=o+q|0;do{if(p>>>0<h>>>0){q=p;do{Af(j,a[q]|0);q=q+1|0;}while(q>>>0<h>>>0);q=(e|0)==-1?-1:e<<1;if((a[k]&1)==0){r=q;s=16;break}t=c[j+8>>2]|0;u=q}else{r=(e|0)==-1?-1:e<<1;s=16}}while(0);if((s|0)==16){t=j+1|0;u=r}r=qb(u|0,f|0,g|0,t|0)|0;ko(l|0,0,12)|0;l=ho(r|0)|0;t=r+l|0;if((l|0)>0){v=r}else{vf(j);i=d;return}do{Af(b,a[v]|0);v=v+1|0;}while(v>>>0<t>>>0);vf(j);i=d;return}function Jk(a,b){a=a|0;b=b|0;nc(((b|0)==-1?-1:b<<1)|0)|0;return}function Kk(a){a=a|0;Ve(a|0);Tn(a);return}function Lk(a){a=a|0;Ve(a|0);return}function Mk(b,d,e){b=b|0;d=d|0;e=e|0;var f=0;if((a[d]&1)==0){f=d+1|0}else{f=c[d+8>>2]|0}d=Rc(f|0,1)|0;return d>>>(((d|0)!=-1|0)>>>0)|0}function Nk(b,d,e,f,g,h){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0;d=i;i=i+224|0;j=d|0;k=d+8|0;l=d+40|0;m=d+48|0;n=d+56|0;o=d+64|0;p=d+192|0;q=d+200|0;r=d+208|0;s=r;t=i;i=i+8|0;u=i;i=i+8|0;ko(s|0,0,12)|0;v=b;w=j;x=n;y=t|0;c[t+4>>2]=0;c[t>>2]=6168;z=a[h]|0;if((z&1)==0){A=h+4|0;B=h+4|0}else{C=c[h+8>>2]|0;A=C;B=C}C=z&255;if((C&1|0)==0){D=C>>>1}else{D=c[h+4>>2]|0}h=A+(D<<2)|0;c[j>>2]=0;c[j+4>>2]=0;L9:do{if(B>>>0<h>>>0){j=t;D=k|0;A=k+32|0;C=B;z=6168;while(1){c[m>>2]=C;E=(id[c[z+12>>2]&31](y,w,C,h,m,D,A,l)|0)==2;F=c[m>>2]|0;if(E|(F|0)==(C|0)){break}if(D>>>0<(c[l>>2]|0)>>>0){E=D;do{Af(r,a[E]|0);E=E+1|0;}while(E>>>0<(c[l>>2]|0)>>>0);G=c[m>>2]|0}else{G=F}if(G>>>0>=h>>>0){break L9}C=G;z=c[j>>2]|0}j=Nc(8)|0;bf(j,2544);Qb(j|0,10208,26)}}while(0);Ve(t|0);if((a[s]&1)==0){H=r+1|0}else{H=c[r+8>>2]|0}s=qb(((e|0)==-1?-1:e<<1)|0,f|0,g|0,H|0)|0;ko(v|0,0,12)|0;v=u|0;c[u+4>>2]=0;c[u>>2]=6112;H=ho(s|0)|0;g=s+H|0;c[n>>2]=0;c[n+4>>2]=0;if((H|0)<1){I=u|0;Ve(I);vf(r);i=d;return}H=u;n=g;f=o|0;e=o+128|0;o=s;s=6112;while(1){c[q>>2]=o;t=(id[c[s+16>>2]&31](v,x,o,(n-o|0)>32?o+32|0:g,q,f,e,p)|0)==2;G=c[q>>2]|0;if(t|(G|0)==(o|0)){break}if(f>>>0<(c[p>>2]|0)>>>0){t=f;do{Kf(b,c[t>>2]|0);t=t+4|0;}while(t>>>0<(c[p>>2]|0)>>>0);J=c[q>>2]|0}else{J=G}if(J>>>0>=g>>>0){K=51;break}o=J;s=c[H>>2]|0}if((K|0)==51){I=u|0;Ve(I);vf(r);i=d;return}d=Nc(8)|0;bf(d,2544);Qb(d|0,10208,26)}function Ok(a,b){a=a|0;b=b|0;nc(((b|0)==-1?-1:b<<1)|0)|0;return}function Pk(b){b=b|0;var d=0,e=0,f=0;c[b>>2]=5632;d=b+8|0;e=c[d>>2]|0;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);if((e|0)==(c[8322]|0)){f=b|0;Ve(f);return}pb(c[d>>2]|0);f=b|0;Ve(f);return}function Qk(a){a=a|0;a=Nc(8)|0;Ye(a,3240);c[a>>2]=4568;Qb(a|0,10224,36)}function Rk(b,d){b=b|0;d=d|0;var e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0;e=i;i=i+448|0;f=e|0;g=e+16|0;h=e+32|0;j=e+48|0;k=e+64|0;l=e+80|0;m=e+96|0;n=e+112|0;o=e+128|0;p=e+144|0;q=e+160|0;r=e+176|0;s=e+192|0;t=e+208|0;u=e+224|0;v=e+240|0;w=e+256|0;x=e+272|0;y=e+288|0;z=e+304|0;A=e+320|0;B=e+336|0;C=e+352|0;D=e+368|0;E=e+384|0;F=e+400|0;G=e+416|0;H=e+432|0;c[b+4>>2]=d-1;c[b>>2]=5888;d=b+8|0;I=b+12|0;a[b+136|0]=1;J=b+24|0;K=J;c[I>>2]=K;c[d>>2]=K;c[b+16>>2]=J+112;J=28;L=K;do{if((L|0)==0){M=0}else{c[L>>2]=0;M=c[I>>2]|0}L=M+4|0;c[I>>2]=L;J=J-1|0;}while((J|0)!=0);tf(b+144|0,3224,1);J=c[d>>2]|0;d=c[I>>2]|0;if((J|0)!=(d|0)){c[I>>2]=d+(~((d-4+(-J|0)|0)>>>2)<<2)}c[8379]=0;c[8378]=5592;if((c[8896]|0)!=-1){c[H>>2]=35584;c[H+4>>2]=14;c[H+8>>2]=0;qf(35584,H,96)}Sk(b,33512,(c[8897]|0)-1|0);c[8377]=0;c[8376]=5552;if((c[8894]|0)!=-1){c[G>>2]=35576;c[G+4>>2]=14;c[G+8>>2]=0;qf(35576,G,96)}Sk(b,33504,(c[8895]|0)-1|0);c[8433]=0;c[8432]=6e3;c[8434]=0;a[33740]=0;c[8434]=c[(ob()|0)>>2];if((c[8976]|0)!=-1){c[F>>2]=35904;c[F+4>>2]=14;c[F+8>>2]=0;qf(35904,F,96)}Sk(b,33728,(c[8977]|0)-1|0);c[8431]=0;c[8430]=5920;if((c[8974]|0)!=-1){c[E>>2]=35896;c[E+4>>2]=14;c[E+8>>2]=0;qf(35896,E,96)}Sk(b,33720,(c[8975]|0)-1|0);c[8385]=0;c[8384]=5688;if((c[8900]|0)!=-1){c[D>>2]=35600;c[D+4>>2]=14;c[D+8>>2]=0;qf(35600,D,96)}Sk(b,33536,(c[8901]|0)-1|0);c[8381]=0;c[8380]=5632;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);c[8382]=c[8322];if((c[8898]|0)!=-1){c[C>>2]=35592;c[C+4>>2]=14;c[C+8>>2]=0;qf(35592,C,96)}Sk(b,33520,(c[8899]|0)-1|0);c[8387]=0;c[8386]=5744;if((c[8902]|0)!=-1){c[B>>2]=35608;c[B+4>>2]=14;c[B+8>>2]=0;qf(35608,B,96)}Sk(b,33544,(c[8903]|0)-1|0);c[8389]=0;c[8388]=5800;if((c[8904]|0)!=-1){c[A>>2]=35616;c[A+4>>2]=14;c[A+8>>2]=0;qf(35616,A,96)}Sk(b,33552,(c[8905]|0)-1|0);c[8359]=0;c[8358]=5096;a[33440]=46;a[33441]=44;ko(33444,0,12)|0;if((c[8880]|0)!=-1){c[z>>2]=35520;c[z+4>>2]=14;c[z+8>>2]=0;qf(35520,z,96)}Sk(b,33432,(c[8881]|0)-1|0);c[8351]=0;c[8350]=5048;c[8352]=46;c[8353]=44;ko(33416,0,12)|0;if((c[8878]|0)!=-1){c[y>>2]=35512;c[y+4>>2]=14;c[y+8>>2]=0;qf(35512,y,96)}Sk(b,33400,(c[8879]|0)-1|0);c[8375]=0;c[8374]=5480;if((c[8892]|0)!=-1){c[x>>2]=35568;c[x+4>>2]=14;c[x+8>>2]=0;qf(35568,x,96)}Sk(b,33496,(c[8893]|0)-1|0);c[8373]=0;c[8372]=5408;if((c[8890]|0)!=-1){c[w>>2]=35560;c[w+4>>2]=14;c[w+8>>2]=0;qf(35560,w,96)}Sk(b,33488,(c[8891]|0)-1|0);c[8371]=0;c[8370]=5344;if((c[8888]|0)!=-1){c[v>>2]=35552;c[v+4>>2]=14;c[v+8>>2]=0;qf(35552,v,96)}Sk(b,33480,(c[8889]|0)-1|0);c[8369]=0;c[8368]=5280;if((c[8886]|0)!=-1){c[u>>2]=35544;c[u+4>>2]=14;c[u+8>>2]=0;qf(35544,u,96)}Sk(b,33472,(c[8887]|0)-1|0);c[8443]=0;c[8442]=6928;if((c[9096]|0)!=-1){c[t>>2]=36384;c[t+4>>2]=14;c[t+8>>2]=0;qf(36384,t,96)}Sk(b,33768,(c[9097]|0)-1|0);c[8441]=0;c[8440]=6864;if((c[9094]|0)!=-1){c[s>>2]=36376;c[s+4>>2]=14;c[s+8>>2]=0;qf(36376,s,96)}Sk(b,33760,(c[9095]|0)-1|0);c[8439]=0;c[8438]=6800;if((c[9092]|0)!=-1){c[r>>2]=36368;c[r+4>>2]=14;c[r+8>>2]=0;qf(36368,r,96)}Sk(b,33752,(c[9093]|0)-1|0);c[8437]=0;c[8436]=6736;if((c[9090]|0)!=-1){c[q>>2]=36360;c[q+4>>2]=14;c[q+8>>2]=0;qf(36360,q,96)}Sk(b,33744,(c[9091]|0)-1|0);c[8333]=0;c[8332]=4752;if((c[8868]|0)!=-1){c[p>>2]=35472;c[p+4>>2]=14;c[p+8>>2]=0;qf(35472,p,96)}Sk(b,33328,(c[8869]|0)-1|0);c[8331]=0;c[8330]=4712;if((c[8866]|0)!=-1){c[o>>2]=35464;c[o+4>>2]=14;c[o+8>>2]=0;qf(35464,o,96)}Sk(b,33320,(c[8867]|0)-1|0);c[8329]=0;c[8328]=4672;if((c[8864]|0)!=-1){c[n>>2]=35456;c[n+4>>2]=14;c[n+8>>2]=0;qf(35456,n,96)}Sk(b,33312,(c[8865]|0)-1|0);c[8327]=0;c[8326]=4632;if((c[8862]|0)!=-1){c[m>>2]=35448;c[m+4>>2]=14;c[m+8>>2]=0;qf(35448,m,96)}Sk(b,33304,(c[8863]|0)-1|0);c[8347]=0;c[8346]=4952;c[8348]=5e3;if((c[8876]|0)!=-1){c[l>>2]=35504;c[l+4>>2]=14;c[l+8>>2]=0;qf(35504,l,96)}Sk(b,33384,(c[8877]|0)-1|0);c[8343]=0;c[8342]=4856;c[8344]=4904;if((c[8874]|0)!=-1){c[k>>2]=35496;c[k+4>>2]=14;c[k+8>>2]=0;qf(35496,k,96)}Sk(b,33368,(c[8875]|0)-1|0);c[8339]=0;c[8338]=5856;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);c[8340]=c[8322];c[8338]=4824;if((c[8872]|0)!=-1){c[j>>2]=35488;c[j+4>>2]=14;c[j+8>>2]=0;qf(35488,j,96)}Sk(b,33352,(c[8873]|0)-1|0);c[8335]=0;c[8334]=5856;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);c[8336]=c[8322];c[8334]=4792;if((c[8870]|0)!=-1){c[h>>2]=35480;c[h+4>>2]=14;c[h+8>>2]=0;qf(35480,h,96)}Sk(b,33336,(c[8871]|0)-1|0);c[8367]=0;c[8366]=5184;if((c[8884]|0)!=-1){c[g>>2]=35536;c[g+4>>2]=14;c[g+8>>2]=0;qf(35536,g,96)}Sk(b,33464,(c[8885]|0)-1|0);c[8365]=0;c[8364]=5144;if((c[8882]|0)!=-1){c[f>>2]=35528;c[f+4>>2]=14;c[f+8>>2]=0;qf(35528,f,96)}Sk(b,33456,(c[8883]|0)-1|0);i=e;return}function Sk(a,b,d){a=a|0;b=b|0;d=d|0;var e=0,f=0,g=0,h=0,i=0,j=0,k=0,l=0,m=0,n=0;We(b|0);e=a+8|0;f=a+12|0;a=c[f>>2]|0;g=e|0;h=c[g>>2]|0;i=a-h>>2;do{if(i>>>0>d>>>0){j=h}else{k=d+1|0;if(i>>>0<k>>>0){Wm(e,k-i|0);j=c[g>>2]|0;break}if(i>>>0<=k>>>0){j=h;break}l=h+(k<<2)|0;if((l|0)==(a|0)){j=h;break}c[f>>2]=a+(~((a-4+(-l|0)|0)>>>2)<<2);j=h}}while(0);h=c[j+(d<<2)>>2]|0;if((h|0)==0){m=j;n=m+(d<<2)|0;c[n>>2]=b;return}Xe(h|0)|0;m=c[g>>2]|0;n=m+(d<<2)|0;c[n>>2]=b;return}function Tk(a){a=a|0;Uk(a);Tn(a);return}function Uk(b){b=b|0;var d=0,e=0,f=0,g=0,h=0,i=0,j=0,k=0,l=0;c[b>>2]=5888;d=b+12|0;e=c[d>>2]|0;f=b+8|0;g=c[f>>2]|0;if((e|0)!=(g|0)){h=0;i=g;g=e;while(1){e=c[i+(h<<2)>>2]|0;if((e|0)==0){j=g;k=i}else{Xe(e|0)|0;j=c[d>>2]|0;k=c[f>>2]|0}e=h+1|0;if(e>>>0<j-k>>2>>>0){h=e;i=k;g=j}else{break}}}vf(b+144|0);j=c[f>>2]|0;if((j|0)==0){l=b|0;Ve(l);return}f=c[d>>2]|0;if((j|0)!=(f|0)){c[d>>2]=f+(~((f-4+(-j|0)|0)>>>2)<<2)}if((j|0)==(b+24|0)){a[b+136|0]=0;l=b|0;Ve(l);return}else{Tn(j);l=b|0;Ve(l);return}}function Vk(){var b=0,d=0;if((a[36448]|0)!=0){b=c[8314]|0;return b|0}if((yb(36448)|0)==0){b=c[8314]|0;return b|0}do{if((a[36456]|0)==0){if((yb(36456)|0)==0){break}Rk(33560,1);c[8318]=33560;c[8316]=33272}}while(0);d=c[c[8316]>>2]|0;c[8320]=d;We(d|0);c[8314]=33280;b=c[8314]|0;return b|0}function Wk(a){a=a|0;var b=0;b=c[(Vk()|0)>>2]|0;c[a>>2]=b;We(b|0);return}function Xk(a,b){a=a|0;b=b|0;var d=0;d=c[b>>2]|0;c[a>>2]=d;We(d|0);return}function Yk(a){a=a|0;Xe(c[a>>2]|0)|0;return}function Zk(a,b){a=a|0;b=b|0;var d=0,e=0,f=0,g=0,h=0;d=i;i=i+16|0;e=d|0;f=c[a>>2]|0;a=b|0;if((c[a>>2]|0)!=-1){c[e>>2]=b;c[e+4>>2]=14;c[e+8>>2]=0;qf(a,e,96)}e=(c[b+4>>2]|0)-1|0;b=c[f+8>>2]|0;if((c[f+12>>2]|0)-b>>2>>>0<=e>>>0){g=Nc(4)|0;h=g;qn(h);Qb(g|0,10192,134);return 0}f=c[b+(e<<2)>>2]|0;if((f|0)==0){g=Nc(4)|0;h=g;qn(h);Qb(g|0,10192,134);return 0}else{i=d;return f|0}return 0}function _k(a){a=a|0;Ve(a|0);Tn(a);return}function $k(a){a=a|0;if((a|0)==0){return}_c[c[(c[a>>2]|0)+4>>2]&511](a);return}function al(a){a=a|0;c[a+4>>2]=(I=c[8906]|0,c[8906]=I+1,I)+1;return}function bl(a){a=a|0;Ve(a|0);Tn(a);return}function cl(a,d,e){a=a|0;d=d|0;e=e|0;var f=0;if(e>>>0>=128>>>0){f=0;return f|0}f=(b[(c[(ob()|0)>>2]|0)+(e<<1)>>1]&d)<<16>>16!=0;return f|0}function dl(a,d,e,f){a=a|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,i=0,j=0;if((d|0)==(e|0)){g=d;return g|0}else{h=d;i=f}while(1){f=c[h>>2]|0;if(f>>>0<128>>>0){j=b[(c[(ob()|0)>>2]|0)+(f<<1)>>1]|0}else{j=0}b[i>>1]=j;f=h+4|0;if((f|0)==(e|0)){g=e;break}else{h=f;i=i+2|0}}return g|0}function el(a,d,e,f){a=a|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,i=0;if((e|0)==(f|0)){g=e;return g|0}else{h=e}while(1){e=c[h>>2]|0;if(e>>>0<128>>>0){if((b[(c[(ob()|0)>>2]|0)+(e<<1)>>1]&d)<<16>>16!=0){g=h;i=8;break}}e=h+4|0;if((e|0)==(f|0)){g=f;i=9;break}else{h=e}}if((i|0)==9){return g|0}else if((i|0)==8){return g|0}return 0}function fl(a,d,e,f){a=a|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,i=0;L1:do{if((e|0)==(f|0)){g=e}else{a=e;while(1){h=c[a>>2]|0;if(h>>>0>=128>>>0){g=a;break L1}i=a+4|0;if((b[(c[(ob()|0)>>2]|0)+(h<<1)>>1]&d)<<16>>16==0){g=a;break L1}if((i|0)==(f|0)){g=f;break}else{a=i}}}}while(0);return g|0}function gl(a,b){a=a|0;b=b|0;var d=0;if(b>>>0>=128>>>0){d=b;return d|0}d=c[(c[(Sc()|0)>>2]|0)+(b<<2)>>2]|0;return d|0}function hl(a,b,d){a=a|0;b=b|0;d=d|0;var e=0,f=0,g=0;if((b|0)==(d|0)){e=b;return e|0}else{f=b}while(1){b=c[f>>2]|0;if(b>>>0<128>>>0){g=c[(c[(Sc()|0)>>2]|0)+(b<<2)>>2]|0}else{g=b}c[f>>2]=g;b=f+4|0;if((b|0)==(d|0)){e=d;break}else{f=b}}return e|0}function il(a,b){a=a|0;b=b|0;var d=0;if(b>>>0>=128>>>0){d=b;return d|0}d=c[(c[(Tc()|0)>>2]|0)+(b<<2)>>2]|0;return d|0}function jl(a,b,d){a=a|0;b=b|0;d=d|0;var e=0,f=0,g=0;if((b|0)==(d|0)){e=b;return e|0}else{f=b}while(1){b=c[f>>2]|0;if(b>>>0<128>>>0){g=c[(c[(Tc()|0)>>2]|0)+(b<<2)>>2]|0}else{g=b}c[f>>2]=g;b=f+4|0;if((b|0)==(d|0)){e=d;break}else{f=b}}return e|0}function kl(a,b){a=a|0;b=b|0;return b<<24>>24|0}function ll(b,d,e,f){b=b|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,i=0;if((d|0)==(e|0)){g=d;return g|0}else{h=d;i=f}while(1){c[i>>2]=a[h]|0;f=h+1|0;if((f|0)==(e|0)){g=e;break}else{h=f;i=i+4|0}}return g|0}function ml(a,b,c){a=a|0;b=b|0;c=c|0;return(b>>>0<128>>>0?b&255:c)|0}function nl(b,d,e,f,g){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;var h=0,i=0,j=0;if((d|0)==(e|0)){h=d;return h|0}b=((e-4+(-d|0)|0)>>>2)+1|0;i=d;j=g;while(1){g=c[i>>2]|0;a[j]=g>>>0<128>>>0?g&255:f;g=i+4|0;if((g|0)==(e|0)){break}else{i=g;j=j+1|0}}h=d+(b<<2)|0;return h|0}function ol(b){b=b|0;var d=0;c[b>>2]=6e3;d=c[b+8>>2]|0;do{if((d|0)!=0){if((a[b+12|0]&1)==0){break}Un(d)}}while(0);Ve(b|0);Tn(b);return}function pl(b){b=b|0;var d=0;c[b>>2]=6e3;d=c[b+8>>2]|0;do{if((d|0)!=0){if((a[b+12|0]&1)==0){break}Un(d)}}while(0);Ve(b|0);return}function ql(a,b){a=a|0;b=b|0;var d=0;if(b<<24>>24<=-1){d=b;return d|0}d=c[(c[(Sc()|0)>>2]|0)+((b&255)<<2)>>2]&255;return d|0}function rl(b,d,e){b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0;if((d|0)==(e|0)){f=d;return f|0}else{g=d}while(1){d=a[g]|0;if(d<<24>>24>-1){h=c[(c[(Sc()|0)>>2]|0)+(d<<24>>24<<2)>>2]&255}else{h=d}a[g]=h;d=g+1|0;if((d|0)==(e|0)){f=e;break}else{g=d}}return f|0}function sl(a,b){a=a|0;b=b|0;var d=0;if(b<<24>>24<=-1){d=b;return d|0}d=c[(c[(Tc()|0)>>2]|0)+(b<<24>>24<<2)>>2]&255;return d|0}function tl(b,d,e){b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0;if((d|0)==(e|0)){f=d;return f|0}else{g=d}while(1){d=a[g]|0;if(d<<24>>24>-1){h=c[(c[(Tc()|0)>>2]|0)+(d<<24>>24<<2)>>2]&255}else{h=d}a[g]=h;d=g+1|0;if((d|0)==(e|0)){f=e;break}else{g=d}}return f|0}function ul(a,b){a=a|0;b=b|0;return b|0}function vl(b,c,d,e){b=b|0;c=c|0;d=d|0;e=e|0;var f=0,g=0,h=0;if((c|0)==(d|0)){f=c;return f|0}else{g=c;h=e}while(1){a[h]=a[g]|0;e=g+1|0;if((e|0)==(d|0)){f=d;break}else{g=e;h=h+1|0}}return f|0}function wl(a,b,c){a=a|0;b=b|0;c=c|0;return(b<<24>>24>-1?b:c)|0}function xl(b,c,d,e,f){b=b|0;c=c|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,i=0;if((c|0)==(d|0)){g=c;return g|0}else{h=c;i=f}while(1){f=a[h]|0;a[i]=f<<24>>24>-1?f:e;f=h+1|0;if((f|0)==(d|0)){g=d;break}else{h=f;i=i+1|0}}return g|0}function yl(a){a=a|0;Ve(a|0);Tn(a);return}function zl(a,b,d,e,f,g,h,i){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;i=i|0;c[f>>2]=d;c[i>>2]=g;return 3}function Al(a,b,d,e,f,g,h,i){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;i=i|0;c[f>>2]=d;c[i>>2]=g;return 3}function Bl(a,b,d,e,f){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;c[f>>2]=d;return 3}function Cl(a){a=a|0;return 1}function Dl(a){a=a|0;return 1}function El(a,b,c,d,e){a=a|0;b=b|0;c=c|0;d=d|0;e=e|0;b=d-c|0;return(b>>>0<e>>>0?b:e)|0}function Fl(a){a=a|0;return 1}function Gl(a){a=a|0;Pk(a);Tn(a);return}function Hl(b,d,e,f,g,h,j,k){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;var l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0;l=i;i=i+8|0;m=l|0;n=m;o=i;i=i+4|0;i=i+7&-8;p=(e|0)==(f|0);L1:do{if(p){c[k>>2]=h;c[g>>2]=e;q=e}else{r=e;while(1){s=r+4|0;if((c[r>>2]|0)==0){t=r;break}if((s|0)==(f|0)){t=f;break}else{r=s}}c[k>>2]=h;c[g>>2]=e;if(p|(h|0)==(j|0)){q=e;break}r=d;s=j;u=b+8|0;v=o|0;w=h;x=e;y=t;while(1){z=c[r+4>>2]|0;c[m>>2]=c[r>>2];c[m+4>>2]=z;z=uc(c[u>>2]|0)|0;A=hn(w,g,y-x>>2,s-w|0,d)|0;if((z|0)!=0){uc(z|0)|0}if((A|0)==0){B=1;C=55;break}else if((A|0)==(-1|0)){C=16;break}z=(c[k>>2]|0)+A|0;c[k>>2]=z;if((z|0)==(j|0)){C=49;break}if((y|0)==(f|0)){D=f;E=z;F=c[g>>2]|0}else{z=uc(c[u>>2]|0)|0;A=gn(v,0,d)|0;if((z|0)!=0){uc(z|0)|0}if((A|0)==-1){B=2;C=52;break}z=c[k>>2]|0;if(A>>>0>(s-z|0)>>>0){B=1;C=53;break}L25:do{if((A|0)!=0){G=A;H=v;I=z;while(1){J=a[H]|0;c[k>>2]=I+1;a[I]=J;J=G-1|0;if((J|0)==0){break L25}G=J;H=H+1|0;I=c[k>>2]|0}}}while(0);z=(c[g>>2]|0)+4|0;c[g>>2]=z;L30:do{if((z|0)==(f|0)){K=f}else{A=z;while(1){I=A+4|0;if((c[A>>2]|0)==0){K=A;break L30}if((I|0)==(f|0)){K=f;break}else{A=I}}}}while(0);D=K;E=c[k>>2]|0;F=z}if((F|0)==(f|0)|(E|0)==(j|0)){q=F;break L1}else{w=E;x=F;y=D}}if((C|0)==52){i=l;return B|0}else if((C|0)==53){i=l;return B|0}else if((C|0)==55){i=l;return B|0}else if((C|0)==16){c[k>>2]=w;L41:do{if((x|0)==(c[g>>2]|0)){L=x}else{y=x;v=w;while(1){s=c[y>>2]|0;r=uc(c[u>>2]|0)|0;A=gn(v,s,n)|0;if((r|0)!=0){uc(r|0)|0}if((A|0)==-1){L=y;break L41}r=(c[k>>2]|0)+A|0;c[k>>2]=r;A=y+4|0;if((A|0)==(c[g>>2]|0)){L=A;break}else{y=A;v=r}}}}while(0);c[g>>2]=L;B=2;i=l;return B|0}else if((C|0)==49){q=c[g>>2]|0;break}}}while(0);B=(q|0)!=(f|0)|0;i=l;return B|0}function Il(b,d,e,f,g,h,j,k){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;var l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0;l=i;i=i+8|0;m=l|0;n=m;o=(e|0)==(f|0);L1:do{if(o){c[k>>2]=h;c[g>>2]=e;p=e}else{q=e;while(1){r=q+1|0;if((a[q]|0)==0){s=q;break}if((r|0)==(f|0)){s=f;break}else{q=r}}c[k>>2]=h;c[g>>2]=e;if(o|(h|0)==(j|0)){p=e;break}q=d;r=j;t=b+8|0;u=h;v=e;w=s;while(1){x=c[q+4>>2]|0;c[m>>2]=c[q>>2];c[m+4>>2]=x;y=w;x=uc(c[t>>2]|0)|0;z=dn(u,g,y-v|0,r-u>>2,d)|0;if((x|0)!=0){uc(x|0)|0}if((z|0)==(-1|0)){A=16;break}else if((z|0)==0){B=2;A=51;break}x=(c[k>>2]|0)+(z<<2)|0;c[k>>2]=x;if((x|0)==(j|0)){A=48;break}z=c[g>>2]|0;if((w|0)==(f|0)){C=f;D=x;E=z}else{F=uc(c[t>>2]|0)|0;G=cn(x,z,1,d)|0;if((F|0)!=0){uc(F|0)|0}if((G|0)!=0){B=2;A=55;break}c[k>>2]=(c[k>>2]|0)+4;G=(c[g>>2]|0)+1|0;c[g>>2]=G;L22:do{if((G|0)==(f|0)){H=f}else{F=G;while(1){z=F+1|0;if((a[F]|0)==0){H=F;break L22}if((z|0)==(f|0)){H=f;break}else{F=z}}}}while(0);C=H;D=c[k>>2]|0;E=G}if((E|0)==(f|0)|(D|0)==(j|0)){p=E;break L1}else{u=D;v=E;w=C}}if((A|0)==16){c[k>>2]=u;L30:do{if((v|0)==(c[g>>2]|0)){I=v}else{w=u;r=v;while(1){q=uc(c[t>>2]|0)|0;F=cn(w,r,y-r|0,n)|0;if((q|0)!=0){uc(q|0)|0}if((F|0)==0){J=r+1|0}else if((F|0)==(-1|0)){A=27;break}else if((F|0)==(-2|0)){A=28;break}else{J=r+F|0}F=(c[k>>2]|0)+4|0;c[k>>2]=F;if((J|0)==(c[g>>2]|0)){I=J;break L30}else{w=F;r=J}}if((A|0)==27){c[g>>2]=r;B=2;i=l;return B|0}else if((A|0)==28){c[g>>2]=r;B=1;i=l;return B|0}}}while(0);c[g>>2]=I;B=(I|0)!=(f|0)|0;i=l;return B|0}else if((A|0)==48){p=c[g>>2]|0;break}else if((A|0)==55){i=l;return B|0}else if((A|0)==51){i=l;return B|0}}}while(0);B=(p|0)!=(f|0)|0;i=l;return B|0}function Jl(b,d,e,f,g){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;var h=0,j=0,k=0,l=0,m=0,n=0;h=i;i=i+8|0;c[g>>2]=e;e=h|0;j=uc(c[b+8>>2]|0)|0;b=gn(e,0,d)|0;if((j|0)!=0){uc(j|0)|0}if((b|0)==(-1|0)|(b|0)==0){k=2;i=h;return k|0}j=b-1|0;b=c[g>>2]|0;if(j>>>0>(f-b|0)>>>0){k=1;i=h;return k|0}if((j|0)==0){k=0;i=h;return k|0}else{l=j;m=e;n=b}while(1){b=a[m]|0;c[g>>2]=n+1;a[n]=b;b=l-1|0;if((b|0)==0){k=0;break}l=b;m=m+1|0;n=c[g>>2]|0}i=h;return k|0}function Kl(a){a=a|0;var b=0,d=0,e=0;b=a+8|0;a=uc(c[b>>2]|0)|0;d=fn(0,0,4)|0;if((a|0)!=0){uc(a|0)|0}if((d|0)!=0){e=-1;return e|0}d=c[b>>2]|0;if((d|0)==0){e=1;return e|0}b=uc(d|0)|0;if((b|0)==0){e=0;return e|0}uc(b|0)|0;e=0;return e|0}function Ll(a){a=a|0;return 0}function Ml(a,b,d,e,f){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,i=0,j=0,k=0,l=0,m=0,n=0,o=0;if((f|0)==0|(d|0)==(e|0)){g=0;return g|0}h=e;i=a+8|0;a=d;d=0;j=0;while(1){k=uc(c[i>>2]|0)|0;l=bn(a,h-a|0,b)|0;if((k|0)!=0){uc(k|0)|0}if((l|0)==0){m=1;n=a+1|0}else if((l|0)==(-1|0)|(l|0)==(-2|0)){g=d;o=18;break}else{m=l;n=a+l|0}l=m+d|0;k=j+1|0;if(k>>>0>=f>>>0|(n|0)==(e|0)){g=l;o=17;break}else{a=n;d=l;j=k}}if((o|0)==17){return g|0}else if((o|0)==18){return g|0}return 0}function Nl(a){a=a|0;var b=0,d=0;b=c[a+8>>2]|0;do{if((b|0)==0){d=1}else{a=uc(b|0)|0;if((a|0)==0){d=4;break}uc(a|0)|0;d=4}}while(0);return d|0}function Ol(a){a=a|0;Ve(a|0);Tn(a);return}function Pl(a,b,d,e,f,g,h,j){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;var k=0,l=0;b=i;i=i+16|0;a=b|0;k=b+8|0;c[a>>2]=d;c[k>>2]=g;l=Ql(d,e,a,g,h,k,1114111,0)|0;c[f>>2]=d+((c[a>>2]|0)-d>>1<<1);c[j>>2]=g+((c[k>>2]|0)-g);i=b;return l|0}function Ql(d,f,g,h,i,j,k,l){d=d|0;f=f|0;g=g|0;h=h|0;i=i|0;j=j|0;k=k|0;l=l|0;var m=0,n=0,o=0,p=0,q=0,r=0;c[g>>2]=d;c[j>>2]=h;do{if((l&2|0)!=0){if((i-h|0)<3){m=1;return m|0}else{c[j>>2]=h+1;a[h]=-17;d=c[j>>2]|0;c[j>>2]=d+1;a[d]=-69;d=c[j>>2]|0;c[j>>2]=d+1;a[d]=-65;break}}}while(0);h=f;l=c[g>>2]|0;if(l>>>0>=f>>>0){m=0;return m|0}d=i;i=l;L10:while(1){l=b[i>>1]|0;n=l&65535;if(n>>>0>k>>>0){m=2;o=39;break}do{if((l&65535)>>>0<128>>>0){p=c[j>>2]|0;if((d-p|0)<1){m=1;o=28;break L10}c[j>>2]=p+1;a[p]=l}else{if((l&65535)>>>0<2048>>>0){p=c[j>>2]|0;if((d-p|0)<2){m=1;o=35;break L10}c[j>>2]=p+1;a[p]=n>>>6|192;p=c[j>>2]|0;c[j>>2]=p+1;a[p]=n&63|128;break}if((l&65535)>>>0<55296>>>0){p=c[j>>2]|0;if((d-p|0)<3){m=1;o=34;break L10}c[j>>2]=p+1;a[p]=n>>>12|224;p=c[j>>2]|0;c[j>>2]=p+1;a[p]=n>>>6&63|128;p=c[j>>2]|0;c[j>>2]=p+1;a[p]=n&63|128;break}if((l&65535)>>>0>=56320>>>0){if((l&65535)>>>0<57344>>>0){m=2;o=37;break L10}p=c[j>>2]|0;if((d-p|0)<3){m=1;o=38;break L10}c[j>>2]=p+1;a[p]=n>>>12|224;p=c[j>>2]|0;c[j>>2]=p+1;a[p]=n>>>6&63|128;p=c[j>>2]|0;c[j>>2]=p+1;a[p]=n&63|128;break}if((h-i|0)<4){m=1;o=32;break L10}p=i+2|0;q=e[p>>1]|0;if((q&64512|0)!=56320){m=2;o=33;break L10}if((d-(c[j>>2]|0)|0)<4){m=1;o=31;break L10}r=n&960;if(((r<<10)+65536|n<<10&64512|q&1023)>>>0>k>>>0){m=2;o=36;break L10}c[g>>2]=p;p=(r>>>6)+1|0;r=c[j>>2]|0;c[j>>2]=r+1;a[r]=p>>>2|240;r=c[j>>2]|0;c[j>>2]=r+1;a[r]=n>>>2&15|p<<4&48|128;p=c[j>>2]|0;c[j>>2]=p+1;a[p]=n<<4&48|q>>>6&15|128;p=c[j>>2]|0;c[j>>2]=p+1;a[p]=q&63|128}}while(0);n=(c[g>>2]|0)+2|0;c[g>>2]=n;if(n>>>0<f>>>0){i=n}else{m=0;o=29;break}}if((o|0)==28){return m|0}else if((o|0)==29){return m|0}else if((o|0)==31){return m|0}else if((o|0)==32){return m|0}else if((o|0)==33){return m|0}else if((o|0)==34){return m|0}else if((o|0)==35){return m|0}else if((o|0)==36){return m|0}else if((o|0)==37){return m|0}else if((o|0)==38){return m|0}else if((o|0)==39){return m|0}return 0}function Rl(a,b,d,e,f,g,h,j){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;var k=0,l=0;b=i;i=i+16|0;a=b|0;k=b+8|0;c[a>>2]=d;c[k>>2]=g;l=Sl(d,e,a,g,h,k,1114111,0)|0;c[f>>2]=d+((c[a>>2]|0)-d);c[j>>2]=g+((c[k>>2]|0)-g>>1<<1);i=b;return l|0}function Sl(e,f,g,h,i,j,k,l){e=e|0;f=f|0;g=g|0;h=h|0;i=i|0;j=j|0;k=k|0;l=l|0;var m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0;c[g>>2]=e;c[j>>2]=h;h=c[g>>2]|0;do{if((l&4|0)==0){m=h}else{if((f-h|0)<=2){m=h;break}if((a[h]|0)!=-17){m=h;break}if((a[h+1|0]|0)!=-69){m=h;break}if((a[h+2|0]|0)!=-65){m=h;break}e=h+3|0;c[g>>2]=e;m=e}}while(0);L8:do{if(m>>>0<f>>>0){h=f;l=i;e=c[j>>2]|0;n=m;L10:while(1){if(e>>>0>=i>>>0){o=n;break L8}p=a[n]|0;q=p&255;if(q>>>0>k>>>0){r=2;s=44;break}do{if(p<<24>>24>-1){b[e>>1]=p&255;c[g>>2]=(c[g>>2]|0)+1}else{if((p&255)>>>0<194>>>0){r=2;s=45;break L10}if((p&255)>>>0<224>>>0){if((h-n|0)<2){r=1;s=46;break L10}t=d[n+1|0]|0;if((t&192|0)!=128){r=2;s=47;break L10}u=t&63|q<<6&1984;if(u>>>0>k>>>0){r=2;s=48;break L10}b[e>>1]=u;c[g>>2]=(c[g>>2]|0)+2;break}if((p&255)>>>0<240>>>0){if((h-n|0)<3){r=1;s=49;break L10}u=a[n+1|0]|0;t=a[n+2|0]|0;if((q|0)==224){if((u&-32)<<24>>24!=-96){r=2;s=50;break L10}}else if((q|0)==237){if((u&-32)<<24>>24!=-128){r=2;s=51;break L10}}else{if((u&-64)<<24>>24!=-128){r=2;s=52;break L10}}v=t&255;if((v&192|0)!=128){r=2;s=53;break L10}t=(u&255)<<6&4032|q<<12|v&63;if((t&65535)>>>0>k>>>0){r=2;s=54;break L10}b[e>>1]=t;c[g>>2]=(c[g>>2]|0)+3;break}if((p&255)>>>0>=245>>>0){r=2;s=55;break L10}if((h-n|0)<4){r=1;s=42;break L10}t=a[n+1|0]|0;v=a[n+2|0]|0;u=a[n+3|0]|0;if((q|0)==244){if((t&-16)<<24>>24!=-128){r=2;s=43;break L10}}else if((q|0)==240){if((t+112&255)>>>0>=48>>>0){r=2;s=60;break L10}}else{if((t&-64)<<24>>24!=-128){r=2;s=58;break L10}}w=v&255;if((w&192|0)!=128){r=2;s=59;break L10}v=u&255;if((v&192|0)!=128){r=2;s=56;break L10}if((l-e|0)<4){r=1;s=57;break L10}u=q&7;x=t&255;t=w<<6;y=v&63;if((x<<12&258048|u<<18|t&4032|y)>>>0>k>>>0){r=2;s=61;break L10}b[e>>1]=x<<2&60|w>>>4&3|((x>>>4&3|u<<2)<<6)+16320|55296;u=(c[j>>2]|0)+2|0;c[j>>2]=u;b[u>>1]=y|t&960|56320;c[g>>2]=(c[g>>2]|0)+4}}while(0);q=(c[j>>2]|0)+2|0;c[j>>2]=q;p=c[g>>2]|0;if(p>>>0<f>>>0){e=q;n=p}else{o=p;break L8}}if((s|0)==42){return r|0}else if((s|0)==43){return r|0}else if((s|0)==44){return r|0}else if((s|0)==45){return r|0}else if((s|0)==46){return r|0}else if((s|0)==47){return r|0}else if((s|0)==48){return r|0}else if((s|0)==49){return r|0}else if((s|0)==50){return r|0}else if((s|0)==51){return r|0}else if((s|0)==52){return r|0}else if((s|0)==53){return r|0}else if((s|0)==54){return r|0}else if((s|0)==55){return r|0}else if((s|0)==56){return r|0}else if((s|0)==57){return r|0}else if((s|0)==58){return r|0}else if((s|0)==59){return r|0}else if((s|0)==60){return r|0}else if((s|0)==61){return r|0}}else{o=m}}while(0);r=o>>>0<f>>>0|0;return r|0}function Tl(a,b,d,e,f){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;c[f>>2]=d;return 3}function Ul(a){a=a|0;return 0}function Vl(a){a=a|0;return 0}function Wl(a,b,c,d,e){a=a|0;b=b|0;c=c|0;d=d|0;e=e|0;return Xl(c,d,e,1114111,0)|0}function Xl(b,c,e,f,g){b=b|0;c=c|0;e=e|0;f=f|0;g=g|0;var h=0,i=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0;do{if((g&4|0)==0){h=b}else{if((c-b|0)<=2){h=b;break}if((a[b]|0)!=-17){h=b;break}if((a[b+1|0]|0)!=-69){h=b;break}h=(a[b+2|0]|0)==-65?b+3|0:b}}while(0);L7:do{if(h>>>0<c>>>0&(e|0)!=0){g=c;i=0;j=h;L9:while(1){k=a[j]|0;l=k&255;if(l>>>0>f>>>0){m=j;break L7}do{if(k<<24>>24>-1){n=j+1|0;o=i}else{if((k&255)>>>0<194>>>0){m=j;break L7}if((k&255)>>>0<224>>>0){if((g-j|0)<2){m=j;break L7}p=d[j+1|0]|0;if((p&192|0)!=128){m=j;break L7}if((p&63|l<<6&1984)>>>0>f>>>0){m=j;break L7}n=j+2|0;o=i;break}if((k&255)>>>0<240>>>0){q=j;if((g-q|0)<3){m=j;break L7}p=a[j+1|0]|0;r=a[j+2|0]|0;if((l|0)==224){if((p&-32)<<24>>24!=-96){s=21;break L9}}else if((l|0)==237){if((p&-32)<<24>>24!=-128){s=23;break L9}}else{if((p&-64)<<24>>24!=-128){s=25;break L9}}t=r&255;if((t&192|0)!=128){m=j;break L7}if(((p&255)<<6&4032|l<<12&61440|t&63)>>>0>f>>>0){m=j;break L7}n=j+3|0;o=i;break}if((k&255)>>>0>=245>>>0){m=j;break L7}u=j;if((g-u|0)<4){m=j;break L7}if((e-i|0)>>>0<2>>>0){m=j;break L7}t=a[j+1|0]|0;p=a[j+2|0]|0;r=a[j+3|0]|0;if((l|0)==240){if((t+112&255)>>>0>=48>>>0){s=34;break L9}}else if((l|0)==244){if((t&-16)<<24>>24!=-128){s=36;break L9}}else{if((t&-64)<<24>>24!=-128){s=38;break L9}}v=p&255;if((v&192|0)!=128){m=j;break L7}p=r&255;if((p&192|0)!=128){m=j;break L7}if(((t&255)<<12&258048|l<<18&1835008|v<<6&4032|p&63)>>>0>f>>>0){m=j;break L7}n=j+4|0;o=i+1|0}}while(0);l=o+1|0;if(n>>>0<c>>>0&l>>>0<e>>>0){i=l;j=n}else{m=n;break L7}}if((s|0)==21){w=q-b|0;return w|0}else if((s|0)==23){w=q-b|0;return w|0}else if((s|0)==25){w=q-b|0;return w|0}else if((s|0)==34){w=u-b|0;return w|0}else if((s|0)==36){w=u-b|0;return w|0}else if((s|0)==38){w=u-b|0;return w|0}}else{m=h}}while(0);w=m-b|0;return w|0}function Yl(a){a=a|0;return 4}function Zl(a){a=a|0;Ve(a|0);Tn(a);return}function _l(a,b,d,e,f,g,h,j){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;var k=0,l=0;b=i;i=i+16|0;a=b|0;k=b+8|0;c[a>>2]=d;c[k>>2]=g;l=$l(d,e,a,g,h,k,1114111,0)|0;c[f>>2]=d+((c[a>>2]|0)-d>>2<<2);c[j>>2]=g+((c[k>>2]|0)-g);i=b;return l|0}function $l(b,d,e,f,g,h,i,j){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;i=i|0;j=j|0;var k=0,l=0,m=0,n=0;c[e>>2]=b;c[h>>2]=f;do{if((j&2|0)!=0){if((g-f|0)<3){k=1;return k|0}else{c[h>>2]=f+1;a[f]=-17;b=c[h>>2]|0;c[h>>2]=b+1;a[b]=-69;b=c[h>>2]|0;c[h>>2]=b+1;a[b]=-65;break}}}while(0);f=c[e>>2]|0;if(f>>>0>=d>>>0){k=0;return k|0}j=g;g=f;L10:while(1){f=c[g>>2]|0;if((f&-2048|0)==55296|f>>>0>i>>>0){k=2;l=22;break}do{if(f>>>0<128>>>0){b=c[h>>2]|0;if((j-b|0)<1){k=1;l=21;break L10}c[h>>2]=b+1;a[b]=f}else{if(f>>>0<2048>>>0){b=c[h>>2]|0;if((j-b|0)<2){k=1;l=27;break L10}c[h>>2]=b+1;a[b]=f>>>6|192;b=c[h>>2]|0;c[h>>2]=b+1;a[b]=f&63|128;break}b=c[h>>2]|0;m=j-b|0;if(f>>>0<65536>>>0){if((m|0)<3){k=1;l=26;break L10}c[h>>2]=b+1;a[b]=f>>>12|224;n=c[h>>2]|0;c[h>>2]=n+1;a[n]=f>>>6&63|128;n=c[h>>2]|0;c[h>>2]=n+1;a[n]=f&63|128;break}else{if((m|0)<4){k=1;l=24;break L10}c[h>>2]=b+1;a[b]=f>>>18|240;b=c[h>>2]|0;c[h>>2]=b+1;a[b]=f>>>12&63|128;b=c[h>>2]|0;c[h>>2]=b+1;a[b]=f>>>6&63|128;b=c[h>>2]|0;c[h>>2]=b+1;a[b]=f&63|128;break}}}while(0);f=(c[e>>2]|0)+4|0;c[e>>2]=f;if(f>>>0<d>>>0){g=f}else{k=0;l=20;break}}if((l|0)==27){return k|0}else if((l|0)==24){return k|0}else if((l|0)==26){return k|0}else if((l|0)==22){return k|0}else if((l|0)==20){return k|0}else if((l|0)==21){return k|0}return 0}function am(a,b,d,e,f,g,h,j){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;var k=0,l=0;b=i;i=i+16|0;a=b|0;k=b+8|0;c[a>>2]=d;c[k>>2]=g;l=bm(d,e,a,g,h,k,1114111,0)|0;c[f>>2]=d+((c[a>>2]|0)-d);c[j>>2]=g+((c[k>>2]|0)-g>>2<<2);i=b;return l|0}function bm(b,e,f,g,h,i,j,k){b=b|0;e=e|0;f=f|0;g=g|0;h=h|0;i=i|0;j=j|0;k=k|0;var l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0;c[f>>2]=b;c[i>>2]=g;g=c[f>>2]|0;do{if((k&4|0)==0){l=g}else{if((e-g|0)<=2){l=g;break}if((a[g]|0)!=-17){l=g;break}if((a[g+1|0]|0)!=-69){l=g;break}if((a[g+2|0]|0)!=-65){l=g;break}b=g+3|0;c[f>>2]=b;l=b}}while(0);L8:do{if(l>>>0<e>>>0){g=e;k=c[i>>2]|0;b=l;L10:while(1){if(k>>>0>=h>>>0){m=b;break L8}n=a[b]|0;o=n&255;do{if(n<<24>>24>-1){if(o>>>0>j>>>0){p=2;q=42;break L10}c[k>>2]=o;c[f>>2]=(c[f>>2]|0)+1}else{if((n&255)>>>0<194>>>0){p=2;q=43;break L10}if((n&255)>>>0<224>>>0){if((g-b|0)<2){p=1;q=41;break L10}r=d[b+1|0]|0;if((r&192|0)!=128){p=2;q=44;break L10}s=r&63|o<<6&1984;if(s>>>0>j>>>0){p=2;q=45;break L10}c[k>>2]=s;c[f>>2]=(c[f>>2]|0)+2;break}if((n&255)>>>0<240>>>0){if((g-b|0)<3){p=1;q=46;break L10}s=a[b+1|0]|0;r=a[b+2|0]|0;if((o|0)==224){if((s&-32)<<24>>24!=-96){p=2;q=47;break L10}}else if((o|0)==237){if((s&-32)<<24>>24!=-128){p=2;q=48;break L10}}else{if((s&-64)<<24>>24!=-128){p=2;q=49;break L10}}t=r&255;if((t&192|0)!=128){p=2;q=50;break L10}r=(s&255)<<6&4032|o<<12&61440|t&63;if(r>>>0>j>>>0){p=2;q=51;break L10}c[k>>2]=r;c[f>>2]=(c[f>>2]|0)+3;break}if((n&255)>>>0>=245>>>0){p=2;q=52;break L10}if((g-b|0)<4){p=1;q=53;break L10}r=a[b+1|0]|0;t=a[b+2|0]|0;s=a[b+3|0]|0;if((o|0)==240){if((r+112&255)>>>0>=48>>>0){p=2;q=54;break L10}}else if((o|0)==244){if((r&-16)<<24>>24!=-128){p=2;q=55;break L10}}else{if((r&-64)<<24>>24!=-128){p=2;q=56;break L10}}u=t&255;if((u&192|0)!=128){p=2;q=57;break L10}t=s&255;if((t&192|0)!=128){p=2;q=58;break L10}s=(r&255)<<12&258048|o<<18&1835008|u<<6&4032|t&63;if(s>>>0>j>>>0){p=2;q=59;break L10}c[k>>2]=s;c[f>>2]=(c[f>>2]|0)+4}}while(0);o=(c[i>>2]|0)+4|0;c[i>>2]=o;n=c[f>>2]|0;if(n>>>0<e>>>0){k=o;b=n}else{m=n;break L8}}if((q|0)==46){return p|0}else if((q|0)==47){return p|0}else if((q|0)==48){return p|0}else if((q|0)==49){return p|0}else if((q|0)==50){return p|0}else if((q|0)==51){return p|0}else if((q|0)==52){return p|0}else if((q|0)==53){return p|0}else if((q|0)==54){return p|0}else if((q|0)==55){return p|0}else if((q|0)==56){return p|0}else if((q|0)==57){return p|0}else if((q|0)==41){return p|0}else if((q|0)==58){return p|0}else if((q|0)==59){return p|0}else if((q|0)==42){return p|0}else if((q|0)==43){return p|0}else if((q|0)==44){return p|0}else if((q|0)==45){return p|0}}else{m=l}}while(0);p=m>>>0<e>>>0|0;return p|0}function cm(a,b,d,e,f){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;c[f>>2]=d;return 3}function dm(a){a=a|0;return 0}function em(a){a=a|0;return 0}function fm(a,b,c,d,e){a=a|0;b=b|0;c=c|0;d=d|0;e=e|0;return gm(c,d,e,1114111,0)|0}function gm(b,c,e,f,g){b=b|0;c=c|0;e=e|0;f=f|0;g=g|0;var h=0,i=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0;do{if((g&4|0)==0){h=b}else{if((c-b|0)<=2){h=b;break}if((a[b]|0)!=-17){h=b;break}if((a[b+1|0]|0)!=-69){h=b;break}h=(a[b+2|0]|0)==-65?b+3|0:b}}while(0);L7:do{if(h>>>0<c>>>0&(e|0)!=0){g=c;i=1;j=h;L9:while(1){k=a[j]|0;l=k&255;do{if(k<<24>>24>-1){if(l>>>0>f>>>0){m=j;break L7}n=j+1|0}else{if((k&255)>>>0<194>>>0){m=j;break L7}if((k&255)>>>0<224>>>0){if((g-j|0)<2){m=j;break L7}o=d[j+1|0]|0;if((o&192|0)!=128){m=j;break L7}if((o&63|l<<6&1984)>>>0>f>>>0){m=j;break L7}n=j+2|0;break}if((k&255)>>>0<240>>>0){p=j;if((g-p|0)<3){m=j;break L7}o=a[j+1|0]|0;q=a[j+2|0]|0;if((l|0)==224){if((o&-32)<<24>>24!=-96){r=21;break L9}}else if((l|0)==237){if((o&-32)<<24>>24!=-128){r=23;break L9}}else{if((o&-64)<<24>>24!=-128){r=25;break L9}}s=q&255;if((s&192|0)!=128){m=j;break L7}if(((o&255)<<6&4032|l<<12&61440|s&63)>>>0>f>>>0){m=j;break L7}n=j+3|0;break}if((k&255)>>>0>=245>>>0){m=j;break L7}t=j;if((g-t|0)<4){m=j;break L7}s=a[j+1|0]|0;o=a[j+2|0]|0;q=a[j+3|0]|0;if((l|0)==240){if((s+112&255)>>>0>=48>>>0){r=33;break L9}}else if((l|0)==244){if((s&-16)<<24>>24!=-128){r=35;break L9}}else{if((s&-64)<<24>>24!=-128){r=37;break L9}}u=o&255;if((u&192|0)!=128){m=j;break L7}o=q&255;if((o&192|0)!=128){m=j;break L7}if(((s&255)<<12&258048|l<<18&1835008|u<<6&4032|o&63)>>>0>f>>>0){m=j;break L7}n=j+4|0}}while(0);if(!(n>>>0<c>>>0&i>>>0<e>>>0)){m=n;break L7}i=i+1|0;j=n}if((r|0)==21){v=p-b|0;return v|0}else if((r|0)==23){v=p-b|0;return v|0}else if((r|0)==25){v=p-b|0;return v|0}else if((r|0)==33){v=t-b|0;return v|0}else if((r|0)==35){v=t-b|0;return v|0}else if((r|0)==37){v=t-b|0;return v|0}}else{m=h}}while(0);v=m-b|0;return v|0}function hm(a){a=a|0;return 4}function im(a){a=a|0;Ve(a|0);Tn(a);return}function jm(a){a=a|0;Ve(a|0);Tn(a);return}function km(a){a=a|0;c[a>>2]=5096;vf(a+12|0);Ve(a|0);Tn(a);return}function lm(a){a=a|0;c[a>>2]=5096;vf(a+12|0);Ve(a|0);return}function mm(a){a=a|0;c[a>>2]=5048;vf(a+16|0);Ve(a|0);Tn(a);return}function nm(a){a=a|0;c[a>>2]=5048;vf(a+16|0);Ve(a|0);return}function om(b){b=b|0;return a[b+8|0]|0}function pm(a){a=a|0;return c[a+8>>2]|0}function qm(b){b=b|0;return a[b+9|0]|0}function rm(a){a=a|0;return c[a+12>>2]|0}function sm(a,b){a=a|0;b=b|0;sf(a,b+12|0);return}function tm(a,b){a=a|0;b=b|0;sf(a,b+16|0);return}function um(a,b){a=a|0;b=b|0;tf(a,3112,4);return}function vm(a,b){a=a|0;b=b|0;Ef(a,3080,ln(3080)|0);return}function wm(a,b){a=a|0;b=b|0;tf(a,3064,5);return}function xm(a,b){a=a|0;b=b|0;Ef(a,3024,ln(3024)|0);return}function ym(b){b=b|0;var d=0;if((a[36544]|0)!=0){d=c[8468]|0;return d|0}if((yb(36544)|0)==0){d=c[8468]|0;return d|0}do{if((a[36432]|0)==0){if((yb(36432)|0)==0){break}ko(32800,0,168)|0;gb(260,0,u|0)|0}}while(0);wf(32800,3456)|0;wf(32812,3448)|0;wf(32824,3440)|0;wf(32836,3424)|0;wf(32848,3408)|0;wf(32860,3400)|0;wf(32872,3384)|0;wf(32884,3376)|0;wf(32896,3368)|0;wf(32908,3352)|0;wf(32920,3344)|0;wf(32932,3336)|0;wf(32944,3304)|0;wf(32956,3296)|0;c[8468]=32800;d=c[8468]|0;return d|0}function zm(b){b=b|0;var d=0;if((a[36488]|0)!=0){d=c[8446]|0;return d|0}if((yb(36488)|0)==0){d=c[8446]|0;return d|0}do{if((a[36408]|0)==0){if((yb(36408)|0)==0){break}ko(32056,0,168)|0;gb(150,0,u|0)|0}}while(0);Hf(32056,4e3)|0;Hf(32068,3960)|0;Hf(32080,3920)|0;Hf(32092,3872)|0;Hf(32104,3816)|0;Hf(32116,3768)|0;Hf(32128,3728)|0;Hf(32140,3696)|0;Hf(32152,3616)|0;Hf(32164,3592)|0;Hf(32176,3552)|0;Hf(32188,3504)|0;Hf(32200,3488)|0;Hf(32212,3472)|0;c[8446]=32056;d=c[8446]|0;return d|0}function Am(b){b=b|0;var d=0;if((a[36536]|0)!=0){d=c[8466]|0;return d|0}if((yb(36536)|0)==0){d=c[8466]|0;return d|0}do{if((a[36424]|0)==0){if((yb(36424)|0)==0){break}ko(32512,0,288)|0;gb(168,0,u|0)|0}}while(0);wf(32512,432)|0;wf(32524,408)|0;wf(32536,384)|0;wf(32548,368)|0;wf(32560,352)|0;wf(32572,328)|0;wf(32584,312)|0;wf(32596,264)|0;wf(32608,232)|0;wf(32620,224)|0;wf(32632,208)|0;wf(32644,184)|0;wf(32656,168)|0;wf(32668,152)|0;wf(32680,136)|0;wf(32692,120)|0;wf(32704,352)|0;wf(32716,104)|0;wf(32728,48)|0;wf(32740,4104)|0;wf(32752,4088)|0;wf(32764,4080)|0;wf(32776,4064)|0;wf(32788,4048)|0;c[8466]=32512;d=c[8466]|0;return d|0}function Bm(b){b=b|0;var d=0;if((a[36480]|0)!=0){d=c[8444]|0;return d|0}if((yb(36480)|0)==0){d=c[8444]|0;return d|0}do{if((a[36400]|0)==0){if((yb(36400)|0)==0){break}ko(31768,0,288)|0;gb(124,0,u|0)|0}}while(0);Hf(31768,1552)|0;Hf(31780,1488)|0;Hf(31792,1448)|0;Hf(31804,1384)|0;Hf(31816,648)|0;Hf(31828,1296)|0;Hf(31840,1256)|0;Hf(31852,1160)|0;Hf(31864,1072)|0;Hf(31876,1e3)|0;Hf(31888,920)|0;Hf(31900,864)|0;Hf(31912,808)|0;Hf(31924,744)|0;Hf(31936,688)|0;Hf(31948,672)|0;Hf(31960,648)|0;Hf(31972,624)|0;Hf(31984,608)|0;Hf(31996,584)|0;Hf(32008,560)|0;Hf(32020,536)|0;Hf(32032,512)|0;Hf(32044,472)|0;c[8444]=31768;d=c[8444]|0;return d|0}function Cm(b){b=b|0;var d=0;if((a[36552]|0)!=0){d=c[8470]|0;return d|0}if((yb(36552)|0)==0){d=c[8470]|0;return d|0}do{if((a[36440]|0)==0){if((yb(36440)|0)==0){break}ko(32968,0,288)|0;gb(122,0,u|0)|0}}while(0);wf(32968,1656)|0;wf(32980,1640)|0;c[8470]=32968;d=c[8470]|0;return d|0}function Dm(b){b=b|0;var d=0;if((a[36496]|0)!=0){d=c[8448]|0;return d|0}if((yb(36496)|0)==0){d=c[8448]|0;return d|0}do{if((a[36416]|0)==0){if((yb(36416)|0)==0){break}ko(32224,0,288)|0;gb(234,0,u|0)|0}}while(0);Hf(32224,1712)|0;Hf(32236,1688)|0;c[8448]=32224;d=c[8448]|0;return d|0}function Em(b){b=b|0;if((a[36560]|0)!=0){return 33888}if((yb(36560)|0)==0){return 33888}tf(33888,2968,8);gb(252,33888,u|0)|0;return 33888}function Fm(b){b=b|0;if((a[36504]|0)!=0){return 33800}if((yb(36504)|0)==0){return 33800}Ef(33800,2896,ln(2896)|0);gb(192,33800,u|0)|0;return 33800}function Gm(b){b=b|0;if((a[36584]|0)!=0){return 33936}if((yb(36584)|0)==0){return 33936}tf(33936,2864,8);gb(252,33936,u|0)|0;return 33936}function Hm(b){b=b|0;if((a[36528]|0)!=0){return 33848}if((yb(36528)|0)==0){return 33848}Ef(33848,2816,ln(2816)|0);gb(192,33848,u|0)|0;return 33848}function Im(b){b=b|0;if((a[36576]|0)!=0){return 33920}if((yb(36576)|0)==0){return 33920}tf(33920,2768,20);gb(252,33920,u|0)|0;return 33920}function Jm(b){b=b|0;if((a[36520]|0)!=0){return 33832}if((yb(36520)|0)==0){return 33832}Ef(33832,2672,ln(2672)|0);gb(192,33832,u|0)|0;return 33832}function Km(b){b=b|0;if((a[36568]|0)!=0){return 33904}if((yb(36568)|0)==0){return 33904}tf(33904,2648,11);gb(252,33904,u|0)|0;return 33904}function Lm(b){b=b|0;if((a[36512]|0)!=0){return 33816}if((yb(36512)|0)==0){return 33816}Ef(33816,2592,ln(2592)|0);gb(192,33816,u|0)|0;return 33816}function Mm(b,d,e){b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0.0,j=0,k=0,l=0.0;f=i;i=i+8|0;g=f|0;if((b|0)==(d|0)){c[e>>2]=4;h=0.0;i=f;return+h}j=kc()|0;k=c[j>>2]|0;c[j>>2]=0;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);l=+go(b,g,c[8322]|0);b=c[j>>2]|0;if((b|0)==0){c[j>>2]=k}if((c[g>>2]|0)!=(d|0)){c[e>>2]=4;h=0.0;i=f;return+h}if((b|0)!=34){h=l;i=f;return+h}c[e>>2]=4;h=l;i=f;return+h}function Nm(b,d,e){b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0.0,j=0,k=0,l=0.0;f=i;i=i+8|0;g=f|0;if((b|0)==(d|0)){c[e>>2]=4;h=0.0;i=f;return+h}j=kc()|0;k=c[j>>2]|0;c[j>>2]=0;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);l=+go(b,g,c[8322]|0);b=c[j>>2]|0;if((b|0)==0){c[j>>2]=k}if((c[g>>2]|0)!=(d|0)){c[e>>2]=4;h=0.0;i=f;return+h}if((b|0)!=34){h=l;i=f;return+h}c[e>>2]=4;h=l;i=f;return+h}function Om(b,d,e){b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0.0,j=0,k=0,l=0.0;f=i;i=i+8|0;g=f|0;if((b|0)==(d|0)){c[e>>2]=4;h=0.0;i=f;return+h}j=kc()|0;k=c[j>>2]|0;c[j>>2]=0;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);l=+go(b,g,c[8322]|0);b=c[j>>2]|0;if((b|0)==0){c[j>>2]=k}if((c[g>>2]|0)!=(d|0)){c[e>>2]=4;h=0.0;i=f;return+h}if((b|0)==34){c[e>>2]=4}h=l;i=f;return+h}function Pm(b,d,e,f){b=b|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0;g=i;i=i+8|0;h=g|0;do{if((b|0)==(d|0)){c[e>>2]=4;j=0;k=0}else{if((a[b]|0)==45){c[e>>2]=4;j=0;k=0;break}l=kc()|0;m=c[l>>2]|0;c[l>>2]=0;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);n=Ja(b|0,h|0,f|0,c[8322]|0)|0;o=c[l>>2]|0;if((o|0)==0){c[l>>2]=m}if((c[h>>2]|0)!=(d|0)){c[e>>2]=4;j=0;k=0;break}if((o|0)!=34){j=K;k=n;break}c[e>>2]=4;j=-1;k=-1}}while(0);i=g;return(K=j,k)|0}function Qm(b,d,e,f){b=b|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,j=0,k=0,l=0,m=0;g=i;i=i+8|0;h=g|0;if((b|0)==(d|0)){c[e>>2]=4;j=0;i=g;return j|0}if((a[b]|0)==45){c[e>>2]=4;j=0;i=g;return j|0}k=kc()|0;l=c[k>>2]|0;c[k>>2]=0;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);m=Ja(b|0,h|0,f|0,c[8322]|0)|0;f=K;b=c[k>>2]|0;if((b|0)==0){c[k>>2]=l}if((c[h>>2]|0)!=(d|0)){c[e>>2]=4;j=0;i=g;return j|0}d=0;if((b|0)==34|(f>>>0>d>>>0|f>>>0==d>>>0&m>>>0>-1>>>0)){c[e>>2]=4;j=-1;i=g;return j|0}else{j=m;i=g;return j|0}return 0}function Rm(b,d,e,f){b=b|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,j=0,k=0,l=0,m=0;g=i;i=i+8|0;h=g|0;if((b|0)==(d|0)){c[e>>2]=4;j=0;i=g;return j|0}if((a[b]|0)==45){c[e>>2]=4;j=0;i=g;return j|0}k=kc()|0;l=c[k>>2]|0;c[k>>2]=0;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);m=Ja(b|0,h|0,f|0,c[8322]|0)|0;f=K;b=c[k>>2]|0;if((b|0)==0){c[k>>2]=l}if((c[h>>2]|0)!=(d|0)){c[e>>2]=4;j=0;i=g;return j|0}d=0;if((b|0)==34|(f>>>0>d>>>0|f>>>0==d>>>0&m>>>0>-1>>>0)){c[e>>2]=4;j=-1;i=g;return j|0}else{j=m;i=g;return j|0}return 0}function Sm(b,d,e,f){b=b|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,j=0,k=0,l=0,m=0;g=i;i=i+8|0;h=g|0;if((b|0)==(d|0)){c[e>>2]=4;j=0;i=g;return j|0}if((a[b]|0)==45){c[e>>2]=4;j=0;i=g;return j|0}k=kc()|0;l=c[k>>2]|0;c[k>>2]=0;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);m=Ja(b|0,h|0,f|0,c[8322]|0)|0;f=K;b=c[k>>2]|0;if((b|0)==0){c[k>>2]=l}if((c[h>>2]|0)!=(d|0)){c[e>>2]=4;j=0;i=g;return j|0}d=0;if((b|0)==34|(f>>>0>d>>>0|f>>>0==d>>>0&m>>>0>65535>>>0)){c[e>>2]=4;j=-1;i=g;return j|0}else{j=m&65535;i=g;return j|0}return 0}function Tm(b,d,e,f){b=b|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,j=0,k=0,l=0,m=0,n=0;g=i;i=i+8|0;h=g|0;if((b|0)==(d|0)){c[e>>2]=4;j=0;k=0;i=g;return(K=j,k)|0}l=kc()|0;m=c[l>>2]|0;c[l>>2]=0;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);n=Ec(b|0,h|0,f|0,c[8322]|0)|0;f=K;b=c[l>>2]|0;if((b|0)==0){c[l>>2]=m}if((c[h>>2]|0)!=(d|0)){c[e>>2]=4;j=0;k=0;i=g;return(K=j,k)|0}if((b|0)!=34){j=f;k=n;i=g;return(K=j,k)|0}c[e>>2]=4;e=0;b=(f|0)>(e|0)|(f|0)==(e|0)&n>>>0>0>>>0;j=b?2147483647:-2147483648;k=b?-1:0;i=g;return(K=j,k)|0}function Um(b,d,e,f){b=b|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,j=0,k=0,l=0,m=0;g=i;i=i+8|0;h=g|0;if((b|0)==(d|0)){c[e>>2]=4;j=0;i=g;return j|0}k=kc()|0;l=c[k>>2]|0;c[k>>2]=0;do{if((a[36464]|0)==0){if((yb(36464)|0)==0){break}c[8322]=Va(2147483647,3224,0)|0}}while(0);m=Ec(b|0,h|0,f|0,c[8322]|0)|0;f=K;b=c[k>>2]|0;if((b|0)==0){c[k>>2]=l}if((c[h>>2]|0)!=(d|0)){c[e>>2]=4;j=0;i=g;return j|0}d=-1;h=0;if((b|0)==34|((f|0)<(d|0)|(f|0)==(d|0)&m>>>0<-2147483648>>>0)|((f|0)>(h|0)|(f|0)==(h|0)&m>>>0>2147483647>>>0)){c[e>>2]=4;e=0;j=(f|0)>(e|0)|(f|0)==(e|0)&m>>>0>0>>>0?2147483647:-2147483648;i=g;return j|0}else{j=m;i=g;return j|0}return 0}function Vm(a){a=a|0;var b=0,d=0,e=0,f=0;b=a+4|0;d=(c[a>>2]|0)+(c[b+4>>2]|0)|0;a=d;e=c[b>>2]|0;if((e&1|0)==0){f=e;_c[f&511](a);return}else{f=c[(c[d>>2]|0)+(e-1)>>2]|0;_c[f&511](a);return}}function Wm(b,d){b=b|0;d=d|0;var e=0,f=0,g=0,h=0,i=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0;e=b+8|0;f=b+4|0;g=c[f>>2]|0;h=c[e>>2]|0;i=g;if(h-i>>2>>>0>=d>>>0){j=d;k=g;do{if((k|0)==0){l=0}else{c[k>>2]=0;l=c[f>>2]|0}k=l+4|0;c[f>>2]=k;j=j-1|0;}while((j|0)!=0);return}j=b+16|0;k=b|0;l=c[k>>2]|0;g=i-l>>2;i=g+d|0;if(i>>>0>1073741823>>>0){Qk(0)}m=h-l|0;do{if(m>>2>>>0>536870910>>>0){n=1073741823;o=11}else{l=m>>1;h=l>>>0<i>>>0?i:l;if((h|0)==0){p=0;q=0;break}l=b+128|0;if(!((a[l]&1)==0&h>>>0<29>>>0)){n=h;o=11;break}a[l]=1;p=j;q=h}}while(0);if((o|0)==11){p=Rn(n<<2)|0;q=n}n=d;d=p+(g<<2)|0;do{if((d|0)==0){r=0}else{c[d>>2]=0;r=d}d=r+4|0;n=n-1|0;}while((n|0)!=0);n=c[k>>2]|0;r=(c[f>>2]|0)-n|0;o=p+(g-(r>>2)<<2)|0;g=n;io(o|0,g|0,r)|0;c[k>>2]=o;c[f>>2]=d;c[e>>2]=p+(q<<2);if((n|0)==0){return}if((n|0)==(j|0)){a[b+128|0]=0;return}else{Tn(g);return}}function Xm(a){a=a|0;Gf(32500);Gf(32488);Gf(32476);Gf(32464);Gf(32452);Gf(32440);Gf(32428);Gf(32416);Gf(32404);Gf(32392);Gf(32380);Gf(32368);Gf(32356);Gf(32344);Gf(32332);Gf(32320);Gf(32308);Gf(32296);Gf(32284);Gf(32272);Gf(32260);Gf(32248);Gf(32236);Gf(32224);return}function Ym(a){a=a|0;vf(33244);vf(33232);vf(33220);vf(33208);vf(33196);vf(33184);vf(33172);vf(33160);vf(33148);vf(33136);vf(33124);vf(33112);vf(33100);vf(33088);vf(33076);vf(33064);vf(33052);vf(33040);vf(33028);vf(33016);vf(33004);vf(32992);vf(32980);vf(32968);return}function Zm(a){a=a|0;Gf(32044);Gf(32032);Gf(32020);Gf(32008);Gf(31996);Gf(31984);Gf(31972);Gf(31960);Gf(31948);Gf(31936);Gf(31924);Gf(31912);Gf(31900);Gf(31888);Gf(31876);Gf(31864);Gf(31852);Gf(31840);Gf(31828);Gf(31816);Gf(31804);Gf(31792);Gf(31780);Gf(31768);return}function _m(a){a=a|0;vf(32788);vf(32776);vf(32764);vf(32752);vf(32740);vf(32728);vf(32716);vf(32704);vf(32692);vf(32680);vf(32668);vf(32656);vf(32644);vf(32632);vf(32620);vf(32608);vf(32596);vf(32584);vf(32572);vf(32560);vf(32548);vf(32536);vf(32524);vf(32512);return}function $m(a){a=a|0;Gf(32212);Gf(32200);Gf(32188);Gf(32176);Gf(32164);Gf(32152);Gf(32140);Gf(32128);Gf(32116);Gf(32104);Gf(32092);Gf(32080);Gf(32068);Gf(32056);return}function an(a){a=a|0;vf(32956);vf(32944);vf(32932);vf(32920);vf(32908);vf(32896);vf(32884);vf(32872);vf(32860);vf(32848);vf(32836);vf(32824);vf(32812);vf(32800);return}function bn(a,b,c){a=a|0;b=b|0;c=c|0;return cn(0,a,b,(c|0)!=0?c:31184)|0}function cn(b,d,e,f){b=b|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,t=0,u=0,v=0,w=0;g=i;i=i+8|0;h=g|0;c[h>>2]=b;j=((f|0)==0?31176:f)|0;f=c[j>>2]|0;L1:do{if((d|0)==0){if((f|0)==0){k=0}else{break}i=g;return k|0}else{if((b|0)==0){l=h;c[h>>2]=l;m=l}else{m=b}if((e|0)==0){k=-2;i=g;return k|0}do{if((f|0)==0){l=a[d]|0;n=l&255;if(l<<24>>24>-1){c[m>>2]=n;k=l<<24>>24!=0|0;i=g;return k|0}else{l=n-194|0;if(l>>>0>50>>>0){break L1}o=d+1|0;p=c[s+(l<<2)>>2]|0;q=e-1|0;break}}else{o=d;p=f;q=e}}while(0);L17:do{if((q|0)==0){r=p}else{l=a[o]|0;n=(l&255)>>>3;if((n-16|n+(p>>26))>>>0>7>>>0){break L1}else{t=o;u=p;v=q;w=l}while(1){t=t+1|0;u=(w&255)-128|u<<6;v=v-1|0;if((u|0)>=0){break}if((v|0)==0){r=u;break L17}w=a[t]|0;if(((w&255)-128|0)>>>0>63>>>0){break L1}}c[j>>2]=0;c[m>>2]=u;k=e-v|0;i=g;return k|0}}while(0);c[j>>2]=r;k=-2;i=g;return k|0}}while(0);c[j>>2]=0;c[(kc()|0)>>2]=84;k=-1;i=g;return k|0}function dn(a,b,d,e,f){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0;g=i;i=i+1032|0;h=g+1024|0;j=c[b>>2]|0;c[h>>2]=j;k=(a|0)!=0;l=g|0;m=k?e:256;e=k?a:l;L1:do{if((j|0)==0|(m|0)==0){n=0;o=d;p=m;q=e;r=j}else{a=m;s=d;t=0;u=e;v=j;while(1){w=s>>>2;x=w>>>0>=a>>>0;if(!(x|s>>>0>131>>>0)){n=t;o=s;p=a;q=u;r=v;break L1}y=x?a:w;z=s-y|0;w=en(u,h,y,f)|0;if((w|0)==-1){break}if((u|0)==(l|0)){A=l;B=a}else{A=u+(w<<2)|0;B=a-w|0}y=w+t|0;w=c[h>>2]|0;if((w|0)==0|(B|0)==0){n=y;o=z;p=B;q=A;r=w;break L1}else{a=B;s=z;t=y;u=A;v=w}}n=-1;o=z;p=0;q=u;r=c[h>>2]|0}}while(0);L11:do{if((r|0)==0){C=n}else{if((p|0)==0|(o|0)==0){C=n;break}else{D=p;E=o;F=n;G=q;H=r}while(1){I=cn(G,H,E,f)|0;if((I+2|0)>>>0<3>>>0){break}z=(c[h>>2]|0)+I|0;c[h>>2]=z;A=D-1|0;B=F+1|0;if((A|0)==0|(E|0)==(I|0)){C=B;break L11}else{D=A;E=E-I|0;F=B;G=G+4|0;H=z}}if((I|0)==0){c[h>>2]=0;C=F;break}else if((I|0)==(-1|0)){C=-1;break}else{c[f>>2]=0;C=F;break}}}while(0);if(!k){i=g;return C|0}c[b>>2]=c[h>>2];i=g;return C|0}function en(b,e,f,g){b=b|0;e=e|0;f=f|0;g=g|0;var h=0,i=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0,Z=0,_=0,$=0,aa=0,ba=0,ca=0,da=0,ea=0,fa=0;h=c[e>>2]|0;do{if((g|0)==0){i=5}else{j=g|0;k=c[j>>2]|0;if((k|0)==0){i=5;break}if((b|0)==0){l=k;m=h;n=f;i=16;break}c[j>>2]=0;o=k;p=h;q=b;r=f;i=36}}while(0);if((i|0)==5){if((b|0)==0){t=h;u=f;i=7}else{v=h;w=b;x=f;i=6}}L7:while(1){if((i|0)==6){i=0;if((x|0)==0){y=f;i=54;break}else{z=x;A=w;B=v}while(1){h=a[B]|0;do{if(((h&255)-1|0)>>>0<127>>>0){if((B&3|0)==0&z>>>0>3>>>0){C=z;D=A;E=B}else{F=B;G=A;H=z;I=h;break}while(1){J=c[E>>2]|0;if(((J-16843009|J)&-2139062144|0)!=0){i=30;break}c[D>>2]=J&255;c[D+4>>2]=d[E+1|0]|0;c[D+8>>2]=d[E+2|0]|0;K=E+4|0;L=D+16|0;c[D+12>>2]=d[E+3|0]|0;M=C-4|0;if(M>>>0>3>>>0){C=M;D=L;E=K}else{i=31;break}}if((i|0)==30){i=0;F=E;G=D;H=C;I=J&255;break}else if((i|0)==31){i=0;F=K;G=L;H=M;I=a[K]|0;break}}else{F=B;G=A;H=z;I=h}}while(0);N=I&255;if((N-1|0)>>>0>=127>>>0){break}c[G>>2]=N;h=H-1|0;if((h|0)==0){y=f;i=56;break L7}else{z=h;A=G+4|0;B=F+1|0}}h=N-194|0;if(h>>>0>50>>>0){O=H;P=G;Q=F;i=47;break}o=c[s+(h<<2)>>2]|0;p=F+1|0;q=G;r=H;i=36;continue}else if((i|0)==16){i=0;h=(d[m]|0)>>>3;if((h-16|h+(l>>26))>>>0>7>>>0){i=17;break}h=m+1|0;do{if((l&33554432|0)==0){R=h}else{if(((d[h]|0)-128|0)>>>0>63>>>0){i=20;break L7}g=m+2|0;if((l&524288|0)==0){R=g;break}if(((d[g]|0)-128|0)>>>0>63>>>0){i=23;break L7}R=m+3|0}}while(0);t=R;u=n-1|0;i=7;continue}else if((i|0)==7){i=0;h=a[t]|0;do{if(((h&255)-1|0)>>>0<127>>>0){if((t&3|0)!=0){S=t;T=u;U=h;break}g=c[t>>2]|0;if(((g-16843009|g)&-2139062144|0)==0){V=u;W=t}else{S=t;T=u;U=g&255;break}do{W=W+4|0;V=V-4|0;X=c[W>>2]|0;}while(((X-16843009|X)&-2139062144|0)==0);S=W;T=V;U=X&255}else{S=t;T=u;U=h}}while(0);h=U&255;if((h-1|0)>>>0<127>>>0){t=S+1|0;u=T-1|0;i=7;continue}g=h-194|0;if(g>>>0>50>>>0){O=T;P=b;Q=S;i=47;break}l=c[s+(g<<2)>>2]|0;m=S+1|0;n=T;i=16;continue}else if((i|0)==36){i=0;g=d[p]|0;h=g>>>3;if((h-16|h+(o>>26))>>>0>7>>>0){i=37;break}h=p+1|0;Y=g-128|o<<6;do{if((Y|0)<0){g=(d[h]|0)-128|0;if(g>>>0>63>>>0){i=40;break L7}k=p+2|0;Z=g|Y<<6;if((Z|0)>=0){_=Z;$=k;break}g=(d[k]|0)-128|0;if(g>>>0>63>>>0){i=43;break L7}_=g|Z<<6;$=p+3|0}else{_=Y;$=h}}while(0);c[q>>2]=_;v=$;w=q+4|0;x=r-1|0;i=6;continue}}if((i|0)==54){return y|0}else if((i|0)==56){return y|0}else if((i|0)==20){aa=l;ba=m-1|0;ca=b;da=n;i=46}else if((i|0)==23){aa=l;ba=m-1|0;ca=b;da=n;i=46}else if((i|0)==37){aa=o;ba=p-1|0;ca=q;da=r;i=46}else if((i|0)==40){aa=Y;ba=p-1|0;ca=q;da=r;i=46}else if((i|0)==43){aa=Z;ba=p-1|0;ca=q;da=r;i=46}else if((i|0)==17){aa=l;ba=m-1|0;ca=b;da=n;i=46}if((i|0)==46){if((aa|0)==0){O=da;P=ca;Q=ba;i=47}else{ea=ca;fa=ba}}do{if((i|0)==47){if((a[Q]|0)!=0){ea=P;fa=Q;break}if((P|0)!=0){c[P>>2]=0;c[e>>2]=0}y=f-O|0;return y|0}}while(0);c[(kc()|0)>>2]=84;if((ea|0)==0){y=-1;return y|0}c[e>>2]=fa;y=-1;return y|0}function fn(b,e,f){b=b|0;e=e|0;f=f|0;var g=0,h=0,j=0,k=0,l=0,m=0,n=0;g=i;i=i+8|0;h=g|0;c[h>>2]=b;if((e|0)==0){j=0;i=g;return j|0}do{if((f|0)!=0){if((b|0)==0){k=h;c[h>>2]=k;l=k}else{l=b}k=a[e]|0;m=k&255;if(k<<24>>24>-1){c[l>>2]=m;j=k<<24>>24!=0|0;i=g;return j|0}k=m-194|0;if(k>>>0>50>>>0){break}m=e+1|0;n=c[s+(k<<2)>>2]|0;if(f>>>0<4>>>0){if((n&-2147483648>>>(((f*6|0)-6|0)>>>0)|0)!=0){break}}k=d[m]|0;m=k>>>3;if((m-16|m+(n>>26))>>>0>7>>>0){break}m=k-128|n<<6;if((m|0)>=0){c[l>>2]=m;j=2;i=g;return j|0}n=(d[e+2|0]|0)-128|0;if(n>>>0>63>>>0){break}k=n|m<<6;if((k|0)>=0){c[l>>2]=k;j=3;i=g;return j|0}m=(d[e+3|0]|0)-128|0;if(m>>>0>63>>>0){break}c[l>>2]=m|k<<6;j=4;i=g;return j|0}}while(0);c[(kc()|0)>>2]=84;j=-1;i=g;return j|0}function gn(b,d,e){b=b|0;d=d|0;e=e|0;var f=0;if((b|0)==0){f=1;return f|0}if(d>>>0<128>>>0){a[b]=d;f=1;return f|0}if(d>>>0<2048>>>0){a[b]=d>>>6|192;a[b+1|0]=d&63|128;f=2;return f|0}if(d>>>0<55296>>>0|(d-57344|0)>>>0<8192>>>0){a[b]=d>>>12|224;a[b+1|0]=d>>>6&63|128;a[b+2|0]=d&63|128;f=3;return f|0}if((d-65536|0)>>>0<1048576>>>0){a[b]=d>>>18|240;a[b+1|0]=d>>>12&63|128;a[b+2|0]=d>>>6&63|128;a[b+3|0]=d&63|128;f=4;return f|0}else{c[(kc()|0)>>2]=84;f=-1;return f|0}return 0}function hn(a,b,d,e,f){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0;f=i;i=i+264|0;g=f+256|0;h=c[b>>2]|0;c[g>>2]=h;j=(a|0)!=0;k=f|0;l=j?e:256;e=j?a:k;L1:do{if((h|0)==0|(l|0)==0){m=0;n=d;o=l;p=e;q=h}else{a=l;r=d;s=0;t=e;u=h;while(1){v=r>>>0>=a>>>0;if(!(v|r>>>0>32>>>0)){m=s;n=r;o=a;p=t;q=u;break L1}w=v?a:r;x=r-w|0;v=jn(t,g,w,0)|0;if((v|0)==-1){break}if((t|0)==(k|0)){y=k;z=a}else{y=t+v|0;z=a-v|0}w=v+s|0;v=c[g>>2]|0;if((v|0)==0|(z|0)==0){m=w;n=x;o=z;p=y;q=v;break L1}else{a=z;r=x;s=w;t=y;u=v}}m=-1;n=x;o=0;p=t;q=c[g>>2]|0}}while(0);L11:do{if((q|0)==0){A=m}else{if((o|0)==0|(n|0)==0){A=m;break}else{B=o;C=n;D=m;E=p;F=q}while(1){G=gn(E,c[F>>2]|0,0)|0;if((G+1|0)>>>0<2>>>0){break}x=(c[g>>2]|0)+4|0;c[g>>2]=x;y=C-1|0;z=D+1|0;if((B|0)==(G|0)|(y|0)==0){A=z;break L11}else{B=B-G|0;C=y;D=z;E=E+G|0;F=x}}if((G|0)!=0){A=-1;break}c[g>>2]=0;A=D}}while(0);if(!j){i=f;return A|0}c[b>>2]=c[g>>2];i=f;return A|0}function jn(b,d,e,f){b=b|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0;f=i;i=i+8|0;g=f|0;if((b|0)==0){h=c[d>>2]|0;j=g|0;k=c[h>>2]|0;if((k|0)==0){l=0;i=f;return l|0}else{m=0;n=h;o=k}while(1){if(o>>>0>127>>>0){k=gn(j,o,0)|0;if((k|0)==-1){l=-1;p=33;break}else{q=k}}else{q=1}k=q+m|0;h=n+4|0;r=c[h>>2]|0;if((r|0)==0){l=k;p=28;break}else{m=k;n=h;o=r}}if((p|0)==28){i=f;return l|0}else if((p|0)==33){i=f;return l|0}}L14:do{if(e>>>0>3>>>0){o=e;n=b;m=c[d>>2]|0;while(1){q=c[m>>2]|0;if((q|0)==0){s=o;t=n;break L14}if(q>>>0>127>>>0){j=gn(n,q,0)|0;if((j|0)==-1){l=-1;break}u=n+j|0;v=o-j|0;w=m}else{a[n]=q;u=n+1|0;v=o-1|0;w=c[d>>2]|0}q=w+4|0;c[d>>2]=q;if(v>>>0>3>>>0){o=v;n=u;m=q}else{s=v;t=u;break L14}}i=f;return l|0}else{s=e;t=b}}while(0);L26:do{if((s|0)==0){x=0}else{b=g|0;u=s;v=t;w=c[d>>2]|0;while(1){m=c[w>>2]|0;if((m|0)==0){p=24;break}if(m>>>0>127>>>0){n=gn(b,m,0)|0;if((n|0)==-1){l=-1;p=29;break}if(n>>>0>u>>>0){p=20;break}gn(v,c[w>>2]|0,0)|0;y=v+n|0;z=u-n|0;A=w}else{a[v]=m;y=v+1|0;z=u-1|0;A=c[d>>2]|0}m=A+4|0;c[d>>2]=m;if((z|0)==0){x=0;break L26}else{u=z;v=y;w=m}}if((p|0)==29){i=f;return l|0}else if((p|0)==24){a[v]=0;x=u;break}else if((p|0)==20){l=e-u|0;i=f;return l|0}}}while(0);c[d>>2]=0;l=e-x|0;i=f;return l|0}function kn(b,c){b=b|0;c=c|0;var d=0,e=0,f=0,g=0;d=ho(c|0)|0;if((a[b]|0)==0){e=0;return e|0}else{f=b}while(1){b=f+1|0;if((no(f|0,c|0,d|0)|0)==0){e=f;g=7;break}if((a[b]|0)==0){e=0;g=5;break}else{f=b}}if((g|0)==5){return e|0}else if((g|0)==7){return e|0}return 0}function ln(a){a=a|0;var b=0;b=a;while(1){if((c[b>>2]|0)==0){break}else{b=b+4|0}}return b-a>>2|0}function mn(a,b,d){a=a|0;b=b|0;d=d|0;var e=0,f=0,g=0;if((d|0)==0){return a|0}else{e=b;f=d;g=a}while(1){d=f-1|0;c[g>>2]=c[e>>2];if((d|0)==0){break}else{e=e+4|0;f=d;g=g+4|0}}return a|0}function nn(a,b,d){a=a|0;b=b|0;d=d|0;var e=0,f=0,g=0,h=0,i=0;e=(d|0)==0;if(a-b>>2>>>0<d>>>0){if(e){return a|0}else{f=d}do{f=f-1|0;c[a+(f<<2)>>2]=c[b+(f<<2)>>2];}while((f|0)!=0);return a|0}else{if(e){return a|0}else{g=b;h=d;i=a}while(1){d=h-1|0;c[i>>2]=c[g>>2];if((d|0)==0){break}else{g=g+4|0;h=d;i=i+4|0}}return a|0}return 0}function on(a,b,d){a=a|0;b=b|0;d=d|0;var e=0,f=0;if((d|0)==0){return a|0}else{e=d;f=a}while(1){d=e-1|0;c[f>>2]=b;if((d|0)==0){break}else{e=d;f=f+4|0}}return a|0}function pn(a){a=a|0;return}function qn(a){a=a|0;c[a>>2]=4504;return}function rn(a){a=a|0;Tn(a);return}function sn(a){a=a|0;return}function tn(a){a=a|0;return 2952}function un(a){a=a|0;pn(a|0);return}function vn(a){a=a|0;return}function wn(a){a=a|0;return}function xn(a){a=a|0;pn(a|0);Tn(a);return}function yn(a){a=a|0;pn(a|0);Tn(a);return}function zn(a){a=a|0;pn(a|0);Tn(a);return}function An(a,b,d){a=a|0;b=b|0;d=d|0;var e=0,f=0,g=0,h=0;e=i;i=i+56|0;f=e|0;if((a|0)==(b|0)){g=1;i=e;return g|0}if((b|0)==0){g=0;i=e;return g|0}h=En(b,11720,11704,0)|0;b=h;if((h|0)==0){g=0;i=e;return g|0}ko(f|0,0,56)|0;c[f>>2]=b;c[f+8>>2]=a;c[f+12>>2]=-1;c[f+48>>2]=1;nd[c[(c[h>>2]|0)+28>>2]&15](b,f,c[d>>2]|0,1);if((c[f+24>>2]|0)!=1){g=0;i=e;return g|0}c[d>>2]=c[f+16>>2];g=1;i=e;return g|0}function Bn(b,d,e,f){b=b|0;d=d|0;e=e|0;f=f|0;var g=0;if((c[d+8>>2]|0)!=(b|0)){return}b=d+16|0;g=c[b>>2]|0;if((g|0)==0){c[b>>2]=e;c[d+24>>2]=f;c[d+36>>2]=1;return}if((g|0)!=(e|0)){e=d+36|0;c[e>>2]=(c[e>>2]|0)+1;c[d+24>>2]=2;a[d+54|0]=1;return}e=d+24|0;if((c[e>>2]|0)!=2){return}c[e>>2]=f;return}function Cn(b,d,e,f){b=b|0;d=d|0;e=e|0;f=f|0;var g=0;if((b|0)!=(c[d+8>>2]|0)){g=c[b+8>>2]|0;nd[c[(c[g>>2]|0)+28>>2]&15](g,d,e,f);return}g=d+16|0;b=c[g>>2]|0;if((b|0)==0){c[g>>2]=e;c[d+24>>2]=f;c[d+36>>2]=1;return}if((b|0)!=(e|0)){e=d+36|0;c[e>>2]=(c[e>>2]|0)+1;c[d+24>>2]=2;a[d+54|0]=1;return}e=d+24|0;if((c[e>>2]|0)!=2){return}c[e>>2]=f;return}function Dn(b,d,e,f){b=b|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,i=0,j=0,k=0,l=0,m=0;if((b|0)==(c[d+8>>2]|0)){g=d+16|0;h=c[g>>2]|0;if((h|0)==0){c[g>>2]=e;c[d+24>>2]=f;c[d+36>>2]=1;return}if((h|0)!=(e|0)){h=d+36|0;c[h>>2]=(c[h>>2]|0)+1;c[d+24>>2]=2;a[d+54|0]=1;return}h=d+24|0;if((c[h>>2]|0)!=2){return}c[h>>2]=f;return}h=c[b+12>>2]|0;g=b+16+(h<<3)|0;i=c[b+20>>2]|0;j=i>>8;if((i&1|0)==0){k=j}else{k=c[(c[e>>2]|0)+j>>2]|0}j=c[b+16>>2]|0;nd[c[(c[j>>2]|0)+28>>2]&15](j,d,e+k|0,(i&2|0)!=0?f:2);if((h|0)<=1){return}h=d+54|0;i=e;k=b+24|0;while(1){b=c[k+4>>2]|0;j=b>>8;if((b&1|0)==0){l=j}else{l=c[(c[i>>2]|0)+j>>2]|0}j=c[k>>2]|0;nd[c[(c[j>>2]|0)+28>>2]&15](j,d,e+l|0,(b&2|0)!=0?f:2);if((a[h]&1)!=0){m=18;break}b=k+8|0;if(b>>>0<g>>>0){k=b}else{m=22;break}}if((m|0)==22){return}else if((m|0)==18){return}}function En(a,b,d,e){a=a|0;b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0;f=i;i=i+56|0;g=f|0;h=c[a>>2]|0;j=a+(c[h-8>>2]|0)|0;k=c[h-4>>2]|0;h=k;c[g>>2]=d;c[g+4>>2]=a;c[g+8>>2]=b;c[g+12>>2]=e;e=g+16|0;b=g+20|0;a=g+24|0;l=g+28|0;m=g+32|0;n=g+40|0;ko(e|0,0,39)|0;if((k|0)==(d|0)){c[g+48>>2]=1;kd[c[(c[k>>2]|0)+20>>2]&31](h,g,j,j,1,0);i=f;return((c[a>>2]|0)==1?j:0)|0}Yc[c[(c[k>>2]|0)+24>>2]&7](h,g,j,1,0);j=c[g+36>>2]|0;if((j|0)==1){do{if((c[a>>2]|0)!=1){if((c[n>>2]|0)!=0){o=0;i=f;return o|0}if((c[l>>2]|0)!=1){o=0;i=f;return o|0}if((c[m>>2]|0)==1){break}else{o=0}i=f;return o|0}}while(0);o=c[e>>2]|0;i=f;return o|0}else if((j|0)==0){if((c[n>>2]|0)!=1){o=0;i=f;return o|0}if((c[l>>2]|0)!=1){o=0;i=f;return o|0}o=(c[m>>2]|0)==1?c[b>>2]|0:0;i=f;return o|0}else{o=0;i=f;return o|0}return 0}function Fn(b,d,e,f,g){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;var h=0,i=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0;h=b|0;if((h|0)==(c[d+8>>2]|0)){if((c[d+4>>2]|0)!=(e|0)){return}i=d+28|0;if((c[i>>2]|0)==1){return}c[i>>2]=f;return}if((h|0)==(c[d>>2]|0)){do{if((c[d+16>>2]|0)!=(e|0)){h=d+20|0;if((c[h>>2]|0)==(e|0)){break}c[d+32>>2]=f;i=d+44|0;if((c[i>>2]|0)==4){return}j=c[b+12>>2]|0;k=b+16+(j<<3)|0;L19:do{if((j|0)>0){l=d+52|0;m=d+53|0;n=d+54|0;o=b+8|0;p=d+24|0;q=e;r=0;s=b+16|0;t=0;L21:while(1){a[l]=0;a[m]=0;u=c[s+4>>2]|0;v=u>>8;if((u&1|0)==0){w=v}else{w=c[(c[q>>2]|0)+v>>2]|0}v=c[s>>2]|0;kd[c[(c[v>>2]|0)+20>>2]&31](v,d,e,e+w|0,2-(u>>>1&1)|0,g);if((a[n]&1)!=0){x=t;y=r;break}do{if((a[m]&1)==0){z=t;A=r}else{if((a[l]&1)==0){if((c[o>>2]&1|0)==0){x=1;y=r;break L21}else{z=1;A=r;break}}if((c[p>>2]|0)==1){B=27;break L19}if((c[o>>2]&2|0)==0){B=27;break L19}else{z=1;A=1}}}while(0);u=s+8|0;if(u>>>0<k>>>0){r=A;s=u;t=z}else{x=z;y=A;break}}if(y){C=x;B=26}else{D=x;B=23}}else{D=0;B=23}}while(0);do{if((B|0)==23){c[h>>2]=e;k=d+40|0;c[k>>2]=(c[k>>2]|0)+1;if((c[d+36>>2]|0)!=1){C=D;B=26;break}if((c[d+24>>2]|0)!=2){C=D;B=26;break}a[d+54|0]=1;if(D){B=27}else{B=28}}}while(0);if((B|0)==26){if(C){B=27}else{B=28}}if((B|0)==28){c[i>>2]=4;return}else if((B|0)==27){c[i>>2]=3;return}}}while(0);if((f|0)!=1){return}c[d+32>>2]=1;return}C=c[b+12>>2]|0;D=b+16+(C<<3)|0;x=c[b+20>>2]|0;y=x>>8;if((x&1|0)==0){E=y}else{E=c[(c[e>>2]|0)+y>>2]|0}y=c[b+16>>2]|0;Yc[c[(c[y>>2]|0)+24>>2]&7](y,d,e+E|0,(x&2|0)!=0?f:2,g);x=b+24|0;if((C|0)<=1){return}C=c[b+8>>2]|0;do{if((C&2|0)==0){b=d+36|0;if((c[b>>2]|0)==1){break}if((C&1|0)==0){E=d+54|0;y=e;A=x;while(1){if((a[E]&1)!=0){B=67;break}if((c[b>>2]|0)==1){B=64;break}z=c[A+4>>2]|0;w=z>>8;if((z&1|0)==0){F=w}else{F=c[(c[y>>2]|0)+w>>2]|0}w=c[A>>2]|0;Yc[c[(c[w>>2]|0)+24>>2]&7](w,d,e+F|0,(z&2|0)!=0?f:2,g);z=A+8|0;if(z>>>0<D>>>0){A=z}else{B=55;break}}if((B|0)==64){return}else if((B|0)==67){return}else if((B|0)==55){return}}A=d+24|0;y=d+54|0;E=e;i=x;while(1){if((a[y]&1)!=0){B=63;break}if((c[b>>2]|0)==1){if((c[A>>2]|0)==1){B=65;break}}z=c[i+4>>2]|0;w=z>>8;if((z&1|0)==0){G=w}else{G=c[(c[E>>2]|0)+w>>2]|0}w=c[i>>2]|0;Yc[c[(c[w>>2]|0)+24>>2]&7](w,d,e+G|0,(z&2|0)!=0?f:2,g);z=i+8|0;if(z>>>0<D>>>0){i=z}else{B=66;break}}if((B|0)==63){return}else if((B|0)==65){return}else if((B|0)==66){return}}}while(0);G=d+54|0;F=e;C=x;while(1){if((a[G]&1)!=0){B=62;break}x=c[C+4>>2]|0;i=x>>8;if((x&1|0)==0){H=i}else{H=c[(c[F>>2]|0)+i>>2]|0}i=c[C>>2]|0;Yc[c[(c[i>>2]|0)+24>>2]&7](i,d,e+H|0,(x&2|0)!=0?f:2,g);x=C+8|0;if(x>>>0<D>>>0){C=x}else{B=58;break}}if((B|0)==62){return}else if((B|0)==58){return}}function Gn(b,d,e,f,g){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;var h=0,i=0,j=0,k=0,l=0,m=0,n=0;h=b|0;if((h|0)==(c[d+8>>2]|0)){if((c[d+4>>2]|0)!=(e|0)){return}i=d+28|0;if((c[i>>2]|0)==1){return}c[i>>2]=f;return}if((h|0)!=(c[d>>2]|0)){h=c[b+8>>2]|0;Yc[c[(c[h>>2]|0)+24>>2]&7](h,d,e,f,g);return}do{if((c[d+16>>2]|0)!=(e|0)){h=d+20|0;if((c[h>>2]|0)==(e|0)){break}c[d+32>>2]=f;i=d+44|0;if((c[i>>2]|0)==4){return}j=d+52|0;a[j]=0;k=d+53|0;a[k]=0;l=c[b+8>>2]|0;kd[c[(c[l>>2]|0)+20>>2]&31](l,d,e,e,1,g);if((a[k]&1)==0){m=0;n=13}else{if((a[j]&1)==0){m=1;n=13}}L23:do{if((n|0)==13){c[h>>2]=e;j=d+40|0;c[j>>2]=(c[j>>2]|0)+1;do{if((c[d+36>>2]|0)==1){if((c[d+24>>2]|0)!=2){n=16;break}a[d+54|0]=1;if(m){break L23}}else{n=16}}while(0);if((n|0)==16){if(m){break}}c[i>>2]=4;return}}while(0);c[i>>2]=3;return}}while(0);if((f|0)!=1){return}c[d+32>>2]=1;return}function Hn(b,d,e,f,g){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;if((c[d+8>>2]|0)==(b|0)){if((c[d+4>>2]|0)!=(e|0)){return}g=d+28|0;if((c[g>>2]|0)==1){return}c[g>>2]=f;return}if((c[d>>2]|0)!=(b|0)){return}do{if((c[d+16>>2]|0)!=(e|0)){b=d+20|0;if((c[b>>2]|0)==(e|0)){break}c[d+32>>2]=f;c[b>>2]=e;b=d+40|0;c[b>>2]=(c[b>>2]|0)+1;do{if((c[d+36>>2]|0)==1){if((c[d+24>>2]|0)!=2){break}a[d+54|0]=1}}while(0);c[d+44>>2]=4;return}}while(0);if((f|0)!=1){return}c[d+32>>2]=1;return}function In(b,d,e,f,g,h){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var i=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0;if((b|0)!=(c[d+8>>2]|0)){i=d+52|0;j=a[i]&1;k=d+53|0;l=a[k]&1;m=c[b+12>>2]|0;n=b+16+(m<<3)|0;a[i]=0;a[k]=0;o=c[b+20>>2]|0;p=o>>8;if((o&1|0)==0){q=p}else{q=c[(c[f>>2]|0)+p>>2]|0}p=c[b+16>>2]|0;kd[c[(c[p>>2]|0)+20>>2]&31](p,d,e,f+q|0,(o&2|0)!=0?g:2,h);L6:do{if((m|0)>1){o=d+24|0;q=b+8|0;p=d+54|0;r=f;s=b+24|0;do{if((a[p]&1)!=0){break L6}do{if((a[i]&1)==0){if((a[k]&1)==0){break}if((c[q>>2]&1|0)==0){break L6}}else{if((c[o>>2]|0)==1){break L6}if((c[q>>2]&2|0)==0){break L6}}}while(0);a[i]=0;a[k]=0;t=c[s+4>>2]|0;u=t>>8;if((t&1|0)==0){v=u}else{v=c[(c[r>>2]|0)+u>>2]|0}u=c[s>>2]|0;kd[c[(c[u>>2]|0)+20>>2]&31](u,d,e,f+v|0,(t&2|0)!=0?g:2,h);s=s+8|0;}while(s>>>0<n>>>0)}}while(0);a[i]=j;a[k]=l;return}a[d+53|0]=1;if((c[d+4>>2]|0)!=(f|0)){return}a[d+52|0]=1;f=d+16|0;l=c[f>>2]|0;if((l|0)==0){c[f>>2]=e;c[d+24>>2]=g;c[d+36>>2]=1;if(!((c[d+48>>2]|0)==1&(g|0)==1)){return}a[d+54|0]=1;return}if((l|0)!=(e|0)){e=d+36|0;c[e>>2]=(c[e>>2]|0)+1;a[d+54|0]=1;return}e=d+24|0;l=c[e>>2]|0;if((l|0)==2){c[e>>2]=g;w=g}else{w=l}if(!((c[d+48>>2]|0)==1&(w|0)==1)){return}a[d+54|0]=1;return}function Jn(b,d,e,f,g,h){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var i=0,j=0;if((b|0)!=(c[d+8>>2]|0)){i=c[b+8>>2]|0;kd[c[(c[i>>2]|0)+20>>2]&31](i,d,e,f,g,h);return}a[d+53|0]=1;if((c[d+4>>2]|0)!=(f|0)){return}a[d+52|0]=1;f=d+16|0;h=c[f>>2]|0;if((h|0)==0){c[f>>2]=e;c[d+24>>2]=g;c[d+36>>2]=1;if(!((c[d+48>>2]|0)==1&(g|0)==1)){return}a[d+54|0]=1;return}if((h|0)!=(e|0)){e=d+36|0;c[e>>2]=(c[e>>2]|0)+1;a[d+54|0]=1;return}e=d+24|0;h=c[e>>2]|0;if((h|0)==2){c[e>>2]=g;j=g}else{j=h}if(!((c[d+48>>2]|0)==1&(j|0)==1)){return}a[d+54|0]=1;return}function Kn(b,d,e,f,g,h){b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var i=0;if((c[d+8>>2]|0)!=(b|0)){return}a[d+53|0]=1;if((c[d+4>>2]|0)!=(f|0)){return}a[d+52|0]=1;f=d+16|0;b=c[f>>2]|0;if((b|0)==0){c[f>>2]=e;c[d+24>>2]=g;c[d+36>>2]=1;if(!((c[d+48>>2]|0)==1&(g|0)==1)){return}a[d+54|0]=1;return}if((b|0)!=(e|0)){e=d+36|0;c[e>>2]=(c[e>>2]|0)+1;a[d+54|0]=1;return}e=d+24|0;b=c[e>>2]|0;if((b|0)==2){c[e>>2]=g;i=g}else{i=b}if(!((c[d+48>>2]|0)==1&(i|0)==1)){return}a[d+54|0]=1;return}function Ln(a){a=a|0;var b=0,d=0,e=0,f=0,g=0,h=0,i=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0,Z=0,_=0,$=0,aa=0,ba=0,ca=0,da=0,ea=0,fa=0,ga=0,ha=0,ia=0,ja=0,ka=0,la=0,ma=0,na=0,oa=0,pa=0,qa=0,ra=0,sa=0,ta=0,ua=0,va=0,wa=0,xa=0,ya=0,za=0,Aa=0,Ba=0,Ca=0,Da=0,Ea=0,Fa=0,Ga=0,Ha=0,Ia=0,Ja=0;do{if(a>>>0<245>>>0){if(a>>>0<11>>>0){b=16}else{b=a+11&-8}d=b>>>3;e=c[7824]|0;f=e>>>(d>>>0);if((f&3|0)!=0){g=(f&1^1)+d|0;h=g<<1;i=31336+(h<<2)|0;j=31336+(h+2<<2)|0;h=c[j>>2]|0;k=h+8|0;l=c[k>>2]|0;do{if((i|0)==(l|0)){c[7824]=e&~(1<<g)}else{if(l>>>0<(c[7828]|0)>>>0){Bc();return 0}m=l+12|0;if((c[m>>2]|0)==(h|0)){c[m>>2]=i;c[j>>2]=l;break}else{Bc();return 0}}}while(0);l=g<<3;c[h+4>>2]=l|3;j=h+(l|4)|0;c[j>>2]=c[j>>2]|1;n=k;return n|0}if(b>>>0<=(c[7826]|0)>>>0){o=b;break}if((f|0)!=0){j=2<<d;l=f<<d&(j|-j);j=(l&-l)-1|0;l=j>>>12&16;i=j>>>(l>>>0);j=i>>>5&8;m=i>>>(j>>>0);i=m>>>2&4;p=m>>>(i>>>0);m=p>>>1&2;q=p>>>(m>>>0);p=q>>>1&1;r=(j|l|i|m|p)+(q>>>(p>>>0))|0;p=r<<1;q=31336+(p<<2)|0;m=31336+(p+2<<2)|0;p=c[m>>2]|0;i=p+8|0;l=c[i>>2]|0;do{if((q|0)==(l|0)){c[7824]=e&~(1<<r)}else{if(l>>>0<(c[7828]|0)>>>0){Bc();return 0}j=l+12|0;if((c[j>>2]|0)==(p|0)){c[j>>2]=q;c[m>>2]=l;break}else{Bc();return 0}}}while(0);l=r<<3;m=l-b|0;c[p+4>>2]=b|3;q=p;e=q+b|0;c[q+(b|4)>>2]=m|1;c[q+l>>2]=m;l=c[7826]|0;if((l|0)!=0){q=c[7829]|0;d=l>>>3;l=d<<1;f=31336+(l<<2)|0;k=c[7824]|0;h=1<<d;do{if((k&h|0)==0){c[7824]=k|h;s=f;t=31336+(l+2<<2)|0}else{d=31336+(l+2<<2)|0;g=c[d>>2]|0;if(g>>>0>=(c[7828]|0)>>>0){s=g;t=d;break}Bc();return 0}}while(0);c[t>>2]=q;c[s+12>>2]=q;c[q+8>>2]=s;c[q+12>>2]=f}c[7826]=m;c[7829]=e;n=i;return n|0}l=c[7825]|0;if((l|0)==0){o=b;break}h=(l&-l)-1|0;l=h>>>12&16;k=h>>>(l>>>0);h=k>>>5&8;p=k>>>(h>>>0);k=p>>>2&4;r=p>>>(k>>>0);p=r>>>1&2;d=r>>>(p>>>0);r=d>>>1&1;g=c[31600+((h|l|k|p|r)+(d>>>(r>>>0))<<2)>>2]|0;r=g;d=g;p=(c[g+4>>2]&-8)-b|0;while(1){g=c[r+16>>2]|0;if((g|0)==0){k=c[r+20>>2]|0;if((k|0)==0){break}else{u=k}}else{u=g}g=(c[u+4>>2]&-8)-b|0;k=g>>>0<p>>>0;r=u;d=k?u:d;p=k?g:p}r=d;i=c[7828]|0;if(r>>>0<i>>>0){Bc();return 0}e=r+b|0;m=e;if(r>>>0>=e>>>0){Bc();return 0}e=c[d+24>>2]|0;f=c[d+12>>2]|0;do{if((f|0)==(d|0)){q=d+20|0;g=c[q>>2]|0;if((g|0)==0){k=d+16|0;l=c[k>>2]|0;if((l|0)==0){v=0;break}else{w=l;x=k}}else{w=g;x=q}while(1){q=w+20|0;g=c[q>>2]|0;if((g|0)!=0){w=g;x=q;continue}q=w+16|0;g=c[q>>2]|0;if((g|0)==0){break}else{w=g;x=q}}if(x>>>0<i>>>0){Bc();return 0}else{c[x>>2]=0;v=w;break}}else{q=c[d+8>>2]|0;if(q>>>0<i>>>0){Bc();return 0}g=q+12|0;if((c[g>>2]|0)!=(d|0)){Bc();return 0}k=f+8|0;if((c[k>>2]|0)==(d|0)){c[g>>2]=f;c[k>>2]=q;v=f;break}else{Bc();return 0}}}while(0);L202:do{if((e|0)!=0){f=d+28|0;i=31600+(c[f>>2]<<2)|0;do{if((d|0)==(c[i>>2]|0)){c[i>>2]=v;if((v|0)!=0){break}c[7825]=c[7825]&~(1<<c[f>>2]);break L202}else{if(e>>>0<(c[7828]|0)>>>0){Bc();return 0}q=e+16|0;if((c[q>>2]|0)==(d|0)){c[q>>2]=v}else{c[e+20>>2]=v}if((v|0)==0){break L202}}}while(0);if(v>>>0<(c[7828]|0)>>>0){Bc();return 0}c[v+24>>2]=e;f=c[d+16>>2]|0;do{if((f|0)!=0){if(f>>>0<(c[7828]|0)>>>0){Bc();return 0}else{c[v+16>>2]=f;c[f+24>>2]=v;break}}}while(0);f=c[d+20>>2]|0;if((f|0)==0){break}if(f>>>0<(c[7828]|0)>>>0){Bc();return 0}else{c[v+20>>2]=f;c[f+24>>2]=v;break}}}while(0);if(p>>>0<16>>>0){e=p+b|0;c[d+4>>2]=e|3;f=r+(e+4)|0;c[f>>2]=c[f>>2]|1}else{c[d+4>>2]=b|3;c[r+(b|4)>>2]=p|1;c[r+(p+b)>>2]=p;f=c[7826]|0;if((f|0)!=0){e=c[7829]|0;i=f>>>3;f=i<<1;q=31336+(f<<2)|0;k=c[7824]|0;g=1<<i;do{if((k&g|0)==0){c[7824]=k|g;y=q;z=31336+(f+2<<2)|0}else{i=31336+(f+2<<2)|0;l=c[i>>2]|0;if(l>>>0>=(c[7828]|0)>>>0){y=l;z=i;break}Bc();return 0}}while(0);c[z>>2]=e;c[y+12>>2]=e;c[e+8>>2]=y;c[e+12>>2]=q}c[7826]=p;c[7829]=m}n=d+8|0;return n|0}else{if(a>>>0>4294967231>>>0){o=-1;break}f=a+11|0;g=f&-8;k=c[7825]|0;if((k|0)==0){o=g;break}r=-g|0;i=f>>>8;do{if((i|0)==0){A=0}else{if(g>>>0>16777215>>>0){A=31;break}f=(i+1048320|0)>>>16&8;l=i<<f;h=(l+520192|0)>>>16&4;j=l<<h;l=(j+245760|0)>>>16&2;B=14-(h|f|l)+(j<<l>>>15)|0;A=g>>>((B+7|0)>>>0)&1|B<<1}}while(0);i=c[31600+(A<<2)>>2]|0;L9:do{if((i|0)==0){C=0;D=r;E=0}else{if((A|0)==31){F=0}else{F=25-(A>>>1)|0}d=0;m=r;p=i;q=g<<F;e=0;while(1){B=c[p+4>>2]&-8;l=B-g|0;if(l>>>0<m>>>0){if((B|0)==(g|0)){C=p;D=l;E=p;break L9}else{G=p;H=l}}else{G=d;H=m}l=c[p+20>>2]|0;B=c[p+16+(q>>>31<<2)>>2]|0;j=(l|0)==0|(l|0)==(B|0)?e:l;if((B|0)==0){C=G;D=H;E=j;break}else{d=G;m=H;p=B;q=q<<1;e=j}}}}while(0);if((E|0)==0&(C|0)==0){i=2<<A;r=k&(i|-i);if((r|0)==0){o=g;break}i=(r&-r)-1|0;r=i>>>12&16;e=i>>>(r>>>0);i=e>>>5&8;q=e>>>(i>>>0);e=q>>>2&4;p=q>>>(e>>>0);q=p>>>1&2;m=p>>>(q>>>0);p=m>>>1&1;I=c[31600+((i|r|e|q|p)+(m>>>(p>>>0))<<2)>>2]|0}else{I=E}if((I|0)==0){J=D;K=C}else{p=I;m=D;q=C;while(1){e=(c[p+4>>2]&-8)-g|0;r=e>>>0<m>>>0;i=r?e:m;e=r?p:q;r=c[p+16>>2]|0;if((r|0)!=0){p=r;m=i;q=e;continue}r=c[p+20>>2]|0;if((r|0)==0){J=i;K=e;break}else{p=r;m=i;q=e}}}if((K|0)==0){o=g;break}if(J>>>0>=((c[7826]|0)-g|0)>>>0){o=g;break}q=K;m=c[7828]|0;if(q>>>0<m>>>0){Bc();return 0}p=q+g|0;k=p;if(q>>>0>=p>>>0){Bc();return 0}e=c[K+24>>2]|0;i=c[K+12>>2]|0;do{if((i|0)==(K|0)){r=K+20|0;d=c[r>>2]|0;if((d|0)==0){j=K+16|0;B=c[j>>2]|0;if((B|0)==0){L=0;break}else{M=B;N=j}}else{M=d;N=r}while(1){r=M+20|0;d=c[r>>2]|0;if((d|0)!=0){M=d;N=r;continue}r=M+16|0;d=c[r>>2]|0;if((d|0)==0){break}else{M=d;N=r}}if(N>>>0<m>>>0){Bc();return 0}else{c[N>>2]=0;L=M;break}}else{r=c[K+8>>2]|0;if(r>>>0<m>>>0){Bc();return 0}d=r+12|0;if((c[d>>2]|0)!=(K|0)){Bc();return 0}j=i+8|0;if((c[j>>2]|0)==(K|0)){c[d>>2]=i;c[j>>2]=r;L=i;break}else{Bc();return 0}}}while(0);L59:do{if((e|0)!=0){i=K+28|0;m=31600+(c[i>>2]<<2)|0;do{if((K|0)==(c[m>>2]|0)){c[m>>2]=L;if((L|0)!=0){break}c[7825]=c[7825]&~(1<<c[i>>2]);break L59}else{if(e>>>0<(c[7828]|0)>>>0){Bc();return 0}r=e+16|0;if((c[r>>2]|0)==(K|0)){c[r>>2]=L}else{c[e+20>>2]=L}if((L|0)==0){break L59}}}while(0);if(L>>>0<(c[7828]|0)>>>0){Bc();return 0}c[L+24>>2]=e;i=c[K+16>>2]|0;do{if((i|0)!=0){if(i>>>0<(c[7828]|0)>>>0){Bc();return 0}else{c[L+16>>2]=i;c[i+24>>2]=L;break}}}while(0);i=c[K+20>>2]|0;if((i|0)==0){break}if(i>>>0<(c[7828]|0)>>>0){Bc();return 0}else{c[L+20>>2]=i;c[i+24>>2]=L;break}}}while(0);L87:do{if(J>>>0<16>>>0){e=J+g|0;c[K+4>>2]=e|3;i=q+(e+4)|0;c[i>>2]=c[i>>2]|1}else{c[K+4>>2]=g|3;c[q+(g|4)>>2]=J|1;c[q+(J+g)>>2]=J;i=J>>>3;if(J>>>0<256>>>0){e=i<<1;m=31336+(e<<2)|0;r=c[7824]|0;j=1<<i;do{if((r&j|0)==0){c[7824]=r|j;O=m;P=31336+(e+2<<2)|0}else{i=31336+(e+2<<2)|0;d=c[i>>2]|0;if(d>>>0>=(c[7828]|0)>>>0){O=d;P=i;break}Bc();return 0}}while(0);c[P>>2]=k;c[O+12>>2]=k;c[q+(g+8)>>2]=O;c[q+(g+12)>>2]=m;break}e=p;j=J>>>8;do{if((j|0)==0){Q=0}else{if(J>>>0>16777215>>>0){Q=31;break}r=(j+1048320|0)>>>16&8;i=j<<r;d=(i+520192|0)>>>16&4;B=i<<d;i=(B+245760|0)>>>16&2;l=14-(d|r|i)+(B<<i>>>15)|0;Q=J>>>((l+7|0)>>>0)&1|l<<1}}while(0);j=31600+(Q<<2)|0;c[q+(g+28)>>2]=Q;c[q+(g+20)>>2]=0;c[q+(g+16)>>2]=0;m=c[7825]|0;l=1<<Q;if((m&l|0)==0){c[7825]=m|l;c[j>>2]=e;c[q+(g+24)>>2]=j;c[q+(g+12)>>2]=e;c[q+(g+8)>>2]=e;break}l=c[j>>2]|0;if((Q|0)==31){R=0}else{R=25-(Q>>>1)|0}L108:do{if((c[l+4>>2]&-8|0)==(J|0)){S=l}else{j=l;m=J<<R;while(1){T=j+16+(m>>>31<<2)|0;i=c[T>>2]|0;if((i|0)==0){break}if((c[i+4>>2]&-8|0)==(J|0)){S=i;break L108}else{j=i;m=m<<1}}if(T>>>0<(c[7828]|0)>>>0){Bc();return 0}else{c[T>>2]=e;c[q+(g+24)>>2]=j;c[q+(g+12)>>2]=e;c[q+(g+8)>>2]=e;break L87}}}while(0);l=S+8|0;m=c[l>>2]|0;i=c[7828]|0;if(S>>>0<i>>>0){Bc();return 0}if(m>>>0<i>>>0){Bc();return 0}else{c[m+12>>2]=e;c[l>>2]=e;c[q+(g+8)>>2]=m;c[q+(g+12)>>2]=S;c[q+(g+24)>>2]=0;break}}}while(0);n=K+8|0;return n|0}}while(0);K=c[7826]|0;if(o>>>0<=K>>>0){S=K-o|0;T=c[7829]|0;if(S>>>0>15>>>0){J=T;c[7829]=J+o;c[7826]=S;c[J+(o+4)>>2]=S|1;c[J+K>>2]=S;c[T+4>>2]=o|3}else{c[7826]=0;c[7829]=0;c[T+4>>2]=K|3;S=T+(K+4)|0;c[S>>2]=c[S>>2]|1}n=T+8|0;return n|0}T=c[7827]|0;if(o>>>0<T>>>0){S=T-o|0;c[7827]=S;T=c[7830]|0;K=T;c[7830]=K+o;c[K+(o+4)>>2]=S|1;c[T+4>>2]=o|3;n=T+8|0;return n|0}do{if((c[7788]|0)==0){T=yc(30)|0;if((T-1&T|0)==0){c[7790]=T;c[7789]=T;c[7791]=-1;c[7792]=-1;c[7793]=0;c[7935]=0;c[7788]=(Wc(0)|0)&-16^1431655768;break}else{Bc();return 0}}}while(0);T=o+48|0;S=c[7790]|0;K=o+47|0;J=S+K|0;R=-S|0;S=J&R;if(S>>>0<=o>>>0){n=0;return n|0}Q=c[7934]|0;do{if((Q|0)!=0){O=c[7932]|0;P=O+S|0;if(P>>>0<=O>>>0|P>>>0>Q>>>0){n=0}else{break}return n|0}}while(0);L269:do{if((c[7935]&4|0)==0){Q=c[7830]|0;L271:do{if((Q|0)==0){U=182}else{P=Q;O=31744;while(1){V=O|0;L=c[V>>2]|0;if(L>>>0<=P>>>0){W=O+4|0;if((L+(c[W>>2]|0)|0)>>>0>P>>>0){break}}L=c[O+8>>2]|0;if((L|0)==0){U=182;break L271}else{O=L}}if((O|0)==0){U=182;break}P=J-(c[7827]|0)&R;if(P>>>0>=2147483647>>>0){X=0;break}e=jc(P|0)|0;L=(e|0)==((c[V>>2]|0)+(c[W>>2]|0)|0);Y=L?e:-1;Z=L?P:0;_=e;$=P;U=191}}while(0);do{if((U|0)==182){Q=jc(0)|0;if((Q|0)==-1){X=0;break}P=Q;e=c[7789]|0;L=e-1|0;if((L&P|0)==0){aa=S}else{aa=S-P+(L+P&-e)|0}e=c[7932]|0;P=e+aa|0;if(!(aa>>>0>o>>>0&aa>>>0<2147483647>>>0)){X=0;break}L=c[7934]|0;if((L|0)!=0){if(P>>>0<=e>>>0|P>>>0>L>>>0){X=0;break}}L=jc(aa|0)|0;P=(L|0)==(Q|0);Y=P?Q:-1;Z=P?aa:0;_=L;$=aa;U=191}}while(0);L291:do{if((U|0)==191){L=-$|0;if((Y|0)!=-1){ba=Z;ca=Y;U=202;break L269}do{if((_|0)!=-1&$>>>0<2147483647>>>0&$>>>0<T>>>0){P=c[7790]|0;Q=K-$+P&-P;if(Q>>>0>=2147483647>>>0){da=$;break}if((jc(Q|0)|0)==-1){jc(L|0)|0;X=Z;break L291}else{da=Q+$|0;break}}else{da=$}}while(0);if((_|0)==-1){X=Z}else{ba=da;ca=_;U=202;break L269}}}while(0);c[7935]=c[7935]|4;ea=X;U=199}else{ea=0;U=199}}while(0);do{if((U|0)==199){if(S>>>0>=2147483647>>>0){break}X=jc(S|0)|0;_=jc(0)|0;if(!((_|0)!=-1&(X|0)!=-1&X>>>0<_>>>0)){break}da=_-X|0;_=da>>>0>(o+40|0)>>>0;if(_){ba=_?da:ea;ca=X;U=202}}}while(0);do{if((U|0)==202){ea=(c[7932]|0)+ba|0;c[7932]=ea;if(ea>>>0>(c[7933]|0)>>>0){c[7933]=ea}ea=c[7830]|0;L311:do{if((ea|0)==0){S=c[7828]|0;if((S|0)==0|ca>>>0<S>>>0){c[7828]=ca}c[7936]=ca;c[7937]=ba;c[7939]=0;c[7833]=c[7788];c[7832]=-1;S=0;do{X=S<<1;da=31336+(X<<2)|0;c[31336+(X+3<<2)>>2]=da;c[31336+(X+2<<2)>>2]=da;S=S+1|0;}while(S>>>0<32>>>0);S=ca+8|0;if((S&7|0)==0){fa=0}else{fa=-S&7}S=ba-40-fa|0;c[7830]=ca+fa;c[7827]=S;c[ca+(fa+4)>>2]=S|1;c[ca+(ba-36)>>2]=40;c[7831]=c[7792]}else{S=31744;while(1){ga=c[S>>2]|0;ha=S+4|0;ia=c[ha>>2]|0;if((ca|0)==(ga+ia|0)){U=214;break}da=c[S+8>>2]|0;if((da|0)==0){break}else{S=da}}do{if((U|0)==214){if((c[S+12>>2]&8|0)!=0){break}da=ea;if(!(da>>>0>=ga>>>0&da>>>0<ca>>>0)){break}c[ha>>2]=ia+ba;da=c[7830]|0;X=(c[7827]|0)+ba|0;_=da;Z=da+8|0;if((Z&7|0)==0){ja=0}else{ja=-Z&7}Z=X-ja|0;c[7830]=_+ja;c[7827]=Z;c[_+(ja+4)>>2]=Z|1;c[_+(X+4)>>2]=40;c[7831]=c[7792];break L311}}while(0);if(ca>>>0<(c[7828]|0)>>>0){c[7828]=ca}S=ca+ba|0;X=31744;while(1){ka=X|0;if((c[ka>>2]|0)==(S|0)){U=224;break}_=c[X+8>>2]|0;if((_|0)==0){break}else{X=_}}do{if((U|0)==224){if((c[X+12>>2]&8|0)!=0){break}c[ka>>2]=ca;S=X+4|0;c[S>>2]=(c[S>>2]|0)+ba;S=ca+8|0;if((S&7|0)==0){la=0}else{la=-S&7}S=ca+(ba+8)|0;if((S&7|0)==0){ma=0}else{ma=-S&7}S=ca+(ma+ba)|0;_=S;Z=la+o|0;da=ca+Z|0;$=da;K=S-(ca+la)-o|0;c[ca+(la+4)>>2]=o|3;L348:do{if((_|0)==(c[7830]|0)){T=(c[7827]|0)+K|0;c[7827]=T;c[7830]=$;c[ca+(Z+4)>>2]=T|1}else{if((_|0)==(c[7829]|0)){T=(c[7826]|0)+K|0;c[7826]=T;c[7829]=$;c[ca+(Z+4)>>2]=T|1;c[ca+(T+Z)>>2]=T;break}T=ba+4|0;Y=c[ca+(T+ma)>>2]|0;if((Y&3|0)==1){aa=Y&-8;W=Y>>>3;L356:do{if(Y>>>0<256>>>0){V=c[ca+((ma|8)+ba)>>2]|0;R=c[ca+(ba+12+ma)>>2]|0;J=31336+(W<<1<<2)|0;do{if((V|0)!=(J|0)){if(V>>>0<(c[7828]|0)>>>0){Bc();return 0}if((c[V+12>>2]|0)==(_|0)){break}Bc();return 0}}while(0);if((R|0)==(V|0)){c[7824]=c[7824]&~(1<<W);break}do{if((R|0)==(J|0)){na=R+8|0}else{if(R>>>0<(c[7828]|0)>>>0){Bc();return 0}L=R+8|0;if((c[L>>2]|0)==(_|0)){na=L;break}Bc();return 0}}while(0);c[V+12>>2]=R;c[na>>2]=V}else{J=S;L=c[ca+((ma|24)+ba)>>2]|0;O=c[ca+(ba+12+ma)>>2]|0;do{if((O|0)==(J|0)){Q=ma|16;P=ca+(T+Q)|0;e=c[P>>2]|0;if((e|0)==0){M=ca+(Q+ba)|0;Q=c[M>>2]|0;if((Q|0)==0){oa=0;break}else{pa=Q;qa=M}}else{pa=e;qa=P}while(1){P=pa+20|0;e=c[P>>2]|0;if((e|0)!=0){pa=e;qa=P;continue}P=pa+16|0;e=c[P>>2]|0;if((e|0)==0){break}else{pa=e;qa=P}}if(qa>>>0<(c[7828]|0)>>>0){Bc();return 0}else{c[qa>>2]=0;oa=pa;break}}else{P=c[ca+((ma|8)+ba)>>2]|0;if(P>>>0<(c[7828]|0)>>>0){Bc();return 0}e=P+12|0;if((c[e>>2]|0)!=(J|0)){Bc();return 0}M=O+8|0;if((c[M>>2]|0)==(J|0)){c[e>>2]=O;c[M>>2]=P;oa=O;break}else{Bc();return 0}}}while(0);if((L|0)==0){break}O=ca+(ba+28+ma)|0;V=31600+(c[O>>2]<<2)|0;do{if((J|0)==(c[V>>2]|0)){c[V>>2]=oa;if((oa|0)!=0){break}c[7825]=c[7825]&~(1<<c[O>>2]);break L356}else{if(L>>>0<(c[7828]|0)>>>0){Bc();return 0}R=L+16|0;if((c[R>>2]|0)==(J|0)){c[R>>2]=oa}else{c[L+20>>2]=oa}if((oa|0)==0){break L356}}}while(0);if(oa>>>0<(c[7828]|0)>>>0){Bc();return 0}c[oa+24>>2]=L;J=ma|16;O=c[ca+(J+ba)>>2]|0;do{if((O|0)!=0){if(O>>>0<(c[7828]|0)>>>0){Bc();return 0}else{c[oa+16>>2]=O;c[O+24>>2]=oa;break}}}while(0);O=c[ca+(T+J)>>2]|0;if((O|0)==0){break}if(O>>>0<(c[7828]|0)>>>0){Bc();return 0}else{c[oa+20>>2]=O;c[O+24>>2]=oa;break}}}while(0);ra=ca+((aa|ma)+ba)|0;sa=aa+K|0}else{ra=_;sa=K}T=ra+4|0;c[T>>2]=c[T>>2]&-2;c[ca+(Z+4)>>2]=sa|1;c[ca+(sa+Z)>>2]=sa;T=sa>>>3;if(sa>>>0<256>>>0){W=T<<1;Y=31336+(W<<2)|0;O=c[7824]|0;L=1<<T;do{if((O&L|0)==0){c[7824]=O|L;ta=Y;ua=31336+(W+2<<2)|0}else{T=31336+(W+2<<2)|0;V=c[T>>2]|0;if(V>>>0>=(c[7828]|0)>>>0){ta=V;ua=T;break}Bc();return 0}}while(0);c[ua>>2]=$;c[ta+12>>2]=$;c[ca+(Z+8)>>2]=ta;c[ca+(Z+12)>>2]=Y;break}W=da;L=sa>>>8;do{if((L|0)==0){va=0}else{if(sa>>>0>16777215>>>0){va=31;break}O=(L+1048320|0)>>>16&8;aa=L<<O;T=(aa+520192|0)>>>16&4;V=aa<<T;aa=(V+245760|0)>>>16&2;R=14-(T|O|aa)+(V<<aa>>>15)|0;va=sa>>>((R+7|0)>>>0)&1|R<<1}}while(0);L=31600+(va<<2)|0;c[ca+(Z+28)>>2]=va;c[ca+(Z+20)>>2]=0;c[ca+(Z+16)>>2]=0;Y=c[7825]|0;R=1<<va;if((Y&R|0)==0){c[7825]=Y|R;c[L>>2]=W;c[ca+(Z+24)>>2]=L;c[ca+(Z+12)>>2]=W;c[ca+(Z+8)>>2]=W;break}R=c[L>>2]|0;if((va|0)==31){wa=0}else{wa=25-(va>>>1)|0}L445:do{if((c[R+4>>2]&-8|0)==(sa|0)){xa=R}else{L=R;Y=sa<<wa;while(1){ya=L+16+(Y>>>31<<2)|0;aa=c[ya>>2]|0;if((aa|0)==0){break}if((c[aa+4>>2]&-8|0)==(sa|0)){xa=aa;break L445}else{L=aa;Y=Y<<1}}if(ya>>>0<(c[7828]|0)>>>0){Bc();return 0}else{c[ya>>2]=W;c[ca+(Z+24)>>2]=L;c[ca+(Z+12)>>2]=W;c[ca+(Z+8)>>2]=W;break L348}}}while(0);R=xa+8|0;Y=c[R>>2]|0;J=c[7828]|0;if(xa>>>0<J>>>0){Bc();return 0}if(Y>>>0<J>>>0){Bc();return 0}else{c[Y+12>>2]=W;c[R>>2]=W;c[ca+(Z+8)>>2]=Y;c[ca+(Z+12)>>2]=xa;c[ca+(Z+24)>>2]=0;break}}}while(0);n=ca+(la|8)|0;return n|0}}while(0);X=ea;Z=31744;while(1){za=c[Z>>2]|0;if(za>>>0<=X>>>0){Aa=c[Z+4>>2]|0;Ba=za+Aa|0;if(Ba>>>0>X>>>0){break}}Z=c[Z+8>>2]|0}Z=za+(Aa-39)|0;if((Z&7|0)==0){Ca=0}else{Ca=-Z&7}Z=za+(Aa-47+Ca)|0;da=Z>>>0<(ea+16|0)>>>0?X:Z;Z=da+8|0;$=ca+8|0;if(($&7|0)==0){Da=0}else{Da=-$&7}$=ba-40-Da|0;c[7830]=ca+Da;c[7827]=$;c[ca+(Da+4)>>2]=$|1;c[ca+(ba-36)>>2]=40;c[7831]=c[7792];c[da+4>>2]=27;c[Z>>2]=c[7936];c[Z+4>>2]=c[7937];c[Z+8>>2]=c[7938];c[Z+12>>2]=c[7939];c[7936]=ca;c[7937]=ba;c[7939]=0;c[7938]=Z;Z=da+28|0;c[Z>>2]=7;if((da+32|0)>>>0<Ba>>>0){$=Z;while(1){Z=$+4|0;c[Z>>2]=7;if(($+8|0)>>>0<Ba>>>0){$=Z}else{break}}}if((da|0)==(X|0)){break}$=da-ea|0;Z=X+($+4)|0;c[Z>>2]=c[Z>>2]&-2;c[ea+4>>2]=$|1;c[X+$>>2]=$;Z=$>>>3;if($>>>0<256>>>0){K=Z<<1;_=31336+(K<<2)|0;S=c[7824]|0;j=1<<Z;do{if((S&j|0)==0){c[7824]=S|j;Ea=_;Fa=31336+(K+2<<2)|0}else{Z=31336+(K+2<<2)|0;Y=c[Z>>2]|0;if(Y>>>0>=(c[7828]|0)>>>0){Ea=Y;Fa=Z;break}Bc();return 0}}while(0);c[Fa>>2]=ea;c[Ea+12>>2]=ea;c[ea+8>>2]=Ea;c[ea+12>>2]=_;break}K=ea;j=$>>>8;do{if((j|0)==0){Ga=0}else{if($>>>0>16777215>>>0){Ga=31;break}S=(j+1048320|0)>>>16&8;X=j<<S;da=(X+520192|0)>>>16&4;Z=X<<da;X=(Z+245760|0)>>>16&2;Y=14-(da|S|X)+(Z<<X>>>15)|0;Ga=$>>>((Y+7|0)>>>0)&1|Y<<1}}while(0);j=31600+(Ga<<2)|0;c[ea+28>>2]=Ga;c[ea+20>>2]=0;c[ea+16>>2]=0;_=c[7825]|0;Y=1<<Ga;if((_&Y|0)==0){c[7825]=_|Y;c[j>>2]=K;c[ea+24>>2]=j;c[ea+12>>2]=ea;c[ea+8>>2]=ea;break}Y=c[j>>2]|0;if((Ga|0)==31){Ha=0}else{Ha=25-(Ga>>>1)|0}L499:do{if((c[Y+4>>2]&-8|0)==($|0)){Ia=Y}else{j=Y;_=$<<Ha;while(1){Ja=j+16+(_>>>31<<2)|0;X=c[Ja>>2]|0;if((X|0)==0){break}if((c[X+4>>2]&-8|0)==($|0)){Ia=X;break L499}else{j=X;_=_<<1}}if(Ja>>>0<(c[7828]|0)>>>0){Bc();return 0}else{c[Ja>>2]=K;c[ea+24>>2]=j;c[ea+12>>2]=ea;c[ea+8>>2]=ea;break L311}}}while(0);$=Ia+8|0;Y=c[$>>2]|0;_=c[7828]|0;if(Ia>>>0<_>>>0){Bc();return 0}if(Y>>>0<_>>>0){Bc();return 0}else{c[Y+12>>2]=K;c[$>>2]=K;c[ea+8>>2]=Y;c[ea+12>>2]=Ia;c[ea+24>>2]=0;break}}}while(0);ea=c[7827]|0;if(ea>>>0<=o>>>0){break}Y=ea-o|0;c[7827]=Y;ea=c[7830]|0;$=ea;c[7830]=$+o;c[$+(o+4)>>2]=Y|1;c[ea+4>>2]=o|3;n=ea+8|0;return n|0}}while(0);c[(kc()|0)>>2]=12;n=0;return n|0}function Mn(a){a=a|0;var b=0,d=0,e=0,f=0,g=0,h=0,i=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0;if((a|0)==0){return}b=a-8|0;d=b;e=c[7828]|0;if(b>>>0<e>>>0){Bc()}f=c[a-4>>2]|0;g=f&3;if((g|0)==1){Bc()}h=f&-8;i=a+(h-8)|0;j=i;L10:do{if((f&1|0)==0){k=c[b>>2]|0;if((g|0)==0){return}l=-8-k|0;m=a+l|0;n=m;o=k+h|0;if(m>>>0<e>>>0){Bc()}if((n|0)==(c[7829]|0)){p=a+(h-4)|0;if((c[p>>2]&3|0)!=3){q=n;r=o;break}c[7826]=o;c[p>>2]=c[p>>2]&-2;c[a+(l+4)>>2]=o|1;c[i>>2]=o;return}p=k>>>3;if(k>>>0<256>>>0){k=c[a+(l+8)>>2]|0;s=c[a+(l+12)>>2]|0;t=31336+(p<<1<<2)|0;do{if((k|0)!=(t|0)){if(k>>>0<e>>>0){Bc()}if((c[k+12>>2]|0)==(n|0)){break}Bc()}}while(0);if((s|0)==(k|0)){c[7824]=c[7824]&~(1<<p);q=n;r=o;break}do{if((s|0)==(t|0)){u=s+8|0}else{if(s>>>0<e>>>0){Bc()}v=s+8|0;if((c[v>>2]|0)==(n|0)){u=v;break}Bc()}}while(0);c[k+12>>2]=s;c[u>>2]=k;q=n;r=o;break}t=m;p=c[a+(l+24)>>2]|0;v=c[a+(l+12)>>2]|0;do{if((v|0)==(t|0)){w=a+(l+20)|0;x=c[w>>2]|0;if((x|0)==0){y=a+(l+16)|0;z=c[y>>2]|0;if((z|0)==0){A=0;break}else{B=z;C=y}}else{B=x;C=w}while(1){w=B+20|0;x=c[w>>2]|0;if((x|0)!=0){B=x;C=w;continue}w=B+16|0;x=c[w>>2]|0;if((x|0)==0){break}else{B=x;C=w}}if(C>>>0<e>>>0){Bc()}else{c[C>>2]=0;A=B;break}}else{w=c[a+(l+8)>>2]|0;if(w>>>0<e>>>0){Bc()}x=w+12|0;if((c[x>>2]|0)!=(t|0)){Bc()}y=v+8|0;if((c[y>>2]|0)==(t|0)){c[x>>2]=v;c[y>>2]=w;A=v;break}else{Bc()}}}while(0);if((p|0)==0){q=n;r=o;break}v=a+(l+28)|0;m=31600+(c[v>>2]<<2)|0;do{if((t|0)==(c[m>>2]|0)){c[m>>2]=A;if((A|0)!=0){break}c[7825]=c[7825]&~(1<<c[v>>2]);q=n;r=o;break L10}else{if(p>>>0<(c[7828]|0)>>>0){Bc()}k=p+16|0;if((c[k>>2]|0)==(t|0)){c[k>>2]=A}else{c[p+20>>2]=A}if((A|0)==0){q=n;r=o;break L10}}}while(0);if(A>>>0<(c[7828]|0)>>>0){Bc()}c[A+24>>2]=p;t=c[a+(l+16)>>2]|0;do{if((t|0)!=0){if(t>>>0<(c[7828]|0)>>>0){Bc()}else{c[A+16>>2]=t;c[t+24>>2]=A;break}}}while(0);t=c[a+(l+20)>>2]|0;if((t|0)==0){q=n;r=o;break}if(t>>>0<(c[7828]|0)>>>0){Bc()}else{c[A+20>>2]=t;c[t+24>>2]=A;q=n;r=o;break}}else{q=d;r=h}}while(0);d=q;if(d>>>0>=i>>>0){Bc()}A=a+(h-4)|0;e=c[A>>2]|0;if((e&1|0)==0){Bc()}do{if((e&2|0)==0){if((j|0)==(c[7830]|0)){B=(c[7827]|0)+r|0;c[7827]=B;c[7830]=q;c[q+4>>2]=B|1;if((q|0)!=(c[7829]|0)){return}c[7829]=0;c[7826]=0;return}if((j|0)==(c[7829]|0)){B=(c[7826]|0)+r|0;c[7826]=B;c[7829]=q;c[q+4>>2]=B|1;c[d+B>>2]=B;return}B=(e&-8)+r|0;C=e>>>3;L112:do{if(e>>>0<256>>>0){u=c[a+h>>2]|0;g=c[a+(h|4)>>2]|0;b=31336+(C<<1<<2)|0;do{if((u|0)!=(b|0)){if(u>>>0<(c[7828]|0)>>>0){Bc()}if((c[u+12>>2]|0)==(j|0)){break}Bc()}}while(0);if((g|0)==(u|0)){c[7824]=c[7824]&~(1<<C);break}do{if((g|0)==(b|0)){D=g+8|0}else{if(g>>>0<(c[7828]|0)>>>0){Bc()}f=g+8|0;if((c[f>>2]|0)==(j|0)){D=f;break}Bc()}}while(0);c[u+12>>2]=g;c[D>>2]=u}else{b=i;f=c[a+(h+16)>>2]|0;t=c[a+(h|4)>>2]|0;do{if((t|0)==(b|0)){p=a+(h+12)|0;v=c[p>>2]|0;if((v|0)==0){m=a+(h+8)|0;k=c[m>>2]|0;if((k|0)==0){E=0;break}else{F=k;G=m}}else{F=v;G=p}while(1){p=F+20|0;v=c[p>>2]|0;if((v|0)!=0){F=v;G=p;continue}p=F+16|0;v=c[p>>2]|0;if((v|0)==0){break}else{F=v;G=p}}if(G>>>0<(c[7828]|0)>>>0){Bc()}else{c[G>>2]=0;E=F;break}}else{p=c[a+h>>2]|0;if(p>>>0<(c[7828]|0)>>>0){Bc()}v=p+12|0;if((c[v>>2]|0)!=(b|0)){Bc()}m=t+8|0;if((c[m>>2]|0)==(b|0)){c[v>>2]=t;c[m>>2]=p;E=t;break}else{Bc()}}}while(0);if((f|0)==0){break}t=a+(h+20)|0;u=31600+(c[t>>2]<<2)|0;do{if((b|0)==(c[u>>2]|0)){c[u>>2]=E;if((E|0)!=0){break}c[7825]=c[7825]&~(1<<c[t>>2]);break L112}else{if(f>>>0<(c[7828]|0)>>>0){Bc()}g=f+16|0;if((c[g>>2]|0)==(b|0)){c[g>>2]=E}else{c[f+20>>2]=E}if((E|0)==0){break L112}}}while(0);if(E>>>0<(c[7828]|0)>>>0){Bc()}c[E+24>>2]=f;b=c[a+(h+8)>>2]|0;do{if((b|0)!=0){if(b>>>0<(c[7828]|0)>>>0){Bc()}else{c[E+16>>2]=b;c[b+24>>2]=E;break}}}while(0);b=c[a+(h+12)>>2]|0;if((b|0)==0){break}if(b>>>0<(c[7828]|0)>>>0){Bc()}else{c[E+20>>2]=b;c[b+24>>2]=E;break}}}while(0);c[q+4>>2]=B|1;c[d+B>>2]=B;if((q|0)!=(c[7829]|0)){H=B;break}c[7826]=B;return}else{c[A>>2]=e&-2;c[q+4>>2]=r|1;c[d+r>>2]=r;H=r}}while(0);r=H>>>3;if(H>>>0<256>>>0){d=r<<1;e=31336+(d<<2)|0;A=c[7824]|0;E=1<<r;do{if((A&E|0)==0){c[7824]=A|E;I=e;J=31336+(d+2<<2)|0}else{r=31336+(d+2<<2)|0;h=c[r>>2]|0;if(h>>>0>=(c[7828]|0)>>>0){I=h;J=r;break}Bc()}}while(0);c[J>>2]=q;c[I+12>>2]=q;c[q+8>>2]=I;c[q+12>>2]=e;return}e=q;I=H>>>8;do{if((I|0)==0){K=0}else{if(H>>>0>16777215>>>0){K=31;break}J=(I+1048320|0)>>>16&8;d=I<<J;E=(d+520192|0)>>>16&4;A=d<<E;d=(A+245760|0)>>>16&2;r=14-(E|J|d)+(A<<d>>>15)|0;K=H>>>((r+7|0)>>>0)&1|r<<1}}while(0);I=31600+(K<<2)|0;c[q+28>>2]=K;c[q+20>>2]=0;c[q+16>>2]=0;r=c[7825]|0;d=1<<K;L199:do{if((r&d|0)==0){c[7825]=r|d;c[I>>2]=e;c[q+24>>2]=I;c[q+12>>2]=q;c[q+8>>2]=q}else{A=c[I>>2]|0;if((K|0)==31){L=0}else{L=25-(K>>>1)|0}L204:do{if((c[A+4>>2]&-8|0)==(H|0)){M=A}else{J=A;E=H<<L;while(1){N=J+16+(E>>>31<<2)|0;h=c[N>>2]|0;if((h|0)==0){break}if((c[h+4>>2]&-8|0)==(H|0)){M=h;break L204}else{J=h;E=E<<1}}if(N>>>0<(c[7828]|0)>>>0){Bc()}else{c[N>>2]=e;c[q+24>>2]=J;c[q+12>>2]=q;c[q+8>>2]=q;break L199}}}while(0);A=M+8|0;B=c[A>>2]|0;E=c[7828]|0;if(M>>>0<E>>>0){Bc()}if(B>>>0<E>>>0){Bc()}else{c[B+12>>2]=e;c[A>>2]=e;c[q+8>>2]=B;c[q+12>>2]=M;c[q+24>>2]=0;break}}}while(0);q=(c[7832]|0)-1|0;c[7832]=q;if((q|0)==0){O=31752}else{return}while(1){q=c[O>>2]|0;if((q|0)==0){break}else{O=q+8|0}}c[7832]=-1;return}function Nn(a,b){a=a|0;b=b|0;var d=0,e=0;do{if((a|0)==0){d=0}else{e=ga(b,a)|0;if((b|a)>>>0<=65535>>>0){d=e;break}d=((e>>>0)/(a>>>0)|0|0)==(b|0)?e:-1}}while(0);b=Ln(d)|0;if((b|0)==0){return b|0}if((c[b-4>>2]&3|0)==0){return b|0}ko(b|0,0,d|0)|0;return b|0}function On(a,b){a=a|0;b=b|0;var d=0,e=0,f=0,g=0;if((a|0)==0){d=Ln(b)|0;return d|0}if(b>>>0>4294967231>>>0){c[(kc()|0)>>2]=12;d=0;return d|0}if(b>>>0<11>>>0){e=16}else{e=b+11&-8}f=Pn(a-8|0,e)|0;if((f|0)!=0){d=f+8|0;return d|0}f=Ln(b)|0;if((f|0)==0){d=0;return d|0}e=c[a-4>>2]|0;g=(e&-8)-((e&3|0)==0?8:4)|0;io(f|0,a|0,g>>>0<b>>>0?g:b)|0;Mn(a);d=f;return d|0}function Pn(a,b){a=a|0;b=b|0;var d=0,e=0,f=0,g=0,h=0,i=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0;d=a+4|0;e=c[d>>2]|0;f=e&-8;g=a;h=g+f|0;i=h;j=c[7828]|0;if(g>>>0<j>>>0){Bc();return 0}k=e&3;if(!((k|0)!=1&g>>>0<h>>>0)){Bc();return 0}l=g+(f|4)|0;m=c[l>>2]|0;if((m&1|0)==0){Bc();return 0}if((k|0)==0){if(b>>>0<256>>>0){n=0;return n|0}do{if(f>>>0>=(b+4|0)>>>0){if((f-b|0)>>>0>c[7790]<<1>>>0){break}else{n=a}return n|0}}while(0);n=0;return n|0}if(f>>>0>=b>>>0){k=f-b|0;if(k>>>0<=15>>>0){n=a;return n|0}c[d>>2]=e&1|b|2;c[g+(b+4)>>2]=k|3;c[l>>2]=c[l>>2]|1;Qn(g+b|0,k);n=a;return n|0}if((i|0)==(c[7830]|0)){k=(c[7827]|0)+f|0;if(k>>>0<=b>>>0){n=0;return n|0}l=k-b|0;c[d>>2]=e&1|b|2;c[g+(b+4)>>2]=l|1;c[7830]=g+b;c[7827]=l;n=a;return n|0}if((i|0)==(c[7829]|0)){l=(c[7826]|0)+f|0;if(l>>>0<b>>>0){n=0;return n|0}k=l-b|0;if(k>>>0>15>>>0){c[d>>2]=e&1|b|2;c[g+(b+4)>>2]=k|1;c[g+l>>2]=k;o=g+(l+4)|0;c[o>>2]=c[o>>2]&-2;p=g+b|0;q=k}else{c[d>>2]=e&1|l|2;e=g+(l+4)|0;c[e>>2]=c[e>>2]|1;p=0;q=0}c[7826]=q;c[7829]=p;n=a;return n|0}if((m&2|0)!=0){n=0;return n|0}p=(m&-8)+f|0;if(p>>>0<b>>>0){n=0;return n|0}q=p-b|0;e=m>>>3;L52:do{if(m>>>0<256>>>0){l=c[g+(f+8)>>2]|0;k=c[g+(f+12)>>2]|0;o=31336+(e<<1<<2)|0;do{if((l|0)!=(o|0)){if(l>>>0<j>>>0){Bc();return 0}if((c[l+12>>2]|0)==(i|0)){break}Bc();return 0}}while(0);if((k|0)==(l|0)){c[7824]=c[7824]&~(1<<e);break}do{if((k|0)==(o|0)){r=k+8|0}else{if(k>>>0<j>>>0){Bc();return 0}s=k+8|0;if((c[s>>2]|0)==(i|0)){r=s;break}Bc();return 0}}while(0);c[l+12>>2]=k;c[r>>2]=l}else{o=h;s=c[g+(f+24)>>2]|0;t=c[g+(f+12)>>2]|0;do{if((t|0)==(o|0)){u=g+(f+20)|0;v=c[u>>2]|0;if((v|0)==0){w=g+(f+16)|0;x=c[w>>2]|0;if((x|0)==0){y=0;break}else{z=x;A=w}}else{z=v;A=u}while(1){u=z+20|0;v=c[u>>2]|0;if((v|0)!=0){z=v;A=u;continue}u=z+16|0;v=c[u>>2]|0;if((v|0)==0){break}else{z=v;A=u}}if(A>>>0<j>>>0){Bc();return 0}else{c[A>>2]=0;y=z;break}}else{u=c[g+(f+8)>>2]|0;if(u>>>0<j>>>0){Bc();return 0}v=u+12|0;if((c[v>>2]|0)!=(o|0)){Bc();return 0}w=t+8|0;if((c[w>>2]|0)==(o|0)){c[v>>2]=t;c[w>>2]=u;y=t;break}else{Bc();return 0}}}while(0);if((s|0)==0){break}t=g+(f+28)|0;l=31600+(c[t>>2]<<2)|0;do{if((o|0)==(c[l>>2]|0)){c[l>>2]=y;if((y|0)!=0){break}c[7825]=c[7825]&~(1<<c[t>>2]);break L52}else{if(s>>>0<(c[7828]|0)>>>0){Bc();return 0}k=s+16|0;if((c[k>>2]|0)==(o|0)){c[k>>2]=y}else{c[s+20>>2]=y}if((y|0)==0){break L52}}}while(0);if(y>>>0<(c[7828]|0)>>>0){Bc();return 0}c[y+24>>2]=s;o=c[g+(f+16)>>2]|0;do{if((o|0)!=0){if(o>>>0<(c[7828]|0)>>>0){Bc();return 0}else{c[y+16>>2]=o;c[o+24>>2]=y;break}}}while(0);o=c[g+(f+20)>>2]|0;if((o|0)==0){break}if(o>>>0<(c[7828]|0)>>>0){Bc();return 0}else{c[y+20>>2]=o;c[o+24>>2]=y;break}}}while(0);if(q>>>0<16>>>0){c[d>>2]=p|c[d>>2]&1|2;y=g+(p|4)|0;c[y>>2]=c[y>>2]|1;n=a;return n|0}else{c[d>>2]=c[d>>2]&1|b|2;c[g+(b+4)>>2]=q|3;d=g+(p|4)|0;c[d>>2]=c[d>>2]|1;Qn(g+b|0,q);n=a;return n|0}return 0}function Qn(a,b){a=a|0;b=b|0;var d=0,e=0,f=0,g=0,h=0,i=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0;d=a;e=d+b|0;f=e;g=c[a+4>>2]|0;L1:do{if((g&1|0)==0){h=c[a>>2]|0;if((g&3|0)==0){return}i=d+(-h|0)|0;j=i;k=h+b|0;l=c[7828]|0;if(i>>>0<l>>>0){Bc()}if((j|0)==(c[7829]|0)){m=d+(b+4)|0;if((c[m>>2]&3|0)!=3){n=j;o=k;break}c[7826]=k;c[m>>2]=c[m>>2]&-2;c[d+(4-h)>>2]=k|1;c[e>>2]=k;return}m=h>>>3;if(h>>>0<256>>>0){p=c[d+(8-h)>>2]|0;q=c[d+(12-h)>>2]|0;r=31336+(m<<1<<2)|0;do{if((p|0)!=(r|0)){if(p>>>0<l>>>0){Bc()}if((c[p+12>>2]|0)==(j|0)){break}Bc()}}while(0);if((q|0)==(p|0)){c[7824]=c[7824]&~(1<<m);n=j;o=k;break}do{if((q|0)==(r|0)){s=q+8|0}else{if(q>>>0<l>>>0){Bc()}t=q+8|0;if((c[t>>2]|0)==(j|0)){s=t;break}Bc()}}while(0);c[p+12>>2]=q;c[s>>2]=p;n=j;o=k;break}r=i;m=c[d+(24-h)>>2]|0;t=c[d+(12-h)>>2]|0;do{if((t|0)==(r|0)){u=16-h|0;v=d+(u+4)|0;w=c[v>>2]|0;if((w|0)==0){x=d+u|0;u=c[x>>2]|0;if((u|0)==0){y=0;break}else{z=u;A=x}}else{z=w;A=v}while(1){v=z+20|0;w=c[v>>2]|0;if((w|0)!=0){z=w;A=v;continue}v=z+16|0;w=c[v>>2]|0;if((w|0)==0){break}else{z=w;A=v}}if(A>>>0<l>>>0){Bc()}else{c[A>>2]=0;y=z;break}}else{v=c[d+(8-h)>>2]|0;if(v>>>0<l>>>0){Bc()}w=v+12|0;if((c[w>>2]|0)!=(r|0)){Bc()}x=t+8|0;if((c[x>>2]|0)==(r|0)){c[w>>2]=t;c[x>>2]=v;y=t;break}else{Bc()}}}while(0);if((m|0)==0){n=j;o=k;break}t=d+(28-h)|0;l=31600+(c[t>>2]<<2)|0;do{if((r|0)==(c[l>>2]|0)){c[l>>2]=y;if((y|0)!=0){break}c[7825]=c[7825]&~(1<<c[t>>2]);n=j;o=k;break L1}else{if(m>>>0<(c[7828]|0)>>>0){Bc()}i=m+16|0;if((c[i>>2]|0)==(r|0)){c[i>>2]=y}else{c[m+20>>2]=y}if((y|0)==0){n=j;o=k;break L1}}}while(0);if(y>>>0<(c[7828]|0)>>>0){Bc()}c[y+24>>2]=m;r=16-h|0;t=c[d+r>>2]|0;do{if((t|0)!=0){if(t>>>0<(c[7828]|0)>>>0){Bc()}else{c[y+16>>2]=t;c[t+24>>2]=y;break}}}while(0);t=c[d+(r+4)>>2]|0;if((t|0)==0){n=j;o=k;break}if(t>>>0<(c[7828]|0)>>>0){Bc()}else{c[y+20>>2]=t;c[t+24>>2]=y;n=j;o=k;break}}else{n=a;o=b}}while(0);a=c[7828]|0;if(e>>>0<a>>>0){Bc()}y=d+(b+4)|0;z=c[y>>2]|0;do{if((z&2|0)==0){if((f|0)==(c[7830]|0)){A=(c[7827]|0)+o|0;c[7827]=A;c[7830]=n;c[n+4>>2]=A|1;if((n|0)!=(c[7829]|0)){return}c[7829]=0;c[7826]=0;return}if((f|0)==(c[7829]|0)){A=(c[7826]|0)+o|0;c[7826]=A;c[7829]=n;c[n+4>>2]=A|1;c[n+A>>2]=A;return}A=(z&-8)+o|0;s=z>>>3;L100:do{if(z>>>0<256>>>0){g=c[d+(b+8)>>2]|0;t=c[d+(b+12)>>2]|0;h=31336+(s<<1<<2)|0;do{if((g|0)!=(h|0)){if(g>>>0<a>>>0){Bc()}if((c[g+12>>2]|0)==(f|0)){break}Bc()}}while(0);if((t|0)==(g|0)){c[7824]=c[7824]&~(1<<s);break}do{if((t|0)==(h|0)){B=t+8|0}else{if(t>>>0<a>>>0){Bc()}m=t+8|0;if((c[m>>2]|0)==(f|0)){B=m;break}Bc()}}while(0);c[g+12>>2]=t;c[B>>2]=g}else{h=e;m=c[d+(b+24)>>2]|0;l=c[d+(b+12)>>2]|0;do{if((l|0)==(h|0)){i=d+(b+20)|0;p=c[i>>2]|0;if((p|0)==0){q=d+(b+16)|0;v=c[q>>2]|0;if((v|0)==0){C=0;break}else{D=v;E=q}}else{D=p;E=i}while(1){i=D+20|0;p=c[i>>2]|0;if((p|0)!=0){D=p;E=i;continue}i=D+16|0;p=c[i>>2]|0;if((p|0)==0){break}else{D=p;E=i}}if(E>>>0<a>>>0){Bc()}else{c[E>>2]=0;C=D;break}}else{i=c[d+(b+8)>>2]|0;if(i>>>0<a>>>0){Bc()}p=i+12|0;if((c[p>>2]|0)!=(h|0)){Bc()}q=l+8|0;if((c[q>>2]|0)==(h|0)){c[p>>2]=l;c[q>>2]=i;C=l;break}else{Bc()}}}while(0);if((m|0)==0){break}l=d+(b+28)|0;g=31600+(c[l>>2]<<2)|0;do{if((h|0)==(c[g>>2]|0)){c[g>>2]=C;if((C|0)!=0){break}c[7825]=c[7825]&~(1<<c[l>>2]);break L100}else{if(m>>>0<(c[7828]|0)>>>0){Bc()}t=m+16|0;if((c[t>>2]|0)==(h|0)){c[t>>2]=C}else{c[m+20>>2]=C}if((C|0)==0){break L100}}}while(0);if(C>>>0<(c[7828]|0)>>>0){Bc()}c[C+24>>2]=m;h=c[d+(b+16)>>2]|0;do{if((h|0)!=0){if(h>>>0<(c[7828]|0)>>>0){Bc()}else{c[C+16>>2]=h;c[h+24>>2]=C;break}}}while(0);h=c[d+(b+20)>>2]|0;if((h|0)==0){break}if(h>>>0<(c[7828]|0)>>>0){Bc()}else{c[C+20>>2]=h;c[h+24>>2]=C;break}}}while(0);c[n+4>>2]=A|1;c[n+A>>2]=A;if((n|0)!=(c[7829]|0)){F=A;break}c[7826]=A;return}else{c[y>>2]=z&-2;c[n+4>>2]=o|1;c[n+o>>2]=o;F=o}}while(0);o=F>>>3;if(F>>>0<256>>>0){z=o<<1;y=31336+(z<<2)|0;C=c[7824]|0;b=1<<o;do{if((C&b|0)==0){c[7824]=C|b;G=y;H=31336+(z+2<<2)|0}else{o=31336+(z+2<<2)|0;d=c[o>>2]|0;if(d>>>0>=(c[7828]|0)>>>0){G=d;H=o;break}Bc()}}while(0);c[H>>2]=n;c[G+12>>2]=n;c[n+8>>2]=G;c[n+12>>2]=y;return}y=n;G=F>>>8;do{if((G|0)==0){I=0}else{if(F>>>0>16777215>>>0){I=31;break}H=(G+1048320|0)>>>16&8;z=G<<H;b=(z+520192|0)>>>16&4;C=z<<b;z=(C+245760|0)>>>16&2;o=14-(b|H|z)+(C<<z>>>15)|0;I=F>>>((o+7|0)>>>0)&1|o<<1}}while(0);G=31600+(I<<2)|0;c[n+28>>2]=I;c[n+20>>2]=0;c[n+16>>2]=0;o=c[7825]|0;z=1<<I;if((o&z|0)==0){c[7825]=o|z;c[G>>2]=y;c[n+24>>2]=G;c[n+12>>2]=n;c[n+8>>2]=n;return}z=c[G>>2]|0;if((I|0)==31){J=0}else{J=25-(I>>>1)|0}L194:do{if((c[z+4>>2]&-8|0)==(F|0)){K=z}else{I=z;G=F<<J;while(1){L=I+16+(G>>>31<<2)|0;o=c[L>>2]|0;if((o|0)==0){break}if((c[o+4>>2]&-8|0)==(F|0)){K=o;break L194}else{I=o;G=G<<1}}if(L>>>0<(c[7828]|0)>>>0){Bc()}c[L>>2]=y;c[n+24>>2]=I;c[n+12>>2]=n;c[n+8>>2]=n;return}}while(0);L=K+8|0;F=c[L>>2]|0;J=c[7828]|0;if(K>>>0<J>>>0){Bc()}if(F>>>0<J>>>0){Bc()}c[F+12>>2]=y;c[L>>2]=y;c[n+8>>2]=F;c[n+12>>2]=K;c[n+24>>2]=0;return}function Rn(a){a=a|0;var b=0,d=0,e=0;b=(a|0)==0?1:a;while(1){d=Ln(b)|0;if((d|0)!=0){e=10;break}a=(I=c[9098]|0,c[9098]=I+0,I);if((a|0)==0){break}hd[a&3]()}if((e|0)==10){return d|0}d=Nc(4)|0;c[d>>2]=4472;Qb(d|0,10176,34);return 0}function Sn(a){a=a|0;return Rn(a)|0}function Tn(a){a=a|0;if((a|0)==0){return}Mn(a);return}function Un(a){a=a|0;Tn(a);return}function Vn(a){a=a|0;Tn(a);return}function Wn(a){a=a|0;return}function Xn(a){a=a|0;return 2800}function Yn(){var a=0;a=Nc(4)|0;c[a>>2]=4472;Qb(a|0,10176,34)}function Zn(b,e,f){b=b|0;e=e|0;f=f|0;var g=0,h=0,j=0,k=0,l=0.0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0.0,T=0.0,U=0,W=0,X=0,Y=0,Z=0,_=0,$=0,aa=0,ba=0,ca=0,da=0,ea=0,fa=0,ha=0,ia=0.0,ja=0.0,ka=0,la=0,ma=0.0,na=0.0,oa=0,pa=0.0,qa=0,ra=0,sa=0,ta=0,ua=0,va=0,wa=0,xa=0,ya=0.0,za=0,Aa=0.0,Ba=0,Ca=0.0,Da=0,Ea=0,Fa=0,Ga=0.0,Ha=0,Ia=0.0,Ja=0.0,Ka=0,La=0,Ma=0,Na=0,Oa=0,Pa=0,Ra=0,Sa=0,Ta=0,Ua=0,Va=0,Wa=0,Xa=0,Ya=0,Za=0,_a=0,$a=0,ab=0,bb=0,cb=0,db=0,eb=0,fb=0,gb=0,hb=0,ib=0,jb=0,kb=0,lb=0,mb=0,nb=0,ob=0,pb=0,qb=0,sb=0,tb=0,ub=0,vb=0,wb=0,xb=0,yb=0,zb=0,Ab=0,Bb=0,Cb=0,Db=0,Eb=0,Fb=0,Gb=0,Ib=0,Jb=0,Kb=0,Lb=0,Mb=0,Nb=0,Ob=0,Pb=0,Qb=0,Rb=0,Sb=0,Tb=0,Ub=0,Vb=0,Wb=0,Xb=0,Yb=0,Zb=0,_b=0,$b=0,ac=0,bc=0,cc=0,dc=0,ec=0,fc=0,gc=0,hc=0,ic=0,jc=0,lc=0,mc=0,nc=0,oc=0,pc=0,qc=0,rc=0,sc=0,tc=0,uc=0,vc=0,wc=0,xc=0,yc=0,zc=0,Ac=0.0,Bc=0,Cc=0,Dc=0.0,Ec=0.0,Fc=0.0,Gc=0.0,Hc=0.0,Ic=0.0,Jc=0.0,Kc=0,Lc=0,Mc=0.0,Nc=0,Oc=0;g=i;i=i+512|0;h=g|0;if((e|0)==1){j=-1074;k=53}else if((e|0)==2){j=-1074;k=53}else if((e|0)==0){j=-149;k=24}else{l=0.0;i=g;return+l}e=b+4|0;m=b+100|0;do{n=c[e>>2]|0;if(n>>>0<(c[m>>2]|0)>>>0){c[e>>2]=n+1;o=d[n]|0}else{o=ao(b)|0}}while((Qa(o|0)|0)!=0);do{if((o|0)==45|(o|0)==43){n=1-(((o|0)==45)<<1)|0;p=c[e>>2]|0;if(p>>>0<(c[m>>2]|0)>>>0){c[e>>2]=p+1;q=d[p]|0;r=n;break}else{q=ao(b)|0;r=n;break}}else{q=o;r=1}}while(0);o=0;n=q;while(1){if((n|32|0)!=(a[1280+o|0]|0)){s=o;t=n;break}do{if(o>>>0<7>>>0){q=c[e>>2]|0;if(q>>>0<(c[m>>2]|0)>>>0){c[e>>2]=q+1;u=d[q]|0;break}else{u=ao(b)|0;break}}else{u=n}}while(0);q=o+1|0;if(q>>>0<8>>>0){o=q;n=u}else{s=q;t=u;break}}do{if((s|0)==3){x=23}else if((s|0)!=8){u=(f|0)==0;if(!(s>>>0<4>>>0|u)){if((s|0)==8){break}else{x=23;break}}L34:do{if((s|0)==0){n=0;o=t;while(1){if((o|32|0)!=(a[3216+n|0]|0)){y=o;z=n;break L34}do{if(n>>>0<2>>>0){q=c[e>>2]|0;if(q>>>0<(c[m>>2]|0)>>>0){c[e>>2]=q+1;A=d[q]|0;break}else{A=ao(b)|0;break}}else{A=o}}while(0);q=n+1|0;if(q>>>0<3>>>0){n=q;o=A}else{y=A;z=q;break}}}else{y=t;z=s}}while(0);if((z|0)==3){o=c[e>>2]|0;if(o>>>0<(c[m>>2]|0)>>>0){c[e>>2]=o+1;B=d[o]|0}else{B=ao(b)|0}if((B|0)==40){C=1}else{if((c[m>>2]|0)==0){l=+v;i=g;return+l}c[e>>2]=(c[e>>2]|0)-1;l=+v;i=g;return+l}while(1){o=c[e>>2]|0;if(o>>>0<(c[m>>2]|0)>>>0){c[e>>2]=o+1;D=d[o]|0}else{D=ao(b)|0}if(!((D-48|0)>>>0<10>>>0|(D-65|0)>>>0<26>>>0)){if(!((D-97|0)>>>0<26>>>0|(D|0)==95)){break}}C=C+1|0}if((D|0)==41){l=+v;i=g;return+l}o=(c[m>>2]|0)==0;if(!o){c[e>>2]=(c[e>>2]|0)-1}if(u){c[(kc()|0)>>2]=22;$n(b,0);l=0.0;i=g;return+l}if((C|0)==0|o){l=+v;i=g;return+l}else{E=C}while(1){o=E-1|0;c[e>>2]=(c[e>>2]|0)-1;if((o|0)==0){l=+v;break}else{E=o}}i=g;return+l}else if((z|0)==0){do{if((y|0)==48){o=c[e>>2]|0;if(o>>>0<(c[m>>2]|0)>>>0){c[e>>2]=o+1;F=d[o]|0}else{F=ao(b)|0}if((F|32|0)!=120){if((c[m>>2]|0)==0){G=48;break}c[e>>2]=(c[e>>2]|0)-1;G=48;break}o=c[e>>2]|0;if(o>>>0<(c[m>>2]|0)>>>0){c[e>>2]=o+1;H=d[o]|0;I=0}else{H=ao(b)|0;I=0}while(1){if((H|0)==46){x=70;break}else if((H|0)!=48){J=H;L=0;M=0;N=0;O=0;P=I;Q=0;R=0;S=1.0;T=0.0;U=0;break}o=c[e>>2]|0;if(o>>>0<(c[m>>2]|0)>>>0){c[e>>2]=o+1;H=d[o]|0;I=1;continue}else{H=ao(b)|0;I=1;continue}}L107:do{if((x|0)==70){o=c[e>>2]|0;if(o>>>0<(c[m>>2]|0)>>>0){c[e>>2]=o+1;W=d[o]|0}else{W=ao(b)|0}if((W|0)==48){X=-1;Y=-1}else{J=W;L=0;M=0;N=0;O=0;P=I;Q=1;R=0;S=1.0;T=0.0;U=0;break}while(1){o=c[e>>2]|0;if(o>>>0<(c[m>>2]|0)>>>0){c[e>>2]=o+1;Z=d[o]|0}else{Z=ao(b)|0}if((Z|0)!=48){J=Z;L=0;M=0;N=X;O=Y;P=1;Q=1;R=0;S=1.0;T=0.0;U=0;break L107}o=oo(Y,X,-1,-1)|0;X=K;Y=o}}}while(0);L120:while(1){o=J-48|0;do{if(o>>>0<10>>>0){_=o;x=84}else{n=J|32;q=(J|0)==46;if(!((n-97|0)>>>0<6>>>0|q)){$=J;break L120}if(q){if((Q|0)==0){aa=L;ba=M;ca=L;da=M;ea=P;fa=1;ha=R;ia=S;ja=T;ka=U;break}else{$=46;break L120}}else{_=(J|0)>57?n-87|0:o;x=84;break}}}while(0);if((x|0)==84){x=0;o=0;do{if((L|0)<(o|0)|(L|0)==(o|0)&M>>>0<8>>>0){la=R;ma=S;na=T;oa=_+(U<<4)|0}else{n=0;if((L|0)<(n|0)|(L|0)==(n|0)&M>>>0<14>>>0){pa=S*.0625;la=R;ma=pa;na=T+pa*+(_|0);oa=U;break}if(!((_|0)!=0&(R|0)==0)){la=R;ma=S;na=T;oa=U;break}la=1;ma=S;na=T+S*.5;oa=U}}while(0);o=oo(M,L,1,0)|0;aa=K;ba=o;ca=N;da=O;ea=1;fa=Q;ha=la;ia=ma;ja=na;ka=oa}o=c[e>>2]|0;if(o>>>0<(c[m>>2]|0)>>>0){c[e>>2]=o+1;J=d[o]|0;L=aa;M=ba;N=ca;O=da;P=ea;Q=fa;R=ha;S=ia;T=ja;U=ka;continue}else{J=ao(b)|0;L=aa;M=ba;N=ca;O=da;P=ea;Q=fa;R=ha;S=ia;T=ja;U=ka;continue}}if((P|0)==0){o=(c[m>>2]|0)==0;if(!o){c[e>>2]=(c[e>>2]|0)-1}do{if(u){$n(b,0)}else{if(o){break}n=c[e>>2]|0;c[e>>2]=n-1;if((Q|0)==0){break}c[e>>2]=n-2}}while(0);l=+(r|0)*0.0;i=g;return+l}o=(Q|0)==0;n=o?M:O;q=o?L:N;o=0;if((L|0)<(o|0)|(L|0)==(o|0)&M>>>0<8>>>0){o=U;p=L;qa=M;while(1){ra=o<<4;sa=oo(qa,p,1,0)|0;ta=K;ua=0;if((ta|0)<(ua|0)|(ta|0)==(ua|0)&sa>>>0<8>>>0){o=ra;p=ta;qa=sa}else{va=ra;break}}}else{va=U}do{if(($|32|0)==112){qa=_n(b,f)|0;p=K;if(!((qa|0)==0&(p|0)==(-2147483648|0))){wa=p;xa=qa;break}if(u){$n(b,0);l=0.0;i=g;return+l}else{if((c[m>>2]|0)==0){wa=0;xa=0;break}c[e>>2]=(c[e>>2]|0)-1;wa=0;xa=0;break}}else{if((c[m>>2]|0)==0){wa=0;xa=0;break}c[e>>2]=(c[e>>2]|0)-1;wa=0;xa=0}}while(0);qa=oo(n<<2|0>>>30,q<<2|n>>>30,-32,-1)|0;p=oo(qa,K,xa,wa)|0;qa=K;if((va|0)==0){l=+(r|0)*0.0;i=g;return+l}o=0;if((qa|0)>(o|0)|(qa|0)==(o|0)&p>>>0>(-j|0)>>>0){c[(kc()|0)>>2]=34;l=+(r|0)*1.7976931348623157e+308*1.7976931348623157e+308;i=g;return+l}o=j-106|0;ra=(o|0)<0|0?-1:0;if((qa|0)<(ra|0)|(qa|0)==(ra|0)&p>>>0<o>>>0){c[(kc()|0)>>2]=34;l=+(r|0)*2.2250738585072014e-308*2.2250738585072014e-308;i=g;return+l}if((va|0)>-1){o=va;pa=T;ra=qa;sa=p;while(1){ta=o<<1;if(pa<.5){ya=pa;za=ta}else{ya=pa+-1.0;za=ta|1}Aa=pa+ya;ta=oo(sa,ra,-1,-1)|0;ua=K;if((za|0)>-1){o=za;pa=Aa;ra=ua;sa=ta}else{Ba=za;Ca=Aa;Da=ua;Ea=ta;break}}}else{Ba=va;Ca=T;Da=qa;Ea=p}sa=0;ra=po(32,0,j,(j|0)<0|0?-1:0)|0;o=oo(Ea,Da,ra,K)|0;ra=K;if((sa|0)>(ra|0)|(sa|0)==(ra|0)&k>>>0>o>>>0){ra=o;Fa=(ra|0)<0?0:ra}else{Fa=k}do{if((Fa|0)<53){pa=+(r|0);Aa=+Hb(+(+bo(1.0,84-Fa|0)),+pa);if(!((Fa|0)<32&Ca!=0.0)){Ga=Ca;Ha=Ba;Ia=Aa;Ja=pa;break}ra=Ba&1;Ga=(ra|0)==0?0.0:Ca;Ha=(ra^1)+Ba|0;Ia=Aa;Ja=pa}else{Ga=Ca;Ha=Ba;Ia=0.0;Ja=+(r|0)}}while(0);pa=Ja*Ga+(Ia+Ja*+(Ha>>>0>>>0))-Ia;if(pa==0.0){c[(kc()|0)>>2]=34}l=+co(pa,Ea);i=g;return+l}else{G=y}}while(0);p=j+k|0;qa=-p|0;ra=G;o=0;while(1){if((ra|0)==46){x=139;break}else if((ra|0)!=48){Ka=ra;La=0;Ma=o;Na=0;Oa=0;break}sa=c[e>>2]|0;if(sa>>>0<(c[m>>2]|0)>>>0){c[e>>2]=sa+1;ra=d[sa]|0;o=1;continue}else{ra=ao(b)|0;o=1;continue}}L209:do{if((x|0)==139){ra=c[e>>2]|0;if(ra>>>0<(c[m>>2]|0)>>>0){c[e>>2]=ra+1;Pa=d[ra]|0}else{Pa=ao(b)|0}if((Pa|0)==48){Ra=-1;Sa=-1}else{Ka=Pa;La=1;Ma=o;Na=0;Oa=0;break}while(1){ra=c[e>>2]|0;if(ra>>>0<(c[m>>2]|0)>>>0){c[e>>2]=ra+1;Ta=d[ra]|0}else{Ta=ao(b)|0}if((Ta|0)!=48){Ka=Ta;La=1;Ma=1;Na=Ra;Oa=Sa;break L209}ra=oo(Sa,Ra,-1,-1)|0;Ra=K;Sa=ra}}}while(0);o=h|0;c[o>>2]=0;ra=Ka-48|0;sa=(Ka|0)==46;L223:do{if(ra>>>0<10>>>0|sa){n=h+496|0;q=Na;ta=Oa;ua=0;Ua=0;Va=0;Wa=Ma;Xa=La;Ya=0;Za=0;_a=Ka;$a=ra;ab=sa;while(1){do{if(ab){if((Xa|0)==0){bb=Za;cb=Ya;db=1;eb=Wa;fb=Va;gb=ua;hb=Ua;ib=ua;jb=Ua}else{kb=q;lb=ta;mb=ua;nb=Ua;ob=Va;pb=Wa;qb=Ya;sb=Za;tb=_a;break L223}}else{ub=oo(Ua,ua,1,0)|0;vb=K;wb=(_a|0)!=48;if((Ya|0)>=125){if(!wb){bb=Za;cb=Ya;db=Xa;eb=Wa;fb=Va;gb=vb;hb=ub;ib=q;jb=ta;break}c[n>>2]=c[n>>2]|1;bb=Za;cb=Ya;db=Xa;eb=Wa;fb=Va;gb=vb;hb=ub;ib=q;jb=ta;break}xb=h+(Ya<<2)|0;if((Za|0)==0){yb=$a}else{yb=_a-48+((c[xb>>2]|0)*10|0)|0}c[xb>>2]=yb;xb=Za+1|0;zb=(xb|0)==9;bb=zb?0:xb;cb=(zb&1)+Ya|0;db=Xa;eb=1;fb=wb?ub:Va;gb=vb;hb=ub;ib=q;jb=ta}}while(0);ub=c[e>>2]|0;if(ub>>>0<(c[m>>2]|0)>>>0){c[e>>2]=ub+1;Ab=d[ub]|0}else{Ab=ao(b)|0}ub=Ab-48|0;vb=(Ab|0)==46;if(ub>>>0<10>>>0|vb){q=ib;ta=jb;ua=gb;Ua=hb;Va=fb;Wa=eb;Xa=db;Ya=cb;Za=bb;_a=Ab;$a=ub;ab=vb}else{Bb=ib;Cb=jb;Db=gb;Eb=hb;Fb=fb;Gb=eb;Ib=db;Jb=cb;Kb=bb;Lb=Ab;x=162;break}}}else{Bb=Na;Cb=Oa;Db=0;Eb=0;Fb=0;Gb=Ma;Ib=La;Jb=0;Kb=0;Lb=Ka;x=162}}while(0);if((x|0)==162){sa=(Ib|0)==0;kb=sa?Db:Bb;lb=sa?Eb:Cb;mb=Db;nb=Eb;ob=Fb;pb=Gb;qb=Jb;sb=Kb;tb=Lb}sa=(pb|0)!=0;do{if(sa){if((tb|32|0)!=101){x=171;break}ra=_n(b,f)|0;ab=K;do{if((ra|0)==0&(ab|0)==(-2147483648|0)){if(u){$n(b,0);l=0.0;i=g;return+l}else{if((c[m>>2]|0)==0){Mb=0;Nb=0;break}c[e>>2]=(c[e>>2]|0)-1;Mb=0;Nb=0;break}}else{Mb=ab;Nb=ra}}while(0);ra=oo(Nb,Mb,lb,kb)|0;Ob=K;Pb=ra}else{x=171}}while(0);do{if((x|0)==171){if((tb|0)<=-1){Ob=kb;Pb=lb;break}if((c[m>>2]|0)==0){Ob=kb;Pb=lb;break}c[e>>2]=(c[e>>2]|0)-1;Ob=kb;Pb=lb}}while(0);if(!sa){c[(kc()|0)>>2]=22;$n(b,0);l=0.0;i=g;return+l}u=c[o>>2]|0;if((u|0)==0){l=+(r|0)*0.0;i=g;return+l}ra=0;do{if((Pb|0)==(nb|0)&(Ob|0)==(mb|0)&((mb|0)<(ra|0)|(mb|0)==(ra|0)&nb>>>0<10>>>0)){if(k>>>0<=30>>>0){if((u>>>(k>>>0)|0)!=0){break}}l=+(r|0)*+(u>>>0>>>0);i=g;return+l}}while(0);u=(j|0)/-2|0;ra=(u|0)<0|0?-1:0;if((Ob|0)>(ra|0)|(Ob|0)==(ra|0)&Pb>>>0>u>>>0){c[(kc()|0)>>2]=34;l=+(r|0)*1.7976931348623157e+308*1.7976931348623157e+308;i=g;return+l}u=j-106|0;ra=(u|0)<0|0?-1:0;if((Ob|0)<(ra|0)|(Ob|0)==(ra|0)&Pb>>>0<u>>>0){c[(kc()|0)>>2]=34;l=+(r|0)*2.2250738585072014e-308*2.2250738585072014e-308;i=g;return+l}if((sb|0)==0){Qb=qb}else{if((sb|0)<9){u=h+(qb<<2)|0;ra=sb;sa=c[u>>2]|0;do{sa=sa*10|0;ra=ra+1|0;}while((ra|0)<9);c[u>>2]=sa}Qb=qb+1|0}ra=Pb;do{if((ob|0)<9){if(!((ob|0)<=(ra|0)&(ra|0)<18)){break}if((ra|0)==9){l=+(r|0)*+((c[o>>2]|0)>>>0>>>0);i=g;return+l}if((ra|0)<9){l=+(r|0)*+((c[o>>2]|0)>>>0>>>0)/+(c[16+(8-ra<<2)>>2]|0);i=g;return+l}ab=k+27+(ra*-3|0)|0;$a=c[o>>2]|0;if((ab|0)<=30){if(($a>>>(ab>>>0)|0)!=0){break}}l=+(r|0)*+($a>>>0>>>0)*+(c[16+(ra-10<<2)>>2]|0);i=g;return+l}}while(0);o=(ra|0)%9|0;if((o|0)==0){Rb=0;Sb=Qb;Tb=0;Ub=ra}else{sa=(ra|0)>-1?o:o+9|0;o=c[16+(8-sa<<2)>>2]|0;do{if((Qb|0)==0){Vb=0;Wb=0;Xb=ra}else{u=1e9/(o|0)|0;$a=ra;ab=0;_a=0;Za=0;while(1){Ya=h+(_a<<2)|0;Xa=c[Ya>>2]|0;Wa=((Xa>>>0)/(o>>>0)|0)+Za|0;c[Ya>>2]=Wa;Yb=ga((Xa>>>0)%(o>>>0)|0,u)|0;Xa=_a+1|0;if((_a|0)==(ab|0)&(Wa|0)==0){Zb=Xa&127;_b=$a-9|0}else{Zb=ab;_b=$a}if((Xa|0)==(Qb|0)){break}else{$a=_b;ab=Zb;_a=Xa;Za=Yb}}if((Yb|0)==0){Vb=Qb;Wb=Zb;Xb=_b;break}c[h+(Qb<<2)>>2]=Yb;Vb=Qb+1|0;Wb=Zb;Xb=_b}}while(0);Rb=Wb;Sb=Vb;Tb=0;Ub=9-sa+Xb|0}L321:while(1){o=h+(Rb<<2)|0;if((Ub|0)<18){ra=Sb;Za=Tb;while(1){_a=0;ab=ra+127|0;$a=ra;while(1){u=ab&127;Xa=h+(u<<2)|0;Wa=c[Xa>>2]|0;Ya=oo(Wa<<29|0>>>3,0<<29|Wa>>>3,_a,0)|0;Wa=K;Va=0;if(Wa>>>0>Va>>>0|Wa>>>0==Va>>>0&Ya>>>0>1e9>>>0){Va=zo(Ya,Wa,1e9,0)|0;Ua=Ao(Ya,Wa,1e9,0)|0;$b=Va;ac=Ua}else{$b=0;ac=Ya}c[Xa>>2]=ac;Xa=(u|0)==(Rb|0);if((u|0)!=($a+127&127|0)|Xa){bc=$a}else{bc=(ac|0)==0?u:$a}if(Xa){break}else{_a=$b;ab=u-1|0;$a=bc}}$a=Za-29|0;if(($b|0)==0){ra=bc;Za=$a}else{cc=$a;dc=bc;ec=$b;break}}}else{if((Ub|0)==18){fc=Sb;gc=Tb}else{hc=Rb;ic=Sb;jc=Tb;lc=Ub;break}while(1){if((c[o>>2]|0)>>>0>=9007199>>>0){hc=Rb;ic=fc;jc=gc;lc=18;break L321}Za=0;ra=fc+127|0;$a=fc;while(1){ab=ra&127;_a=h+(ab<<2)|0;u=c[_a>>2]|0;Xa=oo(u<<29|0>>>3,0<<29|u>>>3,Za,0)|0;u=K;Ya=0;if(u>>>0>Ya>>>0|u>>>0==Ya>>>0&Xa>>>0>1e9>>>0){Ya=zo(Xa,u,1e9,0)|0;Ua=Ao(Xa,u,1e9,0)|0;mc=Ya;nc=Ua}else{mc=0;nc=Xa}c[_a>>2]=nc;_a=(ab|0)==(Rb|0);if((ab|0)!=($a+127&127|0)|_a){oc=$a}else{oc=(nc|0)==0?ab:$a}if(_a){break}else{Za=mc;ra=ab-1|0;$a=oc}}$a=gc-29|0;if((mc|0)==0){fc=oc;gc=$a}else{cc=$a;dc=oc;ec=mc;break}}}o=Rb+127&127;if((o|0)==(dc|0)){$a=dc+127&127;ra=h+((dc+126&127)<<2)|0;c[ra>>2]=c[ra>>2]|c[h+($a<<2)>>2];pc=$a}else{pc=dc}c[h+(o<<2)>>2]=ec;Rb=o;Sb=pc;Tb=cc;Ub=Ub+9|0}L352:while(1){qc=ic+1&127;sa=h+((ic+127&127)<<2)|0;o=hc;$a=jc;ra=lc;while(1){Za=(ra|0)==18;ab=(ra|0)>27?9:1;rc=o;sc=$a;while(1){_a=0;while(1){Xa=_a+rc&127;if((Xa|0)==(ic|0)){tc=2;break}Ua=c[h+(Xa<<2)>>2]|0;Xa=c[8+(_a<<2)>>2]|0;if(Ua>>>0<Xa>>>0){tc=2;break}Ya=_a+1|0;if(Ua>>>0>Xa>>>0){tc=_a;break}if((Ya|0)<2){_a=Ya}else{tc=Ya;break}}if((tc|0)==2&Za){break L352}uc=ab+sc|0;if((rc|0)==(ic|0)){rc=ic;sc=uc}else{break}}Za=(1<<ab)-1|0;_a=1e9>>>(ab>>>0);vc=ra;wc=rc;Ya=rc;xc=0;do{Xa=h+(Ya<<2)|0;Ua=c[Xa>>2]|0;u=(Ua>>>(ab>>>0))+xc|0;c[Xa>>2]=u;xc=ga(Ua&Za,_a)|0;Ua=(Ya|0)==(wc|0)&(u|0)==0;Ya=Ya+1&127;vc=Ua?vc-9|0:vc;wc=Ua?Ya:wc;}while((Ya|0)!=(ic|0));if((xc|0)==0){o=wc;$a=uc;ra=vc;continue}if((qc|0)!=(wc|0)){break}c[sa>>2]=c[sa>>2]|1;o=wc;$a=uc;ra=vc}c[h+(ic<<2)>>2]=xc;hc=wc;ic=qc;jc=uc;lc=vc}ra=rc&127;if((ra|0)==(ic|0)){c[h+(qc-1<<2)>>2]=0;yc=qc}else{yc=ic}pa=+((c[h+(ra<<2)>>2]|0)>>>0>>>0);ra=rc+1&127;if((ra|0)==(yc|0)){$a=yc+1&127;c[h+($a-1<<2)>>2]=0;zc=$a}else{zc=yc}Aa=+(r|0);Ac=Aa*(pa*1.0e9+ +((c[h+(ra<<2)>>2]|0)>>>0>>>0));ra=sc+53|0;$a=ra-j|0;if(($a|0)<(k|0)){Bc=($a|0)<0?0:$a;Cc=1}else{Bc=k;Cc=0}if((Bc|0)<53){pa=+Hb(+(+bo(1.0,105-Bc|0)),+Ac);Dc=+rb(+Ac,+(+bo(1.0,53-Bc|0)));Ec=pa;Fc=Dc;Gc=pa+(Ac-Dc)}else{Ec=0.0;Fc=0.0;Gc=Ac}o=rc+2&127;do{if((o|0)==(zc|0)){Hc=Fc}else{sa=c[h+(o<<2)>>2]|0;do{if(sa>>>0<5e8>>>0){if((sa|0)==0){if((rc+3&127|0)==(zc|0)){Ic=Fc;break}}Ic=Aa*.25+Fc}else{if(sa>>>0>5e8>>>0){Ic=Aa*.75+Fc;break}if((rc+3&127|0)==(zc|0)){Ic=Aa*.5+Fc;break}else{Ic=Aa*.75+Fc;break}}}while(0);if((53-Bc|0)<=1){Hc=Ic;break}if(+rb(+Ic,+1.0)!=0.0){Hc=Ic;break}Hc=Ic+1.0}}while(0);Aa=Gc+Hc-Ec;do{if((ra&2147483647|0)>(-2-p|0)){if(+V(+Aa)<9007199254740992.0){Jc=Aa;Kc=Cc;Lc=sc}else{Jc=Aa*.5;Kc=(Cc|0)!=0&(Bc|0)==($a|0)?0:Cc;Lc=sc+1|0}if((Lc+50|0)<=(qa|0)){if(!((Kc|0)!=0&Hc!=0.0)){Mc=Jc;Nc=Lc;break}}c[(kc()|0)>>2]=34;Mc=Jc;Nc=Lc}else{Mc=Aa;Nc=sc}}while(0);l=+co(Mc,Nc);i=g;return+l}else{if((c[m>>2]|0)!=0){c[e>>2]=(c[e>>2]|0)-1}c[(kc()|0)>>2]=22;$n(b,0);l=0.0;i=g;return+l}}}while(0);do{if((x|0)==23){b=(c[m>>2]|0)==0;if(!b){c[e>>2]=(c[e>>2]|0)-1}if(s>>>0<4>>>0|(f|0)==0|b){break}else{Oc=s}do{c[e>>2]=(c[e>>2]|0)-1;Oc=Oc-1|0;}while(Oc>>>0>3>>>0)}}while(0);l=+(r|0)*w;i=g;return+l}function _n(a,b){a=a|0;b=b|0;var e=0,f=0,g=0,h=0,i=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0;e=a+4|0;f=c[e>>2]|0;g=a+100|0;if(f>>>0<(c[g>>2]|0)>>>0){c[e>>2]=f+1;h=d[f]|0}else{h=ao(a)|0}do{if((h|0)==45|(h|0)==43){f=(h|0)==45|0;i=c[e>>2]|0;if(i>>>0<(c[g>>2]|0)>>>0){c[e>>2]=i+1;j=d[i]|0}else{j=ao(a)|0}if((j-48|0)>>>0<10>>>0|(b|0)==0){k=f;l=j;break}if((c[g>>2]|0)==0){k=f;l=j;break}c[e>>2]=(c[e>>2]|0)-1;k=f;l=j}else{k=0;l=h}}while(0);if((l-48|0)>>>0>9>>>0){if((c[g>>2]|0)==0){m=-2147483648;n=0;return(K=m,n)|0}c[e>>2]=(c[e>>2]|0)-1;m=-2147483648;n=0;return(K=m,n)|0}else{o=l;p=0}while(1){q=o-48+p|0;l=c[e>>2]|0;if(l>>>0<(c[g>>2]|0)>>>0){c[e>>2]=l+1;r=d[l]|0}else{r=ao(a)|0}if(!((r-48|0)>>>0<10>>>0&(q|0)<214748364)){break}o=r;p=q*10|0}p=q;o=(q|0)<0|0?-1:0;if((r-48|0)>>>0<10>>>0){q=r;l=o;h=p;while(1){j=yo(h,l,10,0)|0;b=K;f=oo(q,(q|0)<0|0?-1:0,-48,-1)|0;i=oo(f,K,j,b)|0;b=K;j=c[e>>2]|0;if(j>>>0<(c[g>>2]|0)>>>0){c[e>>2]=j+1;s=d[j]|0}else{s=ao(a)|0}j=21474836;if((s-48|0)>>>0<10>>>0&((b|0)<(j|0)|(b|0)==(j|0)&i>>>0<2061584302>>>0)){q=s;l=b;h=i}else{t=s;u=b;v=i;break}}}else{t=r;u=o;v=p}if((t-48|0)>>>0<10>>>0){do{t=c[e>>2]|0;if(t>>>0<(c[g>>2]|0)>>>0){c[e>>2]=t+1;w=d[t]|0}else{w=ao(a)|0}}while((w-48|0)>>>0<10>>>0)}if((c[g>>2]|0)!=0){c[e>>2]=(c[e>>2]|0)-1}e=(k|0)!=0;k=po(0,0,v,u)|0;m=e?K:u;n=e?k:v;return(K=m,n)|0}function $n(a,b){a=a|0;b=b|0;var d=0,e=0,f=0;c[a+104>>2]=b;d=c[a+8>>2]|0;e=c[a+4>>2]|0;f=d-e|0;c[a+108>>2]=f;if((b|0)!=0&(f|0)>(b|0)){c[a+100>>2]=e+b;return}else{c[a+100>>2]=d;return}}function ao(b){b=b|0;var e=0,f=0,g=0,h=0,i=0,j=0,k=0,l=0;e=b+104|0;f=c[e>>2]|0;if((f|0)==0){g=3}else{if((c[b+108>>2]|0)<(f|0)){g=3}}do{if((g|0)==3){f=fo(b)|0;if((f|0)<0){break}h=c[e>>2]|0;i=c[b+8>>2]|0;do{if((h|0)==0){g=8}else{j=c[b+4>>2]|0;k=h-(c[b+108>>2]|0)-1|0;if((i-j|0)<=(k|0)){g=8;break}c[b+100>>2]=j+k}}while(0);if((g|0)==8){c[b+100>>2]=i}h=c[b+4>>2]|0;if((i|0)!=0){k=b+108|0;c[k>>2]=i+1-h+(c[k>>2]|0)}k=h-1|0;if((d[k]|0|0)==(f|0)){l=f;return l|0}a[k]=f;l=f;return l|0}}while(0);c[b+100>>2]=0;l=-1;return l|0}function bo(a,b){a=+a;b=b|0;var d=0.0,e=0,f=0.0,g=0;do{if((b|0)>1023){d=a*8.98846567431158e+307;e=b-1023|0;if((e|0)<=1023){f=d;g=e;break}e=b-2046|0;f=d*8.98846567431158e+307;g=(e|0)>1023?1023:e}else{if((b|0)>=-1022){f=a;g=b;break}d=a*2.2250738585072014e-308;e=b+1022|0;if((e|0)>=-1022){f=d;g=e;break}e=b+2044|0;f=d*2.2250738585072014e-308;g=(e|0)<-1022?-1022:e}}while(0);return+(f*(c[k>>2]=0<<20|0>>>12,c[k+4>>2]=g+1023<<20|0>>>12,+h[k>>3]))}function co(a,b){a=+a;b=b|0;return+(+bo(a,b))}function eo(b){b=b|0;var d=0,e=0,f=0,g=0,h=0;d=b+74|0;e=a[d]|0;a[d]=e-1&255|e;e=b+20|0;d=b+44|0;if((c[e>>2]|0)>>>0>(c[d>>2]|0)>>>0){ed[c[b+36>>2]&63](b,0,0)|0}c[b+16>>2]=0;c[b+28>>2]=0;c[e>>2]=0;e=b|0;f=c[e>>2]|0;if((f&20|0)==0){g=c[d>>2]|0;c[b+8>>2]=g;c[b+4>>2]=g;h=0;return h|0}if((f&4|0)==0){h=-1;return h|0}c[e>>2]=f|32;h=-1;return h|0}function fo(a){a=a|0;var b=0,e=0,f=0,g=0;b=i;i=i+8|0;e=b|0;if((c[a+8>>2]|0)==0){if((eo(a)|0)==0){f=3}else{g=-1}}else{f=3}do{if((f|0)==3){if((ed[c[a+32>>2]&63](a,e,1)|0)!=1){g=-1;break}g=d[e]|0}}while(0);i=b;return g|0}function go(a,b,d){a=a|0;b=b|0;d=d|0;var e=0,f=0,g=0,h=0.0,j=0,k=0,l=0,m=0;d=i;i=i+112|0;e=d|0;ko(e|0,0,112)|0;f=e+4|0;c[f>>2]=a;g=e+8|0;c[g>>2]=-1;c[e+44>>2]=a;c[e+76>>2]=-1;$n(e,0);h=+Zn(e,2,1);j=(c[f>>2]|0)-(c[g>>2]|0)+(c[e+108>>2]|0)|0;if((b|0)==0){k=112;l=0;i=d;return+h}if((j|0)==0){m=a}else{m=a+j|0}c[b>>2]=m;k=112;l=0;i=d;return+h}function ho(b){b=b|0;var c=0;c=b;while(a[c]|0){c=c+1|0}return c-b|0}function io(b,d,e){b=b|0;d=d|0;e=e|0;var f=0;f=b|0;if((b&3)==(d&3)){while(b&3){if((e|0)==0)return f|0;a[b]=a[d]|0;b=b+1|0;d=d+1|0;e=e-1|0}while((e|0)>=4){c[b>>2]=c[d>>2];b=b+4|0;d=d+4|0;e=e-4|0}}while((e|0)>0){a[b]=a[d]|0;b=b+1|0;d=d+1|0;e=e-1|0}return f|0}function jo(a,b,c){a=a|0;b=b|0;c=c|0;var e=0,f=0,g=0;while((e|0)<(c|0)){f=d[a+e|0]|0;g=d[b+e|0]|0;if((f|0)!=(g|0))return((f|0)>(g|0)?1:-1)|0;e=e+1|0}return 0}function ko(b,d,e){b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0,i=0;f=b+e|0;if((e|0)>=20){d=d&255;g=b&3;h=d|d<<8|d<<16|d<<24;i=f&~3;if(g){g=b+4-g|0;while((b|0)<(g|0)){a[b]=d;b=b+1|0}}while((b|0)<(i|0)){c[b>>2]=h;b=b+4|0}}while((b|0)<(f|0)){a[b]=d;b=b+1|0}return b-e|0}function lo(a){a=a|0;if((a|0)<65)return a|0;if((a|0)>90)return a|0;return a-65+97|0}function mo(b,c,d){b=b|0;c=c|0;d=d|0;var e=0;if((c|0)<(b|0)&(b|0)<(c+d|0)){e=b;c=c+d|0;b=b+d|0;while((d|0)>0){b=b-1|0;c=c-1|0;d=d-1|0;a[b]=a[c]|0}b=e}else{io(b,c,d)|0}return b|0}function no(b,c,d){b=b|0;c=c|0;d=d|0;var e=0,f=0,g=0;while(e>>>0<d>>>0){f=lo(a[b+e|0]|0)|0;g=lo(a[c+e|0]|0)|0;if((f|0)==(g|0)&(f|0)==0)return 0;if((f|0)==0)return-1;if((g|0)==0)return 1;if((f|0)==(g|0)){e=e+1|0;continue}else{return(f>>>0>g>>>0?1:-1)|0}}return 0}function oo(a,b,c,d){a=a|0;b=b|0;c=c|0;d=d|0;var e=0;e=a+c>>>0;return(K=b+d+(e>>>0<a>>>0|0)>>>0,e|0)|0}function po(a,b,c,d){a=a|0;b=b|0;c=c|0;d=d|0;var e=0;e=b-d>>>0;e=b-d-(c>>>0>a>>>0|0)>>>0;return(K=e,a-c>>>0|0)|0}function qo(a,b,c){a=a|0;b=b|0;c=c|0;if((c|0)<32){K=b<<c|(a&(1<<c)-1<<32-c)>>>32-c;return a<<c}K=a<<c-32;return 0}function ro(a,b,c){a=a|0;b=b|0;c=c|0;if((c|0)<32){K=b>>>c;return a>>>c|(b&(1<<c)-1)<<32-c}K=0;return b>>>c-32|0}function so(a,b,c){a=a|0;b=b|0;c=c|0;if((c|0)<32){K=b>>c;return a>>>c|(b&(1<<c)-1)<<32-c}K=(b|0)<0?-1:0;return b>>c-32|0}function to(b){b=b|0;var c=0;c=a[n+(b>>>24)|0]|0;if((c|0)<8)return c|0;c=a[n+(b>>16&255)|0]|0;if((c|0)<8)return c+8|0;c=a[n+(b>>8&255)|0]|0;if((c|0)<8)return c+16|0;return(a[n+(b&255)|0]|0)+24|0}function uo(b){b=b|0;var c=0;c=a[m+(b&255)|0]|0;if((c|0)<8)return c|0;c=a[m+(b>>8&255)|0]|0;if((c|0)<8)return c+8|0;c=a[m+(b>>16&255)|0]|0;if((c|0)<8)return c+16|0;return(a[m+(b>>>24)|0]|0)+24|0}function vo(a,b){a=a|0;b=b|0;var c=0,d=0,e=0,f=0;c=a&65535;d=b&65535;e=ga(d,c)|0;f=a>>>16;a=(e>>>16)+(ga(d,f)|0)|0;d=b>>>16;b=ga(d,c)|0;return(K=(a>>>16)+(ga(d,f)|0)+(((a&65535)+b|0)>>>16)|0,a+b<<16|e&65535|0)|0}function wo(a,b,c,d){a=a|0;b=b|0;c=c|0;d=d|0;var e=0,f=0,g=0,h=0,i=0;e=b>>31|((b|0)<0?-1:0)<<1;f=((b|0)<0?-1:0)>>31|((b|0)<0?-1:0)<<1;g=d>>31|((d|0)<0?-1:0)<<1;h=((d|0)<0?-1:0)>>31|((d|0)<0?-1:0)<<1;i=po(e^a,f^b,e,f)|0;b=K;a=g^e;e=h^f;f=po((Bo(i,b,po(g^c,h^d,g,h)|0,K,0)|0)^a,K^e,a,e)|0;return(K=K,f)|0}function xo(a,b,d,e){a=a|0;b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0,j=0,k=0,l=0,m=0;f=i;i=i+8|0;g=f|0;h=b>>31|((b|0)<0?-1:0)<<1;j=((b|0)<0?-1:0)>>31|((b|0)<0?-1:0)<<1;k=e>>31|((e|0)<0?-1:0)<<1;l=((e|0)<0?-1:0)>>31|((e|0)<0?-1:0)<<1;m=po(h^a,j^b,h,j)|0;b=K;Bo(m,b,po(k^d,l^e,k,l)|0,K,g)|0;l=po(c[g>>2]^h,c[g+4>>2]^j,h,j)|0;j=K;i=f;return(K=j,l)|0}function yo(a,b,c,d){a=a|0;b=b|0;c=c|0;d=d|0;var e=0,f=0;e=a;a=c;c=vo(e,a)|0;f=K;return(K=(ga(b,a)|0)+(ga(d,e)|0)+f|f&0,c|0|0)|0}function zo(a,b,c,d){a=a|0;b=b|0;c=c|0;d=d|0;var e=0;e=Bo(a,b,c,d,0)|0;return(K=K,e)|0}function Ao(a,b,d,e){a=a|0;b=b|0;d=d|0;e=e|0;var f=0,g=0;f=i;i=i+8|0;g=f|0;Bo(a,b,d,e,g)|0;i=f;return(K=c[g+4>>2]|0,c[g>>2]|0)|0}function Bo(a,b,d,e,f){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,i=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,L=0,M=0;g=a;h=b;i=h;j=d;k=e;l=k;if((i|0)==0){m=(f|0)!=0;if((l|0)==0){if(m){c[f>>2]=(g>>>0)%(j>>>0);c[f+4>>2]=0}n=0;o=(g>>>0)/(j>>>0)>>>0;return(K=n,o)|0}else{if(!m){n=0;o=0;return(K=n,o)|0}c[f>>2]=a|0;c[f+4>>2]=b&0;n=0;o=0;return(K=n,o)|0}}m=(l|0)==0;do{if((j|0)==0){if(m){if((f|0)!=0){c[f>>2]=(i>>>0)%(j>>>0);c[f+4>>2]=0}n=0;o=(i>>>0)/(j>>>0)>>>0;return(K=n,o)|0}if((g|0)==0){if((f|0)!=0){c[f>>2]=0;c[f+4>>2]=(i>>>0)%(l>>>0)}n=0;o=(i>>>0)/(l>>>0)>>>0;return(K=n,o)|0}p=l-1|0;if((p&l|0)==0){if((f|0)!=0){c[f>>2]=a|0;c[f+4>>2]=p&i|b&0}n=0;o=i>>>((uo(l|0)|0)>>>0);return(K=n,o)|0}p=(to(l|0)|0)-(to(i|0)|0)|0;if(p>>>0<=30){q=p+1|0;r=31-p|0;s=q;t=i<<r|g>>>(q>>>0);u=i>>>(q>>>0);v=0;w=g<<r;break}if((f|0)==0){n=0;o=0;return(K=n,o)|0}c[f>>2]=a|0;c[f+4>>2]=h|b&0;n=0;o=0;return(K=n,o)|0}else{if(!m){r=(to(l|0)|0)-(to(i|0)|0)|0;if(r>>>0<=31){q=r+1|0;p=31-r|0;x=r-31>>31;s=q;t=g>>>(q>>>0)&x|i<<p;u=i>>>(q>>>0)&x;v=0;w=g<<p;break}if((f|0)==0){n=0;o=0;return(K=n,o)|0}c[f>>2]=a|0;c[f+4>>2]=h|b&0;n=0;o=0;return(K=n,o)|0}p=j-1|0;if((p&j|0)!=0){x=(to(j|0)|0)+33-(to(i|0)|0)|0;q=64-x|0;r=32-x|0;y=r>>31;z=x-32|0;A=z>>31;s=x;t=r-1>>31&i>>>(z>>>0)|(i<<r|g>>>(x>>>0))&A;u=A&i>>>(x>>>0);v=g<<q&y;w=(i<<q|g>>>(z>>>0))&y|g<<r&x-33>>31;break}if((f|0)!=0){c[f>>2]=p&g;c[f+4>>2]=0}if((j|0)==1){n=h|b&0;o=a|0|0;return(K=n,o)|0}else{p=uo(j|0)|0;n=i>>>(p>>>0)|0;o=i<<32-p|g>>>(p>>>0)|0;return(K=n,o)|0}}}while(0);if((s|0)==0){B=w;C=v;D=u;E=t;F=0;G=0}else{g=d|0|0;d=k|e&0;e=oo(g,d,-1,-1)|0;k=K;i=w;w=v;v=u;u=t;t=s;s=0;while(1){H=w>>>31|i<<1;I=s|w<<1;j=u<<1|i>>>31|0;a=u>>>31|v<<1|0;po(e,k,j,a)|0;b=K;h=b>>31|((b|0)<0?-1:0)<<1;J=h&1;L=po(j,a,h&g,(((b|0)<0?-1:0)>>31|((b|0)<0?-1:0)<<1)&d)|0;M=K;b=t-1|0;if((b|0)==0){break}else{i=H;w=I;v=M;u=L;t=b;s=J}}B=H;C=I;D=M;E=L;F=0;G=J}J=C;C=0;if((f|0)!=0){c[f>>2]=E;c[f+4>>2]=D}n=(J|0)>>>31|(B|C)<<1|(C<<1|J>>>31)&0|F;o=(J<<1|0>>>31)&-2|G;return(K=n,o)|0}function Co(a,b,c,d,e,f){a=a|0;b=b|0;c=c|0;d=d|0;e=e|0;f=f|0;Yc[a&7](b|0,c|0,d|0,e|0,f|0)}function Do(a,b,c,d,e,f,g,h){a=a|0;b=b|0;c=c|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;Zc[a&127](b|0,c|0,d|0,e|0,f|0,g|0,h|0)}function Eo(a,b){a=a|0;b=b|0;_c[a&511](b|0)}function Fo(a,b,c){a=a|0;b=b|0;c=c|0;$c[a&127](b|0,c|0)}function Go(a,b,c){a=a|0;b=b|0;c=c|0;return ad[a&31](b|0,c|0)|0}function Ho(a,b,c,d,e,f){a=a|0;b=b|0;c=c|0;d=d|0;e=e|0;f=f|0;return bd[a&31](b|0,c|0,d|0,e|0,f|0)|0}function Io(a,b,c,d,e,f,g,h){a=a|0;b=b|0;c=c|0;d=d|0;e=e|0;f=f|0;g=g|0;h=+h;cd[a&7](b|0,c|0,d|0,e|0,f|0,g|0,+h)}function Jo(a,b){a=a|0;b=b|0;return dd[a&127](b|0)|0}function Ko(a,b,c,d){a=a|0;b=b|0;c=c|0;d=d|0;return ed[a&63](b|0,c|0,d|0)|0}function Lo(a,b,c,d){a=a|0;b=b|0;c=c|0;d=d|0;fd[a&7](b|0,c|0,d|0)}function Mo(a,b,c,d,e,f,g){a=a|0;b=b|0;c=c|0;d=d|0;e=e|0;f=f|0;g=+g;gd[a&15](b|0,c|0,d|0,e|0,f|0,+g)}function No(a){a=a|0;hd[a&3]()}function Oo(a,b,c,d,e,f,g,h,i){a=a|0;b=b|0;c=c|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;i=i|0;return id[a&31](b|0,c|0,d|0,e|0,f|0,g|0,h|0,i|0)|0}function Po(a,b,c,d,e,f,g,h,i,j){a=a|0;b=b|0;c=c|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;i=i|0;j=j|0;jd[a&7](b|0,c|0,d|0,e|0,f|0,g|0,h|0,i|0,j|0)}function Qo(a,b,c,d,e,f,g){a=a|0;b=b|0;c=c|0;d=d|0;e=e|0;f=f|0;g=g|0;kd[a&31](b|0,c|0,d|0,e|0,f|0,g|0)}function Ro(a,b,c,d,e){a=a|0;b=b|0;c=c|0;d=d|0;e=e|0;return ld[a&15](b|0,c|0,d|0,e|0)|0}function So(a,b,c,d,e,f,g,h,i){a=a|0;b=b|0;c=c|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;i=i|0;md[a&15](b|0,c|0,d|0,e|0,f|0,g|0,h|0,i|0)}function To(a,b,c,d,e){a=a|0;b=b|0;c=c|0;d=d|0;e=e|0;nd[a&15](b|0,c|0,d|0,e|0)}function Uo(a,b,c,d,e){a=a|0;b=b|0;c=c|0;d=d|0;e=e|0;ha(0)}function Vo(a,b,c,d,e,f,g){a=a|0;b=b|0;c=c|0;d=d|0;e=e|0;f=f|0;g=g|0;ha(1)}function Wo(a){a=a|0;ha(2)}function Xo(a,b){a=a|0;b=b|0;ha(3)}function Yo(a,b){a=a|0;b=b|0;ha(4);return 0}function Zo(a,b,c,d,e){a=a|0;b=b|0;c=c|0;d=d|0;e=e|0;ha(5);return 0}function _o(a,b,c,d,e,f,g){a=a|0;b=b|0;c=c|0;d=d|0;e=e|0;f=f|0;g=+g;ha(6)}function $o(a){a=a|0;ha(7);return 0}function ap(a,b,c){a=a|0;b=b|0;c=c|0;ha(8);return 0}function bp(a,b,c){a=a|0;b=b|0;c=c|0;ha(9)}function cp(a,b,c,d,e,f){a=a|0;b=b|0;c=c|0;d=d|0;e=e|0;f=+f;ha(10)}function dp(){ha(11)}function ep(a,b,c,d,e,f,g,h){a=a|0;b=b|0;c=c|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;ha(12);return 0}function fp(a,b,c,d,e,f,g,h,i){a=a|0;b=b|0;c=c|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;i=i|0;ha(13)}function gp(a,b,c,d,e,f){a=a|0;b=b|0;c=c|0;d=d|0;e=e|0;f=f|0;ha(14)}function hp(a,b,c,d){a=a|0;b=b|0;c=c|0;d=d|0;ha(15);return 0}function ip(a,b,c,d,e,f,g,h){a=a|0;b=b|0;c=c|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;ha(16)}function jp(a,b,c,d){a=a|0;b=b|0;c=c|0;d=d|0;ha(17)}
// EMSCRIPTEN_END_FUNCS
var Yc=[Uo,Uo,Gn,Uo,Hn,Uo,Fn,Uo];var Zc=[Vo,Vo,Mi,Vo,Wi,Vo,Yi,Vo,Ek,Vo,zi,Vo,xi,Vo,yk,Vo,Ii,Vo,Li,Vo,Zi,Vo,li,Vo,Wh,Vo,Ki,Vo,rh,Vo,Kh,Vo,Xi,Vo,ji,Vo,Oh,Vo,Gh,Vo,Ih,Vo,xh,Vo,Mh,Vo,Eh,Vo,Ch,Vo,Uh,Vo,Sh,Vo,Qh,Vo,_i,Vo,lh,Vo,Ji,Vo,ph,Vo,hh,Vo,jh,Vo,nh,Vo,fh,Vo,vh,Vo,th,Vo,dh,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo,Vo];var _c=[Wo,Wo,Kk,Wo,bh,Wo,si,Wo,ff,Wo,Sf,Wo,Pk,Wo,al,Wo,Ne,Wo,Ae,Wo,ci,Wo,Ze,Wo,Ol,Wo,df,Wo,Ah,Wo,Tg,Wo,Pg,Wo,Wn,Wo,_e,Wo,_k,Wo,ol,Wo,Ui,Wo,Bh,Wo,qn,Wo,Hg,Wo,Gk,Wo,$k,Wo,tg,Wo,ch,Wo,Hj,Wo,Uk,Wo,lm,Wo,vn,Wo,km,Wo,Ng,Wo,df,Wo,Zg,Wo,mj,Wo,nm,Wo,bl,Wo,Mn,Wo,zk,Wo,jm,Wo,xg,Wo,ze,Wo,Yg,Wo,hj,Wo,_e,Wo,Vm,Wo,di,Wo,Zl,Wo,Ij,Wo,sg,Wo,Eg,Wo,ej,Wo,Ti,Wo,Qg,Wo,wj,Wo,Vn,Wo,Rf,Wo,Id,Wo,Ym,Wo,Zm,Wo,Ig,Wo,eg,Wo,yn,Wo,yl,Wo,sn,Wo,ug,Wo,Og,Wo,bk,Wo,re,Wo,Sj,Wo,Jg,Wo,Yk,Wo,$m,Wo,wn,Wo,Ge,Wo,Tk,Wo,fj,Wo,mk,Wo,rn,Wo,ek,Wo,Ak,Wo,_m,Wo,lj,Wo,ri,Wo,Sg,Wo,He,Wo,Ug,Wo,zg,Wo,of,Wo,Gl,Wo,tk,Wo,lk,Wo,mm,Wo,Gf,Wo,sn,Wo,zn,Wo,Dg,Wo,pf,Wo,rg,Wo,Gg,Wo,Fk,Wo,Of,Wo,Cg,Wo,Oe,Wo,Wk,Wo,Rg,Wo,Tj,Wo,Fi,Wo,xj,Wo,te,Wo,yg,Wo,dg,Wo,Bg,Wo,im,Wo,Xm,Wo,Gi,Wo,xn,Wo,Lk,Wo,se,Wo,un,Wo,ij,Wo,uk,Wo,ck,Wo,vf,Wo,pl,Wo,cf,Wo,wg,Wo,an,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo,Wo];var $c=[Xo,Xo,wm,Xo,Yj,Xo,Aj,Xo,tm,Xo,Rj,Xo,sm,Xo,Gj,Xo,ue,Xo,Jk,Xo,fg,Xo,sj,Xo,$j,Xo,Oj,Xo,rj,Xo,Wj,Xo,pj,Xo,Zj,Xo,Xk,Xo,Ie,Xo,ak,Xo,vm,Xo,Bj,Xo,bf,Xo,xm,Xo,Qj,Xo,Dj,Xo,um,Xo,Fj,Xo,Be,Xo,Tf,Xo,Pe,Xo,Ok,Xo,Lj,Xo,vj,Xo,uj,Xo,qj,Xo,Mj,Xo,Nj,Xo,Xj,Xo,Cj,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo,Xo];var ad=[Yo,Yo,Se,Yo,kl,Yo,og,Yo,ul,Yo,ql,Yo,Ee,Yo,Le,Yo,gl,Yo,sl,Yo,cg,Yo,qg,Yo,il,Yo,ag,Yo,xe,Yo,Yo,Yo];var bd=[Zo,Zo,nl,Zo,Tl,Zo,_g,Zo,fm,Zo,Wl,Zo,xl,Zo,Jl,Zo,Vg,Zo,Bl,Zo,El,Zo,cm,Zo,Ml,Zo,Zo,Zo,Zo,Zo,Zo,Zo];var cd=[_o,_o,Bk,_o,vk,_o,_o,_o];var dd=[$o,$o,Lm,$o,Kj,$o,_f,$o,Dl,$o,Bm,$o,$f,$o,Jm,$o,yj,$o,Hi,$o,zm,$o,Re,$o,ng,$o,mg,$o,dm,$o,Fm,$o,Dm,$o,tn,$o,ef,$o,rm,$o,om,$o,Em,$o,pm,$o,Xf,$o,Cl,$o,_j,$o,Gm,$o,ve,$o,zj,$o,Vl,$o,Uj,$o,ym,$o,Ce,$o,em,$o,Xn,$o,Lg,$o,tj,$o,De,$o,qm,$o,Yf,$o,jg,$o,Je,$o,Fl,$o,Ej,$o,Km,$o,Qe,$o,Ul,$o,Ll,$o,kg,$o,nj,$o,Am,$o,oj,$o,$e,$o,Jj,$o,Nl,$o,Pj,$o,Cm,$o,Vj,$o,Kl,$o,Vi,$o,Im,$o,Hm,$o,Yl,$o,hm,$o];var ed=[ap,ap,Xg,ap,rl,ap,jl,ap,An,ap,ml,ap,ah,ap,jf,ap,bg,ap,Zf,ap,cl,ap,gg,ap,Mk,ap,wl,ap,hl,ap,we,ap,kf,ap,lg,ap,tl,ap,Hk,ap,Uf,ap,Ke,ap,pg,ap,ap,ap,ap,ap,ap,ap,ap,ap,ap,ap,ap,ap,ap,ap,ap,ap,ap,ap];var fd=[bp,bp,hf,bp,Mg,bp,bp,bp];var gd=[cp,cp,Ci,cp,Ai,cp,pi,cp,mi,cp,cp,cp,cp,cp,cp,cp];var hd=[dp,dp,Jd,dp];var id=[ep,ep,Hl,ep,Rl,ep,Pl,ep,am,ep,Il,ep,_l,ep,zl,ep,Al,ep,ep,ep,ep,ep,ep,ep,ep,ep,ep,ep,ep,ep,ep,ep];var jd=[fp,fp,$i,fp,Ni,fp,fp,fp];var kd=[gp,gp,In,gp,yi,gp,ui,gp,ti,gp,Jn,gp,Di,gp,Ik,gp,hg,gp,qi,gp,ei,gp,ki,gp,fi,gp,Kn,gp,Vf,gp,Nk,gp];var ld=[hp,hp,dl,hp,el,hp,vl,hp,ll,hp,fl,hp,hp,hp,hp,hp];var md=[ip,ip,jj,ip,gj,ip,dk,ip,nk,ip,hk,ip,pk,ip,ip,ip];var nd=[jp,jp,Cn,jp,Dn,jp,Bn,jp,Wf,jp,$g,jp,ig,jp,Wg,jp];return{_memcmp:jo,_strncasecmp:no,_tolower:lo,_free:Mn,_main:Md,_realloc:On,_memmove:mo,__GLOBAL__I_a:oe,_strlen:ho,_memset:ko,_malloc:Ln,__GLOBAL__I_a191:Ue,_memcpy:io,_calloc:Nn,runPostSets:Ed,stackAlloc:od,stackSave:pd,stackRestore:qd,setThrew:rd,setTempRet0:ud,setTempRet1:vd,setTempRet2:wd,setTempRet3:xd,setTempRet4:yd,setTempRet5:zd,setTempRet6:Ad,setTempRet7:Bd,setTempRet8:Cd,setTempRet9:Dd,dynCall_viiiii:Co,dynCall_viiiiiii:Do,dynCall_vi:Eo,dynCall_vii:Fo,dynCall_iii:Go,dynCall_iiiiii:Ho,dynCall_viiiiiid:Io,dynCall_ii:Jo,dynCall_iiii:Ko,dynCall_viii:Lo,dynCall_viiiiid:Mo,dynCall_v:No,dynCall_iiiiiiiii:Oo,dynCall_viiiiiiiii:Po,dynCall_viiiiii:Qo,dynCall_iiiii:Ro,dynCall_viiiiiiii:So,dynCall_viiii:To}})
// EMSCRIPTEN_END_ASM
({ "Math": Math, "Int8Array": Int8Array, "Int16Array": Int16Array, "Int32Array": Int32Array, "Uint8Array": Uint8Array, "Uint16Array": Uint16Array, "Uint32Array": Uint32Array, "Float32Array": Float32Array, "Float64Array": Float64Array }, { "abort": abort, "assert": assert, "asmPrintInt": asmPrintInt, "asmPrintFloat": asmPrintFloat, "min": Math_min, "invoke_viiiii": invoke_viiiii, "invoke_viiiiiii": invoke_viiiiiii, "invoke_vi": invoke_vi, "invoke_vii": invoke_vii, "invoke_iii": invoke_iii, "invoke_iiiiii": invoke_iiiiii, "invoke_viiiiiid": invoke_viiiiiid, "invoke_ii": invoke_ii, "invoke_iiii": invoke_iiii, "invoke_viii": invoke_viii, "invoke_viiiiid": invoke_viiiiid, "invoke_v": invoke_v, "invoke_iiiiiiiii": invoke_iiiiiiiii, "invoke_viiiiiiiii": invoke_viiiiiiiii, "invoke_viiiiii": invoke_viiiiii, "invoke_iiiii": invoke_iiiii, "invoke_viiiiiiii": invoke_viiiiiiii, "invoke_viiii": invoke_viiii, "_llvm_lifetime_end": _llvm_lifetime_end, "__scanString": __scanString, "_pthread_mutex_lock": _pthread_mutex_lock, "___cxa_end_catch": ___cxa_end_catch, "_strtoul": _strtoul, "_strtoull": _strtoull, "_fflush": _fflush, "_fputc": _fputc, "_strtok": _strtok, "_fwrite": _fwrite, "_llvm_eh_exception": _llvm_eh_exception, "_fputs": _fputs, "_isspace": _isspace, "_read": _read, "_SDL_UnlockSurface": _SDL_UnlockSurface, "_perror": _perror, "___cxa_guard_abort": ___cxa_guard_abort, "_newlocale": _newlocale, "___gxx_personality_v0": ___gxx_personality_v0, "_pthread_cond_wait": _pthread_cond_wait, "___cxa_rethrow": ___cxa_rethrow, "_modf": _modf, "___resumeException": ___resumeException, "_strcmp": _strcmp, "_strncmp": _strncmp, "_vsscanf": _vsscanf, "_snprintf": _snprintf, "_fgetc": _fgetc, "_stat": _stat, "__getFloat": __getFloat, "_atexit": _atexit, "_hypot": _hypot, "___cxa_free_exception": ___cxa_free_exception, "_close": _close, "___setErrNo": ___setErrNo, "_isxdigit": _isxdigit, "_exit": _exit, "_sprintf": _sprintf, "___ctype_b_loc": ___ctype_b_loc, "_freelocale": _freelocale, "_catgets": _catgets, "_fmod": _fmod, "__isLeapYear": __isLeapYear, "_fmax": _fmax, "_asprintf": _asprintf, "___cxa_is_number_type": ___cxa_is_number_type, "_gmtime": _gmtime, "___cxa_does_inherit": ___cxa_does_inherit, "___cxa_guard_acquire": ___cxa_guard_acquire, "___cxa_begin_catch": ___cxa_begin_catch, "_recv": _recv, "__parseInt64": __parseInt64, "__ZSt18uncaught_exceptionv": __ZSt18uncaught_exceptionv, "_SDL_PollEvent": _SDL_PollEvent, "___cxa_call_unexpected": ___cxa_call_unexpected, "_SDL_Init": _SDL_Init, "_round": _round, "_copysign": _copysign, "_fmin": _fmin, "_mmap": _mmap, "__exit": __exit, "_strftime": _strftime, "_llvm_va_end": _llvm_va_end, "_fabsf": _fabsf, "_floorf": _floorf, "_rint": _rint, "___cxa_throw": ___cxa_throw, "_floorl": _floorl, "_fabsl": _fabsl, "_send": _send, "_toupper": _toupper, "_pread": _pread, "_SDL_SetVideoMode": _SDL_SetVideoMode, "_SDL_LockSurface": _SDL_LockSurface, "_open": _open, "_sqrtf": _sqrtf, "__arraySum": __arraySum, "_puts": _puts, "___cxa_find_matching_catch": ___cxa_find_matching_catch, "_strdup": _strdup, "_log10": _log10, "__formatString": __formatString, "_pthread_cond_broadcast": _pthread_cond_broadcast, "__ZSt9terminatev": __ZSt9terminatev, "_isascii": _isascii, "_pthread_mutex_unlock": _pthread_mutex_unlock, "_llvm_pow_f64": _llvm_pow_f64, "_sbrk": _sbrk, "___errno_location": ___errno_location, "_strerror": _strerror, "_fstat": _fstat, "_catclose": _catclose, "_llvm_lifetime_start": _llvm_lifetime_start, "__parseInt": __parseInt, "___cxa_guard_release": ___cxa_guard_release, "_ungetc": _ungetc, "_vsprintf": _vsprintf, "_SDL_EnableUNICODE": _SDL_EnableUNICODE, "_uselocale": _uselocale, "_gmtime_r": _gmtime_r, "_vsnprintf": _vsnprintf, "_sscanf": _sscanf, "_sysconf": _sysconf, "_fread": _fread, "_strtok_r": _strtok_r, "_abort": _abort, "_fprintf": _fprintf, "_isdigit": _isdigit, "_strtoll": _strtoll, "__reallyNegative": __reallyNegative, "__addDays": __addDays, "_fabs": _fabs, "_floor": _floor, "_sqrt": _sqrt, "_write": _write, "_SDL_UpdateRect": _SDL_UpdateRect, "_SDL_GetModState": _SDL_GetModState, "___cxa_allocate_exception": ___cxa_allocate_exception, "_ceilf": _ceilf, "_vasprintf": _vasprintf, "_emscripten_set_main_loop": _emscripten_set_main_loop, "_catopen": _catopen, "___ctype_toupper_loc": ___ctype_toupper_loc, "___ctype_tolower_loc": ___ctype_tolower_loc, "_pwrite": _pwrite, "_strerror_r": _strerror_r, "_time": _time, "STACKTOP": STACKTOP, "STACK_MAX": STACK_MAX, "tempDoublePtr": tempDoublePtr, "ABORT": ABORT, "cttz_i8": cttz_i8, "ctlz_i8": ctlz_i8, "NaN": NaN, "Infinity": Infinity, "_stdin": _stdin, "__ZTVN10__cxxabiv117__class_type_infoE": __ZTVN10__cxxabiv117__class_type_infoE, "__ZTVN10__cxxabiv120__si_class_type_infoE": __ZTVN10__cxxabiv120__si_class_type_infoE, "_stderr": _stderr, "___fsmu8": ___fsmu8, "_stdout": _stdout, "___dso_handle": ___dso_handle }, buffer);
var _memcmp = Module["_memcmp"] = asm["_memcmp"];
var _strncasecmp = Module["_strncasecmp"] = asm["_strncasecmp"];
var _tolower = Module["_tolower"] = asm["_tolower"];
var _free = Module["_free"] = asm["_free"];
var _main = Module["_main"] = asm["_main"];
var _realloc = Module["_realloc"] = asm["_realloc"];
var _memmove = Module["_memmove"] = asm["_memmove"];
var __GLOBAL__I_a = Module["__GLOBAL__I_a"] = asm["__GLOBAL__I_a"];
var _strlen = Module["_strlen"] = asm["_strlen"];
var _memset = Module["_memset"] = asm["_memset"];
var _malloc = Module["_malloc"] = asm["_malloc"];
var __GLOBAL__I_a191 = Module["__GLOBAL__I_a191"] = asm["__GLOBAL__I_a191"];
var _memcpy = Module["_memcpy"] = asm["_memcpy"];
var _calloc = Module["_calloc"] = asm["_calloc"];
var runPostSets = Module["runPostSets"] = asm["runPostSets"];
var dynCall_viiiii = Module["dynCall_viiiii"] = asm["dynCall_viiiii"];
var dynCall_viiiiiii = Module["dynCall_viiiiiii"] = asm["dynCall_viiiiiii"];
var dynCall_vi = Module["dynCall_vi"] = asm["dynCall_vi"];
var dynCall_vii = Module["dynCall_vii"] = asm["dynCall_vii"];
var dynCall_iii = Module["dynCall_iii"] = asm["dynCall_iii"];
var dynCall_iiiiii = Module["dynCall_iiiiii"] = asm["dynCall_iiiiii"];
var dynCall_viiiiiid = Module["dynCall_viiiiiid"] = asm["dynCall_viiiiiid"];
var dynCall_ii = Module["dynCall_ii"] = asm["dynCall_ii"];
var dynCall_iiii = Module["dynCall_iiii"] = asm["dynCall_iiii"];
var dynCall_viii = Module["dynCall_viii"] = asm["dynCall_viii"];
var dynCall_viiiiid = Module["dynCall_viiiiid"] = asm["dynCall_viiiiid"];
var dynCall_v = Module["dynCall_v"] = asm["dynCall_v"];
var dynCall_iiiiiiiii = Module["dynCall_iiiiiiiii"] = asm["dynCall_iiiiiiiii"];
var dynCall_viiiiiiiii = Module["dynCall_viiiiiiiii"] = asm["dynCall_viiiiiiiii"];
var dynCall_viiiiii = Module["dynCall_viiiiii"] = asm["dynCall_viiiiii"];
var dynCall_iiiii = Module["dynCall_iiiii"] = asm["dynCall_iiiii"];
var dynCall_viiiiiiii = Module["dynCall_viiiiiiii"] = asm["dynCall_viiiiiiii"];
var dynCall_viiii = Module["dynCall_viiii"] = asm["dynCall_viiii"];
Runtime.stackAlloc = function(size) { return asm['stackAlloc'](size) };
Runtime.stackSave = function() { return asm['stackSave']() };
Runtime.stackRestore = function(top) { asm['stackRestore'](top) };
// TODO: strip out parts of this we do not need
//======= begin closure i64 code =======
// Copyright 2009 The Closure Library Authors. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS-IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
/**
 * @fileoverview Defines a Long class for representing a 64-bit two's-complement
 * integer value, which faithfully simulates the behavior of a Java "long". This
 * implementation is derived from LongLib in GWT.
 *
 */
var i64Math = (function() { // Emscripten wrapper
  var goog = { math: {} };
  /**
   * Constructs a 64-bit two's-complement integer, given its low and high 32-bit
   * values as *signed* integers.  See the from* functions below for more
   * convenient ways of constructing Longs.
   *
   * The internal representation of a long is the two given signed, 32-bit values.
   * We use 32-bit pieces because these are the size of integers on which
   * Javascript performs bit-operations.  For operations like addition and
   * multiplication, we split each number into 16-bit pieces, which can easily be
   * multiplied within Javascript's floating-point representation without overflow
   * or change in sign.
   *
   * In the algorithms below, we frequently reduce the negative case to the
   * positive case by negating the input(s) and then post-processing the result.
   * Note that we must ALWAYS check specially whether those values are MIN_VALUE
   * (-2^63) because -MIN_VALUE == MIN_VALUE (since 2^63 cannot be represented as
   * a positive number, it overflows back into a negative).  Not handling this
   * case would often result in infinite recursion.
   *
   * @param {number} low  The low (signed) 32 bits of the long.
   * @param {number} high  The high (signed) 32 bits of the long.
   * @constructor
   */
  goog.math.Long = function(low, high) {
    /**
     * @type {number}
     * @private
     */
    this.low_ = low | 0;  // force into 32 signed bits.
    /**
     * @type {number}
     * @private
     */
    this.high_ = high | 0;  // force into 32 signed bits.
  };
  // NOTE: Common constant values ZERO, ONE, NEG_ONE, etc. are defined below the
  // from* methods on which they depend.
  /**
   * A cache of the Long representations of small integer values.
   * @type {!Object}
   * @private
   */
  goog.math.Long.IntCache_ = {};
  /**
   * Returns a Long representing the given (32-bit) integer value.
   * @param {number} value The 32-bit integer in question.
   * @return {!goog.math.Long} The corresponding Long value.
   */
  goog.math.Long.fromInt = function(value) {
    if (-128 <= value && value < 128) {
      var cachedObj = goog.math.Long.IntCache_[value];
      if (cachedObj) {
        return cachedObj;
      }
    }
    var obj = new goog.math.Long(value | 0, value < 0 ? -1 : 0);
    if (-128 <= value && value < 128) {
      goog.math.Long.IntCache_[value] = obj;
    }
    return obj;
  };
  /**
   * Returns a Long representing the given value, provided that it is a finite
   * number.  Otherwise, zero is returned.
   * @param {number} value The number in question.
   * @return {!goog.math.Long} The corresponding Long value.
   */
  goog.math.Long.fromNumber = function(value) {
    if (isNaN(value) || !isFinite(value)) {
      return goog.math.Long.ZERO;
    } else if (value <= -goog.math.Long.TWO_PWR_63_DBL_) {
      return goog.math.Long.MIN_VALUE;
    } else if (value + 1 >= goog.math.Long.TWO_PWR_63_DBL_) {
      return goog.math.Long.MAX_VALUE;
    } else if (value < 0) {
      return goog.math.Long.fromNumber(-value).negate();
    } else {
      return new goog.math.Long(
          (value % goog.math.Long.TWO_PWR_32_DBL_) | 0,
          (value / goog.math.Long.TWO_PWR_32_DBL_) | 0);
    }
  };
  /**
   * Returns a Long representing the 64-bit integer that comes by concatenating
   * the given high and low bits.  Each is assumed to use 32 bits.
   * @param {number} lowBits The low 32-bits.
   * @param {number} highBits The high 32-bits.
   * @return {!goog.math.Long} The corresponding Long value.
   */
  goog.math.Long.fromBits = function(lowBits, highBits) {
    return new goog.math.Long(lowBits, highBits);
  };
  /**
   * Returns a Long representation of the given string, written using the given
   * radix.
   * @param {string} str The textual representation of the Long.
   * @param {number=} opt_radix The radix in which the text is written.
   * @return {!goog.math.Long} The corresponding Long value.
   */
  goog.math.Long.fromString = function(str, opt_radix) {
    if (str.length == 0) {
      throw Error('number format error: empty string');
    }
    var radix = opt_radix || 10;
    if (radix < 2 || 36 < radix) {
      throw Error('radix out of range: ' + radix);
    }
    if (str.charAt(0) == '-') {
      return goog.math.Long.fromString(str.substring(1), radix).negate();
    } else if (str.indexOf('-') >= 0) {
      throw Error('number format error: interior "-" character: ' + str);
    }
    // Do several (8) digits each time through the loop, so as to
    // minimize the calls to the very expensive emulated div.
    var radixToPower = goog.math.Long.fromNumber(Math.pow(radix, 8));
    var result = goog.math.Long.ZERO;
    for (var i = 0; i < str.length; i += 8) {
      var size = Math.min(8, str.length - i);
      var value = parseInt(str.substring(i, i + size), radix);
      if (size < 8) {
        var power = goog.math.Long.fromNumber(Math.pow(radix, size));
        result = result.multiply(power).add(goog.math.Long.fromNumber(value));
      } else {
        result = result.multiply(radixToPower);
        result = result.add(goog.math.Long.fromNumber(value));
      }
    }
    return result;
  };
  // NOTE: the compiler should inline these constant values below and then remove
  // these variables, so there should be no runtime penalty for these.
  /**
   * Number used repeated below in calculations.  This must appear before the
   * first call to any from* function below.
   * @type {number}
   * @private
   */
  goog.math.Long.TWO_PWR_16_DBL_ = 1 << 16;
  /**
   * @type {number}
   * @private
   */
  goog.math.Long.TWO_PWR_24_DBL_ = 1 << 24;
  /**
   * @type {number}
   * @private
   */
  goog.math.Long.TWO_PWR_32_DBL_ =
      goog.math.Long.TWO_PWR_16_DBL_ * goog.math.Long.TWO_PWR_16_DBL_;
  /**
   * @type {number}
   * @private
   */
  goog.math.Long.TWO_PWR_31_DBL_ =
      goog.math.Long.TWO_PWR_32_DBL_ / 2;
  /**
   * @type {number}
   * @private
   */
  goog.math.Long.TWO_PWR_48_DBL_ =
      goog.math.Long.TWO_PWR_32_DBL_ * goog.math.Long.TWO_PWR_16_DBL_;
  /**
   * @type {number}
   * @private
   */
  goog.math.Long.TWO_PWR_64_DBL_ =
      goog.math.Long.TWO_PWR_32_DBL_ * goog.math.Long.TWO_PWR_32_DBL_;
  /**
   * @type {number}
   * @private
   */
  goog.math.Long.TWO_PWR_63_DBL_ =
      goog.math.Long.TWO_PWR_64_DBL_ / 2;
  /** @type {!goog.math.Long} */
  goog.math.Long.ZERO = goog.math.Long.fromInt(0);
  /** @type {!goog.math.Long} */
  goog.math.Long.ONE = goog.math.Long.fromInt(1);
  /** @type {!goog.math.Long} */
  goog.math.Long.NEG_ONE = goog.math.Long.fromInt(-1);
  /** @type {!goog.math.Long} */
  goog.math.Long.MAX_VALUE =
      goog.math.Long.fromBits(0xFFFFFFFF | 0, 0x7FFFFFFF | 0);
  /** @type {!goog.math.Long} */
  goog.math.Long.MIN_VALUE = goog.math.Long.fromBits(0, 0x80000000 | 0);
  /**
   * @type {!goog.math.Long}
   * @private
   */
  goog.math.Long.TWO_PWR_24_ = goog.math.Long.fromInt(1 << 24);
  /** @return {number} The value, assuming it is a 32-bit integer. */
  goog.math.Long.prototype.toInt = function() {
    return this.low_;
  };
  /** @return {number} The closest floating-point representation to this value. */
  goog.math.Long.prototype.toNumber = function() {
    return this.high_ * goog.math.Long.TWO_PWR_32_DBL_ +
           this.getLowBitsUnsigned();
  };
  /**
   * @param {number=} opt_radix The radix in which the text should be written.
   * @return {string} The textual representation of this value.
   */
  goog.math.Long.prototype.toString = function(opt_radix) {
    var radix = opt_radix || 10;
    if (radix < 2 || 36 < radix) {
      throw Error('radix out of range: ' + radix);
    }
    if (this.isZero()) {
      return '0';
    }
    if (this.isNegative()) {
      if (this.equals(goog.math.Long.MIN_VALUE)) {
        // We need to change the Long value before it can be negated, so we remove
        // the bottom-most digit in this base and then recurse to do the rest.
        var radixLong = goog.math.Long.fromNumber(radix);
        var div = this.div(radixLong);
        var rem = div.multiply(radixLong).subtract(this);
        return div.toString(radix) + rem.toInt().toString(radix);
      } else {
        return '-' + this.negate().toString(radix);
      }
    }
    // Do several (6) digits each time through the loop, so as to
    // minimize the calls to the very expensive emulated div.
    var radixToPower = goog.math.Long.fromNumber(Math.pow(radix, 6));
    var rem = this;
    var result = '';
    while (true) {
      var remDiv = rem.div(radixToPower);
      var intval = rem.subtract(remDiv.multiply(radixToPower)).toInt();
      var digits = intval.toString(radix);
      rem = remDiv;
      if (rem.isZero()) {
        return digits + result;
      } else {
        while (digits.length < 6) {
          digits = '0' + digits;
        }
        result = '' + digits + result;
      }
    }
  };
  /** @return {number} The high 32-bits as a signed value. */
  goog.math.Long.prototype.getHighBits = function() {
    return this.high_;
  };
  /** @return {number} The low 32-bits as a signed value. */
  goog.math.Long.prototype.getLowBits = function() {
    return this.low_;
  };
  /** @return {number} The low 32-bits as an unsigned value. */
  goog.math.Long.prototype.getLowBitsUnsigned = function() {
    return (this.low_ >= 0) ?
        this.low_ : goog.math.Long.TWO_PWR_32_DBL_ + this.low_;
  };
  /**
   * @return {number} Returns the number of bits needed to represent the absolute
   *     value of this Long.
   */
  goog.math.Long.prototype.getNumBitsAbs = function() {
    if (this.isNegative()) {
      if (this.equals(goog.math.Long.MIN_VALUE)) {
        return 64;
      } else {
        return this.negate().getNumBitsAbs();
      }
    } else {
      var val = this.high_ != 0 ? this.high_ : this.low_;
      for (var bit = 31; bit > 0; bit--) {
        if ((val & (1 << bit)) != 0) {
          break;
        }
      }
      return this.high_ != 0 ? bit + 33 : bit + 1;
    }
  };
  /** @return {boolean} Whether this value is zero. */
  goog.math.Long.prototype.isZero = function() {
    return this.high_ == 0 && this.low_ == 0;
  };
  /** @return {boolean} Whether this value is negative. */
  goog.math.Long.prototype.isNegative = function() {
    return this.high_ < 0;
  };
  /** @return {boolean} Whether this value is odd. */
  goog.math.Long.prototype.isOdd = function() {
    return (this.low_ & 1) == 1;
  };
  /**
   * @param {goog.math.Long} other Long to compare against.
   * @return {boolean} Whether this Long equals the other.
   */
  goog.math.Long.prototype.equals = function(other) {
    return (this.high_ == other.high_) && (this.low_ == other.low_);
  };
  /**
   * @param {goog.math.Long} other Long to compare against.
   * @return {boolean} Whether this Long does not equal the other.
   */
  goog.math.Long.prototype.notEquals = function(other) {
    return (this.high_ != other.high_) || (this.low_ != other.low_);
  };
  /**
   * @param {goog.math.Long} other Long to compare against.
   * @return {boolean} Whether this Long is less than the other.
   */
  goog.math.Long.prototype.lessThan = function(other) {
    return this.compare(other) < 0;
  };
  /**
   * @param {goog.math.Long} other Long to compare against.
   * @return {boolean} Whether this Long is less than or equal to the other.
   */
  goog.math.Long.prototype.lessThanOrEqual = function(other) {
    return this.compare(other) <= 0;
  };
  /**
   * @param {goog.math.Long} other Long to compare against.
   * @return {boolean} Whether this Long is greater than the other.
   */
  goog.math.Long.prototype.greaterThan = function(other) {
    return this.compare(other) > 0;
  };
  /**
   * @param {goog.math.Long} other Long to compare against.
   * @return {boolean} Whether this Long is greater than or equal to the other.
   */
  goog.math.Long.prototype.greaterThanOrEqual = function(other) {
    return this.compare(other) >= 0;
  };
  /**
   * Compares this Long with the given one.
   * @param {goog.math.Long} other Long to compare against.
   * @return {number} 0 if they are the same, 1 if the this is greater, and -1
   *     if the given one is greater.
   */
  goog.math.Long.prototype.compare = function(other) {
    if (this.equals(other)) {
      return 0;
    }
    var thisNeg = this.isNegative();
    var otherNeg = other.isNegative();
    if (thisNeg && !otherNeg) {
      return -1;
    }
    if (!thisNeg && otherNeg) {
      return 1;
    }
    // at this point, the signs are the same, so subtraction will not overflow
    if (this.subtract(other).isNegative()) {
      return -1;
    } else {
      return 1;
    }
  };
  /** @return {!goog.math.Long} The negation of this value. */
  goog.math.Long.prototype.negate = function() {
    if (this.equals(goog.math.Long.MIN_VALUE)) {
      return goog.math.Long.MIN_VALUE;
    } else {
      return this.not().add(goog.math.Long.ONE);
    }
  };
  /**
   * Returns the sum of this and the given Long.
   * @param {goog.math.Long} other Long to add to this one.
   * @return {!goog.math.Long} The sum of this and the given Long.
   */
  goog.math.Long.prototype.add = function(other) {
    // Divide each number into 4 chunks of 16 bits, and then sum the chunks.
    var a48 = this.high_ >>> 16;
    var a32 = this.high_ & 0xFFFF;
    var a16 = this.low_ >>> 16;
    var a00 = this.low_ & 0xFFFF;
    var b48 = other.high_ >>> 16;
    var b32 = other.high_ & 0xFFFF;
    var b16 = other.low_ >>> 16;
    var b00 = other.low_ & 0xFFFF;
    var c48 = 0, c32 = 0, c16 = 0, c00 = 0;
    c00 += a00 + b00;
    c16 += c00 >>> 16;
    c00 &= 0xFFFF;
    c16 += a16 + b16;
    c32 += c16 >>> 16;
    c16 &= 0xFFFF;
    c32 += a32 + b32;
    c48 += c32 >>> 16;
    c32 &= 0xFFFF;
    c48 += a48 + b48;
    c48 &= 0xFFFF;
    return goog.math.Long.fromBits((c16 << 16) | c00, (c48 << 16) | c32);
  };
  /**
   * Returns the difference of this and the given Long.
   * @param {goog.math.Long} other Long to subtract from this.
   * @return {!goog.math.Long} The difference of this and the given Long.
   */
  goog.math.Long.prototype.subtract = function(other) {
    return this.add(other.negate());
  };
  /**
   * Returns the product of this and the given long.
   * @param {goog.math.Long} other Long to multiply with this.
   * @return {!goog.math.Long} The product of this and the other.
   */
  goog.math.Long.prototype.multiply = function(other) {
    if (this.isZero()) {
      return goog.math.Long.ZERO;
    } else if (other.isZero()) {
      return goog.math.Long.ZERO;
    }
    if (this.equals(goog.math.Long.MIN_VALUE)) {
      return other.isOdd() ? goog.math.Long.MIN_VALUE : goog.math.Long.ZERO;
    } else if (other.equals(goog.math.Long.MIN_VALUE)) {
      return this.isOdd() ? goog.math.Long.MIN_VALUE : goog.math.Long.ZERO;
    }
    if (this.isNegative()) {
      if (other.isNegative()) {
        return this.negate().multiply(other.negate());
      } else {
        return this.negate().multiply(other).negate();
      }
    } else if (other.isNegative()) {
      return this.multiply(other.negate()).negate();
    }
    // If both longs are small, use float multiplication
    if (this.lessThan(goog.math.Long.TWO_PWR_24_) &&
        other.lessThan(goog.math.Long.TWO_PWR_24_)) {
      return goog.math.Long.fromNumber(this.toNumber() * other.toNumber());
    }
    // Divide each long into 4 chunks of 16 bits, and then add up 4x4 products.
    // We can skip products that would overflow.
    var a48 = this.high_ >>> 16;
    var a32 = this.high_ & 0xFFFF;
    var a16 = this.low_ >>> 16;
    var a00 = this.low_ & 0xFFFF;
    var b48 = other.high_ >>> 16;
    var b32 = other.high_ & 0xFFFF;
    var b16 = other.low_ >>> 16;
    var b00 = other.low_ & 0xFFFF;
    var c48 = 0, c32 = 0, c16 = 0, c00 = 0;
    c00 += a00 * b00;
    c16 += c00 >>> 16;
    c00 &= 0xFFFF;
    c16 += a16 * b00;
    c32 += c16 >>> 16;
    c16 &= 0xFFFF;
    c16 += a00 * b16;
    c32 += c16 >>> 16;
    c16 &= 0xFFFF;
    c32 += a32 * b00;
    c48 += c32 >>> 16;
    c32 &= 0xFFFF;
    c32 += a16 * b16;
    c48 += c32 >>> 16;
    c32 &= 0xFFFF;
    c32 += a00 * b32;
    c48 += c32 >>> 16;
    c32 &= 0xFFFF;
    c48 += a48 * b00 + a32 * b16 + a16 * b32 + a00 * b48;
    c48 &= 0xFFFF;
    return goog.math.Long.fromBits((c16 << 16) | c00, (c48 << 16) | c32);
  };
  /**
   * Returns this Long divided by the given one.
   * @param {goog.math.Long} other Long by which to divide.
   * @return {!goog.math.Long} This Long divided by the given one.
   */
  goog.math.Long.prototype.div = function(other) {
    if (other.isZero()) {
      throw Error('division by zero');
    } else if (this.isZero()) {
      return goog.math.Long.ZERO;
    }
    if (this.equals(goog.math.Long.MIN_VALUE)) {
      if (other.equals(goog.math.Long.ONE) ||
          other.equals(goog.math.Long.NEG_ONE)) {
        return goog.math.Long.MIN_VALUE;  // recall that -MIN_VALUE == MIN_VALUE
      } else if (other.equals(goog.math.Long.MIN_VALUE)) {
        return goog.math.Long.ONE;
      } else {
        // At this point, we have |other| >= 2, so |this/other| < |MIN_VALUE|.
        var halfThis = this.shiftRight(1);
        var approx = halfThis.div(other).shiftLeft(1);
        if (approx.equals(goog.math.Long.ZERO)) {
          return other.isNegative() ? goog.math.Long.ONE : goog.math.Long.NEG_ONE;
        } else {
          var rem = this.subtract(other.multiply(approx));
          var result = approx.add(rem.div(other));
          return result;
        }
      }
    } else if (other.equals(goog.math.Long.MIN_VALUE)) {
      return goog.math.Long.ZERO;
    }
    if (this.isNegative()) {
      if (other.isNegative()) {
        return this.negate().div(other.negate());
      } else {
        return this.negate().div(other).negate();
      }
    } else if (other.isNegative()) {
      return this.div(other.negate()).negate();
    }
    // Repeat the following until the remainder is less than other:  find a
    // floating-point that approximates remainder / other *from below*, add this
    // into the result, and subtract it from the remainder.  It is critical that
    // the approximate value is less than or equal to the real value so that the
    // remainder never becomes negative.
    var res = goog.math.Long.ZERO;
    var rem = this;
    while (rem.greaterThanOrEqual(other)) {
      // Approximate the result of division. This may be a little greater or
      // smaller than the actual value.
      var approx = Math.max(1, Math.floor(rem.toNumber() / other.toNumber()));
      // We will tweak the approximate result by changing it in the 48-th digit or
      // the smallest non-fractional digit, whichever is larger.
      var log2 = Math.ceil(Math.log(approx) / Math.LN2);
      var delta = (log2 <= 48) ? 1 : Math.pow(2, log2 - 48);
      // Decrease the approximation until it is smaller than the remainder.  Note
      // that if it is too large, the product overflows and is negative.
      var approxRes = goog.math.Long.fromNumber(approx);
      var approxRem = approxRes.multiply(other);
      while (approxRem.isNegative() || approxRem.greaterThan(rem)) {
        approx -= delta;
        approxRes = goog.math.Long.fromNumber(approx);
        approxRem = approxRes.multiply(other);
      }
      // We know the answer can't be zero... and actually, zero would cause
      // infinite recursion since we would make no progress.
      if (approxRes.isZero()) {
        approxRes = goog.math.Long.ONE;
      }
      res = res.add(approxRes);
      rem = rem.subtract(approxRem);
    }
    return res;
  };
  /**
   * Returns this Long modulo the given one.
   * @param {goog.math.Long} other Long by which to mod.
   * @return {!goog.math.Long} This Long modulo the given one.
   */
  goog.math.Long.prototype.modulo = function(other) {
    return this.subtract(this.div(other).multiply(other));
  };
  /** @return {!goog.math.Long} The bitwise-NOT of this value. */
  goog.math.Long.prototype.not = function() {
    return goog.math.Long.fromBits(~this.low_, ~this.high_);
  };
  /**
   * Returns the bitwise-AND of this Long and the given one.
   * @param {goog.math.Long} other The Long with which to AND.
   * @return {!goog.math.Long} The bitwise-AND of this and the other.
   */
  goog.math.Long.prototype.and = function(other) {
    return goog.math.Long.fromBits(this.low_ & other.low_,
                                   this.high_ & other.high_);
  };
  /**
   * Returns the bitwise-OR of this Long and the given one.
   * @param {goog.math.Long} other The Long with which to OR.
   * @return {!goog.math.Long} The bitwise-OR of this and the other.
   */
  goog.math.Long.prototype.or = function(other) {
    return goog.math.Long.fromBits(this.low_ | other.low_,
                                   this.high_ | other.high_);
  };
  /**
   * Returns the bitwise-XOR of this Long and the given one.
   * @param {goog.math.Long} other The Long with which to XOR.
   * @return {!goog.math.Long} The bitwise-XOR of this and the other.
   */
  goog.math.Long.prototype.xor = function(other) {
    return goog.math.Long.fromBits(this.low_ ^ other.low_,
                                   this.high_ ^ other.high_);
  };
  /**
   * Returns this Long with bits shifted to the left by the given amount.
   * @param {number} numBits The number of bits by which to shift.
   * @return {!goog.math.Long} This shifted to the left by the given amount.
   */
  goog.math.Long.prototype.shiftLeft = function(numBits) {
    numBits &= 63;
    if (numBits == 0) {
      return this;
    } else {
      var low = this.low_;
      if (numBits < 32) {
        var high = this.high_;
        return goog.math.Long.fromBits(
            low << numBits,
            (high << numBits) | (low >>> (32 - numBits)));
      } else {
        return goog.math.Long.fromBits(0, low << (numBits - 32));
      }
    }
  };
  /**
   * Returns this Long with bits shifted to the right by the given amount.
   * @param {number} numBits The number of bits by which to shift.
   * @return {!goog.math.Long} This shifted to the right by the given amount.
   */
  goog.math.Long.prototype.shiftRight = function(numBits) {
    numBits &= 63;
    if (numBits == 0) {
      return this;
    } else {
      var high = this.high_;
      if (numBits < 32) {
        var low = this.low_;
        return goog.math.Long.fromBits(
            (low >>> numBits) | (high << (32 - numBits)),
            high >> numBits);
      } else {
        return goog.math.Long.fromBits(
            high >> (numBits - 32),
            high >= 0 ? 0 : -1);
      }
    }
  };
  /**
   * Returns this Long with bits shifted to the right by the given amount, with
   * the new top bits matching the current sign bit.
   * @param {number} numBits The number of bits by which to shift.
   * @return {!goog.math.Long} This shifted to the right by the given amount, with
   *     zeros placed into the new leading bits.
   */
  goog.math.Long.prototype.shiftRightUnsigned = function(numBits) {
    numBits &= 63;
    if (numBits == 0) {
      return this;
    } else {
      var high = this.high_;
      if (numBits < 32) {
        var low = this.low_;
        return goog.math.Long.fromBits(
            (low >>> numBits) | (high << (32 - numBits)),
            high >>> numBits);
      } else if (numBits == 32) {
        return goog.math.Long.fromBits(high, 0);
      } else {
        return goog.math.Long.fromBits(high >>> (numBits - 32), 0);
      }
    }
  };
  //======= begin jsbn =======
  var navigator = { appName: 'Modern Browser' }; // polyfill a little
  // Copyright (c) 2005  Tom Wu
  // All Rights Reserved.
  // http://www-cs-students.stanford.edu/~tjw/jsbn/
  /*
   * Copyright (c) 2003-2005  Tom Wu
   * All Rights Reserved.
   *
   * Permission is hereby granted, free of charge, to any person obtaining
   * a copy of this software and associated documentation files (the
   * "Software"), to deal in the Software without restriction, including
   * without limitation the rights to use, copy, modify, merge, publish,
   * distribute, sublicense, and/or sell copies of the Software, and to
   * permit persons to whom the Software is furnished to do so, subject to
   * the following conditions:
   *
   * The above copyright notice and this permission notice shall be
   * included in all copies or substantial portions of the Software.
   *
   * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND, 
   * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY 
   * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  
   *
   * IN NO EVENT SHALL TOM WU BE LIABLE FOR ANY SPECIAL, INCIDENTAL,
   * INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND, OR ANY DAMAGES WHATSOEVER
   * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER OR NOT ADVISED OF
   * THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF LIABILITY, ARISING OUT
   * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
   *
   * In addition, the following condition applies:
   *
   * All redistributions must retain an intact copy of this copyright notice
   * and disclaimer.
   */
  // Basic JavaScript BN library - subset useful for RSA encryption.
  // Bits per digit
  var dbits;
  // JavaScript engine analysis
  var canary = 0xdeadbeefcafe;
  var j_lm = ((canary&0xffffff)==0xefcafe);
  // (public) Constructor
  function BigInteger(a,b,c) {
    if(a != null)
      if("number" == typeof a) this.fromNumber(a,b,c);
      else if(b == null && "string" != typeof a) this.fromString(a,256);
      else this.fromString(a,b);
  }
  // return new, unset BigInteger
  function nbi() { return new BigInteger(null); }
  // am: Compute w_j += (x*this_i), propagate carries,
  // c is initial carry, returns final carry.
  // c < 3*dvalue, x < 2*dvalue, this_i < dvalue
  // We need to select the fastest one that works in this environment.
  // am1: use a single mult and divide to get the high bits,
  // max digit bits should be 26 because
  // max internal value = 2*dvalue^2-2*dvalue (< 2^53)
  function am1(i,x,w,j,c,n) {
    while(--n >= 0) {
      var v = x*this[i++]+w[j]+c;
      c = Math.floor(v/0x4000000);
      w[j++] = v&0x3ffffff;
    }
    return c;
  }
  // am2 avoids a big mult-and-extract completely.
  // Max digit bits should be <= 30 because we do bitwise ops
  // on values up to 2*hdvalue^2-hdvalue-1 (< 2^31)
  function am2(i,x,w,j,c,n) {
    var xl = x&0x7fff, xh = x>>15;
    while(--n >= 0) {
      var l = this[i]&0x7fff;
      var h = this[i++]>>15;
      var m = xh*l+h*xl;
      l = xl*l+((m&0x7fff)<<15)+w[j]+(c&0x3fffffff);
      c = (l>>>30)+(m>>>15)+xh*h+(c>>>30);
      w[j++] = l&0x3fffffff;
    }
    return c;
  }
  // Alternately, set max digit bits to 28 since some
  // browsers slow down when dealing with 32-bit numbers.
  function am3(i,x,w,j,c,n) {
    var xl = x&0x3fff, xh = x>>14;
    while(--n >= 0) {
      var l = this[i]&0x3fff;
      var h = this[i++]>>14;
      var m = xh*l+h*xl;
      l = xl*l+((m&0x3fff)<<14)+w[j]+c;
      c = (l>>28)+(m>>14)+xh*h;
      w[j++] = l&0xfffffff;
    }
    return c;
  }
  if(j_lm && (navigator.appName == "Microsoft Internet Explorer")) {
    BigInteger.prototype.am = am2;
    dbits = 30;
  }
  else if(j_lm && (navigator.appName != "Netscape")) {
    BigInteger.prototype.am = am1;
    dbits = 26;
  }
  else { // Mozilla/Netscape seems to prefer am3
    BigInteger.prototype.am = am3;
    dbits = 28;
  }
  BigInteger.prototype.DB = dbits;
  BigInteger.prototype.DM = ((1<<dbits)-1);
  BigInteger.prototype.DV = (1<<dbits);
  var BI_FP = 52;
  BigInteger.prototype.FV = Math.pow(2,BI_FP);
  BigInteger.prototype.F1 = BI_FP-dbits;
  BigInteger.prototype.F2 = 2*dbits-BI_FP;
  // Digit conversions
  var BI_RM = "0123456789abcdefghijklmnopqrstuvwxyz";
  var BI_RC = new Array();
  var rr,vv;
  rr = "0".charCodeAt(0);
  for(vv = 0; vv <= 9; ++vv) BI_RC[rr++] = vv;
  rr = "a".charCodeAt(0);
  for(vv = 10; vv < 36; ++vv) BI_RC[rr++] = vv;
  rr = "A".charCodeAt(0);
  for(vv = 10; vv < 36; ++vv) BI_RC[rr++] = vv;
  function int2char(n) { return BI_RM.charAt(n); }
  function intAt(s,i) {
    var c = BI_RC[s.charCodeAt(i)];
    return (c==null)?-1:c;
  }
  // (protected) copy this to r
  function bnpCopyTo(r) {
    for(var i = this.t-1; i >= 0; --i) r[i] = this[i];
    r.t = this.t;
    r.s = this.s;
  }
  // (protected) set from integer value x, -DV <= x < DV
  function bnpFromInt(x) {
    this.t = 1;
    this.s = (x<0)?-1:0;
    if(x > 0) this[0] = x;
    else if(x < -1) this[0] = x+DV;
    else this.t = 0;
  }
  // return bigint initialized to value
  function nbv(i) { var r = nbi(); r.fromInt(i); return r; }
  // (protected) set from string and radix
  function bnpFromString(s,b) {
    var k;
    if(b == 16) k = 4;
    else if(b == 8) k = 3;
    else if(b == 256) k = 8; // byte array
    else if(b == 2) k = 1;
    else if(b == 32) k = 5;
    else if(b == 4) k = 2;
    else { this.fromRadix(s,b); return; }
    this.t = 0;
    this.s = 0;
    var i = s.length, mi = false, sh = 0;
    while(--i >= 0) {
      var x = (k==8)?s[i]&0xff:intAt(s,i);
      if(x < 0) {
        if(s.charAt(i) == "-") mi = true;
        continue;
      }
      mi = false;
      if(sh == 0)
        this[this.t++] = x;
      else if(sh+k > this.DB) {
        this[this.t-1] |= (x&((1<<(this.DB-sh))-1))<<sh;
        this[this.t++] = (x>>(this.DB-sh));
      }
      else
        this[this.t-1] |= x<<sh;
      sh += k;
      if(sh >= this.DB) sh -= this.DB;
    }
    if(k == 8 && (s[0]&0x80) != 0) {
      this.s = -1;
      if(sh > 0) this[this.t-1] |= ((1<<(this.DB-sh))-1)<<sh;
    }
    this.clamp();
    if(mi) BigInteger.ZERO.subTo(this,this);
  }
  // (protected) clamp off excess high words
  function bnpClamp() {
    var c = this.s&this.DM;
    while(this.t > 0 && this[this.t-1] == c) --this.t;
  }
  // (public) return string representation in given radix
  function bnToString(b) {
    if(this.s < 0) return "-"+this.negate().toString(b);
    var k;
    if(b == 16) k = 4;
    else if(b == 8) k = 3;
    else if(b == 2) k = 1;
    else if(b == 32) k = 5;
    else if(b == 4) k = 2;
    else return this.toRadix(b);
    var km = (1<<k)-1, d, m = false, r = "", i = this.t;
    var p = this.DB-(i*this.DB)%k;
    if(i-- > 0) {
      if(p < this.DB && (d = this[i]>>p) > 0) { m = true; r = int2char(d); }
      while(i >= 0) {
        if(p < k) {
          d = (this[i]&((1<<p)-1))<<(k-p);
          d |= this[--i]>>(p+=this.DB-k);
        }
        else {
          d = (this[i]>>(p-=k))&km;
          if(p <= 0) { p += this.DB; --i; }
        }
        if(d > 0) m = true;
        if(m) r += int2char(d);
      }
    }
    return m?r:"0";
  }
  // (public) -this
  function bnNegate() { var r = nbi(); BigInteger.ZERO.subTo(this,r); return r; }
  // (public) |this|
  function bnAbs() { return (this.s<0)?this.negate():this; }
  // (public) return + if this > a, - if this < a, 0 if equal
  function bnCompareTo(a) {
    var r = this.s-a.s;
    if(r != 0) return r;
    var i = this.t;
    r = i-a.t;
    if(r != 0) return (this.s<0)?-r:r;
    while(--i >= 0) if((r=this[i]-a[i]) != 0) return r;
    return 0;
  }
  // returns bit length of the integer x
  function nbits(x) {
    var r = 1, t;
    if((t=x>>>16) != 0) { x = t; r += 16; }
    if((t=x>>8) != 0) { x = t; r += 8; }
    if((t=x>>4) != 0) { x = t; r += 4; }
    if((t=x>>2) != 0) { x = t; r += 2; }
    if((t=x>>1) != 0) { x = t; r += 1; }
    return r;
  }
  // (public) return the number of bits in "this"
  function bnBitLength() {
    if(this.t <= 0) return 0;
    return this.DB*(this.t-1)+nbits(this[this.t-1]^(this.s&this.DM));
  }
  // (protected) r = this << n*DB
  function bnpDLShiftTo(n,r) {
    var i;
    for(i = this.t-1; i >= 0; --i) r[i+n] = this[i];
    for(i = n-1; i >= 0; --i) r[i] = 0;
    r.t = this.t+n;
    r.s = this.s;
  }
  // (protected) r = this >> n*DB
  function bnpDRShiftTo(n,r) {
    for(var i = n; i < this.t; ++i) r[i-n] = this[i];
    r.t = Math.max(this.t-n,0);
    r.s = this.s;
  }
  // (protected) r = this << n
  function bnpLShiftTo(n,r) {
    var bs = n%this.DB;
    var cbs = this.DB-bs;
    var bm = (1<<cbs)-1;
    var ds = Math.floor(n/this.DB), c = (this.s<<bs)&this.DM, i;
    for(i = this.t-1; i >= 0; --i) {
      r[i+ds+1] = (this[i]>>cbs)|c;
      c = (this[i]&bm)<<bs;
    }
    for(i = ds-1; i >= 0; --i) r[i] = 0;
    r[ds] = c;
    r.t = this.t+ds+1;
    r.s = this.s;
    r.clamp();
  }
  // (protected) r = this >> n
  function bnpRShiftTo(n,r) {
    r.s = this.s;
    var ds = Math.floor(n/this.DB);
    if(ds >= this.t) { r.t = 0; return; }
    var bs = n%this.DB;
    var cbs = this.DB-bs;
    var bm = (1<<bs)-1;
    r[0] = this[ds]>>bs;
    for(var i = ds+1; i < this.t; ++i) {
      r[i-ds-1] |= (this[i]&bm)<<cbs;
      r[i-ds] = this[i]>>bs;
    }
    if(bs > 0) r[this.t-ds-1] |= (this.s&bm)<<cbs;
    r.t = this.t-ds;
    r.clamp();
  }
  // (protected) r = this - a
  function bnpSubTo(a,r) {
    var i = 0, c = 0, m = Math.min(a.t,this.t);
    while(i < m) {
      c += this[i]-a[i];
      r[i++] = c&this.DM;
      c >>= this.DB;
    }
    if(a.t < this.t) {
      c -= a.s;
      while(i < this.t) {
        c += this[i];
        r[i++] = c&this.DM;
        c >>= this.DB;
      }
      c += this.s;
    }
    else {
      c += this.s;
      while(i < a.t) {
        c -= a[i];
        r[i++] = c&this.DM;
        c >>= this.DB;
      }
      c -= a.s;
    }
    r.s = (c<0)?-1:0;
    if(c < -1) r[i++] = this.DV+c;
    else if(c > 0) r[i++] = c;
    r.t = i;
    r.clamp();
  }
  // (protected) r = this * a, r != this,a (HAC 14.12)
  // "this" should be the larger one if appropriate.
  function bnpMultiplyTo(a,r) {
    var x = this.abs(), y = a.abs();
    var i = x.t;
    r.t = i+y.t;
    while(--i >= 0) r[i] = 0;
    for(i = 0; i < y.t; ++i) r[i+x.t] = x.am(0,y[i],r,i,0,x.t);
    r.s = 0;
    r.clamp();
    if(this.s != a.s) BigInteger.ZERO.subTo(r,r);
  }
  // (protected) r = this^2, r != this (HAC 14.16)
  function bnpSquareTo(r) {
    var x = this.abs();
    var i = r.t = 2*x.t;
    while(--i >= 0) r[i] = 0;
    for(i = 0; i < x.t-1; ++i) {
      var c = x.am(i,x[i],r,2*i,0,1);
      if((r[i+x.t]+=x.am(i+1,2*x[i],r,2*i+1,c,x.t-i-1)) >= x.DV) {
        r[i+x.t] -= x.DV;
        r[i+x.t+1] = 1;
      }
    }
    if(r.t > 0) r[r.t-1] += x.am(i,x[i],r,2*i,0,1);
    r.s = 0;
    r.clamp();
  }
  // (protected) divide this by m, quotient and remainder to q, r (HAC 14.20)
  // r != q, this != m.  q or r may be null.
  function bnpDivRemTo(m,q,r) {
    var pm = m.abs();
    if(pm.t <= 0) return;
    var pt = this.abs();
    if(pt.t < pm.t) {
      if(q != null) q.fromInt(0);
      if(r != null) this.copyTo(r);
      return;
    }
    if(r == null) r = nbi();
    var y = nbi(), ts = this.s, ms = m.s;
    var nsh = this.DB-nbits(pm[pm.t-1]);	// normalize modulus
    if(nsh > 0) { pm.lShiftTo(nsh,y); pt.lShiftTo(nsh,r); }
    else { pm.copyTo(y); pt.copyTo(r); }
    var ys = y.t;
    var y0 = y[ys-1];
    if(y0 == 0) return;
    var yt = y0*(1<<this.F1)+((ys>1)?y[ys-2]>>this.F2:0);
    var d1 = this.FV/yt, d2 = (1<<this.F1)/yt, e = 1<<this.F2;
    var i = r.t, j = i-ys, t = (q==null)?nbi():q;
    y.dlShiftTo(j,t);
    if(r.compareTo(t) >= 0) {
      r[r.t++] = 1;
      r.subTo(t,r);
    }
    BigInteger.ONE.dlShiftTo(ys,t);
    t.subTo(y,y);	// "negative" y so we can replace sub with am later
    while(y.t < ys) y[y.t++] = 0;
    while(--j >= 0) {
      // Estimate quotient digit
      var qd = (r[--i]==y0)?this.DM:Math.floor(r[i]*d1+(r[i-1]+e)*d2);
      if((r[i]+=y.am(0,qd,r,j,0,ys)) < qd) {	// Try it out
        y.dlShiftTo(j,t);
        r.subTo(t,r);
        while(r[i] < --qd) r.subTo(t,r);
      }
    }
    if(q != null) {
      r.drShiftTo(ys,q);
      if(ts != ms) BigInteger.ZERO.subTo(q,q);
    }
    r.t = ys;
    r.clamp();
    if(nsh > 0) r.rShiftTo(nsh,r);	// Denormalize remainder
    if(ts < 0) BigInteger.ZERO.subTo(r,r);
  }
  // (public) this mod a
  function bnMod(a) {
    var r = nbi();
    this.abs().divRemTo(a,null,r);
    if(this.s < 0 && r.compareTo(BigInteger.ZERO) > 0) a.subTo(r,r);
    return r;
  }
  // Modular reduction using "classic" algorithm
  function Classic(m) { this.m = m; }
  function cConvert(x) {
    if(x.s < 0 || x.compareTo(this.m) >= 0) return x.mod(this.m);
    else return x;
  }
  function cRevert(x) { return x; }
  function cReduce(x) { x.divRemTo(this.m,null,x); }
  function cMulTo(x,y,r) { x.multiplyTo(y,r); this.reduce(r); }
  function cSqrTo(x,r) { x.squareTo(r); this.reduce(r); }
  Classic.prototype.convert = cConvert;
  Classic.prototype.revert = cRevert;
  Classic.prototype.reduce = cReduce;
  Classic.prototype.mulTo = cMulTo;
  Classic.prototype.sqrTo = cSqrTo;
  // (protected) return "-1/this % 2^DB"; useful for Mont. reduction
  // justification:
  //         xy == 1 (mod m)
  //         xy =  1+km
  //   xy(2-xy) = (1+km)(1-km)
  // x[y(2-xy)] = 1-k^2m^2
  // x[y(2-xy)] == 1 (mod m^2)
  // if y is 1/x mod m, then y(2-xy) is 1/x mod m^2
  // should reduce x and y(2-xy) by m^2 at each step to keep size bounded.
  // JS multiply "overflows" differently from C/C++, so care is needed here.
  function bnpInvDigit() {
    if(this.t < 1) return 0;
    var x = this[0];
    if((x&1) == 0) return 0;
    var y = x&3;		// y == 1/x mod 2^2
    y = (y*(2-(x&0xf)*y))&0xf;	// y == 1/x mod 2^4
    y = (y*(2-(x&0xff)*y))&0xff;	// y == 1/x mod 2^8
    y = (y*(2-(((x&0xffff)*y)&0xffff)))&0xffff;	// y == 1/x mod 2^16
    // last step - calculate inverse mod DV directly;
    // assumes 16 < DB <= 32 and assumes ability to handle 48-bit ints
    y = (y*(2-x*y%this.DV))%this.DV;		// y == 1/x mod 2^dbits
    // we really want the negative inverse, and -DV < y < DV
    return (y>0)?this.DV-y:-y;
  }
  // Montgomery reduction
  function Montgomery(m) {
    this.m = m;
    this.mp = m.invDigit();
    this.mpl = this.mp&0x7fff;
    this.mph = this.mp>>15;
    this.um = (1<<(m.DB-15))-1;
    this.mt2 = 2*m.t;
  }
  // xR mod m
  function montConvert(x) {
    var r = nbi();
    x.abs().dlShiftTo(this.m.t,r);
    r.divRemTo(this.m,null,r);
    if(x.s < 0 && r.compareTo(BigInteger.ZERO) > 0) this.m.subTo(r,r);
    return r;
  }
  // x/R mod m
  function montRevert(x) {
    var r = nbi();
    x.copyTo(r);
    this.reduce(r);
    return r;
  }
  // x = x/R mod m (HAC 14.32)
  function montReduce(x) {
    while(x.t <= this.mt2)	// pad x so am has enough room later
      x[x.t++] = 0;
    for(var i = 0; i < this.m.t; ++i) {
      // faster way of calculating u0 = x[i]*mp mod DV
      var j = x[i]&0x7fff;
      var u0 = (j*this.mpl+(((j*this.mph+(x[i]>>15)*this.mpl)&this.um)<<15))&x.DM;
      // use am to combine the multiply-shift-add into one call
      j = i+this.m.t;
      x[j] += this.m.am(0,u0,x,i,0,this.m.t);
      // propagate carry
      while(x[j] >= x.DV) { x[j] -= x.DV; x[++j]++; }
    }
    x.clamp();
    x.drShiftTo(this.m.t,x);
    if(x.compareTo(this.m) >= 0) x.subTo(this.m,x);
  }
  // r = "x^2/R mod m"; x != r
  function montSqrTo(x,r) { x.squareTo(r); this.reduce(r); }
  // r = "xy/R mod m"; x,y != r
  function montMulTo(x,y,r) { x.multiplyTo(y,r); this.reduce(r); }
  Montgomery.prototype.convert = montConvert;
  Montgomery.prototype.revert = montRevert;
  Montgomery.prototype.reduce = montReduce;
  Montgomery.prototype.mulTo = montMulTo;
  Montgomery.prototype.sqrTo = montSqrTo;
  // (protected) true iff this is even
  function bnpIsEven() { return ((this.t>0)?(this[0]&1):this.s) == 0; }
  // (protected) this^e, e < 2^32, doing sqr and mul with "r" (HAC 14.79)
  function bnpExp(e,z) {
    if(e > 0xffffffff || e < 1) return BigInteger.ONE;
    var r = nbi(), r2 = nbi(), g = z.convert(this), i = nbits(e)-1;
    g.copyTo(r);
    while(--i >= 0) {
      z.sqrTo(r,r2);
      if((e&(1<<i)) > 0) z.mulTo(r2,g,r);
      else { var t = r; r = r2; r2 = t; }
    }
    return z.revert(r);
  }
  // (public) this^e % m, 0 <= e < 2^32
  function bnModPowInt(e,m) {
    var z;
    if(e < 256 || m.isEven()) z = new Classic(m); else z = new Montgomery(m);
    return this.exp(e,z);
  }
  // protected
  BigInteger.prototype.copyTo = bnpCopyTo;
  BigInteger.prototype.fromInt = bnpFromInt;
  BigInteger.prototype.fromString = bnpFromString;
  BigInteger.prototype.clamp = bnpClamp;
  BigInteger.prototype.dlShiftTo = bnpDLShiftTo;
  BigInteger.prototype.drShiftTo = bnpDRShiftTo;
  BigInteger.prototype.lShiftTo = bnpLShiftTo;
  BigInteger.prototype.rShiftTo = bnpRShiftTo;
  BigInteger.prototype.subTo = bnpSubTo;
  BigInteger.prototype.multiplyTo = bnpMultiplyTo;
  BigInteger.prototype.squareTo = bnpSquareTo;
  BigInteger.prototype.divRemTo = bnpDivRemTo;
  BigInteger.prototype.invDigit = bnpInvDigit;
  BigInteger.prototype.isEven = bnpIsEven;
  BigInteger.prototype.exp = bnpExp;
  // public
  BigInteger.prototype.toString = bnToString;
  BigInteger.prototype.negate = bnNegate;
  BigInteger.prototype.abs = bnAbs;
  BigInteger.prototype.compareTo = bnCompareTo;
  BigInteger.prototype.bitLength = bnBitLength;
  BigInteger.prototype.mod = bnMod;
  BigInteger.prototype.modPowInt = bnModPowInt;
  // "constants"
  BigInteger.ZERO = nbv(0);
  BigInteger.ONE = nbv(1);
  // jsbn2 stuff
  // (protected) convert from radix string
  function bnpFromRadix(s,b) {
    this.fromInt(0);
    if(b == null) b = 10;
    var cs = this.chunkSize(b);
    var d = Math.pow(b,cs), mi = false, j = 0, w = 0;
    for(var i = 0; i < s.length; ++i) {
      var x = intAt(s,i);
      if(x < 0) {
        if(s.charAt(i) == "-" && this.signum() == 0) mi = true;
        continue;
      }
      w = b*w+x;
      if(++j >= cs) {
        this.dMultiply(d);
        this.dAddOffset(w,0);
        j = 0;
        w = 0;
      }
    }
    if(j > 0) {
      this.dMultiply(Math.pow(b,j));
      this.dAddOffset(w,0);
    }
    if(mi) BigInteger.ZERO.subTo(this,this);
  }
  // (protected) return x s.t. r^x < DV
  function bnpChunkSize(r) { return Math.floor(Math.LN2*this.DB/Math.log(r)); }
  // (public) 0 if this == 0, 1 if this > 0
  function bnSigNum() {
    if(this.s < 0) return -1;
    else if(this.t <= 0 || (this.t == 1 && this[0] <= 0)) return 0;
    else return 1;
  }
  // (protected) this *= n, this >= 0, 1 < n < DV
  function bnpDMultiply(n) {
    this[this.t] = this.am(0,n-1,this,0,0,this.t);
    ++this.t;
    this.clamp();
  }
  // (protected) this += n << w words, this >= 0
  function bnpDAddOffset(n,w) {
    if(n == 0) return;
    while(this.t <= w) this[this.t++] = 0;
    this[w] += n;
    while(this[w] >= this.DV) {
      this[w] -= this.DV;
      if(++w >= this.t) this[this.t++] = 0;
      ++this[w];
    }
  }
  // (protected) convert to radix string
  function bnpToRadix(b) {
    if(b == null) b = 10;
    if(this.signum() == 0 || b < 2 || b > 36) return "0";
    var cs = this.chunkSize(b);
    var a = Math.pow(b,cs);
    var d = nbv(a), y = nbi(), z = nbi(), r = "";
    this.divRemTo(d,y,z);
    while(y.signum() > 0) {
      r = (a+z.intValue()).toString(b).substr(1) + r;
      y.divRemTo(d,y,z);
    }
    return z.intValue().toString(b) + r;
  }
  // (public) return value as integer
  function bnIntValue() {
    if(this.s < 0) {
      if(this.t == 1) return this[0]-this.DV;
      else if(this.t == 0) return -1;
    }
    else if(this.t == 1) return this[0];
    else if(this.t == 0) return 0;
    // assumes 16 < DB < 32
    return ((this[1]&((1<<(32-this.DB))-1))<<this.DB)|this[0];
  }
  // (protected) r = this + a
  function bnpAddTo(a,r) {
    var i = 0, c = 0, m = Math.min(a.t,this.t);
    while(i < m) {
      c += this[i]+a[i];
      r[i++] = c&this.DM;
      c >>= this.DB;
    }
    if(a.t < this.t) {
      c += a.s;
      while(i < this.t) {
        c += this[i];
        r[i++] = c&this.DM;
        c >>= this.DB;
      }
      c += this.s;
    }
    else {
      c += this.s;
      while(i < a.t) {
        c += a[i];
        r[i++] = c&this.DM;
        c >>= this.DB;
      }
      c += a.s;
    }
    r.s = (c<0)?-1:0;
    if(c > 0) r[i++] = c;
    else if(c < -1) r[i++] = this.DV+c;
    r.t = i;
    r.clamp();
  }
  BigInteger.prototype.fromRadix = bnpFromRadix;
  BigInteger.prototype.chunkSize = bnpChunkSize;
  BigInteger.prototype.signum = bnSigNum;
  BigInteger.prototype.dMultiply = bnpDMultiply;
  BigInteger.prototype.dAddOffset = bnpDAddOffset;
  BigInteger.prototype.toRadix = bnpToRadix;
  BigInteger.prototype.intValue = bnIntValue;
  BigInteger.prototype.addTo = bnpAddTo;
  //======= end jsbn =======
  // Emscripten wrapper
  var Wrapper = {
    abs: function(l, h) {
      var x = new goog.math.Long(l, h);
      var ret;
      if (x.isNegative()) {
        ret = x.negate();
      } else {
        ret = x;
      }
      HEAP32[tempDoublePtr>>2] = ret.low_;
      HEAP32[tempDoublePtr+4>>2] = ret.high_;
    },
    ensureTemps: function() {
      if (Wrapper.ensuredTemps) return;
      Wrapper.ensuredTemps = true;
      Wrapper.two32 = new BigInteger();
      Wrapper.two32.fromString('4294967296', 10);
      Wrapper.two64 = new BigInteger();
      Wrapper.two64.fromString('18446744073709551616', 10);
      Wrapper.temp1 = new BigInteger();
      Wrapper.temp2 = new BigInteger();
    },
    lh2bignum: function(l, h) {
      var a = new BigInteger();
      a.fromString(h.toString(), 10);
      var b = new BigInteger();
      a.multiplyTo(Wrapper.two32, b);
      var c = new BigInteger();
      c.fromString(l.toString(), 10);
      var d = new BigInteger();
      c.addTo(b, d);
      return d;
    },
    stringify: function(l, h, unsigned) {
      var ret = new goog.math.Long(l, h).toString();
      if (unsigned && ret[0] == '-') {
        // unsign slowly using jsbn bignums
        Wrapper.ensureTemps();
        var bignum = new BigInteger();
        bignum.fromString(ret, 10);
        ret = new BigInteger();
        Wrapper.two64.addTo(bignum, ret);
        ret = ret.toString(10);
      }
      return ret;
    },
    fromString: function(str, base, min, max, unsigned) {
      Wrapper.ensureTemps();
      var bignum = new BigInteger();
      bignum.fromString(str, base);
      var bigmin = new BigInteger();
      bigmin.fromString(min, 10);
      var bigmax = new BigInteger();
      bigmax.fromString(max, 10);
      if (unsigned && bignum.compareTo(BigInteger.ZERO) < 0) {
        var temp = new BigInteger();
        bignum.addTo(Wrapper.two64, temp);
        bignum = temp;
      }
      var error = false;
      if (bignum.compareTo(bigmin) < 0) {
        bignum = bigmin;
        error = true;
      } else if (bignum.compareTo(bigmax) > 0) {
        bignum = bigmax;
        error = true;
      }
      var ret = goog.math.Long.fromString(bignum.toString()); // min-max checks should have clamped this to a range goog.math.Long can handle well
      HEAP32[tempDoublePtr>>2] = ret.low_;
      HEAP32[tempDoublePtr+4>>2] = ret.high_;
      if (error) throw 'range error';
    }
  };
  return Wrapper;
})();
//======= end closure i64 code =======
// === Auto-generated postamble setup entry stuff ===
if (memoryInitializer) {
  function applyData(data) {
    HEAPU8.set(data, STATIC_BASE);
  }
  if (ENVIRONMENT_IS_NODE || ENVIRONMENT_IS_SHELL) {
    applyData(Module['readBinary'](memoryInitializer));
  } else {
    addRunDependency('memory initializer');
    Browser.asyncLoad(memoryInitializer, function(data) {
      applyData(data);
      removeRunDependency('memory initializer');
    }, function(data) {
      throw 'could not load memory initializer ' + memoryInitializer;
    });
  }
}
function ExitStatus(status) {
  this.name = "ExitStatus";
  this.message = "Program terminated with exit(" + status + ")";
  this.status = status;
};
ExitStatus.prototype = new Error();
ExitStatus.prototype.constructor = ExitStatus;
var initialStackTop;
var preloadStartTime = null;
var calledMain = false;
dependenciesFulfilled = function runCaller() {
  // If run has never been called, and we should call run (INVOKE_RUN is true, and Module.noInitialRun is not false)
  if (!Module['calledRun'] && shouldRunNow) run();
  if (!Module['calledRun']) dependenciesFulfilled = runCaller; // try this again later, after new deps are fulfilled
}
Module['callMain'] = Module.callMain = function callMain(args) {
  assert(runDependencies == 0, 'cannot call main when async dependencies remain! (listen on __ATMAIN__)');
  assert(__ATPRERUN__.length == 0, 'cannot call main when preRun functions remain to be called');
  args = args || [];
  if (ENVIRONMENT_IS_WEB && preloadStartTime !== null) {
    Module.printErr('preload time: ' + (Date.now() - preloadStartTime) + ' ms');
  }
  ensureInitRuntime();
  var argc = args.length+1;
  function pad() {
    for (var i = 0; i < 4-1; i++) {
      argv.push(0);
    }
  }
  var argv = [allocate(intArrayFromString("/bin/this.program"), 'i8', ALLOC_NORMAL) ];
  pad();
  for (var i = 0; i < argc-1; i = i + 1) {
    argv.push(allocate(intArrayFromString(args[i]), 'i8', ALLOC_NORMAL));
    pad();
  }
  argv.push(0);
  argv = allocate(argv, 'i32', ALLOC_NORMAL);
  initialStackTop = STACKTOP;
  try {
    var ret = Module['_main'](argc, argv, 0);
    // if we're not running an evented main loop, it's time to exit
    if (!Module['noExitRuntime']) {
      exit(ret);
    }
  }
  catch(e) {
    if (e instanceof ExitStatus) {
      // exit() throws this once it's done to make sure execution
      // has been stopped completely
      return;
    } else if (e == 'SimulateInfiniteLoop') {
      // running an evented main loop, don't immediately exit
      Module['noExitRuntime'] = true;
      return;
    } else {
      if (e && typeof e === 'object' && e.stack) Module.printErr('exception thrown: ' + [e, e.stack]);
      throw e;
    }
  } finally {
    calledMain = true;
  }
}
function run(args) {
  args = args || Module['arguments'];
  if (preloadStartTime === null) preloadStartTime = Date.now();
  if (runDependencies > 0) {
    Module.printErr('run() called, but dependencies remain, so not running');
    return;
  }
  preRun();
  if (runDependencies > 0) return; // a preRun added a dependency, run will be called later
  if (Module['calledRun']) return; // run may have just been called through dependencies being fulfilled just in this very frame
  function doRun() {
    if (Module['calledRun']) return; // run may have just been called while the async setStatus time below was happening
    Module['calledRun'] = true;
    ensureInitRuntime();
    preMain();
    if (Module['_main'] && shouldRunNow) {
      Module['callMain'](args);
    }
    postRun();
  }
  if (Module['setStatus']) {
    Module['setStatus']('Running...');
    setTimeout(function() {
      setTimeout(function() {
        Module['setStatus']('');
      }, 1);
      if (!ABORT) doRun();
    }, 1);
  } else {
    doRun();
  }
}
Module['run'] = Module.run = run;
function exit(status) {
  ABORT = true;
  EXITSTATUS = status;
  STACKTOP = initialStackTop;
  // exit the runtime
  exitRuntime();
  // TODO We should handle this differently based on environment.
  // In the browser, the best we can do is throw an exception
  // to halt execution, but in node we could process.exit and
  // I'd imagine SM shell would have something equivalent.
  // This would let us set a proper exit status (which
  // would be great for checking test exit statuses).
  // https://github.com/kripken/emscripten/issues/1371
  // throw an exception to halt the current execution
  throw new ExitStatus(status);
}
Module['exit'] = Module.exit = exit;
function abort(text) {
  if (text) {
    Module.print(text);
    Module.printErr(text);
  }
  ABORT = true;
  EXITSTATUS = 1;
  throw 'abort() at ' + stackTrace();
}
Module['abort'] = Module.abort = abort;
// {{PRE_RUN_ADDITIONS}}
if (Module['preInit']) {
  if (typeof Module['preInit'] == 'function') Module['preInit'] = [Module['preInit']];
  while (Module['preInit'].length > 0) {
    Module['preInit'].pop()();
  }
}
// shouldRunNow refers to calling main(), not run().
var shouldRunNow = true;
if (Module['noInitialRun']) {
  shouldRunNow = false;
}
run();
// {{POST_RUN_ADDITIONS}}
// {{MODULE_ADDITIONS}}
